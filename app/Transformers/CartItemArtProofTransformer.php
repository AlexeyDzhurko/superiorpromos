<?php


namespace App\Transformers;


use App\Models\CartItemArtProof;
use League\Fractal\TransformerAbstract;

class CartItemArtProofTransformer extends TransformerAbstract
{
    /**
     * Class CartItemArtProofTransformer
     * @param CartItemArtProof $cartItemArtProof
     * @return array
     * @package App\Transformers
     *
     * @SWG\Definition (
     *      definition="CartItemArtProof",
     *      @SWG\Property (property="id", type="integer", example="1"),
     *      @SWG\Property (property="name", type="string"),
     *      @SWG\Property (property="path", type="string"),
     * ),
     */
    public function transform(CartItemArtProof $cartItemArtProof)
    {
        return [
            'id' => $cartItemArtProof->id,
            'name' => $cartItemArtProof->name,
            'path' => $cartItemArtProof->path
        ];
    }
}
