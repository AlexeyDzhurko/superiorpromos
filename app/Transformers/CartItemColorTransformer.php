<?php


namespace App\Transformers;


use App\Models\CartItemColor;
use League\Fractal\TransformerAbstract;

/**
 * Class CartItemColorTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="CartItemColor",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="color_group_id", type="integer", example="1"),
 *      @SWG\Property (property="color_id", type="integer", example="3"),
 *      @SWG\Property (property="color_group_name", type="string", example="Items Colors"),
 *      @SWG\Property (property="color_name", type="string", example="Red"),
 * ),
 *
 */
class CartItemColorTransformer extends TransformerAbstract
{
    public function transform(CartItemColor $cartItemColor)
    {
        return [
            'id' => $cartItemColor->id,
            'color_group_id' => $cartItemColor->product_color_group_id,
            'color_id' => $cartItemColor->product_color_id,
            'color_group_name' => $cartItemColor->color_group_name,
            'color_name' => $cartItemColor->color_name,
        ];
    }
}
