<?php

namespace App\Transformers;

use App\PaymentProfile;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class PaymentProfileTransformer extends TransformerAbstract
{
    public function transform(PaymentProfile $paymentProfile)
    {
        return [
            'id' => $paymentProfile->id,
            'card_holder' => $paymentProfile->card_holder,
            'card_number' => $paymentProfile->card_number,
            'card_type' => $paymentProfile->card_type,
            'expiration_month' => strlen($paymentProfile->expiration_month) == 2 ? $paymentProfile->expiration_month : '0' . $paymentProfile->expiration_month,
            'expiration_year' => substr($paymentProfile->expiration_year, -2),
            'first_name' => $paymentProfile->first_name,
            'last_name' => $paymentProfile->last_name,
            'address1' => $paymentProfile->address1,
            'address2' => $paymentProfile->address2,
            'company' => $paymentProfile->company,
            'city' => $paymentProfile->city,
            'state' => $paymentProfile->state,
            'zip' => $paymentProfile->zip,
            'country' => $paymentProfile->country,
            'phone' => $paymentProfile->phone,
            'phone_extension' => $paymentProfile->phone_extension,
            'is_default' => $paymentProfile->is_default,
            'is_expired' => $this->isExpired($paymentProfile)
        ];
    }

    private function isExpired(PaymentProfile $paymentProfile)
    {
        if(is_null($paymentProfile->expiration_year)) {
            return true;
        }
        $now = Carbon::now()->endOfMonth();
        $expirationYear = Carbon::createFromFormat('Y', $paymentProfile->expiration_year)->year;
        if (strlen($expirationYear) != 4) {
            $expirationYear = Carbon::createFromFormat('y', $paymentProfile->expiration_year)->year;
        }
        $expires = Carbon::createFromFormat("m - Y", $paymentProfile->expiration_month . ' - ' . $expirationYear)->endOfMonth();
        return $expires < $now;
    }
}
