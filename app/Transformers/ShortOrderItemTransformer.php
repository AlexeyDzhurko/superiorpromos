<?php

namespace App\Transformers;

use App\Admin\Transformers\TrackingInformationTransformer;
use App\Admin\Transformers\UserNoteTransformer;
use App\Models\OrderItem;
use App\Models\Stage;
use App\Models\TrackingInformation;
use App\Payment\OrderItemCost;
use League\Fractal\TransformerAbstract;

/**
 *
 * @SWG\Definition (
 *      definition="ShortOrderItem",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="stage", ref="#/definitions/Stage"),
 *      @SWG\Property (property="order_item_cost", ref="#/definitions/ShortOrderItemCost"),
 *      @SWG\Property (property="product", ref="#/definitions/ShortProduct"),
 *      @SWG\Property (property="tracking_information", ref="#/definitions/TrackingInformation"),
 * ),
 *
 * Class ShortOrderItemTransformer
 * @package App\Transformers
 */
class ShortOrderItemTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'stage',
        'order_item_cost',
        'product',
        'art_proofs',
        'notes',
        'cart_item_data',
        'tracking_information',
    ];

    /**
     * A Fractal transformer.
     *
     * @param OrderItem $orderItem
     * @return array
     */
    public function transform(OrderItem $orderItem)
    {
        return [
            'id' => $orderItem->id,
        ];
    }

    public function includeStage(OrderItem $orderItem)
    {
        return $this->item($orderItem->stage ?? new Stage(), new StageTransformer());
    }

    public function includeTrackingInformation(OrderItem $orderItem)
    {
        return $this->item($orderItem->trackingInformations->first() ?? new TrackingInformation(), new TrackingInformationTransformer());
    }

    public function includeOrderItemCost(OrderItem $orderItem)
    {
        return $this->item(new OrderItemCost($orderItem), new ShortOrderItemCostTransformer());
    }

    public function includeProduct(OrderItem $orderItem)
    {
        return $this->item($orderItem->product, new ShortProductTransformer());
    }

    public function includeArtProofs(OrderItem $orderItem)
    {
        return $this->collection($orderItem->artProofs, new ArtProofTransformer());
    }

    public function includeNotes(OrderItem $orderItem)
    {
        return $this->collection($orderItem->userNotes, new UserNoteTransformer());
    }
    
    public function includeCartItemData(OrderItem $orderItem)
    {
        return $this->item($orderItem->cartItem, new CartItemTransformer());
    }
}
