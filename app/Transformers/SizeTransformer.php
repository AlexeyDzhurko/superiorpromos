<?php


namespace App\Transformers;


use App\Models\Size;
use League\Fractal\TransformerAbstract;

/**
 * @SWG\Definition (
 *      definition="Size",
 *      @SWG\Property(property="id", type="integer", example="1"),
 *      @SWG\Property(property="name", type="string", example="XXL"),
 * ),
 *
 * Class SizeTransformer
 * @package App\Transformers
 */
class SizeTransformer extends TransformerAbstract
{
    public function transform(Size $size)
    {
        return [
            'id' => $size->id,
            'name' => $size->name,
        ];
    }
}
