<?php


namespace App\Transformers;


use App\Models\Product;
use App\Models\Review;
use League\Fractal\TransformerAbstract;

class ReviewTransformer extends TransformerAbstract
{

    protected $defaultIncludes = [
        'product',
    ];

    public function transform(Review $review)
    {
        return [
            'id' => $review->id,
            'reviewer_name' => $review->reviewer_name,
            'reviewer_email' => $review->reviewer_email,
            'company_name' => $review->company_name,
            'review_date' => $review->review_date,
            'text' => $review->text,
            'rating' => $review->rating,
            'approve' => $review->approve,
            'user_id' => $review->user_id,
            'created_at' => $review->created_at->timestamp * 1000,
        ];
    }

    public function includeProduct(Review $review)
    {
        return $this->item($review->product, function(Product $product) {
            return [
                'id' => $product->id,
                'name' => $product->name,
            ];
        });
    }

}
