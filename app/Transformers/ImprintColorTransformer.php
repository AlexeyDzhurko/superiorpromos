<?php


namespace App\Transformers;


use App\Models\ImprintColor;
use League\Fractal\TransformerAbstract;

class ImprintColorTransformer extends TransformerAbstract
{
    public function transform(ImprintColor $imprintColor)
    {
        return [
            'id' => $imprintColor->id,
            'color' => [
                'id' => $imprintColor->color->id,
                'title' => $imprintColor->color->name,
                'color_hex' => $imprintColor->color->color_hex
            ]
        ];
    }
}
