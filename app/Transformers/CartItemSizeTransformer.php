<?php

namespace App\Transformers;

use App\Models\CartItemSize;
use League\Fractal\TransformerAbstract;

/**
 * @SWG\Definition (
 *      definition="CartItemSize",
 *      @SWG\Property(property="id", type="integer", example="3"),
 *      @SWG\Property(property="product_size_id", type="integer", example="1"),
 *      @SWG\Property(property="qty", type="string", example="150"),
 * ),
 *
 * Class CartItemSizeTransformer
 * @package App\Transformers
 */
class CartItemSizeTransformer extends TransformerAbstract
{
    public function transform(CartItemSize $cartItemSize)
    {
        return [
            'id' => $cartItemSize->id,
            'product_size_id' => $cartItemSize->size_id,
            'qty' => $cartItemSize->qty,
        ];
    }
}
