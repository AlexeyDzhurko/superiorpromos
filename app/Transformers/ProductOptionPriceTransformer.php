<?php


namespace App\Transformers;


use App\Models\ProductOptionPrice;
use League\Fractal\TransformerAbstract;

/**
 * Class ProductOptionPriceTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="ProductOptionPrice",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="quantity", type="integer", example="1000"),
 *      @SWG\Property (property="setup_price", type="string", example="10"),
 *      @SWG\Property (property="item_price", type="string", example="0.5"),
 * ),
 *
 */
class ProductOptionPriceTransformer extends TransformerAbstract
{
    public function transform(ProductOptionPrice $productOptionPrice)
    {
        return [
            'id' => $productOptionPrice->id,
            'quantity' => $productOptionPrice->quantity,
            'setup_price' => $productOptionPrice->setup_price,
            'item_price' => $productOptionPrice->item_price,
        ];
    }
}
