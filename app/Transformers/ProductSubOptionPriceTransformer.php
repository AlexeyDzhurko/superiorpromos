<?php


namespace App\Transformers;


use App\Models\ProductSubOptionPrice;
use League\Fractal\TransformerAbstract;

/**
 * Class ProductSubOptionPriceTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="ProductSubOptionPrice",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="quantity", type="integer", example="1000"),
 *      @SWG\Property (property="setup_price", type="string", example="10"),
 *      @SWG\Property (property="item_price", type="string", example="0.5"),
 * ),
 *
 */
class ProductSubOptionPriceTransformer extends TransformerAbstract
{
    public function transform(ProductSubOptionPrice $productSubOptionPrice)
    {
        return [
            'id' => $productSubOptionPrice->id,
            'quantity' => $productSubOptionPrice->quantity,
            'setup_price' => $productSubOptionPrice->setup_price,
            'item_price' => $productSubOptionPrice->item_price,
        ];
    }

}
