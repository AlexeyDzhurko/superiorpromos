<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 14.04.17
 * Time: 14:22
 */

namespace App\Transformers;


use App\Models\OrderItem;
use App\Models\Stage;
use App\Payment\CartItemCost;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class TinyOrderItemTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'product',
        'vendor',
        'stage',
        'cart_item',
        'product_vendors',
    ];

    public function transform(OrderItem $orderItem)
    {
        return [
            'id' => $orderItem->id,
            'order_id' => $orderItem->order_id,
            'price' => $orderItem->price,
            'price_total' => (new CartItemCost($orderItem->cartItem))->getTotal(),
            'price_delivery' => ($orderItem->price + $orderItem->estimation_shipping_price),
            'received_date' => $orderItem->received_date != '0000-00-00 00:00:00' && !is_null($orderItem->received_date)
                ? Carbon::parse($orderItem->received_date)->format('m/d/y g:i A')
                : 'ASAP',
            'po_number' => $orderItem->po_number,
            'check_notes' => $orderItem->check_notes,
            'not_paid' => $orderItem->not_paid,
            'order_items_count' => !is_null($orderItem->vendor->id ?? null) ? $orderItem->where('vendor_id', $orderItem->vendor->id)->count() : 0,
            'is_important' => $orderItem->is_important,
        ];
    }

    public function includeCartItem(OrderItem $orderItem)
    {
        if (is_null($orderItem->cartItem)) {
            return $this->null();
        }

        return $this->item($orderItem->cartItem, new CartItemTransformer());
    }

    public function includeStage(OrderItem $orderItem)
    {
        return $this->item($orderItem->stage ?? new Stage(), new StageTransformer());
    }

    public function includeProduct(OrderItem $orderItem)
    {
        if (empty($orderItem->product)) {
            return $this->null();
        }

        return $this->item($orderItem->product, function($product) use($orderItem) {
            return [
                'name' => $product->name,
                'id' => $product->id,
            ];
        });
    }

    public function includeVendor(OrderItem $orderItem)
    {
        if (empty($orderItem->product)) {
            return $this->null();
        }

        return $this->item($orderItem->vendor, function($vendor) {
            if(empty($vendor)) {
                return [];
            } else {
                return [
                    'id' => $vendor->id,
                    'name' => $vendor->name ?? null
                ];
            }
        });
    }

    public function includeProductVendors(OrderItem $orderItem)
    {
        if (empty($orderItem->product)) {
            return $this->null();
        }
        return $this->collection($orderItem->product->vendors, function($vendor) {
            return empty($vendor) ? [] : [
                'id' => $vendor->id,
                'name' => $vendor->name ?? null,
                'SKU' => $vendor->pivot->sku,
            ];
        });
    }
}
