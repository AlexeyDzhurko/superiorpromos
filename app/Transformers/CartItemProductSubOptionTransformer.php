<?php


namespace App\Transformers;


use App\Models\CartItemProductSubOption;
use League\Fractal\TransformerAbstract;

/**
 * Class CartItemProductSubOptionTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="CartItemProductSubOption",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="product_sub_option_id", type="integer", example="1"),
 *      @SWG\Property (property="name", type="string", example="Some Sub Option"),
 *      @SWG\Property (property="setup_price", type="string", example="10"),
 *      @SWG\Property (property="item_price", type="string", example="0.1"),
 * ),
 *
 */
class CartItemProductSubOptionTransformer extends TransformerAbstract
{
    public function transform(CartItemProductSubOption $cartItemProductSubOption)
    {
        return [
            'id' => $cartItemProductSubOption->id,
            'product_sub_option_id' => $cartItemProductSubOption->product_sub_option_id,
            'name' => $cartItemProductSubOption->name,
            'setup_price' => $cartItemProductSubOption->setup_price,
            'item_price' => $cartItemProductSubOption->item_price,
        ];
    }
}
