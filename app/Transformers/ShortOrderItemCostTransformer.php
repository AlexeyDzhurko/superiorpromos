<?php

namespace App\Transformers;

use App\Payment\OrderItemCost;
use League\Fractal\TransformerAbstract;

/**
 *
 * @SWG\Definition (
 *      definition="ShortOrderItemCost",
 *      @SWG\Property (property="shipping", type="string", example="100.2"),
 *      @SWG\Property (property="total", type="string", example="587.32"),
 * ),
 *
 * Class ShortOrderItemCostTransformer
 * @package App\Transformers
 */
class ShortOrderItemCostTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param OrderItemCost $orderItemCost
     * @return array
     */
    public function transform(OrderItemCost $orderItemCost)
    {
        return [
            'shipping' => $orderItemCost->getShipping(),
            'total' => $orderItemCost->getTotal(),
        ];
    }
}
