<?php

namespace App\Transformers;

use App\Models\OrderSample;
use League\Fractal\TransformerAbstract;

class OrderSampleTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'order_sample_items',
    ];

    /**
     * A Fractal transformer.
     *
     * @param OrderSample $orderSample
     * @return array
     */
    public function transform(OrderSample $orderSample)
    {
        $userShippingData = $orderSample->user->addresses->where('type',1)->first();
        $userBillingData = $orderSample->user->addresses->where('type',2)->first();
        
        return [
            'id' => $orderSample->id,
            'created_at' => $orderSample->created_at->timestamp * 1000,
            'total' => $orderSample->total_price,
            
            'shipping_first_name' => $userShippingData->first_name,
            'shipping_middle_name' => $userShippingData->middle_name,
            'shipping_last_name' => $userShippingData->last_name,
            'shipping_title' => $userShippingData->title,
            'shipping_suffix' => $userShippingData->suffix,
            'shipping_company_name' => $userShippingData->company_name,
            'shipping_address_line_1' => $userShippingData->address_line_1,
            'shipping_address_line_2' => $userShippingData->address_line_2,
            'shipping_city' => $userShippingData->city,
            'shipping_state' => $userShippingData->state,
            'shipping_zip' => $userShippingData->zip,
            'shipping_country' => $userShippingData->country,
            'shipping_province' => $userShippingData->province,
            'shipping_day_telephone' => $userShippingData->day_telephone,
            'shipping_ext' => $userShippingData->ext,
            'shipping_fax' => $userShippingData->fax,

            'billing_first_name' => $userBillingData->first_name,
            'billing_middle_name' => $userBillingData->middle_name,
            'billing_last_name' => $userBillingData->last_name,
            'billing_title' => $userBillingData->title,
            'billing_suffix' => $userBillingData->suffix,
            'billing_company_name' => $userBillingData->company_name,
            'billing_address_line_1' => $userBillingData->address_line_1,
            'billing_address_line_2' => $userBillingData->address_line_2,
            'billing_city' => $userBillingData->city,
            'billing_state' => $userBillingData->state,
            'billing_zip' => $userBillingData->zip,
            'billing_country' => $userBillingData->country,
            'billing_province' => $userBillingData->province,
            'billing_day_telephone' => $userBillingData->day_telephone,
            'billing_ext' => $userBillingData->ext,
            'billing_fax' => $userBillingData->fax,
        ];
    }

    public function includeOrderSampleItems(OrderSample $orderSample)
    {
        return $this->collection($orderSample->orderSampleItems, new OrderSampleItemTransformer());
    }

}
