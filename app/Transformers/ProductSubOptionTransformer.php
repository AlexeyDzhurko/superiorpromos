<?php


namespace App\Transformers;


use App\Models\ProductSubOption;
use League\Fractal\TransformerAbstract;

/**
 * Class ProductSubOptionTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="ProductSubOption",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="name", type="string", example="Sub Option Name"),
 *      @SWG\Property (property="product_sub_option_prices", type="array", items=@SWG\Schema(ref="#/definitions/ProductSubOptionPrice")),
 * ),
 *
 */
class ProductSubOptionTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'product_sub_option_prices',
    ];

    public function transform(ProductSubOption $productSubOption)
    {
        return [
            'id' => $productSubOption->id,
            'name' => $productSubOption->name,
        ];
    }

    public function includeProductSubOptionPrices(ProductSubOption $productSubOption)
    {
        return $this->collection($productSubOption->productSubOptionPrices, new ProductSubOptionPriceTransformer());
    }

}
