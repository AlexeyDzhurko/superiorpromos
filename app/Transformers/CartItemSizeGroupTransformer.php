<?php

namespace App\Transformers;

use App\Models\CartItemSizeGroup;
use League\Fractal\TransformerAbstract;

/**
 *
 * @SWG\Definition (
 *      definition="CartItemSizeGroup",
 *      @SWG\Property(property="id", type="integer", example="3"),
 *      @SWG\Property(property="product_size_group_id", type="integer", example="1"),
 *      @SWG\Property (property="cart_item_sizes", type="array", items=@SWG\Schema(ref="#/definitions/CartItemSize")),
 * ),
 *
 * Class CartItemSizeGroupTransformer
 * @package App\Transformers
 */
class CartItemSizeGroupTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'cart_item_sizes'
    ];

    public function transform(CartItemSizeGroup $cartItemSizeGroup)
    {
        return [
            'id' => $cartItemSizeGroup->id,
            'product_size_group_id' => $cartItemSizeGroup->size_group_id,
        ];
    }

    public function includeCartItemSizes(CartItemSizeGroup $cartItemSizeGroup)
    {
        return $this->collection($cartItemSizeGroup->cartItemSizes, new CartItemSizeTransformer());
    }
}
