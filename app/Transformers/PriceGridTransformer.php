<?php

namespace App\Transformers;

use App\Models\ProductPrice;
use League\Fractal\TransformerAbstract;

/**
 * Class PriceGridTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="PriceGridTransformer",
 *      @SWG\Property (property="quantity", type="string", example="96"),
 *      @SWG\Property (property="regular_price", type="string", example="5.59"),
 *      @SWG\Property (property="sale_price", type="string", example="3.69"),
 *      @SWG\Property (property="setup_price", type="string", example="1.69"),
 * ),
 */
class PriceGridTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ProductPrice $productPrice)
    {
        return [
            "quantity" => (int)$productPrice->quantity,
            "regular_price" => (float)$productPrice->item_price,
            "sale_price" => (float)$productPrice->sale_item_price,
            "setup_price" => (float)$productPrice->setup_price,
        ];
    }
}
