<?php


namespace App\Transformers;


use App\Discounts\DiscountCalc;
use League\Fractal\TransformerAbstract;

class DiscountCalcTransformer extends TransformerAbstract
{
    public function transform(DiscountCalc $discountCalc)
    {
        return [
            'amount' => number_format($discountCalc->getDiscountAmount(), 2),
            'totalWithDiscount' => number_format($discountCalc->getTotalWithDiscount(), 2),
            'title' => $discountCalc::getTitle(),
            'code' => $discountCalc->getDiscountCode(),
        ];
    }
}
