<?php


namespace App\Transformers;


use App\Models\Survey;
use League\Fractal\TransformerAbstract;

class SurveyTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'survey_questions',
    ];

    public function transform(Survey $survey)
    {
        return [
            'id' => $survey->id,
            'name' => $survey->name,
        ];
    }

    public function includeSurveyQuestions(Survey $survey)
    {
        return $this->collection($survey->surveyQuestions, new SurveyQuestionTransformer());
    }
}
