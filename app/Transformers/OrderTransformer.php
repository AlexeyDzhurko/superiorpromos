<?php

namespace App\Transformers;

use App\Admin\Transformers\PaymentTransformer;
use App\Models\Order;
use App\Models\Payment;
use League\Fractal\TransformerAbstract;

/**
 *
 * @SWG\Definition (
 *      definition="Order",
 *      @SWG\Property(property="id", type="integer", example="1"),
 *      @SWG\Property(property="created_at", type="string", example="1494336788000"),
 *      @SWG\Property(property="total", type="decimal", example="1500.00"),
 *      @SWG\Property(property="shipping_first_name", type="string", example="Tim"),
 *      @SWG\Property(property="shipping_middle_name", type="string", example="Aero"),
 *      @SWG\Property(property="shipping_last_name", type="string", example="White"),
 *      @SWG\Property(property="shipping_title", type="integer", example="Boxes"),
 *      @SWG\Property(property="shipping_suffix", type="string", example="123"),
 *      @SWG\Property(property="shipping_company_name", type="string", example="SkyTech"),
 *      @SWG\Property(property="shipping_address_line_1", type="string", example="70 Calle La Sombra, Camarillo, CA 93010, USA"),
 *      @SWG\Property(property="shipping_address_line_2", type="string", example="76 Calle La Sombra, Camarillo, CA 93010, USA"),
 *      @SWG\Property(property="shipping_city", type="string", example="Camarillo"),
 *      @SWG\Property(property="shipping_state", type="string", example="CA"),
 *      @SWG\Property(property="shipping_zip", type="string", example="93010"),
 *      @SWG\Property(property="shipping_country", type="string", example="USA"),
 *      @SWG\Property(property="shipping_province", type="string", example="California"),
 *      @SWG\Property(property="shipping_day_telephone", type="string", example="987-564-1111"),
 *      @SWG\Property(property="shipping_ext", type="string", example="988"),
 *      @SWG\Property(property="shipping_fax", type="string", example="227-514-1123"),
 *      @SWG\Property(property="billing_first_name", type="string", example="Tim"),
 *      @SWG\Property(property="billing_middle_name", type="string", example="Aero"),
 *      @SWG\Property(property="billing_last_name", type="string", example="White"),
 *      @SWG\Property(property="billing_title", type="integer", example="Boxes"),
 *      @SWG\Property(property="billing_suffix", type="string", example="123"),
 *      @SWG\Property(property="billing_company_name", type="string", example="SkyTech"),
 *      @SWG\Property(property="billing_address_line_1", type="string", example="70 Calle La Sombra, Camarillo, CA 93010, USA"),
 *      @SWG\Property(property="billing_address_line_2", type="string", example="76 Calle La Sombra, Camarillo, CA 93010, USA"),
 *      @SWG\Property(property="billing_city", type="string", example="Camarillo"),
 *      @SWG\Property(property="billing_state", type="string", example="CA"),
 *      @SWG\Property(property="billing_zip", type="string", example="93010"),
 *      @SWG\Property(property="billing_country", type="string", example="USA"),
 *      @SWG\Property(property="billing_province", type="string", example="California"),
 *      @SWG\Property(property="billing_day_telephone", type="string", example="987-564-1111"),
 *      @SWG\Property(property="billing_ext", type="string", example="988"),
 *      @SWG\Property(property="billing_fax", type="string", example="227-514-1123"),
 *      @SWG\Property (property="cart_items", ref="#/definitions/ShortOrderItem"),
 * ),
 *
 * Class OrderTransformer
 * @package App\Transformers
 */
class OrderTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'cart_items',
        'payment_information',
        'order_confirmation',
    ];

    /**
     * A Fractal transformer.
     *
     * @param Order $order
     * @return array
     */
    public function transform(Order $order)
    {
        return [
            'id' => $order->id,
            'created_at' => $order->created_at->timestamp * 1000,
            'total' => $order->total_price,
            'price' => $order->price,
            'discount_amount' => $order->discount_amount,
            'discount_code' => $order->discount_code,
            'shipping_first_name' => $order->shipping_first_name,
            'shipping_middle_name' => $order->shipping_middle_name,
            'shipping_last_name' => $order->shipping_last_name,
            'shipping_title' => $order->shipping_title,
            'shipping_suffix' => $order->shipping_suffix,
            'shipping_company_name' => $order->shipping_company_name,
            'shipping_address_line_1' => $order->shipping_address_line_1,
            'shipping_address_line_2' => $order->shipping_address_line_2,
            'shipping_city' => $order->shipping_city,
            'shipping_state' => $order->shipping_state,
            'shipping_zip' => $order->shipping_zip,
            'shipping_country' => $order->shipping_country,
            'shipping_province' => $order->shipping_province,
            'shipping_day_telephone' => $order->shipping_day_telephone,
            'shipping_ext' => $order->shipping_ext,
            'shipping_fax' => $order->shipping_fax,
            'billing_first_name' => $order->billing_first_name,
            'billing_middle_name' => $order->billing_middle_name,
            'billing_last_name' => $order->billing_last_name,
            'billing_title' => $order->billing_title,
            'billing_suffix' => $order->billing_suffix,
            'billing_company_name' => $order->billing_company_name,
            'billing_address_line_1' => $order->billing_address_line_1,
            'billing_address_line_2' => $order->billing_address_line_2,
            'billing_city' => $order->billing_city,
            'billing_state' => $order->billing_state,
            'billing_zip' => $order->billing_zip,
            'billing_country' => $order->billing_country,
            'billing_province' => $order->billing_province,
            'billing_day_telephone' => $order->billing_day_telephone,
            'billing_ext' => $order->billing_ext,
            'billing_fax' => $order->billing_fax,

        ];
    }

    public function includeCartItems(Order $order)
    {
        return $this->collection($order->orderItems, new ShortOrderItemTransformer());
    }

    public function includePaymentInformation(Order $order)
    {
        return $this->item($order->payment ?? new Payment(), new PaymentTransformer());
    }

    public function includeOrderConfirmation(Order $order)
    {
        return $this->collection($order->cart->cartItems, new CartItemTransformer());
    }
}
