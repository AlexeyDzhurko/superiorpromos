<?php


namespace App\Transformers;


use App\Models\ProductPrice;
use League\Fractal\TransformerAbstract;

class ProductPriceTransformer extends TransformerAbstract
{
    public function transform(ProductPrice $productPrice)
    {
        return [
            'id' => $productPrice->id,
            'quantity' => (int)$productPrice->quantity,
            'setup_price' => (float)$productPrice->setup_price,
            'item_price' => (float)$productPrice->item_price,
            'sale_item_price' => (float)$productPrice->sale_item_price,
        ];
    }

}
