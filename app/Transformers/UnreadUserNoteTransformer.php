<?php

namespace App\Transformers;

use App\Models\UserNote;
use League\Fractal\TransformerAbstract;

/**
 * Class UnreadUserNoteTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="UnreadUserNote",
 *      @SWG\Property(property="id", type="integer", example="1"),
 *      @SWG\Property(property="created_at", type="string", example="1494336788000"),
 *      @SWG\Property(property="subject", type="string", example="Note subject"),
 *      @SWG\Property(property="note", type="string", example="<p>Test test</p>"),
 *      @SWG\Property(property="approved_required", type="boolean", example=1),
 *      @SWG\Property(property="customer_note", type="string", example="my note about this art proof"),
 *      @SWG\Property(property="answered_at", type="string", example="1504090422000"),
 *      @SWG\Property(property="status", type="string", example="declined"),
 *      @SWG\Property(property="order_id", type="integer", example=1345),
 *      @SWG\Property(property="order_item_id", type="integer", example=32541)
 * ),
 */
class UnreadUserNoteTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(UserNote $userNote)
    {
        return [
            'id' => $userNote->id,
            'created_at' => $userNote->created_at->timestamp * 1000,
            'subject' => $userNote->subject,
            'note' => $userNote->note,
            'customer_note' => $userNote->customer_note,
            'approved_required' => $userNote->approved_required,
            'answered_at' => $userNote->answered_at ? $userNote->answered_at->timestamp * 1000 : null,
            'status' => is_null($userNote->approved) ? 'N/A' : ($userNote->approved == 1 ? 'approved' : 'declined'),
            'order_id' => $userNote->orderItem->order->id,
            'order_item_id' => $userNote->orderItem->id,
        ];
    }
}
