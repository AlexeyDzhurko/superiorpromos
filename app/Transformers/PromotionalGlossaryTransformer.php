<?php


namespace App\Transformers;


use App\Models\PromotionalGlossary;
use League\Fractal\TransformerAbstract;

class PromotionalGlossaryTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     * @param PromotionalGlossary $promotionalGlossary
     * @return array
     */
    public function transform(PromotionalGlossary $promotionalGlossary)
    {
        return [
            'title' => $promotionalGlossary->title ?? null,
            'body' => $promotionalGlossary->body ?? null,
            'position' => $promotionalGlossary->position ?? null,
        ];
    }

}
