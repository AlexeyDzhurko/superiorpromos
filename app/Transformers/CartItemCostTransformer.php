<?php

namespace App\Transformers;

use App\Payment\CartItemCost;
use App\Payment\OrderItemCost;
use League\Fractal\TransformerAbstract;

/**
 *
 * @SWG\Definition (
 *      definition="CartItemCost",
 *      @SWG\Property (property="quantity", type="integer", example="1000"),
 *      @SWG\Property (property="base_item_price", type="string", example="2.6"),
 *      @SWG\Property (property="final_item_price", type="string", example="2.8"),
 *      @SWG\Property (property="shipping", type="string", example="100.3"),
 *      @SWG\Property (property="total", type="string", example="5482.2"),
 *      @SWG\Property (property="product_options", type="array", items=@SWG\Schema(ref="#/definitions/AdditionalOption")),
 *      @SWG\Property (property="imprints", type="array", items=@SWG\Schema(ref="#/definitions/AdditionalOption")),
 * ),
 *
 * Class OrderItemCostTransformer
 * @package App\Transformers
 */
class CartItemCostTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'product_options',
//        'imprints',
    ];

    /**
     * A Fractal transformer.
     *
     * @param CartItemCost $cartItemCost
     * @return array
     */
    public function transform(CartItemCost $cartItemCost)
    {
        return [
            'quantity' => $cartItemCost->getQuantity(),
            'base_item_price' => $cartItemCost->getBaseItemPrice(),
            'base_sale_price' => $cartItemCost->getSaleItemPrice(),
            'final_item_price' => $cartItemCost->getFinalItemPrice(),
            'base_setup_price' => $cartItemCost->getSetupPrice(),
            'shipping' => $cartItemCost->getShipping(),
            'total' => $cartItemCost->getTotal(),
            'imprints' => $cartItemCost->getImprints(),
        ];
    }

    public function includeProductOptions(CartItemCost $cartItemCost)
    {
        return $this->collection($cartItemCost->getProductOptions(), new AdditionalOptionTransformer());
    }

    public function includeImprints(CartItemCost $cartItemCost)
    {
        return $this->collection($cartItemCost->getImprints(), new AdditionalOptionTransformer());
    }
}

