<?php

namespace App\Transformers;

use App\Models\OrderItem;
use App\Models\Stage;
use App\Payment\OrderItemCost;
use League\Fractal\TransformerAbstract;

/**
 * Class FullOrderItemTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="FullOrderItem",
 *      @SWG\Property(property="id", type="integer", example="1"),
 *      @SWG\Property(property="created_at", type="string", example="1494336788000"),
 *      @SWG\Property(property="tracking_shipping_date", type="string", example="1494336788000"),
 *      @SWG\Property(property="tracking_shipping_company", type="string", example="USPS"),
 *      @SWG\Property(property="tracking_number", type="string", example="1231234"),
 *      @SWG\Property(property="shipping_first_name", type="string", example="Tim"),
 *      @SWG\Property(property="shipping_middle_name", type="string", example="Aero"),
 *      @SWG\Property(property="shipping_last_name", type="string", example="White"),
 *      @SWG\Property(property="shipping_title", type="integer", example="Boxes"),
 *      @SWG\Property(property="shipping_suffix", type="string", example="123"),
 *      @SWG\Property(property="shipping_company_name", type="string", example="SkyTech"),
 *      @SWG\Property(property="shipping_address_line_1", type="string", example="70 Calle La Sombra, Camarillo, CA 93010, USA"),
 *      @SWG\Property(property="shipping_address_line_2", type="string", example="76 Calle La Sombra, Camarillo, CA 93010, USA"),
 *      @SWG\Property(property="shipping_city", type="string", example="Camarillo"),
 *      @SWG\Property(property="shipping_state", type="string", example="CA"),
 *      @SWG\Property(property="shipping_zip", type="string", example="93010"),
 *      @SWG\Property(property="shipping_country", type="string", example="USA"),
 *      @SWG\Property(property="shipping_province", type="string", example="California"),
 *      @SWG\Property(property="shipping_day_telephone", type="string", example="987-564-1111"),
 *      @SWG\Property(property="shipping_ext", type="string", example="988"),
 *      @SWG\Property(property="shipping_fax", type="string", example="227-514-1123"),
 *      @SWG\Property(property="billing_first_name", type="string", example="Tim"),
 *      @SWG\Property(property="billing_middle_name", type="string", example="Aero"),
 *      @SWG\Property(property="billing_last_name", type="string", example="White"),
 *      @SWG\Property(property="billing_title", type="integer", example="Boxes"),
 *      @SWG\Property(property="billing_suffix", type="string", example="123"),
 *      @SWG\Property(property="billing_company_name", type="string", example="SkyTech"),
 *      @SWG\Property(property="billing_address_line_1", type="string", example="70 Calle La Sombra, Camarillo, CA 93010, USA"),
 *      @SWG\Property(property="billing_address_line_2", type="string", example="76 Calle La Sombra, Camarillo, CA 93010, USA"),
 *      @SWG\Property(property="billing_city", type="string", example="Camarillo"),
 *      @SWG\Property(property="billing_state", type="string", example="CA"),
 *      @SWG\Property(property="billing_zip", type="string", example="93010"),
 *      @SWG\Property(property="billing_country", type="string", example="USA"),
 *      @SWG\Property(property="billing_province", type="string", example="California"),
 *      @SWG\Property(property="billing_day_telephone", type="string", example="987-564-1111"),
 *      @SWG\Property(property="billing_ext", type="string", example="988"),
 *      @SWG\Property(property="billing_fax", type="string", example="227-514-1123"),
 *      @SWG\Property (property="stage", ref="#/definitions/Stage"),
 *      @SWG\Property (property="order_item_cost", ref="#/definitions/OrderItemCost"),
 *      @SWG\Property (property="cart_item", ref="#/definitions/CartItem")),
 * ),
 *
 */
class FullOrderItemTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'stage',
        'order_item_cost',
        'cart_item',
        'art_proofs'
    ];

    /**
     * A Fractal transformer.
     *
     * @param OrderItem $orderItem
     * @return array
     */
    public function transform(OrderItem $orderItem)
    {
        return [
            'id' => $orderItem->id,
            'created_at' => $orderItem->created_at->timestamp * 1000,

            'tracking_shipping_date' => $orderItem->tracking_shipping_date ? $orderItem->tracking_shipping_date->timestamp * 1000 : null,
            'tracking_shipping_company' => $orderItem->tracking_shipping_company,
            'tracking_number' => $orderItem->tracking_number,

            'shipping_first_name' => $orderItem->shipping_first_name,
            'shipping_middle_name' => $orderItem->shipping_middle_name,
            'shipping_last_name' => $orderItem->shipping_last_name,
            'shipping_title' => $orderItem->shipping_title,
            'shipping_suffix' => $orderItem->shipping_suffix,
            'shipping_company_name' => $orderItem->shipping_company_name,
            'shipping_address_line_1' => $orderItem->shipping_address_line_1,
            'shipping_address_line_2' => $orderItem->shipping_address_line_2,
            'shipping_city' => $orderItem->shipping_city,
            'shipping_state' => $orderItem->shipping_state,
            'shipping_zip' => $orderItem->shipping_zip,
            'shipping_country' => $orderItem->shipping_country,
            'shipping_province' => $orderItem->shipping_province,
            'shipping_day_telephone' => $orderItem->shipping_day_telephone,
            'shipping_ext' => $orderItem->shipping_ext,
            'shipping_fax' => $orderItem->shipping_fax,

            'billing_first_name' => $orderItem->billing_first_name,
            'billing_middle_name' => $orderItem->billing_middle_name,
            'billing_last_name' => $orderItem->billing_last_name,
            'billing_title' => $orderItem->billing_title,
            'billing_suffix' => $orderItem->billing_suffix,
            'billing_company_name' => $orderItem->billing_company_name,
            'billing_address_line_1' => $orderItem->billing_address_line_1,
            'billing_address_line_2' => $orderItem->billing_address_line_2,
            'billing_city' => $orderItem->billing_city,
            'billing_state' => $orderItem->billing_state,
            'billing_zip' => $orderItem->billing_zip,
            'billing_country' => $orderItem->billing_country,
            'billing_province' => $orderItem->billing_province,
            'billing_day_telephone' => $orderItem->sbilling_day_telephone,
            'billing_ext' => $orderItem->billing_ext,
            'billing_fax' => $orderItem->billing_fax,
        ];
    }

    public function includeStage(OrderItem $orderItem)
    {
        return $this->item($orderItem->stage ?? new Stage(), new StageTransformer());
    }

    public function includeOrderItemCost(OrderItem $orderItem)
    {
        return $this->item(new OrderItemCost($orderItem), new OrderItemCostTransformer());
    }

    public function includeCartItem(OrderItem $orderItem)
    {
        return $this->item($orderItem->cartItem, new CartItemTransformer());
    }

    public function includeArtProofs(OrderItem $orderItem)
    {
        return $this->collection($orderItem->artProofs, new ArtProofTransformer());
    }
}
