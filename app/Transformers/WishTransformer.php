<?php

namespace App\Transformers;

use App\Models\Wish;
use League\Fractal\TransformerAbstract;

/**
 * Class WishTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="Wish",
 *      @SWG\Property(property="id", type="integer", example="1"),
 *      @SWG\Property(property="created_at", type="string", example="1494336788000"),
 *      @SWG\Property (property="product", ref="#/definitions/Product"),
 * ),
 */
class WishTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'product',
    ];

    /**
     * A Fractal transformer.
     *
     * @param Wish $wish
     * @return array
     */
    public function transform(Wish $wish)
    {
        return [
            'id' => $wish->id,
            'created_at' => $wish->created_at->timestamp * 1000,
        ];
    }

    /**
     * @param Wish $wish
     * @return \League\Fractal\Resource\Item
     */
    public function includeProduct(Wish $wish)
    {
        $product = fractal($wish->product, new ProductTransformer())->toArray();

        return $this->item($product, new ProductBreadcrumbsTransformer([]));
    }
}
