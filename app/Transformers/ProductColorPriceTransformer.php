<?php


namespace App\Transformers;


use App\Models\ProductColorPrice;
use League\Fractal\TransformerAbstract;

/**
 * Class ProductColorPriceTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="ProductColorPriceTransformer",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="quantity", type="string", example="10"),
 *      @SWG\Property (property="setup_price", type="0.1", example="0.1"),
 *      @SWG\Property (property="item_price", type="0.5", example="0.1"),
 * ),
 */
class ProductColorPriceTransformer extends TransformerAbstract
{
    public function transform(ProductColorPrice $productColorPrice)
    {
        return [
            'id' => $productColorPrice->id,
            'quantity' => $productColorPrice->quantity,
            'setup_price' => $productColorPrice->setup_price,
            'item_price' => $productColorPrice->item_price,
        ];
    }
}
