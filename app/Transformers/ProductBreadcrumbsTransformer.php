<?php


namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class ProductBreadcrumbsTransformer extends TransformerAbstract
{
    protected $parameters;

    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

   public function transform(array $product)
   {
       $breadcrumbs = $this->filterBreadcrumbs($product);
       $product['full_slug'] = !empty($breadcrumbs['breadcrumbs']) ? mb_strtolower(end($breadcrumbs['breadcrumbs'])['slug'] . '/' . $product['slug']) : $product['slug'];
       $product['breadcrumbs'] = $this->attachProductToBreadcrumbs($breadcrumbs, $product);

       return $product;
   }

   private function filterBreadcrumbs(array $product)
   {
       if (!empty($this->parameters['categories']) && !empty($product['breadcrumbs'])) {
           foreach ($product['breadcrumbs'] as $breadcrumb) {
               if (in_array($breadcrumb['product_category_id'], $this->parameters['categories'])) {
                   return $breadcrumb;
               }
           }
       }

       $breadcrumbs = is_array($product['breadcrumbs']) ? $product['breadcrumbs'] : $product['breadcrumbs']->toArray();
       return array_shift($breadcrumbs);
   }

   private function attachProductToBreadcrumbs($breadcrumb, array $product)
   {
       $breadcrumb = $breadcrumb ?? [];
       $breadcrumb['breadcrumbs'][] = [
           'id'   => $product['id'],
           'name' => $product['title'],
           'slug' => $product['slug']
       ];

       return $breadcrumb;
   }
}
