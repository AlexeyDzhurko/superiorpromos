<?php

namespace App\Transformers;

use App\Models\Imprint;
use App\Models\Product;
use App\Models\Breadcrumb;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

/**
 * Class ProductTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="Product",
 *      @SWG\Property(property="id", type="integer", example="1"),
 *      @SWG\Property(property="thumbnail", type="string", example="CS.jpg"),
 *      @SWG\Property(property="title", type="string", example="Bic Clic Stic Revo Pen"),
 *      @SWG\Property(property="description", type="string", example="The most popular retractable in promotional products."),
 *      @SWG\Property(property="min_quantity", type="string", example="100"),
 *      @SWG\Property(property="production_time", type="string", example="within 1 working day*"),
 *      @SWG\Property(property="lowest_price", type="string", example="3.65"),
 *      @SWG\Property(property="slug", type="string", example="pens-pencils/pens-plastic/bic-clic-stic-pen"),
 *      @SWG\Property (property="product_color_groups", type="array", items=@SWG\Schema(ref="#/definitions/ProductColorGroup")),
 *      @SWG\Property (property="price_grid", type="array", items=@SWG\Schema(ref="#/definitions/PriceGridTransformer")),
 *      @SWG\Property (property="product_options", type="array", items=@SWG\Schema(ref="#/definitions/ProductOption")),
 *      @SWG\Property (property="imprints", type="array", items=@SWG\Schema(ref="#/definitions/Imprint")),
 *      @SWG\Property (property="product_extra_images", type="array", items=@SWG\Schema(ref="#/definitions/ProductExtraImage")),
 *      @SWG\Property (property="size_group", ref="#/definitions/SizeGroup"),
 * ),
 */
class ProductTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'product_color_groups',
        'price_grid',
        'product_options',
        'imprints',
        'product_extra_images',
        'size_group',
        'breadcrumbs',
        'reviews'
    ];

    public function transform(Product $product)
    {
        return [
            "id" => $product->id,
            "thumbnail" => $product->image_src,
            "title" => $product->name,
            'on_sale' => $product->on_sale,
            "description" => $product->description,
            "pricing_information" => $product->pricing_information,
            "min_quantity" => $this->getMinProductQuantity($product),
            "production_time" => "within 1 working day*",
            "production_time_from" => $product->production_time_from,
            "production_time_to" => $product->production_time_to,
            "lowest_price" => $this->getLowestProductPrice($product),
            "slug" => $product->url,
            "dimensions" => $product->dimensions,
            "imprint_area" => $product->imprint_area,
            "popularity" => $product->popularity,
            'custom_shipping' => $this->getCustomShipping($product)
        ];
    }

    /**
     * Include Prices
     *
     * @param Product $product
     * @return \League\Fractal\Resource\Collection
     */
    public function includePriceGrid(Product $product)
    {
        return $this->collection($product->productPrices->sortBy('quantity'), new PriceGridTransformer());
    }

    /**
     * Include Product Colors
     *
     * @param Product $product
     * @return \League\Fractal\Resource\Collection
     */
    public function includeProductColorGroups(Product $product)
    {
        return $this->collection($product->productColorGroups, new ProductColorGroupTransformer());
    }

    public function includeProductOptions(Product $product)
    {
        return $this->collection($product->productOptions, new ProductOptionTransformer());
    }

    public function includeImprints(Product $product)
    {
        return $this->collection($product->imprints, new ImprintTransformer());
    }

    public function includeProductExtraImages(Product $product)
    {
        return $this->collection($product->productExtraImages, new ProductExtraImageTransformer());
    }

    public function includeSizeGroup(Product $product)
    {
        if (!$product->sizeGroup) {
            return $this->null();
        }

        return $this->item($product->sizeGroup, new SizeGroupTransformer());
    }

    public function includeReviews(Product  $product)
    {
        return $this->collection($product->reviews, function($review){
            return [
                'email' => $review->reviewer_email,
                'company_name' => $review->company_name,
                'name' => $review->reviewer_name,
                'rating' => $review->rating,
                'text' => $review->text,
                'review_date' => $review->review_date,
                'created_at' => Carbon::parse($review->created_at)->format('Y-m-d')
            ];
        });
    }

    /**
     * Include Category Type for Product
     *
     * @param Product $product
     * @return \League\Fractal\Resource\Collection
     */
    public function includeBreadcrumbs(Product $product)
    {
        return $this->collection($product->categories, function($category) {
            /** @var Breadcrumb $breadcrumb */
            $breadcrumb = Breadcrumb::class;
            $breadcrumbs = $breadcrumb::render($category);

            return empty($category) ? '' : [
                'product_category_id' => $category->pivot->category_id,
                'type' => $category->pivot->type,
                'breadcrumbs' => $breadcrumbs
            ];
        });
    }

    /**
     * Get min product quantity
     *
     * @param Product $product
     * @return mixed
     */
    private function getMinProductQuantity(Product $product)
    {
        return $product->productPrices->min('quantity');
    }

    /**
     * Get lowest product price
     *
     * @param Product $product
     * @return mixed
     */
    private function getLowestProductPrice(Product $product)
    {
        $priceField = $product->on_sale ? 'sale_item_price' : 'item_price';
        return $product->productPrices->min($priceField);
    }

    /**
     * @param Product $product
     * @return array
     */
    private function getCustomShipping(Product $product)
    {
        if(is_null($product->custom_shipping_method) || $product->custom_shipping_cost == 0) {
            return [];
        }

        return [
          'cost' => $product->custom_shipping_cost,
          'method' => trans('shipping.method.' . $product->custom_shipping_method)
        ];
    }
}
