<?php


namespace App\Transformers;


use App\Models\Slider;
use League\Fractal\TransformerAbstract;

/**
 * Class SliderTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="Slider",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="url", type="string", example="http://google.com/"),
 *      @SWG\Property (property="image_src", type="string", example="http://localhost/storage/slider/02e63d31488b534028877c6393e6cf42.jpeg"),
 * ),
 */
class SliderTransformer extends TransformerAbstract
{
    public function transform(Slider $slider)
    {
        return [
            'id' => $slider->id,
            'url' => $slider->url,
            'image_src' => asset('storage/' . $slider->image_src),
        ];
    }
}
