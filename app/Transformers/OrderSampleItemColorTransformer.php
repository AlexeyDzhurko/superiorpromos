<?php

namespace App\Transformers;

use App\Models\OrderSampleItemColors;
use League\Fractal\TransformerAbstract;

/**
 * Class OrderSampleItemColorTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="OrderSampleItemColor",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="color_group_id", type="integer", example="1"),
 *      @SWG\Property (property="color_id", type="integer", example="3"),
 *      @SWG\Property (property="color_group_name", type="string", example="Items Colors"),
 *      @SWG\Property (property="color_name", type="string", example="Red"),
 * ),
 *
 */
class OrderSampleItemColorTransformer extends TransformerAbstract
{
    public function transform(OrderSampleItemColors $orderSampleItemColor)
    {
        return [
            'id' => $orderSampleItemColor->id,
            'color_group_id' => $orderSampleItemColor->color_group_id,
            'color_id' => $orderSampleItemColor->color_id,
            'color_group_name' => $orderSampleItemColor->colorGroup->name,
            'color_name' => $orderSampleItemColor->color->color,
        ];
    }
}
