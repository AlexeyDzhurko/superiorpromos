<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 20.04.17
 * Time: 14:12
 */

namespace App\Transformers;


use App\Models\VirtualCabinetFile;
use League\Fractal\TransformerAbstract;

/**
 * Class VirtualCabinetFileTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="VirtualCabinetFile",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="name", type="string", example="aeb0086a8b198c5998f.jpg"),
 *      @SWG\Property (property="created_at", type="integer", example="1494336788000"),
 * ),
 */
class VirtualCabinetFileTransformer extends TransformerAbstract
{
    public function transform(VirtualCabinetFile $file)
    {
        return [
            'id' => $file->id,
            'name' => $file->file_name,
            'created_at' => $file->created_at->timestamp * 1000,
        ];
    }

}
