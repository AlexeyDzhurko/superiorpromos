<?php

namespace App\Transformers;

use App\Payment\AdditionalOption;
use League\Fractal\TransformerAbstract;

/**
 * Class AdditionalOptionTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="AdditionalOption",
 *      @SWG\Property (property="name", type="integer", example="Option Title"),
 *      @SWG\Property (property="item_price", type="string", example="0.1"),
 *      @SWG\Property (property="setup_price", type="string", example="10"),
 * ),
 *
 */
class AdditionalOptionTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param AdditionalOption $additionalOption
     * @return array
     */
    public function transform(AdditionalOption $additionalOption)
    {
        return [
            'name' => $additionalOption->name,
            'item_price' => $additionalOption->itemPrice,
            'setup_price' => $additionalOption->setupPrice,
        ];
    }
}
