<?php

namespace App\Transformers;

use App\Models\UserNote;
use League\Fractal\TransformerAbstract;

class UserNoteTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(UserNote $userNote)
    {
        return [
            'id' => $userNote->id,
            'created_at' => $userNote->created_at->timestamp * 1000,
            'subject' => $userNote->subject,
            'note' => $userNote->note,
            'customer_note' => $userNote->customer_note,
            'approved_required' => $userNote->approved_required,
            'answered_at' => $userNote->answered_at ? $userNote->answered_at->timestamp * 1000 : null,
            'status' => is_null($userNote->approved) ? 'N/A' : ($userNote->approved == 1 ? 'approved' : 'declined'),
        ];
    }
}
