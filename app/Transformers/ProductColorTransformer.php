<?php


namespace App\Transformers;


use App\Models\ProductColor;
use League\Fractal\TransformerAbstract;

/**
 * Class ProductColorTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="ProductColorTransformer",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="image_src", type="string", example="http://example.com"),
 *      @SWG\Property (property="color", type="array", items=@SWG\Schema(ref="#/definitions/ColorTransformer")),
 *      @SWG\Property (property="product_color_prices", type="array", items=@SWG\Schema(ref="#/definitions/ProductColorPriceTransformer")),
 * ),
 */
class ProductColorTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'color',
        'product_color_prices',
    ];

    public function transform(ProductColor $productColor)
    {
        return [
            'id' => $productColor->id,
            'image_src' => $productColor->image_src,
        ];
    }

    public function includeColor(ProductColor $productColor)
    {
        return $this->item($productColor->color, new ColorsTransformer());
    }

    public function includeProductColorPrices(ProductColor $productColor)
    {
        return $this->collection($productColor->productColorPrices, new ProductColorPriceTransformer());
    }

}
