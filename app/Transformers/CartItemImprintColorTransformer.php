<?php

namespace App\Transformers;

use App\Models\CartItemImprintColor;
use League\Fractal\TransformerAbstract;

/**
 * Class CartItemImprintColorTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="CartItemImprintColor",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="color_id", type="integer", example="1"),
 *      @SWG\Property (property="color_name", type="string", example="Red"),
 * ),
 *
 */
class CartItemImprintColorTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param CartItemImprintColor $cartItemImprintColor
     * @return array
     */
    public function transform(CartItemImprintColor $cartItemImprintColor)
    {
        return [
            'id' => $cartItemImprintColor->id,
            'color_id' => $cartItemImprintColor->color_id,
            'name' => $cartItemImprintColor->name,
        ];
    }
}
