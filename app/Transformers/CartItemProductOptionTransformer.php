<?php


namespace App\Transformers;


use App\Models\CartItemProductOption;
use League\Fractal\TransformerAbstract;

/**
 * Class CartItemProductOptionTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="CartItemProductOption",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="product_option_id", type="integer", example="1"),
 *      @SWG\Property (property="name", type="string", example="Some Option"),
 *      @SWG\Property (property="setup_price", type="string", example="10"),
 *      @SWG\Property (property="item_price", type="string", example="0.1"),
 *      @SWG\Property (property="product_sub_options", type="array", items=@SWG\Schema(ref="#/definitions/CartItemProductSubOption")),
 * ),
 *
 */
class CartItemProductOptionTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'product_sub_options',
    ];

    public function transform(CartItemProductOption $cartItemProductOption)
    {
        return [
            'id' => $cartItemProductOption->id,
            'product_option_id' => $cartItemProductOption->product_option_id,
            'name' => $cartItemProductOption->name,
            'setup_price' => $cartItemProductOption->setup_price,
            'item_price' => $cartItemProductOption->item_price,
        ];
    }

    public function includeProductSubOptions(CartItemProductOption $cartItemProductOption)
    {
        return $this->collection($cartItemProductOption->cartItemProductSubOptions, new CartItemProductSubOptionTransformer());
    }
}
