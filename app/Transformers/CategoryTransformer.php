<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Category;
use App\Models\Product;

/**
 * Class CategoryTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="CategoryTransformer",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="title", type="string", example="Root category"),
 *      @SWG\Property (property="slug", type="string", example="root-category"),
 *      @SWG\Property (property="sub_categories", type="array", items=@SWG\Schema(ref="#/definitions/CategoryTransformer")),
 * ),
 */
class CategoryTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        // 'sub_categories',
    ];

    /**
     * A Fractal transformer.
     * @param Category $category
     * @return array
     */
    public function transform(Category $category)
    {
        return [
            'id' => $category->id,
            'title' => $category->name,
            'slug' =>  implode('/',  $category->getParentsAndSelf($category)->pluck('slug')->toArray()),
            'img_src' => $category->image_src ?
                'storage/' . Category::IMAGES_DIR . str_replace('storage/' . Category::IMAGES_DIR, '',
                    $category->image_src)
                : null,
            'meta_title'          => $category->meta_title,
            'meta_description'    => $category->meta_description,
            'keywords'            => $category->keywords,
            'sub_categories_exist' => $category->sub_categories_exist
        ];
    }

    /**
     * Include Products
     *
     * @param Category $category
     * @return \League\Fractal\Resource\Collection
     */
    public function includeSubCategories(Category $category)
    {
//        if (count(Product::categorized($category)) > 0) {
            return $this->collection(
                $category->children()
//                ->whereHas('products',  function($query)  {
//                    $query->where('products.active', 1);
//                })
                ->where('is_quick_link', 0)
                ->where('active', 1)
                ->get(), new CategoryTransformer());
//        }
    }
}
