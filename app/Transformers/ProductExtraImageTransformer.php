<?php


namespace App\Transformers;

use App\Admin\Services\ProductManager;
use App\Models\ProductExtraImage;
use League\Fractal\TransformerAbstract;

/**
 * Class ProductExtraImageTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="ProductExtraImage",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="original", type="string", example="http://localhost:8000/uploads/products/extra/3/original.jpg"),
 *      @SWG\Property (property="small", type="string", example="http://localhost:8000/uploads/products/extra/3/110_110.jpg"),
 *      @SWG\Property (property="medium", type="string", example="http://localhost:8000/uploads/products/extra/3/270_270.jpg"),
 * ),
 *
 */
class ProductExtraImageTransformer extends TransformerAbstract
{
    public function transform(ProductExtraImage $productExtraImage)
    {
        $path = $productExtraImage->image_src
            ? $productExtraImage->image_src
            : ProductManager::IMAGES_PATH . $productExtraImage->product_id . '/' . ProductManager::EXTRA_IMAGES_PATH . $productExtraImage->id;

        return [
            'id' => $productExtraImage->id,
            'original' => $path . '/original.jpg',
            'small' => $path . '/110_110.jpg',
            'medium' => $path . '/270_270.jpg',
        ];
    }

}
