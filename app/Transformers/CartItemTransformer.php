<?php

namespace App\Transformers;

use App\Models\CartItem;
use App\Payment\CartItemCost;
use League\Fractal\TransformerAbstract;

/**
 * Class CartItemTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="CartItem",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="quantity", type="integer", example="1000"),
 *      @SWG\Property (property="later_size_breakdown", type="string", example="1"),
 *      @SWG\Property (property="imprint_comment", type="string", example="Imprint comment"),
 *      @SWG\Property (property="tax_exemption", type="string", example="0"),
 *      @SWG\Property (property="estimation_zip", type="string", example="11211"),
 *      @SWG\Property (property="estimation_shipping_method", type="string", example="ups"),
 *      @SWG\Property (property="estimation_shipping_code", type="string", example="GND"),
 *      @SWG\Property (property="shipping_method", type="string", example="outside_us"),
 *      @SWG\Property (property="own_shipping_type", type="string", example="Ground"),
 *      @SWG\Property (property="own_account_number", type="string", example="1123123123"),
 *      @SWG\Property (property="own_shipping_system", type="string", example="ups"),
 *      @SWG\Property (property="received_date", type="string", example="2017-09-14"),
 *      @SWG\Property (property="product", ref="#/definitions/Product"),
 *      @SWG\Property (property="colors", type="array", items=@SWG\Schema(ref="#/definitions/CartItemColor")),
 *      @SWG\Property (property="product_options", type="array", items=@SWG\Schema(ref="#/definitions/CartItemProductOption")),
 *      @SWG\Property (property="imprints", type="array", items=@SWG\Schema(ref="#/definitions/CartItemImprint")),
 *      @SWG\Property (property="cart_item_cost", ref="#/definitions/CartItemCost"),
 *      @SWG\Property (property="cart_item_size_group", type="array", items=@SWG\Schema(ref="#/definitions/CartItemSizeGroup")),
 * ),
 */
class CartItemTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'product',
        'colors',
        'product_options',
        'imprints',
        'cart_item_cost',
        'cart_item_size_group',
        'cart_item_art_proof'
    ];

    /**
     * A Fractal transformer.
     *
     * @param CartItem $cartItem
     * @return array
     */
    public function transform(CartItem $cartItem)
    {
        return [
            'id' => $cartItem->id,
            'quantity' => $cartItem->quantity,
            'later_size_breakdown' => $cartItem->later_size_breakdown,
            'imprint_comment' => $cartItem->imprint_comment,
            'tax_exemption' => $cartItem->tax_exemption,
            'estimation_zip' => $cartItem->estimation_zip,
            'estimation_shipping_method' => $cartItem->estimation_shipping_method,
            'estimation_shipping_code' => $cartItem->estimation_shipping_code,
            'shipping_method' => $cartItem->shipping_method,
            'own_shipping_type' => $cartItem->own_shipping_type,
            'own_account_number' => $cartItem->own_account_number,
            'own_shipping_system' => $cartItem->own_shipping_system,
            'received_date' => $cartItem->received_date,
            'art_file_name' => $cartItem->art_file_name,
            'art_file_path' => $cartItem->art_file_path,
            'cart_id' => !empty($cartItem->cart->id) ? $cartItem->cart->id : null
        ];
    }

    public function includeProduct(CartItem $cartItem)
    {
        $product = fractal($cartItem->product, new ProductTransformer())->toArray();

        return $this->item($product, new ProductBreadcrumbsTransformer([]));
    }

    public function includeImprints(CartItem $cartItem)
    {
        return $this->collection($cartItem->cartItemImprints, new CartItemImprintTransformer());
    }

    public function includeProductOptions(CartItem $cartItem)
    {
        return $this->collection($cartItem->cartItemProductOptions, new CartItemProductOptionTransformer());
    }

    public function includeColors(CartItem $cartItem)
    {
        return $this->collection($cartItem->cartItemColors, new CartItemColorTransformer());
    }

    public function includeCartItemCost(CartItem $cartItem)
    {
        return $this->item(new CartItemCost($cartItem), new CartItemCostTransformer());
    }

    public function includeCartItemSizeGroup(CartItem $cartItem)
    {
        if (!$cartItem->cartItemSizeGroup) {
            return $this->null();
        }

        return $this->item($cartItem->cartItemSizeGroup, new CartItemSizeGroupTransformer());
    }

    public function includeCartItemArtProof(CartItem $cartItem)
    {
        if($cartItem->cartItemArtProof->isEmpty()) {
            return $this->null();
        }

        return $this->collection($cartItem->cartItemArtProof, new CartItemArtProofTransformer());
    }
}
