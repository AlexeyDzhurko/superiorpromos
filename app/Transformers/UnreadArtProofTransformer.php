<?php

namespace App\Transformers;

use App\Models\ArtProof;
use League\Fractal\TransformerAbstract;

/**
 * Class ArtProofTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="UnreadArtProof",
 *      @SWG\Property(property="id", type="integer", example="1"),
 *      @SWG\Property(property="created_at", type="string", example="1494336788000"),
 *      @SWG\Property(property="path", type="string", example="art_proof/2a8622af96dc2bd5f99cc3bbcec0309b.jpeg"),
 *      @SWG\Property(property="note", type="string", example="<p>Test test</p>"),
 *      @SWG\Property(property="customer_note", type="string", example="my note about this art proof"),
 *      @SWG\Property(property="answered_at", type="string", example="1504090422000"),
 *      @SWG\Property(property="status", type="string", example="declined"),
 *      @SWG\Property(property="order_id", type="integer", example=1345),
 *      @SWG\Property(property="order_item_id", type="integer", example=32541)
 * ),
 */
class UnreadArtProofTransformer extends TransformerAbstract
{
    public function transform(ArtProof $artProof)
    {
        return [
            'id' => $artProof->id,
            'created_at' => $artProof->created_at->timestamp * 1000,
            'path' =>  $artProof->path,
            'note' =>  $artProof->note,
            'customer_note' =>  $artProof->customer_note,
            'answered_at' => $artProof->answered_at ? $artProof->answered_at->timestamp * 1000 : null,
            'status' => is_null($artProof->approved) ? 'N/A' : ($artProof->approved ? 'approved' : 'declined'),
            'order_id' => $artProof->orderItem->order->id,
            'order_item_id' => $artProof->orderItem->id,
            'product_name' => $artProof->orderItem->product->name,
        ];
    }
}
