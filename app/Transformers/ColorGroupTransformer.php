<?php

namespace App\Transformers;

use App\Models\ColorGroup;
use League\Fractal\TransformerAbstract;

/**
 * Class ColorGroupTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="ColorGroup",
 *      @SWG\Property (property="name", type="string", example="Root category"),
 *      @SWG\Property (property="colors", type="array", items=@SWG\Schema(ref="#/definitions/ColorTransformer")),
 * ),
 */
class ColorGroupTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'colors',
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ColorGroup $colorGroup)
    {
        return [
            'id' => $colorGroup->id,
            'name' => $colorGroup->name,
        ];
    }

    /**
     * Include Colors
     *
     * @param ColorGroup $colorGroup
     * @return \League\Fractal\Resource\Collection
     */
    public function includeColors(ColorGroup $colorGroup)
    {
        return $this->collection($colorGroup->colors, new ColorsTransformer());
    }
}
