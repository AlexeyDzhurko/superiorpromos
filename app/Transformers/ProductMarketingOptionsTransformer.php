<?php

namespace App\Transformers;

use App\Models\Color;
use App\Models\ProductPrice;
use League\Fractal\TransformerAbstract;
use app\Models\Product;

/**
 * Class ProductMarketingOptionsTransformer
 * @package App\Transformers
 */
class ProductMarketingOptionsTransformer extends TransformerAbstract
{
    public function transform(array $product)
    {
        $productModel = Product::find($product['id']);

        $product['description'] = $productModel->description;
        $product['vendor'] = !empty($productModel->vendors->first()) ? $productModel->vendors->first()->name : '';
        $product['colors'] = $this->getColors($productModel);
        $product['type'] = $this->getProductType($product);
        $product['box_weight'] = $productModel->box_weight;
        $product['product_price'] = $this->getProductPrice($productModel);

        return $product;
    }

    private function getColors(Product $product){
        $colorIds = [];

        foreach ($product->productColorGroups as $colorGroup) {
            $colorIds = array_merge($colorIds, $colorGroup->productColors->pluck('color_id')->toArray());
        }

        $colors = Color::whereIn('id', $colorIds)->get();

        return implode(', ', $colors->pluck('name')->toArray());
    }
    private function getProductType($product){
        if(!empty($product['breadcrumbs']['breadcrumbs'])){
            array_pop($product['breadcrumbs']['breadcrumbs']);


            return implode(' > ',
                collect($product['breadcrumbs']['breadcrumbs'])->pluck('name')->toArray()
            );
        }
    }
    private function getProductPrice(Product $product){
        /** @var ProductPrice $productPrice */
        $productPrice = $product->productPrices->sortBy('quantity', SORT_REGULAR)->first();

        if (!empty($productPrice)) {
            $itemPrice = $productPrice->sale_item_price > 0 ? $productPrice->sale_item_price : $productPrice->item_price;
            $price = number_format(($productPrice->quantity * $itemPrice) + $productPrice->setup_price, 2) . ' USD';
        } else {
            $price = '';
        }

        return $price;
    }
}
