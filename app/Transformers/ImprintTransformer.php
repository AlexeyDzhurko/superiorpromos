<?php


namespace App\Transformers;


use App\Models\ColorGroup;
use App\Models\Imprint;
use App\Models\ImprintColor;
use League\Fractal\TransformerAbstract;

/**
 * Class ImprintTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="Imprint",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="position", type="integer", example="1"),
 *      @SWG\Property (property="name", type="string", example="Imprint Title"),
 *      @SWG\Property (property="max_colors", type="integer", example="4"),
 *      @SWG\Property (property="color_group_id", type="integer", example="1"),
 *      @SWG\Property (property="imprint_prices", type="array", items=@SWG\Schema(ref="#/definitions/ImprintPrice")),
 *      @SWG\Property (property="color_group", ref="#/definitions/ColorGroup"),
 * ),
 *
 */
class ImprintTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'imprint_prices',
        'color_group',
        'colors'
    ];

    public function transform(Imprint $imprint)
    {
        return [
            'id' => $imprint->id,
            'position' => $imprint->position,
            'name' => $imprint->name,
            'max_colors' => $imprint->max_colors,
            'color_group_id' => $imprint->color_group_id,
        ];
    }

    public function includeImprintPrices(Imprint $imprint)
    {
        return $this->collection($imprint->imprintPrices, new ImprintPriceTransformer());
    }

    public function includeColorGroup(Imprint $imprint)
    {
        if (is_null($imprint->colorGroup)) {
            $imprint->colorGroup = new ColorGroup();
        }

        if(!is_null($imprint->imprintColors)) {
            $imprint->imprintColors->each(function ($imprintColor) use ($imprint){
                if(is_null($imprint->colorGroup->colors->find($imprintColor->color->id))) {
                    $imprint->colorGroup->colors->push($imprintColor->color);
                }
            });
        }

        return $this->item($imprint->colorGroup, new ColorGroupTransformer());
    }

    public function includeColors(Imprint $imprint)
    {
        return $this->collection($imprint->imprintColors, new ImprintColorTransformer());
    }

}
