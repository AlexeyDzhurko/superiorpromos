<?php


namespace App\Transformers;


use App\Models\ProductIcon;
use League\Fractal\TransformerAbstract;

class ProductIconTransformer extends TransformerAbstract
{
    public function transform(ProductIcon $productIcon)
    {
        return [
            'id' => $productIcon->id,
            'name' => $productIcon->name,
            'text' => $productIcon->text,
            'image_src' => asset('storage/' . $productIcon->icon_pic_src),
            'position' => $productIcon->position,
        ];
    }

}