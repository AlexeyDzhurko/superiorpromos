<?php

namespace App\Transformers;

use App\Models\Product;
use League\Fractal\TransformerAbstract;

/**
 *
 * @SWG\Definition (
 *      definition="ShortProduct",
 *      @SWG\Property(property="id", type="integer", example="1"),
 *      @SWG\Property(property="thumbnail", type="string", example="CS.jpg"),
 *      @SWG\Property(property="title", type="string", example="Bic Clic Stic Revo Pen"),
 * ),
 *
 * Class ShortProductTransformer
 * @package App\Transformers
 */
class ShortProductTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Product $product)
    {
        return [
            "id" => $product->id,
            "thumbnail" => $product->image_src,
            "title" => strip_tags($product->name),
        ];
    }
}
