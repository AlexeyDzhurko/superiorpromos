<?php


namespace App\Transformers;


use App\Models\ProductOption;
use League\Fractal\TransformerAbstract;

/**
 * Class ProductOptionTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="ProductOption",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="name", type="string", example="Product Option"),
 *      @SWG\Property (property="show_as", type="string", example="checkbox"),
 *      @SWG\Property (property="required", type="boolean", example=1),
 *      @SWG\Property (property="product_option_prices", type="array", items=@SWG\Schema(ref="#/definitions/ProductOptionPrice")),
 *      @SWG\Property (property="product_sub_options", type="array", items=@SWG\Schema(ref="#/definitions/ProductSubOption")),
 * ),
 *
 */
class ProductOptionTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'product_option_prices',
        'product_sub_options',
    ];

    public function transform(ProductOption $productOption)
    {
        return [
            'id' => $productOption->id,
            'name' => $productOption->name,
            'show_as' => $productOption->show_as,
            'required' => $productOption->required
        ];
    }

    public function includeProductOptionPrices(ProductOption $productOption)
    {
        return $this->collection($productOption->productOptionPrices, new ProductOptionPriceTransformer());
    }

    public function includeProductSubOptions(ProductOption $productOption)
    {
        return $this->collection($productOption->productSubOptions, new ProductSubOptionTransformer());
    }

}
