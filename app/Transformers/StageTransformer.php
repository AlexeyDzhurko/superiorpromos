<?php

namespace App\Transformers;

use App\Models\Stage;
use League\Fractal\TransformerAbstract;

/**
 * @SWG\Definition (
 *      definition="Stage",
 *      @SWG\Property(property="id", type="integer", example="1"),
 *      @SWG\Property(property="name", type="string", example="Complete"),
 * ),
 *
 * Class StageTransformer
 * @package App\Transformers
 */
class StageTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Stage $stage
     * @return array
     */
    public function transform(Stage $stage)
    {
        return [
            'name' => $stage->name ?? 'Without Stage',
            'id' => $stage->id ?? 0,
        ];
    }
}
