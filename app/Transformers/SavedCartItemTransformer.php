<?php

namespace App\Transformers;

use App\Models\SavedCartItem;
use League\Fractal\TransformerAbstract;

/**
 * Class SavedCartItemTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="SavedCartItem",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="cart_item", ref="#/definitions/CartItem"),
 * ),
 */
class SavedCartItemTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'cart_item',
    ];

    /**
     * A Fractal transformer.
     *
     * @param SavedCartItem $savedCartItem
     * @return array
     */
    public function transform(SavedCartItem $savedCartItem)
    {
        return [
            'id' => $savedCartItem->id,
        ];
    }

    public function includeCartItem(SavedCartItem $savedCartItem)
    {
        return $this->item($savedCartItem->cartItem, new CartItemTransformer());
    }
}
