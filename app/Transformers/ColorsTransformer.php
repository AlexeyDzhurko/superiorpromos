<?php

namespace App\Transformers;

use App\Models\Color;
use League\Fractal\TransformerAbstract;

/**
 * Class ColorsTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="ColorTransformer",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="title", type="string", example="Blue"),
 *      @SWG\Property (property="color_hex", type="string", example="#222222"),
 *      @SWG\Property (property="image", type="string", example="http://example.com"),
 * ),
 */
class ColorsTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Color $color)
    {
        return [
            "id" => $color->id,
            "title" => $color->name,
            "color_hex" => $color->color_hex,
            //"image" => $color->picture_src
        ];
    }
}
