<?php

namespace App\Transformers;

use App\Models\CartItemImprint;
use League\Fractal\TransformerAbstract;

/**
 * Class CartItemImprintTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="CartItemImprint",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="name", type="string", example="Main Imprints"),
 *      @SWG\Property (property="imprint_colors", type="array", items=@SWG\Schema(ref="#/definitions/CartItemImprintColor")),
 * ),
 *
 */
class CartItemImprintTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'imprint_colors',
    ];

    /**
     * A Fractal transformer.
     *
     * @param CartItemImprint $cartItemImprint
     * @return array
     */
    public function transform(CartItemImprint $cartItemImprint)
    {
        return [
            'id' => $cartItemImprint->id,
            'name' => $cartItemImprint->imprint_name,
            'product_imprint_id' => $cartItemImprint->imprint_id
        ];
    }

    public function includeImprintColors(CartItemImprint $cartItemImprint)
    {
        return $this->collection($cartItemImprint->cartItemImprintColors, new CartItemImprintColorTransformer());
    }
}
