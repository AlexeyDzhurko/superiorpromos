<?php


namespace App\Transformers;


use App\Models\Block;
use League\Fractal\TransformerAbstract;

/**
 * Class BlockTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="Block",
 *      @SWG\Property (property="footer_text", type="string", example="Example text"),
 *      @SWG\Property (property="content", type="string", example="<div>text</div>"),
 * ),
 */
class BlockTransformer extends TransformerAbstract
{
    public function transform(Block $block)
    {
        return [
            'key' => $block->key,
            'content' => $block->content,
        ];
    }
}
