<?php

namespace App\Transformers;

use App\Payment\OrderItemCost;
use League\Fractal\TransformerAbstract;

/**
 *
 * @SWG\Definition (
 *      definition="OrderItemCost",
 *      @SWG\Property (property="quantity", type="integer", example="1000"),
 *      @SWG\Property (property="base_item_price", type="string", example="2.6"),
 *      @SWG\Property (property="final_item_price", type="string", example="2.8"),
 *      @SWG\Property (property="shipping", type="string", example="100.3"),
 *      @SWG\Property (property="total", type="string", example="5482.2"),
 *      @SWG\Property (property="product_options", type="array", items=@SWG\Schema(ref="#/definitions/AdditionalOption")),
 *      @SWG\Property (property="imprints", type="array", items=@SWG\Schema(ref="#/definitions/AdditionalOption")),
 * ),
 *
 * Class OrderItemCostTransformer
 * @package App\Transformers
 */
class OrderItemCostTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'product_options',
        'imprints',
    ];

    /**
     * A Fractal transformer.
     *
     * @param OrderItemCost $orderItemCost
     * @return array
     */
    public function transform(OrderItemCost $orderItemCost)
    {
        return [
            'quantity' => $orderItemCost->getQuantity(),
            'base_item_price' => $orderItemCost->getBaseItemPrice(),
            'final_item_price' => $orderItemCost->getFinalItemPrice(),
            'shipping' => $orderItemCost->getShipping(),
            'total' => $orderItemCost->getTotal(),
        ];
    }

    public function includeProductOptions(OrderItemCost $orderItemCost)
    {
        return $this->collection($orderItemCost->getProductOptions(), new AdditionalOptionTransformer());
    }

    public function includeImprints(OrderItemCost $orderItemCost)
    {
        return $this->collection($orderItemCost->getImprints(), new AdditionalOptionTransformer());
    }
}

