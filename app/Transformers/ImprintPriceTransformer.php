<?php


namespace App\Transformers;


use App\Models\ImprintPrice;
use League\Fractal\TransformerAbstract;

/**
 * Class ImprintPriceTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="ImprintPrice",
 *      @SWG\Property (property="id", type="integer", example="1"),
 *      @SWG\Property (property="quantity", type="integer", example="1000"),
 *      @SWG\Property (property="setup_price", type="string", example="10"),
 *      @SWG\Property (property="item_price", type="string", example="0.5"),
 *      @SWG\Property (property="color_setup_price", type="string", example="5"),
 *      @SWG\Property (property="color_item_price", type="string", example="0.2"),
 * ),
 *
 */
class ImprintPriceTransformer extends TransformerAbstract
{
    public function transform(ImprintPrice $imprintPrice)
    {
        return [
            'id' => $imprintPrice->id,
            'quantity' => $imprintPrice->quantity,
            'setup_price' => $imprintPrice->setup_price,
            'item_price' => $imprintPrice->item_price,
            'color_setup_price' => $imprintPrice->color_setup_price,
            'color_item_price' => $imprintPrice->color_item_price,
        ];
    }
}
