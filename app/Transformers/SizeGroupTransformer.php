<?php


namespace App\Transformers;


use App\Models\SizeGroup;
use League\Fractal\TransformerAbstract;

/**
 *
 * @SWG\Definition (
 *      definition="SizeGroup",
 *      @SWG\Property(property="id", type="integer", example="1"),
 *      @SWG\Property(property="name", type="string", example="Main"),
 *      @SWG\Property (property="sizes", type="array", items=@SWG\Schema(ref="#/definitions/Size")),
 * ),
 *
 * Class SizeGroupTransformer
 * @package App\Transformers
 */
class SizeGroupTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'sizes'
    ];

    public function transform(SizeGroup $sizeGroup)
    {
        return [
            'id' => $sizeGroup->id,
            'name' => $sizeGroup->name,
        ];
    }

    public function includeSizes(SizeGroup $sizeGroup)
    {
        return $this->collection($sizeGroup->sizes, new SizeTransformer());
    }
}
