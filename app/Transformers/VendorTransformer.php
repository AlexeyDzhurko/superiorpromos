<?php


namespace App\Transformers;


use App\Models\Vendor;
use League\Fractal\TransformerAbstract;

class VendorTransformer extends TransformerAbstract
{
    public function transform(Vendor $vendor)
    {
        return [
            'id' => $vendor->id,
            'name' => $vendor->name,
            'phone' => $vendor->phone,
            'fax' => $vendor->fax,
            'email' => $vendor->email,
            'state_id' => $vendor->state_id,
            'zip_code' => $vendor->zip_code,
            'address_1' => $vendor->address_1,
            'address_2' => $vendor->address_2,
            'supplier_id' => $vendor->supplier_id,
            'site' => $vendor->site,
            'active' => $vendor->active,
            'city' => $vendor->city,
            'country_id' => $vendor->country_id,
            'state_ids' => $vendor->states->pluck('id'),
        ];
    }

}