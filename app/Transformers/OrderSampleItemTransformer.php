<?php

namespace App\Transformers;

use App\Models\OrderSampleItem;
use App\Models\Stage;
use League\Fractal\TransformerAbstract;

/**
 * Class OrderSampleItemTransformer
 * @package App\Transformers
 */
class OrderSampleItemTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'product',
        'colors',
        'stage',
    ];

    /**
     * A Fractal transformer.
     *
     * @param OrderSampleItem $orderSampleItem
     * @return array
     */
    public function transform(OrderSampleItem $orderSampleItem)
    {
        return [
            'id' => $orderSampleItem->id,
            'price' => $orderSampleItem->price,
            'check_notes' => $orderSampleItem->check_notes,

        ];
    }

    public function includeProduct(OrderSampleItem $orderSampleItem)
    {
        return $this->item($orderSampleItem->product, new ShortProductTransformer());
    }

    public function includeColors(OrderSampleItem $orderSampleItem)
    {
        return $this->collection($orderSampleItem->orderSampleItemColor, new OrderSampleItemColorTransformer());
    }

    public function includeStage(OrderSampleItem $orderSampleItem)
    {
        return $this->item($orderSampleItem->stage ?? new Stage(), new StageTransformer());
    }
}
