<?php


namespace App\Transformers;


use App\Models\ProductColorGroup;
use League\Fractal\TransformerAbstract;

/**
 * Class ProductColorGroupTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="ProductColorGroup",
 *      @SWG\Property (property="id", type="integer", example="13"),
 *      @SWG\Property (property="name", type="string", example="Root category"),
 *      @SWG\Property (property="product_colors", type="array", items=@SWG\Schema(ref="#/definitions/ProductColorTransformer")),
 * ),
 */
class ProductColorGroupTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'product_colors',
    ];

    public function transform(ProductColorGroup $productColorGroup)
    {
        return [
            'id' => $productColorGroup->id,
            'name' => $productColorGroup->name,
        ];
    }

    public function includeProductColors(ProductColorGroup $productColorGroup)
    {
        return $this->collection($productColorGroup->productColors, new ProductColorTransformer());
    }
}
