<?php


namespace App\Transformers;


use App\Models\SurveyAnswer;
use App\Models\SurveyQuestion;
use League\Fractal\TransformerAbstract;

class SurveyQuestionTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'survey_answers',
    ];

    public function transform(SurveyQuestion $surveyQuestion)
    {
        return [
            'id' => $surveyQuestion->id,
            'name' => $surveyQuestion->name,
            'type' => $surveyQuestion->type,
        ];
    }

    public function includeSurveyAnswers(SurveyQuestion $surveyQuestion)
    {
        return $this->collection($surveyQuestion->surveyAnswers, function(SurveyAnswer $surveyAnswer) {
            return [
                'id' => $surveyAnswer->id,
                'name' => $surveyAnswer->name,
            ];
        });
    }
}
