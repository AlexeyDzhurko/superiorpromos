<?php

namespace App\Transformers;

use App\Models\PromotionalBlock;
use League\Fractal\TransformerAbstract;

/**
 * Class PromotionalBlocksTransformer
 * @package App\Transformers
 */
class PromotionalBlocksTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'products',
    ];

    public function transform(PromotionalBlock $promotionalBlock)
    {
        return [
            "title" => $promotionalBlock->title,
            "slug" => $promotionalBlock->slug,
            "category" => $promotionalBlock->category ? $promotionalBlock->category->id : null,
        ];
    }

    /**
     * Include Products
     *
     * @param PromotionalBlock $promotionalBlock
     * @return \League\Fractal\Resource\Collection
     */
    public function includeProducts(PromotionalBlock $promotionalBlock)
    {
        $product = fractal($promotionalBlock->products, new ProductTransformer())->toArray();

        return $this->collection($product, new ProductBreadcrumbsTransformer([
            'categories' => [$promotionalBlock->category ? $promotionalBlock->category->id : null]
        ]));
    }
}
