<?php


namespace App\Discounts;


class AnyOfDiscountConditions extends DiscountCondition
{
    protected $discountConditions;

    public function __construct(DiscountCondition ...$discountConditions)
    {
        $this->discountConditions = $discountConditions;
    }

    public function isAccepted(): bool
    {
        foreach($this->discountConditions as $discountCondition) {
            if ($discountCondition->isAccepted()) {
                return true;
            }
        }
        return false;
    }

    public static function getDescription(): string
    {
        return 'Valid if at least one rule will be accepted';
    }

    public static function getTitle(): string
    {
        return 'Any of';
    }
}
