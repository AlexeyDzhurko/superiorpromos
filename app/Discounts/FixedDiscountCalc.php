<?php


namespace App\Discounts;


use App\Models\CartItem;
use App\Models\OrderItem;
use App\Payment\CartItemCost;
use App\Payment\OrderItemCost;

class FixedDiscountCalc extends DiscountCalc
{
    protected $amount;

    public function __construct(int $amount)
    {
        $this->amount = $amount;
    }

    public function getDiscountAmount(): float
    {
        $cost = 0;
        foreach ($this->cart->cartItems as $cartItem) { /** @var CartItem $cartItem */
            $cost += (new CartItemCost($cartItem))->getTotal();
        }

        $this->totalDiscountAmount = min([$cost, $this->amount]);

        return $this->totalDiscountAmount;
    }

    static public function getTitle(): string
    {
        return 'Fixed discount';
    }
}
