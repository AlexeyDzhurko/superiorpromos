<?php


namespace App\Discounts;


use App\Models\Cart;
use App\Models\Order;

abstract class DiscountCondition
{
    /**
     * @var Order
     */
    protected $cart;

    abstract public function isAccepted(): bool;
    abstract static public function getDescription(): string;
    abstract static public function getTitle(): string ;

    public function setCart(Cart $cart)
    {
        $this->cart = $cart;
    }

    public static function getParametrizedTitle(): string
    {
        return static::getTitle();
    }
}
