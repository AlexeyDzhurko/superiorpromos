<?php


namespace App\Discounts;


use App\Models\CartItem;
use App\Models\Category;
use App\Payment\CartItemCost;

class ProductFromCategoryShippingDiscountCalc extends DiscountCalc
{
    protected $percent;
    protected $category;

    public function __construct(Category $category, int $percent)
    {
        $this->percent = $percent;
        $this->category = $category;
    }

    public function getDiscountAmount(): float
    {
        $cost = 0;
        foreach ($this->cart->cartItems as $cartItem) { /** @var CartItem $cartItem */
            if($cartItem->product->categories->where('id', '=', $this->category->id)->isEmpty()) {
                continue;
            }
            $cost += (new CartItemCost($cartItem))->getShipping();
        }

        $this->totalDiscountAmount = $this->round($this->percent / 100 * $cost);

        return  $this->totalDiscountAmount;
    }

    static public function getTitle(): string
    {
        return 'Multiply shipping price of product from category';
    }
}
