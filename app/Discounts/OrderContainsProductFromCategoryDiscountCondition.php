<?php


namespace App\Discounts;


use App\Models\Category;

class OrderContainsProductFromCategoryDiscountCondition extends DiscountCondition
{
    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public static function getCategoryParameterLabel()
    {
        return 'The cart contains a product from category';
    }

    public function isAccepted(): bool
    {
        return $this->category
            ->products()
            ->whereIn('products.id', $this->cart->cartItems->pluck('product_id'))
            ->exists();
    }

    public static function getDescription(): string
    {
        return 'Valid if order contains product from category';
    }

    public static function getTitle(): string
    {
        return 'Product from category';
    }
}
