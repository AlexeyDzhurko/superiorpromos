<?php


namespace App\Discounts;


use App\User;

class FirstOrderForUserDiscountCondition extends DiscountCondition
{
    public function isAccepted(): bool
    {
        /** @var User $user */
        $user = $this->cart->user;

        return count($user->orders) < 1;
    }

    public static function getDescription(): string
    {
        return 'Valid if it is first user\'s order';
    }

    public static function getTitle(): string
    {
        return 'First order';
    }
}
