<?php


namespace App\Discounts;


use App\Models\OrderItem;
use App\Models\Product;

class OrderContainsProductDiscountCondition extends DiscountCondition
{
    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function isAccepted(): bool
    {
        foreach ($this->cart->cartItems as $cartItem) { /** @var $cartItem */
            if ($cartItem->product->id == $this->product->id) {
                return true;
            }
        }

        return false;
    }

    public static function getDescription(): string
    {
        return 'Valid if order contains specific product';
    }

    public static function getTitle(): string
    {
        return 'Contains product';
    }
}
