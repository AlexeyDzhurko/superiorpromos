<?php


namespace App\Discounts;


use App\Models\CartItem;
use App\Payment\CartItemCost;

class TotalDiscountCost extends DiscountCalc
{
    protected $percent;

    public function __construct(float $percent)
    {
        $this->percent = $percent;
    }

    public function getDiscountAmount(): float
    {
        $cost = 0;
        foreach ($this->cart->cartItems as $cartItem) { /** @var CartItem $cartItem */
            $cost += (new CartItemCost($cartItem))->getTotal();
        }

        $this->totalDiscountAmount = $this->round($this->percent / 100 * $cost);

        return $this->totalDiscountAmount;
    }

    static public function getTitle(): string
    {
        return 'Multiply order total';
    }
}
