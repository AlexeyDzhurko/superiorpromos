<?php


namespace App\Discounts;


use App\Exceptions\DiscountException;
use App\Models\Cart;
use Illuminate\Database\Eloquent\Model;
use ReflectionClass;
use ReflectionParameter;

class DiscountCreator
{
    public function getDiscountConditionInfo(ReflectionClass $class): array
    {
        $info = [];
        $info['class'] = $class->getName();
        $info['title'] = $class->getMethod('getTitle')->invoke(null);
        $info['parametrized_title'] = $class->getMethod('getParametrizedTitle')->invoke(null);
        $info['allow_nested'] = false;
        $info['parameters'] = [];

        if ($constructor = $class->getConstructor()) {
            foreach ($constructor->getParameters() as $parameter) {

                $constructorParameter = [];
                $constructorParameter['type'] = 'text';
                $constructorParameter['name'] = $parameter->getName();
                $constructorParameter['label'] = $this->getParameterLabel($class, $parameter);

                if ($this->isCustomClass($parameter)) {
                    $parameterReflectionClass = $this->createParameterReflectionClass($parameter);

                    if ($parameterReflectionClass->isSubclassOf(Model::class)){
                        $constructorParameter['type'] = 'model';
                        $constructorParameter['url'] = route('admin.api.product.index');

                    } elseif ($parameter->getType()->__toString() == DiscountCondition::class) {
                        $info['allow_nested'] = true;
                        continue;
                    }
                }
                $info['parameters'][] = $constructorParameter;
            }
        }

        return $info;
    }

    public function getDiscountCalcInfo(ReflectionClass $class): array
    {
        $info = [];
        $info['class'] = $class->getName();
        $info['title'] = $class->getMethod('getTitle')->invoke(null);
        $info['parametrized_title'] = $class->getMethod('getParametrizedTitle')->invoke(null);
        $info['parameters'] = [];

        if ($constructor = $class->getConstructor()) {
            foreach ($constructor->getParameters() as $parameter) {

                $constructorParameter = [];
                $constructorParameter['type'] = 'text';
                $constructorParameter['name'] = $parameter->getName();
                $constructorParameter['label'] = $this->getParameterLabel($class, $parameter);

                if ($this->isCustomClass($parameter)) {
                    $parameterReflectionClass = $this->createParameterReflectionClass($parameter);

                    if ($parameterReflectionClass->isSubclassOf(Model::class)){
                        $constructorParameter['type'] = 'model';
                        $constructorParameter['url'] = route('admin.api.product.index');

                    }
                }
                $info['parameters'][] = $constructorParameter;
            }
        }

        return $info;
    }

    protected function isCustomClass(ReflectionParameter $reflectionParameter)
    {
        return $reflectionParameter->hasType() && $reflectionParameter->getType()->isBuiltin() == false;
    }

    protected function getParameterLabel(ReflectionClass $class, ReflectionParameter $reflectionParameter)
    {
        $labelMethodName = 'get'.ucfirst($reflectionParameter->name).'ParameterLabel';

        if ($class->hasMethod($labelMethodName)) {
            return $class->getMethod($labelMethodName)->invoke(null);
        }

        return ucfirst($reflectionParameter->name);
    }

    protected function createParameterReflectionClass(ReflectionParameter $reflectionParameter)
    {
        $reflectionParameterClassName = $reflectionParameter->getType()->__toString();
        return new \ReflectionClass($reflectionParameterClassName);
    }

    /**
     * @param string $jsonData
     * @param Cart $cart
     * @return array
     * @throws DiscountException
     */
    public function createCalcFromJson(string $jsonData, Cart $cart): array
    {
        $data = json_decode($jsonData, true);
        $discountCalculations = [];
        foreach ($data as $item) {
            $class = $this->getCalcClassOrThrowException($item);

            $constructorParameters = [];
            if ($class->getConstructor()) {
                foreach ($class->getConstructor()->getParameters() as $reflectionParameter) {
                    $parameterValue = $item['parameters'][$reflectionParameter->name] ?? null;

                    if (empty($parameterValue)) {
                        throw new DiscountException('Invalid discount params');
                    }

                    if ($this->isCustomClass($reflectionParameter)) {

                        $parameterReflectionClass = $this->createParameterReflectionClass($reflectionParameter);

                        if ($reflectionParameter->isVariadic()) {
                            foreach ($parameterValue as $parameterItem) {
                                $constructorParameters[] = $this->getCalcTypedParameter($parameterReflectionClass, $parameterItem);
                            };
                        } else {
                            $constructorParameters[] = $this->getCalcTypedParameter($parameterReflectionClass, $parameterValue);
                        }

                    } else {
                        $constructorParameters[] = $parameterValue;
                    }
                }
            }

            /** @var DiscountCalc $discountCalc */
            $discountCalc = $class->newInstanceArgs($constructorParameters);
            $discountCalc->setCart($cart);
            $discountCalculations[] = $discountCalc;
        }

        return $discountCalculations;
    }

    /**
     * @param string $jsonData
     * @param Cart $cart
     * @return DiscountCondition
     * @throws DiscountException
     * @throws \ReflectionException
     */
    public function createFromJson(string $jsonData, Cart $cart): DiscountCondition
    {
        $data = json_decode($jsonData, true);

        $class = $this->getDiscountClassOrThrowException($data);

        $constructorParameters = [];
        if ($class->getConstructor()) {
            foreach ($class->getConstructor()->getParameters() as $reflectionParameter) {
                $parameterValue = $data['parameters'][$reflectionParameter->name] ?? null;

                if (empty($parameterValue)) {
                    throw new DiscountException('Invalid discount params');
                }

                if ($this->isCustomClass($reflectionParameter)) {

                    $parameterReflectionClass = $this->createParameterReflectionClass($reflectionParameter);

                    if ($reflectionParameter->isVariadic()) {
                        foreach ($parameterValue as $parameterItem) {
                            $constructorParameters[] = $this->getTypedParameter($parameterReflectionClass, $parameterItem, $cart);
                        };
                    } else {
                        $constructorParameters[] = $this->getTypedParameter($parameterReflectionClass, $parameterValue, $cart);
                    }

                } else {
                    $constructorParameters[] = $parameterValue;
                }
            }
        }

        /** @var DiscountCondition $discount */
        $discount = $class->newInstanceArgs($constructorParameters);
        $discount->setCart($cart);

        return $discount;
    }

    /**
     * Check parameter type hinting
     *
     * @param \ReflectionClass $reflectionClass
     * @param $parameterValue
     * @param Cart $cart
     * @return DiscountCondition|Model
     * @throws DiscountException
     */
    protected function getTypedParameter(\ReflectionClass $reflectionClass, $parameterValue, Cart $cart)
    {
        if ($reflectionClass->getName() == DiscountCondition::class) {
            return $this->getDiscountConditionOrThrowException($parameterValue, $cart);
        }

        if ($reflectionClass->isSubclassOf(Model::class)) {
            return $this->getEloquentParameterOrThrowException($reflectionClass, $parameterValue);
        }

        return $parameterValue;
    }

    protected function getCalcTypedParameter(\ReflectionClass $reflectionClass, $parameterValue)
    {
        if ($reflectionClass->isSubclassOf(Model::class)) {
            return $this->getEloquentParameterOrThrowException($reflectionClass, $parameterValue);
        }

        return $parameterValue;
    }

    /**
     * Returns Discount Condition object
     *
     * @param $data
     * @param Cart $cart
     * @return DiscountCondition
     * @throws DiscountException
     */
    protected function getDiscountConditionOrThrowException($data, Cart $cart)
    {
        $discount = $this->createFromJson(json_encode($data), $cart);

        if (empty($discount)) {
            throw new DiscountException('Invalid discount condition');
        }

        return $discount;
    }

    /**
     * Returns Eloquent model by id
     *
     * @param \ReflectionClass $reflectionClass model name
     * @param $id
     * @return Model
     * @throws DiscountException
     */
    protected function getEloquentParameterOrThrowException(\ReflectionClass $reflectionClass, $id): Model
    {
        $model = $reflectionClass->newInstance();
        $parameter =  $model::find($id);

        if (empty($parameter)) {
            throw new DiscountException('Invalid Eloquent model');
        };

        return $parameter;
    }

    /**
     * Returns concrete Discount Condition class
     * @param array $data
     * @return \ReflectionClass
     * @throws DiscountException
     * @throws \ReflectionException
     */
    protected function getDiscountClassOrThrowException(array $data)
    {
        if (empty($data['class'])) {
            throw new DiscountException('Invalid JSON string');
        }

        $class = new \ReflectionClass($data['class']);
        if (!$class->isSubclassOf(DiscountCondition::class)) {
            throw new DiscountException('Invalid class name');
        }

        return $class;
    }

    /**
     * Returns concrete Discount Condition class
     * @param array $data
     * @return \ReflectionClass
     * @throws DiscountException
     */
    protected function getCalcClassOrThrowException(array $data)
    {
        if (empty($data['class'])) {
            throw new DiscountException('Invalid JSON string');
        }

        $class = new \ReflectionClass($data['class']);
        if (!$class->isSubclassOf(DiscountCalc::class)) {
            throw new DiscountException('Invalid class name');
        }

        return $class;
    }
}
