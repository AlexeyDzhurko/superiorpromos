<?php


namespace App\Discounts;


use App\Models\CartItem;
use App\Payment\CartItemCost;

class OrderCostBelowDiscountCondition extends DiscountCondition
{
    protected $cost;

    public function __construct(float $cost)
    {
        $this->cost = $cost;
    }

    public static function getCostParameterLabel()
    {
        return 'Order Total $ Below';
    }

    public function isAccepted(): bool
    {
        $totalCost = 0;
        foreach ($this->cart->cartItems as $cartItem) { /** @var CartItem */
            $totalCost += (new CartItemCost($cartItem))->getTotal();
        }

        return $totalCost < $this->cost;
    }

    public static function getDescription(): string
    {
        return 'Valid if total cost less than';
    }

    public static function getTitle(): string
    {
        return 'Order total below';
    }

    public static function getParametrizedTitle(): string
    {
        return 'Order total below {{cost}}';
    }
}
