<?php


namespace App\Discounts;

use App\Models\CartItem;
use App\Payment\CartItemCost;

class OrderCostAboveDiscountCondition extends DiscountCondition
{
    protected $cost;

    public function __construct(float $cost)
    {
        $this->cost = $cost;
    }

    public static function getCostParameterLabel()
    {
        return 'Order Total $ Above';
    }

    public function isAccepted(): bool
    {
        $totalCost = 0;
        foreach ($this->cart->cartItems as $cartItem) { /** @var CartItem */
            $totalCost += (new CartItemCost($cartItem))->getTotal();
        }

        return $totalCost > $this->cost;
    }

    public static function getDescription(): string
    {
        return 'Valid if total cost greater than';
    }

    public static function getTitle(): string
    {
        return 'Order total above';
    }
}
