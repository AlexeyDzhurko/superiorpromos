<?php


namespace App\Discounts;


use App\Models\CartItem;
use App\Models\Product;
use App\Payment\CartItemCost;

class ProductDiscountCalc extends DiscountCalc
{
    protected $percent;
    protected $product;

    public function __construct(Product $product, int $percent)
    {
        $this->percent = $percent;
        $this->product = $product;
    }

    public function getDiscountAmount(): float
    {
        $cost = 0;
        foreach ($this->cart->cartItems as $cartItem) { /** @var CartItem $cartItem */
            if($cartItem->product_id != $this->product->id) {
                continue;
            }
            $cost += (new CartItemCost($cartItem))->getTotalWithoutShipping();
        }

        $this->totalDiscountAmount = $this->round($this->percent / 100 * $cost);

        return $this->totalDiscountAmount;

    }

    static public function getTitle(): string
    {
        return 'Multiply price of product';
    }
}
