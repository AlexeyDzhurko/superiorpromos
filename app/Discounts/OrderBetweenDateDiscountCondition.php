<?php


namespace App\Discounts;


use Carbon\Carbon;

class OrderBetweenDateDiscountCondition extends DiscountCondition
{
    protected $from;
    protected $to;

    public function __construct($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    public function isAccepted(): bool
    {
        $dateFrom = new Carbon($this->from);
        $dateTo = new Carbon($this->to);
        $now = new Carbon();

        return $now->startOfDay()->between($dateFrom, $dateTo);
    }

    public static function getDescription(): string
    {
        return 'Valid if order will be completed between dates';
    }

    public static function getTitle(): string
    {
        return 'Date between';
    }
}
