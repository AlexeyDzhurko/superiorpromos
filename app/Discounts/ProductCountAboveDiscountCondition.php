<?php


namespace App\Discounts;

use App\Models\CartItem;
use App\Models\Product;

class ProductCountAboveDiscountCondition extends DiscountCondition
{
    protected $product;
    protected $quantity;

    public function __construct(Product $product, int $quantity)
    {
        $this->product = $product;
        $this->quantity = $quantity;
    }

    public static function getQuantityParameterLabel()
    {
        return 'At least (pcs.) in cart';
    }

    public function isAccepted(): bool
    {
        foreach ($this->cart->cartItems as $cartItem) { /** @var CartItem */
            if (($cartItem->product->id == $this->product->id) && ($cartItem->quantity > $this->quantity)) {
                return true;
            }
        }

        return false;
    }

    public static function getDescription(): string
    {
        return 'Valid if order contains special count of specific product';
    }

    public static function getTitle(): string
    {
        return 'Contains product with count above';
    }
}
