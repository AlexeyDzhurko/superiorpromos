<?php


namespace App\Discounts;


use App\Models\CartItem;
use App\Payment\CartItemCost;


class TotalWithoutShippingDiscount extends DiscountCalc
{
    protected $percent;

    public function __construct(int $percent)
    {
        $this->percent = $percent;
    }

    public function getDiscountAmount(): float
    {
        $cost = 0;
        foreach ($this->cart->cartItems as $cartItem) { /** @var CartItem $cartItem */
            $cost += (new CartItemCost($cartItem))->getTotalWithoutShipping();
        }

        $this->totalDiscountAmount = $this->round($this->percent / 100 * $cost);

        return  $this->totalDiscountAmount;
    }

    static public function getTitle(): string
    {
        return 'Multiply order total without shipping';
    }
}
