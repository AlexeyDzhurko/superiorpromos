<?php


namespace App\Discounts;


class AllOfConditionsDiscountCondition extends DiscountCondition
{
    protected $discountConditions;

    public function __construct(DiscountCondition ...$discountConditions)
    {
        $this->discountConditions = $discountConditions;
    }

    public function isAccepted(): bool
    {
        foreach ($this->discountConditions as $discountCondition) {
            if (!$discountCondition->isAccepted()){
                return false;
            }
        }
        return true;
    }

    public static function getDescription(): string
    {
        return 'Valid if all rules will be accepted';
    }

    public static function getTitle(): string
    {
        return 'All of';
    }
}
