<?php


namespace App\Discounts;


class DifferentProductCountBelowDiscountCondition extends DiscountCondition
{
    protected $count;

    public function __construct(int $count)
    {
        $this->count = $count;
    }

    public static function getCountParameterLabel()
    {
        return 'Total number of items in the order below';
    }

    public function isAccepted(): bool
    {
        return $this->cart->cartItems->count() < $this->count;
    }

    public static function getDescription(): string
    {
        return 'Valid if cart contains unique products count less than';
    }

    public static function getTitle(): string
    {
        return 'Order total products below';
    }
}
