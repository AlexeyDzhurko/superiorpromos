<?php


namespace App\Discounts;


use App\Models\CartItem;
use App\Models\OrderItem;
use App\Payment\CartItemCost;
use App\Payment\OrderItemCost;

class OrderCostWithoutShippingBelowDiscountCondition extends DiscountCondition
{
    protected $cost;

    public function __construct(float $cost)
    {
        $this->cost = $cost;
    }

    public static function getCostParameterLabel()
    {
        return 'Order Total Excluding Shipping $ Below';
    }

    public function isAccepted(): bool
    {
        $totalCost = 0;

        foreach ($this->cart->cartItems as $cartItem) { /** @var CartItem */
            $totalCost += (new CartItemCost($cartItem))->getTotalWithoutShipping();
        }

        return $totalCost < $this->cost;
    }

    public static function getDescription(): string
    {
        return 'Valid if total cost (without shipping) less than';
    }

    public static function getTitle(): string
    {
        return 'Order total (excluding shipping) below';
    }
}
