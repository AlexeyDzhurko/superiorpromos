<?php


namespace App\Discounts;


class CouponCodeDiscountCondition extends DiscountCondition
{
    protected $discountCode;

    public function __construct(string $discountCode)
    {
        $this->discountCode = $discountCode;
    }

    public static function getDiscountCodeParameterLabel()
    {
        return 'Entered coupon code';
    }

    public function isAccepted(): bool
    {
        return $this->cart->discount->code == $this->discountCode;
    }

    public static function getDescription(): string
    {
        return 'User enter secret coupon code';
    }

    public static function getTitle(): string
    {
        return 'Coupon code';
    }
}
