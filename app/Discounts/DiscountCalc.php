<?php


namespace App\Discounts;


use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Order;
use App\Payment\CartItemCost;

abstract class DiscountCalc
{
    /**
     * @var Order
     */
    protected $cart;
    protected $totalDiscountAmount;

    abstract public function getDiscountAmount(): float;
    abstract static public function getTitle(): string ;

    public function setCart(Cart $cart)
    {
        $this->cart = $cart;
    }

    public function getTotalWithDiscount(): float
    {
        $cost = 0;
        foreach ($this->cart->cartItems as $cartItem) { /** @var CartItem $cartItem */
            $cost += (new CartItemCost($cartItem))->getTotal();
        }

        return $this->round($cost - $this->totalDiscountAmount);
    }

    public function getDiscountCode(): string
    {
        return $this->cart->discount->code;
    }

    public static function getParametrizedTitle(): string
    {
        return static::getTitle();
    }

    protected function round($amount)
    {
        return round($amount, 2, PHP_ROUND_HALF_UP);
    }
}
