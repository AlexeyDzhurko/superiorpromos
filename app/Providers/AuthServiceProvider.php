<?php

namespace App\Providers;

use App\Models\ArtProof;
use App\Models\CartItem;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\SavedCartItem;
use App\Models\UserNote;
use App\Models\Wish;
use App\PaymentProfile;
use App\Policies\ArtProofPolicy;
use App\Policies\CartItemPolicy;
use App\Policies\OrderItemPolicy;
use App\Policies\OrderPolicy;
use App\Policies\PaymentProfilePolicy;
use App\Policies\SavedCartItemPolicy;
use App\Policies\UserNotePolicy;
use App\Policies\WishPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        'App\Models\Address' => 'App\Policies\AddressPolicy',
        PaymentProfile::class => PaymentProfilePolicy::class,
        CartItem::class => CartItemPolicy::class,
        SavedCartItem::class => SavedCartItemPolicy::class,
        ArtProof::class => ArtProofPolicy::class,
        Wish::class => WishPolicy::class,
        UserNote::class => UserNotePolicy::class,
        Order::class => OrderPolicy::class,
        OrderItem::class => OrderItemPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
