<?php

namespace App\Providers;

use App\Contracts\CartManager;
use App\Contracts\MailService;
use App\Contracts\TaxExemptionServiceInterface;
use App\Models\Address;
use App\Models\ArtProof;
use App\Models\CaseStudy;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderSample;
use App\Models\OrderSampleTrackingInformation;
use App\Models\Payment;
use App\Models\ProductExtraImage;
use App\Models\Setting;
use App\Models\Testimonial;
use App\Models\TrackingInformation;
use App\Models\UserNote;
use App\Observers\AddressBookObserver;
use App\Observers\ArtProofObserver;
use App\Observers\ElasticObserver;
use App\Observers\OrderItemObserver;
use App\Observers\OrderObserver;
use App\Observers\OrderSampleObserver;
use App\Observers\PaymentHistoryObserver;
use App\Observers\PaymentProfileObserver;
use App\Observers\ProductExtraImageObserver;
use App\Observers\SampleOrderItemTrackingInformationObserver;
use App\Observers\TestimonialObserver;
use App\Observers\TrackingInformationObserver;
use App\Observers\UserNoteObserver;
use App\Observers\UserObserver;
use App\PaymentProfile;
use App\Services\BigMailerService;
use App\Services\DatabaseCart;
use App\Services\OrderCalculation;
use App\Services\TaxExemptionService;
use App\Services\UpsShippingCostCalculation\ShippingCostCalculation;
use App\User;
use Illuminate\Support\ServiceProvider;
use App\Contracts\PaymentGateway;
use App\Payment\AuthorizeNet;
use App\Contracts\EmailService;
use App\Notification\SendGrid;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use App\Contracts\ShippingService;
use App\Services\UPSService;
use L5Swagger\L5SwaggerServiceProvider;
use MetricLoop\TransformerMaker\TransformerMakerServiceProvider;
use App\Models\Product;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        PaymentProfile::observe(PaymentProfileObserver::class);
        User::observe(UserObserver::class);
        Testimonial::observe(TestimonialObserver::class);
        ProductExtraImage::observe(ProductExtraImageObserver::class);

        User::observe(ElasticObserver::class);
        Order::observe(ElasticObserver::class);
        OrderSample::observe(ElasticObserver::class);
        Product::observe(ElasticObserver::class);
        CaseStudy::observe(ElasticObserver::class);
        OrderItem::observe(OrderItemObserver::class);
        ArtProof::observe(ArtProofObserver::class);
        Order::observe(OrderObserver::class);
        TrackingInformation::observe(TrackingInformationObserver::class);
        OrderSample::observe(OrderSampleObserver::class);
        UserNote::observe(UserNoteObserver::class);
        Address::observe(AddressBookObserver::class);
        Payment::observe(PaymentHistoryObserver::class);
        OrderSampleTrackingInformation::observe(SampleOrderItemTrackingInformationObserver::class);

        config([
            'site-settings' => Setting::all(['key', 'value'])
                ->keyBy('key')
                ->transform(function ($setting){
                    return $setting->value;
                })->toArray()
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PaymentGateway::class, AuthorizeNet::class);
        $this->app->singleton(TaxExemptionServiceInterface::class, function () {
            return new TaxExemptionService(config('superiorpromos.storage.tax_exemption_certificates_path'));
        });
        $this->app->singleton(ShippingService::class, ShippingCostCalculation::class);
        $this->app->singleton(CartManager::class, DatabaseCart::class);
        $this->app->singleton(OrderCalculation::class);
        $this->app->singleton(MailService::class, BigMailerService::class);

        if ($this->app->environment() !== 'production') {
            $this->app->register(IdeHelperServiceProvider::class);
            $this->app->register(L5SwaggerServiceProvider::class);
            $this->app->register(TransformerMakerServiceProvider::class);
        }
    }
}
