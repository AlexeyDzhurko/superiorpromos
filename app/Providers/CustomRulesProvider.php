<?php

namespace App\Providers;

use App\Models\ColorGroup;
use App\Models\Imprint;
use App\Models\Price;
use App\Models\Product;
use App\Models\ProductOption;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use Validator;
use CreditCard;

class CustomRulesProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerZipRule();
        $this->registerPhoneRule();
        $this->registerCreditCardRule();
        $this->registerExpirationDateRule();
        $this->registerMinQuantityRule();
        $this->registerProductOptionsSetRule();
        $this->registerProductColorsSetRule();
        $this->registerProductImprintsRule();
        $this->registerPasswordRule();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    protected function registerZipRule()
    {
        /** @noinspection PhpUnusedParameterInspection */
        Validator::extend('zip', function ($attribute, $value, $parameters) {
            return preg_match('/^[0-9]{5,6}(\-[0-9]{4})?$/', $value);
        });
    }

    protected function registerPhoneRule()
    {
        /** @noinspection PhpUnusedParameterInspection */
        Validator::extend('phone', function ($attribute, $value, $parameters) {
            return preg_match('/^\d{3}-\d{3}-\d{4}$/', $value);
        });
    }

    protected function registerCreditCardRule()
    {
        /** @noinspection PhpUnusedParameterInspection */
        Validator::extend('credit_card', function ($attribute, $value, $parameters) {
            $result = CreditCard::validCreditCard($value);
            return (bool)$result['valid'];
        });
    }

    protected function registerMinQuantityRule()
    {
        Validator::extend('min_quantity', function ($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) {
            $product = Product::with('productPrices')->find($validator->getData()[$parameters[0]]);
            foreach ($product->productPrices as $price) {
                if ($price->quantity <= $value) {
                    return true;
                }
            }
            return false;
        });
    }

    protected function registerExpirationDateRule()
    {
        /** @noinspection PhpUnusedParameterInspection */
        Validator::extend('expiration_month_with_year', function ($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) {
            $year = $validator->getData()[$parameters[0]] ?? 0;
            return CreditCard::validDate($year, $value);
        });
    }

    /**
     * Validate Product Options
     */
    protected function registerProductOptionsSetRule()
    {
        Validator::extend('product_options_set', function ($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) {
            /** @var Product $product */
            $product = Product::find($value);
            $requestedOptions = (array)json_decode($validator->getData()[$parameters[0]] ?? '', true);

            $invalidRequestedProductOptions = [];

            $productOptions = $product->productOptions;
            foreach ($requestedOptions as $requestedOption) {
                $currentProductOption = array_first($productOptions, function($value) use ($requestedOption) {
                    return $value->id == $requestedOption['product_option_id'];
                });

                if(is_null($currentProductOption)) {
                    $invalidRequestedProductOptions[] = $requestedOption['product_option_id'];
                    continue;
                }

                if(!empty($currentProductOption->productSubOptions)) {

                    if(empty($requestedOption['product_sub_option_ids'])) {
                        $invalidRequestedProductOptions[] = $requestedOption['product_option_id'];
                        continue;
                    }

                    foreach ($requestedOption['product_sub_option_ids'] as $requestedProductSubOptionId) {
                        if (is_null($currentProductOption->productSubOptions->find($requestedProductSubOptionId))) {
                            $invalidRequestedProductOptions[] = $currentProductOption->id;
                            continue;
                        }
                    }
                }
            }

            if (!empty($invalidRequestedProductOptions)) {
                $validator->addReplacer('product_options_set', function ($message, $attribute, $rule, $parameters) use ($invalidRequestedProductOptions) {
                    return str_replace(':options', implode(',', $invalidRequestedProductOptions), $message);
                });
                return false;
            }

            return true;

        }, 'Invalid product options: :options');

    }

    protected function registerProductColorsSetRule()
    {
        Validator::extend('product_colors_set', function ($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) {
            /** @var Product $product */
            $product = Product::find($value);
            $requestedColors = (array)json_decode($validator->getData()[$parameters[0]] ?? '', true);

            $invalidColorGroups = [];

            foreach ($product->productColorGroups as $productColorGroup) { /** @var ProductColorGroup $productColorGroup */

                //check that color belongs to product color group

                $currentColorGroup = array_first($requestedColors, function($value) use ($productColorGroup) {
                    return $value['color_group_id'] == $productColorGroup->id;
                });

                if (is_null($currentColorGroup) ||
                    (!in_array($currentColorGroup['color_id'], $productColorGroup->productColors->pluck('id')->toArray()))
                ) {
                    $invalidColorGroups[] = $productColorGroup->id;
                }
            }

            if (!empty($invalidColorGroups)) {
                $validator->addReplacer('product_colors_set', function ($message, $attribute, $rule, $parameters) use ($invalidColorGroups) {
                    return str_replace(':colors', implode(',', $invalidColorGroups), $message);
                });
                return false;
            }

            return true;

        }, 'Invalid color groups: :colors');
    }

    protected function registerProductImprintsRule()
    {
        Validator::extend('product_imprints_set', function ($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) {
            /** @var Product $product */
            $product = Product::find($value);
            $requestedImprints = (array)json_decode($validator->getData()[$parameters[0]] ?? '', true);

            $invalidImprints = [];

//            foreach ($product->imprints as $imprint) { /** @var Imprint $imprint */
//                $currentImprint = array_first($requestedImprints, function($value) use ($imprint) {
//                    return $value['imprint_id'] == $imprint->id;
//                });
//
//                if (is_null($currentImprint)) {
//                    continue;
//                }
//
//                if (count($currentImprint['color_ids']) > $imprint->max_colors) {
//                    $invalidImprints[] = $imprint->id;
//                    continue;
//                }
//
//                foreach ($currentImprint['color_ids'] as $currentColorId) {
//                    if ($imprint->colorGroup) {
//                        if (!$imprint->colorGroup->colors->contains('id', $currentColorId)) {
//                            $invalidImprints[] = $imprint->id;
//                            break;
//                        }
//                        continue;
//                    }
//                    if (!$imprint->imprintColors->isEmpty()) {
//                        if (!$imprint->imprintColors->contains('color_id', $currentColorId)) {
//                            $invalidImprints[] = $imprint->id;
//                            break;
//                        }
//                        continue;
//                    }
//                    if (!$imprint->colorGroup && $imprint->imprintColors->isEmpty()) {
//                        $invalidImprints[] = $imprint->id;
//                        break;
//                        continue;
//                    }
//                }
//            }

            if (!empty($invalidImprints)) {
                $validator->addReplacer('product_iBase Unit Pricemprints_set', function ($message, $attribute, $rule, $parameters) use ($invalidImprints) {
                    return str_replace(':imprints', implode(',', $invalidImprints), $message);
                });
                return false;
            }

            return true;


        }, 'Invalid imprints: :imprints');
    }

    protected function registerPasswordRule()
    {
        Validator::extend('password', function ($attribute, $value, $parameters) {
            return preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/', $value);
        }, 'Invalid password. Password should have at least 1 lowercase AND 1 uppercase AND 1 number.');
    }
}
