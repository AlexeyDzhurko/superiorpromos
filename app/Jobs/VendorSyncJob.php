<?php

namespace App\Jobs;

use DB;

class VendorSyncJob extends SyncJob
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $oldVendors = $this->getVendorsList();
        $this->syncList($oldVendors->items());

        /** if total amount pages bigger that 1 */
        if ($oldVendors->lastPage() > 1) {
            for ($i = 2; $i <= $oldVendors->lastPage(); $i++) {
                $oldVendors = $this->getVendorsList($i);

                $this->syncList($oldVendors->items());
                echo ">>>Script has inserted " . $i * 100 . " vendors \n";
            }
        }
    }

    /**
     * @param array $oldVendors
     */
    private function syncList($oldVendors)
    {
        $pdo = DB::getPdo();
        $pdo->beginTransaction();

        foreach ($oldVendors as $oldVendor) {
            $this->insertData($oldVendor);
        }

        $pdo->commit();
    }

    /**
     * Insert date to product icon
     * @param object $vendor
     */
    private function insertData($vendor)
    {
        DB::table('vendors')->insert([
            'name' => $vendor->vendor_name,
            'phone' => $vendor->vendor_phone,
            'fax' => $vendor->vendor_fax,
            'email' => $vendor->vendor_email,
            'state' => $vendor->vendor_state,
            'city' => $vendor->vendor_city,
            'country' => $vendor->vendor_country,
            'zip_code' => $vendor->vendor_zipcode,
            'address_1' => $vendor->vendor_address_1,
            'address_2' => $vendor->vendor_address_2,
            'supplier_id' => $vendor->SuppID,
            'site' => $vendor->Web,
            'payment_template' => $vendor->payment_template,
            'id' => $vendor->item_id,
        ]);
    }

    /**
     * @param int $page
     * @return \Illuminate\Support\Collection
     */
    private function getVendorsList($page = 1)
    {
        return $this->oldDbConnection
            ->table('dt_vendor')
            ->paginate(100, ['*'], 'page', $page);
    }

}
