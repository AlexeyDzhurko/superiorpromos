<?php

namespace App\Jobs;

use DB;
use App\Models\Category;

class CategoriesSyncJob extends SyncJob
{

    /**
     * ProductSyncJob constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::table('categories')->delete();
        $level = 1;
        $categories = $this->oldDbConnection
            ->table('treeman')
            ->where('item_type', 0)
            ->where('level', 1)
            ->whereNull('doctype')
            ->get();

        foreach ($categories as $rootCategory) {
            $root = Category::create([
                'name' => $rootCategory->item_name,
                'slug' => $rootCategory->url_name,
                'old_id' => $rootCategory->item_id,
            ]);
            $this->getOldChildCategories($rootCategory->item_id, $root, 2);
        }
    }

    /**
     * @param integer $parentId
     * @param Category $root
     * @param integer $level
     * @return array
     */
    private function getOldChildCategories($parentId, $root, $level)
    {
        $categories = $this->oldDbConnection
            ->table('treeman')
            ->where('item_type', 0)
            ->where('level', $level)
            ->where('parent_item_id', $parentId)
            ->whereNull('doctype')
            ->get();

        $level++;
        foreach ($categories as $categoryChild) {
            $category = Category::create([
                'name' => $categoryChild->item_name,
                'slug' => $categoryChild->url_name,
                'old_id' => $categoryChild->item_id,
            ]);
            $category->makeChildOf($root);
            $this->getOldChildCategories($categoryChild->item_id, $category, $level);
        }
    }
}
