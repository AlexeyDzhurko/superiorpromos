<?php

namespace App\Jobs;

use DB;
use Image;

class ProductIconSyncJob extends SyncJob
{
    /**
     * @var string
     */
    public $oldTableName = 'dt_prodicon';

    /**
     * @var string
     */
    public $newTableName = 'product_icons';

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $oldIcons = $this->getProductIconList();
        $this->syncList($oldIcons);
    }

    /**
     * @param object $icons
     */
    private function syncList($icons)
    {
        $pdo = DB::getPdo();
        $pdo->beginTransaction();

        foreach ($icons as $icon) {
            $this->insertData($icon);
        }

        $pdo->commit();
    }

    /**
     * Insert date to product icon
     * @param object $icon
     */
    private function insertData($icon)
    {
        if($this->saveProductIcons($icon)) {
            DB::table($this->newTableName)->insert([
                'name' => $icon->icon_pic_src,
                'text' => $icon->icontext,
                'icon_pic_src' => 'product-icon/' . $icon->icon_pic_src,
                'icon_pic_width' => $icon->icon_pic_width,
                'icon_pic_height' => $icon->icon_pic_height,
                'position' => $icon->position,
                'old_id' => $icon->item_id
            ]);
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function getProductIconList()
    {
        return $this->oldDbConnection
            ->table($this->oldTableName)
            ->get();
        ;
    }

    private function saveProductIcons($entity)
    {
        $file = 'https://www.superiorpromos.com/img/ucart/images/icon_pic/' . $entity->item_id . '/' . $entity->icon_pic_src;
        try {
            $dir = storage_path('app/public/product-icon');
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            Image::make($file)->save($dir . '/' . $entity->icon_pic_src);

            return true;

        } catch (\Exception $e) {
            
            return false;
        }
    }
}
