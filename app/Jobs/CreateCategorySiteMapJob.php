<?php


namespace App\Jobs;

use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;
use Storage;

class CreateCategorySiteMapJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Handle Job
     */
    public function handle() {
       Category::where('active', 1)->chunk(500, function ($categories){
            $siteMap = Sitemap::create();
            $index = rand();
            foreach ($categories as $category) {
                $siteMap->add(Url::create(\url(implode('/',  $category->getAncestorsAndSelf()->pluck('slug')->toArray())) . '/')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_DAILY)
                    ->setPriority(0.8));
            }

           Storage::disk('sitemap')->put("sitemaps/categories_sitemap_{$index}.xml", $siteMap->render());
        });
    }
}
