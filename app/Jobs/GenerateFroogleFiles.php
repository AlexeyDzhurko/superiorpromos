<?php

namespace App\Jobs;

use App\Models\Breadcrumb;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductColorGroup;
use App\Models\ProductPrice;
use App\Transformers\ProductBreadcrumbsTransformer;
use App\Transformers\ProductMarketingOptionsTransformer;
use App\Transformers\ProductSearchTransformer;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class GenerateFroogleFiles implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $categories;

    /**
     * Create a new job instance.
     *
     * @param array $categories
     */
    public function __construct(array $categories)
    {
        set_time_limit(0);

        $this->categories = $categories;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $csvHeaders = [
            'id',
            'title',
            'description',
            'condition',
            'price',
            'availability',
            'link',
            'image_link',
            'mpn',
            'brand',
            'google product category',
            'gender',
            'age group',
            'size',
            'color',
            'material',
            'pattern',
            'item group id',
            'tax',
            'shipping weight',
            'sale price',
            'sale price effective date',
            'additional image link',
            'product_type'
        ];

        $csvData = [];
        $csvData[] = $csvHeaders;
        $productCollection = [];

        $root = new \SimpleXMLElement('<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0" encoding="UTF-8"></rss>');
        $xml = $root->addChild('channel');
        $xml->addChild('title', 'Promotional Products and Promotional Items by SuperiorPromos.com');
        $xml->addChild('link', 'http://superiorpromos.com/');
        $xml->addChild('description',
            'Promotional Products and promotional items made easy by www.superiorpromos.com. Advertise your logo with the biggest selection of tradeshow giveaways, promotional pens and more.  Our safe shopping guarantee includes the lowest prices on custom t-shirts, promotional tote bags, and custom ceramic mugs.  Our talented art department can make any logo look its best for your upcoming promotional products event.');

        foreach ($this->categories as $category) {
            /** @var Category $category */
            foreach ($category->products as $product) {
                /** @var Product $product */
                try {
                    /** @var ProductPrice $productPrice */
                    $productPrice = $product->productPrices->sortBy('quantity', SORT_REGULAR)->first();

                    if (!empty($productPrice)) {
                        $itemPrice = $productPrice->sale_item_price > 0 ? $productPrice->sale_item_price : $productPrice->item_price;
                        $price = number_format(($productPrice->quantity * $itemPrice) + $productPrice->setup_price, 2) . ' USD';
                    } else {
                        $price = '';
                    }

                    if (!$product->active) {
                        continue;
                    }

//                    $item = $xml->addChild('item');

                    $currentCategory = $product->categories->first();

                    $rootCategory = $currentCategory;
                    $categoriesTree = [];
                    do {
                        $categoriesTree[] = $currentCategory->name;
                        $newRootCategory = $currentCategory->parent()->first();
                        if (is_null($newRootCategory)) {
                            $rootCategory = $currentCategory;
                        }
                    } while ($currentCategory = $newRootCategory);

                    $categoriesTree = implode(' > ', array_reverse($categoriesTree));

                    $description = $rootCategory->froogle_description ? $rootCategory->froogle_description . ' ' : '';
                    $description .= $product->description ?? $product->name;

                    $skuVendor = $product->vendors->first()->pivot->sku ?? '';
                    $brand = !empty($product->vendors->first()) ? $product->vendors->first()->name : '';

                    $breadcrumbs = $this->filterBreadcrumbs($product);
                    $breadcrumbs = !empty($breadcrumbs) ? array_shift($breadcrumbs) : [];
                    $productUri = !empty($breadcrumbs['breadcrumbs']) ? mb_strtolower(end($breadcrumbs['breadcrumbs'])['slug'] . '/' . $product->url) : $product->url;
                    $productUrl = !empty($productUri) ? url($productUri) : '';



                    $colors = [];
                    foreach ($product->productColorGroups as $productColorGroup) {
                        /** @var ProductColorGroup $productColorGroup */
                        foreach ($productColorGroup->productColors as $productColor) {
                            /** @var ProductColor $productColor */
                            $colors[] = $productColor->color->name;
                        }
                    }

                    $colors = implode(',', array_unique($colors));
                    $imageLink = is_null($product->image_src) ? '' : $product->image_src;
                    $imageUrl = $imageLink ? "https://superiorpromos.com/cdn-cgi/image/format=jpeg,width=300/$imageLink" : '';

                    $csvItem = [];

                    $csvItem[] = $product->id;
                    $csvItem[] = 'Promotional Custom ' . $this->clearAllCharacters($product->name);
                    $csvItem[] = $this->clearAllCharacters($description);
                    $csvItem[] = 'new';
                    $csvItem[] = $price;
                    $csvItem[] = 'in_stock';
                    $csvItem[] = $this->clearHtmlEntities($productUrl);
                    $csvItem[] = $imageUrl;
                    $csvItem[] = $this->clearHtmlEntities($skuVendor);
                    $csvItem[] = $this->clearHtmlEntities($brand);
                    $csvItem[] = $this->clearHtmlEntities($product->google_product_category);
                    $csvItem[] = $this->clearHtmlEntities($product->gender);
                    $csvItem[] = $this->clearHtmlEntities($product->age_group);
                    $csvItem[] = '';
                    $csvItem[] = $this->clearHtmlEntities($colors);
                    $csvItem[] = $this->clearHtmlEntities($product->material);
                    $csvItem[] = $this->clearHtmlEntities($product->pattern);
                    $csvItem[] = '';
                    $csvItem[] = '';
                    $csvItem[] = $this->clearHtmlEntities($product->box_weight);
                    $csvItem[] = '';
                    $csvItem[] = '';
                    $csvItem[] = '';
                    $csvItem[] = $this->clearHtmlEntities($categoriesTree);

                    $csvData[] = $csvItem;
                    $productCollection[] = $product;

//                    $item->addChild('g:id', $product->id, 'g');
//                    $item->addChild('title', 'Promotional Custom ' . $this->clearAllCharacters($product->name));
//                    $item->addChild('description', $this->clearAllCharacters($description));
//                    $item->addChild('g:condition', 'new', 'g');
//                    $item->addChild('g:price', $price);
//                    $item->addChild('g:availability', 'in_stock');
//                    $item->addChild('link', $this->clearHtmlEntities(url($productUrl)));
//                    $item->addChild('g:image_link', $this->clearHtmlEntities($imageUrl), 'g');
//                    $item->addChild('g:mpn', $this->clearHtmlEntities($skuVendor));
//                    $item->addChild('g:brand', $this->clearHtmlEntities($brand));
//                    $item->addChild('g:google_product_category', $this->clearHtmlEntities($product->google_product_category), 'g');
//                    $item->addChild('gender', $this->clearHtmlEntities($product->gender));
//                    $item->addChild('age_group', $this->clearHtmlEntities($product->age_group));
//                    $item->addChild('size', '');
//                    $item->addChild('color', $this->clearHtmlEntities($colors));
//                    $item->addChild('material', $this->clearHtmlEntities($product->material));
//                    $item->addChild('pattern', $this->clearHtmlEntities($product->pattern));
//                    $item->addChild('item_group_id', '');
//                    $item->addChild('tax', '');
//                    $item->addChild('shipping_weight', $this->clearHtmlEntities($product->box_weight));
//                    $item->addChild('sale_price', '');
//                    $item->addChild('sale_price_effective_date', '');
//                    $item->addChild('additional_image_link', '');
//                    $item->addChild('g:product_type', $this->clearHtmlEntities($categoriesTree), 'g');
//                    $item->addChild('g:is_bundle', 'yes', 'g');
                } catch (\Exception $exception) {
                    throw new BadRequestHttpException('Error occurred while generating xml file.');
                }
            }
        }

        if (!is_dir(storage_path('app/public/froogle/'))) {
            mkdir(storage_path('app/public/froogle/'), 0777, true);
        }

//        $root->saveXML(storage_path('app/public/froogle/froogle.xml'));

        $csvFile = fopen(storage_path('app/public/froogle/froogle.txt'), 'w');

        foreach ($csvData as $row) {
            fputcsv($csvFile, $row);
        }
        fclose($csvFile);


        $productData = (new ProductSearchTransformer(collect($productCollection)))->toArray();

        $products = fractal()->collection($productData,
            new ProductBreadcrumbsTransformer(['categories' => []]))
            ->toArray();

        $products = fractal()->collection($products,
            new ProductMarketingOptionsTransformer())
            ->toArray();

        $content = view('generated-templates.google', compact('products'))->render();
        \Storage::put('public/froogle/froogle.xml', $content);
    }

    /**
     * @param $text
     * @return string
     */
    protected function clearAllCharacters($text)
    {
        if (empty($text)) {
            return '';
        }

        return trim(preg_replace('/ +/', ' ',
            preg_replace('/[^A-Za-z0-9-+.,"%]/', ' ', html_entity_decode(strip_tags($text)))));
    }

    /**
     * @param $text
     * @return string|string[]
     */
    protected function clearHtmlEntities($text)
    {
        if (empty($text)) {
            return '';
        }

        $search = array(
            '&permil;',
            '&uml;',
            '&lsquo;',
            '&Scaron;',
            '&scaron;',
            '&hellip;',
            '&copy;',
            '&iquest;',
            '&iuml;',
            '&reg;',
            '&trade;',
            '&mdash;',
            '&Acirc;',
            '&sup2;',
            '&frac12;',
            '&rsquo;',
            '&frac14;',
            '&eacute;',
            '&rdquo;',
            '&ldquo;',
            '&bull;',
            '&Ntilde;',
            '&deg;',
            '&acirc;',
            '&cent;',
            '&nbsp;',
            '&ndash;',
            '&ordm;',
            '&frac34;',
            '&euro;',
            '&Atilde;',
            '&sbquo;',
            '&bdquo;',
            '&middot;',
            '&egrave;',
            '&raquo;',
            '&OElig;',
            '&oelig;'
        );
        $htmlText = htmlentities($text, ENT_QUOTES, 'UTF-8');

        return str_replace($search, '', $htmlText);
    }

    private function filterBreadcrumbs($product)
    {
        $breadcrumb = Breadcrumb::class;

        return $product->categories->map(function ($category) use ($breadcrumb) {
            $breadcrumbs = $breadcrumb::render($category);

            return empty($category) ? '' : [
                'product_category_id' => $category->pivot->category_id,
                'type' => $category->pivot->type,
                'breadcrumbs' => $breadcrumbs
            ];
        })->toArray();
    }
}
