<?php

namespace App\Jobs;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;
use Storage;

class CreateProductSiteMapJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    const DEFAULT_PRIORITY = 0.3;

    public function handle()
    {
        Product::where('active', 1)->chunk(200, function ($products) {
            $siteMap = Sitemap::create();
            $index = rand();
            $date = Carbon::yesterday();
            $frequency = Url::CHANGE_FREQUENCY_DAILY;
            $priority = self::DEFAULT_PRIORITY;

            foreach ($products as $product) {
                foreach ($product->categories as $category) {
                    if (!$category->active) {
                        continue;
                    }
                    $categorySlug = implode('/', $category->getAncestorsAndSelf()->pluck('slug')->toArray());
                    $productSlug = $categorySlug . '/' . $product->url;

                    if (strpos($productSlug, ' ') !== false) {
                        foreach (explode(' ', $productSlug) as $slug) {
                            $siteMap->add(Url::create(url($slug))
                                ->setLastModificationDate($date)
                                ->setChangeFrequency($frequency)
                                ->setPriority($priority));
                        }
                    } else {
                        $siteMap->add(Url::create(url($productSlug))
                            ->setLastModificationDate($date)
                            ->setChangeFrequency($frequency)
                            ->setPriority($priority));
                    }
                }
            }

            Storage::disk('sitemap')->put("sitemaps/products_sitemap_{$index}.xml", $siteMap->render());
        });
    }
}
