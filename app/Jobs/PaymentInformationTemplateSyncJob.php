<?php


namespace App\Jobs;


use DB;

class PaymentInformationTemplateSyncJob extends SyncJob
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->syncList($this->getOldData());
    }

    protected function syncList($templates)
    {
        $pdo = DB::getPdo();
        $pdo->beginTransaction();

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('payment_information_templates')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        foreach ($templates as $template) {
            $this->insertData($template);
        }

        $pdo->commit();
    }

    protected function insertData($data)
    {
        DB::table('payment_information_templates')->insert([
            'name' => $data->name,
            'single_body' => $data->template_single,
            'multiple_body' => $data->template_multi,
        ]);
    }

    protected function getOldData()
    {
        return $this->oldDbConnection
            ->table('warehouse_templates')
            ->get();
    }
}