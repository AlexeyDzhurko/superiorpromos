<?php

namespace App\Jobs;

use DB;

class SyncJob
{
    /**
     * Connect to old data base
     * @var object
     */
    protected $oldDbConnection;

    /**
     * ProductSyncJob constructor.
     */
    public function __construct()
    {
        $this->oldDbConnection = DB::connection('old_mysql');
    }

}