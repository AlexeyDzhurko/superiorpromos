<?php


namespace App\Jobs;


use DB;

class PmsColorSyncJob extends SyncJob
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->syncList($this->getOldColors());
    }

    protected function syncList($colors)
    {
        $pdo = DB::getPdo();
        $pdo->beginTransaction();

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('pms_colors')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        foreach ($colors as $color) {
            $this->insertData($color);
        }

        $pdo->commit();
    }

    protected function insertData($color)
    {
        DB::table('pms_colors')->insert([
            'id' => $color->pms_color_id,
            'name' => $color->pms_color_name,
            'color_hex' => '#'.sprintf('%06x',$color->pms_color_hex),
        ]);
    }

    protected function getOldColors()
    {
        return $this->oldDbConnection
            ->table('pms_colors')
            ->get();
    }
}
