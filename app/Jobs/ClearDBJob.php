<?php

namespace App\Jobs;

use DB;

class ClearDBJob
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::table('product_icons')->delete();
        //DB::table('vendors')->delete();
        DB::table('products')->delete();
        DB::table('seo_products')->delete();
        DB::table('product_extra_images')->delete();
        DB::table('product_options')->delete();
        //DB::table('options')->delete();
        //DB::table('suboptions')->delete();
    }
}
