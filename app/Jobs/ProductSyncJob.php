<?php

namespace App\Jobs;

use App\Models\ProductIcon;
use DB;

class ProductSyncJob extends SyncJob
{
    /**
     * @var array
     */
    protected $productIconsArray;

    /**
     * @var int|string
     */
    protected $random_vendor_id;

    /**
     * ProductSyncJob constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->productIconsArray = $this->productIconList();
        $this->random_vendor_id = DB::table('vendors')->first();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $oldProducts = $this->getListProducts();
        $this->syncList($oldProducts->items());

        /** if total amount pages bigger that 1 */
        if ($oldProducts->lastPage() > 1) {
            for ($i = 2; $i <= $oldProducts->lastPage(); $i++) {
                $oldProducts = $this->getListProducts($i);

                $this->syncList($oldProducts->items());
                echo ">>>Script has inserted " . $i * 100 . " products \n";
            }
        }
    }

    /**
     * @param array $oldProducts
     */
    private function syncList($oldProducts)
    {
        $pdo = DB::getPdo();
        $pdo->beginTransaction();

        foreach ($oldProducts as $oldProduct) {
            if ($oldProduct->item_id) {
                $this->syncElement($oldProduct);
            }
        }
        $pdo->commit();
    }

    /**
     * Crete new product element
     * @param object $element
     */
    private function syncElement($element)
    {
        $productData = [
            'image_alt' => $element->pimage_alt,
            'additional_specifications' => $element->additional_specifications,
            'dimensions' => $element->pdimensions,
            'quantity_per_box' => $element->items_per_box,
            'image_src' => $element->pimage_src ?? null,
            'pricing_information' => $element->pricing_information,
            'imprint_area' => $element->imprint_area,
            'description' => $element->description,
            'production_time_from' => $element->production_time_from,
            'production_time_to' => $element->production_time_to,
            'on_sale' => $element->onsale,
            'custom_shipping_cost' => $element->custom_shipping_cost,
            'custom_shipping_method' => $element->custom_shipping_method,
            'old_product_id' => $element->item_id,
            'box_weight' => $element->boxweight,
            'zip_from' => $element->zip_from,
            'active' => (integer)$element->active ?? 0,
            'name' => $element->item_name ?? null,
            'product_icon_id' => $this->productIconsArray[$element->item_id] ?? null,
            'vendor_id' => $this->random_vendor_id->id
        ];

        DB::table('products')->insert($productData);

        $product = DB::getPdo()->lastInsertId();

        foreach ($this->getPricesGrid($element->item_id) as $oldPriceGrid) {
            $productPrice = [
                'product_id' => $product,
                'quantity' => $oldPriceGrid->count_from,
                'setup_price' => $oldPriceGrid->price_setup,
                'item_price' => $oldPriceGrid->price,
                'sale_item_price' => $oldPriceGrid->price_sale,
            ];
            DB::table('product_prices')->insert($productPrice);
        }

        foreach ($this->getProductColorGroups($element->item_id) as $colorGroup) {
            $colorGroupData = [
                'name' => $colorGroup->pcolor_name,
                'product_id' => $product,
                'old_color_group_id' => $colorGroup->pcolor_id,
            ];
            DB::table('color_groups')->insert($colorGroupData);
            $colorGroupId = DB::getPdo()->lastInsertId();

            foreach ($this->getProductColors($colorGroup->pcolor_id) as $color) {

                $colorId = $this->saveColorAndGetId($color->color_id);
                if (empty($colorId)) {
                    continue;
                }

                $colorData = [
                    'color_id' => $colorId,
                    'group_id' => $colorGroupId,
                ];
                DB::table('color_color_group')->insert($colorData);
            }
        }

        foreach ($this->getProductOptions($element->item_id) as $oldProductOption) {
            $productOption = [
                'product_id' => $product,
                'name' => $oldProductOption->option_name,
                'position' => $oldProductOption->option_pos,
                'old_product_option_id' => $oldProductOption->option_id,
                'required' => 1,
            ];
            DB::table('product_options')->insert($productOption);
            $productOptionId = DB::getPdo()->lastInsertId();

            foreach ($this->getProductOptionPricesGrid($oldProductOption->option_id) as $oldProductOptionPriceGrid) {
                $productOptionPrice = [
                    'product_option_id' => $productOptionId,
                    'quantity' => $oldProductOptionPriceGrid->count_from,
                    'setup_price' => $oldProductOptionPriceGrid->option_setup,
                    'item_price' => $oldProductOptionPriceGrid->option_cost,
                ];
                DB::table('product_option_prices')->insert($productOptionPrice);
            }

            foreach ($this->getProductSubOptions($oldProductOption->option_id) as $oldProductSubOption) {
                $productSubOption = [
                    'product_option_id' => $productOptionId,
                    'name' => $oldProductSubOption->suboption_name,
                    'position' => $oldProductSubOption->suboption_pos,
                    'old_product_sub_option_id' => $oldProductSubOption->suboption_id,
                ];
                DB::table('product_sub_options')->insert($productSubOption);
                $productSubOptionId = DB::getPdo()->lastInsertId();

                foreach ($this->getProductSubOptionPricesGrid($oldProductSubOption->suboption_id) as $oldProductSubOptionPriceGrid) {
                    $productSubOptionPrice = [
                        'product_sub_option_id' => $productSubOptionId,
                        'quantity' => $oldProductSubOptionPriceGrid->count_from,
                        'setup_price' => $oldProductSubOptionPriceGrid->suboption_setup,
                        'item_price' => $oldProductSubOptionPriceGrid->suboption_cost,
                    ];
                    DB::table('product_sub_option_prices')->insert($productSubOptionPrice);
                }
            }
        }

        foreach ($this->getProductImprints($element->item_id) as $oldImprint)
        {
            $imprint = [
                'product_id' => $product,
                'position' => $oldImprint->imprint_pos,
                'name' => $oldImprint->location_name,
                'max_colors' => $oldImprint->max_colors,
                'color_group_id' => null, //TODO: real color group
                'old_imprint_id' => $oldImprint->imprint_location_id,
            ];
            DB::table('imprints')->insert($imprint);
            $imprintId = DB::getPdo()->lastInsertId();
            foreach ($this->getImprintPricesGrid($oldImprint->imprint_location_id) as $oldImprintPriceGrid) {
                $productImprintPrice = [
                    'imprint_id' => $imprintId,
                    'quantity' => $oldImprintPriceGrid->count_from,
                    'setup_price' => $oldImprintPriceGrid->location_setup,
                    'item_price' => $oldImprintPriceGrid->location_additional,
                    'color_setup_price' => $oldImprintPriceGrid->location_color_setup,
                    'color_item_price' => $oldImprintPriceGrid->location_color_additional,
                ];
                DB::table('imprint_prices')->insert($productImprintPrice);
            }
        }


        $this->insertSeoInfo($element, $product);

        $images = $this->oldDbConnection
            ->table('product_extra_images')
            ->where('product_id', $element->item_id)
            ->get()
        ;

        if ($images) {
            $this->insertImages($images, $product);
        }
    }

    /**
     * @param int $page
     * @return mixed
     */
    private function getListProducts($page = 1)
    {
        return $this->oldDbConnection
            ->table('dt_product')
            ->join('treeman', 'dt_product.item_id', '=', 'treeman.item_id')
            ->paginate(100, ['*'], 'page', $page);
    }

    protected function getPricesGrid($productId)
    {
        return $this->oldDbConnection
            ->table('price_grid')
            ->where('item_id', $productId)->get();
    }

    protected function getProductOptions($productId)
    {
        return $this->oldDbConnection
            ->table('options')
            ->where('item_id', $productId)->get();
    }

    protected function getProductColorGroups($productId)
    {
        return $this->oldDbConnection
            ->table('pcolors')
            ->where('item_id', $productId)->get();
    }

    protected function getProductColors($oldColorGroupId)
    {
        return $this->oldDbConnection
            ->table('pcolors_colors')
            ->where('pcolor_id', $oldColorGroupId)->get();
    }

    protected function saveColorAndGetId($oldColorId)
    {
        $oldColor = $this->oldDbConnection
            ->table('colors')
            ->where('color_id', $oldColorId)->first();

        if (empty($oldColor)) {
            return null;
        }

        $color = DB::table('colors')->where('name', $oldColor->color_name)->first();
        if (!empty($color)) {
            return $color->id;
        }

        DB::table('colors')->insert([
            'name' => $oldColor->color_name,
            'color_hex' => '#'.sprintf('%06x',$oldColor->color_hex),
        ]);

        return DB::getPdo()->lastInsertId();
    }

    protected function getProductSubOptions($oldProductOptionId)
    {
        return $this->oldDbConnection
            ->table('suboptions')
            ->where('option_id', $oldProductOptionId)->get();
    }

    protected function getProductSubOptionPricesGrid($oldProductSubOptionId)
    {
        return $this->oldDbConnection
            ->table('suboptions_pg')
            ->where('suboption_id', $oldProductSubOptionId)->get();
    }

    protected function getProductOptionPricesGrid($oldProductOptionId)
    {
        return $this->oldDbConnection
            ->table('options_pg')
            ->where('option_id', $oldProductOptionId)->get();
    }

    protected function getProductImprints($productId)
    {
        return $this->oldDbConnection
            ->table('imprint_locations')
            ->where('item_id', $productId)->get();
    }

    protected function getImprintPricesGrid($oldImprintId)
    {
        return $this->oldDbConnection
            ->table('imprints_pg')
            ->where('imprint_location_id', $oldImprintId)->get();
    }

    /**
     * Persist data to product_extra_images table
     * @param array $images
     * @param integer $product
     */
    private function insertImages($images, $product)
    {
        $imagesArr = [];
        foreach ($images as $image) {
            $imageData = [
                'image_src' => $image->image_src,
                'image_main' => $image->image_main,
                'image_thumb' => $image->image_thumb,
                'image_list' => $image->image_list,
                'image_cart' => $image->image_cart,
                'image_color' => $image->image_color,
                'product_id' => $product
            ];
            $imagesArr[] = $imageData;
            unset($imageData);
        }

        if (!empty($imagesArr)) {
            DB::table('product_extra_images')->insert($imagesArr);
        }
    }

    /**
     * @param object $element
     * @param integer $product
     */
    private function insertSeoInfo($element, $product)
    {
        $seoProducts = [
            'name' => $element->item_name,
            'url' => $element->url_name,
            'meta_title' => $element->metatitle,
            'keywords' => $element->keywords,
            'meta_description' => $element->metadescription,
            'product_id' => $product
        ];

        DB::table('seo_products')->insert($seoProducts);
    }

    /**
     * Get product icons list
     * @return array
     */
    private function productIconList()
    {
        $result = [];
        $productsIcons =  ProductIcon::get();
        foreach ($productsIcons as $icon) {
            $result[$icon->old_id] = $icon->id;
        }

        return $result;
    }
}
