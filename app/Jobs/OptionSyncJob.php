<?php

namespace App\Jobs;

use App\Models\Option;
use DB;

class OptionSyncJob extends SyncJob
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $oldOptions = $this->getOptionsList();
        $this->syncList($oldOptions->items());

        /** if total amount pages bigger that 1 */
        if ($oldOptions->lastPage() > 1) {
            for ($i = 2; $i <= $oldOptions->lastPage(); $i++) {
                $oldOptions = $this->getOptionsList($i);

                $this->syncList($oldOptions->items());
                echo ">>>Script has processed " . $i * 100 . " options \n";
            }
        }
    }

    /**
     * @param array $oldOptions
     */
    private function syncList($oldOptions)
    {
        $pdo = DB::getPdo();
        $pdo->beginTransaction();

        foreach ($oldOptions as $oldOption) {
            $this->syncElement($oldOption);
        }

        $pdo->commit();
    }

    /**
     * Crete new option element
     * @param object $element
     */
    private function syncElement($element)
    {
        $product = DB::table('products')->where('old_product_id', $element->item_id)->first();
        /** Check exist product */
        if (!$product) {
            return;
        }

        if (!$option = DB::table('options')->where('name', $element->option_name)->first()) {
            $optionId = DB::table('options')->insertGetId(['name' => $element->option_name]);
        } else {
            $optionId = $option->id;
        }


            $type = $this->formedType($element->show_as);

            DB::table('products_options')->insert([
                'product_id' => $product->id,
                'option_id' => $optionId,
                'position' => $element->option_pos,
                'input_type' => $type,
                'old_option_id' => $element->option_id
            ]);

        $this->insertSuboption($element, $optionId);
    }

    /**
     * @param object $element
     * @param int $optionId
     */
    private function insertSuboption($element, $optionId)
    {
        $suboptions = $this->getSubptionsListByOptionId($element->option_id);

        if ($suboptions) {
            foreach ($suboptions as $suboption) {
                DB::table('suboptions')->updateOrInsert(
                    [
                        'option_id' => $optionId,
                        'position' => $suboption->suboption_pos,
                    ],
                    [
                        'name' => $suboption->suboption_name,
                        'old_suboption_id' => $suboption->suboption_id
                    ]
                );
            }
        }
    }

    /**
     * @param integer $page
     * @return array | null
     */
    private function getOptionsList($page = 1)
    {
        return $this->oldDbConnection
            ->table('options')
            ->paginate(100, ['*'], 'page', $page);
        ;
    }

    /**
     * @param integer $optionId
     * @return array | null
     */
    private function getSubptionsListByOptionId($optionId)
    {
        return $this->oldDbConnection
            ->table('suboptions')
            ->where('option_id', $optionId)
            ->get();
        ;
    }

    /**
     * @param integer $type
     * @return string
     */
    private function formedType($type)
    {
        switch ($type) {
            case Option::INPUT_TYPE_DROP_DOWN_CODE:
                return Option::INPUT_TYPE_DROP_DOWN;
                break;
            case Option::INPUT_TYPE_RADIO_CODE:
                return Option::INPUT_TYPE_RADIO;
                break;
            case Option::INPUT_TYPE_CHECKBOX_CODE:
                return Option::INPUT_TYPE_CHECKBOX;
                break;
        }
    }
}
