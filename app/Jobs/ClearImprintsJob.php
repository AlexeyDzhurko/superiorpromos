<?php


namespace App\Jobs;


use App\Models\Imprint;

class ClearImprintsJob
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $imprints = Imprint::whereColorGroupId(null)->get();

        foreach ($imprints as $imprint) {
            if(count($imprint->imprintPrices())) {
                $imprint->imprintPrices()->delete();
            }
            $imprint->delete();

            echo ">>>Deleting imprint \n";
        }
    }
}
