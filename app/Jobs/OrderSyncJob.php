<?php

// TODO Delete - old class

namespace App\Jobs;

use App\Models\ProductIcon;
use App\OldDBSync\Sync;
use App\OldDBSync;
use Carbon\Carbon;
use DB;
use JMS\Serializer\Tests\Fixtures\Discriminator\Car;

class OrderSyncJob extends SyncJob
{

    /**
     * ProductSyncJob constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $oldStages = $this->getStages();
        foreach ($oldStages as $oldStage) {
            $this->saveStage($oldStage);
        }

        $oldOrders = $this->getOrders();

        $this->syncList($oldOrders->items());

        /** if total amount pages bigger that 1 */
        if ($oldOrders->lastPage() > 1) {
            for ($i = 2; $i <= $oldOrders->lastPage(); $i++) {
                $oldOrders = $this->getOrders($i);

                $this->syncList($oldOrders->items());
                echo ">>>Script has inserted " . $i * 100 . " orders \n";
            }
        }
    }

    protected function getOrders($page = 1)
    {
        return $this->oldDbConnection
            ->table('orders')
            ->paginate(100, ['*'], 'page', $page);
    }

    protected function getCustomer($customerId)
    {
        return $this->oldDbConnection
            ->table('customers')
            ->where('customer_id', $customerId)->first();
    }

    protected function getOrderItems($orderId)
    {
        return $this->oldDbConnection
            ->table('order_items')
            ->where('order_id', $orderId)->get();
    }

    protected function getOrderItemTracking($orderItemId)
    {
        return $this->oldDbConnection
            ->table('order_items_tracking')
            ->where('order_item_id', $orderItemId)->first();
    }

    protected function getCartItem($cartItemId)
    {
        return $this->oldDbConnection
            ->table('cart')
            ->where('cart_item_id', $cartItemId)->first();
    }

    protected function getOrderItemOptions($orderItemId)
    {
        return $this->oldDbConnection
            ->table('order_items_options')
            ->where('order_item_id', $orderItemId)->get();
    }

    protected function getOrderItemSubOptions($orderItemOptionId)
    {
        return $this->oldDbConnection
            ->table('order_items_suboptions')
            ->where('order_items_option_id', $orderItemOptionId)->get();
    }

    protected function getProductId($itemId)
    {
        return DB::table('products')->where('id', $itemId)->first()->id ?? null;
    }

    protected function getProductOptionId($oldProductOptionId)
    {
        return DB::table('product_options')->where('id', $oldProductOptionId)->first()->id ?? null;
    }

    protected function getProductSubOptionId($oldProductOptionId)
    {
        return DB::table('product_sub_options')->where('id', $oldProductOptionId)->first()->id ?? null;
    }

    protected function getStageId($stageId)
    {
        return DB::table('stages')->where('id', $stageId)->first()->id ?? null;
    }

    protected function getStages()
    {
        return $this->oldDbConnection
            ->table('pstages')->get();
    }

    protected function saveStage($oldData)
    {
        try {
            DB::table('stages')->insert([
                'name' => $oldData->stage_name,
                'description' => $oldData->stage_descr,
                'position' => $oldData->pstage_pos,
                'id' => $oldData->pstage_id,
            ]);

            return DB::getPdo()->lastInsertId();
        } catch (\Exception $e) {

            return false;
        }
    }


    protected function saveOrGetCustomerId($oldData)
    {
        try {
            $user = DB::table('users')->where('id', $oldData->customer_id)->first();
            if ($user) {
                return $user->id;
            }
            DB::table('users')->insert([
                'id' => $oldData->customer_id,
                'name' => $oldData->contact_name,
                'email' => $oldData->login,
            ]);

            return DB::getPdo()->lastInsertId();
        } catch (\Exception $e) {

            return false;
        }
    }

    protected function saveOrder($oldData, $userId)
    {
        try {
            $createdDate = new Carbon($oldData->date);

            DB::table('orders')->insert([
                'user_id' => $userId,
                'created_at' => $createdDate,
                'updated_at' => $createdDate,
                'shipping_first_name' => $oldData->shipp_fname,
                'shipping_middle_name' => $oldData->shipp_mname,
                'shipping_last_name' => $oldData->shipp_lname,
                'shipping_title' => $oldData->shipp_title,
                'shipping_suffix' => $oldData->shipp_suffix,
                'shipping_company_name' => $oldData->shipp_company,
                'shipping_address_line_1' => $oldData->shipp_address1,
                'shipping_address_line_2' => $oldData->shipp_address2,
                'shipping_city' => $oldData->shipp_city,
                'shipping_state' => $oldData->shipp_state,
                'shipping_zip' => $oldData->shipp_zip,
                'shipping_country' => $oldData->shipp_country,
                'shipping_province' => $oldData->shipp_ostate,
                'shipping_day_telephone' => $oldData->pay_dphone,
                'billing_first_name' => $oldData->pay_fname,
                'billing_middle_name' => $oldData->pay_mname,
                'billing_last_name' => $oldData->pay_lname,
                'billing_title' => $oldData->pay_title,
                'billing_suffix' => $oldData->pay_suffix,
                'billing_company_name' => $oldData->pay_company,
                'billing_address_line_1' => $oldData->pay_address1,
                'billing_address_line_2' => $oldData->pay_address2,
                'billing_city' => $oldData->pay_city,
                'billing_state' => $oldData->pay_state,
                'billing_zip' => $oldData->pay_zip,
                'billing_country' => $oldData->pay_country,
                'billing_province' => $oldData->pay_ostate,
                'billing_day_telephone' => $oldData->pay_dphone,
                'multiple_location_comment' => $oldData->sm_comment,
                //TODO: add card
                'discount_code' => $oldData->coupon,
                'discount_id' => $oldData->discount_id,
                'price' => $oldData->amount,
                'total_price' => $oldData->amount,
                'discount_amount' => $oldData->discount,
            ]);

            return DB::getPdo()->lastInsertId();
        } catch (\Exception $e) {

            return false;
        }
    }

    protected function saveCartItemProductOptions($oldData, $cartItemId)
    {
        try {
            foreach ($oldData as $oldProductOption) {
                DB::table('cart_item_product_options')->insert([
                    'cart_item_id' => $cartItemId,
                    'product_option_id' => $this->getProductOptionId($oldProductOption->option_id),
                    'name' => $oldProductOption->option_name,
                    'setup_price' => (double)$oldProductOption->option_setup,
                    'item_price' => (double)$oldProductOption->option_cost,
                ]);

                $cartItemProductOptionId = DB::getPdo()->lastInsertId();

                $oldProductSubOptions = $this->getOrderItemSubOptions($oldProductOption->order_items_option_id);
                foreach ($oldProductSubOptions as $oldProductSubOption) {
                    DB::table('cart_item_product_sub_options')->insert([
                        'cart_item_product_option_id' => $cartItemProductOptionId,
                        'product_sub_option_id' => $this->getProductSubOptionId($oldProductSubOption->suboption_id),
                        'name' => $oldProductSubOption->suboption_name,
                        'setup_price' => (double)$oldProductSubOption->suboption_setup,
                        'item_price' => (double)$oldProductSubOption->suboption_cost,
                    ]);
                }
            }
        } catch (\Exception $e) {

            return false;
        }
    }

    protected function saveOrderItem($oldData, $orderId, $createdDate)
    {
        $productId = $this->getProductId($oldData->item_id);
        if (is_null($productId)) {
            return null;
        }

        $cartItemId = $this->saveCartItem($oldData, $createdDate);

        $this->saveCartItemProductOptions($this->getOrderItemOptions($oldData->order_item_id), $cartItemId);

        $orderItemTracking = $this->getOrderItemTracking($oldData->order_id);
        try {
            $trackingDate = new Carbon($orderItemTracking->date_shipped);
        } catch (\Exception $e) {
            $trackingDate = null;
        }

        //own_account,us_shipping,
        $shippingMethod = 'custom';
        if ($oldData->own_account == 2) {
            $shippingMethod = 'outside_us';
        } elseif ($oldData->own_account == 0) {
            $shippingMethod = 'us_shipping';
        } elseif ($oldData->own_account == 1) {
            $shippingMethod = 'own_account';
        } elseif ($oldData->own_account == 3) {
            $shippingMethod = 'custom';
        }

        $receivedDate = new Carbon($oldData->receive_date);

        $stageId = null;
        if ($oldData->pstage_id) {
            $stageId = $this->getStageId($oldData->pstage_id);
        }


        DB::table('order_items')->insert([
            'product_id' => $productId,
            'quantity' => $oldData->count,
            //TODO: calc price
            'price' => 0,
            'options_price' => 0,
            'check_notes' => $oldData->check_notes,
            'auto_remind' => $oldData->auto_remind,
            'not_paid' => $oldData->not_paid,
            //TODO: add files
            'art_file_name' => null,
            'art_file_path' => null,
            'po_number' => $oldData->po_number,
            //TODO: add files
            'invoice_file' => null,
            'tax_exempt' => $oldData->tax_exempt,
            'estimation_zip' => $oldData->estimate_zip,
            'estimation_shipping_method' => $oldData->shipping_est_method,
            'estimation_shipping_price' => $oldData->shipping_est_cost,
            'own_shipping_type' => $oldData->own_shipping_method,
            'own_account_number' => $oldData->own_account_number,
            'own_shipping_system' => $oldData->own_shipping_system,
            'shipping_method' => $shippingMethod,
            'vendor_id' => null,
            'received_date' => $receivedDate,
            'imprint_comment' => $oldData->imprint_comments,
            'shipping_first_name' => $oldData->shipp_fname,
            'shipping_middle_name' => $oldData->shipp_mname,
            'shipping_last_name' => $oldData->shipp_lname,
            'shipping_title' => $oldData->shipp_title,
            'shipping_suffix' => $oldData->shipp_suffix,
            'shipping_company_name' => $oldData->shipp_company,
            'shipping_address_line_1' => $oldData->shipp_address1,
            'shipping_address_line_2' => $oldData->shipp_address2,
            'shipping_city' => $oldData->shipp_city,
            'shipping_state' => $oldData->shipp_state,
            'shipping_zip' => $oldData->shipp_zip,
            'shipping_country' => $oldData->shipp_country,
            'shipping_province' => $oldData->shipp_ostate,
            'shipping_day_telephone' => $oldData->pay_dphone,
            'shipping_ext' => null,
            'shipping_fax' => null,
            'billing_first_name' => $oldData->pay_fname,
            'billing_middle_name' => $oldData->pay_mname,
            'billing_last_name' => $oldData->pay_lname,
            'billing_title' => $oldData->pay_title,
            'billing_suffix' => $oldData->pay_suffix,
            'billing_company_name' => $oldData->pay_company,
            'billing_address_line_1' => $oldData->pay_address1,
            'billing_address_line_2' => $oldData->pay_address2,
            'billing_city' => $oldData->pay_city,
            'billing_state' => $oldData->pay_state,
            'billing_zip' => $oldData->pay_zip,
            'billing_country' => $oldData->pay_country,
            'billing_province' => $oldData->pay_ostate,
            'billing_day_telephone' => $oldData->pay_dphone,
            'billing_ext' => null,
            'billing_fax' => null,
            'created_at' => $createdDate,
            'updated_at' => $createdDate,
            'order_id' => $orderId,
                        
            'tracking_shipping_date' => $trackingDate,
            'tracking_number' => $orderItemTracking->tracking_number ?? null,
            'tracking_shipping_company' => $orderItemTracking->shipping_company ?? null,
            'tracking_note' => $orderItemTracking->shipping_note ?? null,
            'tracking_user_id' => 1,
            'stage_id' => $stageId,
            'tracking_url' => $orderItemTracking->company_url ?? null,
            'item_price' => $oldData->cost,
            'shipping_price' => null,
            'cart_item_id' => $cartItemId,
        ]);

        $orderItemId = DB::getPdo()->lastInsertId();
        
        return $orderItemId;
    }

    protected function saveCartItem($oldData, $createdDate)
    {
        try {
            $productId = $this->getProductId($oldData->item_id);
            $cartItem = $this->getCartItem($oldData->cart_item_id);

            $shippingMethod = 'custom';
            if ($oldData->own_account == 2) {
                $shippingMethod = 'outside_us';
            } elseif ($oldData->own_account == 0) {
                $shippingMethod = 'us_shipping';
            } elseif ($oldData->own_account == 1) {
                $shippingMethod = 'own_account';
            } elseif ($oldData->own_account == 3) {
                $shippingMethod = 'custom';
            }

            $receivedDate = new Carbon($oldData->receive_date);


            if ($cartItem) {
                DB::table('cart_items')->insert([
                    'product_id' => $productId,
                    'quantity' => $oldData->count,
                    'regular_price' => $cartItem->incart_price,
                    'price' => $cartItem->incart_price,
                    'is_sale' => $cartItem->incart_price_field == 'price_sale' ? 1 : 0,
                    'later_size_breakdown' => 0,
                    'imprint_comment' => $oldData->imprint_comments,
                    'tax_exemption' => $oldData->tax_exempt,
                    'estimation_zip' => $oldData->estimate_zip,
                    'estimation_shipping_method' => $oldData->shipping_est_method,
                    'estimation_shipping_code' => $oldData->shipping_est_code,
                    'estimation_shipping_price' => $oldData->shipping_est_cost,
                    'own_shipping_type' => $oldData->own_shipping_method,
                    'own_account_number' => $oldData->own_account_number,
                    'own_shipping_system' => $oldData->own_shipping_system,
                    'shipping_method' => $shippingMethod,
                    'received_date' => $receivedDate,
                    'created_at' => $createdDate,
                    'updated_at' => $createdDate,
                    'cart_id' => null,
                    'setup_price' => null,
                ]);
            } else {
                DB::table('cart_items')->insert([
                    'product_id' => $productId,
                    'quantity' => $oldData->count,
                    'later_size_breakdown' => 0,
                    'imprint_comment' => $oldData->imprint_comments,
                    'tax_exemption' => $oldData->tax_exempt,
                    'estimation_zip' => $oldData->estimate_zip,
                    'estimation_shipping_method' => $oldData->shipping_est_method,
                    'estimation_shipping_price' => $oldData->shipping_est_cost,
                    'own_shipping_type' => $oldData->own_shipping_method,
                    'own_account_number' => $oldData->own_account_number,
                    'own_shipping_system' => $oldData->own_shipping_system,
                    'shipping_method' => $shippingMethod,
                    'received_date' => $receivedDate,
                    'created_at' => $createdDate,
                    'updated_at' => $createdDate,
                    'cart_id' => null,
                    'setup_price' => 0,
                ]);
            }

            return DB::getPdo()->lastInsertId();
        } catch (\Exception $e) {

            return false;
        }
    }

    /**
     * @param array $oldProducts
     */
    private function syncList($oldOrders)
    {
        $pdo = DB::getPdo();
        $pdo->beginTransaction();

        foreach ($oldOrders as $oldOrder) {

            //get customer
            $oldCustomer = $this->getCustomer($oldOrder->customer_id);
            if(empty($oldCustomer)) {
                continue;
            }

            $userId = $this->saveOrGetCustomerId($oldCustomer);

            $orderId = $this->saveOrder($oldOrder, $userId);

            $oldOrderItems = $this->getOrderItems($oldOrder->order_id);

            foreach ($oldOrderItems as $oldOrderItem)
            {
                $orderItemId = $this->saveOrderItem($oldOrderItem, $orderId, null);
            }

            echo ">>>Script has inserted order \n";
        }
        $pdo->commit();
    }
}
