<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;
use Spatie\Sitemap\SitemapIndex;
use Storage;

class CreateIndexSiteMap implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public function handle()
    {
        Storage::disk('sitemap')->delete('sitemap.xml');
        $files = Storage::disk('sitemap')->allFiles('sitemaps');
        if(empty($files)) {
            Log::info('Sitemaps folder empty');
            return;
        }

        $siteMapIndex = SitemapIndex::create();
        foreach ($files ?? [] as $file) {
            $siteMapIndex->add(Storage::disk('sitemap')->url($file));
        }

        Storage::disk('sitemap')->put("sitemap.xml", $siteMapIndex->render());
    }
}
