<?php

namespace App\Notification;


use App\Contracts\EmailService;
use App\Exceptions\ConfigurationException;
use App\Exceptions\EmailServiceException;
use App\User;
use SendGrid\Response;


class SendGrid implements EmailService
{
    protected $sendgrid = null;
    protected $listId = null;

    public function __construct()
    {
        $apiKey = env('SENDGRID_API_KEY');
        $listId = env('SENDGRID_LIST_ID');

        if (is_null($apiKey) || is_null($listId)) {
            throw new ConfigurationException('SENDGRID_API_KEY and SENDGRID_LIST_ID should be set');
        }

        $this->sendgrid = new \SendGrid($apiKey);
        $this->listId = $listId;
    }

    /** @inheritdoc */
    public function addUser(User $user): bool
    {
        $request = [[
            'email' => $user->email,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name
        ]];


        /** @var Response $response */
        /** @noinspection PhpUndefinedMethodInspection */
        /*
         * The contactdb is a database of your contacts for SendGrid Marketing Campaigns.
         * This endpoint allows you to add a Marketing Campaigns recipient.
         */
        $response = $this->sendgrid->client->contactdb()->recipients()->post($request);
        $body = json_decode($response->body(), true);


        if ($body['error_count'] > 0) {
            throw new EmailServiceException(json_encode($body['errors']));
        }

        $recipients = $body['persisted_recipients'];

        /** @var Response $response */
        /** @noinspection PhpUndefinedMethodInspection */
        /**
         * This endpoint allows you to add multiple recipients to a list.
         * Adds existing recipients to a list, passing in the recipient IDs to add.
         */
        $response = $this->sendgrid->client->contactdb()->lists()->_($this->listId)->recipients()->post([$recipients[0]]);

        return $response->statusCode() == 201;
    }
}
