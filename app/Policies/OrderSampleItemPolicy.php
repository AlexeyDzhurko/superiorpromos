<?php

namespace App\Policies;

use App\Models\OrderSampleItem;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderSampleItemPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the order a sample item.
     *
     * @param User $user
     * @param OrderSampleItem $orderSampleItem
     * @return bool
     */
    public function view(User $user, OrderSampleItem $orderSampleItem)
    {
        if (is_null($orderSampleItem->orderSample->user_id)) {
            return false;
        }

        return $orderSampleItem->orderSample->user_id == $user->id;
    }
}
