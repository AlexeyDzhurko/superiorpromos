<?php

namespace App\Policies;

use App\User;
use App\Models\SavedCartItem;
use Illuminate\Auth\Access\HandlesAuthorization;

class SavedCartItemPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the savedCartItem.
     *
     * @param  \App\User  $user
     * @param  \App\Models\SavedCartItem  $savedCartItem
     * @return mixed
     */
    public function update(User $user, SavedCartItem $savedCartItem)
    {
        return $savedCartItem->user_id == $user->id;
    }

    /**
     * Determine whether the user can delete the savedCartItem.
     *
     * @param  \App\User  $user
     * @param  \App\Models\SavedCartItem  $savedCartItem
     * @return mixed
     */
    public function delete(User $user, SavedCartItem $savedCartItem)
    {
        return $savedCartItem->user_id == $user->id;
    }
}
