<?php

namespace App\Policies;

use App\User;
use App\Models\Wish;
use Illuminate\Auth\Access\HandlesAuthorization;

class WishPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can delete the wish.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Wish  $wish
     * @return mixed
     */
    public function delete(User $user, Wish $wish)
    {
        return $wish->user_id == $user->id;
    }
}
