<?php

namespace App\Policies;

use App\User;
use App\Models\ArtProof;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArtProofPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the artProof.
     *
     * @param  \App\User  $user
     * @param  \App\Models\ArtProof  $artProof
     * @return mixed
     */
    public function view(User $user, ArtProof $artProof)
    {
        return $artProof->orderItem->order->user_id == $user->id;
    }

    /**
     * Determine whether the user can update the artProof.
     *
     * @param  \App\User  $user
     * @param  \App\Models\ArtProof  $artProof
     * @return mixed
     */
    public function update(User $user, ArtProof $artProof)
    {
        return $artProof->orderItem->order->user_id == $user->id;
    }
}
