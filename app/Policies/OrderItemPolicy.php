<?php

namespace App\Policies;

use App\Models\OrderItem;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderItemPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the order item.
     *
     * @param User $user
     * @param OrderItem $orderItem
     * @return bool
     */
    public function view(User $user, OrderItem $orderItem)
    {
        if (is_null($orderItem->order->user_id)) {
            return false;
        }

        return $orderItem->order->user_id == $user->id;
    }

}
