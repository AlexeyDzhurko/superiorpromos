<?php

namespace App\Policies;

use App\User;
use App\Models\CartItem;
use Illuminate\Auth\Access\HandlesAuthorization;
use Session;

class CartItemPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the cartItem.
     *
     * @param  \App\User  $user
     * @param  \App\Models\CartItem  $cartItem
     * @return mixed
     */
    public function view(User $user, CartItem $cartItem)
    {
        if (empty($cartItem->cart->user_id)) {
            //todo: check cookie
            return true;
        }

        return $cartItem->cart->user_id == $user->id;
    }

}
