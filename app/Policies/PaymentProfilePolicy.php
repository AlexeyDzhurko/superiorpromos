<?php

namespace App\Policies;

use App\User;
use App\PaymentProfile;
use Illuminate\Auth\Access\HandlesAuthorization;

class PaymentProfilePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the payment profile.
     *
     * @param  \App\User  $user
     * @param  \App\PaymentProfile  $paymentProfile
     * @return mixed
     */
    public function update(User $user, PaymentProfile $paymentProfile)
    {
        return $user->id === $paymentProfile->user_id;
    }

    /**
     * Determine whether the user can delete the payment profile.
     *
     * @param  \App\User  $user
     * @param  \App\PaymentProfile  $paymentProfile
     * @return mixed
     */
    public function delete(User $user, PaymentProfile $paymentProfile)
    {
        return $user->id === $paymentProfile->user_id;
    }
}
