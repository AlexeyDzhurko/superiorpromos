<?php

namespace App\Policies;

use App\User;
use App\Models\UserNote;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserNotePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the user note.
     *
     * @param  \App\User  $user
     * @param  \App\Models\UserNote  $userNote
     * @return mixed
     */
    public function update(User $user, UserNote $userNote)
    {
        return $userNote->orderItem->order->user_id == $user->id;
    }
}
