<?php

namespace App\Policies;

use App\Models\OrderSample;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderSamplePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the OrderSample.
     *
     * @param  \App\User  $user
     * @param  \App\Models\OrderSample $orderSample
     * @return mixed
     */
    public function view(User $user, OrderSample $orderSample)
    {
        if (is_null($orderSample->user_id)) {
            return false;
        }

        return $orderSample->user_id == $user->id;
    }

}
