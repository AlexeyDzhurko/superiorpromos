<?php

namespace App;

use App\Models\Order;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Collection;
use App\Search\Searchable;

/**
 * App\User
 *
 * @SWG\Definition (
 *      definition="User",
 *      @SWG\Property(property="name", type="string", example="John Smith"),
 *      @SWG\Property(property="email", type="string", example="admin@admin.com"),
 *      @SWG\Property(property="password", type="string", example="password"),
 *      @SWG\Property(property="avatar", type="string", example="users/default.png"),
 *      @SWG\Property(property="remember_token", type="integer", example="S6RhZbj3jsTiSUO0g7XiXvI3kDPDIHqqzOT4XPXhGdbyXK58Ie4DOGbMFD3r"),
 *      @SWG\Property(property="contact_telephone", type="string", example="111-222-3333"),
 *      @SWG\Property(property="ext", type="string", example="123"),
 *      @SWG\Property(property="fax", type="string", example="321-123-4567"),
 *      @SWG\Property(property="authorize_net_id", type="string", example="12"),
 *      @SWG\Property(property="certificate", type="string", example="http://local.dev/certificates/23jkj2j23.png"),
 *      @SWG\Property(property="addresses", type="array", items=@SWG\Schema(ref="#/definitions/Address")),
 *      @SWG\Property(property="payment_profiles", type="array", items=@SWG\Schema(ref="#/definitions/PaymentProfile")),
 * ),
 * 
 *  * @SWG\Definition (
 *      definition="RegisterUser",
 *      @SWG\Property(property="name", type="string", example="John Smith"),
 *      @SWG\Property(property="email", type="string", example="admin@admin.com"),
 *      @SWG\Property(property="password", type="string", example="password"),
 *      @SWG\Property(property="avatar", type="string", example="users/default.png"),
 *      @SWG\Property(property="remember_token", type="integer", example="S6RhZbj3jsTiSUO0g7XiXvI3kDPDIHqqzOT4XPXhGdbyXK58Ie4DOGbMFD3r"),
 *      @SWG\Property(property="contact_telephone", type="string", example="111-222-3333"),
 *      @SWG\Property(property="ext", type="string", example="123"),
 *      @SWG\Property(property="authorize_net_id", type="string", example="12"),
 *      @SWG\Property(property="certificate", type="string", example="http://local.dev/certificates/23jkj2j23.png"),
 *      @SWG\Property(property="payment_profiles", type="array", items=@SWG\Schema(ref="#/definitions/PaymentProfile")),
 * ),
 * 
 * Class User
 * @package App
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $avatar
 * @property string $remember_token
 * @property string $contact_telephone
 * @property string $ext
 * @property string $fax
 * @property int $authorize_net_id
 * @property string $certificate
 * @property Collection $payment_profiles
 * @property Collection $addresses
 * @property boolean $active
 * @property boolean $can_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $readNotifications
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $unreadNotifications
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereAvatar($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereContactTelephone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereExt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereFax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereAuthorizeNetId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCertificate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCanDelete($value)
 * @property boolean $subscribe
 * @property boolean $tax_exempt
 * @method static \Illuminate\Database\Query\Builder|\App\User whereSubscribe($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereTaxExempt($value)
 * @property-read \App\Models\Order $orders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 */
class User extends Authenticatable implements Searchable
{
    use Notifiable;

    const CERT_DIR = 'certificates/';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'contact_telephone', 'ext', 'fax', 'authorize_net_id', 'active', 'subscribe', 'tax_exempt', 'fax', 'can_delete'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payment_profiles()
    {
        return $this->hasMany('App\PaymentProfile');
    }

    public function hasRole($name)
    {
        return in_array($name, array_pluck($this->roles->toArray(), 'name'));
    }

    public function addRole($name)
    {
        // If user does not already have this role
        if (!$this->hasRole($name)) {
            // Look up the role and attach it to the user
            $role = Role::where('name', '=', $name)->first();
            $this->roles()->attach($role->id);
        }
    }

    public function deleteRole($name)
    {
        // If user has this role
        if ($this->hasRole($name)) {
            // Lookup the role and detach it from the user
            $role = Role::where('name', '=', $name)->first();
            $this->roles()->detach($role->id);
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addresses()
    {
        return $this->hasMany('App\Models\Address');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'user_roles', 'user_id', 'role_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function getElasticDocument(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'contact_telephone' => $this->contact_telephone,
            'active' => $this->active,
            'created_at' => $this->created_at->toIso8601String(),
            'addresses' => $this->addresses,
            'authorize_net_id' => $this->authorize_net_id,
            'orders_count' => $this->orders()->count(),
            'roles' => $this->roles
        ];
    }

    public static function getElasticMapping(): array
    {
        return [
            'id' => [
                'type' => 'integer',
                'include_in_all' => FALSE
            ],
            'authorize_net_id' => [
                'type' => 'integer',
                'include_in_all' => FALSE
            ],
            'name' => [
                'type' => 'string',
                'analyzer' => 'standard',
                'fields' => [
                    'sort' => [
                        'type' => 'string',
                        'index' => 'not_analyzed',
                    ],
                ],
                'fielddata' => true,
            ],
            'email' => [
                'type' => 'string',
                'analyzer' => 'standard',
                'fields' => [
                    'sort' => [
                        'type' => 'string',
                        'index' => 'not_analyzed',
                    ],
                ],
                'fielddata' => true,
            ],
            'contact_telephone' => [
                'type' => 'string',
                'analyzer' => 'standard',
                'fielddata' => true,
                'fields' => [
                    'sort' => [
                        'type' => 'string',
                        'index' => 'not_analyzed',
                    ],
                ],
            ],
            'active'=> [
                'type' => 'boolean',
            ],
            'created_at'=> [
                'type' => 'date',
            ],
            'name_sort' => [
                'type' => 'string',
                'index' => 'not_analyzed',
            ],
            'orders_count' => [
                'type' => 'integer',
                'include_in_all' => FALSE
            ],
            'addresses' => [
                'type' => 'nested',
                'properties' => [
                    'id' => [
                        'type' => 'integer',
                    ],
                    'company_name' => [
                        'type' => 'string',
                    ],
                    'zip' => [
                        'type' => 'string',
                    ],
                    'type' => [
                        'type' => 'integer',
                    ],
                    'day_telephone' => [
                        'type' => 'string',
                    ],
                    'fax' => [
                        'type' => 'string',
                    ]
                ]
            ],
            'roles' => [
                'type' => 'nested',
                'properties' => [
                    'id' => [
                        'type' => 'integer',
                    ]
                ]
            ]
        ];
    }

    public static function getElasticSortRows(): array
    {
        return [
            'name',
            'email',
            'contact_telephone'
        ];
    }

    public static function getIndexName(): string
    {
        return 'user';
    }

    /**
     * @param $taxExemptionCertificate
     *
     * @return string|null
     */
    public function getTaxExemptionCertificateAttribute($taxExemptionCertificate)
    {
        return null != $taxExemptionCertificate ? asset('storage/' . config('superiorpromos.storage.tax_exemption_certificates_path') . $taxExemptionCertificate) : null;
    }
}
