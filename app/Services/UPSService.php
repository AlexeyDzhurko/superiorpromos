<?php

namespace App\Services;

use App\Models\Product;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Ups\Entity\AddressArtifactFormat;
use Ups\Entity\InvoiceLineTotal;
use Ups\Entity\RatedShipment;
use Ups\Entity\RateResponse;
use Ups\Entity\ServiceSummary;
use Ups\Entity\ShipmentWeight;
use Ups\Entity\TimeInTransitRequest;
use Ups\Entity\TimeInTransitResponse;
use Ups\Rate;
use Ups\Entity\UnitOfMeasurement;
use Ups\Entity\Package;
use Ups\Entity\Shipment;
use Ups\Entity\PackagingType;
use App\Contracts\ShippingService;
use Ups\TimeInTransit;

class UPSService implements ShippingService
{
    /**
     * @var Rate
     */
    protected $rate;

    /**
     * @var string
     */
    protected $countryCode;

    protected $upsCodesMapping = [
        '1DM'=>'14',
        '1DA'=>'01',
        '1DP'=>'13',
        '2DM'=>'59',
        '2DA'=>'02',
        '3DS'=>'12',
        'GND'=>'03'
    ];

    protected $availableServices = [
        '01',
        '02',
        '03',
        '12',
        '13'
    ];

    /**
     * ShippingService constructor.
     */
    public function __construct()
    {
        $this->countryCode = 'US';
        $this->rate = new Rate(
            env('UPS_ACCESS_KEY'),
            env('UPS_USER_ID'),
            env('UPS_PASSWORD')
        );
    }

    /**
     * @param int $zipCode
     * @param Product $product
     * @param int $quantity
     * @return \Illuminate\Support\Collection
     */
    public function getShippingRate(int $zipCode, Product $product, int $quantity): Collection
    {
        $senderZip = $product->zip_from ?? Setting::setting('sender_zip');
        $boxCount = ceil($quantity / $product->quantity_per_box);

        $rates = $this->shipmentRate($zipCode, $senderZip, $product, $boxCount);
        $timesInTransit = $this->timeInTransitByService($zipCode, $senderZip, $product->box_weight * $boxCount, $boxCount);

        $result = [];
        foreach ($timesInTransit as $code => $businessTransitDays) {
            if (isset($this->upsCodesMapping[$code]) && isset($rates[$this->upsCodesMapping[$code]])) {

                $rate = $rates[$this->upsCodesMapping[$code]];
                if (!in_array($this->upsCodesMapping[$code], $this->availableServices)) {
                    continue;
                }

                $result[] = [
                    'service' => $rate['service'],
                    'cost' => $rate['cost'],
                    'time_in_transit' => $businessTransitDays,
                    'code' => $code,
                ];
            }
        }

        return collect($result)->sortBy('cost')->values();
    }

    protected function shipmentRate($zip, $senderZip, Product $product, $boxCount)
    {
        $shipment = new Shipment();

        $shipperAddress = $shipment->getShipper()->getAddress();
        $shipperAddress->setPostalCode($senderZip);
        $shipperAddress->setCountryCode($this->countryCode);

        $shipTo = $shipment->getShipTo();
        $shipToAddress = $shipTo->getAddress();
        $shipToAddress->setPostalCode($zip);
        $shipToAddress->setCountryCode($this->countryCode);

        for ($i = 0; $i < $boxCount; $i++) {
            $package = new Package();
            $package->getPackagingType()->setCode(PackagingType::PT_PACKAGE);
            $package->getPackageWeight()->setWeight($product->box_weight ?? 1);
            $shipment->addPackage($package);
        }

        /** @var RateResponse $rates */
        $rates = $this->rate->shopRates($shipment);

        $result = [];
        foreach ($rates->RatedShipment as $ratedShipment) { /** @var RatedShipment $ratedShipment */
            $result[$ratedShipment->Service->getCode()] = [
                'service' => $ratedShipment->Service->getName(),
                'cost' => $ratedShipment->TotalCharges->MonetaryValue,
            ];
        }
        return $result;
    }

    protected function timeInTransitByService($zip, $senderZip, $totalWeight, $boxCount)
    {
        $timeInTransit = new TimeInTransit(env('UPS_ACCESS_KEY'), env('UPS_USER_ID'), env('UPS_PASSWORD'));

        $timeInTransitRequest = new TimeInTransitRequest();

        $from = new AddressArtifactFormat();
        $from->setCountryCode('US');
        $from->setPostcodePrimaryLow($senderZip);

        $to = new AddressArtifactFormat();
        $to->setCountryCode('US');
        $to->setPostcodePrimaryLow($zip);

        $timeInTransitRequest->setTransitFrom($from);
        $timeInTransitRequest->setTransitTo($to);

        $timeInTransitRequest->setPickupDate((new Carbon())->modify('+1 day'));

        $shipmentWeight = new ShipmentWeight();
        $shipmentWeight->setWeight($totalWeight);
        $unit = new UnitOfMeasurement();
        $unit->setCode(UnitOfMeasurement::UOM_LBS);
        $shipmentWeight->setUnitOfMeasurement($unit);
        $timeInTransitRequest->setShipmentWeight($shipmentWeight);
        $timeInTransitRequest->setTotalPackagesInShipment((int)$boxCount);

        $invoiceLineTotal = new InvoiceLineTotal();
        $invoiceLineTotal->setMonetaryValue(100.00);
        $invoiceLineTotal->setCurrencyCode('USD');
        $timeInTransitRequest->setInvoiceLineTotal($invoiceLineTotal);

        /** @var TimeInTransitResponse $times */
        $times = $timeInTransit->getTimeInTransit($timeInTransitRequest);

        $result = [];
        foreach ($times->ServiceSummary as $serviceSummary) { /** @var ServiceSummary $serviceSummary */
            $result[$serviceSummary->Service->getCode()] = $serviceSummary->EstimatedArrival->BusinessTransitDays;
        }

        return $result;
    }

}
