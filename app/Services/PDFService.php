<?php


namespace App\Services;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class PDFService
{
    public static function generatePdf($view, array $data)
    {
        try {
            $view = View::make($view, $data)->render();
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($view)->setPaper('a4');

            return $pdf->stream();
        }catch (\Exception $exception) {
            return false;
        }
    }
}
