<?php

namespace App\Services\UpsShippingCostCalculation\Entity;

/**
 * Class Package
 * @package App\Services\UpsShippingCostCalculation\Entity
 */
class Package
{
    /**
     * @var int
     */
    private $productId;

    /**
     * @var int
     */
    private $count;

    /**
     * @var string
     */
    private $zipCode;

    /**
     * @var string
     */
    private $zipFrom;

    /**
     * @var float
     */
    private $fullBoxWeight;

    /**
     * @var int
     */
    private $qtyPerFullBox;

    /**
     * @var string
     */
    private $shippingFeeType;

    /**
     * @var float
     */
    private $shippingFeeValue;

    /**
     * @var array
     */
    private $shippData;

    /**
     * @var array
     */
    private $senderData;

    /**
     * Package constructor.
     * @param int $productId
     * @param int $count
     * @param string $zipCode
     * @param string $zipFrom
     * @param float $fullBoxWeight
     * @param int $qtyPerFullBox
     * @param string $shippingFeeType
     * @param float $shippingFeeValue
     */
    public function __construct(
        int $productId,
        int $count,
        string $zipCode,
        string $zipFrom,
        float $fullBoxWeight,
        int $qtyPerFullBox,
        string $shippingFeeType,
        float $shippingFeeValue
    )
    {
        $this->setProductId($productId);
        $this->setCount($count);
        $this->setZipCode($zipCode);
        $this->setZipFrom($zipFrom);
        $this->setFullBoxWeight($fullBoxWeight);
        $this->setQtyPerFullBox($qtyPerFullBox);
        $this->setShippingFeeType($shippingFeeType);
        $this->setShippingFeeValue($shippingFeeValue);
    }

    /**
     * @param int $count
     */
    public function setCount(int $count)
    {
        $this->count = $count;
    }

    /**
     * @param int $productId
     */
    public function setProductId(int $productId)
    {
        $this->productId = $productId;
    }

    /**
     * @param string $zipCode
     */
    public function setZipCode(string $zipCode)
    {
        $this->zipCode = $zipCode;

        $this->setShippData($zipCode);
    }

    /**
     * @param string $zipFrom
     */
    public function setZipFrom(string $zipFrom)
    {
        $this->zipFrom = $zipFrom;

        $this->setSenderData($zipFrom);
    }

    /**
     * @param float $fullBoxWeight
     */
    public function setFullBoxWeight(float $fullBoxWeight)
    {
        $this->fullBoxWeight = $fullBoxWeight;
    }

    /**
     * @param int $qtyPerFullBox
     */
    public function setQtyPerFullBox(int $qtyPerFullBox)
    {
        $this->qtyPerFullBox = $qtyPerFullBox;
    }

    /**
     * @param string $shippingFeeType
     */
    public function setShippingFeeType(string $shippingFeeType)
    {
        $this->shippingFeeType = $shippingFeeType;
    }

    /**
     * @param float $shippingFeeValue
     */
    public function setShippingFeeValue(float $shippingFeeValue)
    {
        $this->shippingFeeValue = $shippingFeeValue;
    }

    /**
     * @param string $zipCode
     */
    private function setShippData(string $zipCode)
    {
        $this->shippData = [
            'zip'     => $zipCode,
            'country' => 'US',
        ];
    }

    /**
     * @param string $zipFrom
     */
    private function setSenderData(string $zipFrom)
    {
        $this->senderData = [
            'zip'     => $zipFrom,
            'country' => 'US',
        ];
    }

    /**
     * @return int
     */
    public function getCount() : int
    {
        return $this->count;
    }

    /**
     * @return int
     */
    public function getProductId() : int
    {
        return $this->productId;
    }

    /**
     * @return string
     */
    public function getZipCode() : string
    {
        return $this->zipCode;
    }

    /**
     * @return string
     */
    public function getZipFrom() : string
    {
        return $this->zipFrom;
    }

    /**
     * @return float
     */
    public function getFullBoxWeight() : float
    {
        return $this->fullBoxWeight;
    }

    /**
     * @return int
     */
    public function getQtyPerFullBox() : int
    {
        return $this->qtyPerFullBox;
    }

    /**
     * @return string
     */
    public function getShippingFeeType() : string
    {
        return $this->shippingFeeType;
    }

    /**
     * @return float
     */
    public function getShippingFeeValue() : float
    {
        return $this->shippingFeeValue;
    }

    /**
     * @return array
     */
    public function getShippData() : array
    {
        return $this->shippData;
    }

    /**
     * @return array
     */
    public function getSenderData() : array
    {
        return $this->senderData;
    }

    /**
     * @return float
     */
    public function getBoxes() : float
    {
        return ceil($this->count / $this->qtyPerFullBox);
    }


    /**
     * @return array
     */
    public function getBoxesWeightArray() : array
    {
        $fullBox = floor($this->count / $this->qtyPerFullBox);
        $partBox = ($this->count % $this->qtyPerFullBox)  / $this->qtyPerFullBox;
        $result = array_fill(0, $fullBox, $this->getFullBoxWeight());
        if(!empty($partBox)) $result[] = $partBox * $this->getFullBoxWeight();

        return $result;
    }
}
