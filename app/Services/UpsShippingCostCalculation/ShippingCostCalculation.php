<?php

namespace App\Services\UpsShippingCostCalculation;

use App\Contracts\ShippingService;
use App\Models\Product;
use App\Services\UpsShippingCostCalculation\Entity\Package;
use Illuminate\Support\Collection;

/**
 * Class ShippingCostCalculation
 * @package App\Services\UpsShippingCostCalculation
 */
class ShippingCostCalculation implements ShippingService
{
    const SHIPPER_ZIP = 11229;
    const UPS_HOST = 'onlinetools.ups.com';
    const UPS_API_RATE_URL = '/ups.app/xml/Rate';
    const UPS_API_TIME_IN_TRANSIT_URL = '/ups.app/xml/TimeInTransit';
    const SHIPPING_ADDITION = 2.50;
    const SUPS_SERVICES_CODES_ALLOWED = ['01', '02', '03', '12', '13'];
    const UPS_SERVICES_CODES = [
        '1DM' => '14',
        '1DA' => '01',
        '1DP' => '13',
        '2DM' => '59',
        '2DA' => '02',
        '3DS' => '12',
        'GND' => '03'
    ];

    /**
     * @var Package
     */
    private $package;

    /**
     * @param int $zipCode
     * @param Product $product
     * @param int $count
     * @return Collection
     */
    public function getShippingRate($zipCode, Product $product, int $count): Collection
    {
        try {
            $this->fillPackage($product, $count, $zipCode);

            $shippingCostData = $this->getShippingCostData();
            $timeInTransitData = $this->getTimeInTransitData();

            return $this->formingCalculatedCost($shippingCostData, $timeInTransitData);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return Collection::make($e->getMessage());
        }
    }

    /**
     * @param Product $product
     * @param int $count
     * @param int $zipCode
     */
    public function fillPackage(Product $product, int $count, $zipCode)
    {
        $this->package = new Package(
            $product->id,
            $count,
            $zipCode,
            empty($product->zip_from) ? $this::SHIPPER_ZIP : $product->zip_from,
            $product->box_weight,
            $product->quantity_per_box,
            $product->shipping_additional_type,
            $product->shipping_additional_value
        );
    }

    /**
     * @return array|bool
     */
    private function getShippingCostData(): array
    {
        $apiResponse = $this->request($this->formingDataForShippingCostRequest(), $this::UPS_API_RATE_URL);

        return $this->parseShippingCostApiResponse($apiResponse);
    }

    /**
     * @return string
     */
    private function formingDataForShippingCostRequest(): string
    {
//        return (string)\View::make('shipping.ups.get_shipping_cost', [
        return (string)\View::make('shipping.ups.get_shipping_cost_by_weight', [
            'senderData' => $this->package->getSenderData(),
            'shippData' => $this->package->getShippData(),
            'boxWeight' => $this->package->getFullBoxWeight() == 0 ? 1 : $this->package->getFullBoxWeight(),
            'boxes' => $this->package->getBoxesWeightArray(),
        ]);
    }

    /**
     * @param bool $shippData
     * @param bool $raw
     * @return array
     */
    private function getTimeInTransitData($shippData = false, bool $raw = false): array
    {
        $requestData = [
            'raw' => $raw,
            'shippData' => $shippData ? $shippData : $this->package->getShippData(),
            'senderData' => $this->package->getSenderData(),
        ];

        $res = $this->request(
            $this->formingDataForTimeInTransitRequest($requestData, $shippData, $raw),
            $this::UPS_API_TIME_IN_TRANSIT_URL
        );

        if (!empty($res['TransitToList']['Candidate'])) {
            $requestData['additional'] = [
                'politicalDivision2' => $res['TransitToList']['Candidate'][0]['AddressArtifactFormat']['PoliticalDivision2'],
                'politicalDivision1' => $res['TransitToList']['Candidate'][0]['AddressArtifactFormat']['PoliticalDivision1'],
                'country' => $res['TransitToList']['Candidate'][0]['AddressArtifactFormat']['Country'],
            ];

            $res = $this->request(
                $this->formingDataForTimeInTransitRequest($requestData, $shippData, $raw),
                $this::UPS_API_TIME_IN_TRANSIT_URL
            );
        }

        return $this->parseTimeInTransitApiResponse($res);
    }

    /**
     * @param $shippData
     * @param bool $raw
     * @return string
     */
    private function formingDataForTimeInTransitRequest($data, $shippData, bool $raw): string
    {
        return (string)\View::make('shipping.ups.get_time_in_transit', $data);
    }

    /**
     * @param string $postFields
     * @param string $apiURL
     * @return string
     * @throws \Exception
     */
    private function request(string $postFields, string $apiURL): array
    {
        $ch = curl_init('https://' . $this::UPS_HOST . $apiURL);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);

        if (curl_errno($ch) != 0) {
            throw new \Exception('Occurred error while calling UPS API' . curl_error($ch));
        }

        curl_close($ch);

        $xml = simplexml_load_string((string)$result);
        $json = json_encode($xml);
        $array = json_decode($json, true);

        return ($array);
    }


    /**
     * @param array $path
     * @param array $vals
     * @param array $indx
     * @param int $first
     * @param int $last
     * @return bool
     */
    private function generateErrorMsgByXmlValue(array $path, array &$vals, array &$indx, &$first, &$last)
    {
        //Return value for $path or false if wrong path, or true if can't find, for example for uncomplete path
        //Init $first and $last
        $first = false;
        $last = false;

        foreach (array_keys($path) as $k) {
            $path[$k] = strtoupper($path[$k]);
        }

        if (!isset($indx[$path[0]])) {
            return (false);
        }

        $f = 0;
        $l = count($vals) - 1;
        $res_index = null;
        $level = 1;

        foreach ($path as $v) {
            $havestarttag = false;
            $haveendtag = false;
            $havelatesttag = false;

            for ($k = $f; $k <= $l; $k++) {

                if ((isset($vals[$k]) && $v == $vals[$k]['tag']) && ($vals[$k]['level'] == $level)) {

                    if ($vals[$k]['type'] == 'open') {
                        $havestarttag = true;
                        $f = $k;
                    }

                    if ($vals[$k]['type'] == 'close') {
                        $haveendtag = true;
                        $l = $k;
                    }

                    if ($vals[$k]['type'] == 'complete' && $vals[$k]['level'] == count($path)
                        && isset($vals[$k]['value'])) {
                        $havelatesttag = true;
                        $res = $vals[$k]['value'];
                    }
                }
            }

            if (!(($havestarttag && $haveendtag) || $havelatesttag)) {
                return (false);
            }

            $level++;
        }

        $first = $f;
        $last = $l;

        if (!$res) {
            $res = true;
        }

        return ($res);
    }

    /**
     * @param string $serviceCode
     * @return string
     */
    private function getServiceCnvByServiceCode(string $serviceCode): string
    {
        if (array_key_exists($serviceCode, $this::UPS_SERVICES_CODES)) {
            return ($this::UPS_SERVICES_CODES[$serviceCode]);
        } else {
            return ($serviceCode);
        }
    }

    /**
     * @param string $serviceId
     * @return string
     */
    private function getCodeByServiceId(string $serviceId): string
    {
        return array_search($serviceId, $this::UPS_SERVICES_CODES);
    }

    /**
     * @param string $serviceCode
     * @param string $countryCode
     * @return string
     */
    private function getUpsServiceName(string $serviceCode, string $countryCode): string
    {
        $res = false;

        $upsServicesUs = [
            '01' => 'UPS Next Day Air',
            '02' => 'UPS 2nd Day Air',
            '03' => 'UPS Ground',
            '07' => 'UPS Worldwide Express',
            '08' => 'UPS Worldwide Expedited',
            '11' => 'UPS Standard',
            '12' => 'UPS 3 Day Select',
            '13' => 'UPS Next Day Air Saver',
            '14' => 'UPS Next Day Air. Early A.M.',
            '54' => 'UPS Worldwide Express Plus',
            '59' => 'UPS 2nd Day Air A.M.',
            '64' => 'Discontinued',
            '65' => 'UPS Express Saver',
        ];

        $upsServicesOther = [
            '01' => 'UPS Express',
            '02' => 'UPS Expedited',
            '13' => 'UPS Express Saver',
            '14' => 'UPS Express Early A.M.',
        ];

        if ($countryCode == 'US') {
            if (isset($upsServicesUs[$serviceCode])) {
                $res = $upsServicesUs[$serviceCode];
            }
        } else {
            if (isset($upsServicesOther[$serviceCode])) {
                $res = $upsServicesOther[$serviceCode];
            } elseif (isset($upsServicesUs[$serviceCode])) {
                $res = $upsServicesUs[$serviceCode];
            }
        }

        return ($res);
    }

    /**
     * @param array $shippingCostData
     * @param array $timeInTransitData
     * @return Collection
     */
    private function formingCalculatedCost(array $shippingCostData, array $timeInTransitData): Collection
    {
        $res = [];

        foreach ($shippingCostData as $k => $v) {

            if (array_key_exists($k, $timeInTransitData)) {

                if ($this->package->getShippingFeeType() == 'percentage') {
                    $scost = $v[1] + ($v[1] * $this->package->getShippingFeeValue() / 100) + $this::SHIPPING_ADDITION;

                } elseif ($this->package->getShippingFeeType() == 'absolute' || (empty($this->package->getShippingFeeType()) || is_null($this->package->getShippingFeeType()))) {
                    $scost = $v[1] + $this->package->getShippingFeeValue()
                        * $this->package->getBoxes() + $this::SHIPPING_ADDITION;
                }

                if (array_search($k, $this::SUPS_SERVICES_CODES_ALLOWED) !== false) {
                    $ttime = '&nbsp;';

                    if (!empty($timeInTransitData[$k])) {
                        $ttime = $timeInTransitData[$k];
                    }

                    $res[] = [
                        'service' => $this->getUpsServiceName($k, 'US'),
                        'cost' => round($scost, 2),
                        'time_in_transit' => $ttime,
                        'code' => $this->getCodeByServiceId($k),
                    ];
                }
            }
        }

        return Collection::make($res)->sortBy('cost')->values();
    }

    /**
     * @param array $apiResponse
     * @return array|bool
     * @throws \Exception
     */
    private function parseShippingCostApiResponse(array $apiResponse): array
    {
        $ratesIndex = [];

        if (!empty($apiResponse['RatedShipment'])) {
            $ratedShipmentData = $apiResponse['RatedShipment'];

            foreach ($ratedShipmentData as $data) {
                $serviceCode = $data['Service']['Code'];

                if (!in_array($serviceCode, self::SUPS_SERVICES_CODES_ALLOWED)) {
                    continue;
                }

                $cost = $data['TotalCharges']['MonetaryValue'];
                $timeTransit = $data['GuaranteedDaysToDelivery'];

                $ratesIndex[$serviceCode] = [$serviceCode, $cost, $timeTransit];
            }
        } else {
            throw new \Exception("Can't extract shipping information");
        }

        return ($ratesIndex);
    }

    /**
     * @param string $apiResponse
     * @return array
     * @throws \Exception
     */
    private function parseTimeInTransitApiResponse(array $apiResponse): array
    {
        if (!empty($apiResponse['TransitResponse']['ServiceSummary'])) {
            $data = $apiResponse['TransitResponse']['ServiceSummary'];
            $ret = [];

            foreach ($data as $item) {
                $code = $item['Service']['Code'];
                $srvcode = $this->getServiceCnvByServiceCode($code);
                $estimatedArrival = $item['EstimatedArrival']['BusinessTransitDays'];
                if (!empty($srvcode)) {
                    $ret[$srvcode] = $estimatedArrival;
                };
            }

            return $ret;
        } else {
            throw new \Exception('Invalid zip code please contact customer service for details');
        }
    }
}
