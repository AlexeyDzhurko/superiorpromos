<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 07.03.17
 * Time: 15:52
 */

namespace App\Services;


use App\Contracts\CartManager;
use App\Contracts\ShippingService;
use App\Exceptions\OrderException;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\CartItemArtProof;
use App\Models\CartItemColor;
use App\Models\CartItemImprint;
use App\Models\CartItemImprintColor;
use App\Models\CartItemProductOption;
use App\Models\CartItemProductSubOption;
use App\Models\CartItemSize;
use App\Models\CartItemSizeGroup;
use App\Models\Color;
use App\Models\Imprint;
use App\Models\ImprintPrice;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductColorGroup;
use App\Models\ProductOption;
use App\Models\ProductOptionPrice;
use App\Models\ProductPrice;
use App\Models\ProductSubOption;
use App\Models\ProductSubOptionPrice;
use App\Models\Size;
use Illuminate\Database\Eloquent\Collection;
use Auth;
use Illuminate\Http\Request;
use Storage;

class DatabaseCart implements CartManager
{
    protected $shippingService;

    public function __construct(ShippingService $shippingService)
    {
        $this->shippingService = $shippingService;
    }

    public function recalculateCart(CartItem &$cartItem)
    {
        $productOptionsData = [];
        foreach ($cartItem->cartItemProductOptions as $cartItemProductOption) { /** @var CartItemProductOption $cartItemProductOption */

            $productOptionsData[] = [
                'product_option_id' => $cartItemProductOption->productOption->id,
                'product_sub_option_ids' => $cartItemProductOption->cartItemProductSubOptions->pluck('product_sub_option_id')->toArray(),
            ];
        }

        $imprintsData = [];
        foreach ($cartItem->cartItemImprints as $cartItemImprint) { /** @var CartItemImprint $cartItemImprint */
            $imprintsData[] = [
                'imprint_id' => $cartItemImprint->imprint_id,
                'color_ids' => $cartItemImprint->cartItemImprintColors->pluck('color_id')->toArray(),
            ];
        }

        $this->setCartItemProductOptions($cartItem, $cartItem->product, $productOptionsData, $cartItem->quantity);
        $this->setCartItemImprints($cartItem, $cartItem->product, $imprintsData, $cartItem->quantity);

        if ($cartItem->shipping_method == 'us_shipping') {
            $this->setCartItemShippingCost($cartItem,
                $cartItem->product,
                $cartItem->estimation_zip,
                $cartItem->quantity,
                $cartItem->estimation_shipping_code
            );
        }

    }

    public function buildCart(
        CartItem &$cartItem,
        Product $product,
        Request $request
    )
    {
        $quantity = $request->get('quantity');
        $productsColors = json_decode($request->get('product_colors'), true);
        $productOptions = json_decode($request->get('product_options'), true);
        $productImprints = json_decode($request->get('product_imprints'), true);
        $productSizes = $request->later_size_breakdown == 0 ? json_decode($request->get('product_sizes'), true) : null;
        $shippingMethod = $request->get('shipping_method');
        $ownShippingType = $request->get('own_shipping_type');
        $ownShippingNumber = $request->get('own_account_number');
        $ownShippingSystem = $request->get('own_shipping_system');
        $estimationZip = $request->get('estimation_zip');
        $estimationShippingCode = $request->get('estimation_shipping_code');
        $artFiles = $request->file('art_files') ?? null;

        $cartItem->product()->associate($product);
        $this->setCartItemCost($cartItem, $product, $quantity);
        $this->setCartItemOwner($cartItem, $request);
        $cartItem->save();

        $this->setCartItemColors($cartItem, $product, $productsColors);
        $this->setCartItemProductOptions($cartItem, $product, $productOptions, $quantity);
        $this->setCartItemImprints($cartItem, $product, $productImprints, $quantity);
        $this->setCartItemSizes($cartItem, $product, $productSizes, $quantity);
        $this->setCartItemArtFiles($cartItem, $artFiles);


        switch ($shippingMethod) {
            case 'outside_us':
                break;
            case 'own_account':
                $this->setCartItemOwnAccountFields(
                    $cartItem,
                    $ownShippingType,
                    $ownShippingNumber,
                    $ownShippingSystem
                );
                break;
            case 'us_shipping':
                $this->setCartItemShippingCost($cartItem,
                    $product,
                    $estimationZip,
                    $quantity,
                    $estimationShippingCode
                );
                break;
            case 'custom':
                $this->setCustomCartItemShippingCost($cartItem, $product);
                break;
        }

        $cartItem->save();
    }

    public function setCartItemOwnAccountFields(CartItem &$cartItem, $type, $number, $system)
    {
        $cartItem->own_shipping_type = $type;
        $cartItem->own_account_number = $number;
        $cartItem->own_shipping_system = $system;
    }

    public function setCartItemShippingCost(CartItem &$cartItem, Product $product, $zip, int $quantity, $serviceCode)
    {
        if ($zip && $quantity) {
            $rates = $this->shippingService->getShippingRate($zip, $product, $quantity);
        } else {
            $rates = [];
        }

        $rateKey = collect($rates)->search(function ($item) use ($serviceCode) {
            return $item['code'] == $serviceCode;
        });

        if ($rateKey !== false) {
            $cartItem->estimation_shipping_method = $rates[$rateKey]['service'];
            $cartItem->estimation_shipping_code = $serviceCode;
            $cartItem->estimation_shipping_price = $rates[$rateKey]['cost'];
            $cartItem->estimation_zip = $zip;
        } else {
            $cartItem->estimation_shipping_method = null;
            $cartItem->estimation_shipping_code = null;
            $cartItem->estimation_shipping_price = null;
            $cartItem->estimation_zip = null;
        }
    }

    public function setCustomCartItemShippingCost(CartItem &$cartItem, Product $product)
    {
        $cartItem->estimation_shipping_price = $product->custom_shipping_cost;
    }


    public function setCartItemCost(CartItem &$cartItem, Product $product, int $quantity)
    {
        /** @var Collection $prices */
        $prices = $product->productPrices;
        $prices = $prices->sortByDesc('quantity');

        foreach ($prices as $price) {
            /** @var ProductPrice $price */
            if ($price->quantity <= $quantity){
                $cartItem->price = $this->getItemPrice($product, $price);
                $cartItem->regular_price = $price->item_price;
                $cartItem->setup_price = $price->setup_price;
                $cartItem->is_sale = $product->on_sale;
                $cartItem->quantity = $quantity;

                return;
            }
        }

        if(1 == $quantity) { // for order a sample
            $price = $prices->sortBy('quantity')->first();
            $cartItem->price = $this->getItemPrice($product, $price);
            $cartItem->regular_price = $price->item_price;
            $cartItem->setup_price = $price->setup_price;
            $cartItem->is_sale = $product->on_sale;
            $cartItem->quantity = $quantity;

            return;
        }

    }

    public function setCartItemOwner(CartItem &$cartItem, Request $request)
    {
        if (Auth::check()) {
            $cart = Cart::current(Auth::user())
                ->first();

            if (is_null($cart)) {
                $cart = Cart::create(['user_id' => Auth::id()]);
            }

        } else {
            if($request->hasHeader('guest')) {
                $cart = Cart::firstOrCreate(['session' => $request->header('guest')]);
            } else {
                throw new \InvalidArgumentException("User token not found");
            }

        }
        $cartItem->cart()->associate($cart);
    }

    public function setCartItemProductOptions(CartItem &$cartItem, Product $product, $productOptionsData, int $quantity)
    {
        $cartItem->cartItemProductOptions()->delete();

        $productOptions = ProductOption::whereHas('productSubOptions')
            ->where(['product_id' => $product->id])->get();


        foreach ($productOptions as $productOption) { /** @var ProductOption $productOption */
            $productOptionPrice = $this->getProductOptionPrice($productOption, $quantity);

            $currentProductOption = array_first($productOptionsData, function($value) use ($productOption) {
                return $value['product_option_id'] == $productOption->id;
            });

            $productSubOptionIds = $currentProductOption['product_sub_option_ids'];

            $cartItemProductOption = new CartItemProductOption();
            $cartItemProductOption->cartItem()->associate($cartItem);
            $cartItemProductOption->productOption()->associate($productOption);
            $cartItemProductOption->name = $productOption->name;
            $cartItemProductOption->setup_price = $productOptionPrice->setup_price;
            $cartItemProductOption->item_price = $productOptionPrice->item_price;
            $cartItemProductOption->save();

            if(!is_null($productSubOptionIds)) {
                foreach ($productSubOptionIds as $productSubOptionId) {
                    /** @var ProductSubOption $productSubOption */
                    $productSubOption = $productOption->productSubOptions->find($productSubOptionId);
                    $productSubOptionPrice = $this->getProductSubOptionPrice($productSubOption, $quantity);

                    $cartItemProductSubOption = new CartItemProductSubOption();
                    $cartItemProductSubOption->cartItemProductOption()->associate($cartItemProductOption);
                    $cartItemProductSubOption->productSubOption()->associate($productSubOption);
                    $cartItemProductSubOption->name = $productSubOption->name;
                    $cartItemProductSubOption->setup_price = $productSubOptionPrice->setup_price;
                    $cartItemProductSubOption->item_price = $productSubOptionPrice->item_price;
                    $cartItemProductSubOption->save();
                }
            }
        }

    }

    public function setCartItemColors(CartItem &$cartItem, Product $product, $productColorsData)
    {
        $cartItem->cartItemColors()->delete();
        foreach ($product->productColorGroups as $productColorGroup) { /** @var ProductColorGroup $productColorGroup */

            $currentProductColor = array_first($productColorsData, function($value) use ($productColorGroup) {
                return $value['color_group_id'] == $productColorGroup->id;
            });

            /** @var ProductColor $productColor */
            $productColor = ProductColor::find($currentProductColor['color_id']);
            $cartItemColor = new CartItemColor();
            $cartItemColor->cartItem()->associate($cartItem);
            $cartItemColor->productColorGroup()->associate($productColorGroup);
            $cartItemColor->color_group_name = $productColorGroup->name;
            $cartItemColor->productColor()->associate($productColor);
            $cartItemColor->color_name = $productColor->color->name;
            $cartItemColor->save();
        }
    }

    public function setCartItemImprints(CartItem &$cartItem, Product $product, $imprintsData, int $quantity)
    {
        $cartItem->cartItemImprints()->delete();
        foreach ($product->imprints as $imprint) {
            /** @var Imprint $imprint */
            $currentImprint = array_first((array)$imprintsData, function($value) use ($imprint) {
                return $value['imprint_id'] == $imprint->id;
            });

            if (is_null($currentImprint)) {
                continue;
            }

            $imprintPrice = $this->getImprintPrice($imprint, $quantity);

            $cartItemImprint = new CartItemImprint();
            $cartItemImprint->cartItem()->associate($cartItem);
            $cartItemImprint->imprint()->associate($imprint);
            $cartItemImprint->imprint_name = $imprint->name;
            $cartItemImprint->setup_price = $imprintPrice->setup_price;
            $cartItemImprint->item_price = $imprintPrice->item_price;
            $cartItemImprint->color_setup_price = $imprintPrice->color_setup_price;
            $cartItemImprint->color_item_price = $imprintPrice->color_item_price;
            $cartItemImprint->save();

            foreach ($currentImprint['color_ids'] as $colorId) {
                $cartItemImprintColor = new CartItemImprintColor();
                $color = Color::find($colorId);
                $cartItemImprintColor->cartItemImprint()->associate($cartItemImprint);
                $cartItemImprintColor->color()->associate($color);
                $cartItemImprintColor->name = $color->name;
                $cartItemImprintColor->save();
            }
        }
    }

    public function setCartItemSizes(CartItem &$cartItem, Product $product, $sizesData, int $quantity)
    {
        if(!isset($sizesData[0])) return false;
        $sizesData = $sizesData[0]; // TODO fix it

        if(!isset($sizesData['size_ids'])
            or !isset($sizesData['size_group_id'])
            or array_sum(array_pluck((array)$sizesData['size_ids'], 'qty')) != $quantity) {
            return false; // TODO fix it
        }

        $cartItem->cartItemSizeGroup()->delete();
        $sizeGroup = $product->sizeGroup;
        
        if($sizeGroup->id == $sizesData['size_group_id']) {

            $cartItemSizeGroup = new CartItemSizeGroup();
            $cartItemSizeGroup->cartItem()->associate($cartItem);
            $cartItemSizeGroup->sizeGroup()->associate($sizeGroup);
            $cartItemSizeGroup->save();

            foreach ($sizeGroup->sizes as $size) { /** @var Imprint $imprint */
                $currentSize = array_first((array)$sizesData['size_ids'], function($value) use ($size) {
                    return $value['id'] == $size->id;
                });

                if (is_null($currentSize)) {
                    continue;
                }

                $cartItemSize = new CartItemSize();
                $size = Size::find($currentSize['id']);
                $cartItemSize->cartItemSizeGroup()->associate($cartItemSizeGroup);
                $cartItemSize->size()->associate($size);
                $cartItemSize->qty = $currentSize['qty'];
                $cartItemSize->save();
            }
        }
    }

    /**
     * @param Imprint $imprint
     * @param int $quantity
     * @return ImprintPrice
     */
    protected function getImprintPrice(Imprint $imprint, int $quantity): ImprintPrice
    {
        $imprintPrices = $imprint->imprintPrices->sortByDesc('quantity');
        foreach ($imprintPrices as $imprintPrice) {
            /** @var ImprintPrice $imprintPrice */
            if ($imprintPrice->quantity <= $quantity) {
                return $imprintPrice;
            }
        }
        return $imprintPrices->last();

//        throw new OrderException('Invalid quantity for imprint: ' . $imprint->name);
    }

    /**
     * Get proper Product Option price row
     *
     * @param ProductOption $productOption
     * @param int $quantity
     * @return ProductOptionPrice
     * @throws OrderException
     */
    protected function getProductOptionPrice(ProductOption $productOption, int $quantity): ProductOptionPrice
    {
        $productOptionPrices = $productOption->productOptionPrices->sortByDesc('quantity');
        foreach ($productOptionPrices as $productOptionPrice) { /** @var ProductOptionPrice $productOptionPrice */
            if ($productOptionPrice->quantity <= $quantity) {
                return $productOptionPrice;
            }
        }
        throw new OrderException('Invalid quantity for product option: ' . $productOption->id);
    }

    protected function getProductSubOptionPrice(ProductSubOption $productSubOption, int $quantity): ProductSubOptionPrice
    {
        $productSubOptionPrices = $productSubOption->productSubOptionPrices->sortByDesc('quantity');
        foreach ($productSubOptionPrices as $productSubOptionPrice) { /** @var ProductSubOptionPrice $productSubOptionPrice */
            if ($productSubOptionPrice->quantity <= $quantity) {
                return $productSubOptionPrice;
            }
        }
        throw new OrderException('Invalid quantity for product sub option: ' . $productSubOption->id);
    }

    protected function getItemPrice(Product $product, ProductPrice $price)
    {
        return $product->on_sale ? $price->sale_item_price : $price->item_price;
    }

    /**
     * @param CartItem $cartItem
     * @param $artFiles
     */
    public function setCartItemArtFiles(CartItem &$cartItem, $artFiles)
    {
//        $cartItem->cartItemArtProof()->delete();
//        if(!is_null($cartItem->art_file_name)) {
//            $existFilepath = $path . '/' . $cartItem->art_file_name;
//            if(Storage::disk('public')->exists($existFilepath)) {
//                Storage::disk('public')->delete($existFilepath);
//            }
//        }

        $path = CartItem::ART_FILE_DIR . $cartItem->id;
        if(Storage::disk('public')->exists($path)) {
            Storage::disk('public')->deleteDirectory($path);
        }

        if(!empty($artFiles) && count($artFiles)) {
            foreach ($artFiles as $artFile) {
                if (isset($artFile) && $artFile->isValid()) {
                    $fileName = str_random(20) . '.' . $artFile->getClientOriginalExtension();
                    $fullPath = $path . '/'  . $fileName;
                    Storage::disk('public')->put($fullPath,  file_get_contents($artFile));

                    $cartItem->art_file_name = $fileName;
                    $cartItem->art_file_path = $fullPath;
//                    $cartItemArtProof = new CartItemArtProof();
//                    $cartItemArtProof->cartItem()->associate($cartItem);
//                    $cartItemArtProof->name = $fileName;
//                    $cartItemArtProof->path = $fullPath;
//                    $cartItemArtProof->save();
                }
            }
        }
    }
}
