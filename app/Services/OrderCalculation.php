<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 16.03.17
 * Time: 15:47
 */

namespace App\Services;


use App\Exceptions\OrderException;
use App\Models\Price;
use App\Models\Product;
use App\Models\ProductPrice;
use Illuminate\Database\Eloquent\Collection;

class OrderCalculation
{
    public function getItemPrice(Product $product, int $quantity)
    {
        /** @var Collection $prices */
        $prices = $product->productPrices;

        if(1 == $quantity) { // for order a sample
            $price = $prices->sortBy('quantity')->first();
            return $product->on_sale ? $price->sale_item_price : $price->item_price;
        }
        
        $prices = $prices->sortByDesc('quantity');

        foreach ($prices as $price) { /** @var ProductPrice $price */
            if ($price->quantity <= $quantity){
                return $product->on_sale ? $price->sale_item_price : $price->item_price;
            }
        }

        throw new OrderException('Invalid quantity: ' . $quantity);
    }

}