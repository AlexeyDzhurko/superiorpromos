<?php

namespace App\Services;

use App\Mail\DatabaseMailable;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;

class DatabaseMailer extends Mailer
{
    /**
     * Send email using template from database
     *
     * @param DatabaseMailable $mailable
     */
    public function sendFromDatabase(DatabaseMailable $mailable)
    {
        $this->send([], [], function(Message $message) use ($mailable) {
            $mailable->buildMessage($message);
        });
    }

}
