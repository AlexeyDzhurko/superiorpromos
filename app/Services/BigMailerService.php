<?php


namespace App\Services;

use App\Contracts\MailService;
use App\Contracts\Notification;
use GuzzleHttp\Client;


class BigMailerService implements MailService
{
    /**
     * Create Http Client
     *
     * @return Client
     */
    private function createClient()
    {
        return new Client([
            'headers' =>  [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'X-API-Key' => config('bigmailer.api_key')
            ]
        ]);
    }

    /**
     * Create BigMailer contact
     *
     * @param $email
     * @return bool
     */
    public function createContact($email)
    {
        try {
            $client = $this->createClient();

            $data = array(
                'brand_id' => config('bigmailer.brand_id'),
                'email' => $email
            );

            $response =  $client->post(config('bigmailer.api_url') . '/contacts', [
                'body' => json_encode($data)
            ]);

            $decodeRes = json_decode($response->getBody()->getContents(), true);

            if(array_key_exists('contact_id', $decodeRes)) {

                return $decodeRes['contact_id'];
            }

            return false;
        } catch (\Exception $e) {

            return false;
        }
    }

    /**
     * Update exist contact
     *
     * @param $uuid
     * @param $email
     * @param bool $unsubscribe
     * @return bool
     */
    public function updateContact($uuid, $email, $unsubscribe = false)
    {
        try {
            $client = $this->createClient();

            $data = array(
                'contact_id' => $uuid,
                'brand_id' => config('bigmailer.brand_id'),
                'email' => $email,
                'unsubscribe_all' => $unsubscribe
            );

            $response =  $client->post(config('bigmailer.api_url') . '/contacts/' . $uuid, [
                'body' => json_encode($data)
            ]);

            $decodeRes = json_decode($response->getBody(), true);

            if(array_key_exists('contact_id', $decodeRes)) {

                return $decodeRes['contact_id'];
            }

            return false;
        } catch (\Exception $e) {

            return false;
        }
    }

    /**
     * Send transactional email
     * @param Notification $notification
     * @return false|mixed
     */
    public function sendMultipleTransactionalEmail(Notification $notification)
    {
        $variables = $this->parseVariables($notification->toBigMailer());
        $client = $this->createClient();
        $emails = $notification->emails();

        if (is_array($emails) && !empty($emails)) {
            foreach ($emails as $email) {
                $data = array('email' => trim($email), 'variables' => $variables);
                try {
                    $client->post(config('bigmailer.api_url') . '/brands/' . config('bigmailer.brand_id') . '/transactional-campaigns/' . $notification->campaignId() . '/send', [
                        'body' => json_encode($data)
                    ]);
                } catch (\Exception $exception) {
                    continue;
                }
            }
        }
    }

    /**
     * Prepare data for the request
     *
     * @param null $data
     * @return array
     */
    private function parseVariables($data = null)
    {
        if(empty($data)) {
            return array();
        }

        $vars = array();

        foreach ($data as $k => $val) {
            $vars[] = array(
                'name' => $k,
                "value" => (string)$val
            );
        }

        return $vars;

    }

    /**
     * @param Notification $notification
     * @return bool
     */
    public function sendSingleTransactionalEmail(Notification $notification)
    {
        $variables = $this->parseVariables($notification->toBigMailer());
        $client = $this->createClient();
        $email = array_first($notification->emails());
        $data = array('email' => trim($email), 'variables' => $variables);

        try {
            $response = $client->post(config('bigmailer.api_url') . '/brands/' . config('bigmailer.brand_id') . '/transactional-campaigns/' . $notification->campaignId() . '/send', [
                'body' => json_encode($data)
            ]);

            $decodeRes = json_decode($response->getBody(), true);

            if(array_key_exists('contact_id', $decodeRes)) {
                return  true;
            }

            return false;
        } catch (\Exception $exception) {
            return false;
        }
    }
}
