<?php

namespace App\Services;

use App\Exceptions\MissingUserException;
use Illuminate\Support\Facades\Storage;
use App\Contracts\TaxExemptionServiceInterface;
use App\Exceptions\MissingTaxExemptionCertificateException;
use App\User;
use Illuminate\Http\UploadedFile;

class TaxExemptionService implements TaxExemptionServiceInterface
{
    /**
     * @var UploadedFile
     */
    protected $certificate;

    /**
     * @var string
     */
    protected $certificateStoragePath;

    /**
     * @var string
     */
    protected $certificateName;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var boolean
     */
    protected $provideCertificateLater;

    /**
     * TaxExemptionService constructor.
     * @param string $certificateStoragePath
     */
    public function __construct(string $certificateStoragePath)
    {
        $this->certificateStoragePath = $certificateStoragePath;
    }

    /**
     * @param UploadedFile $certificate
     *
     * @return void
     */
    public function setCertificate(UploadedFile $certificate): void
    {
        $this->certificate = $certificate;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getCertificateName(): string
    {
        return $this->certificateName;
    }

    /**
     * @param bool $provideCertificateLater
     */
    public function setProvideCertificateLater(bool $provideCertificateLater)
    {
        $this->provideCertificateLater = $provideCertificateLater;
    }

    /**
     * @throws MissingTaxExemptionCertificateException
     * @throws MissingUserException
     */
    public function uploadCertificate(): void
    {
        $this->trowExceptionIfUserIsMissing();
        $this->trowExceptionIfCertificateIsMissing();

        $this->certificateName = $this->createCertificateName($this->certificate);
        $certificatePath = $this->getCertificateFullPath($this->certificateName);

        Storage::disk('public')->put($certificatePath, file_get_contents($this->certificate));

        $this->updateUserTaxExemptionData($this->user, $this->certificateName);
    }

    /**
     * @throws MissingUserException
     */
    public function provideTaxExemptionCertificateLater(): void
    {
        $this->trowExceptionIfUserIsMissing();

        $this->user->provide_tax_exemption_later = true;
        $this->user->save();
    }

    /**
     * @param UploadedFile $certificate
     *
     * @return string
     */
    private function createCertificateName(UploadedFile $certificate): string
    {
        return str_random(20) . '.' . $certificate->getClientOriginalExtension();
    }

    /**
     * @param string $certificateName
     *
     * @return string
     */
    private function getCertificateFullPath(string $certificateName): string
    {
        return $this->certificateStoragePath . $certificateName;
    }

    /**
     * @throws MissingTaxExemptionCertificateException
     */
    private function trowExceptionIfCertificateIsMissing(): void
    {
        if (null === $this->certificate) {
            throw new MissingTaxExemptionCertificateException('Certificate is not provided');
        }
    }

    /**
     * @throws MissingUserException
     */
    private function trowExceptionIfUserIsMissing(): void
    {
        if (null === $this->user) {
            throw new MissingUserException('User is not provided');
        }
    }

    /**
     * @param User $user
     */
    private function updateUserTaxExemptionData(User $user, string $newCertificateName): void
    {
        $oldCertificateName = $user->getOriginal('tax_exemption_certificate');

        if (null != $oldCertificateName) {
            Storage::disk('public')->delete($this->getCertificateFullPath($oldCertificateName));
        }

        $user->tax_exemption_certificate = $newCertificateName;
        $user->provide_tax_exemption_later = false;
        $user->save();
    }
}
