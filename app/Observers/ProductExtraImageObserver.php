<?php


namespace App\Observers;


use App\Admin\Services\ProductManager;
use App\Models\ProductExtraImage;
use File;

class ProductExtraImageObserver
{
    public function deleting(ProductExtraImage $productExtraImage)
    {
        File::deleteDirectory(public_path().ProductManager::EXTRA_IMAGES_PATH . $productExtraImage->id);
    }
}
