<?php

namespace App\Observers;

use App\Contracts\MailService;
use App\Notifications\BigMailer\RegistrationNotification;
use App\User;
use Storage;

class UserObserver
{
    /**
     * @param User $user
     */
    public function created(User $user)
    {
        $mailService = app(MailService::class);
        $mailService->sendSingleTransactionalEmail(
            new RegistrationNotification(
                [$user->email],
                [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email
                ]
            )
        );
    }

    /**
     * @param user $user
     */
    public function updating(User $user)
    {
        if ($user->getOriginal('certificate') != $user->certificate) {
            Storage::disk('public')->delete($user->getOriginal('certificate'));
        }
    }

    /**
     * @param user $user
     */
    public function deleting(User $user)
    {
        Storage::disk('public')->delete($user->certificate);
    }
}
