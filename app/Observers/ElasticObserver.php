<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Elasticsearch;

class ElasticObserver
{
    public function saved(Model $model)
    {
        if (Elasticsearch::indices()->exists(['index' => $model::getIndexName()])) {
            $data = [
                'body' => $model->getElasticDocument(),
                'index' => $model::getIndexName(),
                'type' => $model::getIndexName(),
                'id' => $model->id,
            ];

            Elasticsearch::index($data);
        }
    }

    public function deleted(Model $model)
    {
        if (Elasticsearch::indices()->exists(['index' => $model::getIndexName()])) {
            $data = [
                'index' => $model::getIndexName(),
                'type' => $model::getIndexName(),
                'id' =>  $model->id
            ];

            Elasticsearch::delete($data);
        }
    }

}
