<?php

namespace App\Observers;

use App\Models\Testimonial;

class TestimonialObserver
{
    /**
     * @param Testimonial $testimonial
     */
    public function deleting(Testimonial $testimonial)
    {
        unlink(public_path() . '/uploads/' . $testimonial->image);
    }
}
