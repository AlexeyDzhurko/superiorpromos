<?php


namespace App\Observers;


use App\Contracts\MailService;
use App\Models\UserNote;
use App\Notifications\BigMailer\AdminOrderUserNoteNotification;
use App\Notifications\BigMailer\CustomerOrderUserNoteNotification;

class UserNoteObserver
{
    public function created(UserNote $userNote)
    {
        $mailService = app(MailService::class);

        $mailService->sendSingleTransactionalEmail(new CustomerOrderUserNoteNotification(
            [$userNote->orderItem->order->user->email],
            [
                'customer_name' => $userNote->orderItem->order->user->name,
                'customer_id' => $userNote->orderItem->order->user->id,
                'order_item_id' => $userNote->orderItem->id,
                'order_note' => $userNote->note
            ]
        ));
    }

    public function updated(UserNote $userNote)
    {
        if($userNote->customer_read == 1) {
            $mailService = app(MailService::class);
            $recipients = config('site-settings.order_note_email') ? explode(',', config('site-settings.order_note_email')) : [];

            $mailService->sendMultipleTransactionalEmail(new AdminOrderUserNoteNotification(
                $recipients,
                [
                    'sub_order_id' => $userNote->orderItem->id,
                    'status' => $userNote->approved == 1 ? 'Approved' : 'Denied',
                    'customer_id' => $userNote->orderItem->order->user->id,
                    'customer_name' => $userNote->orderItem->order->user->name,
                    'order_item_id' => $userNote->orderItem->product->id,
                    'order_item_name' => $userNote->orderItem->product->name,
                    'admin_note' => $userNote->note,
                    'customer_note' => $userNote->customer_note,
                ]
            ));
        }
    }
}
