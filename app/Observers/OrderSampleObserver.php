<?php


namespace App\Observers;


use App\Contracts\MailService;
use App\Models\OrderSample;
use App\Notifications\BigMailer\NewSampleOrderNotification;

class OrderSampleObserver
{
    /**
     * @param OrderSample $orderSample
     */
    public function created(OrderSample $orderSample)
    {
        $mailService = app(MailService::class);
        $recipients = config('site-settings.new_order_email') ? explode(',', config('site-settings.new_order_email')) : [];
        array_push($recipients, $orderSample->user->email);

        $mailService->sendMultipleTransactionalEmail(
            new NewSampleOrderNotification($recipients, [
                'customer_name' => $orderSample->user->name
            ])
        );

    }
}
