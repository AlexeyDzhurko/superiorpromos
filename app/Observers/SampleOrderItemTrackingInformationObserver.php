<?php


namespace App\Observers;


use App\Contracts\MailService;
use App\Models\OrderSampleTrackingInformation;
use App\Notifications\BigMailer\TrackingInformationNotification;

class SampleOrderItemTrackingInformationObserver
{
    public function created(OrderSampleTrackingInformation $orderSampleTrackingInformation)
    {
        $mailService = app(MailService::class);
        if($orderSampleTrackingInformation->tracking_notify_customer) {
            $mailService->sendSingleTransactionalEmail(
                new TrackingInformationNotification(
                    [$orderSampleTrackingInformation->orderSampleItem->orderSample->user->email],
                    [
                        'customer_id' => $orderSampleTrackingInformation->orderSampleItem->orderSample->user->id,
                        'customer_name' => $orderSampleTrackingInformation->orderSampleItem->orderSample->user->name,
                        'order_item_id' => $orderSampleTrackingInformation->orderSampleItem->id,
                        'shipping_date' => $orderSampleTrackingInformation->tracking_shipping_date,
                        'carrier' => $orderSampleTrackingInformation->tracking_shipping_company,
                        'tracking_id' => $orderSampleTrackingInformation->tracking_shipping_company === 'Truck' ? $orderSampleTrackingInformation->tracking_url : $orderSampleTrackingInformation->tracking_number,
                        'note' => $orderSampleTrackingInformation->tracking_note,
                    ]
                )
            );
        }
    }
}
