<?php


namespace App\Observers;

use App\Contracts\MailService;
use App\Models\ArtProof;
use App\Notifications\BigMailer\ArtProofStatusNotification;
use App\Notifications\BigMailer\NewArtProofNotification;

class ArtProofObserver
{
    /**
     * @param ArtProof $artProof
     */
    public function created(ArtProof $artProof)
    {
        $mailService = app(MailService::class);
        $mailService->sendSingleTransactionalEmail(
            new NewArtProofNotification(
                [$artProof->orderItem->order->user->email],
                [
                    'customer_id' => $artProof->orderItem->order->user->id,
                    'customer_name' => $artProof->orderItem->order->user->name,
                    'order_item_id' => $artProof->orderItem->id
                ]
            )
        );
    }

    public function updated(ArtProof $artProof)
    {
        $mailService = app(MailService::class);
        $recipients = config('site-settings.art_proof_email') ? explode(',', config('site-settings.art_proof_email')) : [];

        $mailService->sendMultipleTransactionalEmail(
            new ArtProofStatusNotification(
                $recipients,
                [
                    'sub_order_id' => $artProof->orderItem->order->id . 'S' . $artProof->orderItem->id,
                    'status' => $artProof->approved ? 'approved' : 'denied',
                    'customer_name' => $artProof->orderItem->order->user->name,
                    'customer_id' => $artProof->orderItem->order->user->id,
                    'order_item_id' => $artProof->orderItem->id,
                    'order_item_name' => $artProof->orderItem->product->name,
                    'admin_note' => $artProof->note,
                    'customer_note' => $artProof->customer_note,
                ]
            )
        );
    }
}
