<?php

namespace App\Observers;

use App\Contracts\MailService;
use App\Contracts\PaymentGateway;
use App\Notifications\BigMailer\AdminDefaultPaymentProfileNotification;
use App\Notifications\BigMailer\AdminDeletePaymentProfileNotification;
use App\Notifications\BigMailer\AdminNewPaymentProfileNotification;
use App\PaymentProfile;

class PaymentProfileObserver
{
    protected $paymentGateway;

    /**
     * PaymentProfileObserver constructor.
     * @param PaymentGateway $paymentGateway
     */
    public function __construct(PaymentGateway $paymentGateway)
    {
        $this->paymentGateway = $paymentGateway;
    }

    /**
     * @param PaymentProfile $paymentProfile
     */
    public function creating(PaymentProfile $paymentProfile)
    {
        $this->saveOnPaymentGateway($paymentProfile);
        $this->setCardTypeNumber($paymentProfile);
        $this->maskCreditCardNumber($paymentProfile);
    }

    /**
     * @param PaymentProfile $paymentProfile
     */
    public function updating(PaymentProfile $paymentProfile)
    {
        $this->saveOnPaymentGateway($paymentProfile);
    }

    /**
     * @param PaymentProfile $paymentProfile
     */
    public function deleting(PaymentProfile $paymentProfile)
    {
        $this->paymentGateway->deletePaymentProfile($paymentProfile->user, $paymentProfile);
    }

    /**
     * Add Payment Profile to Payment Gateway
     *
     * @param PaymentProfile $paymentProfile
     */
    protected function saveOnPaymentGateway(PaymentProfile $paymentProfile)
    {
        $paymentProfileId = $this->paymentGateway->storePaymentProfileForUser($paymentProfile->user, $paymentProfile);
        $paymentProfile->id = $paymentProfileId;
    }

    /**
     * Generates credit card number that can be stored without encryption
     *
     * @param PaymentProfile $paymentProfile
     */
    protected function maskCreditCardNumber(PaymentProfile $paymentProfile)
    {
        $maskedCreditCardNumber = str_repeat('*', strlen($paymentProfile->card_number) - 4) . substr($paymentProfile->card_number, -4);
        $paymentProfile->card_number = $maskedCreditCardNumber;
    }

    /**
     * Set card type
     *
     * @param PaymentProfile $paymentProfile
     */
    protected function setCardTypeNumber(PaymentProfile $paymentProfile)
    {
        $cardType = $this->getCreditCardType($paymentProfile->card_number);
        $paymentProfile->card_type = $cardType ?? 'visa';
    }

    protected function getCreditCardType($str, $format = 'string')
    {
        if (empty($str)) {
            return null;
        }

//        ['visa', 'mastercard', 'discover', 'amex'] // only this cards

        $matchingPatterns = [
            'visa' => '/^4[0-9]{12}(?:[0-9]{3})?$/',
            'mastercard' => '/^5[1-5][0-9]{14}$/',
            'amex' => '/^3[47][0-9]{13}$/',
//            'diners' => '/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/',
            'discover' => '/^6(?:011|5[0-9]{2})[0-9]{12}$/',
//            'jcb' => '/^(?:2131|1800|35\d{3})\d{11}$/',
//            "maestro" => "((?:5020|5038|6304|6579|6761)\d{12}(?:\d\d)?)",
//            "solo" => "((?:6334|6767)\d{12}(?:\d\d)?\d?)",
//            "switch" => "(?:(?:(?:4903|4905|4911|4936|6333|6759)\d{12})|(?:(?:564182|633110)\d{10})(\d\d)?\d?)",
//            'any' => '/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/'
        ];

        $ctr = 1;
        foreach ($matchingPatterns as $key => $pattern) {
            if (preg_match($pattern, $str)) {

                return $format == 'string' ? $key : $ctr;
            }
            $ctr++;
        }

        return null;
    }

    public function created(PaymentProfile  $paymentProfile)
    {
        $mailService = app(MailService::class);
        $recipients = config('site-settings.customer_alert_email') ? explode(',', config('site-settings.customer_alert_email')) : [];

         $mailService->sendMultipleTransactionalEmail(new AdminNewPaymentProfileNotification(
            $recipients,
            [
                'customer_id' => $paymentProfile->user->id,
                'card_holder' => $paymentProfile->card_holder,
                'acct' => $paymentProfile->card_number,
                'created' => $paymentProfile->created_at->format('Y-m-d')
            ])
        );
    }

    public function updated(PaymentProfile  $paymentProfile)
    {
        if($paymentProfile->is_default) {
            $mailService = app(MailService::class);
            $recipients = config('site-settings.customer_alert_email') ? explode(',', config('site-settings.customer_alert_email')) : [];

            $mailService->sendMultipleTransactionalEmail(new AdminDefaultPaymentProfileNotification(
                $recipients,
                [
                    'customer_id' => $paymentProfile->user->id,
                    'card_holder' => $paymentProfile->card_holder,
                    'card_type' => $paymentProfile->card_type,
                    'acct' => $paymentProfile->card_number,
                    'expire_mon' => $paymentProfile->expiration_month,
                    'expire_year' => $paymentProfile->expiration_year,
                    'customer_first_name' => $paymentProfile->first_name,
                    'customer_last_name' => $paymentProfile->last_name,
                    'company' => $paymentProfile->company,
                    'address' => $paymentProfile->address1,
                    'city' => $paymentProfile->city,
                    'state' => $paymentProfile->state,
                    'country' => $paymentProfile->country,
                    'phone' => $paymentProfile->phone,
                    'ext' => $paymentProfile->phone_extension
                ]
            ));
        }
    }

    public function deleted(PaymentProfile  $paymentProfile)
    {
        $mailService = app(MailService::class);
        $recipients = config('site-settings.customer_alert_email') ? explode(',', config('site-settings.customer_alert_email')) : [];

        $mailService->sendMultipleTransactionalEmail(new AdminDeletePaymentProfileNotification(
            $recipients,
            [
                'customer_id' => $paymentProfile->user->id,
                'card_holder' => $paymentProfile->card_holder,
                'card_type' => $paymentProfile->card_type,
                'acct' => $paymentProfile->card_number,
                'expire_mon' => $paymentProfile->expiration_month,
                'expire_year' => $paymentProfile->expiration_year,
                'customer_first_name' => $paymentProfile->first_name,
                'customer_last_name' => $paymentProfile->last_name,
                'company' => $paymentProfile->company,
                'address' => $paymentProfile->address1,
                'city' => $paymentProfile->city,
                'state' => $paymentProfile->state,
                'country' => $paymentProfile->country,
                'phone' => $paymentProfile->phone,
                'ext' => $paymentProfile->phone_extension
            ]
        ));
    }
}
