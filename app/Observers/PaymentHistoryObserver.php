<?php


namespace App\Observers;


use App\Contracts\MailService;
use App\Enums\PaymentTypeEnum;
use App\Models\Payment;
use App\Notifications\BigMailer\CreditConfirmationEmail;
use App\Notifications\BigMailer\ExtraBillingConfirmationNotification;
use App\Notifications\BigMailer\ExtraBillingShippingConfirmationNotification;

class PaymentHistoryObserver
{
    public function created(Payment $payment)
    {
        if($payment->notify_customer == 1) {
            $mailService = app(MailService::class);

            $order = $payment->orderItem->order;
            $customer = $order->user;
            $product = $payment->orderItem->product;
            $paymentProfile = $payment->paymentProfile;

            $billingCompany = $order->billing_company_name ? $order->billing_company_name . ', ' : '';
            $billingAddress2 = $order->billing_address_line_2 ? $order->billing_address_line_2 . ', ' : '';
            $billingState = $order->billing_state ? $order->billing_state . ', ' : '';
            $shippingCompany = $order->shipping_company_name ? $order->shipping_company_name . ', ' : '';
            $shippingAddress2 = $order->shipping_address_line_2 ? $order->shipping_address_line_2 . ', ' : '';
            $shippingState = $order->shipping_state ? $order->shipping_state . ', ' : '';
            $paymentProfileCompany = $paymentProfile->company ? $paymentProfile->company . ', ' : '';
            $paymentProfileAddress2 = $paymentProfile->address2 ? $paymentProfile->address2 . ', ' : '';
            $paymentProfileState = $paymentProfile->state ? $paymentProfile->state . ', ' : '';

            $data = [
                'customer_id' => $customer->id,
                'customer_name' => $customer->name,
                'customer_email' => $customer->email,
                'order_item_id' => $product->id,
                'order_item_name' => $product->name,
                'order_date' => $order->created_at->format('Y-m-d'),
                'card_expire_date_year' => $payment->paymentProfile->expiration_year,
                'card_number' => $payment->paymentProfile->card_number,
                'billing_address' =>  $order->billing_first_name . ' ' .
                    $order->billing_last_name . ', ' .
                    $billingCompany .
                    $order->billing_address_line_1 . ', ' .
                    $billingAddress2 .
                    $order->billing_city . ', ' .
                    $billingState .
                    $order->billing_zip . ', ' .
                    'Phone #' . $order->billing_day_telephone,
                'shipping_address' => $order->shipping_first_name . ' ' .
                    $order->shipping_last_name . ' ' .
                    $shippingCompany .
                    $order->shipping_address_line_1 . ', ' .
                    $shippingAddress2 .
                    $order->shipping_city . ', ' .
                    $shippingState .
                    $order->shipping_zip . ', ' .
                    'Phone #' . $order->shipping_day_telephone,
                'payment_address' => $paymentProfile->first_name . ' ' .
                    $paymentProfile->last_name . ' ' .
                    $paymentProfileCompany .
                    $paymentProfile->address1 . ', ' .
                    $paymentProfileAddress2 .
                    $paymentProfile->city . ', ' .
                    $paymentProfileState .
                    $paymentProfile->zip . ', ' .
                    'Phone #' . $paymentProfile->phone,
                'message' => $payment->notify_message,
                'credit_amount' => number_format($payment->credit_amount,2),
                'shipping_method' => '',
                'shipping_date' => !is_null($payment->shipping_date) ? $payment->shipping_date->format('Y-m-d') : null,
                'shipping_quantity' => $payment->shipping_quantity,
                'overrun_quantity' => $payment->over_quantity,
                'overrun_fees' => number_format($payment->over_fees, 2),
                'extra_amount' => number_format($payment->price + $payment->credit_amount, 2),
                'total' => number_format($payment->price,2),
                'tax' => $payment->tax,
                'total_cost' => '',
            ];

            if($payment->type === PaymentTypeEnum::OTHER_TYPE) {
                $mailService->sendSingleTransactionalEmail(new ExtraBillingConfirmationNotification(
                    [$customer->email],
                    $data
                ));
            }

            if($payment->type === PaymentTypeEnum::OVERRUN_TYPE) {
                $mailService->sendSingleTransactionalEmail(new ExtraBillingShippingConfirmationNotification(
                   [$customer->email],
                   $data
                ));
            }

            if ($payment->type === PaymentTypeEnum::CREDIT_TYPE) {
                $mailService->sendSingleTransactionalEmail(new CreditConfirmationEmail(
                    [$customer->email],
                    $data
                ));
            }
        }
    }
}
