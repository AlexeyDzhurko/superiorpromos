<?php


namespace App\Observers;


use App\Models\ArtProof;
use App\Models\CartItem;
use App\Models\OrderItem;
use Storage;

class OrderItemObserver
{
    public function updating(OrderItem $orderItem)
    {
        /** Store cart art proofs to ArtProof table **/
        if (Storage::disk('public')->exists(CartItem::ART_FILE_DIR . $orderItem->cartItem->id)) {
            if (!Storage::disk('public')->exists(ArtProof::CUSTOMER_ART_FILE_DIR . $orderItem->id)) {
                Storage::disk('public')->makeDirectory(ArtProof::CUSTOMER_ART_FILE_DIR . $orderItem->id);
            }

            $artFileName = $orderItem->cartItem->art_file_name;
            $artPath = ArtProof::CUSTOMER_ART_FILE_DIR . $orderItem->id;

            if (Storage::disk('public')->exists(CartItem::ART_FILE_DIR . $orderItem->cartItem->id)) {
                $command = sprintf('cp -r storage/%s/* storage/%s/',
                    CartItem::ART_FILE_DIR . $orderItem->cartItem->id,
                    $artPath
                );
                exec($command);
            }

            $orderItem->art_file_path = $artPath;
            $orderItem->art_file_name = $artFileName;

//                Storage::disk('public')->deleteDirectory(CartItem::ART_FILE_DIR . $orderItem->cartItem->id);
        }
//            foreach ($orderItem->cartItem->cartItemArtProof as $cartItemArtProof) {
//                try {
//                    $artProof = new ArtProof();
//                    $artProof->user()->associate($user);
//                    $artProof->orderItem()->associate($orderItem);
//                    $artProof->path = $cartItemArtProof->name;
//                    $artProof->save();
//
//                    if(!Storage::disk('public')->exists(ArtProof::ART_FILE_DIR . $orderItem->id)) {
//                        Storage::disk('public')->makeDirectory(ArtProof::ART_FILE_DIR . $orderItem->id);
//                    }
//
//                    Storage::disk('public')->move($cartItemArtProof->path, ArtProof::ART_FILE_DIR . $orderItem->id . '/'. $cartItemArtProof->name);
//                    $cartItemArtProof->delete();
//                }catch (\Exception $exception) {
//                    continue;
//                }
//            }

    }
}
