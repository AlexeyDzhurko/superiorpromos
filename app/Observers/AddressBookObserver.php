<?php


namespace App\Observers;

use App\Contracts\MailService;
use App\Models\Address;
use App\Notifications\BigMailer\AdminDefaultAddressBookNotification;
use App\Notifications\BigMailer\AdminDeleteAddressBookNotification;
use App\Notifications\BigMailer\AdminNewAddressBookNotification;
use App\Notifications\BigMailer\AdminUpdateAddressBookNotification;

class AddressBookObserver
{
    public function created(Address $address)
    {
        $mailService = app(MailService::class);
        $recipients = config('site-settings.customer_alert_email') ? explode(',', config('site-settings.customer_alert_email')) : [];

        $mailService->sendMultipleTransactionalEmail(
            new AdminNewAddressBookNotification(
                $recipients,
                [
                    'customer_id' => $address->user->id,
                    'address_type' => $address->type == 1 ? 'Billing' : 'Shipping',
                    'customer_first_name' => $address->first_name,
                    'customer_middle_name' => $address->middle_name,
                    'customer_last_name' => $address->last_name,
                    'company' => $address->company_name,
                    'title' => $address->title,
                    'suffix' => $address->suffix,
                    'address1' => $address->address_line_1,
                    'address2' => $address->address_line_2,
                    'city' => $address->city,
                    'state' => $address->state,
                    'zip' => $address->zip,
                    'country' => $address->country,
                    'phone' => $address->day_telephone,
                ]
            )
        );
    }

    public function updated(Address $address)
    {
        $mailService = app(MailService::class);
        $recipients = config('site-settings.customer_alert_email') ? explode(',', config('site-settings.customer_alert_email')) : [];

        if ($address->is_default) {
            $mailService->sendMultipleTransactionalEmail(
                new AdminDefaultAddressBookNotification(
                    $recipients,
                    [
                        'customer_id' => $address->user->id,
                        'address_type' => $address->type == 1 ? 'Billing' : 'Shipping',
                        'customer_first_name' => $address->first_name,
                        'customer_middle_name' => $address->middle_name,
                        'customer_last_name' => $address->last_name,
                        'company' => $address->company_name,
                        'title' => $address->title,
                        'suffix' => $address->suffix,
                        'address1' => $address->address_line_1,
                        'address2' => $address->address_line_2,
                        'city' => $address->city,
                        'state' => $address->state,
                        'zip' => $address->zip,
                        'country' => $address->country,
                        'phone' => $address->day_telephone,
                    ]
                )
            );
        }

        $mailService->sendMultipleTransactionalEmail(
            new AdminUpdateAddressBookNotification(
                $recipients,
                [
                    'customer_id' => $address->user->id,
                    'address_type' => $address->type == 1 ? 'Billing' : 'Shipping',
                    'customer_first_name' => $address->first_name,
                    'customer_middle_name' => $address->middle_name,
                    'customer_last_name' => $address->last_name,
                    'company' => $address->company_name,
                    'title' => $address->title,
                    'suffix' => $address->suffix,
                    'address1' => $address->address_line_1,
                    'address2' => $address->address_line_2,
                    'city' => $address->city,
                    'state' => $address->state,
                    'zip' => $address->zip,
                    'country' => $address->country,
                    'phone' => $address->day_telephone,
                ]
            )
        );
    }

    public function deleted(Address $address)
    {
        $mailService = app(MailService::class);
        $recipients = config('site-settings.customer_alert_email') ? explode(',', config('site-settings.customer_alert_email')) : [];

        $mailService->sendMultipleTransactionalEmail(
            new AdminDeleteAddressBookNotification(
                $recipients,
                [
                    'customer_id' => $address->user->id,
                    'address_type' => $address->type == 1 ? 'Billing' : 'Shipping',
                    'customer_first_name' => $address->first_name,
                    'customer_middle_name' => $address->middle_name,
                    'customer_last_name' => $address->last_name,
                    'company' => $address->company_name,
                    'title' => $address->title,
                    'suffix' => $address->suffix,
                    'address1' => $address->address_line_1,
                    'address2' => $address->address_line_2,
                    'city' => $address->city,
                    'state' => $address->state,
                    'zip' => $address->zip,
                    'country' => $address->country,
                    'phone' => $address->day_telephone,
                ]
            )
        );
    }
}
