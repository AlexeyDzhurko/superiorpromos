<?php


namespace App\Observers;


use App\Contracts\MailService;
use App\Models\TrackingInformation;
use App\Notifications\BigMailer\TrackingInformationNotification;

class TrackingInformationObserver
{
    public function created(TrackingInformation $trackingInformation)
    {
        $mailService = app(MailService::class);
        if($trackingInformation->tracking_notify_customer) {
            $mailService->sendSingleTransactionalEmail(
                new TrackingInformationNotification(
                    [$trackingInformation->orderItem->order->user->email],
                    [
                        'customer_id' => $trackingInformation->orderItem->order->user->id,
                        'customer_name' => $trackingInformation->orderItem->order->user->name,
                        'order_item_id' => $trackingInformation->orderItem->id,
                        'shipping_date' => $trackingInformation->tracking_shipping_date,
                        'carrier' => $trackingInformation->tracking_shipping_company,
                        'tracking_id' => $trackingInformation->tracking_shipping_company === 'Truck' ? $trackingInformation->tracking_url : $trackingInformation->tracking_number,
                        'note' => $trackingInformation->tracking_note,
                        'display_reseller_image' => $trackingInformation->tracking_request_rating ? 'block' : 'none',
                    ]
                )
            );
        }
    }
}
