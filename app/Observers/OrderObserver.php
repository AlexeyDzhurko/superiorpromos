<?php


namespace App\Observers;

use App\Contracts\MailService;
use App\Models\Order;
use App\Notifications\BigMailer\AdminOrderNotification;
use App\Notifications\BigMailer\CustomerOrderNotification;

class OrderObserver
{
    /**
     * @param Order $order
     */
    public function created(Order $order)
    {
        $mailService = app(MailService::class);
        $billingCompany = $order->billing_company_name ? $order->billing_company_name . ', ' : '';
        $billingAddress2 = $order->billing_address_line_2 ? $order->billing_address_line_2 . ', ' : '';
        $billingState = $order->billing_state ? $order->billing_state . ', ' : '';
        $shippingCompany = $order->shipping_company_name ? $order->shipping_company_name . ', ' : '';
        $shippingAddress2 = $order->shipping_address_line_2 ? $order->shipping_address_line_2 . ', ' : '';
        $shippingState =  $order->shipping_state ? $order->shipping_state . ', ' : '';
        $paymentCompany = '';
        $paymentAddress2 = '';
        $paymentState = '';
        if($order->payment) {
            $paymentCompany = $order->payment->paymentProfile->company ? $order->payment->paymentProfile->company . ', ' : '';
            $paymentAddress2 = $order->payment->paymentProfile->address2 ? $order->payment->paymentProfile->address2 . ', ' : '';
            $paymentState = $order->payment->paymentProfile->state ? $order->payment->paymentProfile->state . ', ' : '';
        }
        $recipients = config('site-settings.new_order_email') ? explode(',', config('site-settings.new_order_email')) : [];
        $total_price = filter_var($order->total_price, FILTER_SANITIZE_NUMBER_INT) / 100;

        $mailService->sendMultipleTransactionalEmail(new AdminOrderNotification(
            $recipients,
            [
                'user_id' => $order->user->id,
                'order_id' => $order->id,
                'sub_orders' => $order->id,
                'total' => (float)number_format($total_price, 2, '.', ''),
            ]
        ));

        $mailService->sendSingleTransactionalEmail(new CustomerOrderNotification(
            [$order->user->email],
            [
                'user_id' => $order->user->id,
                'user_name' => $order->shipping_first_name . ' ' . $order->shipping_last_name,
                'order_date' => $order->created_at->format('Y-m-d g:i A'),
                'order_id' => $order->id,
                'payment_method' => $order->payment ? 'Credit Card' : 'Check/Money Order or Apply For Terms',
                'billing_address' =>
                    $order->billing_first_name . ' ' . $order->billing_last_name . ', ' .
                    $billingCompany .
                    $order->billing_address_line_1 . ', ' .
                    $billingAddress2 .
                    $order->billing_city . ', ' .
                    $billingState .
                    $order->billing_zip . ', ' .
                    'Phone #' . $order->billing_day_telephone,
                'shipping_address' =>
                    $order->shipping_first_name . ' ' . $order->shipping_last_name . ' ' .
                    $shippingCompany .
                    $order->shipping_address_line_1 . ', ' .
                    $shippingAddress2 .
                    $order->shipping_city . ', ' .
                    $shippingState .
                    $order->shipping_zip . ', ' .
                    'Phone #' . $order->shipping_day_telephone,
                'payment_address' =>
                    $order->payment ?
                        $order->payment->paymentProfile->first_name . ' ' . $order->payment->paymentProfile->last_name . ', ' .
                        $paymentCompany .
                        $order->payment->paymentProfile->address1 . ', ' .
                        $paymentAddress2  .
                        $order->payment->paymentProfile->city . ', ' .
                        $paymentState .
                        $order->payment->paymentProfile->zip . ', ' .
                        'Phone #' .  $order->payment->paymentProfile->phone
                        : 'Not Set.',
                'total' => (float)number_format($total_price, 2, '.', ''),
            ]
        ));
    }
}
