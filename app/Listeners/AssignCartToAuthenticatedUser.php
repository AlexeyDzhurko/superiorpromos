<?php

namespace App\Listeners;

use App\Models\Cart;
use App\Models\CartItem;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Session;

class AssignCartToAuthenticatedUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param User $user
     * @return void
     */
    public function handle(User $user)
    {
        if ($cart = Cart::withoutOrder()->whereSession(Session::getId())->whereRaw('carts.user_id IS NULL')->first()) { /** @var Cart $cart */

            //Move session cart items to existed user's cart
            if ($currentCart = Cart::current($user)->first()) {
                foreach ($cart->cartItems as $cartItem) { /** @var CartItem $cartItem */
                    $cartItem->cart()->associate($currentCart);
                    $cartItem->save();
                }
                $cart->delete();
            } else {
                //Assign session cart to current user
                $cart->user()->associate($user);
                $cart->save();
            }
        }
    }
}
