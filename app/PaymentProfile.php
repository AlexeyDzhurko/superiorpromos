<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\PaymentProfile
 *
 * @SWG\Definition (
 *      definition="PaymentProfile",
 *      @SWG\Property(property="id", type="string", example="FirstName LastName"),
 *      @SWG\Property(property="card_holder", type="string", example="FirstName LastName"),
 *      @SWG\Property(property="card_number", type="string", example="4007000000027"),
 *      @SWG\Property(property="expiration_month", type="integer", example="11"),
 *      @SWG\Property(property="expiration_year", type="integer", example="2019"),
 *      @SWG\Property(property="first_name", type="string", example="FirstName"),
 *      @SWG\Property(property="last_name", type="string", example="LastName"),
 *      @SWG\Property(property="address1", type="string", example="address 1"),
 *      @SWG\Property(property="address2", type="string", example="address 2"),
 *      @SWG\Property(property="company", type="string", example="Company"),
 *      @SWG\Property(property="city", type="string", example="New York"),
 *      @SWG\Property(property="state", type="string", example="Florida"),
 *      @SWG\Property(property="zip", type="string", example="11211"),
 *      @SWG\Property(property="country", type="string", example="US"),
 *      @SWG\Property(property="phone", type="string", example="333-333-1212"),
 *      @SWG\Property(property="phone_extension", type="string", example="123"),
 *      @SWG\Property(property="is_default", type="string", example="1"),
 * ),
 * 
 * Class PaymentProfile
 * @package App
 * @property int $id
 * @property User $user
 * @property string $first_name
 * @property string $last_name
 * @property string $company
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $country
 * @property string $phone
 * @property string $phone_extension
 * @property bool $is_default
 * @property string $card_type
 * @property string $expiration_month
 * @property string $expiration_year
 * @property string $card_number
 * @property string $card_holder
 * @property Carbon $deleted_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property integer $user_id
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereCompany($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereState($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereZip($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile wherePhoneExtension($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereIsDefault($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereCardType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereExpirationMonth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereExpirationYear($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereCardNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereCardHolder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $address1
 * @property string $address2
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereAddress1($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentProfile whereAddress2($value)
 */
class PaymentProfile extends Model
{
    use SoftDeletes;

    public $incrementing = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'card_holder',
        'card_number',
        'card_type',
        'expiration_month',
        'expiration_year',
        'first_name',
        'last_name',
        'company',
        'address1',
        'address2',
        'city',
        'state',
        'zip',
        'country',
        'phone',
        'phone_extension',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
