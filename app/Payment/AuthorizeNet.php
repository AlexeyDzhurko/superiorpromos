<?php

namespace App\Payment;


use App\Contracts\PaymentGateway;
use App\Exceptions\ConfigurationException;
use App\Exceptions\PaymentGatewayException;
use App\Models\Order;
use App\Models\Payment;
use App\PaymentProfile;
use App\User;
use net\authorize\api\contract\v1 as AnetAPI;
use Exception;


class AuthorizeNet implements PaymentGateway
{
    protected $apiLogin = null;
    protected $transactionKey = null;
    protected $sandBox = null;

    public function __construct()
    {
        $apiLogin = env('AUTHORIZE_NET_API_LOGIN');
        $transactionKey = env('AUTHORIZE_NET_TRANSACTION_KEY');

        $sandBox = env('AUTHORIZE_NET_SANDBOX', true);
        if ($sandBox) {
            $apiLogin = env('SANDBOX_AUTHORIZE_NET_API_LOGIN');
            $transactionKey = env('SANDBOX_AUTHORIZE_NET_TRANSACTION_KEY');
        }

        if (is_null($apiLogin) || is_null($transactionKey)) {
            throw new ConfigurationException('AUTHORIZE_NET_API_LOGIN and AUTHORIZE_NET_TRANSACTION_KEY should be set');
        }

        $this->apiLogin = $apiLogin;
        $this->transactionKey = $transactionKey;
        $this->sandBox = $sandBox;
    }

    /**
     * Create a Customer Profile in Authorize.Net CIM
     *
     * @param User $user
     * @return int
     * @throws PaymentGatewayException
     */
    public function createCustomer(User $user): int
    {
        $customerProfile = new \AuthorizeNetCustomer();
        $customerProfile->description = $user->first_name . ' ' . $user->last_name;
        $customerProfile->merchantCustomerId = $user->id;
        $customerProfile->email = $user->email;

        $request = new \AuthorizeNetCIM($this->apiLogin, $this->transactionKey);
        $request->setSandbox($this->sandBox);

        return $this->sendCreateCustomerResponse($request, $customerProfile);
    }

    /**
     * @param $request
     * @param $customerProfile
     * @return int
     * @throws PaymentGatewayException
     */
    protected function sendCreateCustomerResponse($request, $customerProfile)
    {
        $errors = 0;
        do {
            try {
                $response = $request->createCustomerProfile($customerProfile);
                $this->checkCIMResponseAndThrowException($response);

                return (int)$response->getCustomerProfileId();
            } catch (Exception $exception) {
                if ($errors > 2) {
                    throw $exception;
                }
                $errors++;
            }
        } while ($errors <= 3);

    }

    /**
     * Delete a Customer Profile in Authorize.Net CIM
     *
     * @param User $user
     * @return bool
     * @throws PaymentGatewayException
     */
    public function deleteCustomer(User $user): bool
    {
        $request = new \AuthorizeNetCIM($this->apiLogin, $this->transactionKey);
        $request->setSandbox($this->sandBox);


        return $this->sendDeleteCustomerResponse($request, $user->authorize_net_id);
    }

    /**
     * @param $request
     * @param $authorizeNetId
     * @return bool
     * @throws PaymentGatewayException
     */
    protected function sendDeleteCustomerResponse($request, $authorizeNetId)
    {
        $errors = 0;
        do {
            try {
                $response = $request->deleteCustomerProfile($authorizeNetId);
                $this->checkCIMResponseAndThrowException($response);

                return true;
            } catch (Exception $exception) {
                if ($errors > 2) {
                    throw $exception;
                }
                $errors++;
            }
        } while ($errors <= 3);
    }

    /**
     * Create/Update a Payment Profile method in Authorize.Net CIM (Credit Card)
     *
     * @param User $user
     * @param PaymentProfile $paymentProfile
     * @return int
     * @throws PaymentGatewayException
     */
    public function storePaymentProfileForUser(User $user, PaymentProfile $paymentProfile): int
    {
        $cimPaymentProfile = new \AuthorizeNetPaymentProfile();
        $cimPaymentProfile->customerType = 'individual';
        $cimPaymentProfile->payment->creditCard->cardNumber = $paymentProfile->card_number;
        $expirationDate = $paymentProfile->expiration_year . '-' . $paymentProfile->expiration_month;
        $cimPaymentProfile->payment->creditCard->expirationDate = $expirationDate;

        $address = new \AuthorizeNetAddress();
        $address->firstName = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $paymentProfile->first_name);
        $address->lastName = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $paymentProfile->last_name);
        $address->company = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $paymentProfile->company);
        $address->address = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $paymentProfile->address);
        $address->city = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $paymentProfile->city);
        $address->country = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $paymentProfile->country);
        $address->state = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $paymentProfile->state);
        $address->phoneNumber = $paymentProfile->phone;
        $address->zip = $paymentProfile->zip;

        $cimPaymentProfile->billTo = $address;


        $request = new \AuthorizeNetCIM($this->apiLogin, $this->transactionKey);
        $request->setSandbox($this->sandBox);

        return $this->sendPaymentProfileForUserResponse($request, $paymentProfile, $cimPaymentProfile, $user->authorize_net_id);
    }

    /**
     * @param $request
     * @param $paymentProfile
     * @param $cimPaymentProfile
     * @param $authorizeNetId
     * @return int
     * @throws PaymentGatewayException
     */
    protected function sendPaymentProfileForUserResponse($request, $paymentProfile, $cimPaymentProfile, $authorizeNetId)
    {
        $errors = 0;
        do {
            try {
                if ($paymentProfile->id) {
                    $cimPaymentProfile->payment->creditCard->cardNumber =
                        'XXXXXXXXXX' . substr($paymentProfile->card_number,-4);
                    $response = $request->updateCustomerPaymentProfile(
                        $authorizeNetId, $paymentProfile->id, $cimPaymentProfile);

                    $this->checkCIMResponseAndThrowException($response);

                    return $paymentProfile->id;
                }

                $response = $request->createCustomerPaymentProfile($authorizeNetId, $cimPaymentProfile);
                $this->checkCIMResponseAndThrowException($response);

                return (int)$response->getPaymentProfileId();
            } catch (Exception $exception) {
                if ($errors > 2) {
                    throw $exception;
                }
                $errors++;
            }
        } while ($errors <= 3);
    }

    /**
     * Check default Authorize.Net Customer API status code
     *
     * @param \AuthorizeNetCIM_Response $response
     * @throws PaymentGatewayException
     */
    protected function checkCIMResponseAndThrowException(\AuthorizeNetCIM_Response $response)
    {
        if ($response->getResultCode() == 'Error') {
            $errorCode = (int)ltrim($response->getMessageCode(), 'E');
            throw new PaymentGatewayException($response->getMessageText(), $errorCode);
        }
    }

    /**
     * Delete a Payment Profile method in Authorize.Net CIM (Credit Card)
     *
     * @param User $user
     * @param PaymentProfile $paymentProfile
     * @return bool
     * @throws PaymentGatewayException
     */
    public function deletePaymentProfile(User $user, PaymentProfile $paymentProfile): bool
    {
        $request = new \AuthorizeNetCIM($this->apiLogin, $this->transactionKey);
        $request->setSandbox($this->sandBox);

        $response = $request->deleteCustomerPaymentProfile($user->authorize_net_id, $paymentProfile->id);

        $this->checkCIMResponseAndThrowException($response);

        return true;
    }

    /**
     * @param User $user
     * @param PaymentProfile $paymentProfile
     * @param Order $order
     * @param $description
     * @param string $cvv
     * @return array
     */
    public function createTransaction(User $user, PaymentProfile $paymentProfile, Order $order, $description, $cvv = '')
    {
        $transaction = new \AuthorizeNetTransaction();
        $transaction->amount = $order->total_price;
        $transaction->customerProfileId = $user->authorize_net_id;
        $transaction->customerPaymentProfileId = $paymentProfile->id;
        $transaction->order->invoiceNumber = '';
        $transaction->order->description = $description;
        $transaction->order->purchaseOrderNumber = $order->id;

        if (!empty($cvv) && $cvv !== '0011') {
            $transaction->cardCode = $cvv;
        }

        $request = new \AuthorizeNetCIM($this->apiLogin, $this->transactionKey);
        $request->setSandbox($this->sandBox);

        $response = $request->createCustomerProfileTransaction("AuthCapture", $transaction);
        $transactionResponse = $response->getTransactionResponse();

        if ($transactionResponse->approved) {
            return [
                'transaction_id' => $transactionResponse->transaction_id,
                'message' => $transactionResponse->response_reason_text,
                'amount' => $transactionResponse->amount,
            ];
        } else {
            return [
                'error_code' => $transactionResponse->response_reason_code,
                'error_message' => $transactionResponse->response_reason_text,
            ];
        }
    }

    /**
     * @param int $authorizeNetId
     * @param int $paymentProfileId
     * @param int $transactionId
     * @param $orderNumber
     * @param int $amount
     * @return array
     */
    public function createRefundTransaction(
        int $authorizeNetId,
        int $paymentProfileId,
        int $transactionId,
        $orderNumber,
        $amount = 0
    ) {
        $transaction = new \AuthorizeNetTransaction();
        $transaction->amount = number_format($amount, 2);
        $transaction->customerProfileId = $authorizeNetId;
        $transaction->customerPaymentProfileId = $paymentProfileId;
        $transaction->order->invoiceNumber = '';
        $transaction->order->description = 'Credit transaction for order ' . $orderNumber;
        $transaction->order->purchaseOrderNumber = $orderNumber;
        $transaction->transId = $transactionId;

        $request = new \AuthorizeNetCIM($this->apiLogin, $this->transactionKey);
        $request->setSandbox($this->sandBox);

        $response = $request->createCustomerProfileTransaction("Refund", $transaction);
        $transactionResponse = $response->getTransactionResponse();

        if ($transactionResponse->approved) {
            return [
                'transaction_id' => $transactionResponse->transaction_id,
                'message' => $transactionResponse->response_reason_text,
                'amount' => $transactionResponse->amount,
            ];
        } else {
            return [
                'error_code' => $transactionResponse->response_reason_code,
                'error_message' => $transactionResponse->response_reason_text,
            ];
        }
    }
}
