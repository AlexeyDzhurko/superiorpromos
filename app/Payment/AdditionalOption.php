<?php


namespace App\Payment;


class AdditionalOption
{
    public $name;
    public $setupPrice;
    public $itemPrice;

    public function __construct($name, $setupPrice, $itemPrice)
    {
        $this->name = $name;
        $this->setupPrice = round($setupPrice, 2, PHP_ROUND_HALF_UP);
        $this->itemPrice = round($itemPrice, 2, PHP_ROUND_HALF_UP);
    }
}
