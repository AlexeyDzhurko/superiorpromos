<?php


namespace App\Payment;


use App\Models\CartItem;
use App\Models\CartItemImprint;
use App\Models\CartItemProductOption;
use App\Models\CartItemProductSubOption;
use Carbon\Carbon;

class CartItemCost
{
    protected $cartItem;
    protected $additionalOptions = [];
    protected $additionalItemPrice = 0;
    protected $additionalSetupPrice = 0;
    protected $imprints = [];
    protected $imprintsDetails = [];
    protected $productOptions = [];

    public function __construct(CartItem $cartItem)
    {
        $this->cartItem = $cartItem;
        $this->calcImprints();
        $this->calcProductOptions();
    }

    /**
     * Returns shipping cost
     *
     * @return float|int
     */
    public function getShipping()
    {
        return $this->cartItem->estimation_shipping_price;
    }

    /**
     * Returns quantity of items
     *
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->cartItem->quantity;
    }

    /**
     * @return float
     */
    public function getSetupPrice(): float
    {
        if ($this->cartItem->created_at && $this->cartItem->created_at > Carbon::parse('2021-03-30')) {
            return $this->cartItem->setup_price;
        }

        return 0;
    }

    /**
     * Returns per item price without additional options (product options, imprints)
     *
     * @return float
     */
    public function getBaseItemPrice()
    {
        return $this->cartItem->price
            ? ($this->cartItem->price + $this->additionalItemPrice)
            : $this->cartItem->regular_price ;
    }

    /**
     * Returns per item price without additional options (product options, imprints)
     *
     * @return float
     */
    public function getSaleItemPrice()
    {
        return $this->cartItem->price;
    }

    public function getProductOptions()
    {
        return $this->productOptions;
    }

    public function getImprints()
    {
        return $this->imprints;
    }

    /**
     * Returns per item price with additional options (product options, imprints)
     *
     * @return float
     */
    public function getFinalItemPrice()
    {
        return $this->round($this->additionalItemPrice);
    }

    /**
     * Returns final cart item cost
     *
     * @return float|int
     */
    public function getTotal()
    {
        return $this->getTotalWithoutShipping() + $this->getShipping();
    }

    public function getTotalWithoutShipping()
    {
        $total = ($this->getBaseItemPrice()) * $this->getQuantity();
        $total += $this->additionalSetupPrice;

        if ($this->cartItem->created_at && $this->cartItem->created_at > Carbon::parse('2021-03-30')) {
            $total += $this->cartItem->setup_price;
        }

        return $total;
    }

    protected function calcImprints()
    {
        foreach ($this->cartItem->cartItemImprints as $key => $cartItemImprint) {
            /** @var CartItemImprint $cartItemImprint */
            $colorsCount = $cartItemImprint->cartItemImprintColors->count();
            $itemPrice = $cartItemImprint->color_item_price * ($colorsCount - 1) + $cartItemImprint->item_price;
            $setupPrice = $cartItemImprint->color_setup_price * ($colorsCount - 1) + $cartItemImprint->setup_price;
            $additionalOption = new AdditionalOption($cartItemImprint->imprint_name, $setupPrice, $itemPrice);
            $this->additionalItemPrice += $itemPrice;
            $this->additionalSetupPrice += $setupPrice;
            $this->additionalOptions[] = $additionalOption;

            foreach ($cartItemImprint->cartItemImprintColors as $color) {
                $additionalOption->details[] = [
                    'title' => $color->name,
                    'setup_cost' => $cartItemImprint->color_setup_price,
                    'color_item_price' => $cartItemImprint->color_item_price
                ];
            }
            $this->imprints[] = $additionalOption;
        }
    }

    protected function calcProductOptions()
    {
        foreach ($this->cartItem->cartItemProductOptions as $cartItemProductOption) {
            /** @var CartItemProductOption $cartItemProductOption */
            $itemPrice = $cartItemProductOption->item_price;
            $setupPrice = $cartItemProductOption->setup_price;

            foreach ($cartItemProductOption->cartItemProductSubOptions as $cartItemProductSubOption) {
                /** @var CartItemProductSubOption */
                $setupPrice += $cartItemProductSubOption->setup_price;
                $itemPrice += $cartItemProductSubOption->item_price;
            }
            $additionalOption = new AdditionalOption($cartItemProductOption->name, $setupPrice, $itemPrice);
            $this->additionalItemPrice += $itemPrice;
            $this->additionalSetupPrice += $setupPrice;
            $this->additionalOptions[] = $additionalOption;
            $this->productOptions[] = $additionalOption;
        }
    }

    protected function round($amount)
    {
        return round($amount, 2, PHP_ROUND_HALF_UP);
    }

}
