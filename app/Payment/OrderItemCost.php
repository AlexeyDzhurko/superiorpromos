<?php


namespace App\Payment;


use App\Models\CartItemImprint;
use App\Models\CartItemProductOption;
use App\Models\CartItemProductSubOption;
use App\Models\OrderItem;

class OrderItemCost
{
    protected $orderItem;
    protected $additionalOptions = [];
    protected $additionalItemPrice = 0;
    protected $additionalSetupPrice = 0;
    protected $imprints = [];
    protected $productOptions = [];

    public function __construct(OrderItem $orderItem)
    {
        $this->orderItem = $orderItem;
        $this->calcImprints();
        $this->calcProductOptions();
    }

    /**
     * Returns shipping cost
     *
     * @return float|int
     */
    public function getShipping()
    {
        if ($this->orderItem->shipping_method == 'own_account') {
            return 0;
        }

        return $this->orderItem->shipping_price;
    }

    /**
     * Returns quantity of items
     *
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->orderItem->quantity;
    }

    /**
     * Returns per item price without additional options (product options, imprints)
     *
     * @return float
     */
    public function getBaseItemPrice()
    {
        return $this->orderItem->item_price;
    }

    public function getProductOptions()
    {
        return $this->productOptions;
    }

    public function getImprints()
    {
        return $this->imprints;
    }

    /**
     * Returns per item price with additional options (product options, imprints)
     *
     * @return float
     */
    public function getFinalItemPrice()
    {
        return $this->round($this->orderItem->item_price + $this->additionalItemPrice);
    }

    /**
     * Returns final order item cost
     *
     * @return float|int
     */
    public function getTotal()
    {
        return $this->getTotalWithoutShipping() + $this->getShipping();
    }

    public function getTotalWithoutShipping()
    {
        $total = $this->getFinalItemPrice() * $this->getQuantity();
        $total += $this->additionalSetupPrice;
        return $total;
    }

    protected function calcImprints()
    {
        foreach ($this->orderItem->cartItem->cartItemImprints as $cartItemImprint) {
            /** @var CartItemImprint $cartItemImprint */
            $colorsCount = $cartItemImprint->cartItemImprintColors->count();
            $itemPrice = $cartItemImprint->color_item_price * $colorsCount + $cartItemImprint->item_price;
            $setupPrice = $cartItemImprint->color_setup_price * $colorsCount + $cartItemImprint->setup_price;

            $additionalOption = new AdditionalOption($cartItemImprint->imprint_name, $setupPrice, $itemPrice);
             $this->additionalItemPrice += $itemPrice;
            $this->additionalSetupPrice += $setupPrice;
            $this->additionalOptions[] = $additionalOption;
            $this->imprints[] = $additionalOption;
        }

    }

    protected function calcProductOptions()
    {
        foreach ($this->orderItem->cartItem->cartItemProductOptions as $cartItemProductOption) {
            /** @var CartItemProductOption $cartItemProductOption */
            $itemPrice = $cartItemProductOption->item_price;
            $setupPrice = $cartItemProductOption->setup_price;

            foreach ($cartItemProductOption->cartItemProductSubOptions as $cartItemProductSubOption) {
                /** @var CartItemProductSubOption */
                $setupPrice += $cartItemProductSubOption->setup_price;
                $itemPrice += $cartItemProductSubOption->item_price;
            }
            $additionalOption = new AdditionalOption($cartItemProductOption->name, $setupPrice, $itemPrice);
            $this->additionalItemPrice += $itemPrice;
            $this->additionalSetupPrice += $setupPrice;
            $this->additionalOptions[] = $additionalOption;
            $this->productOptions[] = $additionalOption;
        }
    }

    protected function round($amount)
    {
        return round($amount, 2, PHP_ROUND_HALF_UP);
    }
}
