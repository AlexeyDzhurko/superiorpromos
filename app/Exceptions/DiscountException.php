<?php


namespace App\Exceptions;


use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class DiscountException extends BadRequestHttpException
{

}
