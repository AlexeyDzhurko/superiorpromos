<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 16.03.17
 * Time: 15:57
 */

namespace App\Exceptions;


use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class OrderException extends BadRequestHttpException
{

}
