<?php

namespace App\Contracts;

use App\User;
use Illuminate\Http\UploadedFile;

interface TaxExemptionServiceInterface
{
    public function __construct(string $certificateStoragePath);
    public function setCertificate(UploadedFile $certificate);
    public function getCertificateName(): string;
    public function uploadCertificate(): void;
    public function setUser(User $user);
    public function provideTaxExemptionCertificateLater();
}