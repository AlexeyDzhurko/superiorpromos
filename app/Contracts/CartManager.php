<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 07.03.17
 * Time: 13:22
 */

namespace App\Contracts;


use App\Models\CartItem;
use App\Models\Product;
use Illuminate\Http\Request;

interface CartManager
{
    public function buildCart(
        CartItem &$cartItem,
        Product $product,
        Request $request
    );

    public function recalculateCart(CartItem &$cartItem);
    public function setCartItemCost(CartItem &$cart, Product $product, int $quantity);
    public function setCartItemOwner(CartItem &$cart, Request $request);
    public function setCartItemProductOptions(CartItem &$cartItem, Product $product, $productOptionsData, int $quantity);
    public function setCartItemColors(CartItem &$cartItem, Product $product, $productColorsData);
    public function setCartItemImprints(CartItem &$cartItem, Product $product, $imprintsData, int $quantity);
    public function setCartItemSizes(CartItem &$cartItem, Product $product, $sizesData, int $quantity);
    public function setCartItemArtFiles(CartItem &$cartItem, $artFiles);
    public function setCartItemShippingCost(CartItem &$cartItem, Product $product, $zip, int $quantity, $serviceType);
    public function setCartItemOwnAccountFields(CartItem &$cartItem, $type, $number, $system);
    public function setCustomCartItemShippingCost(CartItem &$cartItem, Product $product);
}
