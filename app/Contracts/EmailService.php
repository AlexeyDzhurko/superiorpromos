<?php

namespace App\Contracts;


use App\User;

/**
 * Service for managing email send list
 *
 * Interface EmailService
 * @package App\Contracts
 */
interface EmailService
{
    /**
     * Add user email to Email Service send list
     *
     * @param User $user
     * @return bool
     */
    public function addUser(User $user): bool;

}
