<?php


namespace App\Contracts;


interface Notification
{
    /**
     * Notification constructor.
     * @param array $emails
     * @param array $data
     */
    public function __construct(array $emails = [], array $data = []);

    /**
     * @return string
     */
    public function campaignId();

    /**
     * @return array
     */
    public function toBigMailer();

    /**
     * @return array
     */
    public function emails();
}
