<?php

namespace App\Contracts;
use App\Models\Order;
use App\Models\Payment;
use App\PaymentProfile;
use App\User;

/**
 * Service for working with Payment Gateway (e.g. Authorize.Net, PayPal, etc)
 *
 * Interface PaymentGateway
 * @package App\Contracts
 */
interface PaymentGateway
{
    public function createCustomer(User $user): int;
    public function deleteCustomer(User $user): bool;
    public function storePaymentProfileForUser(User $user, PaymentProfile $paymentProfile): int;
    public function deletePaymentProfile(User $user, PaymentProfile $paymentProfile): bool;
    public function createTransaction(User $user, PaymentProfile $paymentProfile, Order $order, $description, $cvv = '');
    public function createRefundTransaction(int $authorizeNetId, int $paymentProfileId, int $transactionId, $orderNumber, $amount = 0);
}
