<?php


namespace App\Contracts;


interface MailService
{
    /**
     * @param $email
     * @return mixed
     */
    public function createContact($email);

    /**
     * @param $uuid
     * @param $email
     * @param bool $unsubscribe
     * @return mixed
     */
    public function updateContact($uuid, $email, $unsubscribe = false);

    /**
     * @param Notification $notification
     * @return mixed
     */
    public function sendMultipleTransactionalEmail(Notification $notification);

    /**
     * @param Notification $notification
     * @return boolean
     */
    public function sendSingleTransactionalEmail(Notification $notification);
}
