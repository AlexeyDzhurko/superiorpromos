<?php

namespace App\Contracts;


use App\Models\Product;
use Illuminate\Support\Collection;

/**
 * Service for working with UPS
 *
 * Interface ShippingUPS
 * @package App\Contracts
 */
interface ShippingService
{
    public function getShippingRate($zipCode, Product $product, int $quantity): Collection;
}
