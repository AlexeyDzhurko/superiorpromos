<?php


namespace App\Repository;


use App\Models\Subscriber;

class SubscriberRepository
{
    /**
     * @param $email
     * @return Subscriber
     */
    public static function create($email) {
        return Subscriber::create([
            'email' => $email
        ]);
    }

    /**
     * @param Subscriber $subscriber
     * @param $code
     * @return mixed
     */
    public static function setCode($subscriber, $code) {
        $subscriber->coupon = $code;
        $subscriber->save();

        return $subscriber;
    }

    /**
     * @param Subscriber $subscriber
     * @param $uuid
     * @return mixed
     */
    public static function setBigMailerUuid($subscriber, $uuid) {
        $subscriber->bigmailer_uuid = $uuid;
        $subscriber->save();

        return $subscriber;
    }
}
