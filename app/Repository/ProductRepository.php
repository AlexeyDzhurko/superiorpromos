<?php

namespace App\Repository;

use App\Models\Imprint;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductOption;
use App\Models\ProductOptionPrice;
use App\Models\ProductPrice;
use App\Models\ProductSubOption;
use App\Models\ProductSubOptionPrice;
use App\Transformers\ProductTransformer;

class ProductRepository
{
    /**
     * Get list of products with extra images
     * @param int $amount
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getProductsList($amount)
    {
        return Product::with('product_icon')
            ->with('productExtraImages')
            ->with('seoProduct')
            ->with('productOptions')
            ->with('productColorGroups')
            ->with('productColorGroups.productColors')
            ->paginate($amount);
    }

    /**
     * @param int $id
     * @return Product|null
     */
    public static function getProductById($id)
    {
        return Product::where('id', $id)
            ->with('product_icon')
            ->with('productExtraImages')
            ->with('seoProduct')
            ->with('productOptions')
            ->with('productColorGroups')
            ->with('productColorGroups.productColors')
            ->first()
        ;
    }

    /**
     * Get products by ids
     * @param array $ids
     * @return mixed
     */
    public static function getProductsByIds($ids)
    {
        return Product::whereIn('id', $ids)
            ->with('productIcon')
            ->with('seoProduct')
            ->with('productExtraImages')
            ->get()
            ;
    }

    /**
     * @param array $params
     * @param int $perPage
     * @return mixed
     */
    public static function getProductsByParameters(array $params = [], $perPage = 10)
    {
        $products = new Product();

        if (!empty($params)) {
            foreach ($params as $key => $param) {
                switch ($key){
                    case 'vendor':
                        $products = $products->join('product_vendor', 'product_vendor.product_id', '=', 'products.id')
                        ->where('product_vendor.vendor_id', $param);
                        break;
                    case 'categories':
                        $products = $products->where('active', 1)->whereHas('categories', function ($query) use ($param) {
                            $query->whereIn('category_id', $param);
                        });
                        break;
                    default:
                        $products = $products->where($key, $param);
                        break;
                }
            }
        }

        return $products->paginate($perPage);
    }

    /**
     * @param string $id
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public static function getProductBySageId($id)
    {
        return Product::where('sage_id', $id)->first();
    }

    /**
     * Add new product
     * @param $product
     * @param array $categories
     * @param array $data
     * @return Product
     */
    public static function add($product, $categories, $data)
    {
        $id = Product::insertGetId($product);
        $newProduct = Product::find($id);
        $newProduct->categories()->attach($categories);
        $newProduct->vendors()->attach($data['supplier_id'], ['sku' => $data['ItemNum']]);

        return $newProduct;
    }

    /**
     * Get Product by name
     * @param string $query
     * @return array
     */
    public static function getProductByName(string $query):array
    {
        return Product::where('name', 'like', '%' . $query . '%')
            ->get()
            ->toArray();
    }

    /**
     * Get Category by slug
     * @param string $slug
     * @return Product
     */
    public static function getProductBySlug(string $slug)
    {
        return Product::where('url', $slug)->first();
    }

    /**
     * @param Product $product
     * @param $productColorsData
     * @param $quantity
     * @return array
     */
    public static function getProductColors(Product $product, $productColorsData, int $quantity)
    {
        $productColors = [];

        foreach ($product->productColorGroups as $productColorGroup) {
            $currentProductColorGroup = array_first($productColorsData, function($value) use ($productColorGroup) {
                return $value['color_group_id'] == $productColorGroup->id;
            });

            $productColor = ProductColor::find($currentProductColorGroup['color_id']);
            $productColorPrice = (new self())->getProductColorPrice($productColor, $quantity);
            if(empty($productColorPrice)) {
                continue;
            }
            $productColors[] = [
                'title'       => $productColorGroup->name,
                'value'       => $productColor->color->name,
                'setup_price' => $productColorPrice['setup_price'],
                'item_price'  => $productColorPrice['item_price']
            ];
        }

        return $productColors;
    }

    /**
     * @param ProductColor $productColor
     * @param int $quantity
     * @return array
     */
    protected function getProductColorPrice(ProductColor $productColor, int $quantity): array
    {
        $productColorPrices = $productColor->productColorPrices->sortByDesc('quantity');
        foreach ($productColorPrices as $productColorPrice) { /** @var ProductOptionPrice $productColorPrice */
            if ($productColorPrice->quantity <= $quantity) {
                return [
                    'setup_price' => $productColorPrice->setup_price,
                    'item_price'  => $productColorPrice->item_price
                ] ;
            }
        }
        return [];
    }

    /**
     * @param array $colors
     * @return array
     */
    public static function getProductColorsSetupPrices(array $colors)
    {
        $productColorsPrices = [
            'prices_colors' => [],
            'prices_setup'  => [],
            'price_total'   => 0,
            'setup_total'   => 0
        ];

        $setupTotal = 0;
        $priceTotal = 0;

        if (empty($options)) {
            return $productColorsPrices;
        }

        foreach ($colors as $key => $color) {
            $colorSetupPrice = $color['setup_price'];
            $colorItemPrice  = $color['item_price'];

            if($colorItemPrice > 0) {
                $productColorsPrices['prices_colors'][] = [
                    'title' => $color['title'] . ' Additional fee (per item)',
                    'value' => '$' . number_format($colorItemPrice, 2)
                ];
            }

            if($colorSetupPrice > 0) {
                $productColorsPrices['prices_setup'][] = [
                    'title' => $color['title'] . ' Additional fee (per item)',
                    'value' => '$' . number_format($colorSetupPrice, 2)
                ];
            }

            $setupTotal += number_format($colorSetupPrice, 2);
            $priceTotal += number_format($colorItemPrice, 2);
        }

        $productColorsPrices['price_total'] = $priceTotal;
        $productColorsPrices['setup_total'] = $setupTotal;

        return $productColorsPrices;
    }

    /**
     * @param Product $product
     * @return array
     */
    public static function getProductInfo(Product $product) {
        $outProduct = fractal()->item($product, new ProductTransformer())->toArray();
        $img = '/img/image-not-available.jpg';

        if(!empty($outProduct['product_extra_images'])) {
            $path = explode('/', $outProduct['product_extra_images'][0]['medium']);
            unset($path[0],$path[1],$path[2]);
            $img = implode('/', $path);
        }

        return [
            'id' => $outProduct['id'],
            'title' => strip_tags($outProduct['title']),
            'description' => $outProduct['description'],
            'img' => $img
        ];
    }

    /**
     * @param Product $product
     * @param int $quantity
     * @return array
     */
    public static function getProductCost(Product $product, int $quantity) {
        $productPrice = [];
        $prices = $product->productPrices->sortByDesc('quantity');

        foreach ($prices as $price) {
            if ($price->quantity <= $quantity) {
                $itemPrice = (new self())->getProductPrice($product, $price);
                $productPrice = [
                    'price'         => $itemPrice,
                    'regular_price' => $price->item_price,
                    'setup_price'   => $price->setup_price,
                    'is_sale'       => $product->on_sale,
                    'quantity'      => $quantity,
                    'total'      =>  $quantity * $itemPrice
                ];

                return  $productPrice;
            }
        }

        return  $productPrice;
    }

    /**
     * @param Product $product
     * @param ProductPrice $price
     * @return float
     */
    protected function getProductPrice(Product $product, ProductPrice $price)
    {
        return $product->on_sale ? $price->sale_item_price : $price->item_price;
    }

    /**
     * @param array $prices
     * @return array
     */
    public static function getProductSetupPrices(array $prices) {
        $productPrices = [
            'product_prices' => [],
            'prices_setup'  => [],
            'price_total'   => 0,
            'setup_total'   => 0
        ];

        if (empty($prices)) {
            return $productPrices;
        }

        $setupTotal = number_format($prices['setup_price'], 2) ?: 0;
        $priceTotal = number_format($prices['price'], 2) ?: 0;

        $productPrices['product_prices'][] = [
            'title' => ' Quantity',
            'value' => number_format($prices['quantity'], 2)
        ];
        $productPrices['product_prices'][] = [
            'title' => ' Base Unit Price',
            'value' => '$'.number_format($prices['price'],2)
        ];

        if($prices['setup_price'] > 0) {
            $productPrices['prices_setup'][] = [
                'title' => 'Item Setup',
                'value' => '$' . number_format($prices['setup_price'], 2)
            ];
        }

        $productPrices['price_total'] = $priceTotal;
        $productPrices['setup_total'] = $setupTotal;

        return $productPrices;
    }

    /**
     * @param Product $product
     * @param $productOptionsData
     * @param int $quantity
     * @return array
     */
    public static function getProductOptions(Product $product, $productOptionsData, int $quantity)
    {
        $productOptions = [];
        $currentProductOptions = ProductOption::whereHas('productSubOptions')
            ->where(['product_id' => $product->id])->get();

        foreach ($currentProductOptions as $key => $productOption) {
            $currentProductOption = array_first($productOptionsData, function($value) use ($productOption) {
                return $value['product_option_id'] == $productOption->id;
            });

            if (is_null($currentProductOption)) {
                continue;
            }

            $productOptionPrice = (new self())->getProductOptionPrice($productOption, $quantity);

            if(empty($productOptionPrice)) {
                continue;
            }

            $productSubOptionIds = $currentProductOption['product_sub_option_ids'];

            $productOptions[] = [
                'title'       => $productOption->name,
                'setup_price' => $productOptionPrice['setup_price'],
                'item_price'  => $productOptionPrice['item_price']
            ];

            if(!is_null($productSubOptionIds)) {
                foreach ($productSubOptionIds as $productSubOptionId) {
                    /** @var ProductSubOption $productSubOption */
                    $productSubOption = $productOption->productSubOptions->find($productSubOptionId);
                    $productSubOptionPrice = (new self())->getProductSubOptionPrice($productSubOption, $quantity);
                    if(empty($productOptionPrice)) {
                        continue;
                    }
                    $productOptions[$key]['sub_option'][] = [
                        'title'       => $productSubOption->name,
                        'setup_price' => $productSubOptionPrice['setup_price'],
                        'item_price'  => $productSubOptionPrice['item_price']
                    ];
                }
            }
        }

        return $productOptions;
    }

    /**
     * @param ProductOption $productOption
     * @param int $quantity
     * @return array
     */
    protected function getProductOptionPrice(ProductOption $productOption, int $quantity): array
    {
        $productOptionPrices = $productOption->productOptionPrices->sortByDesc('quantity');
        foreach ($productOptionPrices as $productOptionPrice) { /** @var ProductOptionPrice $productOptionPrice */
            if ($productOptionPrice->quantity <= $quantity) {
                return [
                    'setup_price' => $productOptionPrice->setup_price,
                    'item_price'  => $productOptionPrice->item_price
                ];
            }
        }
        return [];
    }

    /**
     * @param ProductSubOption $productSubOption
     * @param int $quantity
     * @return array
     */
    protected function getProductSubOptionPrice(ProductSubOption $productSubOption, int $quantity): array
    {
        $productSubOptionPrices = $productSubOption->productSubOptionPrices->sortByDesc('quantity');
        foreach ($productSubOptionPrices as $productSubOptionPrice) { /** @var ProductSubOptionPrice $productSubOptionPrice */
            if ($productSubOptionPrice->quantity <= $quantity) {
                return [
                    'setup_price' => $productSubOptionPrice->setup_price,
                    'item_price'  => $productSubOptionPrice->item_price
                ];
            }
        }
        return [];
    }

    /**
     * @param array $options
     * @return array
     */
    public static function getProductOptionsSetupPrices(array $options)
    {
        $productOptionsPrices = [
            'prices_options' => [],
            'prices_setup'  => [],
            'price_total'   => 0,
            'setup_total'   => 0
        ];
        $setupTotal = 0;
        $priceTotal = 0;

        if (empty($options)) {
            return $productOptionsPrices;
        }

        foreach ($options as $key => $option) {
            $optionSetupPrice = $option['setup_price'];
            $optionItemPrice  = $option['item_price'];

            if(!empty($option['sub_option'])) {
                foreach ($option['sub_option'] as $subOption) {
                    $optionSetupPrice += $subOption['setup_price'];
                    $optionItemPrice  += $subOption['item_price'];
                }

                if($optionItemPrice > 0) {
                    $productOptionsPrices['prices_options'][] = [
                        'title' => $option['title'] . ' Additional fee (per item)',
                        'value' => '$' . number_format($optionItemPrice, 2)
                    ];
                }

                if($optionSetupPrice > 0) {
                    $productOptionsPrices['prices_setup'][] = [
                        'title' => $option['title'] . ' Setup',
                        'value' => '$' . number_format($optionSetupPrice, 2)
                    ];
                }

                $setupTotal += number_format($optionSetupPrice, 2);
                $priceTotal += number_format($optionItemPrice, 2);
            }
        }

        $productOptionsPrices['price_total'] = $priceTotal;
        $productOptionsPrices['setup_total'] = $setupTotal;

        return $productOptionsPrices;
    }

    /**
     * @param Product $product
     * @param $imprintsData
     * @param int $quantity
     * @return array
     */
    public static function getProductImprints(Product $product, $imprintsData, int $quantity)
    {
        $productImprints = [];

        foreach ($product->imprints as $imprint) {
            $currentImprint = array_first((array)$imprintsData, function($value) use ($imprint) {
                return $value['imprint_id'] == $imprint->id;
            });

            if (is_null($currentImprint)) {
                continue;
            }

            $countColors = count($currentImprint['color_ids']) ?? 0;

            $imprintPrice = (new self())->getImprintPrice($imprint, $quantity);
            if(empty($imprintPrice)) {
                continue;
            }
            $productImprints[] = [
                'title'             => $imprint->name,
                'setup_price'       => $imprintPrice['setup_price'],
                'item_price'        => $imprintPrice['item_price'],
                'color_setup_price' => $imprintPrice['color_setup_price'],
                'color_item_price'  => $imprintPrice['color_item_price'],
                'count_colors'      => $countColors,
            ];
        }

        return $productImprints;
    }

    /**
     * @param Imprint $imprint
     * @param int $quantity
     * @return array
     */
    protected function getImprintPrice(Imprint $imprint, int $quantity): array
    {
        $imprintPrices = $imprint->imprintPrices->sortByDesc('quantity');
        foreach ($imprintPrices as $imprintPrice) {
            if ($imprintPrice->quantity <= $quantity) {
                return [
                    'setup_price'       => $imprintPrice->setup_price,
                    'item_price'        => $imprintPrice->item_price,
                    'color_setup_price' => $imprintPrice->color_setup_price,
                    'color_item_price'  => $imprintPrice->color_item_price,
                ];
            }
        }

        return [];
    }

    /**
     * @param array $imprints
     * @return array
     */
    public static function getProductImprintsSetupPrices(array $imprints)
    {
        $productImprintsPrices = [
            'prices_imprints' => [],
            'prices_setup'  => [],
            'price_total'   => 0,
            'setup_total'   => 0
        ];
        $setupTotal = 0;
        $priceTotal = 0;

        if (empty($imprints)) {
            return $productImprintsPrices;
        }

        foreach ($imprints as $key => $imprint) {
            $imprintSetupPrice = $imprint['setup_price'];
            $imprintItemPrice = $imprint['item_price'];
            $imprintSetupColorsPrice = $imprint['color_setup_price'] * ($imprint['count_colors'] - 1);
            $imprintTotalColorsPrice = $imprint['color_item_price'] * ($imprint['count_colors'] - 1);

            $setupTotal += number_format($imprintSetupPrice + $imprintSetupColorsPrice, 2);
            $priceTotal += number_format($imprintItemPrice + $imprintTotalColorsPrice, 2);

            if ($imprintItemPrice > 0) {
                $productImprintsPrices['prices_imprints'][] = [
                    'title' => $imprint['title'] . ' Additional fee (per item)',
                    'value' => '$' . number_format($imprintItemPrice, 2)
                ];
            }

            if ($imprintTotalColorsPrice > 0) {
                $productImprintsPrices['prices_imprints'][] = [
                    'title' => $imprint['title'] . ' Additional Color fee (per item)',
                    'value' => '$' . number_format($imprintTotalColorsPrice, 2)
                ];
            }

            if ($imprintSetupPrice > 0) {
                $productImprintsPrices['prices_setup'][] = [
                    'title' => $imprint['title'] . ' Setup',
                    'value' => '$' . number_format($imprintSetupPrice, 2)
                ];
            }

            if ($imprintSetupColorsPrice > 0) {
                $productImprintsPrices['prices_setup'][] = [
                    'title' => $imprint['title'] . ' Additional Color Setup',
                    'value' => '$' . number_format($imprintSetupColorsPrice, 2)
                ];
            }

        }

        $productImprintsPrices['price_total'] = $priceTotal;
        $productImprintsPrices['setup_total'] = $setupTotal;

        return $productImprintsPrices;
    }
}
