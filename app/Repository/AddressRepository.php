<?php

namespace App\Repository;

use App\Models\Address;
use Auth;

class AddressRepository
{
    /**
     * Set address as default
     *
     * @param Address $address
     * @return bool
     */
    public static function setDefault(Address $address)
    {
        Address::where('type', $address->type)
            ->where('user_id', Auth::id())
            ->update(['is_default' => Address::COMMON_ADDRESS]);

        $address->is_default = Address::DEFAULT_ADDRESS;

        return $address->save();
    }
}