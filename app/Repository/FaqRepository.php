<?php

namespace App\Repository;

use App\Models\Faq;

class FaqRepository
{
    /**
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getFaqsByParameters(array $params = [])
    {
        $perPage = env('MAX_PER_PAGE', 10);
        $faqs = new Faq();

        if (!empty($params)) {
            foreach ($params as $key => $param) {
                $faqs = $faqs->where($key, $param);
            }
        }

        return $faqs->paginate($perPage);
    }
}