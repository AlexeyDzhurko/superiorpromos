<?php

namespace App\Repository;

use App\Models\ProductPrice;
use App\Models\Product;

class ProductPriceRepository
{
    /**
     * @param array $values
     * @param Product $product
     * @return array
     */
    public static function batch(array $values, Product $product)
    {
        foreach ($values as $value) {
            $productPrice = new ProductPrice();
            $productPrice->product()->associate($product);

            $productPrice->quantity = $value['quantity'];
            $productPrice->item_price = $value['item_price'];
            $productPrice->sale_item_price = $value['sale_item_price'];

            $productPrice->save();
        }
    }
}