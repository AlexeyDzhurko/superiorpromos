<?php

namespace App\Repository;

use App\Models\Size;

class SizeRepository
{
    /**
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getSizesByParameters(array $params = [])
    {
        $perPage = env('MAX_PER_PAGE', 10);
        $sizes = new Size();

        if (!empty($params)) {
            foreach ($params as $key => $param) {
                $sizes = $sizes->where($key, $param);
            }
        }

        return $sizes->paginate($perPage);
    }
}