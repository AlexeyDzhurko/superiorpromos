<?php

namespace App\Repository;

use App\Models\Vendor;

class VendorRepository
{
    /**
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getVendorsByParameters(array $params = [])
    {
        $perPage = env('MAX_PER_PAGE', 10);
        $vendors = new Vendor();

        if (!empty($params)) {
            foreach ($params as $key => $param) {
                switch ($key){
                    case 'id':
                        $vendors = $vendors->where('id', '=', $param);
                        break;
                    case 'active':
                        $vendors = $vendors->where('active', '=', $param);
                        break;
                    case 'name':
                        $vendors = $vendors->where('name', 'like', $param . '%');
                        break;
                    case 'createdFrom':
                        $vendors = $vendors->where('created_at', '>=', $param);
                        break;
                    case 'createdTo':
                        $vendors = $vendors->where('created_at', '<=', $param);
                        break;
                }
            }

            if(isset($params['order']) && !empty($params['order'])) {
                $direction = isset($params['orderDirection']) ? $params['orderDirection'] : 'ASC';
                $vendors = $vendors->orderBy($params['order'], $direction);
            }

        }

        return $vendors->paginate($perPage);
    }

    public static function getByProductId($productId)
    {
        return Vendor::selectRaw('vendors.*')
            ->join('product_vendor', 'product_vendor.vendor_id', '=', 'vendors.id')
            ->where('product_vendor.product_id', $productId)
            ->get();
    }

    /**
     * @param string $type
     * @param string $query
     * @return array
     */
    public static function searchVendor($type = 'name', $query)
    {
        return Vendor::where($type, 'like', '%' . $query . '%')
            ->limit(300)
            ->get(['id', 'name', 'supplier_id'])
            ->toArray();
    }

    /**
     * Insert or update value
     * @param array $data
     */
    public static function insertOrUpdate($data)
    {
        if (!Vendor::where('supplier_id', $data['supplier_id'])->first()) {
            Vendor::create($data);
        } else {
            Vendor::where('supplier_id', $data['supplier_id'])->update($data);
        }
    }

    /**
     * @param array $data
     * @param Vendor $vendor
     */
    public static function syncImportProduct($data, Vendor $vendor)
    {
        $vendor->importProducts()->sync($data);
    }

    /**
     * @param string|int $supplier_id
     * @return mixed
     */
    public static function getVendorById($supplier_id)
    {
        return Vendor::where('supplier_id', $supplier_id)->first();
    }
}
