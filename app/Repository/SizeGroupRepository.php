<?php

namespace App\Repository;

use App\Models\SizeGroup;

class SizeGroupRepository
{
    /**
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getGroupByParameters(array $params = [])
    {
        $perPage = env('MAX_PER_PAGE', 10);
        $groups = new SizeGroup();

        if (!empty($params)) {
            foreach ($params as $key => $param) {
                $groups = $groups->where($key, $param);
            }
        }

        return $groups->paginate($perPage);
    }

    /**
     * Get size's ids array
     * @param SizeGroup $sizeGroup
     * @return array
     */
    public static function getSizeIdsArray(SizeGroup $sizeGroup)
    {
        $result = [];
        $sizes = $sizeGroup->sizes()->get();

        foreach ($sizes as $size) {
            $result[] = $size->id;
        }

        return $result;
    }

    /**
     * Update size group
     * @param SizeGroup $sizeGroup
     * @param array $data
     */
    public static function updateSizeGroup(SizeGroup $sizeGroup, array $data)
    {
        $sizeGroup->name = $data['name'];
        $sizeGroup->addSizes($data['sizes']);

        $sizeGroup->save();
    }

    /**
     * Insert new size group to db
     * @param array $data
     */
    public static function insertSizeGroup(array $data)
    {
        $sizeGroup = SizeGroup::insertGetId(['name' => $data['name']]);
        SizeGroup::find($sizeGroup)->addSizes($data['sizes']);
    }
}