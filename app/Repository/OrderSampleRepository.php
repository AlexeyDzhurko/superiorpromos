<?php

namespace App\Repository;

use App\Models\OrderSample;
use App\Models\Product;
use App\Models\OrderSampleItem;
use App\Models\OrderSampleItemColors;
use App\Models\Stage;
use Auth;

class OrderSampleRepository
{
    /**
     * @param int $id
     * @return OrderSample|null
     */
    public static function getOrderSampleById($id)
    {
        return OrderSample::where('id', $id)
            ->with('user')
            ->first()
        ;
    }

    /**
     * Get list of products with extra images
     * @return OrderSample
     */
    public static function getOrderSampleByUser()
    {
        return OrderSample::whereUserId(Auth::id())
            ->orderBy('created_at', 'desc')
            ->get();
    }

    /**
     * @param array $params
     * @return mixed
     */
    public static function getOrderSampleByParameters(array $params = [])
    {
        $perPage = env('MAX_PER_PAGE', 10);
        $orderSample = new OrderSample();

        if (!empty($params)) {
            foreach ($params as $key => $param) {
                $orderSample = $orderSample->where($key, $param);
            }
        }

        return $orderSample->paginate($perPage);
    }
    
    /**
     * Add new order a sample
     * @param $requestData
     * @return OrderSample
     */
    public static function add($requestData)
    {
        $user = Auth::user();
        $orderSample = new OrderSample();
        $orderSample->user()->associate($user);
        $orderSample->total_price = 0;
        $orderSample->save();

        $shippingAddress = $user->addresses->where('type', 1)->first();
        $billingAddress = $user->addresses->where('type', 2)->first() ?: $shippingAddress;

        $addressFields = [
            'first_name',
            'middle_name',
            'last_name',
            'title',
            'suffix',
            'company_name',
            'address_line_1',
            'address_line_2',
            'city',
            'state',
            'zip',
            'country',
            'province',
            'day_telephone'
        ];

        foreach ($requestData['products'] as $product) {
            $stage = Stage::where('name', '=', 'New Sample Request')->first();
            $currentProduct = Product::find($product['product_id']);
            $orderSampleItems = new OrderSampleItem();
            $orderSampleItems->product()->associate($currentProduct);
            $orderSampleItems->stage()->associate($stage);
            $orderSampleItems->orderSample()->associate($orderSample);
            $orderSampleItems->save();
            foreach ($product['selected_colors'] as $productColors) {
                $orderSampleItemColors = new OrderSampleItemColors($productColors);
                $orderSampleItemColors->orderSampleItem()->associate($orderSampleItems);
                $orderSampleItemColors->save();
            }
            $orderSampleItems->quantity = 1;
            $orderSampleItems->price = $currentProduct->productPrices->max('item_price');

            foreach ($addressFields as $addressField) {
                $orderSampleItems->setAttribute('shipping_' . $addressField, $shippingAddress->getAttribute($addressField));
                $orderSampleItems->setAttribute('billing_' . $addressField, $billingAddress->getAttribute($addressField));
            }

            $orderSampleItems->save();
            $orderSample->total_price += $orderSampleItems->price;
        }
        $orderSample->shipping_method = $requestData['shipping_method'];
        if ($requestData['shipping_method'] == 'own_account') {
            $orderSample->own_shipping_type = $requestData['shipping_data']['own_shipping_type'];
            $orderSample->own_account_number = $requestData['shipping_data']['own_account_number'];
            $orderSample->own_shipping_system = $requestData['shipping_data']['own_shipping_system'];
        }
        $orderSample->upcoming_event = $requestData['additionalInfo']['upcomingEvent'];
        $orderSample->sample_comment =$requestData['additionalInfo']['sampleComment'] ?? null;
        $orderSample->sample = $requestData['additionalInfo']['sample'];
        $orderSample->frequency = $requestData['additionalInfo']['frequency'];
        $orderSample->quantity = $requestData['additionalInfo']['quantity'];
        $orderSample->event_date = $requestData['additionalInfo']['event_date'];
        $orderSample->attraction = json_encode($requestData['additionalInfo']['attraction']);
        $orderSample->save();
        
        
        
        return $orderSample;
    }
}
