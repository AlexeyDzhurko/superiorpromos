<?php

namespace App\Repository;

use App\Models\ColorGroup;

class ColorGroupRepository
{
    /**
     * Get Colors Group by name
     * @param string $query
     * @return array
     */
    public static function getColorGroupsByName(string $query):array
    {
        return ColorGroup::where('name', 'like', '%' . $query . '%')
            ->get()
            ->toArray();
    }

    /**
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getColorGroupsByParameters(array $params = [])
    {
        $perPage = env('MAX_PER_PAGE', 10);
        $colors = new ColorGroup();

        if (!empty($params)) {
            foreach ($params as $key => $param) {
                $colors = $colors->where($key, $param);
            }
        }

        return $colors->paginate($perPage);
    }

    /**
     * @param array $data
     */
    public static function insertColorGroup(array $data)
    {
        $colorGroup = ColorGroup::insertGetId([
            'name' => $data['name'],
        ]);

        if ( isset($data['colors']) ) {
            ColorGroup::find($colorGroup)->addColors($data['colors']);
        }
    }

    /**
     * @param ColorGroup $colorGroup
     * @param array $data
     */
    public static function updateColorGroup(ColorGroup $colorGroup, array $data)
    {
        $colorGroup->name = $data['name'];
        if (isset($data['colors'])) {
            $colorGroup->addColors($data['colors']);
        }

        $colorGroup->save();
    }

    /**
     * @param array $colors
     * @param string $name
     * @param int $productId
     * @return int
     */
    public static  function add($colors, $name, $productId)
    {
        $groupId = ColorGroup::insertGetId([
            'product_id' => $productId,
            'name' => $name
        ]);

        ColorGroup::find($groupId)->addColors($colors);

        return (int)$groupId;
    }
}