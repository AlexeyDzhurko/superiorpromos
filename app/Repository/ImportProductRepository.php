<?php

namespace App\Repository;

use App\Models\ImportProduct;

class ImportProductRepository
{
    /**
     * @param Vendor $vendor
     * @return array
     */
    public static function getVendorProducts($vendor)
    {
        $result = [];
        $products = ImportProduct::where('vendor_id', $vendor)->get();
        foreach ($products as $product) {
            $result[] = $product->import_product_id;
        }

        return $result;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getAllImportProducts()
    {
        return ImportProduct::where('status', 0)->limit(500)->get();
    }

}