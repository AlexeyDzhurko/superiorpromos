<?php

namespace App\Repository;

use App\Models\PromotionalBlock;

class PromotionalBlockRepository
{
    /**
     * Store Promotional Blocks
     *
     */
    public static function storePromotionalBlocks(array $blocksData)
    {
        foreach($blocksData as $blockId => $blockData) {
            $promotionalBlock = PromotionalBlock::find($blockId);
            $promotionalBlock->fill($blockData);
            if (isset($blockData['category'])) $promotionalBlock->category()->associate($blockData['category']);
            $promotionalBlock->save();
            $promotionalBlock->products()->sync(isset($blockData['products']) ? $blockData['products'] : []);
        }
    }

}