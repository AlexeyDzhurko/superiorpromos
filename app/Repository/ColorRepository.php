<?php

namespace App\Repository;

use App\Models\Color;

class ColorRepository
{
    /**
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getColorsByParameters(array $params = [])
    {
        $perPage = env('MAX_PER_PAGE', 10);
        $colors = new Color();

        if (!empty($params)) {
            foreach ($params as $key => $param) {
                $colors = $colors->where($key, $param);
            }
        }

        return $colors->paginate($perPage);
    }

    /**
     * @param array $data
     */
    public static function insertColor(array $data)
    {
        $exist_color = Color::where('name', $data['name'])->first();

        if(is_null($exist_color)) {
            $color = Color::insertGetId([
                'name' => $data['name'],
                'color_hex' => $data['color_hex'],
                'picture_src' => $data['picture_src']->originalName ?? null,
                'pms_color_id' => $data['pms_color_id'] ?? null,
                'picture_width' => !isset($data['picture_width']) || empty($data['picture_width'])? null:$data['picture_width'],
                'picture_height' => !isset($data['picture_height']) || empty($data['picture_height'])? null:$data['picture_height'],
            ]);

            if ( isset($data['groups']) ) {
                Color::find($color)->addGroups($data['groups']);
            }
        }
    }

    /**
     * @param Color $color
     * @param array $data
     */
    public static function updateColor(Color $color, array $data)
    {
        $color->name = $data['name'];
        $color->color_hex = $data['color_hex'];
        if (isset($data['picture_src'])) {
            $color->picture_src = $data['picture_src'];
        }
        $color->pms_color_id = $data['pms_color_id'] ?? null;
        $color->picture_width = !isset($data['picture_width']) || empty($data['picture_width'])? null:$data['picture_width'];
        $color->picture_height = !isset($data['picture_height']) || empty($data['picture_height'])? null:$data['picture_height'];
        if (isset($data['groups'])) {
            $color->addGroups($data['groups']);
        }

        $color->save();
    }

    /**
     * Get colors by name
     * @param string $query
     * @return array
     */
    public static function getColorsByName(string $query):array
    {
        return Color::where('name', 'like', '%' . $query . '%')
            ->get()
            ->toArray();
    }

    /**
     * Get colors by name
     * @param array $query
     * @return array
     */
    public static function findColorsByName(array $query):array
    {
        $colors = Color::whereIn('name', $query)
            ->get()
            ->toArray();
        $result = [];

        foreach ($colors as $color) {
            $result[$color['name']] = $color;
        }

        return $result;
    }

    /**
     * @param array $names
     * @return \Illuminate\Support\Collection
     */
    public static function getColorIdsByName(array $names)
    {
        return Color::whereIn('name', $names)->get()->pluck('id')->toArray();
    }

    /**
     * @return array
     */
    public static function list():array
    {
        return Color::get()->toArray();
    }

    /**
     * @param $colors
     * @return int
     */
    public static function countColors($colors)
    {
        return Color::whereIn('name', $colors)->count();
    }

}