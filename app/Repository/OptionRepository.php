<?php

namespace App\Repository;

use App\Models\Option;

class OptionRepository
{
    /**
     * Get list of options with suboptions
     * @param int $productId
     * @return array
     */
    public static function getOptionsListByProductId($productId)
    {
        return Option::join('products_options', 'options.id', '=', 'products_options.option_id')
            ->where('products_options.product_id', $productId)
            ->with('suboptions')
            ->get()
        ;
    }
}