<?php

namespace App\Repository;

use App\Models\Role;
use App\User;

class UserRepository
{
    // Admin User Section

    /**
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getAdminUsersByParameters(array $params = [])
    {
        $perPage = env('MAX_PER_PAGE', 10);
        $users = User::with('roles')->whereHas('roles', function($q){
            $q->whereIn('id', [Role::TYPE_ADMINISTRATOR, Role::TYPE_MANAGER]);
        });

        if (!empty($params)) {
            foreach ($params as $key => $param) {
                $users = $users->where($key, $param);
            }
        }

        return $users->paginate($perPage);
    }

    /**
     * Store Admin User
     *
     * @param array $params
     * @return User
     */
    public static function storeAdminUser(array $params)
    {
        $user = new User();
        $user->fill($params);
        $user->password = bcrypt($params['password']);
        $user->active = $params['active'];
        $user->can_delete = $params['can_delete'];
        $user->save();
        $user->roles()->sync([$params['role']]);

        return $user;
    }

    /**
     * @param array $params
     * @param $id
     * @return User|User[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public static function updateAdminUser(array $params, $id)
    {
        $user = User::find($id);

        $user->fill($params);

        if(isset($params['password'])) {
            $user->password = bcrypt($params['password']);
        }

        $user->active = $params['active'];
        $user->can_delete = $params['can_delete'];
        $user->save();
        $user->roles()->sync([$params['role']]);

        return $user;
    }

    //Customer section

    /**
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getCustomerByParameters(array $params = [])
    {
        $perPage = env('MAX_PER_PAGE', 10);
        $users = new User();

        if (!empty($params)) {
            foreach ($params as $key => $param) {
                $users = $users->where($key, $param);
            }
        }

        return $users->paginate($perPage);
    }

    /**
     * Store Admin User
     *
     * @param array $params
     */
    public static function storeCustomer(array $params)
    {
        $user = new User();
        $user->fill($params);
        if (isset($params['password']) && !empty($params['password'])) {
            $user->forceFill(['password' => bcrypt($params['password'])]);
        }
        $user->active = (isset($params['active'])) ? 1 : 0;
        $user->subscribe = (isset($params['subscribe'])) ? 1 : 0;
        $user->tax_exempt = (isset($params['tax_exempt'])) ? 1 : 0;
        $user->save();
        $user->roles()->sync([Role::TYPE_CUSTOMER]);
    }

    /**
     * @param array $params
     * @param $id
     */
    public static function updateCustomer(array $params, $id)
    {
        $user = User::find($id);
        $user->fill($params);
        if (isset($params['password']) && !empty($params['password'])) {
            $user->forceFill(['password' => bcrypt($params['password'])]);
        }
        $user->active = (isset($params['active'])) ? 1 : 0;
        $user->subscribe = (isset($params['subscribe'])) ? 1 : 0;
        $user->tax_exempt = (isset($params['tax_exempt'])) ? 1 : 0;
        $user->save();
    }
}
