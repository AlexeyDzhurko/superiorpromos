<?php

namespace App\Repository;

use App\Models\ImprintLocation;
use App\Models\Product;

class ImprintLocationRepository
{
    /**
     * @param array $values
     * @param Product $product
     * @return array
     */
    public static function batch(array $values, Product $product)
    {
        $data = [];
        foreach ($values as $value) {
            array_push($data, ImprintLocation::insertGetId($value));

        }
        $product->imprintLocations()->sync($data);
    }
}