<?php

namespace App\Repository;

use App\Models\Category;
use App\Models\Product;

class CategoryRepository
{
    /**
     * @param array $params
     * @param int $perPage
     * @param int $page
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getCategoriesByParameters(array $params = [], $perPage = 10, $page = 1)
    {
        $categories = new Category();

        if (!empty($params)) {
            foreach ($params as $key => $param) {
                $categories = $categories->where($key, $param);
            }
        }

        return $categories->paginate($perPage, ['*'], 'page', $page);
    }

    /**
     * @param boolean $withZero
     * @return \Illuminate\Database\Query\Builder
     */
    public static function getAllCategoriesWithChild($withZero = true)
    {
        $categories = Category::all()->toHierarchy();
        $data = Category::treeToList($categories);

        if ($withZero) {
            $result = [
                [
                    'id' => 0,
                    'text' => 'Parent category',
                    'nested' => 0
                ]
            ];
        } else {
            $result = [];
        }

        foreach ($data as $item) {
            $result[] = [
                'id' => $item['id'],
                'text' => $item['text'],
                'nested' => $item['nested'],
            ];
        }

        return $result;
    }

    /**
     * @return \Baum\Array
     */
    public static function getAllCategories()
    {
        return Category::all()->toHierarchy()->toArray();
    }

    /**
     * @return Category[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAllActiveCategories()
    {
        return Category::where('is_quick_link', 0)
            ->where('active', 1)
            ->where('depth', 0)
            ->orderBy('lft')
            ->get();
    }

    /**
     * Insert new category to db
     * @param array $data
     */
    public static function insertCategory(array $data)
    {
        $category = Category::create([
            'name' => $data['name'],
            'slug' => $data['slug'] ?? null,
            'meta_title' => $data['meta_title'] ?? null,
            'meta_description' => $data['meta_description'] ?? null,
            'keywords' => $data['keywords'] ?? null,
            'title' => $data['title'] ?? null,
            'description' => $data['description'] ?? null,
            'footer_text' => $data['footer_text'] ?? null,
            'froogle_description' => $data['froogle_description'] ?? null,
            'active' => $data['active'] ?? false,
            'is_quick_link' => $data['is_quick_link'] ?? 0,
            'is_popular' => $data['is_popular'] ?? 0,
        ]);

        if (isset($data['image'])) {
            $fullPath = str_random(20) . '.' . $data['image']->getClientOriginalExtension();
            \Storage::disk('public')->put(Category::IMAGES_DIR . $fullPath,  file_get_contents($data['image']));
            $category->image_src = $fullPath;
            $category->save();
        }

        /** if exist parent category */
        if (!is_null($data['parent']) && (integer)$data['parent_id'] !== 0) {
            $root = Category::find($data['parent_id']);
            $category->makeChildOf($root);
        }

        return $category;
    }

    /**
     * Insert new category to db
     * @param Category $category
     * @param array $data
     */
    public static function updateCategory(Category $category, array $data)
    {
        $category->name = $data['name'];
        if (isset($data['slug'])) {
            $category->name = $data['slug'];
        }

        if (isset($data['image'])) {
            if (isset($category->image_src)) \Storage::disk('public')->delete(Category::IMAGES_DIR . $category->image_src);
            $fullPath = str_random(20) . '.' . $data['image']->getClientOriginalExtension();
            \Storage::disk('public')->put(Category::IMAGES_DIR . $fullPath,  file_get_contents($data['image']));
            $category->image_src = $fullPath;
            $category->save();
        }

        $category->name = $data['name'] ?? null;
        $category->slug = $data['slug'] ?? null;
        $category->meta_title = $data['meta_title'] ?? null;
        $category->meta_description = $data['meta_description'] ?? null;
        $category->keywords = $data['keywords'] ?? null;
        $category->title = $data['title'] ?? null;
        $category->description = $data['description'] ?? null;
        $category->footer_text = $data['footer_text'] ?? null;
        $category->active = $data['active'] ?? false;
        $category->is_quick_link = $data['is_quick_link'] ?? 0;
        $category->froogle_description = $data['froogle_description'] ?? null;
        $category->is_popular = $data['is_popular'] ?? 0;

        $category->save();

        /** if exist parent category */
        if (!is_null($data['parent_id']) && (integer)$data['parent_id'] !== $category->id) {
            if ((integer)$data['parent_id'] === 0) {
                $category->makeRoot();
            } else {
                $root = Category::find($data['parent_id']);
                $category->makeChildOf($root);
            }
        }

        return $category;
    }

    /**
 * Get Category by name
 * @param string $query
 * @return array
 */
    public static function getCategoryByName(string $query) : array
    {
        return Category::where('name', 'like', '%' . $query . '%')
            ->get()
            ->toArray();
    }

    /**
     * Get Category by slug
     * @param string $slug
     * @return Category
     */
    public static function getCategoryBySlug(string $slug)
    {
        return Category::where('slug', $slug)->first();
    }
}
