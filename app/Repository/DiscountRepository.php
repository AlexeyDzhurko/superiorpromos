<?php


namespace App\Repository;


use App\Models\Discount;

class DiscountRepository
{
    /**
     * @param array $params
     * @return mixed
     */
    public static function create(array $params) {
        $discount = Discount::firstOrCreate([
            'name' => $params['name'],
            'type' => $params['type'],
        ],[
            'discount_condition' => $params['discount_condition'],
            'discount_calc' => $params['discount_calc'],
            'code' => $params['code']
        ]);

        return $discount;
    }
}
