<?php

namespace App\Repository;

use App\Models\Testimonial;

class TestimonialRepository
{
    /**
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getTestimonialsByParameters(array $params = [])
    {
        $perPage = env('MAX_PER_PAGE', 10);
        $testimonials = new Testimonial();

        if (!empty($params)) {
            foreach ($params as $key => $param) {
                $testimonials = $testimonials->where($key, $param);
            }
        }

        return $testimonials->paginate($perPage);
    }

    /**
     * Store Testimonial
     *
     * @param array $params
     * @param string $image
     * @return Testimonial
     */
    public static function storeTestimonial(array $params, string $image)
    {
        $testimonial = new Testimonial();
        $testimonial->fill($params);
        $testimonial->active = (isset($params['active'])) ? 1 : 0;
        $testimonial->image = $image;
        $testimonial->save();
        return $testimonial;
    }

    /**
     * Update Testimonial
     *
     * @param array $params
     * @param $id
     * @return Testimonial
     */
    public static function updateTestimonial(array $params, $id, $image = null)
    {
        $testimonial = Testimonial::find($id);
        $testimonial->fill($params);
        if ($image) {
            unlink(public_path() . '/uploads/' . $testimonial->image);
            $testimonial->image = $image;
        }
        $testimonial->active = (isset($params['active'])) ? 1 : 0;
        $testimonial->save();
        return $testimonial;
    }
}