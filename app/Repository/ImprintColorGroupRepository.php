<?php

namespace App\Repository;

use App\Models\ImprintColorGroup;

class ImprintColorGroupRepository
{
    /**
     * Get Colors Group by name
     * @param string $query
     * @return array
     */
    public static function getColorGroupsByName(string $query):array
    {
        return ImprintColorGroup::where('name', 'like', '%' . $query . '%')
            ->get()
            ->toArray();
    }

    /**
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getColorGroupsByParameters(array $params = [])
    {
        $perPage = env('MAX_PER_PAGE', 10);
        $colors = new ImprintColorGroup();

        if (!empty($params)) {
            foreach ($params as $key => $param) {
                $colors = $colors->where($key, $param);
            }
        }

        return $colors->paginate($perPage);
    }

    /**
     * @param array $data
     */
    public static function insertColorGroup(array $data)
    {
        $colorGroup = ImprintColorGroup::insertGetId([
            'name' => $data['name'],
        ]);

        if ( isset($data['colors']) ) {
            ImprintColorGroup::find($colorGroup)->addColors($data['colors']);
        }
    }

    /**
     * @param ImprintColorGroup $colorGroup
     * @param array $data
     */
    public static function updateColorGroup(ImprintColorGroup $colorGroup, array $data)
    {
        $colorGroup->name = $data['name'];
        if (isset($data['colors'])) {
            $colorGroup->addColors($data['colors']);
        }

        $colorGroup->save();
    }
}