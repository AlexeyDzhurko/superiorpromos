<?php

namespace App\Admin;

use Closure;

class FormFieldBuilder
{
    protected $fields = [];

    public static function init()
    {
        return new self();
    }

    /**
     * add text input
     *
     * @param string $field
     * @param string|null $displayName
     * @param bool $required
     * @param Closure|null $afterCreate
     * @return FormFieldBuilder
     */
    public function text(string $field, string $displayName = null, bool $required = true, Closure $afterCreate = null)
    {
        return $this->add($field, FormField::TYPE_TEXT, $displayName, $required, $afterCreate);
    }

    /**
     * add bool input
     *
     * @param string $field
     * @param string|null $displayName
     * @param bool $required
     * @param Closure|null $afterCreate
     * @return FormFieldBuilder
     */
    public function bool(string $field, string $displayName = null, bool $required = true, Closure $afterCreate = null)
    {
        return $this->add($field, FormField::TYPE_BOOL, $displayName, $required, $afterCreate);
    }

    /**
     * add WYSIWYG editor
     *
     * @param string $field
     * @param string|null $displayName
     * @param bool $required
     * @param Closure|null $afterCreate
     * @return FormFieldBuilder
     */
    public function richTextarea(string $field, string $displayName = null, bool $required = true, Closure $afterCreate = null)
    {
        return $this->add($field, FormField::TYPE_RICH_TEXTAREA, $displayName, $required, $afterCreate);
    }

    /**
     * add textarea
     *
     * @param string $field
     * @param string|null $displayName
     * @param bool $required
     * @param Closure|null $afterCreate
     * @return FormFieldBuilder
     */
    public function textarea(string $field, string $displayName = null, bool $required = true, Closure $afterCreate = null)
    {
        return $this->add($field, FormField::TYPE_TEXTAREA, $displayName, $required, $afterCreate);
    }

    /**
     * add select box
     *
     * @param string $field
     * @param string|null $displayName
     * @param bool $required
     * @param Closure|null $afterCreate
     * @return FormFieldBuilder
     */
    public function select(string $field, string $displayName = null, bool $required = true, Closure $afterCreate = null)
    {
        return $this->add($field, FormField::TYPE_SELECT, $displayName, $required, $afterCreate);
    }

    /**
     * add image
     *
     * @param string $field
     * @param string|null $displayName
     * @param bool $required
     * @param Closure|null $afterCreate
     * @return FormFieldBuilder
     */
    public function image(
        string $field,
        string $displayName = null,
        bool $required = true,
        Closure $afterCreate = null
    )
    {
        return $this->add($field, FormField::TYPE_IMAGE, $displayName, $required, $afterCreate);
    }

    /**
     * add integer
     *
     * @param string $field
     * @param string|null $displayName
     * @param bool $required
     * @param Closure|null $afterCreate
     * @return FormFieldBuilder
     */
    public function integer(
        string $field,
        string $displayName = null,
        bool $required = true,
        Closure $afterCreate = null
    )
    {
        return $this->add($field, FormField::TYPE_INTEGER, $displayName = null, $required, $afterCreate);
    }

    /**
     * add checkbox list
     *
     * @param string $field
     * @param string|null $displayName
     * @param bool $required
     * @param Closure|null $afterCreate
     * @return FormFieldBuilder
     */
    public function checkboxList(
        string $field,
        string $displayName = null,
        bool $required = true,
        Closure $afterCreate = null
    )
    {
        return $this->add($field, FormField::TYPE_CHECKBOX_LIST, $displayName = null, $required, $afterCreate);
    }

    /**
     * add link
     *
     * @param string $field
     * @param string|null $displayName
     * @param bool $required
     * @param string $link
     * @param Closure|null $afterCreate
     * @return FormFieldBuilder
     */
    public function link(
        string $field,
        string $displayName = null,
        bool $required = true,
        string $link = '',
        Closure $afterCreate = null
    )
    {
        return $this->add($field, FormField::TYPE_LINK, $displayName = null, $required, $afterCreate, $link);
    }

    /**
     * add collection
     *
     * @param string $field
     * @param string|null $displayName
     * @param bool $required
     * @param string $link
     * @param Closure|null $afterCreate
     * @return FormFieldBuilder
     */
    public function collection(
        string $field,
        string $displayName = null,
        bool $required = true,
        string $link = '',
        Closure $afterCreate = null
    )
    {
        return $this->add($field, FormField::TYPE_COLLECTION, $displayName = null, $required, $afterCreate, $link);
    }

    /**
     * add checkbox
     *
     * @param string $field
     * @param string|null $displayName
     * @param bool $required
     * @param Closure|null $afterCreate
     * @return FormFieldBuilder
     */
    public function checkbox(
        string $field,
        string $displayName = null,
        bool $required = true,
        Closure $afterCreate = null
    ){
        return $this->add($field, FormField::TYPE_CHECKBOX, $displayName, $required, $afterCreate);
    }

    public function password(
        string $field,
        string $displayName = null,
        bool $required = true,
        Closure $afterCreate = null
    ){
        return $this->add($field, FormField::TYPE_PASSWORD, $displayName, $required, $afterCreate);
    }

    /**
     * add custom form field
     *
     * @param string $field
     * @param string $type
     * @param string|null $displayName
     * @param bool $required
     * @param Closure|null $afterCreate
     * @param string $link
     * @return $this
     */
    public function add(
        string $field,
        string $type = FormField::TYPE_TEXT,
        string $displayName = null,
        bool $required = true,
        Closure $afterCreate = null,
        string $link = ''
    )
    {
        $field = new FormField($field, $type, $displayName, $required, $link);
        if ($afterCreate) {
            $afterCreate($field);
        }
        $this->fields[] = $field;
        return $this;
    }

    /**
     * return form fields
     *
     * @return array
     */
    public function build(): array
    {
        return $this->fields;
    }
}
