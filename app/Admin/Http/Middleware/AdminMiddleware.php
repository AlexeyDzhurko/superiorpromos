<?php

namespace App\Admin\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guest()) {
            return redirect(route('admin.login'));
        }

        $user = User::find(Auth::user()->id);
        if ($user->hasRole('admin') || $user->hasRole('manager')) {
            return $next($request);
        }

        return redirect('/');
    }
}
