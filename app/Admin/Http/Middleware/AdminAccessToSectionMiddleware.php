<?php


namespace App\Admin\Http\Middleware;


use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class AdminAccessToSectionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::find(Auth::user()->id);

        if ($user->hasRole('admin')) {
            return $next($request);
        }

        Auth::logout();
        return redirect(route('admin.login'));
    }
}
