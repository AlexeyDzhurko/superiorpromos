<?php

namespace App\Admin\Http\Requests;

use App\Http\Requests\Request;

class CategoryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                => 'required',
            'parent'              => 'required|nullable',
            'meta_title'          => 'string|nullable',
            'meta_description'    => 'string|nullable',
            'keywords'            => 'string|nullable',
            'title'               => 'string|nullable',
            'description'         => 'string|nullable',
            'footer_text'         => 'string|nullable',
            'froogle_description' => 'string|nullable',
            'active'              => 'boolean|nullable',
            'image'               => 'image|required_if:is_popular,1|required_if:is_quick_link,1',
        ];
    }

    public function messages()
    {
        return [
            'image.required_if' => 'The :attribute field is required if category :other',
        ];
    }
}
