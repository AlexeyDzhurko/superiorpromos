<?php

namespace App\Admin\Http\Requests;

use App\Http\Requests\Request;

class CreateCustomerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|min:6|unique:users',
            'password' => 'required|password|confirmed|min:6',
            'active' => 'boolean',
            'authorize_net_id' => 'nullable|max:255',
            'contact_telephone' => 'phone',
            'roles.*' => 'exists:roles,id'
        ];
    }
}
