<?php


namespace App\Admin\Http\Requests;

use App\Http\Requests\Request;

class UpdateProductRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'url' => 'string|max:255',
            'meta_title' => 'nullable|string|max:255',
            'keywords' => 'nullable|string|max:255',
            'image_alt' => 'nullable|string|max:255',
            'google_product_category' => 'nullable|string|max:255',
            'brand' => 'nullable|string|max:255',
            'gender' => 'nullable|string|max:255',
            'age_group' => 'nullable|string|max:255',
            'material' => 'nullable|string|max:255',
            'pattern' => 'nullable|string|max:255',
            'meta_description' => 'nullable|string',
            'vendors' => 'array|exists:vendors,id',
            'quantity_per_box' => 'required|numeric',
            'box_weight' => 'required|between:0.01,999999.99',
            'shipping_additional_type' => 'in:absolute,percentage,null',
            'shipping_additional_value' => 'nullable|numeric',
            'production_time_from' => 'nullable|numeric',
            'production_time_to' => 'nullable|numeric',
            'sage_id' => 'required|string|unique:products,sage_id,' . $this->get('id'),
            'zip_from' => 'nullable|zip',
            'dimensions' => 'nullable|string|max:255',
            'popularity' => 'nullable|numeric',
            'description' => 'nullable|string',
            'imprint_area' => 'nullable|string',
            'pricing_information' => 'nullable|string',
            'active' => 'nullable|boolean',
            'on_sale' => 'nullable|boolean',
            'images.*' => 'image',
            'product_icon_id' => 'nullable|exists:product_icons,id',
            'size_group_id' => 'nullable|exists:size_groups,id',
            'video_url' => 'nullable|string',
        ];
    }

}
