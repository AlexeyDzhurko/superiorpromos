<?php

namespace App\Admin\Http\Requests;

use App\Http\Requests\Request;

class UpdateTestimonialRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_name' => 'required|max:255',
            'position_after' => 'required|numeric',
            'active' => 'bool',
            'name' => 'required|max:255',
            'content' => 'required|max:255',
            'product_link' => 'required|url|max:255',
            'image' => 'file|mimes:jpeg,bmp,png',
        ];
    }
}
