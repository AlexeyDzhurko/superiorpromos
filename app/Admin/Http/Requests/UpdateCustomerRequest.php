<?php

namespace App\Admin\Http\Requests;

use App\Http\Requests\Request;

class UpdateCustomerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|min:6',
            'password' => 'min:6|password',
            'active' => 'boolean',
            'subscribe' => 'boolean',
            'tax_exempt' => 'boolean',
            'authorize_net_id' => 'max:255',
            'contact_telephone' => 'phone',
            'ext' => 'max:3',
            'certificate' => 'file|mimes:jpeg,bmp,png,pdf',
            'roles.*' => 'exists:roles,id'
        ];
    }
}
