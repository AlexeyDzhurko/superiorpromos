<?php

namespace App\Admin\Http\Requests;

use App\Http\Requests\Request;

class SageProductDetailRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all()
    {
        $data = parent::all();
        $data['product_id'] = $this->route('product_id');
        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required|numeric'
        ];
    }
}
