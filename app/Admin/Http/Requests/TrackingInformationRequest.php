<?php

namespace App\Admin\Http\Requests;

use App\Http\Requests\Request;

class TrackingInformationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tracking_shipping_date' => 'required|date',
            'tracking_number' => 'required|string',
            'tracking_shipping_company' => 'required|string',
            'tracking_note' => 'string',
            'tracking_notify_customer' => 'boolean',
            'tracking_request_rating' => 'boolean',
        ];
    }
}
