<?php

namespace App\Admin\Http\Requests;

use App\Http\Requests\Request;

class SageQuickSearchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'keywords' => 'required|string',
            'current_page' => 'digits_between:1,50|numeric',
            'items_per_page' => 'digits_between:1,100|numeric'
        ];
    }
}
