<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 03.04.17
 * Time: 0:11
 */

namespace App\Admin\Http\Requests;

use App\Http\Requests\Request;


class UpdateOrderItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(
            $this->getAddressFields('shipping'),
            $this->getAddressFields('billing'),
            [
                'auto_remind' => 'boolean',
                'check_notes' => 'boolean',
                'not_paid' => 'boolean',
                'invoice_notify_customer' => 'boolean|nullable',
                'invoice_file' => 'file|nullable',
                'is_important' => 'boolean',
                'vendor_id' => 'nullable|exists:vendors,id'
            ]
        );
    }

    public function getAddressFields($prefix)
    {
        return [
            $prefix . '_first_name' => 'max:255',
            $prefix . '_last_name' => 'max:255',
            $prefix . '_middle_name' => 'max:255',
            $prefix . '_title' => 'max:255',
            $prefix . '_suffix' => 'max:255',
            $prefix . '_company_name' => 'max:255',
            $prefix . '_address_line_1' => 'max:255',
            $prefix . '_address_line_2' => 'max:255',
            $prefix . '_city' => 'max:255',
            $prefix . '_state' => 'max:255',
            $prefix . '_zip' => 'zip',
            $prefix . '_country' => 'max:255',
            $prefix . '_province' => 'max:255',
            $prefix . '_day_telephone' => 'phone',
        ];
    }
}
