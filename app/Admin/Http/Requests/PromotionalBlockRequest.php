<?php

namespace App\Admin\Http\Requests;

use App\Http\Requests\Request;
use App\Models\PromotionalBlock;

class PromotionalBlockRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            PromotionalBlock::TOP_BLOCK    . '.title' => 'required|string',
            PromotionalBlock::MIDDLE_BLOCK . '.title' => 'required|string',
            PromotionalBlock::BOTTOM_BLOCK . '.title' => 'required|string',
            PromotionalBlock::TOP_BLOCK    . '.slug' => 'required|string',
            PromotionalBlock::MIDDLE_BLOCK . '.slug' => 'required|string',
            PromotionalBlock::BOTTOM_BLOCK . '.slug' => 'required|string',
            PromotionalBlock::TOP_BLOCK    . '.products' => 'array',
            PromotionalBlock::MIDDLE_BLOCK . '.products' => 'array',
            PromotionalBlock::BOTTOM_BLOCK . '.products' => 'array',
            PromotionalBlock::TOP_BLOCK    . '.category' => 'string',
            PromotionalBlock::MIDDLE_BLOCK . '.category' => 'string',
            PromotionalBlock::BOTTOM_BLOCK . '.category' => 'string',
        ];
    }
}
