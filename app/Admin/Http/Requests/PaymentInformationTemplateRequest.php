<?php


namespace App\Admin\Http\Requests;


use App\Http\Requests\Request;

class PaymentInformationTemplateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'single_body' => 'required|string',
            'multiple_body' => 'required|string',
        ];
    }

}
