<?php

namespace App\Admin\Http\Requests;

use App\Http\Requests\Request;

class CreatePostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'slug' => 'required|string|max:255',
            'seo_title' => 'string|max:255',
            'excerpt' => 'string|required',
            'body' => 'string|required',
            'meta_description' => 'string|max:255',
            'meta_keywords' => 'string|max:255',
            'image' => 'image',
            'status' => 'required|in:published,draft,pending',
            'post_category_id' => 'required|numeric|exists:post_categories,id'
        ];
    }
}
