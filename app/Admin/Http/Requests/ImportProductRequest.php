<?php

namespace App\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImportProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @todo need add some rule for this request
     * @return array
     */
    public function rules()
    {
        return [
            'colorMode' => 'required',
            'imprintPriceGrid' => 'required',
            'mainCategory' => 'required',
            'priceGridPercent' => 'required',
            'products' => 'required',
            'sale' => 'required',
            'products.*.supplier_id' => 'required',
        ];
    }
}
