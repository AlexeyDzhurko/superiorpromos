<?php


namespace App\Admin\Http\Controllers;


use Blade;

class SiteMapController extends BaseAdminController
{
    public function index()
    {
        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');

        return view('admin.Sitemap.index');
    }
}
