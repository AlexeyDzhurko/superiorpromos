<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Entities\SizeEntity;
use App\Repository\SizeRepository;
use Symfony\Component\HttpFoundation\Request;

class SizeController extends BaseAdminController
{
    /**
     * Get list of sizes
     * @param Request $request
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $page = 1)
    {
        $adminSizeEntity = new SizeEntity();
        $params = $this->formedParams($request->all());

        $dataTypeContent = SizeRepository::getSizesByParameters($params);

        return view('admin.Size.index', [
            'dataTypeContent' => $dataTypeContent,
            'adminModel' => $adminSizeEntity,
            'parameters' => $params,
        ]);
    }
}
