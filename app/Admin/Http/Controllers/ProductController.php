<?php

namespace App\Admin\Http\Controllers;

use App\Models\Product;
use App\Repository\ProductRepository;
use App\Admin\Entities\ProductEntity;
use Blade;
use Illuminate\Http\Request;

class ProductController extends BaseAdminController
{
    /**
     * @param Request $request
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $page = 1)
    {
        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');
        $adminEntity = new ProductEntity();
        $params = $this->formedParams($request->all());

        $dataTypeContent = ProductRepository::getProductsByParameters($params);

        return view('admin.dashboard.product.index', [
            'dataTypeContent' => $dataTypeContent,
            'adminModel' => $adminEntity,
            'parameters' => $params
        ]);
    }

    public function edit(Product $product)
    {
        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');

        return view('admin.Product.edit-add', [
            'collections' => [],
            'product' => $product,
        ]);
    }

    public function create()
    {
        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');

        return view('admin.Product.edit-add', [
            'collections' => [],
        ]);
    }

}
