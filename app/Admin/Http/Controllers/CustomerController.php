<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Entities\CustomerEntity;
use App\Admin\Http\Requests\CreateCustomerRequest;
use App\Admin\Http\Requests\UpdateCustomerRequest;
use App\Repository\UserRepository;
use App\User;
use Symfony\Component\HttpFoundation\Request;
use JWTAuth;
use Blade;

class CustomerController extends BaseAdminController
{
    /**
     * @param Request $request
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $page = 1)
    {
        $adminEntity = new CustomerEntity();
        $params = $this->formedParams($request->all());

        $dataTypeContent = UserRepository::getCustomerByParameters($params);

        return view('admin.Customer.index', [
            'dataTypeContent' => $dataTypeContent,
            'adminModel' => $adminEntity,
            'parameters' => $params
        ]);
    }

    /**
     * Save new item to database
     *
     * @param CustomerEntity $adminEntity
     * @param CreateCustomerRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CustomerEntity $adminEntity, CreateCustomerRequest $request)
    {
        UserRepository::storeCustomer($request->all());

        return redirect(route('admin_entity.index', ['admin_entity' => $adminEntity->getBaseRouteName()]))->with([
            'message' => 'Successfully Added New ' . $adminEntity->getDisplayNameSingular(),
            'alert-type' => 'success'
        ]);
    }

    public function edit(User $user)
    {
        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');

        return view('admin.Customer.edit-add', [
            'customer' => $user->load('addresses', 'payment_profiles'),
            'collections' => []
        ]);
    }

    /**
     * Update existed item
     *
     * @param CustomerEntity $adminEntity
     * @param UpdateCustomerRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CustomerEntity $adminEntity, UpdateCustomerRequest $request, $id)
    {
        UserRepository::updateCustomer($request->all(), $id);

        return redirect(route('admin_entity.index', ['admin_entity' => $adminEntity->getBaseRouteName()]))->with([
            'message' => 'Successfully Updated ' . $adminEntity->getDisplayNameSingular(),
            'alert-type' => 'success'
        ]);
    }

    /**
     * Login by customer
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginByCustomer(User $user)
    {
        $token = JWTAuth::fromUser($user);
        return response()->json(compact('token'));
    }
}
