<?php

namespace App\Admin\Http\Controllers;

use App\Models\ColorGroup;
use App\Repository\ColorGroupRepository;
use App\Admin\Entities\ColorGroupEntity;
use Symfony\Component\HttpFoundation\Request;
use App\Admin\Http\Requests\ColorGroupRequest;

class ColorGroupController extends BaseAdminController
{
    /**
     * @param Request $request
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $page = 1)
    {
        $adminEntity = new ColorGroupEntity();
        $params = $this->formedParams($request->all());

        $dataTypeContent = ColorGroupRepository::getColorGroupsByParameters($params);

        return view('admin.ColorGroup.index', [
            'dataTypeContent' => $dataTypeContent,
            'adminModel' => $adminEntity,
            'parameters' => $params
        ]);
    }

    /**
     * Return page with edit item form
     *
     * @param ColorGroup $colorGroup
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(ColorGroup $colorGroup)
    {
        $adminEntity = new ColorGroupEntity();

        return view('admin.ColorGroup.edit-add', [
            'adminModel' => $adminEntity,
            'dataTypeContent' => $colorGroup,
            'collections' => $this->formedCollection($colorGroup)
        ]);
    }

    /**
     * Return page with create item form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $adminEntity = new ColorGroupEntity();

        return view('admin.ColorGroup.edit-add', [
            'adminModel' => $adminEntity,
            'dataTypeContent' => [],
            'collections' => []
        ]);
    }

    /**
     * Save new Color to database
     *
     * @param ColorGroupRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ColorGroupRequest $request)
    {
        $adminEntity = new ColorGroupEntity();

        ColorGroupRepository::insertColorGroup($request->all());

        return redirect(route('color_group.index'))
            ->with([
                'message' => 'Successfully Added New ' . $adminEntity->getDisplayNameSingular(),
                'alert-type' => 'success'
            ]);
    }

    /**
     * Update existed Color
     *
     * @param ColorGroupRequest $request
     * @param ColorGroup $colorGroup
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ColorGroupRequest $request, ColorGroup $colorGroup)
    {
        $adminEntity = new ColorGroupEntity();

        ColorGroupRepository::updateColorGroup($colorGroup, $request->all());

        return redirect(route('color_group.index'))
            ->with([
                'message' => 'Successfully Updated ' . $adminEntity->getDisplayNameSingular(),
                'alert-type' => 'success'
            ]);
    }

    /**
     * Search color by name
     * @param Request $request
     * @return array
     */
    public function search(Request $request)
    {
        $query = $request->get('query');
        $data = ColorGroupRepository::getColorGroupsByName($query);

        return $data;
    }

    /**
     * @param ColorGroup $colorGroup
     * @return array
     */
    private function formedCollection(ColorGroup $colorGroup)
    {
        $result = [];
        $data = $colorGroup->colors()->get();

        foreach ($data as $item) {
            $result[] = [
                'id' => $item->id,
                'text' => $item->name,
            ];
        }

        return $result;
    }
}
