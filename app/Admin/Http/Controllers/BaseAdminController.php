<?php

namespace App\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Search\Searchable;
use App\Search\SearchBuilder;
use Illuminate\Http\Request;
use Elasticsearch;

class BaseAdminController extends Controller
{
    const DEFAULT_API_PER_PAGE = 100;
    const DEFAULT_DASHBOARD_PER_PAGE = 20;

    /**
     * Prepare params
     *
     * @param array $data
     * @return array
     */
    protected function formedParams(array $data): array
    {
        $result = [];
        $requestKeys = ['_url', 'page'];

        foreach ($data as $key => $param) {
            if (!in_array($key, $requestKeys)) {
                $result[$key] = $param;
            }
        }

        return $result;
    }

    protected function elasticSearch(Request $request, $searchableName): SearchBuilder
    {
        $search = SearchBuilder::createBuilder($searchableName);

        $mapping = call_user_func($searchableName . '::getElasticMapping');
        foreach ($mapping as $field => $fieldCondition) {
            switch ($field) {
                case 'id':
                    if ($request->has($field)) {
                        $search->term($field, $request->get($field));
                    }
                    break;
                case 'created_at':
                    if ($request->has(['date_from', 'date_to'])) {
                        $search->between($field, $request->date_from, $request->date_to);
                    }
                    break;
            }
        }

        if ($request->has('order')) {
            // check exist not analyzed rows for sorting in model
            if(method_exists($searchableName, 'getElasticSortRows') and
                in_array($request->get('order'), $searchableName::getElasticSortRows())) {
                $search->order($request->get('order') . '.sort', $request->get('orderDirection', 'asc'));
            } else {
                $search->order($request->get('order'), $request->get('orderDirection', 'asc'));
            }
        } else {
            $search->order('id', 'asc');
        }

        return $search;
    }

    /**
     * Update sortable elastic search documents without triggering update event
     *
     * @param Searchable $firstModel
     * @param Searchable $secondModel
     */
    protected function elasticMove(Searchable $firstModel, Searchable $secondModel)
    {
        $oldPosition = $firstModel->position;
        $newPosition = $secondModel->position;

        if ($oldPosition === $newPosition) {
            return;
        }

        $isMoveForward = $oldPosition < $newPosition;
        if ($isMoveForward) {
            Elasticsearch::updateByQuery([
                'index' => $firstModel::getIndexName(),
                'type' => $firstModel::getIndexName(),
                'body' => [
                    'query' => ['range' => ['position' => ['gt' => $oldPosition, 'lt' => $newPosition]]],
                    'script' => [
                        'inline' => 'ctx._source.position--'
                    ]
                ]
            ]);
        } else {
            Elasticsearch::updateByQuery([
                'index' => $firstModel::getIndexName(),
                'type' => $firstModel::getIndexName(),
                'body' => [
                    'query' => ['range' => ['position' => ['gt' => $newPosition, 'lt' => $oldPosition]]],
                    'script' => [
                        'inline' => 'ctx._source.position++'
                    ]
                ]
            ]);
        }
    }

    protected function elasticRemoveByIndex(Searchable $model)
    {
        Elasticsearch::DeleteByQuery([
            'index' => $model::getIndexName(),
            'type' => $model::getIndexName(),
            'body' => [
                'query' => ['term' => ['id' => $model->id]],
            ]
        ]);

    }
}
