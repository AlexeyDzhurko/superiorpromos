<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PageController extends BaseAdminController
{
    public function index()
    {
        $pages = Page::all();

        return $pages;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'slug' => 'required|string',
            'body' => 'required|string',
            'meta_title' => 'required|string',
            'meta_description' => 'required|string',
            'meta_keywords' => 'required|string',
            'status' => 'required|boolean',
        ]);

        $page = new Page($request->only([
            'title',
            'slug',
            'body',
            'meta_title',
            'meta_description',
            'meta_keywords',
            'status',
        ]));
        $page->save();

        return $page;
    }

    public function update(Request $request, Page $page)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'slug' => 'required|string',
            'body' => 'required|string',
            'meta_title' => 'required|string',
            'meta_description' => 'required|string',
            'meta_keywords' => 'required|string',
            'status' => 'required|boolean',
        ]);

        $page->fill($request->only([
            'title',
            'slug',
            'body',
            'meta_title',
            'meta_description',
            'meta_keywords',
            'status',
        ]));
        $page->save();

        return $page;
    }

    public function delete(Page $page)
    {
        $page->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

}