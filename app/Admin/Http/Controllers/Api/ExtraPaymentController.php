<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 24.04.17
 * Time: 13:00
 */

namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Contracts\PaymentGateway;
use App\Enums\PaymentStatusEnum;
use App\Enums\PaymentTypeEnum;
use App\Models\OrderItem;
use App\Models\Payment;
use App\PaymentProfile;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Http\Response;

class ExtraPaymentController extends BaseAdminController
{

    /**
     * @param Request $request
     * @param OrderItem $orderItem
     * @param PaymentGateway $paymentGateway
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Symfony\Component\HttpFoundation\Response
     */
    public function other(Request $request, OrderItem $orderItem, PaymentGateway  $paymentGateway)
    {
        $this->validate($request, [
            'payment_profile_id' => 'required|exists:payment_profiles,id',
            'extra_amount' => 'required|numeric',
            'credit_amount' => 'numeric|nullable',
            'notify_customer' => 'required|boolean',
            'notify_message' => 'string',
        ]);

        $order = $orderItem->order;
        $paymentProfile = PaymentProfile::find($request->get('payment_profile_id'));

        $payment = new Payment($request->only([
            'notify_customer',
            'notify_message',
        ]));

        $creditAmount = $request->get('credit_amount', 0);
        $extraAmount = $request->get('extra_amount', 0);
        $price = $extraAmount - $creditAmount;
        $order->total_price = floatval($extraAmount);
        $order->discount_amount = floatval($creditAmount);

        $transactionResponse = $paymentGateway->createTransaction($order->user, $paymentProfile, $order, 'Additional Billing (Shipping/Other)');

        if (!isset($transactionResponse['transaction_id'])) {
            return response('Sorry, transaction declined', Response::HTTP_BAD_REQUEST);
        }

        $payment->credit_amount = $creditAmount;
        $payment->price = $price;
        $payment->payment_profile_id = $paymentProfile->id;
        $payment->status = $price > 0 ? PaymentStatusEnum::STATUS_PAID : PaymentStatusEnum::STATUS_NOT_PAID;
        $payment->transaction_id = $transactionResponse['transaction_id'];
        $payment->type = PaymentTypeEnum::OTHER_TYPE;
        $payment->orderItem()->associate($orderItem);
        $payment->user()->associate(Auth::user());

        try {
            $payment->save();
        } catch (\Exception $e) {
            return response('Something Wrong', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Request $request
     * @param OrderItem $orderItem
     * @param PaymentGateway $paymentGateway
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Symfony\Component\HttpFoundation\Response
     */
    public function credit(Request $request, OrderItem $orderItem, PaymentGateway $paymentGateway)
    {
        $this->validate($request, [
            'payment_id' => 'required|exists:payments,id',
            'credit_amount' => 'required|numeric',
            'notify_customer' => 'required|boolean',
            'notify_message' => 'string',
        ]);

        $customerPayment = Payment::find($request->input('payment_id'));

        $payment = new Payment($request->only([
            'notify_customer',
            'notify_message',
            'credit_amount'
        ]));

        $user = $customerPayment->user ?? $customerPayment->order->user;
        $transactionResponse = $paymentGateway->createRefundTransaction(
            $user->authorize_net_id,
            $customerPayment->payment_profile_id,
            $customerPayment->transaction_id,
            $customerPayment->order_id,
            $request->input('credit_amount', 0)
        );

        if (!isset($transactionResponse['transaction_id'])) {
            return response('Sorry, transaction declined', Response::HTTP_BAD_REQUEST);
        }

        $payment->payment_profile_id = $customerPayment->payment_profile_id;
        $payment->transaction_id = $transactionResponse['transaction_id'];
        $payment->status = PaymentStatusEnum::STATUS_PROCESSED;
        $payment->type = PaymentTypeEnum::CREDIT_TYPE;
        $payment->over_fees = '-' . $request->input('credit_amount');
        $payment->credit_amount = $request->input('credit_amount');

        $payment->orderItem()->associate($orderItem);
        $payment->user()->associate(Auth::user());

        try {
            $payment->save();
        } catch (\Exception $e) {
            return response('Something Wrong', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response('', Response::HTTP_NO_CONTENT);
    }

}
