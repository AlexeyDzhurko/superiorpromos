<?php

namespace App\Admin\Http\Controllers\Api;

use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Http\Requests\FAQRequest;
use App\Models\Faq;

class FAQController extends BaseAdminController
{
    public function index()
    {
        return Faq::paginate(10);
    }

    /**
     * @param FAQRequest $request
     * @return Faq
     */
    public function store(FAQRequest $request)
    {
        return Faq::create($request->all());
    }

    /**
     * @param FAQRequest $request
     * @param Faq $faq
     * @return Faq
     */
    public function update(FAQRequest $request, Faq $faq)
    {
        $faq->update($request->all());

        return $faq;
    }

    /**
     * @param Faq $faq
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy(Faq $faq)
    {
        try {
            if($faq->delete()) {
                return response('success', 204);
            } else {
                return response('error on the server side', 500);
            }
        } catch (\Exception $exception) {
            return response('error on the server side', 500);
        }
    }
}
