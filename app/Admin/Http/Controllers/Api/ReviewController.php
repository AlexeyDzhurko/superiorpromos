<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Models\Review;
use App\Transformers\ReviewTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Serializer\ArraySerializer;

class ReviewController extends BaseAdminController
{
    /**
     * @return \Spatie\Fractal\Fractal
     */
    public function index()
    {
        $reviews = Review::orderBy('approve')->paginate(self::DEFAULT_DASHBOARD_PER_PAGE);

        return fractal()
            ->serializeWith(new ArraySerializer())
            ->collection($reviews, new ReviewTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($reviews));
    }

    /**
     * @param Request $request
     * @return \Spatie\Fractal\Fractal
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'approve' => 'boolean',
            'text' => 'required',
            'reviewer_name' => 'required',
            'reviewer_email' => 'nullable|email',
            'company_name' => 'nullable|string',
            'review_date' => 'nullable|date',
            'rating' => 'required|numeric|min:0|max:5',
            'product_id' => 'required|exists:products,id'
        ]);

        $review = new Review();

        $review->fill($request->only('approve', 'text', 'reviewer_name', 'reviewer_email', 'company_name', 'rating', 'product_id', 'review_date'));
        $review->save();

        return fractal()->item($review, new ReviewTransformer());
    }

    /**
     * @param Request $request
     * @param Review $review
     * @return \Spatie\Fractal\Fractal
     */
    public function update(Request $request, Review $review)
    {
        $this->validate($request, [
            'approve' => 'nullable|boolean',
            'text' => 'required',
            'reviewer_name' => 'required',
            'reviewer_email' => 'nullable|email',
            'company_name' => 'nullable|string',
            'review_date' => 'nullable|date',
            'rating' => 'required|numeric|min:0|max:5',
        ]);

        $review->fill($request->only('approve', 'text', 'reviewer_name', 'reviewer_email', 'review_date', 'company_name', 'rating'));
        $review->save();

        return fractal()->item($review, new ReviewTransformer());
    }

    /**
     * @param Review $review
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function destroy(Review $review)
    {
        $review->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }
}
