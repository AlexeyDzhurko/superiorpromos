<?php

namespace App\Admin\Http\Controllers\Api;

use App\Admin\Http\Controllers\BaseAdminController;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends BaseAdminController
{
    public function index()
    {
        return Setting::all()->groupBy('group');
    }

    public function update(Request $request, Setting $setting)
    {
        $this->validate($request, [
            'value' => 'nullable'
        ]);

        $setting->value = $request->get('value');
        $setting->save();

        return $setting;
    }

}
