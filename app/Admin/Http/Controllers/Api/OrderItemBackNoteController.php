<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 19.04.17
 * Time: 14:07
 */

namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Transformers\BackNoteTransformer;
use App\Models\BackNote;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Http\Response;

class OrderItemBackNoteController extends BaseAdminController
{
    public function index(OrderItem $orderItem)
    {
        return fractal()->collection($orderItem->backNotes, new BackNoteTransformer());
    }

    public function store(Request $request, OrderItem $orderItem)
    {
        $this->validate($request, [
            'note' => 'required'
        ]);

        $backNote =  new BackNote();
        $backNote->note = $request->get('note');
        $backNote->orderItem()->associate($orderItem);
        $backNote->user()->associate(Auth::user());

        try {
            $backNote->save();
        } catch (\Exception $e) {
            return response('Something Wrong', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response('', Response::HTTP_NO_CONTENT);
    }

}
