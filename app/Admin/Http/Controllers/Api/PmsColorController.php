<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Models\PmsColor;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PmsColorController extends BaseAdminController
{
    public function index()
    {
        return PmsColor::all();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'color_hex' => 'required|string',
        ]);

        $pmsColor = new PmsColor();
        $pmsColor->name = $request->name;
        $pmsColor->color_hex = $request->color_hex;
        $pmsColor->save();

        return $pmsColor;
    }

    public function update(Request $request, PmsColor $pmsColor)
    {
        $this->validate($request, [
            'name' => 'string',
            'color_hex' => 'string',
        ]);

        if ($request->has('name')) {
            $pmsColor->name = $request->name;
        }

        if ($request->has('color_hex')) {
            $pmsColor->color_hex = $request->color_hex;
        }

        $pmsColor->save();
        return response('', Response::HTTP_NO_CONTENT);
    }

    public function destroy(PmsColor $pmsColor)
    {
        $pmsColor->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

}
