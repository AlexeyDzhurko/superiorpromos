<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Http\Requests\PaymentInformationTemplateRequest;
use App\Models\PaymentInformationTemplate;
use Illuminate\Http\Response;

class PaymentInformationTemplateController extends BaseAdminController
{
    public function index()
    {
        return PaymentInformationTemplate::all();
    }

    public function store(PaymentInformationTemplateRequest $request)
    {
        $data = $request->only(['name', 'single_body', 'multiple_body']);
        $paymentInformationTemplate = new PaymentInformationTemplate($data);
        $paymentInformationTemplate->save();

        return $paymentInformationTemplate;
    }

    public function update(PaymentInformationTemplateRequest $request, PaymentInformationTemplate $paymentInformationTemplate)
    {
        $data = $request->only(['name', 'single_body', 'multiple_body']);
        $paymentInformationTemplate->fill($data);
        $paymentInformationTemplate->save();

        return $paymentInformationTemplate;
    }

    public function destroy(PaymentInformationTemplate $paymentInformationTemplate)
    {
        $paymentInformationTemplate->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }
}
