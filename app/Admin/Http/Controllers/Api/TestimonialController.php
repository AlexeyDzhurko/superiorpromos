<?php

namespace App\Admin\Http\Controllers\Api;

use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Http\Requests\CreateTestimonialRequest;
use App\Admin\Http\Requests\UpdateTestimonialRequest;
use App\Models\Testimonial;
use App\Repository\TestimonialRepository;

class TestimonialController extends BaseAdminController
{
    public function index()
    {
        return TestimonialRepository::getTestimonialsByParameters();
    }

    public function store(CreateTestimonialRequest $request)
    {
        $imagePath ='';
        if ($request->hasFile('image')) {
            $imagePath = $request->file('image')->store(Testimonial::TESTIMONIAL_DIR , 'uploads');
        }

        return TestimonialRepository::storeTestimonial($request->all(), $imagePath);
    }

    public function update(UpdateTestimonialRequest $request, $id)
    {
        if ($request->hasFile('image')) {
            $imagePath = $request->file('image')->store(Testimonial::TESTIMONIAL_DIR , 'uploads');
            return TestimonialRepository::updateTestimonial($request->except(['image']), $id, $imagePath);
        } else {
            return TestimonialRepository::updateTestimonial($request->except(['image']), $id);
        }
    }

    public function destroy(Testimonial $testimonial)
    {
        try {
            if($testimonial->delete()) {
                return response('success', 204);
            } else {
                return response('error on the server side', 500);
            }
        } catch (\Exception $exception) {
            return response('error on the server side', 500);
        }
    }
}
