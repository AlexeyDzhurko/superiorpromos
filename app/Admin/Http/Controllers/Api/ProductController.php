<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Http\Requests\UpdateProductRequest;
use App\Admin\Services\ProductManager;
use App\Admin\Services\ProductOptionManager;
use App\Admin\Transformers\AdminProductTransformer;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductColorGroup;
use App\Models\ProductExtraImage;
use App\Models\ProductOption;
use App\Models\ProductOptionPrice;
use App\Models\ProductPrice;
use App\Models\ProductSubOption;
use App\Models\ProductSubOptionPrice;
use App\Transformers\ProductOptionTransformer;
use App\Transformers\ProductPriceTransformer;
use App\Transformers\ProductSubOptionTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Image;

class ProductController extends BaseAdminController
{
    public function __construct()
    {
        $this->middleware('elastic.pagination.page.limit')->only('elasticIndex');
    }

    public function elasticIndex(Request $request)
    {
        if((int)$request->page > 250) {
            return response(['message' => 'Please refine your search for continue pagination'], 422);
        }

        $search = $this->elasticSearch($request, Product::class);
        foreach (['name', 'active', 'vendor_id', 'sku', 'sage', 'color_images_status', 'category_id'] as $field){
            if ($request->has($field)) {
                switch ($field) {
                    case 'name':
                        $search->match('name', $request->get('name'));
                        break;
                    case 'active':
                        $search->term('active', $request->get('active'));
                        break;
                    case 'vendor_id':
                        $search->nestedTerm('vendors', ['vendor_id' => $request->get('vendor_id')]);
                        break;
                    case 'sku':
                        $search->nestedMatch('vendors', ['sku' => $request->get('sku')]);
                        break;
                    case 'sage':
                        if ($request->get('sage') == 1) {
                            $search->exists('sage_id');
                        } else {
                            $search->notExists('sage_id');
                        }
                        break;
                    case 'color_images_status':
                        $search->term('color_images_status', $request->get('color_images_status'));
                        break;
                    case 'category_id':
                        /** @var Category $category */
                        $category = Category::find($request->get('category_id'));
                        if(is_null($category)){
                            break;
                        }
                        $categoryIds = $category->getDescendantsAndSelf()->pluck('id')->toArray();
                        $search->nestedTerms('categories',  ['id' => $categoryIds]);
                        break;
                }
            }
        }

        return $search->paginate($request->get('page', 1), self::DEFAULT_DASHBOARD_PER_PAGE);
    }

    public function index(Request $request)
    {
        $products = Product::take(self::DEFAULT_API_PER_PAGE);

        if ($request->has('query')) {
            $query = $request->get('query');
            $products->where('name', 'like', '%' . $query . '%')
                ->orWhere('id', 'like', $query . '%');
        }

        return fractal()->collection($products->get(), function(Product $product) {
            return  [
                'id' => $product->id,
                'name' => $product->name,
            ];
        });
    }

    public function show(Product $product)
    {
        return fractal()->item($product, new AdminProductTransformer());
    }

    public function update(UpdateProductRequest $request, Product $product, ProductManager $productManager)
    {
        $nullableColumns = [
            'box_weight',
            'description',
            'production_time_from',
            'production_time_to',
            'on_sale',

            'shipping_additional_value',
            'popularity'
        ];
        $data = $request->only([
            'name',
            'url',
            'meta_title',
            'keywords',
            'image_alt',
            'google_product_category',
            'brand',
            'gender',
            'age_group',
            'material',
            'pattern',
            'meta_description',
            'quantity_per_box',
            'box_weight',
            'shipping_additional_type',
            'shipping_additional_value',
            'production_time_from',
            'production_time_to',
            'sage_id',
            'zip_from',
            'dimensions',
            'popularity',
            'description',
            'imprint_area',
            'pricing_information',
            'active',
            'on_sale',
            'product_icon_id',
            'size_group_id',
            'custom_shipping_method',
            'custom_shipping_cost',
            'product_thumbnail'
        ]);

        if (is_null($data['active'])) {
            $data['active'] = 0;
        }

        $productExtraImageIds = array_pluck($request->get('product_extra_images', []), 'id');
        $product->productExtraImages->each(function(ProductExtraImage $productExtraImage) use ($productExtraImageIds) {
            if (!in_array($productExtraImage->id, $productExtraImageIds)) {
                $productExtraImage->delete();
            }
        });

        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $file) {
                $productManager->createProductExtraImage($product, $file);
            }
        }

        $files = Storage::disk('uploads')
            ->directories('products/' . $product->id . '/' . ProductManager::EXTRA_IMAGES_PATH);
        $mainImage = collect($files)->first();
        if ($mainImage) {
            $link = '/uploads/' . $mainImage . '/270_270.jpg';
            $product->image_src = $link;
            $product->save();

            $extraImageId = collect(explode('/', $mainImage))->last();
            $extraImage = ProductExtraImage::find($extraImageId);
            $extraImage->image_src = null;
            $extraImage->save();
        }

        foreach ($nullableColumns as $column) {
            if (!$request->has($column)) {
                $data[$column] = 0;
            }
        }

        $product->fill($data)->save();

        if (!$request->has('url')) {
            $product->url = str_slug($product->name, '-');
        }

        if ($request->hasFile('additional_specifications')) {
            $product->additional_specifications = $request->file('additional_specifications')->store('additional_spec', 'public');
        }

        if ($request->hasFile('video')) {
            $product->video = $request->file('video')->store('video', 'public');
        }

        if ($request->has('video_url')) {
            $productManager->updateVideoUrl($product, $request->video_url);
        }

        if ($request->has('product_thumbnail')) {
            if($productManager->checkExistExtraImage($request->product_thumbnail)) {
                $product->image_src = ProductManager::EXTRA_IMAGES_PATH . $request->product_thumbnail . '/270_270.jpg';
            }
        }

        $requestedVendorsSku = $request->get('vendor_sku');
        $requestedVendorIds = $request->get('vendors', []);
        $vendorsToSync = [];
        $vendorPosition = 1;
        foreach ($requestedVendorIds as $key => $requestedVendorId) {
            $vendorsToSync[$requestedVendorId]['sku'] = $requestedVendorsSku[$requestedVendorId] ?? null;
            $vendorsToSync[$requestedVendorId]['position'] = $vendorPosition++;
        }

        $categoriesToSync = [];
        foreach ($request->get('categories', []) as $key => $categoryId) {
            $categoriesToSync[$categoryId] = ['type' => ($key == 0) ? 'main' : 'sub'];
        }

        $product->categories()->sync($categoriesToSync);
        $product->vendors()->sync($vendorsToSync);

        $product->save();

//        return response('', Response::HTTP_NO_CONTENT);
        return response()->json(['id' => $product->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product  $product
     * @return Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return response('', Response::HTTP_NO_CONTENT);
    }

    public function updateProductPrice(Request $request, Product $product, ProductManager $productManager)
    {
        $this->validate($request, [
            'product_prices.*.id' => 'numeric|nullable',
            'product_prices.*.quantity' => 'required|numeric',
            'product_prices.*.setup_price' => 'required|numeric',
            'product_prices.*.item_price' => 'required|numeric',
            'product_prices.*.sale_item_price' => 'required|numeric',
        ]);

        $productManager->updateProductPriceGrid($product, $request->get('product_prices'));

        return fractal()->collection($product->productPrices, new ProductPriceTransformer());
    }

    public function destroyProductPrice(Product $product, ProductPrice $productPrice)
    {
        $productPrice->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    public function updateProductOption(
        Request $request,
        Product $product,
        ProductOption $productOption,
        ProductOptionManager $productOptionManager
    ) {
        $this->validate($request, [
            'name' => 'string',
            'required'                            => 'required|boolean',
            'show_as'                             => 'required|in:radio,drop_down,checkbox',
            'product_option_prices.*.id'          => 'numeric|nullable',
            'product_option_prices.*.quantity'    => 'numeric|nullable',
            'product_option_prices.*.setup_price' => 'numeric|nullable',
            'product_option_prices.*.item_price'  => 'numeric|nullable',
        ]);

        if ($request->has('name')) {
            $productOption->name = $request->get('name');
        }

        $productOption->required = $request->get('required');
        $productOption->show_as = $request->get('show_as');

        if ($request->has('product_option_prices')) {
            $productOptionManager->updateProductOptionPriceGrid($productOption, $request->get('product_option_prices'));
        }

        $productOption->save();

        return fractal()->item($productOption, new ProductOptionTransformer());
    }

    public function storeProductOption(Request $request, Product $product, ProductOptionManager $productOptionManager)
    {
        $this->validate($request, [
            'name'                                => 'required',
            'required'                            => 'required|boolean',
            'show_as'                             => 'required|in:radio,drop_down,checkbox',
            'product_option_prices.*.id'          => 'numeric|nullable',
            'product_option_prices.*.quantity'    => 'required|numeric',
            'product_option_prices.*.setup_price' => 'numeric|nullable',
            'product_option_prices.*.item_price'  => 'numeric|nullable',
        ]);

        $productOption = new ProductOption();
        $productOption->product()->associate($product);
        $productOption->name = $request->get('name');
        $productOption->required = $request->get('required');
        $productOption->show_as = $request->get('show_as');
        $productOption->save();


        $productOptionManager->updateProductOptionPriceGrid($productOption, $request->get('product_option_prices'));

        return fractal()->item($productOption, new ProductOptionTransformer());
    }

    /**
     * Remove row from ProductOption price grid
     *
     * @param Product $product
     * @param ProductOption $productOption
     * @param ProductOptionPrice $productOptionPrice
     * @return Response
     */
    public function destroyProductOptionPrice(Product $product, ProductOption $productOption, ProductOptionPrice $productOptionPrice)
    {
        $productOptionPrice->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Remove row from ProductSubOption price grid
     *
     * @param Product $product
     * @param ProductOption $productOption
     * @param ProductSubOption $productSubOption
     * @param ProductSubOptionPrice $productSubOptionPrice
     * @return Response
     * @internal param ProductOptionPrice $productOptionPrice
     */
    public function destroyProductSubOptionPrice(Product $product, ProductOption $productOption, ProductSubOption $productSubOption, ProductSubOptionPrice $productSubOptionPrice)
    {
        $productSubOptionPrice->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    public function destroyProductOption(Product $product, ProductOption $productOption)
    {
        $productOption->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    public function destroyProductSubOption(Product $product, ProductOption $productOption, ProductSubOption $productSubOption)
    {
        $productSubOption->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    public function storeProductSubOption(Request $request, Product $product, ProductOption $productOption, ProductOptionManager $productOptionManager)
    {
        $this->validate($request, [
            'name' => 'required',
            'product_sub_option_prices.*.id' => 'numeric|nullable',
            'product_sub_option_prices.*.quantity' => 'required|numeric',
            'product_sub_option_prices.*.setup_price' => 'numeric|nullable',
            'product_sub_option_prices.*.item_price' => 'numeric|nullable',
        ]);

        $productSubOption = new ProductSubOption();
        $productSubOption->productOption()->associate($productOption);
        $productSubOption->name = $request->get('name');
        $productSubOption->save();


        $productOptionManager->updateProductSubOptionPriceGrid($productSubOption, $request->get('product_sub_option_prices'));

        return fractal()->item($productSubOption, new ProductSubOptionTransformer());
    }

    public function updateProductSubOption(Request $request, Product $product, ProductOption $productOption, ProductSubOption $productSubOption, ProductOptionManager $productOptionManager)
    {
        $this->validate($request, [
            'name' => 'string',
            'product_sub_option_prices.*.id' => 'numeric|nullable',
            'product_sub_option_prices.*.quantity' => 'numeric|nullable',
            'product_sub_option_prices.*.setup_price' => 'numeric|nullable',
            'product_sub_option_prices.*.item_price' => 'numeric|nullable',
        ]);

        if ($request->has('name')) {
            $productSubOption->name = $request->get('name');
        }

        if ($request->has('product_sub_option_prices')) {
            $productOptionManager->updateProductSubOptionPriceGrid($productSubOption, $request->get('product_sub_option_prices'));
        }

        $productSubOption->save();

        return fractal()->item($productSubOption, new ProductSubOptionTransformer());
    }

    public function updateStatus(Product $product)
    {
        $product->active = !$product->active;
        $product->save();

        return response()->json([], Response::HTTP_OK);
    }

}
