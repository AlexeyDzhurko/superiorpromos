<?php

namespace App\Admin\Http\Controllers\Api;

use App\Admin\Http\Controllers\BaseAdminController;
use App\Http\Requests\AddressRequest;
use App\Models\Address;
use Illuminate\Http\Request;

class AddressController extends BaseAdminController
{
    public function index(Request $request)
    {
        //
    }

    public function show(Address $address)
    {
        //
    }

    public function store(AddressRequest $request, Address $address)
    {
        $address->fill($request->all());
        if($address->save()) {
            return response('', 204);
        } else {
            abort(500, 'Error occurred while address storing');
        }
    }

    public function update(AddressRequest $request, Address $address)
    {
        if($address->fill($request->all())->save()) {
            return response('', 204);
        } else {
            abort(500, 'Error occurred while address updating');
        }
    }

    public function destroy(Address $address)
    {
        if($address->delete()) {
            return response('', 204);
        }
    }
}
