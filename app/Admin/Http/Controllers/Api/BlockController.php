<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Models\Block;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BlockController extends BaseAdminController
{
    public function index()
    {
        return Block::all();
    }

    public function update(Request $request, Block $block)
    {
        $this->validate($request, [
            'content' => 'nullable',
            'active' => 'required|boolean',
        ]);

        $block->content = $request->get('content');
        $block->active = $request->get('active');
        $block->save();
dd($block->content);
        return $block;
    }

    public function upload(Request $request)
    {
        $valid_exts = ['jpeg', 'jpg', 'png'];

        $file = $request->file('image');

        $ext = $file->guessClientExtension();

        if (in_array($ext, $valid_exts)) {

            $filepath = $request->file('image')->storeAs('site-files', str_random(20) . '.' . $ext, 'uploads');

            if (!empty($file) && !empty($filepath)) {
                $fileurl = Storage::disk('uploads')->url($filepath);

                return response(['location' => $fileurl]);
            }

            return response(['location' => '']);

        } else {
            return response(['message' => 'Wrong file extension'], 400);
        }
    }
}
