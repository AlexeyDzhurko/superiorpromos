<?php


namespace App\Admin\Http\Controllers\Api;


use App\Models\Promos;
use Illuminate\Http\Request;
use App\Admin\Http\Controllers\BaseAdminController;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Storage;

class PromosController extends BaseAdminController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $promos = Promos::all()->groupBy('position');

        return response()->json($promos);
    }

    /**
     * @param Request $request
     * @param Promos $promos
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Promos $promos)
    {
        $this->validate($request, [
            'url'       => 'required',
            'image_src' => 'required',
        ]);

        if (!Str::is($promos->path, $request->get('image_src'))) {
            $exist = Storage::disk('uploads')->exists($promos->path);
            if ($exist) {
                Storage::disk('uploads')->delete($promos->path);
            }
            $promos->image_src = $request->get('image_src');
        }

        $promos->url = $request->get('url');
        $promos->save();

        return response()->json($promos);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function upload(Request $request)
    {
        $this->validate($request, [
            'image' => 'file|mimes:jpeg,png'
        ]);

        $file = $request->file('image');
        $ext = $file->guessClientExtension();

        $filepath = $request->file('image')->storeAs('site-files/promos', str_random(20) . '.' . $ext, 'uploads');

        if (!empty($file) && !empty($filepath)) {
            $fileUrl = Storage::disk('uploads')->url($filepath);

            return response([
                'image_src' => $fileUrl,
                'path' => $filepath
            ]);
        }

        return response([]);
    }
}
