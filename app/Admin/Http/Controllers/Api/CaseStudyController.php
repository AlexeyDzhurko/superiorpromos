<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Http\Requests\CaseStudyRequest;
use App\Models\CaseStudy;
use App\Search\SearchBuilder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Elasticsearch;

class CaseStudyController extends BaseAdminController
{
    public function index(Request $request)
    {
        $search = $this->elasticSearch($request, CaseStudy::class);

        if ($request->has('title')) {
            $search->match('title', $request->title);
        }

        $search->order('position');

        return $search->paginate($request->get('page', 1));
    }

    public function show(CaseStudy $caseStudy)
    {
        return $caseStudy;
    }

    public function store(CaseStudyRequest $request)
    {
        $caseStudy = new CaseStudy($request->only(['title', 'body', 'active']));
        $caseStudy->save();

        return $caseStudy;
    }

    public function update(CaseStudyRequest $request, CaseStudy $caseStudy)
    {
        $caseStudy->fill($request->only(['title', 'body', 'active']));
        $caseStudy->save();

        return $caseStudy;
    }

    public function destroy(CaseStudy $caseStudy)
    {
        $caseStudy->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    public function sort(Request $request, CaseStudy $caseStudy)
    {
        $this->validate($request, [
            'position_case_study_id' => 'required|exists:case_studies,id',
            'type' => 'required|in:moveBefore,moveAfter'
        ]);

        $positionCaseStudy = CaseStudy::find($request->get('position_case_study_id'));
        $this->elasticMove($caseStudy, $positionCaseStudy);

        if ($request->get('type') == 'moveBefore') {
            $caseStudy->moveBefore($positionCaseStudy);
        } else {
            $caseStudy->moveAfter($positionCaseStudy);
        }

        return response('', Response::HTTP_NO_CONTENT);
    }
}
