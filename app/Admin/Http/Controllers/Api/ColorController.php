<?php

namespace App\Admin\Http\Controllers\Api;

use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Http\Requests\ColorRequest;
use App\Models\Color;
use App\Transformers\ColorsTransformer;
use Illuminate\Http\Request;

class ColorController extends BaseAdminController
{
    public function index(Request $request)
    {
        $this->validate($request, [
            'color_group_id' => 'required|exists:color_groups,id'
        ]);

        $colors = Color::join('color_color_group', 'color_color_group.color_id', '=', 'colors.id')
            ->where('group_id', $request->color_group_id)
            ->get();


        return fractal()->collection($colors, new ColorsTransformer());
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getColorsList()
    {
        return Color::with(['groups', 'pmsColor'])->paginate(self::DEFAULT_DASHBOARD_PER_PAGE);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllColors()
    {
        return Color::get();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function searchColor(Request $request)
    {
        if ($query = $request->get('query')) {
            return Color::where(
                'name', 'like', '%' . $query . '%'
            )->get();
        }

        return Color::get();
    }

    /**
     * @param ColorRequest $request
     * @return Color
     */
    public function store(ColorRequest $request)
    {
        return \DB::transaction(function () use ($request) {
            try {
                $color = Color::create($request->all());

                if ($request->hasFile('image') and $request->image->isValid()) {
                    $fullPath = Color::IMAGES_DIR . str_random(20) . '.' . $request->image->getClientOriginalExtension();
                    \Storage::disk('public')->put($fullPath, file_get_contents($request->image));
                    $color->picture_src = $fullPath;
                    $color->save();
                }

                if ($request->has('groups') and is_array($request->groups)) {
                    $color->groups()->sync($request->groups);
                }

                return $color->load(['groups', 'pmsColor']);
            } catch (\Exception $e) {
                return response('error on the server side' . $e->getMessage(), 500);
            }
        });
    }

    /**
     * @param ColorRequest $request
     * @param Color $color
     * @return Color
     */
    public function update(ColorRequest $request, Color $color)
    {
        return \DB::transaction(function () use ($request, $color) {
            try {
                $color->update($request->all());

                if ($request->hasFile('image') and $request->image->isValid()) {
                    \Storage::disk('public')->delete(Color::IMAGES_DIR . $color->picture_src);

                    $fullPath = str_random(20) . '.' . $request->image->getClientOriginalExtension();
                    \Storage::disk('public')->put(Color::IMAGES_DIR . $fullPath, file_get_contents($request->image));
                    $color->picture_src = $fullPath;
                    $color->save();
                }

                if ($request->has('groups') and is_array($request->groups)) {
                    $color->groups()->sync($request->groups);
                }

                return $color->load(['groups', 'pmsColor']);
            } catch (\Exception $e) {
                return response('error on the server side' . $e->getMessage(), 500);
            }
        });
    }

    /**
     * @param Color $color
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy(Color $color)
    {
        try {
            return \DB::transaction(function () use ($color) {
                if ($color->delete()) {
                    \Storage::disk('public')->delete(Color::IMAGES_DIR . $color->picture_src);
                    return response('success', 204);
                } else {
                    return response('error on the server side', 500);
                }
            });
        } catch (\Exception $exception) {
            return response('error on the server side', 500);
        }
    }
}
