<?php


namespace App\Admin\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\OrderSampleItem;
use App\Models\OrderSampleItemNote;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Admin\Transformers\OrderSampleItemNoteTransformer;

class OrderSampleItemNoteController extends Controller
{
    /**
     * @param OrderSampleItem $orderSampleItem
     * @return \Spatie\Fractal\Fractal
     */
    public function index(OrderSampleItem $orderSampleItem)
    {
        return fractal()->collection($orderSampleItem->note, new OrderSampleItemNoteTransformer());
    }

    /**
     * @param Request $request
     * @param OrderSampleItem $orderSampleItem
     */
    public function store(Request $request, OrderSampleItem $orderSampleItem)
    {
        $this->validate($request, [
            'note' => 'required|string'
        ]);

        $orderSampleItemNote = new OrderSampleItemNote($request->only(['note']));
        $orderSampleItemNote->orderSampleItem()->associate($orderSampleItem);
        $orderSampleItemNote->user()->associate(Auth::user());

        try {
            $orderSampleItemNote->save();
        } catch (\Exception $e) {
            return response()->json('Error occurred while creating sample order item note.', Response::HTTP_BAD_REQUEST);
        }

        return fractal()->item($orderSampleItemNote, new OrderSampleItemNoteTransformer());
    }
}
