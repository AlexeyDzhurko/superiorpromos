<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Models\Order;
use App\Search\SearchBuilder;
use App\Transformers\OrderTransformer;
use Illuminate\Http\Request;

class ReminderController extends BaseAdminController
{
    public function index(Request $request)
    {
        $this->validate($request, [
            'date_from' => '',
            'date_to' => '',
            'user_id' => 'nullable|exists:users,id',
            'user_name' => 'nullable|string',
        ]);

        $search = SearchBuilder::createBuilder(Order::class);

        if ($request->has(['date_from', 'date_to'])) {
            $search->between('created_at', $request->date_from, $request->date_to);
        }

        if ($request->has('user_name')) {
            $search->match(['billing_first_name', 'billing_last_name'], $request->user_name);
        }

        return $search->get();
    }

}
