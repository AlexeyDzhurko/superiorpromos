<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 25.04.17
 * Time: 22:54
 */

namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Models\Stage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StageController extends BaseAdminController
{
    public function index()
    {
        return Stage::sorted()->get();
    }

    public function sort(Request $request, Stage $stage)
    {
        $this->validate($request, [
            'position_stage_id' => 'required|exists:stages,id',
            'type' => 'required|in:moveBefore,moveAfter'
        ]);

        $positionStage = Stage::find($request->get('position_stage_id'));

        if ($request->get('type') == 'moveBefore') {
            $stage->moveBefore($positionStage);
        } else {
            $stage->moveAfter($positionStage);
        }

        return response('', Response::HTTP_NO_CONTENT);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
        ]);

        $stage = new Stage();
        $stage->name = $request->get('name');
        try {
            $stage->save();
        } catch (\Exception $e) {
            return response('Something Wrong', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response('', Response::HTTP_NO_CONTENT);
    }

    public function update(Request $request, Stage $stage)
    {
        $this->validate($request, [
            'name' => 'string',
        ]);

        if ($request->has('name')) {
            $stage->name = $request->get('name');
        }

        try {
            $stage->save();
        } catch (\Exception $e) {
            return response('Something Wrong', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response('', Response::HTTP_NO_CONTENT);
    }

    public function destroy(Stage $stage)
    {
        $stage->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

}
