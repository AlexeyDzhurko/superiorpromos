<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Services\ProductColorGroupManager;
use App\Admin\Services\ProductManager;
use App\Models\ColorGroup;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductColorGroup;
use App\Models\ProductColorPrice;
use App\Transformers\ColorGroupTransformer;
use App\Transformers\ProductColorGroupTransformer;
use App\Transformers\ProductColorTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductColorGroupController extends BaseAdminController
{
    public function store(Request $request, Product $product)
    {
        $this->validate($request, [
            'name' => 'required_without:color_group_id',
            'color_group_id' => 'required_without:name|exists:color_groups,id'
        ]);

        $productColorGroup = new ProductColorGroup();
        $productColorGroup->product()->associate($product);

        // Import color from existed default color group
        if ($request->has('color_group_id')) {
            $colorGroup = ColorGroup::find($request->color_group_id);
            $productColorGroup->name = $colorGroup->name;
            $productColors = [];
            foreach ($colorGroup->colors as $color) {
                $productColors[] = new ProductColor(['color_id' => $color->id]);
            }
            $productColorGroup->save();
            $productColorGroup->productColors()->saveMany($productColors);
        } else {
            //Create empty color group
            $productColorGroup->name = $request->name;
        }

        $productColorGroup->save();

        return fractal()->item($productColorGroup, new ProductColorGroupTransformer());
    }

    public function update(Request $request, Product $product, ProductColorGroup $productColorGroup)
    {
        $this->validate($request, [
            'name' => 'string',
        ]);

        if ($request->has('name')) {
            $productColorGroup->name = $request->name;
        }

        $productColorGroup->save();

        return response('', Response::HTTP_NO_CONTENT);
    }

    public function destroy(Product $product, ProductColorGroup $productColorGroup)
    {
        if ($productColorGroup->delete()) {
            return response('', Response::HTTP_NO_CONTENT);
        }
    }

    public function storeProductColor(Request $request, Product $product, ProductColorGroup $productColorGroup)
    {
        $this->validate($request, [
            'color_id' => 'exists:colors,id'
        ]);
        $productColor = $productColorGroup->productColors()->save(new ProductColor(['color_id' => $request->color_id]));

        return fractal()->item($productColor, new ProductColorTransformer());
    }

    public function updateProductColor(
        Request $request,
        Product $product,
        ProductColorGroup $productColorGroup,
        ProductColor $productColor,
        ProductColorGroupManager $productColorGroupManager,
        ProductManager $productManager
    ) {
        $this->validate($request, [
            'product_color_prices.*.id' => 'numeric|nullable',
            'product_color_prices.*.quantity' => 'required|numeric',
            'product_color_prices.*.setup_price' => 'numeric|nullable',
            'product_color_prices.*.item_price' => 'numeric|nullable',
            'image' => 'image|nullable',
        ]);

        if ($request->hasFile('image')) {
            $productManager->createProductColorImage($productColor, $request->file('image'));
        }


        if ($request->has('product_color_prices')) {
            $productColorGroupManager->updateProductColorPriceGrid($productColor, $request->product_color_prices);
        }

        return fractal()->item($productColor, new ProductColorTransformer());
    }

    public function deleteProductColor(
        Product $product,
        ProductColorGroup $productColorGroup,
        ProductColor $productColor
    ) {
        if ($productColor->delete()) {
            return response('', Response::HTTP_NO_CONTENT);
        }
    }

    public function deleteProductColorPrice(
        Product $product,
        ProductColorGroup $productColorGroup,
        ProductColor $productColor,
        ProductColorPrice $productColorPrice
    ) {
        if ($productColorPrice->delete()) {
            return response('', Response::HTTP_NO_CONTENT);
        }
    }

}
