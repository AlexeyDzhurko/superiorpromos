<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 13.04.17
 * Time: 14:29
 */

namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Transformers\OrderTransformer;
use App\Models\Order;
use App\Models\Stage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class OrderController extends BaseAdminController
{
    public function __construct()
    {
        $this->middleware('elastic.pagination.page.limit')->only('search');
    }

    public function search(Request $request)
    {
        $search = $this->elasticSearch($request, Order::class);

        foreach ([
                     'billing_first_name',
                     'product_id',
                     'vendor_id',
                     'user_id',
                     'state',
                     'po_number',
                     'stage_id',
                     'not_paid',
                     'sub_order_id',
                     'date_from',
                     'date_to',
                     'check_notes',
                     'is_sample'
                 ] as $field
        ) {
            if ($request->has($field)) {
                switch ($field) {
                    case 'billing_first_name':
                        $searchArr = array_map(function ($item) {
                                return '*' . $item;
                            }, explode(' ', $request->get('billing_first_name'))
                        );
                        $search->matchWildcard('billing_full_name', $searchArr);
                        break;
                    case 'product_id':
                        $search->nestedTerm('order_items', ['product_id' => $request->get('product_id')]);
                        break;
                    case 'user_id':
                        $search->term('user_id', $request->get('user_id'));
                        break;
                    case 'state':
                        $search->match('state', $request->get('state'));
                        break;
                    case 'vendor_id':
                        $search->nestedTerm('order_items', ['vendor_id' => $request->get('vendor_id')]);
                        break;
                    case 'po_number':
                        $search->nestedMatchPhrasePrefix('order_items', ['po_number' => $request->get('po_number')]);
                        break;
                    case 'stage_id':
                        if ($request->get('stage_id') != 0) {
                            $search->nestedTerm('order_items', ['stage_id' => $request->get('stage_id')]);
                        }
                        break;
                    case 'not_paid':
                        $search->nestedTerm('order_items', ['not_paid' => $request->get('not_paid')]);
                        break;
                    case 'check_notes':
                        $search->nestedTerm('order_items', ['check_notes' => $request->get('check_notes')]);
                        break;
//                    case 'is_sample':
//                        $search->nestedTerm('order_items', ['is_sample' => $request->get('is_sample')]);
//                        break;
                    case 'sub_order_id':
                        $search->nestedTerm('order_items', ['id' => $request->get('sub_order_id')]);
                        break;
                    case 'date_from':
                        $search->between('created_at', $request->date_from, $request->date_to ?? null);
                        break;
                    case 'date_to':
                        $search->between('created_at', $request->date_from ?? null, $request->date_to);
                        break;
                }
            }
        }

        $searchResult = $search->paginate($request->page);

        $allStages = $search->getWithAggregations(Order::getElasticAggregationsParams());
        $formattedStages = $this->formingStages($allStages['aggregations']['by_stage_id']['by_stage_id']['buckets']);

        return [
            'data' => fractal()->collection($searchResult['data'], new OrderTransformer()),
            'stages' => $formattedStages,
            'filteredStages' => $this->getNotEmptyStages($formattedStages),
            'pagination' => [
                'total' => $searchResult['meta']['pagination']['total_pages'],
                'last_page' => (int)($searchResult['meta']['pagination']['total'] / BaseAdminController::DEFAULT_DASHBOARD_PER_PAGE),
                'current_page' => (int)$request->page,
                'per_page' => BaseAdminController::DEFAULT_DASHBOARD_PER_PAGE,
            ]
        ];
    }

    public function delete(Order $order)
    {
        if ($order->delete()) {
            return response(null, 204);
        }

        return response('Something went wrong, please try again', 406);
    }

    private function formingStages($ordersCountByStages)
    {
        $stages = [];
        foreach (Stage::sorted()->get() as $stage) {
            $orders_count = 0;
            foreach ($ordersCountByStages as $countByStage) {
                if ($countByStage['key'] == $stage->id) {
                    $orders_count = $countByStage['doc_count'];
                }
            }
            $stages[] = [
                'name' => $stage->name,
                'stage_id' => $stage->id,
                'orders_count' => $orders_count,
            ];
        }

        return $stages;
    }

    /**
     * @param array $stages
     * @return array
     */
    private function getNotEmptyStages(array $stages): array
    {
        return array_where($stages, function ($value, $key) {
            return $value['orders_count'] > 0;
        });
    }

}
