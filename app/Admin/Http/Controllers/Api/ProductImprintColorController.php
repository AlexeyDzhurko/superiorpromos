<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Models\Imprint;
use App\Models\ImprintColor;
use App\Models\Product;
use App\Transformers\ImprintColorTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductImprintColorController extends BaseAdminController
{
    /**
     * @param Request $request
     * @param Product $product
     * @param Imprint $imprint
     * @return \Spatie\Fractal\Fractal
     */
    public function store(Request $request, Product $product, Imprint $imprint)
    {
        $this->validate($request, [
            'color_id' => 'required|exists:colors,id',
        ]);

       $imprintColor = $imprint->imprintColors()->save(new ImprintColor([
            'color_id' => $request->input('color_id')
        ]));

        return fractal()->item($imprintColor, new ImprintColorTransformer());
    }

    /**
     * @param Product $product
     * @param Imprint $imprint
     * @param ImprintColor $imprintColor
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function destroy(Product $product, Imprint $imprint, ImprintColor $imprintColor)
    {
        $imprintColor->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }
}
