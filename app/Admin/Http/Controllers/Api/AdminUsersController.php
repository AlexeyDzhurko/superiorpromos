<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Http\Requests\CreateAdminUserRequest;
use App\Admin\Http\Requests\UpdateAdminUserRequest;
use App\Http\Controllers\Controller;
use App\Repository\UserRepository;
use App\User;
use Illuminate\Http\Response;

class AdminUsersController extends BaseAdminController
{
    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index()
    {
        return UserRepository::getAdminUsersByParameters();
    }

    /**
     * @param CreateAdminUserRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Symfony\Component\HttpFoundation\Response
     */
    public function store(CreateAdminUserRequest $request)
    {
        $params = $this->formedParams($request->all());
        $userAdmin = UserRepository::storeAdminUser($params);

        return response($userAdmin, Response::HTTP_CREATED);
    }

    /**
     * @param $id
     * @param UpdateAdminUserRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Symfony\Component\HttpFoundation\Response
     */
    public function update($id, UpdateAdminUserRequest $request)
    {
        $params = $this->formedParams($request->all());
        $userAdmin = UserRepository::updateAdminUser($params, $id);

        return response($userAdmin, Response::HTTP_OK);
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        try {
            if($user->id === auth()->user()->id) {
                return response("You can't delete own account", Response::HTTP_BAD_REQUEST);
            }

            $user->delete();

            return response('', Response::HTTP_NO_CONTENT);
        } catch (\Exception $exception) {

            return response('error on the server side', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
