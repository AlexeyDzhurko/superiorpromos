<?php


namespace App\Admin\Http\Controllers\Api;

use App\Admin\Http\Controllers\BaseAdminController;
use App\Jobs\CreateCategorySiteMapJob;
use Illuminate\Http\Response;
use Storage;

class SiteMapController extends BaseAdminController
{
    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $exist = Storage::disk('public')->exists('sitemap/sitemap.xml');

        if($exist) {
            $fileUrl = Storage::disk('public')->url('sitemap/sitemap.xml');

            return response([
                'url' => $fileUrl
            ], Response::HTTP_OK);
        }

        return response([
            'url' => ''
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store()
    {
        try {
            $this->dispatch(new CreateCategorySiteMapJob());

            $fileUrl = Storage::disk('public')->url('sitemap/sitemap.xml');

            return response([
                'message' => 'Sitemap created successfully.',
                'url' => $fileUrl
            ], Response::HTTP_OK);
        }catch (\Exception $exception) {
            return response([
                'message' => 'Error while generating sitemap. Please, try again later.',
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
