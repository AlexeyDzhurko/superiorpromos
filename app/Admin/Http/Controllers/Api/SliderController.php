<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Models\Slider;
use App\Transformers\SliderTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Storage;

class SliderController extends BaseAdminController
{
    /**
     * @param Request $request
     * @return \Spatie\Fractal\Fractal
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image',
            'url' => 'required|url',
        ]);

        $slider = new Slider();
        $slider->url = $request->url;
        $slider->image_src = $request->file('image')->storeAs('slider', md5(time() . $request->file('image')->getClientOriginalName()) . '.' . $request->file('image')->getClientOriginalExtension(), 'public');
        $slider->save();

        return fractal()->item($slider, new SliderTransformer());
    }

    /**
     * @param Request $request
     * @param Slider $slider
     * @return \Spatie\Fractal\Fractal
     */
    public function update(Request $request, Slider $slider)
    {
        $this->validate($request, [
            'image' => 'image',
            'url' => 'url',
        ]);

        if ($request->has('url')) {
            $slider->url = $request->url;
        }

        if ($request->hasFile('image')) {
            if(Storage::disk('public')->exists($slider->image_src)) {
                Storage::disk('public')->delete($slider->image_src);
            }
            $slider->image_src = $request->file('image')->storeAs('slider', md5(time() . $request->file('image')->getClientOriginalName()) . '.' . $request->file('image')->getClientOriginalExtension(),'public');
        }

        $slider->save();

        return fractal()->item($slider, new SliderTransformer());
    }

    /**
     * @return \Spatie\Fractal\Fractal
     */
    public function index()
    {
        return fractal()->collection(Slider::sorted()->get(), new SliderTransformer());
    }

    /**
     * @param Request $request
     * @param Slider $slider
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Symfony\Component\HttpFoundation\Response
     * @throws \Rutorika\Sortable\SortableException
     */
    public function sort(Request $request, Slider $slider)
    {
        $this->validate($request, [
            'position_slider_id' => 'required|exists:sliders,id',
            'type' => 'required|in:moveBefore,moveAfter'
        ]);

        $positionStage = Slider::find($request->get('position_slider_id'));

        if ($request->get('type') == 'moveBefore') {
            $slider->moveBefore($positionStage);
        } else {
            $slider->moveAfter($positionStage);
        }

        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Slider $slider
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function destroy(Slider $slider)
    {
        if(Storage::disk('public')->exists($slider->image_src)) {
            Storage::disk('public')->delete($slider->image_src);
        }

        $slider->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }
}
