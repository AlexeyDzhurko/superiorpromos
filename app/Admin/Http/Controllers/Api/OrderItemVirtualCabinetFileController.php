<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 20.04.17
 * Time: 11:52
 */

namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Models\OrderItem;
use App\Models\VirtualCabinetFile;
use App\Transformers\VirtualCabinetFileTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Storage;

class OrderItemVirtualCabinetFileController extends BaseAdminController
{
    public function index(OrderItem $orderItem)
    {
        return fractal()->collection($orderItem->virtualCabinetFiles, new VirtualCabinetFileTransformer());
    }

    public function store(Request $request, OrderItem $orderItem)
    {
        $this->validate($request, [
            'file' => 'required|file',
        ]);

        $file = $request->file('file');

        $virtualCabinetFile = new VirtualCabinetFile();
        $virtualCabinetFile->file_name = $file->getClientOriginalName();
        $virtualCabinetFile->file_path = $file->store('virtual_cabinet', 'local');
        $virtualCabinetFile->orderItem()->associate($orderItem);

        try {
            $virtualCabinetFile->save();
        } catch (\Exception $e) {
            return response('Something Wrong', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response('', Response::HTTP_NO_CONTENT);
    }

    public function file(OrderItem $orderItem, VirtualCabinetFile $virtualCabinetFile)
    {
        $prefix = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();

        return response()->file($prefix . $virtualCabinetFile->file_path);
    }
}
