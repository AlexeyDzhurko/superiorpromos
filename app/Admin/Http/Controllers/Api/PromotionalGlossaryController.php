<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Http\Requests\PromotionalGlossaryRequest;
use App\Models\PromotionalGlossary;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PromotionalGlossaryController extends BaseAdminController
{
    public function index()
    {
        return PromotionalGlossary::sorted()->get();
    }

    public function store(PromotionalGlossaryRequest $request)
    {
        $promotionalGlossary = new PromotionalGlossary($request->only(['title', 'body', 'active']));
        $promotionalGlossary->save();

        return $promotionalGlossary;
    }

    public function update(PromotionalGlossaryRequest $request, PromotionalGlossary $promotionalGlossary)
    {
        $promotionalGlossary->fill($request->only(['title', 'body', 'active']));
        $promotionalGlossary->save();

        return $promotionalGlossary;
    }

    public function destroy(PromotionalGlossary $promotionalGlossary)
    {
        $promotionalGlossary->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    public function sort(Request $request, PromotionalGlossary $promotionalGlossary)
    {
        $this->validate($request, [
            'position_promotional_glossary_id' => 'required|exists:promotional_glossaries,id',
            'type' => 'required|in:moveBefore,moveAfter'
        ]);

        $positionPromotionalGlossary = PromotionalGlossary::find($request->get('position_promotional_glossary_id'));

        if ($request->get('type') == 'moveBefore') {
            $promotionalGlossary->moveBefore($positionPromotionalGlossary);
        } else {
            $promotionalGlossary->moveAfter($positionPromotionalGlossary);
        }

        return response('', Response::HTTP_NO_CONTENT);
    }
}
