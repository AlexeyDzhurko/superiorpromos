<?php

namespace App\Admin\Http\Controllers\Api;

use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Transformers\PostCategoryTransformer;
use App\Models\PostCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class PostCategoryController extends BaseAdminController
{
    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        try {
            $postsCategories = PostCategory::paginate(BaseAdminController::DEFAULT_DASHBOARD_PER_PAGE);
            $postsCategories->setCollection(new Collection(fractal($postsCategories->getCollection(), new PostCategoryTransformer)));

            return $postsCategories;
        } catch (\Exception $e) {
            return response('error on the server side', 500);
        }
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
        ]);

        try {
            $postCategory = PostCategory::create($request->all());

            return fractal()->item($postCategory, new PostCategoryTransformer);
        } catch (\Exception $e) {
            return response('error on the server side', 500);
        }
    }


    /**
     * @param Request $request
     * @param PostCategory $postCategory
     * @return $this|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, PostCategory $postCategory)
    {
        $this->validate($request, [
            'title' => 'string|max:255',
        ]);

        try {
            $postCategory->fill($request->all());
            $postCategory->save();

            return fractal()->item($postCategory, new PostCategoryTransformer);
        } catch (\Exception $e) {
            return response('error on the server side', 500);
        }
    }

    /**
     * @param PostCategory $postCategory
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy(PostCategory $postCategory)
    {
        try {
            $postCategory->delete();

            return response('', 204);
        } catch (\Exception $e) {
            return response('error on the server side', 500);
        }
    }

    /**
     * @return $this|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function all()
    {
        try {
            $postsCategories = PostCategory::get();

            return fractal()->collection($postsCategories, new PostCategoryTransformer);
        } catch (\Exception $e) {
            return response('error on the server side', 500);
        }
    }
}
