<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Services\SurveyManager;
use App\Models\Survey;
use App\Models\SurveyAnswer;
use App\Models\SurveyQuestion;
use App\Transformers\SurveyTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SurveyController extends BaseAdminController
{
    public function index()
    {
        return fractal()->collection(Survey::all(), new SurveyTransformer());
    }

    public function store(Request $request, SurveyManager $surveyManager)
    {
        $this->validate($request, [
            'name' => 'required',
            'survey_questions.*.id' => 'numeric|nullable',
            'survey_questions.*.name' => 'required|string',
            'survey_questions.*.type' => 'required|in:single,multiple,custom',
            'survey_questions.*.survey_answers.*.name' => 'required',
        ]);

        $survey = new Survey();
        $survey->name = $request->name;
        $survey->save();

        $surveyManager->updateQuestions($survey, $request->get('survey_questions'));

        return fractal()->item($survey, new SurveyTransformer());
    }

    public function update(Request $request, Survey $survey, SurveyManager $surveyManager)
    {
        $this->validate($request, [
            'name' => 'required',
            'survey_questions.*.id' => 'numeric|nullable',
            'survey_questions.*.name' => 'required|string',
            'survey_questions.*.type' => 'required|in:single,multiple,custom',
            'survey_questions.*.survey_answers.*.name' => 'required',
        ]);

        $survey->name = $request->name;
        $survey->save();

        $surveyManager->updateQuestions($survey, $request->get('survey_questions'));

        return fractal()->item($survey, new SurveyTransformer());
    }

    public function destroy(Survey $survey)
    {
        $survey->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    public function destroySurveyQuestions(Survey $survey, SurveyQuestion $surveyQuestion)
    {
        $surveyQuestion->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    public function destroySurveyAnswer(Survey $survey, SurveyQuestion $surveyQuestion, SurveyAnswer $surveyAnswer)
    {
        $surveyAnswer->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }
}
