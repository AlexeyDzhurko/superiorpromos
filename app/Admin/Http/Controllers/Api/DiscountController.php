<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Http\Requests\DiscountRequest;
use App\Discounts\DiscountCalc;
use App\Discounts\DiscountCondition;
use App\Discounts\DiscountCreator;
use App\Models\Discount;
use Illuminate\Filesystem\ClassFinder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DiscountController extends BaseAdminController
{
    public function index()
    {
        return Discount::sorted()->get();
    }

    public function sort(Request $request, Discount $discount)
    {
        $this->validate($request, [
            'position_discount_id' => 'required|exists:discounts,id',
            'type' => 'required|in:moveBefore,moveAfter'
        ]);

        $positionDiscount = Discount::find($request->get('position_discount_id'));

        if ($request->get('type') == 'moveBefore') {
            $discount->moveBefore($positionDiscount);
        } else {
            $discount->moveAfter($positionDiscount);
        }

        return response('', Response::HTTP_NO_CONTENT);
    }

    public function destroy(Discount $discount)
    {
        $discount->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    public function indexDiscountConditions(DiscountCreator $discountCreator)
    {
        $types = [];

        $finder = new ClassFinder();
        $classes = $finder->findClasses(app_path('Discounts'));
        foreach ($classes as $class) {
            $rClass = new \ReflectionClass($class);
            if ($rClass->isSubclassOf(DiscountCondition::class)) {
                $types[] = $discountCreator->getDiscountConditionInfo($rClass);
            }
        }
        return $types;
    }

    public function indexDiscountCalc(DiscountCreator $discountCreator)
    {
        $types = [];

        $finder = new ClassFinder();
        $classes = $finder->findClasses(app_path('Discounts'));
        foreach ($classes as $class) {
            $rClass = new \ReflectionClass($class);
            if ($rClass->isSubclassOf(DiscountCalc::class)) {
                $types[] = $discountCreator->getDiscountCalcInfo($rClass);
            }
        }
        return $types;
    }

    public function store(DiscountRequest $request)
    {
        $discount = new Discount($request->only(['name', 'discount_condition', 'discount_calc', 'code', 'active']));
        try {
            $discount->save();
        } catch (\Exception $e) {
            return response('Something Wrong', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $discount;
    }

    public function update(DiscountRequest $request, Discount $discount)
    {
        $discount->fill($request->only(['name', 'discount_condition', 'discount_calc', 'code', 'active']));
        try {
            $discount->save();
        } catch (\Exception $e) {
            return response('Something Wrong', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $discount;
    }
}
