<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Models\InfoMessage;
use Illuminate\Http\Request;

class InfoMessageController extends BaseAdminController
{
    public function index()
    {
        return InfoMessage::all()->groupBy('group');
    }

    public function update(Request $request, InfoMessage $infoMessage)
    {
        $this->validate($request, [
            'value' => 'nullable'
        ]);

        $infoMessage->value = $request->get('value');
        $infoMessage->save();

        return $infoMessage;
    }

}
