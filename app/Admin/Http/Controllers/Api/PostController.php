<?php

namespace App\Admin\Http\Controllers\Api;

use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Http\Requests\CreatePostRequest;
use App\Admin\Transformers\PostTransformer;
use App\Models\Post;
use Illuminate\Support\Collection;

class PostController extends BaseAdminController
{
    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @throws \Exception
     */
    public function index()
    {
        try {
            $posts = Post::paginate(BaseAdminController::DEFAULT_DASHBOARD_PER_PAGE);
            $posts->setCollection(new Collection(fractal($posts->getCollection(), new PostTransformer)));

            return $posts;
        } catch (\Exception $e) {
            return response('error on the server side', 500);
        }
    }

    /**
     * @param CreatePostRequest $request
     * @return \Spatie\Fractal\Fractal
     * @throws \Exception
     */
    public function store(CreatePostRequest $request)
    {
        try {
            $post = Post::create($request->all());

            return fractal()->item($post, new PostTransformer);
        } catch (\Exception $e) {
            return response('error on the server side', 500);
        }
    }

    /**
     * @param CreatePostRequest $request
     * @param Post $post
     * @return \Spatie\Fractal\Fractal
     * @throws \Exception
     */
    public function update(CreatePostRequest $request, Post $post)
    {
        try {
            $post->fill($request->all());
            $post->save();

            return fractal()->item($post, new PostTransformer);
        } catch (\Exception $e) {
            return response('error on the server side', 500);
        }
    }

    /**
     * @param Post $post
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function destroy(Post $post)
    {
        try {
            $post->delete();

            return response('', 204);
        } catch (\Exception $e) {
            return response('error on the server side', 500);
        }
    }
}
