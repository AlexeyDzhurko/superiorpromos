<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Services\SizeGroupManager;
use App\Models\Size;
use App\Models\SizeGroup;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SizeGroupController extends BaseAdminController
{
    public function index()
    {
        return SizeGroup::with('sizes')->get();
    }

    public function store(Request $request, SizeGroupManager $sizeGroupManager)
    {
        $this->validate($request, [
            'name' => 'required',
            'sizes.*.name' => 'required',
        ]);

        $sizeGroup = new SizeGroup();
        $sizeGroup->name = $request->name;
        $sizeGroup->save();

        $sizeGroupManager->updateSizes($sizeGroup, $request->get('sizes'));

        return $sizeGroup;
    }

    public function update(Request $request, SizeGroup $sizeGroup, SizeGroupManager $sizeGroupManager)
    {
        $this->validate($request, [
            'name' => 'required',
            'sizes.*.name' => 'required',
        ]);

        $sizeGroup->name = $request->name;
        $sizeGroup->save();

        $sizeGroupManager->updateSizes($sizeGroup, $request->get('sizes'));

        return $sizeGroup;
    }

    public function destroy(SizeGroup $sizeGroup)
    {
        $sizeGroup->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    public function destroySize(SizeGroup $sizeGroup, Size $size)
    {
        $size->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }
}
