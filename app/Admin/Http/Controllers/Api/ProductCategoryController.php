<?php

namespace App\Admin\Http\Controllers\Api;

use App\Admin\Http\Controllers\BaseAdminController;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductCategoryController extends BaseAdminController
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|mixed|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'data.*.product_id' => 'required|exists:products,id',
            'data.*.category_id' => 'required|exists:categories,id'
        ]);

        foreach ($request->productsId as $productId) {
            $product = Product::findOrFail($productId);
            
            foreach ($request->categoriesId as $categoryId) {
                if(!$product->categories->contains($categoryId)) {
                    $product->categories()->attach($categoryId);
                    $product->save();
                }
            }
        }

        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'data.*.product_id' => 'required|exists:products,id',
            'data.*.category_id' => 'required|exists:categories,id'
        ]);

        foreach ($request->data as $value) {
            $product = Product::findOrFail($value['product_id']);

            if($product->categories->contains($value['category_id'])) {
                $product->categories()->detach($value['category_id']);
                $product->save();
            }
        }

        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function activate(Request $request)
    {
        $this->validate($request, [
            'products_id.*' => 'required|numeric|exists:products,id'
        ]);

        $products = Product::findOrFail($request->products_id);

        foreach ($products as $product) {
            $product->active = 1;
            $product->save();
        }

        return \response('', 204);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function deactivate(Request $request)
    {
        $this->validate($request, [
            'products_id.*' => 'required|numeric|exists:products,id'
        ]);

        $products = Product::findOrFail($request->products_id);

        foreach ($products as $product) {
            $product->active = 0;
            $product->save();
        }

        return \response('', 204);
    }
}
