<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 22.04.17
 * Time: 0:28
 */

namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Transformers\UserNoteTransformer;
use App\Models\OrderItem;
use App\Models\UserNote;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrderItemUserNoteController extends BaseAdminController
{
    public function index(OrderItem $orderItem)
    {
        return fractal()->collection($orderItem->userNotes, new UserNoteTransformer());
    }

    public function store(Request $request, OrderItem $orderItem)
    {
        $this->validate($request, [
            'subject' => 'required',
            'note' => 'required',
            'approve_required' => 'required',
        ]);

        $userNote = new UserNote($request->all());

        $userNote->orderItem()->associate($orderItem);
        $userNote->user()->associate(Auth::user());

        try {
            $userNote->save();
        } catch (\Exception $e) {
            return response('Something Wrong', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response('', Response::HTTP_NO_CONTENT);
    }

}
