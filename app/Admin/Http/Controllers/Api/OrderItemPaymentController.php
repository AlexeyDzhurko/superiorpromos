<?php

namespace App\Admin\Http\Controllers\Api;

use App\Models\OrderItem;
use App\Admin\Transformers\PaymentTransformer;
use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Transformers\CustomerPaymentTransformer;

class OrderItemPaymentController extends BaseAdminController
{
    /**
     * @param OrderItem $orderItem
     * @return \Spatie\Fractal\Fractal
     */
    public function index(OrderItem $orderItem)
    {
        if(!is_null($orderItem->order->payment)) {
            $orderItem->payments->add($orderItem->order->payment);
        }

        return fractal()->collection($orderItem->payments, new PaymentTransformer());
    }

    /**
     * @param OrderItem $orderItem
     * @return \Spatie\Fractal\Fractal
     */
    public function customerPayments(OrderItem $orderItem)
    {
        return fractal()->collection($orderItem->order->user->payment_profiles, new CustomerPaymentTransformer());
    }
}
