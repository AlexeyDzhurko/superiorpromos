<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 23.04.17
 * Time: 2:37
 */

namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Transformers\ArtProofTransformer;
use App\Models\ArtProof;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Storage;
use Auth;

class OrderItemArtProofController extends BaseAdminController
{
    /**
     * @param OrderItem $orderItem
     * @return \Spatie\Fractal\Fractal
     */
    public function index(OrderItem $orderItem)
    {
        return fractal()->collection($orderItem->artProofs, new ArtProofTransformer());
    }

    /**
     * @param Request $request
     * @param OrderItem $orderItem
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request, OrderItem $orderItem)
    {
        $this->validate($request, [
            'file' => 'file|required|mimes:jpg,jpeg,png,pdf,psd,cals|max:10000',
            'note' => 'required',
            'approve_required' => 'required|boolean',
        ]);

        $artProof = new ArtProof($request->all());
        $artProof->orderItem()->associate($orderItem);
        $artProof->user()->associate(Auth::user());

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $fileName = str_random(20) . '.' . $file->getClientOriginalExtension();
            $path = ArtProof::ART_FILE_DIR . $orderItem->id . '/' . $fileName;
            Storage::disk('public')->put($path,  file_get_contents($file));
            $artProof->path = $fileName;
        }

        try {
            $artProof->save();
        } catch (\Exception $e) {
            return response('Something Wrong', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * @param OrderItem $orderItem
     * @param ArtProof $artProof
     * @return mixed
     */
    public function file(OrderItem $orderItem, ArtProof $artProof)
    {
        $prefix = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $filePath = $prefix . ArtProof::ART_FILE_DIR . $orderItem->id . '/' . $artProof->path;

        return response()->file($filePath);
    }

}
