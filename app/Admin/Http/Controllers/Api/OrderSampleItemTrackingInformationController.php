<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Requests\TrackingInformationRequest;
use App\Admin\Transformers\OrderSampleItemTrackingInformationTransformer;
use App\Http\Controllers\Controller;
use App\Models\OrderSampleItem;
use App\Models\OrderSampleTrackingInformation;
use Auth;
use Carbon\Carbon;

class OrderSampleItemTrackingInformationController extends Controller
{
    /**
     * @param OrderSampleItem $orderSampleItem
     * @return \Spatie\Fractal\Fractal
     */
    public function index(OrderSampleItem $orderSampleItem)
    {
        return fractal()->collection($orderSampleItem->orderSampleTrackingInformation, new OrderSampleItemTrackingInformationTransformer());
    }

    /**
     * @param TrackingInformationRequest $request
     * @param OrderSampleItem $orderSampleItem
     * @return \Spatie\Fractal\Fractal
     */
    public function store(TrackingInformationRequest $request, OrderSampleItem $orderSampleItem)
    {
        $trackingInformation = new OrderSampleTrackingInformation();
        $trackingInformation->fill($request->only('tracking_number', 'tracking_shipping_company', 'tracking_shipping_date', 'tracking_note', 'tracking_notify_customer'));

        $trackingInformation->orderSampleItem()->associate($orderSampleItem);
        $trackingInformation->trackingUser()->associate(Auth::user());

        try {
            $trackingInformation->save();

            return fractal()->item($trackingInformation, new OrderSampleItemTrackingInformationTransformer());
        }catch (\Exception $exception) {
            abort(400, 'Error occurred while tracking information adding');
        }
    }
}
