<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 23.04.17
 * Time: 2:37
 */

namespace App\Admin\Http\Controllers\Api;

use App\Admin\Http\Controllers\BaseAdminController;
use App\Models\OrderItem;

/**
 * Class OrderItemController
 * @package App\Admin\Http\Controllers\Api
 */
class OrderItemController extends BaseAdminController
{
    /**
     * @param OrderItem $item
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Symfony\Component\HttpFoundation\Response
     */
    public function delete($item)
    {
        OrderItem::where('id', $item)->delete();

        return response(null, 204);
    }
}
