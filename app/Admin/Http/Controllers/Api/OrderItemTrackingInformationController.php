<?php

namespace App\Admin\Http\Controllers\Api;

use App\Admin\Http\Requests\TrackingInformationRequest;
use App\Admin\Transformers\TrackingInformationTransformer;
use App\Models\OrderItem;
use App\Models\TrackingInformation;
use Illuminate\Http\Request;
use App\Admin\Http\Controllers\BaseAdminController;

class OrderItemTrackingInformationController extends BaseAdminController
{
    /**
     * @param OrderItem $orderItem
     * @return \Spatie\Fractal\Fractal
     */
    public function index(OrderItem $orderItem)
    {
        return fractal()->collection($orderItem->trackingInformations, new TrackingInformationTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TrackingInformationRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TrackingInformationRequest $request, OrderItem $orderItem, TrackingInformation $trackingInformation)
    {
        try {
            $trackingInformation->fill($request->all());
            $trackingInformation->orderItem()->associate($orderItem);
            $trackingInformation->trackingUser()->associate(auth()->user());
            $trackingInformation->save();
            return response($trackingInformation->toJson(), 200);
        } catch (\Exception $e) {
            abort(500, 'Error occurred while tracking information adding');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
