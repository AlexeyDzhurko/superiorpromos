<?php

namespace App\Admin\Http\Controllers\Api;

use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Http\Requests\CreateCustomerRequest;
use App\Admin\Http\Requests\UpdateCustomerRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Image;
use Storage;

class CustomerController extends BaseAdminController
{
    public function __construct()
    {
        $this->middleware('elastic.pagination.page.limit')->only('elasticIndex');
    }

    public function elasticIndex(Request $request)
    {
        $search = $this->elasticSearch($request, User::class);
        foreach (['name', 'active', 'email', 'contact_telephone', 'zip', 'fax', 'company', 'billing_phone', 'date_from', 'date_to', 'role'] as $field){
            if ($request->has($field)) {
                switch ($field) {
                    case 'name':
                        $search->matchPhrasePrefix('name', $request->get('name'));
                        break;
                    case 'email':
                        $search->matchPhrasePrefix('email', $request->get('email'));
                        break;
                    case 'active':
                        $search->term('active', $request->get('active'));
                        break;
                    case 'contact_telephone':
                        $search->matchPhrasePrefix('contact_telephone', $request->get('contact_telephone'));
                        break;
                    case 'zip':
                        $search->nestedMatchPhrasePrefix('addresses', ['zip' => $request->get('zip')]);
                        break;
                    case 'fax':
                        $search->nestedMatchPhrasePrefix('addresses', ['fax' => $request->get('fax')]);
                        break;
                    case 'company':
                        $search->nestedMatchPhrasePrefix('addresses', ['company_name' => $request->get('company')]);
                        break;
                    case 'billing_phone':
                        $search->nestedMatchPhrasePrefix('addresses', ['day_telephone' => $request->get('billing_phone')]);
                        break;
                    case 'date_from':
                        $search->between('created_at', $request->date_from, $request->date_to ?? null);
                        break;
                    case 'date_to':
                        $search->between('created_at', $request->date_from ?? null, $request->date_to);
                        break;
                    case 'role':
                        $search->nestedMatchPhrasePrefix('roles', ['id' => $request->get('role')]);
                        break;
                }
            }
        }

        return $search->paginate($request->get('page', 1), self::DEFAULT_DASHBOARD_PER_PAGE);
    }


    /**
     * @param CreateCustomerRequest $request
     * @return User
     */
    public function store(CreateCustomerRequest $request)
    {
        $user = new User();
        $user->fill($request->all());
        $user->password = bcrypt($request->input('password'));
        $user->active = $request->input('active') ?? 0;
        $user->can_delete = $request->input('can_delete') ?? 0;

        if($user->save()) {
            $user->roles()->sync($request->input('roles') ?? []);
        }

        return $user;
    }

    /**
     * @param UpdateCustomerRequest $request
     * @param User $user
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|string|\Symfony\Component\HttpFoundation\Response
     */
    public function update(UpdateCustomerRequest $request, User $user)
    {
        if(!is_null($user->certificate)) Storage::disk('public')->delete($user->certificate);
        $user->fill($request->all());
        $user->password = bcrypt($request->password);

        $file = $request->certificate;

        if (isset($file) && $file->isValid()) {
            $fullPath = User::CERT_DIR . str_random(20) . '.' . $file->getClientOriginalExtension();
            Storage::disk('public')->put($fullPath,  file_get_contents($file));
            $user->certificate = $fullPath;
        }

        if($user->save()) {
            $user->roles()->sync($request->input('roles') ?? []);

            return response($user);
        } else {
            return response('Error occurred while user saving');
        }
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy(User $user)
    {
        try {
            $user->delete();

            return response('', Response::HTTP_NO_CONTENT);
        } catch (\Exception $exception) {

            return response('error on the server side', 500);
        }
    }
}
