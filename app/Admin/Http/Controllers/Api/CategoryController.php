<?php

namespace App\Admin\Http\Controllers\Api;

use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Http\Requests\CategoryRequest;
use App\Admin\Transformers\CategoryTransformer;
use App\Models\Category;
use App\Models\Product;
use App\Repository\CategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends BaseAdminController
{
    /**
     * Array of values keys for get values in request
     *
     * @var array
     */
    public $only = [
        'name',
        'slug',
        'meta_title',
        'meta_description',
        'keywords',
        'title',
        'description',
        'footer_text',
        'froogle_description',
        'active',
        'parent_id',
        'parent',
        'is_quick_link',
        'image',
        'is_popular',
    ];

    /**
     * Return full categories list with tree
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function index()
    {
        return CategoryRepository::getAllCategoriesWithChild(false);
    }

    /**
     * Store new category resource
     *
     * @param CategoryRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(CategoryRequest $request)
    {
        try {
            return \DB::transaction(function () use ($request) {
                $category = CategoryRepository::insertCategory($request->only($this->only));
                return fractal()->item($category, new CategoryTransformer());
            });
        } catch (\Exception $e) {
            return response('error on the server side', 500);
        }
    }

    /**
     * Update category resource
     *
     * @param CategoryRequest $request
     * @param Category $category
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
        try {
            return \DB::transaction(function () use ($request, $category) {
                $category = CategoryRepository::updateCategory($category, $request->only($this->only));
                return fractal()->item($category, new CategoryTransformer());
            });
        } catch (\Exception $e) {
            return response('error on the server side', 500);
        }
    }

    /**
     * Destroy category
     *
     * @param Category $category
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy(Category $category)
    {
        try {
            DB::transaction(function () use ($category) {
                \Storage::disk('public')->delete(Category::IMAGES_DIR . $category->image_src);
                $category->delete();
            });

            return response('', 204);
        } catch (\Exception $e) {
            return response('error on the server side', 500);
        }
    }

    /**
     * Get products by category
     *
     * @param Category $category
     * @return \Spatie\Fractal\Fractal
     */
    public function productIndex(Category $category)
    {
        $products = $category->products;

        return fractal()->collection($products, function (Product $product) {
            return [
                'id' => $product->id,
                'name' => $product->name,
            ];
        });
    }

    /**
     * Save changed categories tree
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function saveHierarchy(Request $request)
    {
        ini_set('max_execution_time', 0);

        DB::beginTransaction();

        try {
            $cats = $request->all();
            $this->formingCategories($cats);

            Category::rebuild(true);
            $this->saveNewTree($cats);

            DB::commit();
            return response('categories tree rebuilt', 204);
        } catch (\Exception $exception) {
            DB::rollBack();

            return response($exception->getMessage(), $exception->getCode() ?? 500);
        }
    }

    protected function saveNewTree($cats, $parentNode = null)
    {
        $previousNode = null;

//        $modelClass = Category::class;
//        Category::rebuild(true);
//        dd(11);
        foreach ($cats as $category) {
            try {
                $attributes = $this->getDataAttributes(new Category(), $category);
                $nodeId = !empty($category['id']) ? $category['id'] : null;

                $node = Category::firstOrCreate(['id' => $nodeId]);
                $attributes['parent_id'] = $previousNode ? $previousNode->id : null;

                $node->fill($attributes);

                $node->lft = $previousNode ? $previousNode->rgt + 1 : $node->lft ;
                $node->rgt = $previousNode ? $previousNode->rgt + 2 : $node->rgt ;
                $node->save();
                if ($parentNode) {
                    $node->makeChildOf($parentNode);
                } else {
                    $node->parent_id = null;
                }

                if ($previousNode) {
                    $node->makeSiblingOf($previousNode);
                }

                $previousNode = $node;
                $node->save();

                if (!empty($category['children'])) {
                    $this->saveNewTree($category['children'], $node);
                }
            } catch (\Exception $exception) {
                print_r($node);
                print_r($exception);

                continue;
            }
        }
    }

    protected function getDataAttributes($node, $attributes)
    {
        $exceptions = array($node->getKeyName(), 'children');

        return array_except($attributes, $exceptions);
    }

    /**
     * Edit categories array for right format
     *
     * @param array $categories
     */
    public function formingCategories(&$categories)
    {
        foreach ($categories as $key => &$category) {
            if (isset($category['children'])) {
                if (count($category['children']) == 0) {
                    unset($categories[$key]['children']);
                } else {
                    $this->formingCategories($category['children']);
                };
            } else {
                unset($categories[$key]);
                continue;
            }
        }
    }

    public function getListWithoutSeparator()
    {
        $data = Category::getNestedList('name', null, '');

        $result = [];
        foreach ($data as $key => $item) {
            $result[] = [
                'id' => $key,
                'text' => $item,
            ];
        }

        return $result;
    }
}
