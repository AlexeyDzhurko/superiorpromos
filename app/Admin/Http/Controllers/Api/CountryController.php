<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Models\Country;

class CountryController extends BaseAdminController
{
    public function index()
    {
        return Country::all();
    }
}
