<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StateController extends BaseAdminController
{
    public function index()
    {
        return State::all();
    }

    public function update(Request $request, State $state)
    {
        $this->validate($request, [
            'tax' => 'numeric'
        ]);

        if ($request->has('tax')) {
            $state->tax = $request->tax;
        }

        $state->save();

        return response('', Response::HTTP_NO_CONTENT);
    }
}
