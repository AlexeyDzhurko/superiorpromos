<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 17.04.17
 * Time: 17:28
 */

namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Models\Vendor;
use App\Repository\VendorRepository;
use App\Transformers\VendorTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VendorController extends BaseAdminController
{
    public function vendorsList(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'exists:products,id'
        ]);

        if ($request->has('product_id')) {
            $vendors = VendorRepository::getByProductId($request->get('product_id'));
        } else {
            $vendors = Vendor::all();
        }

        return fractal()->collection($vendors, function($vendor) { /** @var Vendor $vendor */
            return [
                'id' => $vendor->id,
                'text' => $vendor->name,
            ];
        });
    }

    public function index(Request $request)
    {
        $params = $this->formedParams($request->all());
        return VendorRepository::getVendorsByParameters($params);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'phone' => 'required|string',
            'fax' => 'string',
            'email' => 'required|email',
            'state_id' => 'required|exists:states,id',
            //'state_ids.*' => 'required|exists:states,id',
            'city' => 'required|string',
            'payment_information_template_id' => 'exists:payment_information_templates,id',
            'zip_code' => 'required|zip',
            'address_1' => 'required|string',
            'address_2' => 'string|nullable',
            'supplier_id' => 'required|string',
            'site' => 'string|nullable',
        ]);

        $data = $request->only([
            'name',
            'phone',
            'fax',
            'email',
            'state_id',
            'city',
            'zip_code',
            'address_1',
            'address_2',
            'supplier_id',
            'site',
            'payment_information_template_id'
        ]);

        $vendor = new Vendor();
        $vendor->fill($data);
        $vendor->save();

        return $vendor;
    }

    public function update(Request $request, Vendor $vendor)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'phone' => 'required|string',
            'fax' => 'string',
            'email' => 'required|email',
            'state_id' => 'required|exists:states,id',
            //'state_ids.*' => 'required|exists:states,id',
            'city' => 'required|string',
            'payment_information_template_id' => 'exists:payment_information_templates,id',
            'zip_code' => 'required|zip',
            'address_1' => 'required|string',
            'address_2' => 'string|nullable',
            'supplier_id' => 'required|string',
            'site' => 'string|nullable',
        ]);

        $data = $request->only([
            'name',
            'phone',
            'fax',
            'email',
            'state_id',
            'city',
            'zip_code',
            'address_1',
            'address_2',
            'supplier_id',
            'site',
            'payment_information_template_id'
        ]);

        $vendor->fill($data);
        //$vendor->states()->sync($request->get('state_ids'));
        $vendor->save();

        return response('', Response::HTTP_NO_CONTENT);
    }

    public function show(Vendor $vendor)
    {
        return fractal()->item($vendor, new VendorTransformer());
    }

    public function destroy(Vendor $vendor)
    {
        try {
            if($vendor->delete()) {
                return response('success', 204);
            } else {
                return response('error on the server side', 500);
            }
        } catch (\Exception $exception) {
            return response('error on the server side', 500);
        }
    }
}
