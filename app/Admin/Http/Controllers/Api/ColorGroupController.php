<?php

namespace App\Admin\Http\Controllers\Api;

use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Http\Requests\ColorGroupRequest;
use App\Models\ColorGroup;

class ColorGroupController extends BaseAdminController
{
    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index()
    {
        return ColorGroup::with('colors')->paginate(self::DEFAULT_DASHBOARD_PER_PAGE);
    }

    /**
     * @param ColorGroupRequest $request
     * @return ColorGroup
     */
    public function store(ColorGroupRequest $request)
    {
        $colorGroup = ColorGroup::create($request->all());

        if ($request->has('colors') and is_array($request->colors)) {
            $colorGroup->colors()->sync($request->colors);
        }

        return $colorGroup->load(['colors']);
    }

    /**
     * @param ColorGroupRequest $request
     * @param string $colorGroupId
     * @return ColorGroup
     */
    public function update(ColorGroupRequest $request, $colorGroupId)
    {
        $colorGroup = ColorGroup::find($colorGroupId);

        $colorGroup->update($request->all());

        if ($request->has('colors') and is_array($request->colors)) {
            $colorGroup->colors()->sync($request->colors);
        }

        return $colorGroup->load(['colors']);
    }

    /**
     * @param $colorGroupId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy($colorGroupId)
    {
        $colorGroup = ColorGroup::find($colorGroupId);
        try {
            if($colorGroup->delete()) {
                return response('success', 204);
            } else {
                return response('error on the server side', 500);
            }
        } catch (\Exception $exception) {
            return response('error on the server side', 500);
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllColorGroups()
    {
        return ColorGroup::get();
    }
}
