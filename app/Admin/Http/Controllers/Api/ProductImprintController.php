<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Admin\Services\ImprintManager;
use App\Models\ImprintPrice;
use Illuminate\Http\Request;
use App\Models\Imprint;
use App\Models\Product;
use App\Transformers\ImprintTransformer;
use Illuminate\Http\Response;

class ProductImprintController extends BaseAdminController
{
    public function store(Request $request, Product $product, ImprintManager $imprintManager)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'color_group_id' => 'nullable|exists:color_groups,id',
            'imprint_prices.*.id' => 'numeric|nullable',
            'imprint_prices.*.quantity' => 'required|numeric',
            'imprint_prices.*.setup_price' => 'numeric|nullable',
            'imprint_prices.*.item_price' => 'numeric|nullable',
            'imprint_prices.*.color_setup_price' => 'numeric|nullable',
            'imprint_prices.*.color_item_price' => 'numeric|nullable',
        ]);

        $imprint = new Imprint();
        $imprint->product()->associate($product);
        $imprint->fill($request->only(['name', 'max_colors', 'color_group_id']));
        $imprint->save();

        $imprintManager->updatePriceGrid($imprint, $request->get('imprint_prices'));

        return fractal()->item($imprint, new ImprintTransformer());
    }

    public function update(Request $request, Product $product, Imprint $imprint, ImprintManager $imprintManager)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'color_group_id' => 'nullable|exists:color_groups,id',
            'imprint_prices.*.id' => 'numeric|nullable',
            'imprint_prices.*.quantity' => 'required|numeric',
            'imprint_prices.*.setup_price' => 'numeric|nullable',
            'imprint_prices.*.item_price' => 'numeric|nullable',
            'imprint_prices.*.color_setup_price' => 'numeric|nullable',
            'imprint_prices.*.color_item_price' => 'numeric|nullable',
        ]);

        $imprint->fill($request->only(['name', 'max_colors', 'color_group_id']));
        $imprint->save();

        if ($request->has('imprint_prices')) {
            $imprintManager->updatePriceGrid($imprint, $request->get('imprint_prices'));
        }

        return fractal()->item($imprint, new ImprintTransformer());
    }

    public function destroy(Product $product, Imprint $imprint)
    {
        $imprint->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    public function destroyImprintPrice(Product $product, Imprint $imprint, ImprintPrice $imprintPrice)
    {
        $imprintPrice->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }
}
