<?php


namespace App\Admin\Http\Controllers\Api;


use App\Admin\Http\Controllers\BaseAdminController;
use App\Models\ProductIcon;
use App\Transformers\ProductIconTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductIconController extends BaseAdminController
{
    public function index()
    {
        return fractal()->collection(ProductIcon::all(), new ProductIconTransformer());
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'icon' => 'required|image',
            'name' => 'required',
            'text' => 'required',
            'position' => 'required',
        ]);

        $productIcon = new ProductIcon($request->only(['name', 'text', 'position']));
        $productIcon->icon_pic_src = $request->file('icon')->store('product-icon', 'public');
        $productIcon->save();

        return fractal()->item($productIcon, new ProductIconTransformer());
    }

    public function update(Request $request, ProductIcon $productIcon)
    {
        $this->validate($request, [
            'icon' => 'image',
            'name' => 'required',
            'text' => 'required',
            'position' => 'required',
        ]);

        $productIcon->fill($request->only(['name', 'text', 'position']));

        if ($request->hasFile('icon')) {
            $productIcon->icon_pic_src = $request->file('icon')->store('product-icon', 'public');
        }

        $productIcon->save();

        return fractal()->item($productIcon, new ProductIconTransformer());
    }

    public function destroy(ProductIcon $productIcon)
    {
        $productIcon->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }
}
