<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Entities\SizeGroupsEntity;
use App\Admin\Entities\SizeEntity;
use App\Models\SizeGroup;
use App\Models\Size;
use App\Repository\SizeGroupRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Admin\Http\Requests\SizeGroupRequest;

class SizeGroupController extends BaseAdminController
{
    /**
     * @param Request $request
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $page = 1)
    {
        $adminEntity = new SizeGroupsEntity();
        $adminSizeEntity = new SizeEntity();
        $params = $this->formedParams($request->all());

        $dataTypeContent = SizeGroupRepository::getGroupByParameters($params);
        $sizes = Size::all();

        return view('admin.SizeGroup.index', [
            'dataTypeContent' => $dataTypeContent,
            'adminModel' => $adminEntity,
            'parameters' => $params,
            'sizes' => $sizes,
            'adminSizeModel' => $adminSizeEntity,
        ]);
    }

    /**
     * Return page with edit item form
     *
     * @param SizeGroup $sizeGroup
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(SizeGroup $sizeGroup)
    {
        $adminEntity = new SizeGroupsEntity();
        $options = SizeGroupRepository::getSizeIdsArray($sizeGroup);

        return view('admin.SizeGroup.edit', [
            'adminModel' => $adminEntity,
            'dataTypeContent' => $sizeGroup->load('sizes'),
            'sizesIds' => $options
        ]);
    }

    /**
     * Update existed SizeGroup
     *
     * @param SizeGroupRequest $request
     * @param SizeGroup $sizeGroup
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SizeGroupRequest $request, SizeGroup $sizeGroup)
    {
        $adminEntity = new SizeGroupsEntity();
        $data = [
            'name' => $request->get('name'),
            'sizes' => array_keys($request->get('sizes')),
        ];
        SizeGroupRepository::updateSizeGroup($sizeGroup, $data);

        return redirect(route('size_group.index'))
        ->with([
            'message' => 'Successfully Updated ' . $adminEntity->getDisplayNameSingular(),
            'alert-type' => 'success'
        ]);
    }

    /**
     * Return page with create item form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $adminEntity = new SizeGroupsEntity();

        return view('admin.SizeGroup.edit', [
            'adminModel' => $adminEntity,
            'dataTypeContent' => [],
            'sizesIds' => []
        ]);
    }

    /**
     * Save new SizeGroup to database
     *
     * @param SizeGroupRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SizeGroupRequest $request)
    {
        $adminEntity = new SizeGroupsEntity();
        $data = [
            'name' => $request->get('name'),
            'sizes' => array_keys($request->get('sizes')),
        ];
        SizeGroupRepository::insertSizeGroup($data);

        return redirect(route('size_group.index'))
        ->with([
            'message' => 'Successfully Added New ' . $adminEntity->getDisplayNameSingular(),
            'alert-type' => 'success'
        ]);
    }
}
