<?php


namespace App\Admin\Http\Controllers;


use App\Jobs\GenerateFroogleFiles;
use App\Models\Category;
use Blade;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Storage;

class FroogleController extends BaseAdminController
{
    public function index()
    {
        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');

        $existFiles = [];

        $xmlPath = storage_path('app/public/froogle/froogle.xml');
        if (file_exists($xmlPath)) {
            $existFiles['xml'] = [
                'path' => Storage::disk('local')->url('public/froogle/froogle.xml'),
                'createdAt' => Carbon::createFromTimestamp(filemtime($xmlPath))
                    ->timezone('america/new_york')
                    ->format('m/d/Y g:i:s A')
            ];
        }

        $txtPath = storage_path('app/public/froogle/froogle.txt');
        if (file_exists($txtPath)) {
            $existFiles['txt'] = [
                'path' => Storage::disk('local')->url('public/froogle/froogle.txt'),
                'createdAt' => Carbon::createFromTimestamp(filemtime($txtPath))
                    ->timezone('america/new_york')
                    ->format('m/d/Y g:i:s A')
            ];
        }

        return view('admin.Froogle.index', compact('existFiles'));
    }

    public function generate(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'nullable|exists:categories,id'
        ]);

        if ($request->has('category_id')) {
            $category[] = Category::find($request->input('category_id'));
            $this->dispatch(new GenerateFroogleFiles($category));

            return response('', Response::HTTP_NO_CONTENT);
        }

        $categories = Category::all();

        $this->dispatch(new GenerateFroogleFiles($categories->all()));

        return response('', Response::HTTP_NO_CONTENT);
    }

    public function xml()
    {
        $prefix = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        return response()->download($prefix . 'froogle/froogle.xml');
    }

    public function csv()
    {
        $prefix = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        return response()->download($prefix . 'froogle/froogle.txt');
    }
}
