<?php


namespace App\Admin\Http\Controllers;


use App\Admin\Exceptions\NotSupportedException;
use Blade;

class DashboardController extends BaseAdminController
{
    public function index($dashboardModel)
    {
        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');

        if (view()->exists('admin.dashboard.' . $dashboardModel . '.index')) {
            return view('admin.dashboard.' . $dashboardModel . '.index');
        }

        throw new NotSupportedException();
    }

}
