<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Transformers\CategoryTransformer;
use App\Models\Category;
use Symfony\Component\HttpFoundation\Request;
use Blade;

class ProductCategoryController extends BaseAdminController
{
    /**
     * @param Request $request
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');

        return view('admin.ProductCategory.index', [
            'nestedCategories' => fractal()->collection(Category::all()->toHierarchy(), new CategoryTransformer()),
        ]);
    }

}
