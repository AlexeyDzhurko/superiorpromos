<?php

namespace App\Admin\Http\Controllers;

use App\Models\Address;
use App\Models\OrderSample;
use App\Repository\OrderSampleRepository;
use Blade;
use Auth;

class OrderSampleController
{

    public function index()
    {
        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');

        return view('admin.OrderSample.index');
    }

    public function edit(OrderSample $orderSample)
    {
        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');

        return view('admin.OrderSample.edit-add', [
            'order' => $orderSample,
            'collections' => []
        ]);
    }
}
