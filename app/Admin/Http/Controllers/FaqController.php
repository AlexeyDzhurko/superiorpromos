<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Entities\FaqEntity;
use App\Repository\FaqRepository;
use Symfony\Component\HttpFoundation\Request;

class FaqController extends BaseAdminController
{
    /**
     * @param Request $request
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $page = 1)
    {
        $adminEntity = new FaqEntity();
        $params = $this->formedParams($request->all());

        $dataTypeContent = FaqRepository::getFaqsByParameters($params);

        return view('admin.Faq.index', [
            'dataTypeContent' => $dataTypeContent,
            'adminModel' => $adminEntity,
            'parameters' => $params
        ]);
    }
}
