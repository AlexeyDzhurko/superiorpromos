<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Contracts\AdminEntity;
use App\Admin\Contracts\AdminResolver;
use App\Admin\FormField;
use App\Admin\FormFieldExtractor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Storage;

class BreadController
{
    protected $resolver;

    public function __construct(AdminResolver $resolver)
    {
        $this->resolver = $resolver;
    }

    /**
     * Read list of enetities
     *
     * @param AdminEntity $adminEntity
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(AdminEntity $adminEntity)
    {
        // Next Get the actual content from the MODEL that corresponds to the slug DataType
        $dataTypeContent = call_user_func([$adminEntity->getModel(), 'all']);
        $slug = $adminEntity->getBaseRouteName();
        if (view()->exists('admin.' . $slug . '.browse')) {
            return view('admin.' . $slug . '.browse',
                ['dataTypeContent' => $dataTypeContent]);
        } else {
            if (view()->exists('voyager::' . $slug . '.browse')) {
                return view('voyager::' . $slug . '.browse',
                    ['dataTypeContent' => $dataTypeContent]);
            } else {
                return view('voyager::bread.browse',
                    ['dataTypeContent' => $dataTypeContent, 'adminModel' => $adminEntity]);
            }
        }
    }

    /**
     * Read specific item
     *
     * @param AdminEntity $adminEntity
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(AdminEntity $adminEntity, $id)
    {
        $dataTypeContent = call_user_func([$adminEntity->getModel(), 'find'], $id);

        return view('voyager::bread.read', ['adminModel' => $adminEntity, 'dataTypeContent' => $dataTypeContent]);
    }

    /**
     * Return page with edit item form
     *
     * @param AdminEntity $adminEntity
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(AdminEntity $adminEntity, $id)
    {
        $dataTypeContent = call_user_func([$adminEntity->getModel(), 'find'], $id);
        $slug = $adminEntity->getBaseRouteName();

        if (view()->exists('admin.' . $slug . '.edit-add')) {
            return view('admin.' . $slug . '.edit-add',
                ['adminModel' => $adminEntity, 'dataTypeContent' => $dataTypeContent]);
        } else {
            if (view()->exists('voyager::' . $slug . '.edit-add')) {
                return view('voyager::' . $slug . '.edit-add',
                    ['adminModel' => $adminEntity, 'dataTypeContent' => $dataTypeContent]);
            } else {
                return view('voyager::bread.edit-add',
                    ['adminModel' => $adminEntity, 'dataTypeContent' => $dataTypeContent]);
            }
        }
    }

    /**
     * Return page with create item form
     *
     * @param AdminEntity $adminEntity
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(AdminEntity $adminEntity)
    {
        $slug = $adminEntity->getBaseRouteName();
        if (view()->exists('admin.' . $slug . '.edit-add')) {
            return view('admin.' . $slug . '.edit-add', ['adminModel' => $adminEntity]);
        } else {
            if (view()->exists('voyager::' . $slug . '.edit-add')) {
                return view('voyager::' . $slug . '.edit-add', ['adminModel' => $adminEntity]);
            } else {
                return view('voyager::bread.edit-add', ['adminModel' => $adminEntity]);
            }
        }
    }

    /**
     * Save new item to database
     *
     * @param AdminEntity $adminEntity
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminEntity $adminEntity, Request $request)
    {
        if (function_exists('voyager_add_post')) {
            voyager_add_post($request);
        }

        $modelClass = $adminEntity->getModel();

        $data = new $modelClass;
        $result = $this->insertOrUpdateData($request, $adminEntity, $data);

        if ($result !== 'success') {
            return redirect()->back()->with([
                'message' => implode('; ', $result),
                'alert-type' => 'error'
            ]);
        }

        return redirect(route('admin_entity.index', ['admin_entity' => $adminEntity->getBaseRouteName()]))->with([
            'message' => 'Successfully Added New ' . $adminEntity->getDisplayNameSingular(),
            'alert-type' => 'success'
        ]);
    }

    /**
     * Update existed item
     *
     * @param AdminEntity $adminEntity
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminEntity $adminEntity, Request $request, $id)
    {
        $data = call_user_func([$adminEntity->getModel(), 'find'], $id);
        $result = $this->insertOrUpdateData($request, $adminEntity, $data);

        if ($result !== 'success') {
            return redirect()->back()->with([
                'message' => implode('; ', $result),
                'alert-type' => 'error'
            ]);
        }

        return redirect(route('admin_entity.index', ['admin_entity' => $adminEntity->getBaseRouteName()]))->with([
            'message' => 'Successfully Updated ' . $adminEntity->getDisplayNameSingular(),
            'alert-type' => 'success'
        ]);
    }

    /**
     * Delete a specific item
     *
     * @param AdminEntity $adminEntity
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(AdminEntity $adminEntity, $id)
    {
        $data = call_user_func([$adminEntity->getModel(), 'find'], $id);

        foreach ($adminEntity->formFields() as $formField) { /** @var FormField $formField */
            if ($formField->type == FormField::TYPE_IMAGE) {
                if (\Storage::exists('/uploads/' . $data->{$formField->field})) {
                    Storage::delete('/uploads/' . $data->{$formField->field});
                }
            }
        }

        if ($data->destroy($id)) {
            return redirect(route('admin_entity.index', ['admin_entity' => $adminEntity->getBaseRouteName()]))->with([
                'message' => 'Successfully Deleted ' . $adminEntity->getDisplayNameSingular(),
                'alert-type' => 'success'
            ]);
        }

        return redirect(route('admin_entity.index', ['admin_entity' => $adminEntity->getBaseRouteName()]))->with([
            'message' => 'Sorry it appears there was a problem deleting this ' . $adminEntity->getDisplayNameSingular(),
            'alert-type' => 'error'
        ]);
    }

    /**
     * Save given admin entity to database
     *
     * @param $request
     * @param AdminEntity $adminEntity
     * @param Model $data target model
     * @return array|string array returns if empty required field
     */
    protected function insertOrUpdateData($request, AdminEntity $adminEntity, Model $data)
    {
        $requiredErrors = [];
        foreach ($adminEntity->formFields() as $row) {
            $content = FormFieldExtractor::getValue($request, $adminEntity, $row);

            if ($row->required && empty($content)) {
                $requiredErrors[] =  $row->field . ' field is required';
            }

            if ($content === null) {
                if (isset($data->{$row->field})) {
                    $content = $data->{$row->field};
                }
                if ($row->field == FormField::TYPE_PASSWORD) {
                    $content = $data->{$row->field};
                }
                if ($row->field == FormField::TYPE_CHECKBOX_LIST) {
                }
            }

            $data->{$row->field} = $content;
        }

        if (empty($requiredErrors)) {
            $data->save();

            return 'success';
        } else {
            return $requiredErrors;
        }
    }
}
