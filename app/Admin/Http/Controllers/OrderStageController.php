<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 31.03.17
 * Time: 14:12
 */

namespace App\Admin\Http\Controllers;


use App\Admin\Transformers\HistoryStageTransformer;
use App\Contracts\MailService;
use App\Http\Controllers\Controller;
use App\Models\OrderItem;
use App\Models\OrderSampleItem;
use App\Models\Stage;
use App\Notifications\BigMailer\OrderFeedbackNotification;
use App\Notifications\BigMailer\OrderStatusUpdateNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;

class OrderStageController extends Controller
{
    public function index(OrderItem $orderItem)
    {
        return fractal()->collection($orderItem->stages()->orderBy('order_item_stage.created_at', 'desc')->get(), new HistoryStageTransformer());
    }

    public function store(Request $request, OrderItem $orderItem, MailService $mailService)
    {
        $this->validate($request, [
            'stage_id' => 'required|exists:stages,id',
            'notify_customer' => 'boolean',
        ]);

        $orderItem->stage_id = $request->get('stage_id');
        $orderItem->save();
        $orderItem->stages()->attach($request->get('stage_id'), ['created_at' => new Carbon(), 'user_id' => Auth::id()]);

        $orderItem->order->touch();

        if($request->input('notify_customer')) {
            $mailService->sendSingleTransactionalEmail(new OrderStatusUpdateNotification(
                [$orderItem->order->user->email],
                [
                    'customer_id' => $orderItem->order->user->id,
                    'customer_name' => $orderItem->order->user->name,
                    'order_item_id' => $orderItem->id,
                    'customer_email' => $orderItem->order->user->email,
                    'order_stage' => $orderItem->stage->name
                ]
            ));

            if($orderItem->stage->id == Stage::COMPLETE_ID) {
                $mailService->sendSingleTransactionalEmail(new OrderFeedbackNotification(
                    [$orderItem->order->user->email],
                    [
                        'customer_name' => $orderItem->order->user->name,
                        'order_item_id' => $orderItem->id
                    ]
                ));
            }
        }
    }

    public function getOrderSampleStage(OrderSampleItem $orderSampleItem)
    {
        return fractal()->collection($orderSampleItem->stages()->orderBy('order_sample_item_stage.created_at', 'desc')->get(), new HistoryStageTransformer());
    }

    public function saveOrderSampleStage(Request $request, OrderSampleItem $orderSampleItem)
    {
        $this->validate($request, [
            'stage_id' => 'required|exists:stages,id',
            'notify_customer' => 'boolean',
        ]);

        $orderSampleItem->stage_id = $request->get('stage_id');
        $orderSampleItem->save();

        $orderSampleItem->stages()->attach($request->get('stage_id'), ['created_at' => new Carbon(), 'user_id' => Auth::id()]);
    }
}
