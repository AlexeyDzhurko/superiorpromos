<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 21.03.17
 * Time: 1:22
 */

namespace App\Admin\Http\Controllers;


use App\Admin\Entities\OrderItemEntity;
use App\Admin\Http\Requests\UpdateOrderItemRequest;
use App\Contracts\MailService;
use App\Models\ArtProof;
use App\Models\OrderItem;
use App\Notifications\BigMailer\FinalInvoiceNotification;
use App\Payment\CartItemCost;
use Illuminate\Http\Response;
use Blade;
use Storage;
use Auth;

class OrderItemController extends BaseAdminController
{
    public function edit(OrderItem $orderItem)
    {
        $adminEntity = new OrderItemEntity();
        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');
        $cartItemCost = new CartItemCost($orderItem->cartItem);

        $artFiles = Storage::disk('public')->files(ArtProof::CUSTOMER_ART_FILE_DIR . $orderItem->id);
        return view('admin.OrderItem.edit-add', [
            'adminModel' => $adminEntity,
            'orderItem' => $orderItem,
            'cartItemCost' => $cartItemCost,
            'artFiles' => $artFiles,
            'collections' => []
        ]);
    }

    public function update(UpdateOrderItemRequest $request, OrderItem $orderItem, MailService $mailService)
    {
        if ($request->has('auto_remind')) {
            $orderItem->auto_remind = $request->get('auto_remind');
        }

        if ($request->has('shipping_title')) {
            $orderItem->fill($request->only(array_keys($request->getAddressFields('shipping'))));
        }

        if ($request->has('billing_title')) {
            $orderItem->fill($request->only(array_keys($request->getAddressFields('billing'))));
        }

        if ($request->has('is_important')) {
            $orderItem->is_important = $request->is_important;
        }

        if ($request->hasFile('invoice_file')) {
            $orderItem->invoice_file = $request->file('invoice_file')->store('invoices', 'local');
            if($request->input('invoice_notify_customer')) {
                if(!is_null($user = $orderItem->order->user)) {
                    $mailService->sendSingleTransactionalEmail(new FinalInvoiceNotification(
                        [$user->email],
                        [
                            'customer_id' => $user->id,
                            'customer_name' => $user->name,
                            'order_item_id' => $orderItem->id,
                    ]
                    ));
                }
            }
        }

        if ($request->has('not_paid')) {
            $orderItem->not_paid = $request->not_paid;
        }

        $orderItem->fill($request->all());

        $orderItem->save();

        return response('', Response::HTTP_NO_CONTENT);
    }

    public function invoice(OrderItem $orderItem)
    {
        $prefix = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        return response()->download($prefix . $orderItem->invoice_file);
    }

    public function destroyInvoice(OrderItem $orderItem)
    {
        if ($orderItem->invoice_file) {
            $orderItem->invoice_file = null;
        }
        $orderItem->save();

        return response('', Response::HTTP_NO_CONTENT);
    }

    public function show(OrderItem $orderItem)
    {
        return $orderItem;
    }

}
