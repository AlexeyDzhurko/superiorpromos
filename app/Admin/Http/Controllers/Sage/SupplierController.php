<?php

namespace App\Admin\Http\Controllers\Sage;

use App\Admin\Http\Requests\ImportProductRequest;
use App\Admin\Http\Requests\SageProductDetailRequest;
use App\Admin\Http\Requests\SageQuickSearchRequest;
use App\Admin\Http\Requests\SageSearchBySupplierRequest;
use App\Repository\ColorRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\VendorRepository;
use App\Repository\CategoryRepository;
use App\Admin\Contracts\CreateProduct;
use App\Models\ColorGroup;

class SupplierController extends SageController
{
    /**
     * Get list of Categories from Sage
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.sage.Supplier.index', [
            'categories' => CategoryRepository::getAllCategoriesWithChild(false),
            'imprintColorGroups' => ColorGroup::get(),
            'nestedCategories' => CategoryRepository::getAllCategories()
        ]);
    }

    /**
     * Ajax request for get list of suppliers
     * @param Request $request
     * @return array
     */
    public function list(Request $request)
    {
        $type = $request->get('type');
        $query = $request->get('query');

        return VendorRepository::searchVendor($type, $query);
    }

    /**
     * @param ImportProductRequest $request
     * @param CreateProduct $productService
     */
    public function import(ImportProductRequest $request, CreateProduct $productService)
    {
        $productService->addProduct($request->all());
    }

    /**
     * Ajax request for get supplier's list of product
     * @param SageSearchBySupplierRequest $request
     * @return array
     */
    public function productList(SageSearchBySupplierRequest $request)
    {
        return $this->sage->getSuppliersProductList($request);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function checkColors(Request $request)
    {
        $colors = $request->get('colors');

        return ColorRepository::countColors($colors);
    }

    /**
     * Ajax request for get products search
     * @param SageQuickSearchRequest $request
     * @return array
     */
    public function productsSearch(SageQuickSearchRequest $request)
    {
        return $this->sage->ProductsSearch($request);
    }

    /**
     * Ajax request for get product detail
     * @param SageProductDetailRequest $request
     * @return array
     */
    public function productDetail(SageProductDetailRequest $request)
    {
        $product = $this->sage->getProductDetail($request->product_id);
        $supplier = $this->sage->getSupplierInfo($product['SuppID']);

        VendorRepository::insertOrUpdate($supplier);
        $supplier = VendorRepository::getVendorById($supplier['supplier_id']);
        $product['supplier_id'] = $supplier->id;

        return $product;
    }
}
