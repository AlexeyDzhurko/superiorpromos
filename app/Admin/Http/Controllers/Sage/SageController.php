<?php

namespace App\Admin\Http\Controllers\Sage;

use App\Admin\Contracts\Sage;

class SageController
{
    /**
     * @var Sage
     */
    protected $sage;

    /**
     * SageController constructor.
     * @param Sage $sage
     */
    public function __construct(Sage $sage)
    {
        $this->sage = $sage;
    }
}
