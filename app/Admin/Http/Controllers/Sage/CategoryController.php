<?php

namespace App\Admin\Http\Controllers\Sage;

class CategoryController extends SageController
{
    /**
     * Get list of Categories from Sage
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        try {
            $data = $this->sage->getCategoriesList();

            return view('admin.sage.Category.index', [
            'dataTypeContent' => $data,
        ]);
        }
        catch (\Exception $e) {
            return response('Error on the server side: ' . $e->getMessage(), 500);
        }
    }
}
