<?php

namespace App\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use \Storage;

class HomeController extends Controller
{

    public function index()
    {
        \Blade::setContentTags('<%', '%>');
        \Blade::setEscapedContentTags('<%%', '%%>');
        return view('vendor.voyager.index');
    }

    public function logout()
    {
        Auth::logout();
        return redirect(route('admin.logout'));
    }

    public function upload(Request $request)
    {
        $valid_exts = ['jpeg', 'jpg', 'png', 'gif']; // valid extensions
        $full_filename = null;

        $slug = $request->input('type_slug');
        $file = $request->file('image');
        $filename = str_random(20);

        $path = $slug . '/' . date('F') . date('Y') . '/';
        $full_path = $path . $filename . '.' . $file->getClientOriginalExtension();

        $ext = $file->guessClientExtension();

        $resize_width = 1800;
        $resize_height = null;

        if (in_array($ext, $valid_exts)) {
            $image = Image::make($file)->resize($resize_width, $resize_height, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->encode($file->getClientOriginalExtension(), 75);

            // move uploaded file from temp to uploads directory
            if (Storage::put(config('voyager.storage.subfolder') . $full_path, (string)$image, 'public')) {
                $status = 'Image successfully uploaded!';
                $full_filename = $full_path;
            } else {
                $status = 'Upload Fail: Unknown error occurred!';
            }
        } else {

            $status = 'Upload Fail: Unsupported file format or It is too large to upload!';
        }

        //echo out script that TinyMCE can handle and update the image in the editor

        return ("<script> parent.setImageValue('" . $this->image($full_filename) . "'); </script>");
    }

    public function image($file, $default = '')
    {
        if (!empty($file) && Storage::exists(config('voyager.storage.subfolder') . $file)) {
            return Storage::url(config('voyager.storage.subfolder') . $file);
        }

        return $default;
    }

    public function profile()
    {
        return view('vendor.voyager.profile');
    }
}
