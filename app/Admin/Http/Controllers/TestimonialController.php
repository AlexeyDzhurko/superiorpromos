<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Entities\TestimonialEntity;
use App\Admin\Http\Requests\CreateTestimonialRequest;
use App\Admin\Http\Requests\UpdateTestimonialRequest;
use App\Models\Testimonial;
use App\Repository\TestimonialRepository;
use Symfony\Component\HttpFoundation\Request;

class TestimonialController extends BaseAdminController
{
    /**
     * @param Request $request
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $page = 1)
    {
        $adminEntity = new TestimonialEntity();
        $params = $this->formedParams($request->all());

        $dataTypeContent = TestimonialRepository::getTestimonialsByParameters($params);

        return view('admin.Testimonial.index', [
            'dataTypeContent' => $dataTypeContent,
            'adminModel' => $adminEntity,
            'parameters' => $params
        ]);
    }
}
