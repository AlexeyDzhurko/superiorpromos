<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 30.03.17
 * Time: 19:24
 */

namespace App\Admin\Http\Controllers;


use App\Models\Stage;
use Blade;

class StageController
{
    public function index()
    {
        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');

        return view('admin.Stage.index', [
            'stages' => Stage::all(),
        ]);
    }
}
