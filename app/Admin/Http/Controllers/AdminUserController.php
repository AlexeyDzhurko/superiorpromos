<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Entities\AdminUserEntity;
use App\Admin\Http\Requests\CreateAdminUserRequest;
use App\Admin\Http\Requests\UpdateAdminUserRequest;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;

class AdminUserController extends BaseAdminController
{
    /**
     * @param Request $request
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $page = 1)
    {
        $adminEntity = new AdminUserEntity();
        $params = $this->formedParams($request->all());

        $dataTypeContent = UserRepository::getAdminUsersByParameters($params);

        return view('admin.AdminUser.index', [
            'dataTypeContent' => $dataTypeContent,
            'adminModel' => $adminEntity,
            'parameters' => $params
        ]);
    }

    /**
     * Save new item to database
     *
     * @param AdminUserEntity $adminEntity
     * @param CreateAdminUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminUserEntity $adminEntity, CreateAdminUserRequest $request)
    {
        UserRepository::storeAdminUser($request->all());

        return redirect(route('admin_entity.index', ['admin_entity' => $adminEntity->getBaseRouteName()]))->with([
            'message' => 'Successfully Added New ' . $adminEntity->getDisplayNameSingular(),
            'alert-type' => 'success'
        ]);
    }

    /**
     * Update existed item
     *
     * @param AdminUserEntity $adminEntity
     * @param UpdateAdminUserRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminUserEntity $adminEntity, UpdateAdminUserRequest $request, $id)
    {
        UserRepository::updateAdminUser($request->all(), $id);

        return redirect(route('admin_entity.index', ['admin_entity' => $adminEntity->getBaseRouteName()]))->with([
            'message' => 'Successfully Updated ' . $adminEntity->getDisplayNameSingular(),
            'alert-type' => 'success'
        ]);
    }

}
