<?php

namespace App\Admin\Http\Controllers;
use Illuminate\Http\Request;
use Storage;

class CompanyInfoController extends BaseAdminController
{
    /**
     * Index Company Info page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $exists = Storage::disk('local')->exists('company_info');
        $data = '';

        if ($exists) {
            $data = json_decode(Storage::disk('local')->get('company_info'), true);
        }

        return view('admin.CompanyInfo.index', [
            'data' => $data
        ]);
    }

    /**
     * Update Company Info page
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        Storage::disk('local')->put('company_info', json_encode($request->only(['title', 'description'])));
        return redirect(route('company_info.index'));
    }
}
