<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Http\Requests\PromotionalBlockRequest;
use App\Models\PromotionalBlock;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Repository\PromotionalBlockRepository;
use Symfony\Component\HttpFoundation\Request;


class PromotionalBlockController extends BaseAdminController
{
    /**
     * Show Promotional Blocks page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $promotionalBlocks = PromotionalBlock::with('category')->get();
        $blocksCollections = $this->formedCollection();

        return view('admin.PromotionalBlock.index', [
            'promotionalBlocks' => $promotionalBlocks,
            'topBlockCollections' => $blocksCollections[PromotionalBlock::TOP_BLOCK],
            'topMiddleCollections' => $blocksCollections[PromotionalBlock::MIDDLE_BLOCK],
            'topBottomCollections' => $blocksCollections[PromotionalBlock::BOTTOM_BLOCK],
        ]);
    }

    /**
     * Save new Promotional Blocks to database
     *
     * @param PromotionalBlockRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PromotionalBlockRequest $request)
    {
        PromotionalBlockRepository::storePromotionalBlocks($request->only([
            PromotionalBlock::TOP_BLOCK,
            PromotionalBlock::MIDDLE_BLOCK,
            PromotionalBlock::BOTTOM_BLOCK
        ]));

        return redirect(route('promotional_block.index'))
            ->with([
                'message' => 'Successfully updated promotional blocks',
                'alert-type' => 'success'
            ]);
    }

    /**
     * Search product by name
     *
     * @param Request $request
     * @return array
     */
    public function search(Request $request)
    {
        $query = $request->get('query');
        $data = ProductRepository::getProductByName($query);
        return $data;
    }

    /**
     * Search category by name
     *
     * @param Request $request
     * @return array
     */
    public function searchCategory(Request $request)
    {
        $query = $request->get('query');
        $data = CategoryRepository::getCategoryByName($query);
        return $data;
    }

    /**
     * @return array
     */
    protected function formedCollection()
    {
        $result = [
            PromotionalBlock::TOP_BLOCK => [],
            PromotionalBlock::MIDDLE_BLOCK => [],
            PromotionalBlock::BOTTOM_BLOCK => [],
        ];

        $promotionalBlocks = PromotionalBlock::all();

        foreach ($promotionalBlocks as $promotionalBlock) {
            foreach ($promotionalBlock->products as $product)
            $result[$promotionalBlock->id][] = [
                'id' => $product['id'],
                'text' => $product['name'],
            ];
        }

        return $result;
    }
}
