<?php

namespace App\Admin\Http\Controllers;

use App\Models\Color;
use App\Repository\ColorRepository;
use App\Admin\Entities\ColorEntity;
use Symfony\Component\HttpFoundation\Request;
use App\Admin\Http\Requests\ColorRequest;

class ColorController extends BaseAdminController
{
    /**
     * @param Request $request
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $page = 1)
    {
        $adminEntity = new ColorEntity();
        $params = $this->formedParams($request->all());

        $dataTypeContent = ColorRepository::getColorsByParameters($params);

        return view('admin.Color.index', [
            'dataTypeContent' => $dataTypeContent,
            'adminModel' => $adminEntity,
            'parameters' => $params
        ]);
    }

    /**
     * Return page with edit item form
     *
     * @param Color $color
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Color $color)
    {
        $adminEntity = new ColorEntity();

        return view('admin.Color.edit-add', [
            'adminModel' => $adminEntity,
            'dataTypeContent' => $color,
            'collections' => $this->formedCollection($color)
        ]);
    }

    /**
     * Return page with create item form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $adminEntity = new ColorEntity();

        return view('admin.Color.edit-add', [
            'adminModel' => $adminEntity,
            'dataTypeContent' => [],
            'collections' => []
        ]);
    }

    /**
     * Save new Color to database
     *
     * @param ColorRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ColorRequest $request)
    {
        $adminEntity = new ColorEntity();

        ColorRepository::insertColor($request->all());

        return redirect(route('color.index'))
            ->with([
                'message' => 'Successfully Added New ' . $adminEntity->getDisplayNameSingular(),
                'alert-type' => 'success'
            ]);
    }

    /**
     * Save new Color to database
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function multipleStore(Request $request)
    {
        foreach ($request->get('colors') as $data) {
            ColorRepository::insertColor($data);
        }

        return response()->json(['ok']);
    }

    /**
     * Update existed Color
     *
     * @param ColorRequest $request
     * @param Color $color
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ColorRequest $request, Color $color)
    {
        $adminEntity = new ColorEntity();

        ColorRepository::updateColor($color, $request->all());

        return redirect(route('color.index'))
            ->with([
                'message' => 'Successfully Updated ' . $adminEntity->getDisplayNameSingular(),
                'alert-type' => 'success'
            ]);
    }

    /**
     * Search color by name
     * @param Request $request
     * @return array
     */
    public function search(Request $request)
    {
        $query = $request->get('query', '');
        $data = ColorRepository::getColorsByName($query);

        return $data;
    }

    /**
     * Search color by name
     * @param Request $request
     * @return array
     */
    public function find(Request $request)
    {
        $query = $request->get('data');
        $data = ColorRepository::findColorsByName($query);

        return $data;
    }

    /**
     * @return array
     */
    public function list()
    {
        return ColorRepository::list();

    }

    /**
     * @param Color $color
     * @return array
     */
    private function formedCollection(Color $color)
    {
        $result = [];
        $data = $color->groups()->get();

        foreach ($data as $item) {
            $result[] = [
                'id' => $item->id,
                'text' => $item->name,
            ];
        }

        return $result;
    }
}
