<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Entities\OrderSampleItemEntity;
use App\Admin\Http\Requests\UpdateOrderItemRequest;
use App\Models\OrderSampleItem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Blade;

class OrderSampleItemController extends BaseAdminController
{
    public function edit(OrderSampleItem $orderSampleItem)
    {
        $adminEntity = new OrderSampleItemEntity();
        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');

        return view('admin.OrderSampleItem.edit-add', [
            'adminModel' => $adminEntity,
            'orderItem' => $orderSampleItem,
            'collections' => []
        ]);
    }

    public function update(UpdateOrderItemRequest $request, OrderSampleItem $orderSampleItem)
    {
        if ($request->has('auto_remind')) {
            $orderSampleItem->auto_remind = $request->get('auto_remind');
        }

        if ($request->has('shipping_title')) {
            $orderSampleItem->fill($request->only(array_keys($request->getAddressFields('shipping'))));
        }

        if ($request->has('billing_title')) {
            $orderSampleItem->fill($request->only(array_keys($request->getAddressFields('billing'))));
        }

//        if ($request->has('is_important')) {
//            $orderSampleItem->is_important = $request->is_important;
//        }

        $orderSampleItem->fill($request->all());
        $orderSampleItem->save();

        return response('', Response::HTTP_NO_CONTENT);
    }

    public function show(OrderSampleItem $orderSampleItem)
    {
        return $orderSampleItem;
    }

}
