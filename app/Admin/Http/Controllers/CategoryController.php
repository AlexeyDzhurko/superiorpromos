<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Transformers\CategoryTransformer;
use App\Models\Category;
use App\Repository\CategoryRepository;
use App\Admin\Entities\CategoryEntity;
use Symfony\Component\HttpFoundation\Request;
use App\Admin\Http\Requests\CategoryRequest;

class CategoryController extends BaseAdminController
{
    /**
     * @param Request $request
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $page = 1)
    {
        return view('admin.Category.index', [
            'nestedCategories' => fractal()->collection(Category::all()->toHierarchy(), new CategoryTransformer()),
//            'adminModel' => $adminEntity,
        ]);
    }

    /**
     * Return page with create item form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $adminEntity = new CategoryEntity();

        return view('admin.Category.edit-add', [
            'adminModel' => $adminEntity,
            'dataTypeContent' => [],
            'category' => null,
            'collections' => CategoryRepository::getAllCategoriesWithChild(false)
        ]);
    }

    /**
     * Return page with edit item form
     *
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Category $category)
    {
        $adminEntity = new CategoryEntity();

        return view('admin.Category.edit-add', [
            'adminModel' => $adminEntity,
            'dataTypeContent' => $category,
            'parent' => $category->parent?$category->parent->id: null,
            'collections' => CategoryRepository::getAllCategoriesWithChild(false)
        ]);
    }

    /**
     * Search color by name
     * @return array
     */
    public function search()
    {
        $data = CategoryRepository::getAllCategoriesWithChild(false);

        return $data;
    }

    /**
     * Save new Category to database
     *
     * @param CategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CategoryRequest $request)
    {
        $adminEntity = new CategoryEntity();

        CategoryRepository::insertCategory($request->all());

        return redirect(route('category.index'))
            ->with([
                'message' => 'Successfully Added New ' . $adminEntity->getDisplayNameSingular(),
                'alert-type' => 'success'
            ]);
    }

    /**
     * Update existed Category
     *
     * @param CategoryRequest $request
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $adminEntity = new CategoryEntity();

        CategoryRepository::updateCategory($category, $request->all());

        return redirect(route('category.index'))
            ->with([
                'message' => 'Successfully Updated ' . $adminEntity->getDisplayNameSingular(),
                'alert-type' => 'success'
            ]);
    }

}
