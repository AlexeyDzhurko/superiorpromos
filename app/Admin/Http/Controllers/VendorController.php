<?php

namespace App\Admin\Http\Controllers;

use App\Models\Vendor;
use App\Repository\VendorRepository;
use App\Admin\Entities\VendorEntity;
use Symfony\Component\HttpFoundation\Request;
use Blade;

class VendorController extends BaseAdminController
{
    /**
     * @param Request $request
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $page = 1)
    {
        $adminEntity = new VendorEntity();
        $params = $this->formedParams($request->all());

        $dataTypeContent = VendorRepository::getVendorsByParameters($params);

        return view('admin.Vendor.index', [
            'dataTypeContent' => $dataTypeContent,
            'adminModel' => $adminEntity,
            'parameters' => $params
        ]);
    }

    public function edit(Vendor $vendor)
    {
        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');

        return view('admin.Vendor.edit-add', [
            'vendor' => $vendor,
        ]);
    }
}
