<?php


namespace App\Admin\Http\Controllers;


use App\Models\Discount;
use Blade;

class DiscountController extends BaseAdminController
{
    public function index()
    {
        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');

        return view('admin.Discount.index', [
            'stages' => Discount::all(),
        ]);
    }

    public function create()
    {
        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');

        return view('admin.Discount.edit-add', [

        ]);
    }

    public function edit(Discount $discount)
    {
        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');

        return view('admin.Discount.edit-add', [
            'discount' => $discount,
        ]);
    }

}
