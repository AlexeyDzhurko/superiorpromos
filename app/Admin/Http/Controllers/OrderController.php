<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 12.04.17
 * Time: 19:34
 */

namespace App\Admin\Http\Controllers;

use App\Models\Order;
use App\Models\OrderItem;
use Blade;

class OrderController
{

    public function index()
    {
        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');

        return view('admin.Order.index');
    }

    public function edit(Order $order)
    {
        Blade::setContentTags('<%', '%>');
        Blade::setEscapedContentTags('<%%', '%%>');

//        $order->total_price = $this->getTotalPrice($order);
        $canRemove = true;

        foreach ($order->orderItems as $orderItem) {
            /** @var OrderItem $orderItem */
            $status = $orderItem->stage ? $orderItem->stage->name : '';

            if ($status
                && $status !== ''
                && $status != 'New Order Received - Processing'
                && $status != 'New Sample Request'
            ) {
                $canRemove = false;
                break;
            }
        }

        return view('admin.Order.edit-add', [
            'order' => $order,
            'canRemove' => $canRemove,
            'collections' => []
        ]);
    }

    /**
     * @param $order
     * @return string
     */
    private function getTotalPrice($order)
    {
//        $order = Order::find($order['id']);
//        $orderItems = $order->orderItems;
//        $sum = 0;
//
//        foreach ($orderItems as $orderItem) {
//            $sum += ($orderItem->price);
//        }

        return number_format($order->total_price, 2);
    }
}
