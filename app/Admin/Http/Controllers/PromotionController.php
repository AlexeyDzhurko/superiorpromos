<?php


namespace App\Admin\Http\Controllers;


use App\Admin\Http\Requests\PromotionRequest;
use App\Models\Promotion;

class PromotionController extends BaseAdminController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {
        $promotion = Promotion::first();

        return view('admin.Promotion.index', [
            'promotion' => $promotion,
        ]);
    }

    /**
     * @param PromotionRequest $request
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function store(PromotionRequest  $request)
    {
        $promotion = Promotion::find($request->get('id')) ?? new Promotion();
        $promotion->data = $request->get('data');

        if (!$promotion->save()) {
            return redirect(route('promotion.index'))
                ->with([
                    'message' => 'Something went wrong, please try again',
                    'alert-type' => 'error'
                ]);
        }

        return redirect(route('promotion.index'))
            ->with([
                'message' => 'Successfully updated current promotion',
                'alert-type' => 'success'
            ]);
    }
}
