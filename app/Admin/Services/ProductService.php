<?php

namespace App\Admin\Services;

use App\Admin\Contracts\CreateProduct;
use App\Models\Product;
use App\Models\SeoProduct;
use App\Repository\ColorRepository;
use App\Repository\ProductPriceRepository;
use App\Repository\ProductRepository;
use App\Repository\ColorGroupRepository;
use Image;

class ProductService implements CreateProduct
{
    /**
     * @param array $products
     */
    public function addProduct(array $products)
    {
        foreach ($products['products'] as $productItem) {
            if (!$this->checkExistProduct($productItem['ProductID'])) {
                $child =  isset($products['childCategories']) ?  $products['childCategories'] : [];
                $product = $this->newProduct(
                    $productItem, $products['sale']??false, $products['urlPrefix']??$this->formUrl($productItem['PrName']),
                    $products['mainCategory'], $child
                );
                $this->formedPriceData($products['priceGridPercent'], $product, $productItem);
                $colorGroupId = $this->formedColorsData($productItem, $product);
                $this->formedImprintData($products['imprintPriceGrid'], $product, $productItem, $colorGroupId);
                $this->formedSeo($productItem, $product);

                $productManager = new ProductManager();
                $extraImage = $productManager->createProductExtraImage($product, $productItem['PicLink']);
                $product->image_src = ProductManager::EXTRA_IMAGES_PATH . $extraImage->id . '/270_270.jpg';
                $product->save();
            }
        }
    }

    /**
     * @param array $data
     * @param string $sale
     * @param string $urlPrefix
     * @param integer $mainCategory
     * @param array $childCategories
     * @return Product
     */
    private function newProduct($data, $sale, $urlPrefix, $mainCategory, $childCategories)
    {
        $categories = [];
        foreach ($childCategories as $category) {
            $categories[$category] = ['type' => 'sub'];
        }
        $categories[$mainCategory] = ['type' => 'main'];

        return ProductRepository::add( [
                'sage_id' => $data['SPC'],
                'name' => $data['PrName'],
                'description' => $data['Description'],
                'dimensions' => $data['Dimensions'],
                'imprint_area' => $data['ImprintArea'],
                'production_time_from' => empty($data['TimeFrom']) ? 0 : $data['TimeFrom'],
                'production_time_to' => empty($data['TimeTo']) ? 0 : $data['TimeTo'],
                'box_weight' => $data['WeightPerCarton'],
                'on_sale' => $sale === 'false' ? 0 : 1,
                'image_src' => $data['PicLink'],
                'url' => $data['Url'] . uniqid(),
                'url_prefix' => $urlPrefix ?? null,
                'vendor_id' => $data['supplier_id'],
                'active' => 0,
                'pricing_information' => $data['PriceIncludes'],
        ], $categories, $data);
    }

    /**
     * @param array $priceGridPercent
     * @param Product $product
     * @param array $data
     * @return array
     */
    private function formedPriceData($priceGridPercent, Product $product, $data)
    {
        $value = [];
        /**
         * Multiple import
         */
        for ($i = 1; $i < 7; $i++) {
            if ($data['Qty' . $i]) {
                array_push($value, [
                    'product_id' => $product->id,
                    'quantity' => $data['Qty' . $i],
                    'item_price' => $data['Prc' . $i],
                    'sale_item_price' => $data['Net' . $i],
                ]);
            }
        }

        return ProductPriceRepository::batch($value, $product);
    }

    /**
     * @param float $price
     * @param int $percent
     * @return int
     */
    private function countSalePrice($price, $percent)
    {
        return round(
            (float)$price * (1 + (int)$percent/100),
            2
        );
    }

    /**
     * @param array $priceGrid
     * @param Product $product
     * @param array $productItem
     * @param int $colorGroupId
     */
    private function formedImprintData($priceGrid, $product, $productItem, $colorGroupId)
    {
        $imprint= [
            'name' => $productItem['ImprintArea'],
            'color_group_id' => $colorGroupId
        ];
        $imprint = $product->imprints()->create($imprint);

        $value = [];
        foreach ($priceGrid as $price) {
            for ($i = 0; $i < 6; $i++) {
                if ($price['quantity'][$i]) {
                    array_push($value, [
                        'quantity' => $price['quantity'][$i],
                        'setup_price' => $price['locationSetupFee'][$i],
                        'item_price' => $price['additionalLocationRunningFee'][$i],
                        'color_setup_price' => $price['additionalColorSetupFee'][$i],
                        'color_item_price' => $price['additionalColorRunningFee'][$i],
                        'imprint_id' => $imprint->id
                    ]);
                }
            }
        }

        $imprint->imprintPrices()->createMany($value);

//        /**
//         * Multiple import
//         */
//        if (empty($value)) {
//            $value  = $this->formedMultipleImprint($productItem, ImprintColorGroup::first()->id);
//        }
//
//        return ImprintLocationRepository::batch($value, $product);
    }

    /**
     * @param array $productItem
     * @param string $imprintColorGroup
     * @return array
     */
    private function formedMultipleImprint($productItem, $imprintColorGroup)
    {
//        $value = [];
//        for ($n = 1; $n < 7; $n++) {
//            if ((int)$productItem['Qty'.$n]) {
//                array_push($value, [
//                    'quantity' => $productItem['Qty'.$n],
//                    'location_setup_fee' => $productItem['SetupChg'],
//                    'additional_location_running_fee' => 0,
//                    'additional_color_setup_fee' => $productItem['SetupChg'],
//                    'additional_color_running_fee' => 0,
//                    'imprint_color_group_id' => $imprintColorGroup,
//                    'type' => ImprintLocation::IMPRINT_FIRST_LOCATION
//                ]);
//            }
//        }
//        for ($n = 1; $n < 7; $n++) {
//            if ((int)$productItem['Qty'.$n]) {
//                array_push($value, [
//                    'quantity' => $productItem['Qty'.$n],
//                    'location_setup_fee' => $productItem['SetupChg'],
//                    'additional_location_running_fee' => $productItem['AddClrRunChg'.$n],
//                    'additional_color_setup_fee' => $productItem['SetupChg'],
//                    'additional_color_running_fee' => $productItem['AddClrRunChg'.$n],
//                    'imprint_color_group_id' => $imprintColorGroup,
//                    'type' => ImprintLocation::IMPRINT_SECOND_LOCATION
//                ]);
//            }
//        }
//
//        return $value;
    }

    /**
     * @param array $data
     * @param Product $product
     */
    private function formedSeo($data, Product $product)
    {
        SeoProduct::insert([
            'keywords' => $data['Keywords'],
            'product_id' => $product->id
        ]);
    }

    /**
     * @param array $data
     * @param Product $product
     * @return integer
     */
    private function formedColorsData($data, Product $product)
    {
        $colors = explode(', ', $data['Colors']);
        $colorIds = ColorRepository::getColorIdsByName($colors);

        return ColorGroupRepository::add($colorIds, $data['PrName'], $product->id);
    }

    /**
     * @param string $sageId
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    private function checkExistProduct($sageId)
    {
        return ProductRepository::getProductBySageId($sageId);

    }

    /**
     * @param string $name
     * @return string
     */
    private function formUrl($name)
    {
        return strtolower(str_replace(' ', '-', $name));
    }
}