<?php


namespace App\Admin\Services;


use App\Models\ProductColor;
use App\Models\ProductColorPrice;

class ProductColorGroupManager
{
    public function updateProductColorPriceGrid(ProductColor $productColor, array $productColorPricesData)
    {
        foreach ($productColorPricesData as $productColorPriceData) {
            if (isset($productColorPriceData['id']) and $productColorPriceData['id'] > 0) {
                $productColorPrice = ProductColorPrice::find($productColorPriceData['id']);
            } else {
                $productColorPrice = new ProductColorPrice();
                $productColorPrice->productColor()->associate($productColor);
            }
            $productColorPrice->quantity = $productColorPriceData['quantity'];
            $productColorPrice->setup_price = $productColorPriceData['setup_price'];
            $productColorPrice->item_price = $productColorPriceData['item_price'];

            $productColorPrice->save();
        }
    }

}
