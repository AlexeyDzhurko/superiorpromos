<?php

namespace App\Admin\Services;

use App\Admin\Contracts\AdminEntity;
use App\Admin\Contracts\AdminResolver;
use App\Admin\Exceptions\AdminEntityNotFoundException;

class AdminDefaultResolver implements AdminResolver
{
    protected $entities = [];

    public function __construct(array $entities)
    {
        $this->entities = $entities;
    }

    /**
     * return Service that used for CRUD implementation
     *
     * @param string $baseRouteName
     * @return AdminEntity
     * @throws AdminEntityNotFoundException
     */
    public function get(string $baseRouteName): AdminEntity
    {
        foreach ($this->entities as $entity) { /** @var AdminEntity $entity */
            if ($entity->getBaseRouteName() == $baseRouteName) {
                return $entity;
            }
        }

        throw new AdminEntityNotFoundException();
    }

    /**
     * return all URLs for Admin CRUDs
     *
     * @return array
     */
    public function getAllSlugs(): array
    {
        $slugs = [];
        foreach ($this->entities as $entity) { /** @var AdminEntity $entity*/
            $slugs[] = $entity->getBaseRouteName();
        }
        return $slugs;
    }
}
