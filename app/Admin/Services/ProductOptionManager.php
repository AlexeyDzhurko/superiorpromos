<?php


namespace App\Admin\Services;


use App\Models\ProductOption;
use App\Models\ProductOptionPrice;
use App\Models\ProductSubOption;
use App\Models\ProductSubOptionPrice;

class ProductOptionManager
{
    /**
     * Store new price grid for Product Option
     *
     * @param ProductOption $productOption
     * @param array $productOptionPricesData
     */
    public function updateProductOptionPriceGrid(ProductOption $productOption, array $productOptionPricesData)
    {
        foreach ($productOptionPricesData as $productOptionPriceData) {
            if (isset($productOptionPriceData['id'])) {
                $productOptionPrice = ProductOptionPrice::find($productOptionPriceData['id']);
            } else {
                $productOptionPrice = new ProductOptionPrice();
                $productOptionPrice->productOption()->associate($productOption);
            }
            $productOptionPrice->quantity = $productOptionPriceData['quantity'];
            $productOptionPrice->setup_price = $productOptionPriceData['setup_price'];
            $productOptionPrice->item_price = $productOptionPriceData['item_price'];

            $productOptionPrice->save();
        }
    }

    /**
     * Store new price grid for Product Sub Option
     *
     * @param ProductSubOption $productSubOption
     * @param array $productSubOptionPricesData
     */
    public function updateProductSubOptionPriceGrid(ProductSubOption $productSubOption, array $productSubOptionPricesData)
    {
        foreach ($productSubOptionPricesData as $productSubOptionPriceData) {
            if (isset($productSubOptionPriceData['id'])) {
                $productSubOptionPrice = ProductSubOptionPrice::find($productSubOptionPriceData['id']);
            } else {
                $productSubOptionPrice = new ProductSubOptionPrice();
                $productSubOptionPrice->productSubOption()->associate($productSubOption);
            }
            $productSubOptionPrice->quantity = $productSubOptionPriceData['quantity'];
            $productSubOptionPrice->setup_price = $productSubOptionPriceData['setup_price'];
            $productSubOptionPrice->item_price = $productSubOptionPriceData['item_price'];

            $productSubOptionPrice->save();
        }
    }

}
