<?php


namespace App\Admin\Services;


use App\Models\Survey;
use App\Models\SurveyAnswer;
use App\Models\SurveyQuestion;
use Illuminate\Http\Response;

class SurveyManager
{
    public function updateQuestions(Survey $survey, array $questionsData)
    {
        foreach ($questionsData as $questionData) {
            if (isset($questionData['id'])) {
                $question = SurveyQuestion::find($questionData['id']);
            } else {
                $question = new SurveyQuestion();
                $question->survey()->associate($survey);
            }
            $question->name = $questionData['name'];
            $question->type = $questionData['type'];

            $question->save();
            $this->updateAnswers($question, $questionData['survey_answers']);
        }
    }

    public function updateAnswers(SurveyQuestion $surveyQuestion, array $answersData)
    {
        foreach ($answersData as $answerData) {
            if (isset($answerData['id'])) {
                $answer = SurveyAnswer::find($answerData['id']);
            } else {
                $answer = new SurveyAnswer();
                $answer->surveyQuestion()->associate($surveyQuestion);
            }
            $answer->name = $answerData['name'];

            $answer->save();
        }
    }

}
