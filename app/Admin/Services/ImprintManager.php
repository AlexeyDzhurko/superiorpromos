<?php


namespace App\Admin\Services;


use App\Models\Imprint;
use App\Models\ImprintPrice;

class ImprintManager
{
    public function updatePriceGrid(Imprint $imprint, array $pricesData)
    {
        foreach ($pricesData as $priceData) {
            if (isset($priceData['id'])) {
                $imprintPrice = ImprintPrice::find($priceData['id']);
            } else {
                $imprintPrice = new ImprintPrice();
                $imprintPrice->imprint()->associate($imprint);
            }
            $imprintPrice->quantity = $priceData['quantity'];
            $imprintPrice->setup_price = $priceData['setup_price'];
            $imprintPrice->item_price = $priceData['item_price'];
            $imprintPrice->color_setup_price = $priceData['color_setup_price'];
            $imprintPrice->color_item_price = $priceData['color_item_price'];

            $imprintPrice->save();
        }

    }

}
