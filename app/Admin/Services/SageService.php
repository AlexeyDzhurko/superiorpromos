<?php

namespace App\Admin\Services;

use App\Admin\Contracts\Sage;
use App\Models\Vendor;
use GuzzleHttp\Client;
use View;

class SageService implements Sage
{
    /**
     * @var string
     */
    protected $acctId;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $loginId;

    /**
     * @var string
     */
    protected $sageUrl;

    /**
     * SageService constructor.
     */
    public function __construct()
    {
        $this->acctId = env('SAGE_ACC_ID');
        $this->password = env('SAGE_PASSWORD');
        $this->loginId = env('SAGE_LOGIN_ID');
        $this->sageUrl = env('SAGE_URL');
    }

    /**
     * Get list of categories from Sage
     * @return array
     */
    public function getCategoriesList():array
    {
            $result = $this->sendRequest('admin.sage.categories');
            $xmlObject = new \SimpleXMLElement($result->getBody()->getContents());

            if (isset($xmlObject->ErrMsg)) {
                throw new \Exception("Error get categories from Sage: " . $xmlObject->ErrMsg);
            }

            return $this->formedCategoryData($xmlObject->CategoryList);
    }

    /**
     * Get list of suppliers from Sage
     * @return array
     */
    public function getSuppliersList():array
    {
        $result = $this->sendRequest('admin.sage.suppliers');
        $xmlObject = new \SimpleXMLElement($result->getBody()->getContents());

        return $this->formedSupplierData($xmlObject->SupplierList);
    }

    /**
     * Get supplier's info from Sage
     * @param string $supplier
     * @return array
     */
    public function getSupplierInfo(string $supplier):array
    {
        $result = $this->sendRequest('admin.sage.supplier_info', ['supplier' => $supplier]);
        $xmlObject = new \SimpleXMLElement($result->getBody()->getContents());

        return $this->formedSupplierInfo($xmlObject->SupplierInfo);
    }

    /**
     * Get supplier's product list
     * @param \App\Admin\Http\Requests\SageSearchBySupplierRequest $request
     * @return array
     */
    public function getSuppliersProductList($request):array
    {
        $vendor = Vendor::find($request->supplier_id);

        if(is_null($vendor)) {
            return [null];
        }

        $request_data = [
            'supplier' => $vendor->supplier_id
        ];

        $this->formedPaginationForRequest($request, $request_data);

        $result = $this->sendRequest('admin.sage.supplier_products', $request_data);
        $xmlObject = new \SimpleXMLElement($result->getBody()->getContents());

        if($xmlObject->SupplierProductDataDumpResults->TotalFound > 0) {
            return [
                'items' => $this->formedSupplierProductData($xmlObject->SupplierProductDataDumpResults->Products, $vendor->supplier_id),
                'total' => (int)$xmlObject->SupplierProductDataDumpResults->TotalFound,
                'current_page' => $request_data['current_page'],
                'items_per_page' => $request_data['items_per_page'],
                'pages' => ceil($xmlObject->SupplierProductDataDumpResults->TotalFound / $request_data['items_per_page']),
            ];
        } else {
            return [null];
        }
    }

    /**
     * Get product by search params from sage
     * @param \App\Admin\Http\Requests\SageQuickSearchRequest $request
     * @return array
     */
    public function ProductsSearch($request):array
    {
        $request_data = [
            'keywords' => $request->keywords
        ];

        $this->formedPaginationForRequest($request, $request_data);

        $result = $this->sendRequest('admin.sage.products_search', $request_data);
        $xmlObject = new \SimpleXMLElement($result->getBody()->getContents());

        if($xmlObject->SearchResults->Success == 1 and $xmlObject->SearchResults->TotalFound > 0) {
            return [
                'items' => $this->formedProductsSearchResultData($xmlObject->SearchResults->Items),
                'total' => (int)$xmlObject->SearchResults->TotalFound,
                'current_page' => $request_data['current_page'],
                'items_per_page' => $request_data['items_per_page'],
                'pages' => ceil($xmlObject->SearchResults->TotalFound / $request_data['items_per_page']),
            ];
        } else {
            return [null];
        }
    }

    /**
     * Get product detail by id from sage
     * @param string $product_id
     * @return array
     */
    public function getProductDetail(string $product_id):array
    {
        $result = $this->sendRequest('admin.sage.product_detail', [
            'product_id' => $product_id
        ]);
        $xmlObject = new \SimpleXMLElement($result->getBody()->getContents());

        $productDetail = $this->formedSupplierProductData($xmlObject);

        return $productDetail[0];
    }

    /**
     * @param SimpleXMLElement $xmlObject
     * @return array
     */
    private function formedCategoryData($xmlObject)
    {
        $result = [];
        $arrayKey = 0;
        $count = 0;
        $letter = 'A';


        foreach ($xmlObject->Category as $item) {
            $item = (array)$item;
            /** Change letter and counter item */
            if ($letter !== $item['Name'][0]) {
                $letter = $item['Name'][0];
                $count = 0;
            }

            if ($count % 5 == 0) {
                $arrayKey++;
            }
            $result[$letter][$arrayKey][] = [
                'id' => $item['Num'],
                'name' => $item['Name']
            ];

            $count++;
        }

        return $result;
    }

    /**
     * @param SimpleXMLElement $xmlObject
     * @return array
     */
    private function formedSupplierData($xmlObject)
    {
        $result = [];

        foreach ($xmlObject->Supplier as $item) {
            $result[] = (array)$item;
        }

        return $result;
    }

    /**
     * @param SimpleXMLElement $xmlObject
     * @param string $vendor
     * @return array
     */
    private function formedSupplierProductData($xmlObject, $vendor = false)
    {
        $results = [];

        foreach ($xmlObject->ProductDetail as $item) {
            $itemArr = (array)$item;
            $data = [];
            foreach ($itemArr as $key => $value) {
                $data[$key] = (string)$value;
            }
            $prices = $this->formedPrices($itemArr);
            preg_match_all('!\d+!', $data['ProdTime'], $matches);

            $data['MaxPrice'] = !empty($prices) ? max($prices): 0;
            $data['MinPrice'] = !empty($prices) ? min($prices): 0;
            $data['Url'] = strtolower(str_replace([' ', '.'], '-', $data['PrName']));
            $data['TimeFrom'] = $matches[0][0] ?? null;
            $data['TimeTo'] = $matches[0][1] ?? null;
            $data['supplier_id'] = $vendor;

            $results[] = $data;
        }

        return $results;
    }

    /**
     * @param SimpleXMLElement $xmlObject
     * @return array
     */
    private function formedSupplierInfo($xmlObject)
    {
        return [
            'name' => (string)$xmlObject->CoName,
            'phone' => (string)$xmlObject->Tel,
            'fax' => (string)$xmlObject->Fax,
            'email' => (string)$xmlObject->Email,
            'state' => (string)$xmlObject->MState,
            'city' => (string)$xmlObject->MCity,
            'site' => (string)$xmlObject->Web,
            'country' => (string)$xmlObject->MCountry,
            'zip_code' => (string)$xmlObject->MZip,
            'address_1' => (string)$xmlObject->MAddr,
            'supplier_id' => (string)$xmlObject->SuppID,
        ];

    }

    /**
     * Formed prices array
     * @param array $itemArr
     * @return array
     */
    private function formedPrices($itemArr)
    {
        $result = [];

        for ( $i = 1; $i < 7; $i++ ) {
            $key = 'Prc';
            $key .= $i;
            if ((float)$itemArr[$key]) {
                $result[] = (float)$itemArr[$key];
            }
        }

        return $result;
    }

    /**
     * @param SimpleXMLElement $xmlObject
     * @return array
     */
    private function formedProductsSearchResultData($xmlObject) :array
    {
        $data = [];

        foreach ($xmlObject->Item as $item) {
            $data[] = [
                'ProductID' => (string)$item->ProductID,
                'SPC'       => (string)$item->SPC,
                'PrName'    => (string)$item->PrName,
                'Prc'       => (string)$item->Prc,
                'PicLink'   => (string)$item->ThumbPicLink,
                'Themes'   => (string)$item->Themes,
                'Colors'   => (string)$item->Colors,
                'Description'   => (string)$item->Description,
            ];
        }

        return $data;
    }

    /**
     * @param string $template
     * @param array $parameters
     * @return \Psr\Http\Message\ResponseInterface
     */
    private function sendRequest($template, $parameters = [])
    {
        $client = new Client();
        $data = array_merge([
            'acctId' => $this->acctId,
            'password' => $this->password,
            'loginId' => $this->loginId,
        ], $parameters);

        $content = View::make($template, $data);

        return $client->post($this->sageUrl, [
            'body' => $content,
            'headers' => [
                'Content-type' => 'text/xml',
                'Accept-Charset' => 'UTF-8',
            ],
        ]);
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param array $request_data
     */
    private function formedPaginationForRequest($request, &$request_data)
    {
        $items_per_page = (is_null($request->items_per_page) or $request->items_per_page > 100) ? 20 : $request->items_per_page;
        $current_page = is_null($request->current_page) ? 1 : $request->current_page;

        $request_data['current_page'] = $current_page;
        $request_data['items_per_page'] = $items_per_page;
    }

}