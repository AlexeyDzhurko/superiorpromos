<?php


namespace App\Admin\Services;


use App\Models\Size;
use App\Models\SizeGroup;

class SizeGroupManager
{
    public function updateSizes(SizeGroup $sizeGroup, array $sizesData)
    {
        foreach ($sizesData as $sizeData) {
            if (isset($sizeData['id'])) {
                $size = Size::find($sizeData['id']);
            } else {
                $size = new Size();
                $size->sizeGroup()->associate($sizeGroup);
            }
            $size->name = $sizeData['name'];
            $size->save();
        }
    }
}
