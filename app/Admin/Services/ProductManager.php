<?php


namespace App\Admin\Services;


use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductExtraImage;
use App\Models\ProductPrice;
use Embed\Embed;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Image as InterventionImage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;

class ProductManager
{
    const IMAGE_THUMBNAILS_SIZES = [
        64,
        77,
        90,
        110,
        270,
    ];

    const IMAGES_PATH = '/uploads/products/';
    const EXTRA_IMAGES_PATH = 'extra/';

//    const EXTRA_IMAGES_PATH = '/uploads/products/extra/';
    const EXTRA_COLOR_IMAGES_PATH = '/uploads/products/colors/';

    /**
     * Store new price grid for Product
     *
     * @param Product $product
     * @param array $pricesData
     */
    public function updateProductPriceGrid(Product $product, array $pricesData)
    {
        foreach ($pricesData as $priceData) {
            if (isset($priceData['id'])) {
                $productPrice = ProductPrice::find($priceData['id']);
            } else {
                $productPrice = new ProductPrice();
                $productPrice->product()->associate($product);
            }
            $productPrice->quantity = $priceData['quantity'];
            $productPrice->setup_price = $priceData['setup_price'];
            $productPrice->item_price = $priceData['item_price'];
            $productPrice->sale_item_price = $priceData['sale_item_price'];

            $productPrice->save();
        }
    }

    public function updateVideoUrl(Product $product, $url)
    {
        try {
            $videoInfo = Embed::create($url);
            preg_match('/src="(.+?)"/', $videoInfo->getCode(), $videoLinkMatch);
        } catch (\Exception $e) {
            return;
        }

        if (isset($videoLinkMatch[1])) {
            $product->video_url = $videoLinkMatch[1];
            $product->save();
        }
    }

    public function createProductExtraImage(Product $product, $file)
    {
        return DB::transaction(function () use ($product, $file) {
            $productExtraImage = new ProductExtraImage();
            $productExtraImage->product()->associate($product);
            $productExtraImage->save();

            $dir = sprintf('%s%s%s/%s%s',
                public_path(),
                self::IMAGES_PATH,
                $product->id,
                self::EXTRA_IMAGES_PATH,
                $productExtraImage->id
            );

            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            Image::make($file)->save($dir . '/original.jpg');

            foreach (self::IMAGE_THUMBNAILS_SIZES as $size) {
                $this->resizeImage($file, $size, $size, $dir);
            }

            return $productExtraImage;
        });
    }

    public function createProductColorImage(ProductColor $productColor, $file)
    {
        return DB::transaction(function () use ($productColor, $file) {
            $dir = public_path(). self::EXTRA_COLOR_IMAGES_PATH . $productColor->id;
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            Image::make($file)->save($dir . '/original.jpg');

            foreach (self::IMAGE_THUMBNAILS_SIZES as $size) {
                $this->resizeImage($file, $size, $size, $dir);
            }
            $productColor->image_src = self::EXTRA_COLOR_IMAGES_PATH . $productColor->id . '/original.jpg';
            $productColor->save();
        });
    }

    protected function resizeImage($file, $width, $height, $dir)
    {
        $name = implode('_', [$width, $height]);

        $backgroundImage = $this->createBackgroundImage($width, $height, '#fefefe');
        $resizedImage = Image::make($file)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $backgroundImage->insert($resizedImage, 'center')->save($dir . '/' . $name . '.jpg');
    }

    /**
     * @param int $width
     * @param int $height
     * @param null $backgroundColor
     *
     * @return InterventionImage
     */
    protected function createBackgroundImage(int $width, int $height, $backgroundColor = null): InterventionImage
    {
        return Image::canvas($width, $height, $backgroundColor);
    }

    /**
     * Check existing extra image in DB
     *
     * @author Ruslan Ivanov
     * @param $image_id string|int
     * @return bool
     */
    public function checkExistExtraImage($image_id)
    {
        $productExtraImage = ProductExtraImage::find($image_id);

        if(!is_null($productExtraImage)) {
            return true;
        }

        return false;
    }

}
