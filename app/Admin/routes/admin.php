<?php

Route::get('login', ['uses' => 'Auth\AuthController@login', 'as' => 'admin.login'])->middleware('web');
Route::post('login', 'Auth\AuthController@postLogin')->middleware('web');

Route::group(['middleware' => ['web', 'admin.user']], function () {

    // Main Admin and Logout Route
    Route::get('/', ['uses' => 'HomeController@index', 'as' => 'admin.dashboard']);
    Route::post('upload', ['uses' => 'HomeController@upload', 'as' => 'admin.upload']);
    Route::get('logout', ['uses' => 'HomeController@logout', 'as' => 'admin.logout']);
    Route::get('profile', 'HomeController@profile')->name('admin.profile');


    /**
     * Product admin's page
     */
    
    Route::get('product/{product}/edit', 'ProductController@edit');
    Route::get('product/create', 'ProductController@create');

    /**
     * Vendor admin's page
     */
    /*Route::get('vendor', 'VendorController@index')->name('vendor.index');
    Route::get('vendor/{vendor}/edit', 'VendorController@edit');*/

    /**
     * Size admin's page
     */
    //Route::get('size', 'SizeController@index')->name('size.index');

    /**
     * Froogle
     */
    Route::get('froogle', 'FroogleController@index')->name('froogle.index')->middleware('admin.access');;
    Route::post('froogle/generate', 'FroogleController@generate')->middleware('admin.access');;
    Route::get('froogle/xml', 'FroogleController@xml')->middleware('admin.access');;
    Route::get('froogle/csv', 'FroogleController@csv')->middleware('admin.access');;

    /**
     * Discount
     */
    Route::group(['middleware' => 'admin.access'], function() {
        Route::resource('discount', 'DiscountController', ['only' => ['create', 'index', 'edit']]);
    });


    /**
     * Size-group admin's page
     */
    Route::get('size-group/{sizeGroup}/edit', 'SizeGroupController@edit')->name('size_group.edit');
    Route::match(['put', 'patch'], 'size-group/{sizeGroup}', 'SizeGroupController@update')->name('size_group.update');
    Route::get('size-group/create', 'SizeGroupController@create')->name('size_group.create');
    Route::post('size-group', 'SizeGroupController@store')->name('size_group.store');

    /**
     * Orders admin's page
     */
    Route::get('order', 'OrderController@index')->name('order.index')->middleware('admin.access');
    Route::delete('api/order/{order}/', 'Api\OrderController@delete')->name('order.delete')->middleware('admin.access');

    Route::get('order_item/{orderItem}/edit', 'OrderItemController@edit')->name('order_item.edit')->middleware('admin.access');
    Route::get('order/{order}/edit', 'OrderController@edit')->name('order.edit')->middleware('admin.access');
    Route::any('api/order-item/{id}', 'Api\OrderItemController@delete')->name('order_item.delete');

    Route::get('stage', 'StageController@index')->middleware('admin.access');
    
    /**
     * Orders a sample admin's page
     */
    Route::get('order_sample', 'OrderSampleController@index')->name('order_sample.index')->middleware('admin.access');
    Route::get('order_sample/{orderSample}/edit', 'OrderSampleController@edit')->name('order_sample.edit')->middleware('admin.access');
    Route::get('order_sample_item/{orderSampleItem}/edit', 'OrderSampleItemController@edit')->name('order_sample_item.edit')->middleware('admin.access');

    Route::get('api/order_sample', 'Api\OrderSampleController@index');
    Route::get('api/order_sample/_search', 'Api\OrderSampleController@search')->name('order_sample.search');
    /**
     * Orders a sample items
     */
    Route::get('api/order_sample_item/{orderSampleItem}', 'OrderSampleItemController@show');
    Route::patch('api/order_sample_item/{orderSampleItem}', 'OrderSampleItemController@update');
    Route::get('api/order_sample_item/{orderSampleItem}/stage', 'OrderStageController@getOrderSampleStage');
    Route::post('api/order_sample_item/{orderSampleItem}/stage', 'OrderStageController@saveOrderSampleStage');
    Route::get('api/order_sample_item/{orderSampleItem}/note', 'Api\OrderSampleItemNoteController@index');
    Route::post('api/order_sample_item/{orderSampleItem}/note', 'Api\OrderSampleItemNoteController@store');
    Route::get('api/order_sample_item/{orderSampleItem}/tracking_information', 'Api\OrderSampleItemTrackingInformationController@index');
    Route::post('api/order_sample_item/{orderSampleItem}/tracking_information', 'Api\OrderSampleItemTrackingInformationController@store');

    /**
     * Customer
     */
    Route::get('api/customer/_search', 'Api\CustomerController@elasticIndex')->name('admin.api.customer.index.search');
    Route::delete('api/customer/{user}', 'Api\CustomerController@destroy')->name('admin.api.customer.destroy');
    Route::post('api/customer', 'Api\CustomerController@store')->name('admin.api.customer.store');
    Route::put('api/customer/{user}', 'Api\CustomerController@update')->name('admin.api.customer.update');
    Route::get('api/customer/{user}/login', 'CustomerController@loginByCustomer')->name('customer.login');

    /*
     * Address
     */
    Route::resource('api/address', 'Api\AddressController');

    /**
     * FAQ
     */
    Route::resource('api/faqs', 'Api\FAQController', ['only' => ['index', 'store', 'update', 'destroy']]);

    /**
     * Colors
     */
    Route::resource('api/colors', 'Api\ColorController', ['only' => ['store', 'update', 'destroy']]);
    Route::get('api/colors', 'Api\ColorController@getColorsList');
    Route::get('api/all-colors', 'Api\ColorController@getAllColors');
    Route::get('api/search-colors', 'Api\ColorController@searchColor');

    /**
     * Color Groups
     */
    Route::resource('api/color-groups', 'Api\ColorGroupController', ['only' => ['index', 'store', 'update', 'destroy']]);
    Route::get('api/all-color-groups', 'Api\ColorGroupController@getAllColorGroups');

    /**
     * Product
     */
    Route::get('api/product/_search', 'Api\ProductController@elasticIndex')->name('admin.api.product.index.search');
    Route::get('api/product/{product}', 'Api\ProductController@show');
    Route::get('api/product', 'Api\ProductController@index')->name('admin.api.product.index');
    Route::match(['put', 'patch'], 'api/product/{product?}', 'Api\ProductController@update');
    Route::delete('api/product/{product}', 'Api\ProductController@destroy');
    Route::put('api/product/{product}/status', 'Api\ProductController@updateStatus')->name('admin.api.product.status');

    Route::patch('api/product/{product}/product-price', 'Api\ProductController@updateProductPrice');
    Route::delete('api/product/{product}/product-price/{productPrice}', 'Api\ProductController@destroyProductPrice');

    Route::post('api/product/{product}/product-color-group', 'Api\ProductColorGroupController@store');
    Route::patch('api/product/{product}/product-color-group/{productColorGroup}', 'Api\ProductColorGroupController@update');
    Route::delete('api/product/{product}/product-color-group/{productColorGroup}', 'Api\ProductColorGroupController@destroy');
    Route::post('api/product/{product}/product-color-group/{productColorGroup}/product-color', 'Api\ProductColorGroupController@storeProductColor');
    Route::patch('api/product/{product}/product-color-group/{productColorGroup}/product-color/{productColor}', 'Api\ProductColorGroupController@updateProductColor');
    Route::delete('api/product/{product}/product-color-group/{productColorGroup}/product-color/{productColor}', 'Api\ProductColorGroupController@deleteProductColor');
    Route::delete('api/product/{product}/product-color-group/{productColorGroup}/product-color/{productColor}/product-color-price/{productColorPrice}', 'Api\ProductColorGroupController@deleteProductColorPrice');

    Route::patch('api/product/{product}/product-option/{productOption}', 'Api\ProductController@updateProductOption');
    Route::post('api/product/{product}/product-option', 'Api\ProductController@storeProductOption');
    Route::patch('api/product/{product}/product-option/{productOption}/product-sub-option/{productSubOption}', 'Api\ProductController@updateProductSubOption');
    Route::post('api/product/{product}/product-option/{productOption}/product-sub-option', 'Api\ProductController@storeProductSubOption');
    Route::delete('api/product/{product}/product-option/{productOption}/option-price/{productOptionPrice}', 'Api\ProductController@destroyProductOptionPrice');
    Route::delete('api/product/{product}/product-option/{productOption}/product-sub-option/{productSubOption}/product-sub-option-price/{productSubOptionPrice}', 'Api\ProductController@destroyProductSubOptionPrice');
    Route::delete('api/product/{product}/product-option/{productOption}', 'Api\ProductController@destroyProductOption');
    Route::delete('api/product/{product}/product-option/{productOption}/product-sub-option/{productSubOption}', 'Api\ProductController@destroyProductSubOption');

    /**
     * State
     */
    Route::get('api/state', 'Api\StateController@index');
    Route::patch('api/state/{state}', 'Api\StateController@update');

    /**
     * Discount
     */
    Route::get('api/discount/discount-condition', 'Api\DiscountController@indexDiscountConditions');
    Route::get('api/discount/discount-calc', 'Api\DiscountController@indexDiscountCalc');
    Route::post('api/discount', 'Api\DiscountController@store');
    Route::get('api/discount', 'Api\DiscountController@index');
    Route::delete('api/discount/{discount}', 'Api\DiscountController@destroy');
    Route::put('api/discount/{discount}', 'Api\DiscountController@update');
    Route::post('api/discount/{discount}/sort', 'Api\DiscountController@sort');

    /**
     * Category
     */
    Route::post('api/category/save-hierarchy', 'Api\CategoryController@saveHierarchy');
    Route::get('api/category/list', 'Api\CategoryController@getListWithoutSeparator');
    Route::resource('api/category', 'Api\CategoryController');
    Route::get('api/category/{category}/product', 'Api\CategoryController@productIndex');

    /**
     * Product Category
     */
    Route::post('api/product-category', 'Api\ProductCategoryController@store');
    Route::post('api/product-category/delete', 'Api\ProductCategoryController@destroy');
    Route::post('api/product-category/activate', 'Api\ProductCategoryController@activate');
    Route::post('api/product-category/deactivate', 'Api\ProductCategoryController@deactivate');

    /**
     * Imprints
     */
    Route::post('api/product/{product}/imprint', 'Api\ProductImprintController@store');
    Route::patch('api/product/{product}/imprint/{imprint}', 'Api\ProductImprintController@update');
    Route::delete('api/product/{product}/imprint/{imprint}', 'Api\ProductImprintController@destroy');
    Route::delete('api/product/{product}/imprint/{imprint}/imprint-price/{imprintPrice}', 'Api\ProductImprintController@destroyImprintPrice');

    /**
     * Imprint colors
     */
    Route::post('api/product/{product}/imprint/{imprint}/imprint-color', 'Api\ProductImprintColorController@store');
    Route::delete('api/product/{product}/imprint/{imprint}/imprint-color/{imprintColor}', 'Api\ProductImprintColorController@destroy');

    /**
     * Countries
     */
    Route::get('api/country', 'Api\CountryController@index');

    /**
     * Reminder
     */
    Route::get('api/reminder', 'Api\ReminderController@index');

    /**
     * Page
     */
    Route::get('api/page', 'Api\PageController@index');
    Route::post('api/page', 'Api\PageController@store');
    Route::put('api/page/{page}', 'Api\PageController@update');
    Route::delete('api/page/{page}', 'Api\PageController@delete');

    Route::get('api/stage', 'Api\StageController@index');
    Route::get('api/order/_search', 'Api\OrderController@search')->name('order.search');
    Route::get('api/order', 'Api\OrderController@index');

    /*
     * Tracking information of order item
     */
    Route::resource('api/order_item/{orderItem}/tracking_information', 'Api\OrderItemTrackingInformationController', ['only' => ['index', 'store']]);
    /*
     * End Tracking information of order item
     */

    Route::get('api/order_item/{orderItem}/invoice_file', 'OrderItemController@invoice');
    Route::delete('api/order_item/{orderItem}/invoice_file', 'OrderItemController@destroyInvoice');
    Route::get('api/order_item/{orderItem}', 'OrderItemController@show');
    Route::patch('api/order_item/{orderItem}', 'OrderItemController@update');
    Route::get('api/order_item/{orderItem}/stage', 'OrderStageController@index');
    Route::post('api/order_item/{orderItem}/stage', 'OrderStageController@store');

    Route::get('api/order_item/{orderItem}/back_note', 'Api\OrderItemBackNoteController@index');
    Route::post('api/order_item/{orderItem}/back_note', 'Api\OrderItemBackNoteController@store');

    Route::get('api/order_item/{orderItem}/user_note', 'Api\OrderItemUserNoteController@index');
    Route::post('api/order_item/{orderItem}/user_note', 'Api\OrderItemUserNoteController@store');

    Route::get('api/order_item/{orderItem}/virtual_cabinet_file', 'Api\OrderItemVirtualCabinetFileController@index');
    Route::post('api/order_item/{orderItem}/virtual_cabinet_file', 'Api\OrderItemVirtualCabinetFileController@store');
    Route::get('api/order_item/{orderItem}/virtual_cabinet_file/{virtualCabinetFile}/file', 'Api\OrderItemVirtualCabinetFileController@file');

    Route::get('api/order_item/{orderItem}/art_proof', 'Api\OrderItemArtProofController@index');
    Route::post('api/order_item/{orderItem}/art_proof', 'Api\OrderItemArtProofController@store');
    Route::get('api/order_item/{orderItem}/art_proof/{artProof}/file', 'Api\OrderItemArtProofController@file');

    Route::post('api/order_item/{orderItem}/extra_payment/other', 'Api\ExtraPaymentController@other');
    Route::get('api/order_item/{orderItem}/payment', 'Api\OrderItemPaymentController@index');
    Route::get('api/order_item/{orderItem}/customer_payment', 'Api\OrderItemPaymentController@customerPayments');
    Route::post('api/order_item/{orderItem}/extra_payment/credit', 'Api\ExtraPaymentController@credit');

    Route::get('api/vendor', 'Api\VendorController@index');
    Route::get('api/vendors-list', 'Api\VendorController@vendorsList');
    Route::get('api/vendor/{vendor}', 'Api\VendorController@show');
    Route::delete('api/vendor/{vendor}', 'Api\VendorController@destroy');
    Route::put('api/vendor/{vendor}', 'Api\VendorController@update');
    Route::post('api/vendor', 'Api\VendorController@store');


    Route::post('api/stage/{stage}/sort', 'Api\StageController@sort');
    Route::patch('api/stage/{stage}', 'Api\StageController@update');
    Route::delete('api/stage/{stage}', 'Api\StageController@destroy');
    Route::post('api/stage', 'Api\StageController@store');

    /**
     * Slider
     */
    Route::get('api/slider', 'Api\SliderController@index');
    Route::post('api/slider', 'Api\SliderController@store');
    Route::patch('api/slider/{slider}', 'Api\SliderController@update');
    Route::delete('api/slider/{slider}', 'Api\SliderController@destroy');

    Route::post('api/slider/{slider}/sort', 'Api\SliderController@sort');

    /**
     * Case Studies
     */
    Route::get('api/case-study', 'Api\CaseStudyController@index');
    Route::get('api/case-study/{caseStudy}', 'Api\CaseStudyController@show');
    Route::post('api/case-study', 'Api\CaseStudyController@store');
    Route::put('api/case-study/{caseStudy}', 'Api\CaseStudyController@update');
    Route::delete('api/case-study/{caseStudy}', 'Api\CaseStudyController@destroy');
    Route::post('api/case-study/{caseStudy}/sort', 'Api\CaseStudyController@sort');

    /**
     * Promotional Glossary
     */
    Route::get('api/promotional-glossary', 'Api\PromotionalGlossaryController@index');
    Route::post('api/promotional-glossary', 'Api\PromotionalGlossaryController@store');
    Route::put('api/promotional-glossary/{promotionalGlossary}', 'Api\PromotionalGlossaryController@update');
    Route::delete('api/promotional-glossary/{promotionalGlossary}', 'Api\PromotionalGlossaryController@destroy');
    Route::post('api/promotional-glossary/{promotionalGlossary}/sort', 'Api\PromotionalGlossaryController@sort');


    /**
     * Review
     */
    Route::get('api/review', 'Api\ReviewController@index');
    Route::post('api/review', 'Api\ReviewController@store');
    Route::put('api/review/{review}', 'Api\ReviewController@update');
    Route::delete('api/review/{review}', 'Api\ReviewController@destroy');

    /**
     * Size Group
     */
    Route::get('api/size-group', 'Api\SizeGroupController@index');
    Route::post('api/size-group', 'Api\SizeGroupController@store');
    Route::put('api/size-group/{sizeGroup}', 'Api\SizeGroupController@update');
    Route::delete('api/size-group/{sizeGroup}', 'Api\SizeGroupController@destroy');
    Route::delete('api/size-group/{sizeGroup}/size/{size}', 'Api\SizeGroupController@destroySize');

    /**
     * Payment Information Templates
     */
    Route::get('api/payment-information-template', 'Api\PaymentInformationTemplateController@index')->name('payment-information-template.index');
    Route::post('api/payment-information-template', 'Api\PaymentInformationTemplateController@store');
    Route::put('api/payment-information-template/{paymentInformationTemplate}', 'Api\PaymentInformationTemplateController@update');
    Route::delete('api/payment-information-template/{paymentInformationTemplate}', 'Api\PaymentInformationTemplateController@destroy');

    /**
     * Testimonials
     */
    Route::resource('api/testimonials', 'Api\TestimonialController');

    /**
     * Survey
     */
    Route::get('api/survey', 'Api\SurveyController@index');
    Route::post('api/survey', 'Api\SurveyController@store');
    Route::put('api/survey/{survey}', 'Api\SurveyController@update');
    Route::delete('api/survey/{survey}', 'Api\SurveyController@destroy');
    Route::delete('api/survey/{survey}/survey-question/{surveyQuestion}', 'Api\SurveyController@destroySurveyQuestion');
    Route::delete('api/survey/{survey}/survey-question/{surveyQuestion}/survey-answer/{surveyAnswer}', 'Api\SurveyController@destroySurveyAnswer');

    /**
     * Setting
     */
    Route::get('api/setting', 'Api\SettingController@index');
    Route::patch('api/setting/{setting}', 'Api\SettingController@update');

    /**
     * Post
     */
    Route::resource('api/post', 'Api\PostController');

    /**
     * Post Category
     */
    Route::model('post-category', '\App\Models\PostCategory');
    Route::get('api/post-category/all', 'Api\PostCategoryController@all');
    Route::resource('api/post-category', 'Api\PostCategoryController');


    /**
     * Info Messages
     */
    Route::get('api/info-message', 'Api\InfoMessageController@index')->name('info-messages.index');
    Route::patch('api/info-message/{infoMessage}', 'Api\InfoMessageController@update');

    /**
     * Editable Blocks
     */
    Route::get('api/block', 'Api\BlockController@index');
    Route::patch('api/block/{block}', 'Api\BlockController@update');
    Route::post('api/block/upload', 'Api\BlockController@upload');

    /**
     * Product Icons
     */
    Route::get('api/product-icon', 'Api\ProductIconController@index');
    Route::post('api/product-icon', 'Api\ProductIconController@store');
    Route::patch('api/product-icon/{productIcon}', 'Api\ProductIconController@update');
    Route::delete('api/product-icon/{productIcon}', 'Api\ProductIconController@destroy');

    /**
     * Color
     */
    Route::get('api/color', 'Api\ColorController@index');

    /**
     * PMS Color
     */
    Route::get('api/pms-color', 'Api\PmsColorController@index');
    Route::post('api/pms-color', 'Api\PmsColorController@store');
    Route::patch('api/pms-color/{pmsColor}', 'Api\PmsColorController@update');
    Route::delete('api/pms-color/{pmsColor}', 'Api\PmsColorController@destroy');

    /**
     * Color admin's page
     */
    Route::get('color', 'ColorController@index')->name('color.index');
    Route::get('color/search', 'ColorController@search')->name('color.search');
    Route::get('color/list', 'ColorController@list')->name('color.list');
    Route::get('color/create', 'ColorController@create')->name('color.create');
    Route::get('color/{color}/edit', 'ColorController@edit')->name('color.edit');
    Route::post('color', 'ColorController@store')->name('color.store');
    Route::post('color/multiple', 'ColorController@multipleStore')->name('color.multiple_store');
    Route::post('color/find', 'ColorController@find')->name('color.find');
    Route::match(['put', 'patch'], 'color/{color}', 'ColorController@update')->name('color.update');

    /**
     * Category admin's page
     */
    Route::get('category', 'CategoryController@index')->name('category.index');
    Route::post('category', 'CategoryController@store')->name('category.store');
    Route::get('category/search', 'CategoryController@search')->name('category.search');
    Route::get('category/create', 'CategoryController@create')->name('category.create');
    Route::get('category/{category}/edit', 'CategoryController@edit')->name('category.edit');
    Route::match(['put', 'patch'], 'category/{category}', 'CategoryController@update')->name('category.update');

    /**
     * Product categories admin's page
     */
    Route::get('product-category', 'ProductCategoryController@index')->name('product_category.index');

    /**
     * Color-group admin's page
     */
    Route::get('color-group', 'ColorGroupController@index')->name('color_group.index');
    Route::get('color-group/search', 'ColorGroupController@search')->name('color_group.search');
    Route::get('color-group/create', 'ColorGroupController@create')->name('color_group.create');
    Route::get('color-group/{colorGroup}/edit', 'ColorGroupController@edit')->name('color_group.edit');
    Route::post('color-group', 'ColorGroupController@store')->name('color_group.store');
    Route::match(['put', 'patch'], 'color-group/{colorGroup}', 'ColorGroupController@update')->name('color_group
    .update');

    /**
     * Imprint Color-group admin's page
//     */
//    Route::get('imprint-color-group', 'ImprintColorGroupController@index')->name('imprint_color_group.index');
//    Route::get('imprint-color-group/search', 'ImprintColorGroupController@search')->name('imprint_color_group.search');
//    Route::get('imprint-color-group/create', 'ImprintColorGroupController@create')->name('imprint_color_group.create');
//    Route::get('imprint-color-group/{colorGroup}/edit', 'ImprintColorGroupController@edit')->name('imprint_color_group.edit');
//    Route::post('imprint-color-group', 'ImprintColorGroupController@store')->name('imprint_color_group.store');
//    Route::match(['put', 'patch'], 'imprint-color-group/{colorGroup}', 'ImprintColorGroupController@update')->name('imprint_color_group
//    .update');

    /**
     * Color Group
     */
    Route::get('api/color-group', 'Api\ColorGroupController@index');

    /**
     * Sage admin's page
     */
    Route::group(['prefix' => 'sage', 'namespace' => 'Sage'], function () {
        Route::get('category', 'CategoryController@index')->name('sage_category.index');

        Route::get('supplier', 'SupplierController@index')->name('sage_supplier.index');
        Route::get('supplier-list', 'SupplierController@list')->name('sage_supplier.list');
        Route::post('supplier-product-list', 'SupplierController@productList')->name('sage_supplier.list');
        Route::get('supplier-product-detail/{product_id}', 'SupplierController@productDetail')->name('sage_supplier.product_detail');
        Route::post('supplier-products-search', 'SupplierController@productsSearch')->name('sage_supplier.products_search');
        Route::post('supplier-product-import', 'SupplierController@import')->name('sage_supplier_import.import');
        Route::post('check-colors', 'SupplierController@checkColors')->name('sage_supplier_import.check_colors');
    });

    /**
     * FAQ admin's page
     */
    Route::get('faq', 'FaqController@index')->name('faq.index')->middleware('admin.access');

    /**
     *  Testimonial admin's page
     */
    //Route::resource('testimonial', 'TestimonialController');

    /**
     *  Admin User admin's page
     */
//    Route::get('user', 'AdminUserController@index')->name('admin_user.index');
//    Route::post('user', 'AdminUserController@store')->name('admin_user.store');
//    Route::match(['put', 'patch'], 'user/{id}', 'AdminUserController@update')->name('admin_user.update');

    Route::get('api/administrators', 'Api\AdminUsersController@index');
    Route::post('api/administrators', 'Api\AdminUsersController@store');
    Route::put('api/administrators/{id}', 'Api\AdminUsersController@update');
    Route::delete('api/administrators/{user}', 'Api\AdminUsersController@destroy');

    /**
     * Customer page
     */
    Route::get('customer/{user}/edit', 'CustomerController@edit')->name('customer.edit')->middleware('admin.access');
    Route::post('customer', 'CustomerController@store')->name('customer.store')->middleware('admin.access');
    Route::match(['put', 'patch'], 'customer/{id}', 'CustomerController@update')->name('customer.update')->middleware('admin.access');

    /**
     * New CMS
     */
    Route::get('promotional-block', 'PromotionalBlockController@index')->name('promotional_block.index')->middleware('admin.access');
    Route::get('promotional-block/search', 'PromotionalBlockController@search')->name('promotional_block.search')->middleware('admin.access');
    Route::get('promotional-block/searchCategory', 'PromotionalBlockController@searchCategory')->name('promotional_block.search_category')->middleware('admin.access');
    Route::post('promotional-block', 'PromotionalBlockController@store')->name('promotional_block.store')->middleware('admin.access');

    /**
     * Company info
     */
    Route::get('company-info', 'CompanyInfoController@index')->name('company_info.index')->middleware('admin.access');
    Route::post('company-update', 'CompanyInfoController@update')->name('company_info.update')->middleware('admin.access');


    /**
     * Default routs
     */
    Route::get('{dashboardModel}', 'DashboardController@index')
        ->where('dashboardModel', 'color-groups|colors|vendors|size-group|product')
        ->name('dashboard.index');

    /**
     * Admin routes
     */
    Route::get('{adminDashboardModel}', 'DashboardController@index')
        ->where('adminDashboardModel', 'post-category|post|faqs|testimonials|survey|payment-information-template|case-study|review|block|pms-color|product-icon|promotional-glossary|state|slider|setting|info-message|reminder|page|customer|promos|administrators')
        ->name('admin.dashboard.index')
        ->middleware('admin.access');

    /**
     * Promos
     */

    Route::get('api/promos', 'Api\PromosController@index');
    Route::patch('api/promos/{promos}', 'Api\PromosController@update');
    Route::post('api/promos/file/upload', 'Api\PromosController@upload');

    /**
     * Sitemap
     */
    Route::get('sitemap', 'SiteMapController@index')->name('sitemap.index')->middleware('admin.access');
    Route::get('api/sitemap', 'Api\SiteMapController@index');
    Route::post('api/sitemap/generate', 'Api\SiteMapController@store');

    /*Route::get('{admin_entity}', 'BreadController@index')->name('admin_entity.index');
    Route::get('{admin_entity}/create', 'BreadController@create')->name('admin_entity.create');
    Route::post('{admin_entity}', 'BreadController@store')->name('admin_entity.store');
    Route::get('{admin_entity}/{id}', 'BreadController@show')->name('admin_entity.show');
    Route::get('{admin_entity}/{id}/edit', 'BreadController@edit')->name('admin_entity.edit');
    Route::match(['put', 'patch'], '{admin_entity}/{id}', 'BreadController@update')->name('admin_entity.update');
    Route::delete('{admin_entity}/{id}', 'BreadController@destroy')->name('admin_entity.destroy');*/

    Route::get('promotions', 'PromotionController@index')->name('promotion.index')->middleware('admin.access');
    Route::post('promotions', 'PromotionController@store')->name('promotion.store')->middleware('admin.access');

});
