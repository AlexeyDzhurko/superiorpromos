<?php

namespace App\Admin\Entities;

use App\Admin\Contracts\AdminEntity;
use App\Admin\FormField;
use App\Admin\FormFieldBuilder;
use App\Models\Role;
use App\User;

class UsersEntity implements AdminEntity
{

    /**
     * URL slug for specific Entity (e.g. /admin/{address}/edit, /admin/{address}/ )
     *
     * @return string
     */
    public function getBaseRouteName(): string
    {
        return 'users';
    }

    /**
     * Eloquent Model for CRUD
     *
     * @return string
     */
    public function getModel(): string
    {
        return User::class;
    }

    /**
     * Display Name (Plural)
     *
     * @return string
     */
    public function getDisplayNamePlural(): string
    {
        return 'Users';
    }

    /**
     * Display Name (Singular)
     *
     * @return string
     */
    public function getDisplayNameSingular(): string
    {
        return 'User';
    }

    /**
     * Icon CSS class
     *
     * @return string
     */
    public function getIcon(): string
    {
        return '';
    }

    /**
     * Fields configuration for showing
     * @return array
     */
    public function showFields(): array
    {
        return FormFieldBuilder::init()
            ->text('id')
            ->text('name', 'Name')
            ->text('email', 'Email')
            ->text('created_at', 'Created at')
            ->text('updated_at', 'Updated at')
            ->text('avatar', 'Avatar')
            ->text('contact_telephone', 'Contact telephone')
            ->text('ext', 'Ext')
            ->text('fax', 'Fax')
            ->build();
    }

    /**
     * Field configuration for add/edit pages
     *
     * @return array
     */
    public function formFields(): array
    {
        return FormFieldBuilder::init()
            ->text('id')
            ->text('name', 'Name')
            ->text('email', 'Email')
            ->text('created_at', 'Created at')
            ->text('updated_at', 'Updated at')
            ->text('avatar', 'Avatar')
            ->text('contact_telephone', 'Contact telephone')
            ->text('ext', 'Ext')
            ->text('fax', 'Fax')
            ->build();
        /*return FormFieldBuilder::init()
            ->text('name')
            ->text('email')
            ->password('password', 'Password', false)
            ->select('role', null, false, function(FormField $field){
                $field->setOptions([
                    Role::TYPE_ADMINISTRATOR => 'Administrator',
                    Role::TYPE_MANAGER => 'Manager',
                ])->setDefault(Role::TYPE_MANAGER);
            })
            ->checkbox('active', 'Active', false,
                function (FormField $field) {
                    $field->setOptions(['on' => 'Yes', 'off' => 'No']);
                }
            )
            ->checkbox('can_delete', 'Can do action "delete" (only for manager)', false,
                function (FormField $field) {
                    $field->setOptions(['on' => 'Yes', 'off' => 'No']);
                }
            )
            ->build();*/
    }
}
