<?php

namespace App\Admin\Entities;

use App\Admin\Contracts\AdminEntity;
use App\Models\ColorGroup;
use App\Admin\FormFieldBuilder;

class ColorGroupEntity implements AdminEntity
{

    /**
     * URL slug for specific Entity (e.g. /admin/{color-group}/edit, /admin/{color-group}/ )
     *
     * @return string
     */
    public function getBaseRouteName(): string
    {
        return 'color-group';
    }

    /**
     * Eloquent Model for CRUD
     *
     * @return string
     */
    public function getModel(): string
    {
        return ColorGroup::class;
    }

    /**
     * Display Name (Plural)
     *
     * @return string
     */
    public function getDisplayNamePlural(): string
    {
        return 'Color Groups';
    }

    /**
     * Display Name (Singular)
     *
     * @return string
     */
    public function getDisplayNameSingular(): string
    {
        return 'Color Group';
    }

    /**
     * Icon CSS class
     *
     * @return string
     */
    public function getIcon(): string
    {
        return '';
    }

    /**
     * Fields configuration for showing
     * @return array
     */
    public function showFields(): array
    {
        return FormFieldBuilder::init()
            ->text('id')
            ->text('name')
            ->build();
    }

    /**
     * Field configuration for add/edit pages
     *
     * @return array
     */
    public function formFields(): array
    {
        $rout = route('color.search');

        return FormFieldBuilder::init()
            ->text('name')
            ->collection('colors', 'colors', false, $rout )
            ->build();
    }
}
