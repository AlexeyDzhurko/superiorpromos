<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 21.03.17
 * Time: 13:41
 */

namespace App\Admin\Entities;


use App\Admin\Contracts\AdminEntity;
use App\Models\OrderItem;

class OrderItemEntity implements AdminEntity
{

    /**
     * URL slug for specific Entity (e.g. /admin/{customer}/edit, /admin/{email-template}/ )
     *
     * @return string
     */
    public function getBaseRouteName(): string
    {
        return 'order_item';
    }

    /**
     * Eloquent Model for CRUD
     *
     * @return string
     */
    public function getModel(): string
    {
        return OrderItem::class;
    }

    /**
     * Display Name (Plural)
     *
     * @return string
     */
    public function getDisplayNamePlural(): string
    {
        return 'Order Items';
    }

    /**
     * Display Name (Singular)
     *
     * @return string
     */
    public function getDisplayNameSingular(): string
    {
        return 'Order Item';
    }

    /**
     * Icon CSS class
     *
     * @return string
     */
    public function getIcon(): string
    {
        return '';
    }

    /**
     * Fields configuration for showing
     *
     * @return array
     */
    public function showFields(): array
    {
        return [];
    }

    /**
     * Field configuration for add/edit pages
     *
     * @return array
     */
    public function formFields(): array
    {
        return [];
    }
}
