<?php

namespace App\Admin\Entities;

use App\Admin\Contracts\AdminEntity;
use App\Models\Size;
use App\Admin\FormFieldBuilder;

class SizeEntity implements AdminEntity
{

    /**
     * URL slug for specific Entity (e.g. /admin/{size}/edit, /admin/{size}/ )
     *
     * @return string
     */
    public function getBaseRouteName(): string
    {
        return 'size';
    }

    /**
     * Eloquent Model for CRUD
     *
     * @return string
     */
    public function getModel(): string
    {
        return Size::class;
    }

    /**
     * Display Name (Plural)
     *
     * @return string
     */
    public function getDisplayNamePlural(): string
    {
        return 'Sizes';
    }

    /**
     * Display Name (Singular)
     *
     * @return string
     */
    public function getDisplayNameSingular(): string
    {
        return 'Sizes';
    }

    /**
     * Icon CSS class
     *
     * @return string
     */
    public function getIcon(): string
    {
        return '';
    }

    /**
     * Fields configuration for showing
     *
     * @return array
     */
    public function showFields(): array
    {
        return FormFieldBuilder::init()
            ->text('name')
            ->build();
    }

    /**
     * Field configuration for add/edit pages
     *
     * @return array
     */
    public function formFields(): array
    {
        return FormFieldBuilder::init()
            ->text('name')
            ->build();
    }
}
