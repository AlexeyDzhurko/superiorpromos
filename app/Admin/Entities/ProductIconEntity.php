<?php

namespace App\Admin\Entities;

use App\Admin\Contracts\AdminEntity;
use App\Models\ProductIcon;
use App\Admin\FormField;
use App\Admin\FormFieldBuilder;

class ProductIconEntity implements AdminEntity
{

    /**
     * URL slug for specific Entity (e.g. /admin/{customer}/edit, /admin/{product-icon}/ )
     *
     * @return string
     */
    public function getBaseRouteName(): string
    {
        return 'product-icon';
    }

    /**
     * Eloquent Model for CRUD
     *
     * @return string
     */
    public function getModel(): string
    {
        return ProductIcon::class;
    }

    /**
     * Display Name (Plural)
     *
     * @return string
     */
    public function getDisplayNamePlural(): string
    {
        return 'Product Icons';
    }

    /**
     * Display Name (Singular)
     *
     * @return string
     */
    public function getDisplayNameSingular(): string
    {
        return 'Product Icon';
    }

    /**
     * Icon CSS class
     *
     * @return string
     */
    public function getIcon(): string
    {
        return '';
    }

    /**
     * Fields configuration for showing
     *
     * @return array
     */
    public function showFields(): array
    {
        return FormFieldBuilder::init()
            ->text('text')
            ->build();
    }

    /**
     * Field configuration for add/edit pages
     *
     * @return array
     */
    public function formFields(): array
    {
        return FormFieldBuilder::init()
            ->text('text', 'text', true)
            ->image('icon_pic_src', 'icon', false)
            ->integer('icon_pic_width', 'width')
            ->integer('icon_pic_height', 'height')
            ->select('position', null, false, function(FormField $field){
                $field->setOptions([
                    ProductIcon::ICON_CODE_BOTTOM_LEFT => ProductIcon::ICON_NAME_BOTTOM_LEFT,
                    ProductIcon::ICON_CODE_TOP_LEFT => ProductIcon::ICON_NAME_TOP_LEFT,
                    ProductIcon::ICON_CODE_TOP_RIGHT => ProductIcon::ICON_NAME_TOP_RIGHT,
                    ProductIcon::ICON_CODE_BOTTOM_RIGHT => ProductIcon::ICON_NAME_BOTTOM_RIGHT,
                ])->setDefault(ProductIcon::ICON_CODE_BOTTOM_LEFT);
            })
            ->build();
    }
}
