<?php

namespace App\Admin\Entities;

use App\Admin\Contracts\AdminEntity;
use App\Admin\FormField;
use App\Models\Category;
use App\Admin\FormFieldBuilder;

class CategoryEntity implements AdminEntity
{

    /**
     * URL slug for specific Entity (e.g. /admin/{category}/edit, /admin/{category}/ )
     *
     * @return string
     */
    public function getBaseRouteName(): string
    {
        return 'category';
    }

    /**
     * Eloquent Model for CRUD
     *
     * @return string
     */
    public function getModel(): string
    {
        return Category::class;
    }

    /**
     * Display Name (Plural)
     *
     * @return string
     */
    public function getDisplayNamePlural(): string
    {
        return 'Categories';
    }

    /**
     * Display Name (Singular)
     *
     * @return string
     */
    public function getDisplayNameSingular(): string
    {
        return 'Category';
    }

    /**
     * Icon CSS class
     *
     * @return string
     */
    public function getIcon(): string
    {
        return '';
    }

    /**
     * Fields configuration for showing
     *
     * @return array
     */
    public function showFields(): array
    {
        return FormFieldBuilder::init()
            ->text('name')
            ->text('slug')
            ->build();
    }

    /**
     * Field configuration for add/edit pages
     *
     * @return array
     */
    public function formFields(): array
    {
        $rout = route('category.search');

        return FormFieldBuilder::init()
            ->text('name')
            ->text('slug')
            ->textarea('meta_title')
            ->textarea('meta_description')
            ->textarea('keywords')
            ->textarea('title')
            ->textarea('description')
            ->textarea('footer_text')
            ->textarea('froogle_description')
            ->image('image')
            ->checkbox('active', 'Active', false,
                function (FormField $field) {
                    $field->setOptions(['on' => 'Yes', 'off' => 'No']);
                }
            )
            ->collection('parent', 'parent', false, $rout)
            ->build();
    }
}
