<?php

namespace App\Admin\Entities;

use App\Admin\Contracts\AdminEntity;
use App\Admin\FormField;
use App\Admin\FormFieldBuilder;
use App\Models\Testimonial;

class TestimonialEntity implements AdminEntity
{

    /**
     * URL slug for specific Entity (e.g. /admin/{testimonial}/edit, /admin/{testimonial}/ )
     *
     * @return string
     */
    public function getBaseRouteName(): string
    {
        return 'testimonial';
    }

    /**
     * Eloquent Model for CRUD
     *
     * @return string
     */
    public function getModel(): string
    {
        return Testimonial::class;
    }

    /**
     * Display Name (Plural)
     *
     * @return string
     */
    public function getDisplayNamePlural(): string
    {
        return 'Testimonials';
    }

    /**
     * Display Name (Singular)
     *
     * @return string
     */
    public function getDisplayNameSingular(): string
    {
        return 'Testimonial';
    }

    /**
     * Icon CSS class
     *
     * @return string
     */
    public function getIcon(): string
    {
        return '';
    }

    /**
     * Fields configuration for showing
     * @return array
     */
    public function showFields(): array
    {
        return FormFieldBuilder::init()
            ->text('id')
            ->text('item_name', 'Name')
            ->text('created_at', 'Creation date')
            ->build();
    }

    /**
     * Field configuration for add/edit pages
     *
     * @return array
     */
    public function formFields(): array
    {
        return FormFieldBuilder::init()
            ->text('item_name', 'Item name')
            ->text('position_after', 'Position after')
            ->checkbox('active', '', false,
                function (FormField $field) {
                    $field->setOptions(['on' => 'Active', 'off' => 'Inactive']);
                }
            )
            ->text('name', 'Name')
            ->richTextarea('content', 'Contents')
            ->text('product_link', 'Product link')
            ->image('image', 'Image', false)
            ->build();
    }
}
