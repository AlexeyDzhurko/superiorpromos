<?php

namespace App\Admin\Entities;

use App\Admin\Contracts\AdminEntity;
use App\Admin\FormField;
use App\Models\Color;
use App\Admin\FormFieldBuilder;
use App\Models\PmsColor;

class ColorEntity implements AdminEntity
{

    /**
     * URL slug for specific Entity (e.g. /admin/{color}/edit, /admin/{color}/ )
     *
     * @return string
     */
    public function getBaseRouteName(): string
    {
        return 'color';
    }

    /**
     * Eloquent Model for CRUD
     *
     * @return string
     */
    public function getModel(): string
    {
        return Color::class;
    }

    /**
     * Display Name (Plural)
     *
     * @return string
     */
    public function getDisplayNamePlural(): string
    {
        return 'Colors';
    }

    /**
     * Display Name (Singular)
     *
     * @return string
     */
    public function getDisplayNameSingular(): string
    {
        return 'Color';
    }

    /**
     * Icon CSS class
     *
     * @return string
     */
    public function getIcon(): string
    {
        return '';
    }

    /**
     * Fields configuration for showing
     * @return array
     */
    public function showFields(): array
    {
        return FormFieldBuilder::init()
            ->text('id')
            ->text('name')
            ->text('color_hex')
            ->text('picture_width')
            ->text('picture_height')
            ->build();
    }

    /**
     * Field configuration for add/edit pages
     *
     * @return array
     */
    public function formFields(): array
    {
        $rout = route('color_group.search');

        $pmsColorsRoute = route('color_group.search');

        return FormFieldBuilder::init()
            ->text('name')
            ->text('color_hex')
            ->image('picture_src', 'image', false)
            ->integer('picture_width', 'width', false)
            ->integer('picture_height', 'height', false)
            ->select('pms_color_id', 'PMS Color', false, function(FormField $field){
                $pmsColors = PmsColor::all()->pluck('name', 'id');
                $field->setOptions($pmsColors->toArray());
            })
            ->collection('groups', 'groups', false, $rout)
            ->build();
    }
}
