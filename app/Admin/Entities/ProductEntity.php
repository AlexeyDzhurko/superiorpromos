<?php

namespace App\Admin\Entities;

use App\Admin\Contracts\AdminEntity;
use App\Models\Product;
use App\Admin\FormFieldBuilder;

class ProductEntity implements AdminEntity
{

    /**
     * URL slug for specific Entity (e.g. /admin/{product}/edit, /admin/{product}/ )
     *
     * @return string
     */
    public function getBaseRouteName(): string
    {
        return 'product';
    }

    /**
     * Eloquent Model for CRUD
     *
     * @return string
     */
    public function getModel(): string
    {
        return Product::class;
    }

    /**
     * Display Name (Plural)
     *
     * @return string
     */
    public function getDisplayNamePlural(): string
    {
        return 'Products';
    }

    /**
     * Display Name (Singular)
     *
     * @return string
     */
    public function getDisplayNameSingular(): string
    {
        return 'Product';
    }

    /**
     * Icon CSS class
     *
     * @return string
     */
    public function getIcon(): string
    {
        return '';
    }

    /**
     * Fields configuration for showing
     * @return array
     */
    public function showFields(): array
    {
        return FormFieldBuilder::init()
            ->text('id')
            ->text('name')
            ->bool('active')
            ->build();
    }

    /**
     * Field configuration for add/edit pages
     *
     * @return array
     */
    public function formFields(): array
    {
        return FormFieldBuilder::init()
            ->text('name')
            ->build();
    }
}
