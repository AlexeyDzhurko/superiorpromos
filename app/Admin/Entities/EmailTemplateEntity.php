<?php

namespace App\Admin\Entities;

use App\Admin\Contracts\AdminEntity;
use App\Models\EmailTemplate;
use App\Admin\FormField;
use App\Admin\FormFieldBuilder;

class EmailTemplateEntity implements AdminEntity
{

    /**
     * URL slug for specific Entity (e.g. /admin/{customer}/edit, /admin/{email-template}/ )
     *
     * @return string
     */
    public function getBaseRouteName(): string
    {
        return 'email-templates';
    }

    /**
     * Eloquent Model for CRUD
     *
     * @return string
     */
    public function getModel(): string
    {
        return EmailTemplate::class;
    }

    /**
     * Display Name (Plural)
     *
     * @return string
     */
    public function getDisplayNamePlural(): string
    {
        return 'Email Templates';
    }

    /**
     * Display Name (Singular)
     *
     * @return string
     */
    public function getDisplayNameSingular(): string
    {
        return 'Email Template';
    }

    /**
     * Icon CSS class
     *
     * @return string
     */
    public function getIcon(): string
    {
        return '';
    }

    /**
     * Fields configuration for showing
     *
     * @return array
     */
    public function showFields(): array
    {
        return FormFieldBuilder::init()
            ->text('title')
            ->build();
    }

    /**
     * Field configuration for add/edit pages
     *
     * @return array
     */
    public function formFields(): array
    {
        return FormFieldBuilder::init()
            ->text('title')
            ->text('subject')
            ->richTextarea('body')
            ->textarea('description')
            ->select('type', null, true, function(FormField $field){
                $field->setOptions(['html' => 'HTML', 'text' => 'Text'])->setDefault('html');
            })
            ->build();
    }
}
