<?php

namespace App\Admin\Entities;

use App\Admin\Contracts\AdminEntity;
use App\Admin\FormField;
use App\Models\Faq;
use App\Admin\FormFieldBuilder;

class FaqEntity implements AdminEntity
{

    /**
     * URL slug for specific Entity (e.g. /admin/{faq}/edit, /admin/{faq}/ )
     *
     * @return string
     */
    public function getBaseRouteName(): string
    {
        return 'faq';
    }

    /**
     * Eloquent Model for CRUD
     *
     * @return string
     */
    public function getModel(): string
    {
        return Faq::class;
    }

    /**
     * Display Name (Plural)
     *
     * @return string
     */
    public function getDisplayNamePlural(): string
    {
        return 'FAQs';
    }

    /**
     * Display Name (Singular)
     *
     * @return string
     */
    public function getDisplayNameSingular(): string
    {
        return 'FAQ';
    }

    /**
     * Icon CSS class
     *
     * @return string
     */
    public function getIcon(): string
    {
        return '';
    }

    /**
     * Fields configuration for showing
     * @return array
     */
    public function showFields(): array
    {
        return FormFieldBuilder::init()
            ->text('id')
            ->text('question')
            ->richTextarea('answer')
            ->bool('active')
            ->build();
    }

    /**
     * Field configuration for add/edit pages
     *
     * @return array
     */
    public function formFields(): array
    {
        return FormFieldBuilder::init()
            ->text('question')
            ->richTextarea('answer')
            ->checkbox('active', '', false,
                function (FormField $field) {
                    $field->setOptions(['on' => 'Active', 'off' => 'Inactive']);
                }
            )
            ->build();
    }
}
