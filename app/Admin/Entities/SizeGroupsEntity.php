<?php

namespace App\Admin\Entities;

use App\Admin\Contracts\AdminEntity;
use App\Models\Size;
use App\Admin\FormField;
use App\Admin\FormFieldBuilder;
use App\Models\SizeGroup;

class SizeGroupsEntity implements AdminEntity
{

    /**
     * URL slug for specific Entity (e.g. /admin/{size-group}/edit, /admin/{size-group}/ )
     *
     * @return string
     */
    public function getBaseRouteName(): string
    {
        return 'size-group';
    }

    /**
     * Eloquent Model for CRUD
     *
     * @return string
     */
    public function getModel(): string
    {
        return SizeGroup::class;
    }

    /**
     * Display Name (Plural)
     *
     * @return string
     */
    public function getDisplayNamePlural(): string
    {
        return 'Size Groups';
    }

    /**
     * Display Name (Singular)
     *
     * @return string
     */
    public function getDisplayNameSingular(): string
    {
        return 'Size Group';
    }

    /**
     * Icon CSS class
     *
     * @return string
     */
    public function getIcon(): string
    {
        return '';
    }

    /**
     * Fields configuration for showing
     *
     * @return array
     */
    public function showFields(): array
    {
        return FormFieldBuilder::init()
            ->text('name')
            ->build();
    }

    /**
     * Field configuration for add/edit pages
     *
     * @return array
     */
    public function formFields(): array
    {
        return FormFieldBuilder::init()
            ->text('name')
            ->checkboxList('sizes', 'sizes', true, function(FormField $field){
                $options = Size::all();
                $field->setOptions($options->toArray());
            })
            ->build();
    }
}
