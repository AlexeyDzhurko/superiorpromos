<?php

namespace App\Admin\Entities;

use App\Admin\Contracts\AdminEntity;
use App\Admin\FormField;
use App\Admin\FormFieldBuilder;
use App\User;

class CustomerEntity implements AdminEntity
{

    /**
     * URL slug for specific Entity (e.g. /admin/{customer}/edit, /admin/{customer}/ )
     *
     * @return string
     */
    public function getBaseRouteName(): string
    {
        return 'customer';
    }

    /**
     * Eloquent Model for CRUD
     *
     * @return string
     */
    public function getModel(): string
    {
        return User::class;
    }

    /**
     * Display Name (Plural)
     *
     * @return string
     */
    public function getDisplayNamePlural(): string
    {
        return 'Customers';
    }

    /**
     * Display Name (Singular)
     *
     * @return string
     */
    public function getDisplayNameSingular(): string
    {
        return 'Customer';
    }

    /**
     * Icon CSS class
     *
     * @return string
     */
    public function getIcon(): string
    {
        return '';
    }

    /**
     * Fields configuration for showing
     * @return array
     */
    public function showFields(): array
    {
        return FormFieldBuilder::init()
            ->text('id')
            ->text('email')
            ->text('name', 'Contact Name')
            ->bool('active')
            ->bool('tax_exempt')
            ->text('created_at', 'Created at')
            ->build();
    }

    /**
     * Field configuration for add/edit pages
     *
     * @return array
     */
    public function formFields(): array
    {
        return FormFieldBuilder::init()
            ->text('name', 'Contact Name')
            ->text('email')
            ->password('password', 'Password', false)
            ->text('contact_telephone', 'Contact telephone', false)
            ->text('ext', 'Contact Phone Number Extention', false)
            ->text('authorize_net_id', 'Authorize Net ID', false)
            ->checkbox('tax_exempt', 'Tax Exempt', false,
                function (FormField $field) {
                    $field->setOptions(['on' => 'Yes', 'off' => 'No']);
                }
            )
            ->checkbox('active', 'Active', false,
                function (FormField $field) {
                    $field->setOptions(['on' => 'Yes', 'off' => 'No']);
                }
            )
            ->checkbox('subscribe', 'Subscribe to newsletter', false,
                function (FormField $field) {
                    $field->setOptions(['on' => 'Yes', 'off' => 'No']);
                })
            ->checkbox('tax_exempt', 'Tax exempt', false,
                function (FormField $field) {
                    $field->setOptions(['on' => 'Yes', 'off' => 'No']);
                })
            ->build();
    }
}
