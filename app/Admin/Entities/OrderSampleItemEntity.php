<?php

namespace App\Admin\Entities;

use App\Admin\Contracts\AdminEntity;
use App\Models\OrderSampleItem;

class OrderSampleItemEntity implements AdminEntity
{

    /**
     * URL slug for specific Entity (e.g. /admin/{customer}/edit, /admin/{email-template}/ )
     *
     * @return string
     */
    public function getBaseRouteName(): string
    {
        return 'order_sample_item';
    }

    /**
     * Eloquent Model for CRUD
     *
     * @return string
     */
    public function getModel(): string
    {
        return OrderSampleItem::class;
    }

    /**
     * Display Name (Plural)
     *
     * @return string
     */
    public function getDisplayNamePlural(): string
    {
        return 'Order Sample Items';
    }

    /**
     * Display Name (Singular)
     *
     * @return string
     */
    public function getDisplayNameSingular(): string
    {
        return 'Order Sample Item';
    }

    /**
     * Icon CSS class
     *
     * @return string
     */
    public function getIcon(): string
    {
        return '';
    }

    /**
     * Fields configuration for showing
     *
     * @return array
     */
    public function showFields(): array
    {
        return [];
    }

    /**
     * Field configuration for add/edit pages
     *
     * @return array
     */
    public function formFields(): array
    {
        return [];
    }
}
