<?php

namespace App\Admin\Entities;

use App\Admin\Contracts\AdminEntity;
use App\Models\Vendor;
use App\Admin\FormFieldBuilder;

class VendorEntity implements AdminEntity
{

    /**
     * URL slug for specific Entity (e.g. /admin/{vendor}/edit, /admin/{vendor}/ )
     *
     * @return string
     */
    public function getBaseRouteName(): string
    {
        return 'vendor';
    }

    /**
     * Eloquent Model for CRUD
     *
     * @return string
     */
    public function getModel(): string
    {
        return Vendor::class;
    }

    /**
     * Display Name (Plural)
     *
     * @return string
     */
    public function getDisplayNamePlural(): string
    {
        return 'Vendors';
    }

    /**
     * Display Name (Singular)
     *
     * @return string
     */
    public function getDisplayNameSingular(): string
    {
        return 'Vendor';
    }

    /**
     * Icon CSS class
     *
     * @return string
     */
    public function getIcon(): string
    {
        return '';
    }

    /**
     * Fields configuration for showing
     * @todo after add product need refactor
     * @return array
     */
    public function showFields(): array
    {
        $rout = route('admin.api.product.index') . '?vendor=';

        return FormFieldBuilder::init()
            ->text('id')
            ->text('name')
            ->text('email')
            ->text('state')
            ->text('city')
            ->text('country')
            ->text('zip_code')
            ->text('address_1')
            ->bool('active')
            ->build();
    }

    /**
     * Field configuration for add/edit pages
     *
     * @return array
     */
    public function formFields(): array
    {
        return FormFieldBuilder::init()
            ->text('name')
            ->build();
    }
}
