<?php

namespace App\Admin\Entities;

use App\Admin\Contracts\AdminEntity;
use App\Admin\FormField;
use App\Admin\FormFieldBuilder;
use App\Models\Address;
use App\Models\Role;

class AddressEntity implements AdminEntity
{

    /**
     * URL slug for specific Entity (e.g. /admin/{address}/edit, /admin/{address}/ )
     *
     * @return string
     */
    public function getBaseRouteName(): string
    {
        return 'address';
    }

    /**
     * Eloquent Model for CRUD
     *
     * @return string
     */
    public function getModel(): string
    {
        return Address::class;
    }

    /**
     * Display Name (Plural)
     *
     * @return string
     */
    public function getDisplayNamePlural(): string
    {
        return 'Addresses';
    }

    /**
     * Display Name (Singular)
     *
     * @return string
     */
    public function getDisplayNameSingular(): string
    {
        return 'Address';
    }

    /**
     * Icon CSS class
     *
     * @return string
     */
    public function getIcon(): string
    {
        return '';
    }

    /**
     * Fields configuration for showing
     * @return array
     */
    public function showFields(): array
    {
        return FormFieldBuilder::init()
            ->text('id')
            ->text('user_id', 'User Id')
            ->text('first_name', 'First name')
            ->text('middle_name', 'Middle name')
            ->text('last_name', 'Last name')
            ->text('title')
            ->text('suffix')
            ->text('company_name', 'Company Name')
            ->text('address_line_1', 'Address line 1')
            ->text('address_line_2', 'Address line 2')
            ->text('city')
            ->text('state')
            ->text('zip')
            ->text('country')
            ->text('province')
            ->text('day_telephone', 'Day phone')
            ->text('ext')
            ->text('fax')
            ->build();
    }

    /**
     * Field configuration for add/edit pages
     *
     * @return array
     */
    public function formFields(): array
    {
        return FormFieldBuilder::init()
            ->text('name')
            ->text('email')
            ->password('password', 'Password', false)
            ->select('role', null, false, function(FormField $field){
                $field->setOptions([
                    Role::TYPE_ADMINISTRATOR => 'Administrator',
                    Role::TYPE_MANAGER => 'Manager',
                ])->setDefault(Role::TYPE_MANAGER);
            })
            ->checkbox('active', 'Active', false,
                function (FormField $field) {
                    $field->setOptions(['on' => 'Yes', 'off' => 'No']);
                }
            )
            ->checkbox('can_delete', 'Can do action "delete" (only for manager)', false,
                function (FormField $field) {
                    $field->setOptions(['on' => 'Yes', 'off' => 'No']);
                }
            )
            ->build();
    }
}
