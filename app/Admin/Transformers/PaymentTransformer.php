<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 25.04.17
 * Time: 15:45
 */

namespace App\Admin\Transformers;


use App\Models\Payment;
use App\PaymentProfile;
use App\Transformers\PaymentProfileTransformer;
use League\Fractal\TransformerAbstract;

class PaymentTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'paymentProfile',
    ];

    public function transform(Payment $payment)
    {
        return [
            'id' => $payment->id,
            'title' => trans('payments.type.' . $payment->type) . ' (Payment ID #' . $payment->id . ') - Payment Profile ID #' . $payment->payment_profile_id,
            'payment_profile_id' => $payment->payment_profile_id,
            'status' => trans('payments.status.' . $payment->status),
            'transaction_id' => $payment->transaction_id,
            'created_at' => isset($payment->created_at) ? $payment->created_at->format('m/d/y g:i A') : null,
            'type' => trans('payments.type.' . $payment->type),
            'tax' => $payment->tax,
            'price' => $payment->price,
            'credit_amount' => $payment->credit_amount,
            'overall' => $payment->tax - ($payment->credit_amount ?? 0 ),
        ];
    }

    public function includePaymentProfile(Payment $payment)
    {
        return $this->item($payment->paymentProfile ?? new PaymentProfile(), new PaymentProfileTransformer());
    }

}
