<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 24.04.17
 * Time: 0:41
 */

namespace App\Admin\Transformers;


use App\Models\ArtProof;
use League\Fractal\TransformerAbstract;

class ArtProofTransformer extends TransformerAbstract
{

    protected $defaultIncludes = [
        'user',
    ];

    public function transform(ArtProof $artProof)
    {
        return [
            'id' => $artProof->id,
            'created_at' => $artProof->created_at->format('m/d/y g:i A'),
            'path' =>  $artProof->path,
            'note' =>  $artProof->note,
            'customer_note' =>  $artProof->customer_note,
            'answered_at' => $artProof->answered_at && !is_null($artProof->approved)
                    ? $artProof->answered_at->format('m/d/y g:i A')
                    : null,
            'status' => is_null($artProof->approved) ? 'N/A' : ($artProof->approved ? 'approved' : 'declined'),
        ];
    }

    public function includeUser(ArtProof $artProof)
    {
        return $this->item($artProof->user, function ($user) {
            return [
                'id' => $user->id,
                'name' => $user->name,
            ];
        });
    }

}
