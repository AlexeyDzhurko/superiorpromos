<?php

namespace App\Admin\Transformers;

use App\Models\Post;
use League\Fractal\TransformerAbstract;

class PostTransformer extends TransformerAbstract
{
    public function transform(Post $post)
    {
        return [
            'id' => $post->id,
            'title' => $post->title,
            'seo_title' => $post->seo_title,
            'excerpt' => $post->excerpt,
            'body' => $post->body,
            'image' => $post->image,
            'slug' => $post->slug,
            'meta_description' => $post->meta_description,
            'meta_keywords' => $post->meta_keywords,
            'status' => $post->status,
            'created_at' => $post->created_at->timestamp * 1000,
            'post_category_id' => $post->post_category_id,
        ];
    }
}
