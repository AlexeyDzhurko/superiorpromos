<?php

namespace App\Admin\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Category;

/**
 * Class CategoryTransformer
 * @package App\Transformers
 *
 * @SWG\Definition (
 *      definition="CategoryFull",
 *      @SWG\Property (property="id", type="integer", example=1),
 *      @SWG\Property (property="meta_title", type="string", example="Meta Title"),
 *      @SWG\Property (property="meta_description", type="string", example="Meta Description"),
 *      @SWG\Property (property="keywords", type="string", example="Keywords"),
 *      @SWG\Property (property="title", type="string", example="Title"),
 *      @SWG\Property (property="description", type="string", example="Description"),
 *      @SWG\Property (property="footer_text", type="string", example="Footer Text"),
 *      @SWG\Property (property="active", type="boolean", example=1),
 *      @SWG\Property (property="is_quick_link", type="boolean", example=0),
 *      @SWG\Property (property="froogle_description", type="string", example="Category"),
 *      @SWG\Property (property="name", type="string", example="Name"),
 *      @SWG\Property (property="slug", type="string", example="first-category"),
 *      @SWG\Property (property="image_src", type="string", example="Ni0mu0nwknyciN6viCnh.jpg"),
 *      @SWG\Property (property="parent_id", type="integer", example=23),
 *      @SWG\Property (property="created_at", type="string", example="2018-03-07 11:04:07"),
 *      @SWG\Property (property="children", type="array", items=@SWG\Schema(ref="#/definitions/CategoryFull")),
 *     @SWG\Property (property="is_popular", type="boolean", example=0),
 * ),
 */
class CategoryTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'children',
    ];

    /**
     * A Fractal transformer.
     * @param Category $category
     * @return array
     */
    public function transform(Category $category)
    {
        return [
            'meta_title'          => $category->meta_title ?? null,
            'meta_description'    => $category->meta_description ?? null,
            'keywords'            => $category->keywords ?? null,
            'title'               => $category->title ?? null,
            'description'         => $category->description ?? null,
            'footer_text'         => $category->footer_text ?? null,
            'active'              => $category->active ?? false,
            'is_quick_link'       => $category->is_quick_link ?? 0,
            'froogle_description' => $category->froogle_description ?? null,
            'id'                  => $category->id ?? null,
            'name'                => $category->name ?? null,
            'slug'                => $category->slug ?? null,
            'image_src'           => 'storage/' . Category::IMAGES_DIR . $category->image_src ?? null,
            'parent_id'           => $category->parent_id ?? null,
            //'lft'                 => $category->lft ?? null,
            //'rgt'                 => $category->rgt ?? null,
            //'depth'               => $category->depth ?? null,
            'created_at'          => $category->created_at->toDateTimeString() ?? null,
            'is_popular'       => $category->is_popular ?? 0,
        ];
    }

    /**
     * Include Products
     *
     * @param Category $category
     * @return \League\Fractal\Resource\Item
     */
    public function includeChildren(Category $category)
    {
        return $this->collection($category->children, new CategoryTransformer());
    }
}
