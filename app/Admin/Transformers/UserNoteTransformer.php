<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 22.04.17
 * Time: 1:14
 */

namespace App\Admin\Transformers;


use App\Models\UserNote;
use League\Fractal\TransformerAbstract;

class UserNoteTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'user',
    ];

    public function transform(UserNote $userNote)
    {
        return [
            'id' => $userNote->id,
            'created_at' => $userNote->created_at->format('m/d/y g:i A'),
            'subject' => $userNote->subject,
            'note' => $userNote->note,
            'customer_note' => $userNote->customer_note,
            'answered_at' => $userNote->answered_at ? $userNote->answered_at->format('m/d/y g:i A') : null,
            'status' => is_null($userNote->approved) ? 'N/A' : ($userNote->approved ? 'approved' : 'declined'),
        ];
    }


    public function includeUser(UserNote $userNote)
    {
        return $this->item($userNote->user, function ($user) {
            return [
                'id' => $user->id,
                'name' => $user->name,
            ];
        });
    }
}
