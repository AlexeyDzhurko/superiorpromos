<?php

namespace App\Admin\Transformers;

use App\Models\Post;
use App\Models\PostCategory;
use League\Fractal\TransformerAbstract;

class PostCategoryTransformer extends TransformerAbstract
{
    public function transform(PostCategory $post)
    {
        return [
            'id' => $post->id,
            'title' => $post->title,
            'created_at' => $post->created_at->timestamp * 1000,
        ];
    }
}
