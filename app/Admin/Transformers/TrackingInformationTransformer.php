<?php

namespace App\Admin\Transformers;

use App\Models\TrackingInformation;
use League\Fractal\TransformerAbstract;

/**
 * Class TrackingInformationTransformer
 * @package App\Admin\Transformers
 *
 * @SWG\Definition (
 *      definition="TrackingInformation",
 *      @SWG\Property(property="id", type="integer", example="1"),
 *      @SWG\Property(property="created_at", type="string", example="2018-10-17 15:40:45"),
 *      @SWG\Property(property="tracking_note", type="string", example="tracking note"),
 *      @SWG\Property(property="tracking_number", type="string", example="1Z69R6E00329117067"),
 *      @SWG\Property(property="tracking_shipping_company", type="string", example="UPS"),
 *      @SWG\Property(property="tracking_shipping_date", type="string", example="2018-10-17"),
 * ),
 */
class TrackingInformationTransformer extends TransformerAbstract
{
    public function transform(TrackingInformation $trackingInformation)
    {
        return [
            'id' => $trackingInformation->id,
            'created_at' => (string)$trackingInformation->created_at,
            'tracking_note' =>  $trackingInformation->tracking_note,
            'tracking_number' =>  $trackingInformation->tracking_number,
            'tracking_shipping_company' => $trackingInformation->tracking_shipping_company,
            'tracking_shipping_date' => $trackingInformation->tracking_shipping_date,
        ];
    }
}
