<?php


namespace App\Admin\Transformers;


use App\Models\OrderSampleTrackingInformation;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class OrderSampleItemTrackingInformationTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $defaultIncludes = [
        'user',
    ];

    /**
     * @param OrderSampleTrackingInformation $orderSampleTrackingInformation
     * @return array
     */
    public function transform(OrderSampleTrackingInformation $orderSampleTrackingInformation)
    {
        return [
            'id' => $orderSampleTrackingInformation->id,
            'created_at' => $orderSampleTrackingInformation->created_at->format('Y-m-d'),
            'tracking_note' =>  $orderSampleTrackingInformation->tracking_note,
            'tracking_number' =>  $orderSampleTrackingInformation->tracking_number,
            'tracking_shipping_company' => $orderSampleTrackingInformation->tracking_shipping_company,
            'tracking_shipping_date' => $orderSampleTrackingInformation->tracking_shipping_date,
        ];
    }

    /**
     * @param OrderSampleTrackingInformation $orderSampleTrackingInformation
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(OrderSampleTrackingInformation $orderSampleTrackingInformation)
    {
        if(is_null($orderSampleTrackingInformation->trackingUser)) {
            return $this->item([], function (){
                return [
                    'name' => 'Admin',
                ];
            });
        }

        return $this->item($orderSampleTrackingInformation->trackingUser, function ($user) {
            return [
                'name' => $user->name,
            ];
        });
    }
}
