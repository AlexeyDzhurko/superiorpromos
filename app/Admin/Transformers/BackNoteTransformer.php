<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 19.04.17
 * Time: 15:03
 */

namespace App\Admin\Transformers;


use App\Models\BackNote;
use League\Fractal\TransformerAbstract;

class BackNoteTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'user',
    ];

    public function transform(BackNote $backNote)
    {
        return [
            'created_at' => $backNote->created_at->format('m/d/y g:i A'),
            'note' => $backNote->note,
        ];
    }

    public function includeUser(BackNote $backNote)
    {
        return $this->item($backNote->user, function ($user) {
            return [
                'id' => $user->id,
                'name' => $user->name,
            ];
        });
    }

}
