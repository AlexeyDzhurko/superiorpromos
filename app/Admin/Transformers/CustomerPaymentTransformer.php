<?php


namespace App\Admin\Transformers;

use Carbon\Carbon;
use App\PaymentProfile;
use League\Fractal\TransformerAbstract;

class CustomerPaymentTransformer extends TransformerAbstract
{
    public function transform(PaymentProfile $paymentProfile)
    {
        return [
            'id'          => $paymentProfile->id,
            'card_holder' => $paymentProfile->card_holder,
            'card_number' => $paymentProfile->card_number,
            'is_default'  => $paymentProfile->is_default,
            'is_expired'  => $this->isExpired($paymentProfile)
        ];
    }

    private function isExpired(PaymentProfile $paymentProfile)
    {
        if(is_null($paymentProfile->expiration_year)) {
            return true;
        }
        $now = Carbon::now()->endOfMonth();
        $expirationYear = Carbon::createFromFormat('Y', $paymentProfile->expiration_year)->year;
        if (strlen($expirationYear) != 4) {
            $expirationYear = Carbon::createFromFormat('y', $paymentProfile->expiration_year)->year;
        }
        $expires = Carbon::createFromFormat("m - Y", $paymentProfile->expiration_month . ' - ' . $expirationYear)->endOfMonth();
        return $expires < $now;
    }
}
