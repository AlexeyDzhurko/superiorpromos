<?php


namespace App\Admin\Transformers;


use App\Models\Product;
use App\Transformers\ImprintTransformer;
use App\Transformers\ProductColorGroupTransformer;
use App\Transformers\ProductExtraImageTransformer;
use App\Transformers\ProductOptionTransformer;
use App\Transformers\ProductPriceTransformer;
use League\Fractal\TransformerAbstract;

class AdminProductTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'product_options',
        'imprints',
        'product_prices',
        'product_extra_images',
        'product_color_groups',
    ];

    public function transform(Product $product)
    {
        return [
            'id' => $product->id,
            'name' => $product->name,
            'url' => $product->url,
            'active' => $product->active,
            'keywords' => $product->keywords,
            'meta_title' => $product->meta_title,
            'meta_description' => $product->meta_description,
            'popularity' => $product->popularity,
            'sage_id' => $product->sage_id,
            'image_alt' => $product->image_alt,
            'production_time_from' => $product->production_time_from,
            'production_time_to' => $product->production_time_to,
            'quantity_per_box' => $product->quantity_per_box,
            'box_weight' => $product->box_weight,
            'brand' => $product->brand,
            'gender' => $product->gender,
            'age_group' => $product->age_group,
            'material' => $product->material,
            'pattern' => $product->pattern,
            'google_product_category' => $product->google_product_category,
            'zip_from' => $product->zip_from,
            'shipping_additional_type' => $product->shipping_additional_type,
            'shipping_additional_value' => $product->shipping_additional_value,
            'dimensions' => $product->dimensions,
            'description' => $product->description,
            'imprint_area' => $product->imprint_area,
            'pricing_information' => $product->pricing_information,
            'on_sale' => $product->on_sale,
            'additional_specifications' => asset('storage/' . $product->additional_specifications),
            'video' => asset('storage/' . $product->video),
            'video_url' => $product->video_url,
            'vendors' => $product->vendors->pluck('id'),
            'categories' => $product->categories->pluck('id'),
            'product_icon_id' => $product->product_icon_id,
            'size_group_id' => $product->size_group_id,
            'vendor_sku' => $this->getVendorIdsWithSku($product),
            'custom_shipping_method' => $product->custom_shipping_method,
            'custom_shipping_cost' => $product->custom_shipping_cost
        ];
    }

    public function getVendorIdsWithSku(Product $product)
    {
        $data = [];
        foreach ($product->vendors as $vendor) {
            $data[$vendor->id] =  $vendor->pivot->sku;
        }
        return $data;
    }

    public function includeProductOptions(Product $product)
    {
        return $this->collection($product->productOptions, new ProductOptionTransformer());
    }

    public function includeImprints(Product $product)
    {
        return $this->collection($product->imprints, new ImprintTransformer());
    }

    public function includeProductPrices(Product $product)
    {
        return $this->collection($product->productPrices, new ProductPriceTransformer());
    }

    public function includeProductExtraImages(Product $product)
    {
        return $this->collection($product->productExtraImages, new ProductExtraImageTransformer());
    }

    public function includeProductColorGroups(Product $product)
    {
        return $this->collection($product->productColorGroups, new ProductColorGroupTransformer());
    }

}
