<?php

namespace App\Admin\Transformers;

use App\Models\OrderSample;
use App\Transformers\OrderSampleItemTransformer;
use League\Fractal\TransformerAbstract;

class OrderSampleTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'order_items',
    ];

    public function transform($order)
    {
        return [
            'order_id' => $order['id'],
            'first_name' => $order['billing_first_name'],
            'last_name' => $order['billing_last_name'],
            'user_id' => $order['user_id'],
            'total_price' => $order['total_price'],
            'state' => $order['state'],
            'created_at' => $order['created_at'],
        ];
    }

    public function includeOrderItems($order)
    {
        return $this->collection(OrderSample::find($order['id'])->orderSampleItems, new OrderSampleItemTransformer());
    }
}
