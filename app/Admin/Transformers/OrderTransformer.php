<?php

namespace App\Admin\Transformers;

use App\Models\Order;
use App\Transformers\TinyOrderItemTransformer;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class OrderTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'order_items',
    ];

    public function transform($order)
    {
        return [
            'order_id' => $order['id'],
            'first_name' => $order['billing_first_name'],
            'last_name' => $order['billing_last_name'],
            'shipping_first_name' => $order['shipping_first_name'],
            'shipping_last_name' => $order['shipping_last_name'],
            'user_id' => $order['user_id'],
            'user_name' => $order['user_name'],
            'total_price' => $this->getTotalPrice($order),
            'state' => $order['state'],
            'created_at' => Carbon::parse($order['created_at'])->format('m/d/y g:i A'),
        ];
    }

    public function getTotalPrice($order)
    {
//        $order = Order::find($order['id']);
//        $orderItems = $order->orderItems;
//        $sum = 0;
//
//        foreach ($orderItems as $orderItem) {
//            $sum += ($orderItem->price);
//        }

        return number_format($order['total_price'], 2);
    }
    /**
     * @param $order
     * @return \League\Fractal\Resource\Collection
     */
    public function includeOrderItems($order)
    {
        return $this->collection(Order::find($order['id'])->orderItems, new TinyOrderItemTransformer());
    }
}
