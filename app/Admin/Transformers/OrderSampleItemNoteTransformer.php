<?php


namespace App\Admin\Transformers;


use App\Models\OrderSampleItemNote;
use League\Fractal\TransformerAbstract;

class OrderSampleItemNoteTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $defaultIncludes = [
        'user',
    ];

    /**
     * @param OrderSampleItemNote $orderSampleItemNote
     * @return array
     */
    public function transform(OrderSampleItemNote $orderSampleItemNote)
    {
        return [
            'note' => $orderSampleItemNote->note
        ];
    }

    /**
     * @param OrderSampleItemNote $orderSampleItemNote
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(OrderSampleItemNote $orderSampleItemNote)
    {
        if(is_null($orderSampleItemNote->user)) {
            return $this->item([], function (){
                return [
                    'name' => 'Admin',
                ];
            });
        }

        return $this->item($orderSampleItemNote->user, function ($user) {
            return [
                'name' => $user->name,
            ];
        });
    }
}
