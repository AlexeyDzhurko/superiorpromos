<?php
/**
 * Created by PhpStorm.
 * User: dexm1
 * Date: 31.03.17
 * Time: 16:12
 */

namespace App\Admin\Transformers;


use App\Models\Stage;
use App\User;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class HistoryStageTransformer extends TransformerAbstract
{
    public function transform(Stage $stage)
    {
        return [
            'time' => $stage->pivot->created_at->format('M/d/Y H:i'),
            'user' => User::find($stage->pivot->user_id)->name,
            'name' => $stage->name,
        ];
    }

}
