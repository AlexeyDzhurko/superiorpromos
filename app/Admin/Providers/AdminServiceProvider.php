<?php

namespace App\Admin\Providers;

use App\Admin\Contracts\CreateProduct;
use App\Admin\Contracts\Sage;
use App\Admin\Http\Middleware\AdminAccessToSectionMiddleware;
use App\Admin\Http\Middleware\AdminMiddleware;
use App\Admin\Services\ProductService;
use App\Admin\Services\SageService;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @param Router $router
     */
    public function boot(Router $router)
    {
        $router->middleware('admin.user', AdminMiddleware::class);
        $router->middleware('admin.access', AdminAccessToSectionMiddleware::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Sage::class, SageService::class);
        $this->app->singleton(CreateProduct::class, ProductService::class);
    }
}
