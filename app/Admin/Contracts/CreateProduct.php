<?php

namespace App\Admin\Contracts;

/**
 * Interface Sage
 *
 * @package app\Admin\Contracts
 */
interface CreateProduct
{

    /**
     *Add new product
     * @param array $data
     */
    public function addProduct(array $data);
}