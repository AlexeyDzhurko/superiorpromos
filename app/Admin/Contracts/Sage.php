<?php

namespace App\Admin\Contracts;

/**
 * Interface Sage
 *
 * @package app\Admin\Contracts
 */
interface Sage
{
    /**
     * Get list of categories from sage
     * @return array
     */
    public function getCategoriesList():array;

    /**
     * Get list of suppliers from sage
     * @return array
     */
    public function getSuppliersList():array;

    /**
     * Get supplier's product list
     * @param \App\Admin\Http\Requests\SageSearchBySupplierRequest $request
     * @return array
     */
    public function getSuppliersProductList($request):array;

    /**
     * Get supplier's info from sage
     * @param string $supplier
     * @return array
     */
    public function getSupplierInfo(string $supplier):array;

    /**
     * Get products by search params from sage
     * @param \App\Admin\Http\Requests\SageQuickSearchRequest $request
     * @return array
     */
    public function ProductsSearch($request):array;

    /**
     * Get product detail by id from sage
     * @param string $product_id
     * @return array
     */
    public function getProductDetail(string $product_id):array;
}
