<?php

namespace App\Admin\Contracts;

use App\Admin\Exceptions\AdminEntityNotFoundException;

/**
 * Interface AdminResolver
 *
 * @package app\Admin\Contracts
 */
interface AdminResolver
{
    /**
     * return Service that used for CRUD implementation
     *
     * @param string $baseRouteName
     * @return AdminEntity
     * @throws AdminEntityNotFoundException
     */
    public function get(string $baseRouteName): AdminEntity;

    /**
     * return all URLs for Admin CRUDs
     *
     * @return array
     */
    public function getAllSlugs(): array;
}
