<?php

namespace App\Admin\Contracts;


interface AdminEntity
{
    /**
     * URL slug for specific Entity (e.g. /admin/{customer}/edit, /admin/{email-template}/ )
     *
     * @return string
     */
    public function getBaseRouteName(): string;

    /**
     * Eloquent Model for CRUD
     *
     * @return string
     */
    public function getModel(): string;

    /**
     * Display Name (Plural)
     *
     * @return string
     */
    public function getDisplayNamePlural(): string;

    /**
     * Display Name (Singular)
     *
     * @return string
     */
    public function getDisplayNameSingular(): string;

    /**
     * Icon CSS class
     *
     * @return string
     */
    public function getIcon(): string;

    /**
     * Fields configuration for showing
     *
     * @return array
     */
    public function showFields(): array ;

    /**
     * Field configuration for add/edit pages
     *
     * @return array
     */
    public function formFields(): array ;
}
