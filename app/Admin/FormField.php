<?php

namespace App\Admin;

/**
 * Represents simple field
 *
 * @package App\Admin
 */
class FormField
{
    const TYPE_TEXT = 'text';
    const TYPE_RICH_TEXTAREA = 'rich_text_box';
    const TYPE_TEXTAREA = 'text_area';
    const TYPE_SELECT = 'select_dropdown';
    const TYPE_PASSWORD = 'password';
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_CHECKBOX_LIST = 'checkbox_list';
    const TYPE_FILE = 'file';
    const TYPE_IMAGE = 'image';
    const TYPE_INTEGER = 'integer';
    const TYPE_LINK = 'link';
    const TYPE_BOOL = 'bool';
    const TYPE_COLLECTION = 'collection';

    public $field;
    public $displayName;
    public $type;
    public $options = [];

    public $required= true;
    public $link = '';

    /**
     * FormField constructor
     *
     * @param string $field field name from database
     * @param string $type field type
     * @param string|null $displayName human readable field name
     * @param bool $required
     * @param string $link
     */
    public function __construct(
        string $field,
        string $type = 'text',
        string $displayName = null,
        bool $required = true,
        string $link = ''
    )
    {
        $this->field = $field;
        $this->displayName = is_null($displayName) ? ucfirst($field) : $displayName;
        $this->type = $type;
        $this->required = $required;
        $this->link = $link;
    }

    /**
     * Default field value
     *
     * @param $value
     * @return FormField
     */
    public function setDefault($value): FormField
    {
        $this->options['default'] = $value;
        return $this;
    }

    /**
     * Set options for select box
     *
     * @param array $options
     * @return FormField
     */
    public function setOptions(array $options): FormField
    {
        $this->options['options'] = $options;
        return $this;
    }

    public function getDefault()
    {
        return $this->options['default'] ?? '';
    }

    public function getOptions()
    {
        return $this->options['options'] ?? [];
    }

}
