<?php

namespace App\Admin;

use App\Admin\Contracts\AdminEntity;
use Illuminate\Http\Request;
use Storage;
use Intervention\Image\Facades\Image as Image;

class FormFieldExtractor
{
    /**
     * Extract field value from request
     *
     * @param Request $request
     * @param AdminEntity $adminEntity
     * @param FormField $formField
     * @return array|int|null|string
     */
    public static function getValue(Request $request, AdminEntity $adminEntity, FormField $formField)
    {
        switch ($formField->type) {
            case FormField::TYPE_PASSWORD:
                $passField = $request->input($formField->field);
                if (!empty($passField)) {
                    return bcrypt($request->input($formField->field));
                }
                break;
            case FormField::TYPE_CHECKBOX:
                return is_null($request->input($formField->field)) ? 0 : 1;
            case FormField::TYPE_FILE:
                $file = $request->file($formField->field);
                $filename = str_random(20);

                $path = $adminEntity->getBaseRouteName() . '/' . date('F') . date('Y') . '/';

                $fullPath = $path . $filename . '.' . $file->getClientOriginalExtension();

                Storage::put(config('voyager.storage.subfolder') . $fullPath, (string)$file, 'public');

                return $fullPath;
            case FormField::TYPE_IMAGE:
                if ($request->hasFile($formField->field)) {
                    return self::processImage($request, $adminEntity, $formField);
                }
                break;
            default:
                return $request->input($formField->field);
        }
        return null;
    }

    /**
     * Resize and move image to admin media folder
     *
     * @param Request $request
     * @param AdminEntity $adminEntity
     * @param FormField $formField
     * @return string file path
     */
    protected static function processImage(Request $request, AdminEntity $adminEntity, FormField $formField): string
    {
        $file = $request->file($formField->field);
        $filename = str_random(20);

        $path = $adminEntity->getBaseRouteName() . '/' . date('F') . date('Y') . '/';
        $fullPath = $path . $filename . '.' . $file->getClientOriginalExtension();

        $options = json_decode($formField->getDefault());
        $resizeWidth = $options['resize']['width'] ?? 1800;
        $resizeHeight = $options['resize']['height'] ?? null;

        self::resizeImage($file, $fullPath, $resizeWidth, $resizeHeight);

        if (isset($options['thumbnails'])) {
            foreach ($options['thumbnails'] as $thumbnails) {
                $thumbnailFullPath = $path . $filename . '-' . $thumbnails['name'] . '.' . $file->getClientOriginalExtension();

                if (isset($thumbnails['scale'])) {
                    $scale = intval($thumbnails['scale']) / 100;
                    $thumbResizeWidth = ($resizeWidth) ? $resizeWidth * $scale : $resizeWidth;
                    $thumbResizeHeight = ($resizeHeight) ? $resizeHeight * $scale : $resizeHeight;
                    self::resizeImage($file, $thumbnailFullPath, $thumbResizeWidth, $thumbResizeHeight);
                } elseif (isset($thumbnails['crop']['width']) && isset($thumbnails['crop']['height'])) {
                    self::fitImage($file, $thumbnailFullPath, $thumbnails['crop']['width'], $thumbnails['crop']['height']);
                }
            }
        }
        return $fullPath;

    }

    /**
     * Resize uploaded image
     *
     * @param $file
     * @param $fullPath
     * @param $resizeWidth
     * @param $resizeHeight
     * @return bool
     */
    protected static function resizeImage($file, $fullPath, $resizeWidth, $resizeHeight): bool
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $image = Image::make($file)->resize($resizeWidth, $resizeHeight, function ($constraint) {
            /** @noinspection PhpUndefinedMethodInspection */
            $constraint->aspectRatio();
            /** @noinspection PhpUndefinedMethodInspection */
            $constraint->upsize();
        })->encode($file->getClientOriginalExtension(), 75);

        return Storage::put(config('voyager.storage.subfolder') . $fullPath, (string)$image, 'public');
    }

    /**
     * Scale uploaded image
     *
     * @param $file
     * @param $fullPath
     * @param $cropWidth
     * @param $cropHeight
     * @return bool
     */
    protected static function fitImage($file, $fullPath, $cropWidth, $cropHeight): bool
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $image = Image::make($file)->fit($cropWidth,
            $cropHeight)->encode($file->getClientOriginalExtension(), 75);

        return Storage::put(config('voyager.storage.subfolder') . $fullPath, (string)$image, 'public');
    }
}
