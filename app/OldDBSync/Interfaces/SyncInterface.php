<?php

namespace App\OldDBSync\Interfaces;

interface SyncInterface
{
    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity);

    public function handle();
}
