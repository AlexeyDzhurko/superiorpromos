<?php

namespace App\OldDBSync\Sync;

use DB;

abstract class AbstractSync
{
    protected $oldDbConnection;
    protected $newTableName;
    protected $model;

    abstract public function getOldRows($oldEntityId);
    abstract public function map($oldEntity);

    public function __construct()
    {
        $this->oldDbConnection = DB::connection('old_mysql');
    }

    public function sync($oldEntityId)
    {
        $oldRows = $this->map($this->getOldRows($oldEntityId));
        $checkExistEntity = $this->checkExistOldItemImport($oldRows['id']);
        if($checkExistEntity) {
            return $checkExistEntity->id;
        } else {
            $newEntity = new $this->model;
            $newEntity->fill($oldRows);
            $newEntity->save();
            return $newEntity->id;
        }
    }

    public function checkExistOldItemImport($oldEntityId)
    {
        return DB::table($this->newTableName)->where('id', $oldEntityId)->first();
    }
}
