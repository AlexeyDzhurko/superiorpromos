<?php

namespace App\OldDBSync;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;

class Sync
{
    /**
     * Tables names for import
     *
     * @var string $oldTableName
     * @var string $newTableName
     */
    public $oldTableName, $newTableName;

    public function __construct()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        if ($this->newTableName !== 'addresses') {
            DB::table($this->newTableName)->truncate();
        }

        \Log::useDailyFiles(storage_path() . '/logs/sync-db.log');
        $this->run();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    /**
     * Run synchronize process for entity
     */
    public function run()
    {
        $failRecords = 0;

        foreach ($this->getOldEntities() as $oldEntity) {
            if (!empty($entity = $this->map($oldEntity))) {
                /*if(key_exists('id', $entity)) {
                    if((DB::table($this->newTableName)->where('id', $entity['id'])->first()) != null) continue;
                }*/
                echo $this->getMessageForPrint($entity);
                $this->saveNewEntity($entity);
            } else {
                $failRecords++;
            }
        }
        $lastIdObject = DB::table($this->newTableName)->orderBy('id', 'desc')->first();
        if (!is_null($lastIdObject)) {
            DB::raw("ALTER TABLE $this->newTableName AUTO_INCREMENT=$lastIdObject->id;");
        }

        \Log::error("$this->newTableName Fail records: " . $failRecords);
    }

    /**
     * Get old entities for synchronize process
     *
     * @return array
     */
    public function getOldEntities(): array
    {
        return DB::connection('old_mysql')
            ->table($this->oldTableName)
            ->get()->toArray();
    }

    /**
     * Save new entity to new database
     *
     * @param Model $entity
     *
     * @return bool
     */
    public function saveNewEntity($entity): bool
    {
        try {
            if (is_array($entity)) {
                if (!empty($entity['id'])) {
                    $attributes = ['id' => $entity['id']];
                    return DB::table($this->newTableName)->updateOrInsert($attributes, $entity);
                } else {
                    return DB::table($this->newTableName)->insert($entity);
                }

            } else {
                return $entity->save();
            }
        } catch (QueryException $e) {
            \Log::error(sprintf('%s table: %d id: error %s',
                $this->newTableName,
                $entity['id'],
                $e->getMessage()
            ));
            \Log::info('Save Entity. Old ID: ' . $entity['id'] . 'Error: ' . $e->getMessage());
            print "\033[31m" . $e->getMessage() . "\033[0m";
            return false;
        }
    }

    /**
     * Return message for print in console
     *
     * @param array $entity
     *
     * @return string
     */
    public function getMessageForPrint($entity): string
    {
        $id = !empty($entity['id']) ? $entity['id'] : 'new record';

        return "imported entity from \"$this->oldTableName\" table with id {$id} to \"$this->newTableName\" table \n";
    }

    /**
     * Check exist entity by id
     *
     * @param $table
     * @param $entityId
     * @return integer|null
     */
    protected function existEntity($table, $entityId)
    {
        return DB::table($table)->where('id', $entityId)->exists();
    }

    /**
     * Delete old data in table before synchronize databases
     *
     * @return int
     */
    protected function deleteOldData()
    {
        return DB::table($this->newTableName)->delete();
    }
}
