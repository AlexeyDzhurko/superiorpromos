<?php


namespace App\OldDBSync\SyncClasses;


use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;

class SampleOrderItemTrackingInfo extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'sample_order_items_tracking';

    /**
     * @var string
     */
    public $newTableName = 'order_sample_tracking_info';

    /**
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity): array
    {
        if($this->existEntity($this->newTableName, $oldEntity->oi_tracking_id)) {
            return [];
        }

        if(!$this->existEntity('order_sample_items', $oldEntity->order_item_id)) {
            return [];
        }

        return [
            'id' => $oldEntity->oi_tracking_id,
            'tracking_shipping_date' => $oldEntity->date_shipped,
            'tracking_number' => $oldEntity->tracking_number,
            'tracking_shipping_company' => $oldEntity->shipping_company,
            'tracking_note' => $oldEntity->shipping_note,
            'tracking_url' => $oldEntity->company_url,
            'tracking_user_id' => 1,
            'order_sample_item_id' => $oldEntity->order_item_id
        ];
    }

    public function handle()
    {
        echo "Finished synchronize sample order item tracking info \n";
    }
}
