<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use App\Models\Product;

class ProductsPopularitySync extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'dt_product';

    /**
     * @var string
     */
    public $newTableName = 'products';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if(($product = Product::find($oldEntity->item_id)) != null) {
            $product->popularity = $oldEntity->popularity;
            $product->save();
            echo "Copy popularity for product with id $oldEntity->item_id \n";
        }

        return [];
    }

    public function handle()
    {
        echo "Finished synchronize popularity \n";
    }
}