<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Carbon\Carbon;
use DB;

class ProductVendor extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'vendors_prods';

    /**
     * @var string
     */
    public $newTableName = 'product_vendor';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if ($this->existEntity($this->newTableName, $oldEntity->vp_id)) {
            return [];
        }

        if (!$this->existEntity('products', $oldEntity->item_id)) {
            return [];
        }

        if (!$this->existEntity('vendors', $oldEntity->vendor_id)) {
            return [];
        }

        return [
            'vendor_id'  => $oldEntity->vendor_id,
            'product_id' => $oldEntity->item_id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'sku'        => $oldEntity->sku_vendor,
            'id'         => $oldEntity->vp_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize couple of products with vendors \n";
    }
}
