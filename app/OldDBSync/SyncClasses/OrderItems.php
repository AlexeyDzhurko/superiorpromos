<?php

namespace App\OldDBSync\SyncClasses;

use App\Models\OrderItem;
use App\Models\Vendor;
use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class OrderItems extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'order_items';

    /**
     * @var string
     */
    public $newTableName = 'order_items';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return Model|null
     */
    public function map($oldEntity)
    {
        if(!$this->existEntity('products', $oldEntity->item_id)) {
            return null;
        }

        if(!$this->existEntity('cart_items', $oldEntity->order_item_id)) {
            return null;
        }

        if(!$this->existEntity('orders', $oldEntity->order_id)) {
            return null;
        }

        if (!$this->existEntity('stages', $oldEntity->pstage_id)) {
            return [];
        }

        $model = OrderItem::findOrNew($oldEntity->order_item_id);

        $vendorId = null;
        if ($vendor = Vendor::find($oldEntity->vendor_id)) {
            $vendorId = $vendor->id;
        }

        $model->setRawAttributes([
            'product_id'                 => $oldEntity->item_id,
            'quantity'                   => $oldEntity->count,
            'price'                      => (float)number_format($oldEntity->cost * $oldEntity->count + $oldEntity->cost_setup, 2, '.', ''),
            'options_price'              => $oldEntity->cost_setup,
            'check_notes'                => $oldEntity->check_notes,
            'auto_remind'                => $oldEntity->auto_remind,
            'not_paid'                   => $oldEntity->not_paid,
            'art_file_name'              => $oldEntity->art_file_name,
            'art_file_path'              => $oldEntity->art_file_path,
            'po_number'                  => $oldEntity->po_number,
            'invoice_file'               => $oldEntity->invoice_file,
            'tax_exempt'                 => $oldEntity->tax_exempt,
            'estimation_zip'             => $oldEntity->estimate_zip,
            'estimation_shipping_method' => $oldEntity->shipping_est_method,
            'estimation_shipping_price'  => $oldEntity->shipping_est_cost,
            'own_shipping_type'          => $oldEntity->own_shipping_method,
            'own_account_number'         => $oldEntity->own_account_number,
            'own_shipping_system'        => $oldEntity->own_shipping_system,
            'shipping_method'            => $oldEntity->own_shipping_method ?? '',
            'vendor_id'                  => $vendorId,
            'received_date'              => $oldEntity->receive_date,
            'imprint_comment'            => $oldEntity->imprint_comments,
            'shipping_first_name'        => $oldEntity->shipp_fname,
            'shipping_middle_name'       => $oldEntity->shipp_mname,
            'shipping_last_name'         => $oldEntity->shipp_lname,
            'shipping_title'             => $oldEntity->shipp_title,
            'shipping_suffix'            => $oldEntity->shipp_suffix,
            'shipping_company_name'      => $oldEntity->shipp_company,
            'shipping_address_line_1'    => $oldEntity->shipp_address1,
            'shipping_address_line_2'    => $oldEntity->shipp_address2,
            'shipping_city'              => $oldEntity->shipp_city,
            'shipping_state'             => $oldEntity->shipp_state,
            'shipping_zip'               => $oldEntity->shipp_zip,
            'shipping_country'           => $oldEntity->shipp_country,
            'shipping_province'          => $oldEntity->shipp_ostate,
            'shipping_day_telephone'     => $oldEntity->shipp_dphone,
            //'shipping_ext' => $oldEntity->productId,
            //'shipping_fax' => $oldEntity->productId,
            'billing_first_name'         => $oldEntity->pay_fname,
            'billing_middle_name'        => $oldEntity->pay_mname,
            'billing_last_name'          => $oldEntity->pay_lname,
            'billing_title'              => $oldEntity->pay_title,
            'billing_suffix'             => $oldEntity->pay_suffix,
            'billing_company_name'       => $oldEntity->pay_company,
            'billing_address_line_1'     => $oldEntity->pay_address1,
            'billing_address_line_2'     => $oldEntity->pay_address2,
            'billing_city'               => $oldEntity->pay_city,
            'billing_state'              => $oldEntity->pay_state,
            'billing_zip'                => $oldEntity->pay_zip,
            'billing_country'            => $oldEntity->pay_country,
            'billing_province'           => $oldEntity->pay_ostate,
            'billing_day_telephone'      => $oldEntity->pay_dphone,
            //'billing_ext' => $oldEntity->productId,
            //'billing_fax' => $oldEntity->productId,
            'created_at'                 => Carbon::parse($oldEntity->receive_date),
            'order_id'                   => $oldEntity->order_id,
            //'tracking_shipping_date'     => $oldEntity->date_shipped,
            //'tracking_number'            => $oldEntity->tracking_number,
            //'tracking_shipping_company' => $oldEntity->productId,
            //'tracking_note' => $oldEntity->productId,
            //'tracking_user_id' => $oldEntity->productId,
            'stage_id'                   => $oldEntity->pstage_id,
            //'tracking_url' => $oldEntity->productId,
            'item_price'                 => $oldEntity->cost,
            'shipping_price'             => $oldEntity->shipping_cost,
            'cart_item_id'               => $oldEntity->order_item_id,
            'id'                         => $oldEntity->order_item_id,
        ]);

        return $model;
    }

    public function handle()
    {
        echo "Finished synchronize order items \n";
    }
}
