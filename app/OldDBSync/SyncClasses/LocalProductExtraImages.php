<?php


namespace App\OldDBSync\SyncClasses;

use App\Admin\Services\ProductManager;
use App\Models\Product;
use App\Models\ProductColor;
use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Storage;

class LocalProductExtraImages extends Sync implements SyncInterface
{

    /**
     * @var string
     */
    public $oldTableName = 'product_extra_images';

    /**
     * @var object
     */
    public $productManager;

    /**
     * @var string
     */
    public $newTableName = 'product_extra_images';

    public function __construct()
    {
        \Log::useDailyFiles(storage_path() . '/logs/sync-extra-color-product-images-log.log');
        $this->productManager = new ProductManager();
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if ($this->saveProductExtraImage($oldEntity)) {
            echo "Imported image for product with id $oldEntity->product_id \n";
        }
        else {
            echo "Error while uploading image for product with id $oldEntity->product_id \n";
        }
        return [];
    }

    public function handle()
    {
        echo "Finished synchronize extra and color images for products \n";
    }

    private function saveProductExtraImage($entity)
    {
        try {
            if (is_null($product = Product::find($entity->product_id))) {
                return false;
            }

            $filePath = '/' . $entity->product_id . '/' . $entity->image_src;

            if (!Storage::disk('old_image')->exists($filePath)) {
                \Log::info("Image doesn't found - " . $filePath);
                return false;
            }
            $file = Storage::disk('old_image')->get($filePath);

            if ($entity->pcolors_color_id == 0) {
                $this->productManager->createProductExtraImage($product, $file);
//                $product->image_src = $this->productManager::EXTRA_IMAGES_PATH . "{$extraImage->id}/270_270.jpg";
//                $product->save();
                return true;
            } else {
                if (($productColor = ProductColor::find($entity->pcolors_color_id)) != null) {
                    $this->productManager->createProductColorImage($productColor, $file);
                    return true;
                }
            }
            return true;
        } catch (\Exception $e) {
            \Log::info("Error while uploading image for ItemID - " . $entity->product_id . 'Error: ' . $e->getMessage());
            return false;
        }
    }
}
