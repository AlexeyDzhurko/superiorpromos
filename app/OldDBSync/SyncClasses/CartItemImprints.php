<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Carbon\Carbon;

class CartItemImprints extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'order_items_imprints';

    /**
     * @var string
     */
    public $newTableName = 'cart_item_imprints';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if($this->existEntity($this->newTableName, $oldEntity->order_items_imprint_id)) {
            return [];
        }

        if(!$this->existEntity('cart_items', $oldEntity->order_item_id)) {
            return [];
        }

        if(!$this->existEntity('imprints', $oldEntity->imprint_location_id)) {
            return [];
        }

        return [
            'cart_item_id'      => $oldEntity->order_item_id,
            'imprint_id'        => $oldEntity->imprint_location_id,
            'imprint_name'      => $oldEntity->location_name,
            'setup_price'       => (double)$oldEntity->location_setup,
            'item_price'        => (double)$oldEntity->location_additional,
            'color_setup_price' => (double)$oldEntity->location_colors_setup,
            'color_item_price'  => (double)$oldEntity->location_colors_additional,
            'id'                => $oldEntity->order_items_imprint_id,
            'created_at'        => Carbon::now(),
        ];
    }

    public function handle()
    {
        echo "Finished synchronize cart item imprints \n";
    }
}
