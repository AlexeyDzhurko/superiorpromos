<?php

namespace App\OldDBSync\SyncClasses;

use App\Models\Cart;
use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;

class Orders extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'orders';

    /**
     * @var string
     */
    public $newTableName = 'orders';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if($this->existEntity($this->newTableName, $oldEntity->order_id)) {
            return [];
        }

        try {
            $cart = Cart::findOrNew($oldEntity->order_id);
            $cart->user_id = $oldEntity->customer_id;
            $cart->save();
        } catch (\Exception $e) {
            return [];
        }

        return [
            'user_id'                 => $oldEntity->customer_id,
            'shipping_first_name'     => is_null($oldEntity->shipp_fname) ? 'none fname' : $oldEntity->shipp_fname,
            'shipping_last_name'      => is_null($oldEntity->shipp_lname) ? 'none lname' : $oldEntity->shipp_lname,
            'shipping_address_line_1' => is_null($oldEntity->shipp_address1) ? 'none address' : $oldEntity->shipp_address1,
            'shipping_city'           => is_null($oldEntity->shipp_city) ? 'none city' : $oldEntity->shipp_city,
            'shipping_state'          => is_null($oldEntity->shipp_state) ? 'none state' : $oldEntity->shipp_state,
            'shipping_zip'            => is_null($oldEntity->shipp_zip) ? 'none zip' : $oldEntity->shipp_zip,
            'shipping_country'        => is_null($oldEntity->shipp_country) ? 'none country' : $oldEntity->shipp_country,
            'shipping_day_telephone'  => is_null($oldEntity->shipp_dphone) ? 'none day_telephone' : $oldEntity->shipp_dphone,
            'billing_first_name'      => is_null($oldEntity->pay_fname) ? 'none billing_first_name' : $oldEntity->pay_fname,
            'billing_last_name'       => is_null($oldEntity->pay_lname) ? 'none billing_last_name' : $oldEntity->pay_lname,
            'billing_address_line_1'  => is_null($oldEntity->pay_address1) ? 'none billing_address_line_1' : $oldEntity->pay_address1,
            'billing_city'            => is_null($oldEntity->pay_city) ? 'none billing_city' : $oldEntity->pay_city,
            'billing_state'           => is_null($oldEntity->pay_state) ? 'none billing_state' : $oldEntity->pay_state,
            'billing_zip'             => is_null($oldEntity->pay_zip) ? 'none billing_zip' : $oldEntity->pay_zip,
            'billing_country'         => is_null($oldEntity->pay_country) ? 'none billing_country' : $oldEntity->pay_country,
            'billing_day_telephone'   => is_null($oldEntity->pay_dphone) ? 'none billing_day_telephone' : $oldEntity->pay_dphone,
            'price'                   => $oldEntity->amount,
            'total_price'             => $oldEntity->grandtotal,
            'discount_amount'         => $oldEntity->discount,
            'cart_id'                 => $cart->id,
            'created_at'              => $oldEntity->date,
            'id'                      => $oldEntity->order_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize orders \n";
    }
}
