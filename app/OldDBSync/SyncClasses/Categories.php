<?php

namespace App\OldDBSync\SyncClasses;

use App\Models\Category;
use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use DB;

class Categories extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'treeman';

    /**
     * @var string
     */
    public $newTableName = 'categories';

    public function __construct()
    {
        $this->run();
    }

    /**
     * Run synchronize process for entity
     */
    public function run()
    {
        $oldEntities = DB::connection('old_mysql')->select("SELECT * FROM treeman WHERE tree_id = 10 AND item_type = 0 ORDER BY pos");
        foreach ($oldEntities as $entity) {
            if(!empty($entity = $this->map($entity))) {
                echo $this->getMessageForPrint($entity);
                $this->saveNewEntity($entity);
            }
        }
        $lastId = DB::table($this->newTableName)->orderBy('id', 'desc')->first()->id + 1;
        DB::raw("ALTER TABLE {$this->newTableName} AUTO_INCREMENT=$lastId;");
    }

    /**
     * Save new entity to new database
     *
     * @param array $entity
     *
     * @return bool
     */
    public function saveNewEntity($entity) : bool
    {
        try {
            $category = Category::findOrNew($entity['id']);
            $category->setRawAttributes($entity);
            $category->save();

            /** if exist parent category */
            if (!is_null($entity['parent_id'])) {
                $root = Category::find($entity['parent_id']);
                $category->makeChildOf($root);
            }

            return true;
        } catch (\Exception $e) {
            print "\033[31m" . $e->getMessage() . "\033[0m";
            return false;
        }
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if ($this->existEntity('categories', $oldEntity->item_id)) {
            return [];
        }

        return [
            'id'               => $oldEntity->item_id,
            'parent_id'        => $oldEntity->parent_item_id == 0 ? null : $oldEntity->parent_item_id,
            'name'             => $oldEntity->item_name,
            'slug'             => mb_strtolower($oldEntity->url_name),
            'depth'            => $oldEntity->level,
            'meta_title'       => $oldEntity->metatitle,
            'meta_description' => $oldEntity->metadescription,
            'keywords'         => $oldEntity->keywords,
            'created_at'       => $oldEntity->cdate,
            'updated_at'       => $oldEntity->timestamp,
            'active'           => $oldEntity->active,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize categories \n";
    }
}
