<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use App\PaymentProfile;
use App\User;
use Baum\Extensions\Eloquent\Model;


class UserPaymentsInfo extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'cim_payment_profile';

    /**
     * @var string
     */
    public $newTableName = 'payment_profiles';

    /**
     * Run synchronize process for entity
     */
    public function run()
    {
        $oldEntities = $this->getOldEntities();

        foreach ($oldEntities as $oldEntity) {
            if($oldEntity->deleted == 0) {
                $user = User::find($oldEntity->customer_id);
                if ($user && $oldEntity->cim_profile_id == $user->authorize_net_id) {
                    $entity = $this->map($oldEntity);
                    echo $this->getMessageForPrint($entity);
                    $this->saveNewEntity($entity);
                } else {
                    \Log::error('user ' . $oldEntity->customer_id . ' :: ' .$oldEntity->cim_profile_id );
                }
            }
        }
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity)
    {
        if(!$this->existEntity('users', $oldEntity->customer_id)) {
            \Log::error('users' . $oldEntity->customer_id);
            return [];
        }

        return [
            'id'               => $oldEntity->cim_payment_profile_id,
            'user_id'          => $oldEntity->customer_id,
            'first_name'       => trim($oldEntity->fname),
            'last_name'        => trim($oldEntity->lname),
            'company'          => trim($oldEntity->company),
            'city'             => trim($oldEntity->city),
            'state'            => trim($oldEntity->state),
            'zip'              => trim($oldEntity->zip),
            'country'          => trim($oldEntity->country),
            'phone'            => trim($oldEntity->dphone),
            'phone_extension'  => trim($oldEntity->dphone_ext),
            'is_default'       => $oldEntity->is_default,
            'card_type'        => trim($oldEntity->cardtype),
            'expiration_month' => trim($oldEntity->expdate_mon),
            'expiration_year'  => trim( $oldEntity->expdate_year),
            'card_number'      => $this->convertCardNumber($oldEntity->acct),
            'card_holder'      => trim($oldEntity->card_holder),
            'address1'         => trim($oldEntity->address1),
            'address2'         => trim($oldEntity->address2)
        ];
    }

    public function handle()
    {
        echo "Finished synchronize user payments information \n";
    }

    private function convertCardNumber($cardNumber)
    {
        return '************' . substr($cardNumber, -4);
    }
}
