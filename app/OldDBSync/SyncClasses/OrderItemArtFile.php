<?php


namespace App\OldDBSync\SyncClasses;

use App\Models\ArtProof;
use App\Models\OrderItem;
use Storage;

class OrderItemArtFile
{
    /**
     * OrderItemArtFile constructor.
     */
    public function __construct()
    {
        \Log::useDailyFiles(storage_path() . '/logs/sync-order-item-art-file-log.log');
    }

    public function handle()
    {
        $lastId = 54250;
        $orderItems = OrderItem::where('art_file_path', '!=' , '')->where('id', '>', $lastId)->get();

        foreach ($orderItems as $orderItem) {
            try {
                $fileName = basename($orderItem->art_file_path);
                $fileUrl = 'https://www.superiorpromos.com/images/orders_art_images/' . $fileName . '?' . time();
                $path = ArtProof::CUSTOMER_ART_FILE_DIR . $orderItem->id;

                if(!Storage::disk('public')->exists($path)) {
                    Storage::disk('public')->makeDirectory($path);
                }

                $fullPath = $path . '/'  . $fileName;
                Storage::disk('public')->put($fullPath,  file_get_contents($fileUrl));
                $orderItem->art_file_name = $fileName;
                $orderItem->art_file_path = $fullPath;
                $orderItem->save();

                echo "synchronization art file for order item  \"$orderItem->id\" successfully done \n";
            }catch (\Exception $exception) {
                \Log::info($exception->getMessage());
                echo "error synchronization art file for order item  \"$orderItem->id\" successfully done \n";
                continue;
            }
        }
    }
}
