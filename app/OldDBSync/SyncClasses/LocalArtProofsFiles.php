<?php

namespace App\OldDBSync\SyncClasses;

use App\Models\ArtProof;
use App\Models\OrderItem;
use File;
use Illuminate\Support\Facades\Log;
use Storage;

class LocalArtProofsFiles
{
    /**
     * LocalArtProofsFiles constructor.
     */
    public function __construct()
    {
        \Log::useDailyFiles(storage_path() . '/logs/sync-art-proofs-log.log');
        $this->run();
    }

    /**
     * Run synchronize process for entity
     */
    public function run()
    {
        $entities = ArtProof::all();

        foreach ($entities as $entity) {
            if (!empty($filePath = $this->map($entity))) {
                try {
                    $fileName = basename($filePath);
                    $fullPathSource = Storage::disk('old_art_proofs')->getDriver()->getAdapter()->applyPathPrefix($filePath);
                    $destinationPath = 'art_files/' . $entity->order_item_id . '/' . $fileName;
                    $fullPathDestination = Storage::disk('public')->getDriver()->getAdapter()->applyPathPrefix($destinationPath);

                    if (!File::exists(dirname($fullPathDestination))) {
                        File::makeDirectory(dirname($fullPathDestination), 0775, true);
                    }

                    File::copy($fullPathSource, $fullPathDestination);

                    if ($entity->path !== $fileName) {
                        \Log::info("Old path {$entity->path} AND New path {$fileName} doesn't match");
                        continue;
                    }


                    echo "Imported art proof - {$entity->id} \n";
                } catch (\Exception $exception) {
                    \Log::info("Entity ID: {$entity->id} . Exception " . $exception->getMessage());
                    echo "Error while importing art proof \n";
                    continue;
                }
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function map($entity): string
    {
        if(empty($entity->path)) {
            \Log::info("Image path IS EMPTY in Art proof #" . $entity->id);
            return '';
        }

        if (is_null($orderItem = OrderItem::find($entity->order_item_id))) {
            \Log::info("Order Item #" . $entity->order_item_id . " doesn't found for Art proof #" . $entity->id);
            return '';
        }

        $filePath = $orderItem->order_id . '/' . basename($entity->path);

        if (!Storage::disk('old_art_proofs')->exists($filePath)) {
            \Log::info("Art Proof doesn't found - " . Storage::disk('old_art_proofs')->getAdapter()->getPathPrefix() . $filePath);
            return '';
        }

        return $filePath;
    }

    public function handle()
    {
        echo "Finished synchronize art proof files \n";
    }
}
