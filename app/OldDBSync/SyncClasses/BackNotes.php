<?php

namespace App\OldDBSync\SyncClasses;

use App\Models\Role;
use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Carbon\Carbon;

class BackNotes extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'order_items_backnotes';

    /**
     * @var string
     */
    public $newTableName = 'back_notes';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if(!$this->existEntity('order_items', $oldEntity->order_item_id)) {
            return [];
        }

        return [
            'order_item_id' => $oldEntity->order_item_id,
            'user_id'       => 1,
            'note'          => $oldEntity->note,
            'created_at'    => Carbon::parse($oldEntity->cdate),
            'updated_at'    => Carbon::parse($oldEntity->cdate),
            'id'            => $oldEntity->oi_bn_id,
        ];
    }

    /**
     * Run synchronize process for entity
     */
    public function run()
    {
        $failRecords = 0;

        $role = Role::where('name', '=', 'admin')->first();
        $admins = $role->users;

        foreach ($this->getOldEntities() as $oldEntity) {
            if(!empty($entity = $this->map($oldEntity))) {
                $admin = $admins->filter(function ($item) use ($oldEntity) {
                    return strtolower($item->name) == strtolower($oldEntity->admin_login);
                })->first();
                $adminId = $admin ? $admin->id : 1;

                $entity['user_id'] = $adminId;

                echo $this->getMessageForPrint($entity);
                $this->saveNewEntity($entity);
            }else {
                $failRecords++;
            }
        }
        $lastIdObject = \DB::table($this->newTableName)->orderBy('id', 'desc')->first();
        if(!is_null($lastIdObject)) {
            \DB::raw("ALTER TABLE $this->newTableName AUTO_INCREMENT=$lastIdObject->id;");
        }

        \Log::error("$this->newTableName Fail records: " . $failRecords);
    }

    public function handle()
    {
        echo "Finished synchronize back notes \n";
    }
}
