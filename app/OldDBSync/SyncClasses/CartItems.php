<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;

class CartItems extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'order_items';

    /**
     * @var string
     */
    public $newTableName = 'cart_items';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if(!$this->existEntity('products', $oldEntity->item_id)) {
            return [];
        }

        if(!$this->existEntity('carts', $oldEntity->order_id)) {
            return [];
        }

        return [
            'product_id'                 => $oldEntity->item_id,
            'quantity'                   => $oldEntity->count,
            'regular_price'              => $oldEntity->cost,
            'price'                      => $oldEntity->cost_total,
            'is_sale'                    => $oldEntity->item_status,
            'later_size_breakdown'       => $oldEntity->weight,
            'imprint_comment'            => $oldEntity->imprint_comments,
            'tax_exemption'              => $oldEntity->tax_exempt,
            'estimation_zip'             => $oldEntity->estimate_zip,
            'estimation_shipping_method' => $oldEntity->shipping_est_method,
            'estimation_shipping_code'   => $this->getShippingCode($oldEntity->shipping_company_id),
            'estimation_shipping_price'  => $oldEntity->shipping_est_cost,
            'own_account_number'         => $oldEntity->own_account_number,
            'own_shipping_system'        => $oldEntity->own_shipping_system,
            'shipping_method'            => $oldEntity->own_shipping_method ?? '',
            'received_date'              => $oldEntity->receive_date,
            'setup_price'                => $oldEntity->cost_setup,
            'cart_id'                    => $oldEntity->order_id,
            'created_at'                 => $oldEntity->receive_date,
            'id'                         => $oldEntity->order_item_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize cart items \n";
    }

    private function getShippingCode($id)
    {
        $companies=[
            1=>'UPS',
            2=>'USPS',
            3=>'FedEx',
            4=>'AirEx',
            5=>'DHL',
            6=>'Truck'
        ];

        return isset($companies[$id]) ? $companies[$id] : '';
    }
}
