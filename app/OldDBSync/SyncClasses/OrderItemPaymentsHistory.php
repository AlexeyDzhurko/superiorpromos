<?php


namespace App\OldDBSync\SyncClasses;


use App\Enums\PaymentStatusEnum;
use App\Enums\PaymentTypeEnum;
use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use App\PaymentProfile;
use Illuminate\Support\Facades\DB;

class OrderItemPaymentsHistory extends Sync implements SyncInterface
{
    /**
     * After sync, run ChangePaymentTableSeeder
     */

    /**
     * @var string
     */
    public $oldTableName = 'order_items_payment_history';

    /**
     * @var string
     */
    public $newTableName = 'payments';

    /**
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity): array
    {
        if(!$this->existEntity('order_items', $oldEntity->order_item_id)) {
            \Log::error('order_items ' .  $oldEntity->order_item_id);
            return [];
        }

        if(!$this->existEntity('payment_profiles', $oldEntity->cim_payment_profile_id)) {
            \Log::error('payment_profiles ' .  $oldEntity->cim_payment_profile_id);
            return [];
        }

        return [
            'id' => $oldEntity->oip_id,
            'order_item_id' => $oldEntity->order_item_id,
            'payment_profile_id' => $oldEntity->cim_payment_profile_id,
            'status' => $this->getStatus($oldEntity->status),
            'transaction_id' => $oldEntity->trx_id,
            'shipping_date' => $oldEntity->date_shipped,
            'type' => $this->getType($oldEntity->payment_type),
            'tax' => $oldEntity->tax,
            'shipping_company' => $oldEntity->smethod,
            'shipping_quantity' => $oldEntity->s_quantity,
            'over_quantity' => $oldEntity->o_quantity,
            'over_fees' => $oldEntity->o_fees,
            'price' => (float)$oldEntity->total_cost !== 0.0 ? $oldEntity->total_cost : $oldEntity->overall_totals,
            'credit_amount' => $oldEntity->credit_amount,
            'notify_customer' => $oldEntity->notify_customer,
            'notify_message' => $oldEntity->notify_message,
            'user_id' => 1,
            'created_at' => $oldEntity->payment_date
        ];
    }

    public function handle()
    {
        echo "Finished synchronize order item payments history information \n";
    }

    private function getStatus($status)
    {
        switch ($status) {
            case 'Processed':
                return PaymentStatusEnum::STATUS_PROCESSED;
            case 'Not Paid (Negative totals)':
                return PaymentStatusEnum::STATUS_NOT_PAID;
            default:
                return PaymentStatusEnum::STATUS_PAID;
        }
    }

    private function getType($type)
    {
        switch ($type) {
            case 'Billing (Shipping/Other)':
                return PaymentTypeEnum::OTHER_TYPE;
            case 'Billing (Shipping/Overrun)':
                return PaymentTypeEnum::OVERRUN_TYPE;
            case 'Credit':
                return PaymentTypeEnum::CREDIT_TYPE;
            default:
                return PaymentTypeEnum::INITIAL_TYPE;
        }
    }
}
