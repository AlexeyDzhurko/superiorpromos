<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use DB;

class ProductPrices extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'price_grid';

    /**
     * @var string
     */
    public $newTableName = 'product_prices';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if ($this->existEntity($this->newTableName, $oldEntity->price_grid_id)) {
            return [];
        }

        if (!$this->existEntity('products', $oldEntity->item_id)) {
            return [];
        }

        return [
            'product_id'      => $oldEntity->item_id,
            'quantity'        => $oldEntity->count_from,
            'setup_price'     => $oldEntity->price_setup,
            'item_price'      => $oldEntity->price,
            'sale_item_price' => $oldEntity->price_sale,
            'id'              => $oldEntity->price_grid_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize products prices \n";
    }
}
