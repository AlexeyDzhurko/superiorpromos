<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use DB;

class Colors extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'colors';

    /**
     * @var string
     */
    public $newTableName = 'colors';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if ($this->existEntity($this->newTableName, $oldEntity->color_id)) {
            return [];
        }

        return [
            'name'      => $oldEntity->color_name,
            'color_hex' => '#' . sprintf('%06x', $oldEntity->color_hex),
            'id'        => $oldEntity->color_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize colors \n";
    }
}
