<?php


namespace App\OldDBSync\SyncClasses;


use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use DB;

class SampleOrderItemStages extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'sample_order_items_stages';

    /**
     * @var string
     */
    public $newTableName = 'order_sample_item_stage';

    /**
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity): array
    {
        if($this->existEntity($this->newTableName, $oldEntity->order_items_stage_id)) {
            return [];
        }

        if(!$this->existEntity('order_sample_items', $oldEntity->order_item_id)) {
            return [];
        }

        $stage = DB::table('stages')->where('name', $oldEntity->new_pstage)->first();

        if(is_null($stage)) {
            return [];
        }

        return [
            'id' => $oldEntity->order_items_stage_id,
            'order_sample_item_id' => $oldEntity->order_item_id,
            'stage_id' => $stage->id,
            'user_id' => 1
        ];
    }

    public function handle()
    {
        echo "Finished synchronize sample order item stages \n";
    }
}
