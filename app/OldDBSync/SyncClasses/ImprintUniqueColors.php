<?php


namespace App\OldDBSync\SyncClasses;


use App\Models\ColorGroup;
use App\Models\Imprint;
use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Illuminate\Support\Facades\DB;

class ImprintUniqueColors extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'imprint_location_colors';

    /**
     * @var string
     */
    public $newTableName = 'imprint_colors';

    /**
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity): array
    {
        if($this->existEntity($this->newTableName, $oldEntity->imprint_location_colors_id)) {
            return [];
        }

        if(!$this->existEntity('imprints', $oldEntity->imprint_location_id)) {
            return [];
        }

        if(!$this->existEntity('colors', $oldEntity->color_id)) {
            return [];
        }

        return [
            'imprint_id' => $oldEntity->imprint_location_id,
            'color_id' => $oldEntity->color_id,
            'id' => $oldEntity->imprint_location_colors_id
        ];
    }

    public function handle()
    {
        echo "Finished synchronize imprints colors \n";
    }
}
