<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;

class Imprints extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'imprint_locations';

    /**
     * @var string
     */
    public $newTableName = 'imprints';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if($this->existEntity($this->newTableName, $oldEntity->imprint_location_id)) {
            return [];
        }

        if(!$this->existEntity('products', $oldEntity->item_id)) {
            return [];
        }

        if($oldEntity->color_group_id > 0 && !$this->existEntity('color_groups', $oldEntity->color_group_id)) {
            return [];
        }

        return [
            'product_id'     => $oldEntity->item_id,
            'position'       => $oldEntity->imprint_pos,
            'name'           => $oldEntity->location_name,
            'max_colors'     => $oldEntity->max_colors,
            'color_group_id' => $oldEntity->color_group_id > 0 ? $oldEntity->color_group_id : null,
            'id'             => $oldEntity->imprint_location_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize imprints \n";
    }
}
