<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Carbon\Carbon;

class CartItemProductOptions extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'order_items_options';

    /**
     * @var string
     */
    public $newTableName = 'cart_item_product_options';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if($this->existEntity($this->newTableName, $oldEntity->order_items_option_id)) {
            return [];
        }

        if(!$this->existEntity('cart_items', $oldEntity->order_item_id)) {
            return [];
        }

        if(!$this->existEntity('product_options', $oldEntity->option_id)) {
            return [];
        }

        return [
            'id'                => $oldEntity->order_items_option_id,
            'cart_item_id'      => $oldEntity->order_item_id,
            'product_option_id' => $oldEntity->option_id,
            'name'              => $oldEntity->option_name,
            'setup_price'       => $oldEntity->option_setup,
            'item_price'        => $oldEntity->option_cost,
            'created_at'        => Carbon::now(),
        ];
    }

    public function handle()
    {
        echo "Finished synchronize cart item product options \n";
    }
}
