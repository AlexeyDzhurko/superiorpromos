<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;

/**
 * Class Carts
 * @package App\OldDBSync\SyncClasses
 */
class Carts extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'cart';

    /**
     * @var string
     */
    public $newTableName = 'carts';

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity): array
    {
//        if ($this->existEntity($this->newTableName, $oldEntity->color_id)) {
//            return [];
//        }

        if (!$oldEntity->cart_item_id) {
            \Log::info('cart_item_id ' . $oldEntity->cart_item_id);
            return [];
        }

        return [
            'id' => $oldEntity->cart_item_id,
            'user_id' => $oldEntity->customer_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize carts \n";
    }
}
