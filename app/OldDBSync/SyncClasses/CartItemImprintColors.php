<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Carbon\Carbon;

class CartItemImprintColors extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'order_items_imprints_colors';

    /**
     * @var string
     */
    public $newTableName = 'cart_item_imprint_colors';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if($this->existEntity($this->newTableName, $oldEntity->order_items_imprints_color_id)) {
            return [];
        }

        if(!$this->existEntity('cart_item_imprints', $oldEntity->order_items_imprint_id)) {
            return [];
        }

        if(!$this->existEntity('colors', $oldEntity->color_id)) {
            return [];
        }

        return [
            'cart_item_imprint_id' => $oldEntity->order_items_imprint_id,
            'color_id'             => $oldEntity->color_id,
            'name'                 => $oldEntity->color_name,
            'id'                   => $oldEntity->order_items_imprints_color_id,
            'created_at'           => Carbon::now(),
        ];
    }

    public function handle()
    {
        echo "Finished synchronize cart item imprint colors \n";
    }
}
