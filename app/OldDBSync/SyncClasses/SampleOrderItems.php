<?php


namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use DB;

class SampleOrderItems extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'sample_order_items';

    /**
     * @var string
     */
    public $newTableName = 'order_sample_items';

    /**
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity): array
    {
        if ($this->existEntity($this->newTableName, $oldEntity->id)) {
            return [];
        }

        if (!$this->existEntity('products', $oldEntity->item_id)) {
            return [];
        }

        if (!$this->existEntity('order_sample', $oldEntity->sample_order_id)) {
            return [];
        }

        if (!is_null($oldEntity->pstage_id) && !$this->existEntity('stages', $oldEntity->pstage_id)) {
            return [];
        }

        return [
            'id' => $oldEntity->id,
            'product_id' => $oldEntity->item_id,
            'order_sample_id' => $oldEntity->sample_order_id,
            'stage_id' => $oldEntity->pstage_id,
            'check_notes' => $oldEntity->check_notes,
            'quantity' => json_decode($oldEntity->data, true)['quantity'],
            'price' => 0,
            'created_at' => DB::table('order_sample')->where('id', $oldEntity->sample_order_id)->first()->created_at
        ];
    }

    public function handle()
    {
        echo "Finished synchronize sample order items \n";
    }
}
