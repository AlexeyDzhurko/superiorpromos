<?php


namespace App\OldDBSync\SyncClasses;


use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;

class UserBillingAddresses extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'addressbook';

    /**
     * @var string
     */
    public $newTableName = 'addresses';

    /**
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity): array
    {
        if(!$this->existEntity('users', $oldEntity->customer_id)) {
            return [];
        }

        if($oldEntity->address_type == 1) {
            return [
                'user_id'        => $oldEntity->customer_id,
                'first_name'     => $oldEntity->fname,
                'middle_name'    => $oldEntity->mname,
                'last_name'      => $oldEntity->lname,
                'title'          => $oldEntity->title,
                'suffix'         => $oldEntity->suffix,
                'company_name'   => $oldEntity->company,
                'address_line_1' => $oldEntity->address1,
                'address_line_2' => $oldEntity->address2,
                'city'           => $oldEntity->city,
                'state'          => $oldEntity->state,
                'zip'            => $oldEntity->zip,
                'country'        => $oldEntity->country,
                'province'       => $oldEntity->ostate,
                'day_telephone'  => $oldEntity->dphone,
                'ext'            => $oldEntity->dphone_ext,
                'fax'            => $oldEntity->ephone,
                'is_po_box'      => 0,
                'type'           => 2,
            ];
        }

        return [];
    }

    public function handle()
    {
        echo "Finished synchronize users billing address \n";
    }
}
