<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Carbon\Carbon;

class CartItemProductSubOptions extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'order_items_suboptions';

    /**
     * @var string
     */
    public $newTableName = 'cart_item_product_sub_options';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if($this->existEntity($this->newTableName, $oldEntity->order_items_suboption_id)) {
            return [];
        }

        if(!$this->existEntity('cart_item_product_options', $oldEntity->order_items_option_id)) {
            return [];
        }

        if(!$this->existEntity('product_sub_options', $oldEntity->suboption_id)) {
            return [];
        }

        return [
            'id'                          => $oldEntity->order_items_suboption_id,
            'cart_item_product_option_id' => $oldEntity->order_items_option_id,
            'product_sub_option_id'       => $oldEntity->suboption_id,
            'name'                        => $oldEntity->suboption_name,
            'setup_price'                 => $oldEntity->suboption_setup,
            'item_price'                  => $oldEntity->suboption_cost,
            'created_at'                  => Carbon::now(),
        ];
    }

    public function handle()
    {
        echo "Finished synchronize cart items sub options \n";
    }
}
