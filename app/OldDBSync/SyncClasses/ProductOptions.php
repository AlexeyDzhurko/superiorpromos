<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use DB;

class ProductOptions extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'options';

    /**
     * @var string
     */
    public $newTableName = 'product_options';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if ($this->existEntity($this->newTableName, $oldEntity->option_id)) {
            return [];
        }

        if (!$this->existEntity('products', $oldEntity->item_id)) {
            return [];
        }

        return [
            'product_id' => $oldEntity->item_id,
            'name'       => $oldEntity->option_name,
            'position'   => $oldEntity->option_pos,
            'id'         => $oldEntity->option_id,
            'required'   => 1,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize products options \n";
    }
}
