<?php


namespace App\OldDBSync\SyncClasses;


use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;

class UserBillingAddressFromCustomer extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'customers';

    /**
     * @var string
     */
    public $newTableName = 'addresses';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity): array
    {
        if(!$this->existEntity('users', $oldEntity->customer_id)) {
            return [];
        }

        return [
            'user_id'        => $oldEntity->customer_id,
            'first_name'     => $oldEntity->fname,
            'middle_name'    => $oldEntity->mname,
            'last_name'      => $oldEntity->lname,
            'title'          => $oldEntity->title,
            'suffix'         => $oldEntity->suffix,
            'company_name'   => $oldEntity->company,
            'address_line_1' => $oldEntity->address1,
            'address_line_2' => $oldEntity->address2,
            'city'           => $oldEntity->city,
            'state'          => $oldEntity->state,
            'zip'            => $oldEntity->zip,
            'country'        => $oldEntity->country,
            'province'       => $oldEntity->ostate,
            'day_telephone'  => $oldEntity->dphone,
            'ext'            => $oldEntity->dphone_ext,
            'fax'            => $oldEntity->ephone,
            'is_po_box'      => $oldEntity->po_box,
            'type'           => 2,
            'is_default'     => 1,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize users billing address \n";
    }
}
