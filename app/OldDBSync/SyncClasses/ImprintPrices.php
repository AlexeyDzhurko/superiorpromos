<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;

class ImprintPrices extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'imprints_pg';

    /**
     * @var string
     */
    public $newTableName = 'imprint_prices';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if($this->existEntity($this->newTableName, $oldEntity->imprints_pg_id)) {
            return [];
        }

        if(!$this->existEntity('imprints', $oldEntity->imprint_location_id)) {
            return [];
        }

        return [
            'imprint_id'        => $oldEntity->imprint_location_id,
            'quantity'          => $oldEntity->count_from,
            'setup_price'       => $oldEntity->location_setup,
            'item_price'        => $oldEntity->location_additional,
            'color_setup_price' => $oldEntity->location_color_setup,
            'color_item_price'  => $oldEntity->location_color_additional,
            'id'                => $oldEntity->imprints_pg_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize imprints prices \n";
    }
}
