<?php

namespace App\OldDBSync\SyncClasses;

use App\Admin\Services\ProductManager;
use App\Models\Product;
use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Carbon\Carbon;
use DB;

class Products extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'dt_product';

    /**
     * @var string
     */
    public $newTableName = 'products';

    /**
     * @var object
     */
    public $productManager;

    public function __construct()
    {
        $this->productManager = new ProductManager();
        $this->run();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        foreach ($this->getOldEntities() as $entity) {
            if(!empty($entity = $this->map($entity))) {
                echo $this->getMessageForPrint($entity);
                $this->saveNewEntity($entity);
                // custom logic
                if(config('is_import_with_images')) {
                    try {
                        $product = Product::find($entity['id']);
                        $product->image_src = $this->saveImageForProduct($product);
                        $product->save();
                    } catch (\Exception $e) {
                        echo "Error while loading image! \n";
                    }
                }
            }
        }
        $lastId = DB::table($this->newTableName)->orderBy('id', 'desc')->first()->id + 1;
        DB::raw("ALTER TABLE $this->newTableName AUTO_INCREMENT=$lastId;");
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return Product|null
     */
    public function map($oldEntity)
    {
        if (DB::table('products')->where('sage_id', $oldEntity->sage_id)->first()) {
            $oldEntity->sage_id = null;
        }

        $extendedInfo = $this->getExtendedInfo($oldEntity->item_id);

        $createdDate = new Carbon();

        $model = Product::findOrNew($oldEntity->item_id);
        $model->setRawAttributes([
            'dimensions'                => $oldEntity->pdimensions,
            'imprint_area'              => $oldEntity->imprint_area,
            'description'               => $oldEntity->description,
            'production_time_from'      => $oldEntity->production_time_from,
            'box_weight'                => $oldEntity->boxweight,
            'production_time_to'        => $oldEntity->production_time_to,
            'on_sale'                   => $oldEntity->onsale,
            'custom_shipping_cost'      => $oldEntity->custom_shipping_cost,
            'zip_from'                  => $oldEntity->zip_from,
            'name'                      => $extendedInfo->item_name ?? 'New imported product without name with old id ' . $oldEntity->item_id,
            'active'                    => $extendedInfo->active ?? 1,
            'image_src'                 => $oldEntity->pimage_src,
            'sage_id'                   => empty($oldEntity->sage_id) ? $oldEntity->item_id : $oldEntity->sage_id,
            'vendor_id'                 => is_null($oldEntity->vendor_id) ? 1 : (!$this->existEntity('vendors', $oldEntity->vendor_id) ? 1 : $oldEntity->vendor_id),
            'url'                       => !empty($extendedInfo->url_name) ? mb_strtolower($extendedInfo->url_name) : null,
            'url_prefix'                => null,
            'keywords'                  => $extendedInfo->keywords ?? null,
            'meta_title'                => $extendedInfo->metatitle ?? null,
            'meta_description'          => $extendedInfo->metadescription ?? null,
            'quantity_per_box'          => $oldEntity->qty_per_box,
            'brand'                     => $oldEntity->brand,
            'gender'                    => $oldEntity->gender,
            'google_product_category'   => $oldEntity->google_product_category,
            'shipping_additional_type'  => $oldEntity->shipping_fee_type == 0 ? 1 : 2,
            'shipping_additional_value' => $oldEntity->shipping_fee_value,
            'pricing_information'       => $oldEntity->pricing_information,
            'image_alt'                 => $oldEntity->pimage_alt,
            'size_group_id'             => null,
            'custom_shipping_method'    => $this->getCustomShippingMethod($oldEntity->custom_shipping_method),
            'created_at'                => $createdDate,
            'id'                        => $oldEntity->item_id,
            'popularity'                => $oldEntity->popularity,
        ]);

        return $model;
    }

    /**
     * Get extended info about product
     *
     * @param $oldEntityId
     *
     * @return mixed
     */
    public function getExtendedInfo($oldEntityId)
    {
        return DB::connection('old_mysql')
            ->table('treeman')->where('item_id', $oldEntityId)->first();
    }

    public function saveImageForProduct($product)
    {
        $file = 'https://www.superiorpromos.com/img/ucart/images/pimage/' . $product->id . '/' . $product->image_src;
        $extraImage = $this->productManager->createProductExtraImage($product, $file);
        return "/uploads/products/extra/{$extraImage->id}/270_270.jpg";
    }

    public function handle()
    {
        echo "Finished synchronize products \n";
    }

    /**
     * @param $customShippingMethod
     * @return mixed|null
     */
    private function getCustomShippingMethod($customShippingMethod)
    {
        $customShippingMethods = [
          '0' => null,
          '1' => 'ground',
          '2' => '3day',
          '3' => '2day',
          '4' => 'overnight',
          '5' => 'overnight_am',
          '6' => 'overnight_saturday_delivery',
        ];

        return $customShippingMethods[$customShippingMethod] ?? null;
    }
}
