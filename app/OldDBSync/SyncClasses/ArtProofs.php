<?php

namespace App\OldDBSync\SyncClasses;

use App\Models\Role;
use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Carbon\Carbon;

class ArtProofs extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'order_items_artproofs';

    /**
     * @var string
     */
    public $newTableName = 'art_proofs';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Run synchronize process for entity
     */
    public function run()
    {
        $failRecords = 0;

        $role = Role::where('name', '=', 'admin')->first();
        $admins = $role->users;

        foreach ($this->getOldEntities() as $oldEntity) {
            if(!empty($entity = $this->map($oldEntity))) {
                $admin = $admins->filter(function ($item) use ($oldEntity) {
                    return strtolower($item->name) == strtolower($oldEntity->admin_login);
                })->first();
                $adminId = $admin ? $admin->id : 1;

                $entity['user_id'] = $adminId;

                echo $this->getMessageForPrint($entity);
                $this->saveNewEntity($entity);
            }else {
                $failRecords++;
            }
        }
        $lastIdObject = \DB::table($this->newTableName)->orderBy('id', 'desc')->first();
        if(!is_null($lastIdObject)) {
            \DB::raw("ALTER TABLE $this->newTableName AUTO_INCREMENT=$lastIdObject->id;");
        }

        \Log::error("$this->newTableName Fail records: " . $failRecords);
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if($this->existEntity($this->newTableName, $oldEntity->order_items_artproof_id)) {
            return [];
        }

        if(!$this->existEntity('order_items', $oldEntity->order_item_id)) {
            return [];
        }

        return [
            'order_item_id'    => $oldEntity->order_item_id,
            'customer_read'    => (integer)$oldEntity->read_by_customer,
            'admin_read'       => (integer)$oldEntity->read_by_admin,
            'answered_at'      => Carbon::parse($oldEntity->cdate_answer),
            'note'             => $oldEntity->admin_note,
            'customer_note'    => $oldEntity->clients_note,
            'approved'         => is_null($oldEntity->approved) ? null : (integer)$oldEntity->approved,
            'approve_required' => (integer)$oldEntity->require_approve,
            'path'             => $oldEntity->filename,
            'created_at'       => Carbon::parse($oldEntity->cdate),
            'updated_at'       => Carbon::parse($oldEntity->cdate),
            'id'               => $oldEntity->order_items_artproof_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize art proofs \n";
    }
}
