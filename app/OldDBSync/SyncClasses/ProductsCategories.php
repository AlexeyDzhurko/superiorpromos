<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use DB;

class ProductsCategories extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'treeman';

    /**
     * @var string
     */
    public $newTableName = 'products_categories';

    public function __construct()
    {
        $this->run();
    }

    /**
     * Run synchronize process for entity
     */
    public function run()
    {
        $oldEntities = DB::connection('old_mysql')->select("SELECT * FROM treeman WHERE item_type = 1;");
        foreach ($oldEntities as $entity) {
            if(!empty($entity = $this->map($entity))) {
                echo $this->getMessageForPrint($entity);
                $this->saveNewEntity($entity);
            }
        }
        $lastId = DB::table($this->newTableName)->orderBy('id', 'desc')->first()->id + 1;
        DB::raw("ALTER TABLE {$this->newTableName} AUTO_INCREMENT=$lastId;");
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if ($this->existEntity($this->newTableName, $oldEntity->item_id)) {
            return [];
        }

        if (!$this->existEntity('products', $oldEntity->item_id)) {
            return [];
        }

        if (!$this->existEntity('categories', $oldEntity->parent_item_id)) {
            return [];
        }

        return [
            'id'          => $oldEntity->item_id,
            'product_id'  => $oldEntity->item_id,
            'category_id' => $oldEntity->parent_item_id,
            'created_at'  => $oldEntity->cdate,
            'updated_at'  => $oldEntity->timestamp,
            'position'    => $oldEntity->pos,
            'type'        => 'main',
        ];
    }

    public function handle()
    {
        echo "Finished synchronize products with categories \n";
    }
}
