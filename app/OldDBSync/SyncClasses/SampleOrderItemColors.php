<?php


namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use DB;

class SampleOrderItemColors extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'sample_order_items';

    /**
     * @var string
     */
    public $newTableName = 'order_sample_item_colors';

    /**
     * Run synchronize process for entity
     */
    public function run()
    {
        foreach ($this->getOldEntities() as $entity) {
                $itemData = json_decode($entity->data, true);
                if(!empty($itemData) && !empty($itemData['colors'])) {
                    foreach ($itemData['colors'] as $color) {
                        $entity->product_color_id = $color['val'];
                        $entity->product_color_group_id = str_replace('color', '', $color['name']) ;

                        if(!empty($data = $this->map($entity))) {
                            echo $this->getMessageForPrint($data);
                            $this->saveNewEntity($data);
                        }
                    }
                }
        }

        $lastIdObject = DB::table($this->newTableName)->orderBy('id', 'desc')->first();
        if(!is_null($lastIdObject)) {
            DB::raw("ALTER TABLE $this->newTableName AUTO_INCREMENT=$lastIdObject->id;");
        }
    }

    /**
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity): array
    {
        if (!$this->existEntity('order_sample_items', $oldEntity->id)) {
            return [];
        }

        if(!$this->existEntity('product_colors', $oldEntity->product_color_id)) {
            return [];
        }

        if(!$this->existEntity('product_color_groups', $oldEntity->product_color_group_id)) {
            return [];
        }

        $id = 0;
        $lastIdObject = DB::table($this->newTableName)->orderBy('id', 'desc')->first();
        if(!is_null($lastIdObject)) {
            $id = $lastIdObject->id;
        }

        return [
            'id' => $id + 1,
            'order_sample_item_id' => $oldEntity->id,
            'color_group_id' => $oldEntity->product_color_group_id,
            'color_id'  => $oldEntity->product_color_id,
            'created_at' => DB::table('order_sample_items')->where('id', $oldEntity->id)->first()->created_at
        ];
    }

    public function handle()
    {
        echo "Finished synchronize sample order item colors \n";
    }
}
