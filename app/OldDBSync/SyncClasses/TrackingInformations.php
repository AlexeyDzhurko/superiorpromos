<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Carbon\Carbon;

class TrackingInformations extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'order_items_tracking';

    /**
     * @var string
     */
    public $newTableName = 'tracking_informations';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if($this->existEntity($this->newTableName, $oldEntity->oi_tracking_id)) {
            return [];
        }

        if(!$this->existEntity('order_items', $oldEntity->order_item_id)) {
            return [];
        }
        return [
            'id'                        => $oldEntity->oi_tracking_id,
            'tracking_shipping_date'    => Carbon::parse($oldEntity->date_shipped),
            'tracking_number'           => $oldEntity->tracking_number,
            'tracking_shipping_company' => $oldEntity->shipping_company,
            'tracking_note'             => $oldEntity->shipping_note,
            'tracking_url'              => $oldEntity->company_url,
            'tracking_user_id'          => 1,
            'order_item_id'             => $oldEntity->order_item_id,
            'created_at'                => Carbon::parse($oldEntity->ts),
        ];
    }

    public function handle()
    {
        echo "Finished synchronize tracking informations \n";
    }
}
