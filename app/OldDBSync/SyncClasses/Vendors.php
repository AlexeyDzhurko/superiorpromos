<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use DB;

class Vendors extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'dt_vendor';

    /**
     * @var string
     */
    public $newTableName = 'vendors';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if ($this->existEntity($this->newTableName, $oldEntity->item_id)) {
            return [];
        }

        return [
            'name'             => $oldEntity->vendor_name,
            'phone'            => $oldEntity->vendor_phone,
            'fax'              => $oldEntity->vendor_fax,
            'email'            => $oldEntity->vendor_email,
            'state'            => $oldEntity->vendor_state,
            'city'             => $oldEntity->vendor_city,
            'country'          => $oldEntity->vendor_country,
            'zip_code'         => $oldEntity->vendor_zipcode,
            'address_1'        => $oldEntity->vendor_address_1,
            'address_2'        => $oldEntity->vendor_address_2,
            'supplier_id'      => $oldEntity->SuppID,
            'site'             => $oldEntity->Web,
            'payment_template' => $oldEntity->payment_template,
            'id'               => $oldEntity->item_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize vendors \n";
    }
}
