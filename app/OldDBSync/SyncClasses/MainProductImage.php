<?php


namespace App\OldDBSync\SyncClasses;

use App\Admin\Services\ProductManager;
use App\Models\Product;
use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;

class MainProductImage extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'dt_product';

    /**
     * @var string
     */
    public $newTableName = 'products';

    /**
     * @var object
     */
    public $productManager;

    public function __construct()
    {
        \Log::useDailyFiles(storage_path() . '/logs/sync-main-product-images-log.log');
        $this->productManager = new ProductManager();
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity): array
    {
        if ($this->saveMainProductImage($oldEntity)) {
            echo "Imported main image for product with id $oldEntity->item_id \n";
        }
        else {
            echo "Error while uploading image for product with id $oldEntity->item_id \n";
        }
        return [];
    }

    public function handle()
    {
        echo "Finished synchronize main product image \n";
    }

    private function saveMainProductImage($entity)
    {
        try {
            if (is_null($product = Product::find($entity->item_id))) {
                return false;
            }

            $file = 'https://www.superiorpromos.com/img/ucart/images/pimage/' . $product->id . '/' . $product->image_src . '?' . time();
            $mainImage = $this->productManager->createProductExtraImage($product, $file);
            $product->image_src = $this->productManager::EXTRA_IMAGES_PATH . "{$mainImage->id}/270_270.jpg";
            $product->save();
            return true;

        } catch (\Exception $e) {
            return false;
        }
    }
}
