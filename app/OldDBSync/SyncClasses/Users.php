<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Baum\Extensions\Eloquent\Model;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Artisan;

class Users extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'customers';

    /**
     * @var string
     */
    public $newTableName = 'users';

    //TODO: update users
    public function __construct()
    {
        parent::__construct();
    }
//
//    public function run()
//    {
//        foreach ($this->getOldEntities() as $entity) {
//            if (!empty($entity = $this->map($entity))) {
//                if ($this->existEntity($this->newTableName, $entity['id']))) {
//                    DB::table($this->newTableName)->where('id', $entity['id'])->update($entity);
//                    echo 'update' . ' ' . $this->getMessageForPrint($entity);
//                    continue;
//                }
//                echo $this->getMessageForPrint($entity);
//                $this->saveNewEntity($entity);
//            }
//        }
//
//        $lastIdObject = DB::table($this->newTableName)->orderBy('id', 'desc')->first();
//        if(!is_null($lastIdObject)) {
//            DB::raw("ALTER TABLE $this->newTableName AUTO_INCREMENT=$lastIdObject->id;");
//        }
//    }

    public function run()
    {
        Artisan::call('db:seed', ['--class' => 'UsersTableSeeder']);

        parent::run(); // TODO: Change the autogenerated stub
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return Model|null
     */
    public function map($oldEntity)
    {
        $createdDate = new Carbon($oldEntity->register_date);

        return [
            'name'              => empty($oldEntity->contact_name) ? $oldEntity->fname . ' ' . $oldEntity->lname : $oldEntity->contact_name,
            'email'             => $oldEntity->login,
            'password'          => bcrypt($oldEntity->password),
            'contact_telephone' => $oldEntity->dphone,
            'authorize_net_id'  => $oldEntity->cim_profile_id,
            'ext'               => $oldEntity->title_ext,
            'active'            => 1,
            'can_delete'        => 0,
            'subscribe'         => $oldEntity->subscr,
            'tax_exempt'        => $oldEntity->tax_exempt,
            'created_at'        => $createdDate,
            'id'                => $oldEntity->customer_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize users \n";
    }

}
