<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use DB;

class ColorGroups extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'color_groups';

    /**
     * @var string
     */
    public $newTableName = 'color_groups';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if ($this->existEntity($this->newTableName, $oldEntity->color_group_id)) {
            return [];
        }

        return [
            'name'       => $oldEntity->color_group_name,
            'id'         => $oldEntity->color_group_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize colors groups \n";
    }
}
