<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Carbon\Carbon;
use DB;

class ColorColorGroup extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'color_group_colors';

    /**
     * @var string
     */
    public $newTableName = 'color_color_group';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if ($this->existEntity($this->newTableName, $oldEntity->color_group_color_id)) {
            return [];
        }

        if (!$this->existEntity('color_groups', $oldEntity->color_group_id)) {
            return [];
        }

        if (!$this->existEntity('colors', $oldEntity->color_id)) {
            return [];
        }

        return [
            'id'         => $oldEntity->color_group_color_id,
            'group_id'   => $oldEntity->color_group_id,
            'color_id'   => $oldEntity->color_id,
            'created_at' => Carbon::now(),
        ];
    }

    public function handle()
    {
        echo "Finished synchronize colors with colors group \n";
    }
}
