<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Carbon\Carbon;

class Stages extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'pstages';

    /**
     * @var string
     */
    public $newTableName = 'stages';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if($this->existEntity($this->newTableName, $oldEntity->pstage_id)) {
            return [];
        }

        return [
            'id'          => $oldEntity->pstage_id,
            'name'        => $oldEntity->stage_name,
            'description' => $oldEntity->stage_descr,
            'position'    => $oldEntity->pstage_pos,
            'created_at'  => Carbon::now(),
        ];
    }

    public function handle()
    {
        echo "Finished synchronize stages \n";
    }
}
