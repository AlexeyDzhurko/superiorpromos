<?php


namespace App\OldDBSync\SyncClasses;


use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Carbon\Carbon;

class SampleOrders extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'sample_orders';

    /**
     * @var string
     */
    public $newTableName = 'order_sample';

    /**
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity): array
    {
        if($this->existEntity($this->newTableName, $oldEntity->id)) {
            return [];
        }

        if(!$this->existEntity('users', $oldEntity->customer_id)) {
            return [];
        }

        return [
            'id' => $oldEntity->id,
            'user_id' => $oldEntity->customer_id,
            'own_shipping_type' => is_null($oldEntity->own_shipping_method) ? '' : $oldEntity->own_shipping_method,
            'own_account_number' => $oldEntity->own_account_number,
            'own_shipping_system' => $oldEntity->own_shipping_system,
            'shipping_method' => empty($oldEntity->own_account_number) ? 'us_shipping' : 'own_account',
            'quantity' => $oldEntity->quantity_to_purchase,
            'attraction' => json_encode(explode(',', $oldEntity->attracted)),
            'event_date' => $oldEntity->event_date,
            'frequency' => $oldEntity->times,
            'sample' => $oldEntity->decoration,
            'sample_comment' => trim($oldEntity->info),
            'upcoming_event' => $oldEntity->upcoming_event_list,
            'created_at' => Carbon::parse($oldEntity->date)
        ];
    }

    public function handle()
    {
        echo "Finished synchronize sample orders \n";
    }
}
