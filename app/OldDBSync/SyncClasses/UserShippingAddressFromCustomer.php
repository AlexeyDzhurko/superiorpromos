<?php


namespace App\OldDBSync\SyncClasses;


use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;

class UserShippingAddressFromCustomer extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'customers';

    /**
     * @var string
     */
    public $newTableName = 'addresses';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity): array
    {
        if(!$this->existEntity('users', $oldEntity->customer_id)) {
            return [];
        }

        return [
            'user_id'        => $oldEntity->customer_id,
            'first_name'     => $oldEntity->shipp_fname,
            'middle_name'    => $oldEntity->shipp_mname,
            'last_name'      => $oldEntity->shipp_lname,
            'title'          => $oldEntity->shipp_title,
            'suffix'         => $oldEntity->shipp_suffix,
            'company_name'   => $oldEntity->shipp_company,
            'address_line_1' => $oldEntity->shipp_address1,
            'address_line_2' => $oldEntity->shipp_address2,
            'city'           => $oldEntity->shipp_city,
            'state'          => $oldEntity->shipp_state,
            'zip'            => $oldEntity->shipp_zip,
            'country'        => $oldEntity->shipp_country,
            'province'       => $oldEntity->shipp_ostate,
            'day_telephone'  => $oldEntity->shipp_dphone,
            'ext'            => $oldEntity->shipp_dphone_ext,
            'fax'            => $oldEntity->shipp_ephone,
            'is_po_box'      => $oldEntity->po_box,
            'type'           => 1,
            'is_default'     => 1,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize users shipping address \n";
    }
}
