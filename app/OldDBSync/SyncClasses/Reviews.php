<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;

class Reviews extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'reviews';

    /**
     * @var string
     */
    public $newTableName = 'reviews';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if($this->existEntity($this->newTableName, $oldEntity->review_id)) {
            return [];
        }

        if(!$this->existEntity('users', $oldEntity->customer_id)) {
            return [];
        }

        if(!$this->existEntity('products', $oldEntity->item_id)) {
            return [];
        }

        return [
            'product_id'     => $oldEntity->item_id,
            'reviewer_name'  => $oldEntity->rv_name,
            'reviewer_email' => $oldEntity->rv_email,
            'text'           => $oldEntity->rv_text,
            'rating'         => $oldEntity->rv_rating,
            'id'             => $oldEntity->review_id,
            'approve'        => $oldEntity->approved,
            'user_id'        => $oldEntity->customer_id,
            'created_at'     => $oldEntity->rv_date,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize product reviews \n";
    }
}
