<?php

namespace App\OldDBSync\SyncClasses;

use App\Models\Product;
use App\Models\ProductColor;
use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use App\Admin\Services\ProductManager;

class ProductExtraImages extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'product_extra_images';

    /**
     * @var object
     */
    public $productManager;

    /**
     * @var string
     */
    public $newTableName = 'product_extra_images';

    public function __construct()
    {
        \Log::useDailyFiles(storage_path() . '/logs/sync-main-product-images-log.log');
        $this->productManager = new ProductManager();
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if($this->saveProductExtraImage($oldEntity)) {
            echo "Imported image for product with id $oldEntity->product_id \n";
        } else {
            echo "Error while uploading image for product with id $oldEntity->product_id \n";
        }

        return [];
    }

    public function handle()
    {
        echo "Finished synchronize extra and color images for products \n";
    }

    private function saveProductExtraImage($entity)
    {
        $file = 'https://www.superiorpromos.com/img/ucart/images/pimage/' . $entity->product_id . '/' . $entity->image_src . '?' . time();
        try {
            if (is_null($product = Product::find($entity->product_id))) {
                return false;
            }

            if($entity->pcolors_color_id == 0) {
                    $this->productManager->createProductExtraImage($product, $file);
//                    $product->image_src = "/uploads/products/extra/{$extraImage->id}/270_270.jpg";
//                    $product->save();
                    return true;
            } else {
                if(($productColor = ProductColor::find($entity->pcolors_color_id)) != null) {
                    $this->productManager->createProductColorImage($productColor, $file);
                    return true;
                } else {
                    return false;
                }
            }
        } catch (\Exception $e) {
            return false;
        }
    }
}
