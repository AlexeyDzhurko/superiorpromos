<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Carbon\Carbon;

class Wishes extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'wishlist';

    /**
     * @var string
     */
    public $newTableName = 'wishes';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if($this->existEntity($this->newTableName, $oldEntity->wl_id)) {
            return [];
        }

        if(!$this->existEntity('users', $oldEntity->customer_id)) {
            return [];
        }

        if(!$this->existEntity('products', $oldEntity->item_id)) {
            return [];
        }

        return [
            'user_id'    => $oldEntity->customer_id,
            'product_id' => $oldEntity->item_id,
            'created_at' => new Carbon($oldEntity->ts),
            'id'         => $oldEntity->wl_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize wishes \n";
    }
}
