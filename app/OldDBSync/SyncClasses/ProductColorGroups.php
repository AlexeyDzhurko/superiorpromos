<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use DB;

class ProductColorGroups extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'pcolors';

    /**
     * @var string
     */
    public $newTableName = 'product_color_groups';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if ($this->existEntity($this->newTableName, $oldEntity->pcolor_id)) {
            return [];
        }

        if (!$this->existEntity('products', $oldEntity->item_id)) {
            return [];
        }

        return [
            'name'       => $oldEntity->pcolor_name,
            'product_id' => $oldEntity->item_id,
            'id'         => $oldEntity->pcolor_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize product colors groups \n";
    }
}
