<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use DB;

class ProductSubOptionPrices extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'suboptions_pg';

    /**
     * @var string
     */
    public $newTableName = 'product_sub_option_prices';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if ($this->existEntity($this->newTableName, $oldEntity->suboption_pg_id)) {
            return [];
        }

        if (!$this->existEntity('product_sub_options', $oldEntity->suboption_id)) {
            return [];
        }

        return [
            'product_sub_option_id' => $oldEntity->suboption_id,
            'quantity'              => $oldEntity->count_from,
            'setup_price'           => $oldEntity->suboption_setup,
            'item_price'            => $oldEntity->suboption_cost,
            'id'                    => $oldEntity->suboption_pg_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize products sub options prices \n";
    }
}
