<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use DB;

class CartItemColors extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'order_items_colors';

    /**
     * @var string
     */
    public $newTableName = 'cart_item_colors';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if($this->existEntity($this->newTableName, $oldEntity->order_item_color_id)) {
            return [];
        }

        if(!$this->existEntity('cart_items', $oldEntity->order_item_id)) {
            return [];
        }

        return [
            'cart_item_id'     => $oldEntity->order_item_id,
            'color_group_id'   => DB::table('color_groups')->where('name', $oldEntity->pcolor_name)->first()->id ?? null,
            'color_group_name' => $oldEntity->pcolor_name,
            'color_id'         => DB::table('product_colors')->find($oldEntity->pcolors_color_id)->color_id ?? null,
            'color_name'       => $oldEntity->color_name,
            'id'               => $oldEntity->order_item_color_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize cart item colors \n";
    }
}
