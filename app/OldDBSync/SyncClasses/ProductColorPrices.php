<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Carbon\Carbon;
use DB;

class ProductColorPrices extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'pcolors_colors_pg';

    /**
     * @var string
     */
    public $newTableName = 'product_color_prices';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if($this->existEntity($this->newTableName, $oldEntity->pcolor_color_pg_id)) {
            return [];
        }

        if(!$this->existEntity('product_colors',$oldEntity->pcolors_color_id)){
            return [];
        }

        return [
            'product_color_id' => $oldEntity->pcolors_color_id,
            'quantity'         => $oldEntity->count_from,
            'setup_price'      => $oldEntity->pcolor_color_setup,
            'item_price'       => $oldEntity->pcolor_color_cost,
            'created_at'       => Carbon::now(),
            'id'               => $oldEntity->pcolor_color_pg_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize product colors prices \n";
    }
}
