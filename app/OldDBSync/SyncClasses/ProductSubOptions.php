<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use DB;

class ProductSubOptions extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'suboptions';

    /**
     * @var string
     */
    public $newTableName = 'product_sub_options';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if ($this->existEntity($this->newTableName, $oldEntity->suboption_id)) {
            return [];
        }

        if (!$this->existEntity('product_options', $oldEntity->option_id)) {
            return [];
        }

        return [
            'product_option_id' => $oldEntity->option_id,
            'name'              => $oldEntity->suboption_name,
            'position'          => $oldEntity->suboption_pos,
            'id'                => $oldEntity->suboption_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize products sub options \n";
    }
}
