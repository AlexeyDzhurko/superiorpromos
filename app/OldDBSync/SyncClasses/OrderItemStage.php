<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Carbon\Carbon;
use DB;

class OrderItemStage extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'order_items_stages';

    /**
     * @var string
     */
    public $newTableName = 'order_item_stage';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if($this->existEntity($this->newTableName, $oldEntity->order_items_stage_id)) {
            return [];
        }

        if(!$this->existEntity('order_items', $oldEntity->order_item_id)) {
            return [];
        }

        $stage = DB::table('stages')->where('name', $oldEntity->new_pstage)->first();

        if (is_null($stage)) {
            return [];
        }

        return [
            'order_item_id' => $oldEntity->order_item_id,
            'stage_id'      => $stage->id,
            'user_id'       => 1,
            'created_at'    => Carbon::parse($oldEntity->last_sent),
            'id'            => $oldEntity->order_items_stage_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize order item stages \n";
    }
}
