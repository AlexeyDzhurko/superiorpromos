<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use DB;

class ProductOptionPrices extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'options_pg';

    /**
     * @var string
     */
    public $newTableName = 'product_option_prices';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if ($this->existEntity($this->newTableName, $oldEntity->option_pg_id)) {
            return [];
        }

        if (!$this->existEntity('product_options', $oldEntity->option_id)) {
            return [];
        }

        return [
            'product_option_id' => $oldEntity->option_id,
            'quantity'          => $oldEntity->count_from,
            'setup_price'       => $oldEntity->option_setup,
            'item_price'        => $oldEntity->option_cost,
            'id'                => $oldEntity->option_pg_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize products option prices \n";
    }
}
