<?php

namespace App\OldDBSync\SyncClasses;

use App\OldDBSync\Interfaces\SyncInterface;
use App\OldDBSync\Sync;
use Carbon\Carbon;
use DB;

class ProductColors extends Sync implements SyncInterface
{
    /**
     * @var string
     */
    public $oldTableName = 'pcolors_colors';

    /**
     * @var string
     */
    public $newTableName = 'product_colors';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Mapping entity for new database rows format
     *
     * @param $oldEntity
     * @return array
     */
    public function map($oldEntity) : array
    {
        if ($this->existEntity($this->newTableName, $oldEntity->pcolors_color_id)) {
            return [];
        }

        if (!$this->existEntity('product_color_groups', $oldEntity->pcolor_id)) {
            return [];
        }

        if (!$this->existEntity('colors', $oldEntity->color_id)) {
            return [];
        }

        return [
            'product_color_group_id' => $oldEntity->pcolor_id,
            'color_id'               => $oldEntity->color_id,
            'created_at'             => Carbon::now(),
            'id'                     => $oldEntity->pcolors_color_id,
        ];
    }

    public function handle()
    {
        echo "Finished synchronize product colors \n";
    }
}
