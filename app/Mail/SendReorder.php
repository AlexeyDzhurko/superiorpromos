<?php

namespace App\Mail;

use App\Models\Address;
use App\Models\CartItem;
use App\Models\Color;
use App\Models\Imprint;
use App\Models\ProductColor;
use App\Models\ProductColorGroup;
use App\PaymentProfile;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReorder extends Mailable
{
    use Queueable, SerializesModels;

    public $orderItems;

    /**
     * Create a new message instance.
     *
     * @param array $orderItems
     * @return void
     */
    public function __construct(array $orderItems)
    {
        $this->mapOrderItemsData($orderItems);

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.superior_from_email_address'))->view('emails.reorder');
    }

    private function mapOrderItemsData(array $orderItems) : void
    {
        $newOrderItemsData = [];

        foreach ($orderItems as $orderItem) {

            $cartItem = CartItem::with('orderItem', 'product')->findOrFail($orderItem['order_item_id']);

            $extendedData = [
                'order_id' => $cartItem->orderItem->order->id ?? null,
                'order_item_id' => $cartItem->orderItem->id ?? null,
                'old_order_cost' => $cartItem->orderItem->price ?? null,
                'product_id' => $cartItem->product_id,
                'product_name' => $cartItem->product->name ?? null,
            ];

            if($orderItem['is_item_color_change']) {
                foreach ($orderItem['product_colors'] as &$colorData) {
                    $productColor = ProductColor::with('color')->find($colorData['color_id'])->toArray() ?? null;
                    $productColorGroup = ProductColorGroup::find($colorData['color_group_id'])->toArray() ?? null;

                    if(!is_null($productColor)) {
                        $colorData['color_name'] = $productColor['color']['name'];
                        $colorData['color_hex'] = $productColor['color']['color_hex'];
                    }

                    if(!is_null($productColorGroup)) {
                        $colorData['color_group_name'] = $productColorGroup['name'];
                    }
                }
            }

            if($orderItem['is_imprint_color_change']) {
                foreach ($orderItem['product_imprints'] as &$imprintData) {
                    $imprint = Imprint::find($imprintData['imprint_id'])->toArray() ?? null;
                    if(!is_null($imprint)) {
                        $imprintData['imprint_name'] = $imprint['name'];
                    }

                    if(!empty($imprintData['color_ids'])) {
                        foreach ($imprintData['color_ids'] as &$colorData) {
                            $color = Color::find($colorData)->toArray() ?? null;

                            if(!is_null($color)) {
                                $colorData = [];
                                $colorData['color_name'] = $color['name'];
                                $colorData['color_hex'] = $color['color_hex'];
                            }
                        }
                    }
                }
            }

            if(!$orderItem['is_shipping_same']) {
                $shippingAddress = Address::find($orderItem['shipping_address_id'])->toArray() ?? null;
                $extendedData['shipping_address'] = $shippingAddress;
            }

            if(!$orderItem['is_billing_same']) {
                $billingAddress = Address::find($orderItem['billing_address_id'])->toArray() ?? null;
                $extendedData['billing_address'] = $billingAddress;
            }

            if(!$orderItem['is_payment_same']) {
                $paymentAddress = PaymentProfile::find($orderItem['payment_profile_id'])->toArray() ?? null;
                $extendedData['payment_address'] = $paymentAddress;
            }

            $newOrderItemsData[] = array_merge($orderItem, $extendedData);
        }

        $this->orderItems = $newOrderItemsData;
    }
}
