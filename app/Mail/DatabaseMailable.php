<?php

namespace App\Mail;

use App\Models\EmailTemplate;
use App\User;
use Illuminate\Mail\Message;

/**
 * Email's presentation
 *
 * Class DatabaseMailable
 * @package App\Mail
 */
abstract class DatabaseMailable
{
    protected $from;
    protected $subject;
    protected $body;
    /** @var User */
    protected $user;

    /**
     * Return model for current email template
     *
     * @return EmailTemplate
     */
    abstract protected function getEmailTemplate(): EmailTemplate;

    /**
     * DatabaseMailable constructor.
     * @param User $user a user that should get email message
     */
    public function __construct(User $user)
    {
        $emailTemplate = $this->getEmailTemplate();

        $this->user = $user;
        $this->subject = $emailTemplate->subject;
        $this->body = $emailTemplate->body;
    }

    /**
     * Set required parameters for Swift Message
     *
     * @param Message $message
     */
    public function buildMessage(Message &$message)
    {
        $body = $this->body;

        $subject = $this->subject;

        foreach ($this->getVars() as $key => $value) {
            $body = str_replace("[[$key]]", $value, $body);
            $subject = str_replace("[[$key]]", $value, $subject);
        }

        $body = view('emails.template', ['content' => $body]);

        $message
            ->to($this->user->email)
            ->subject($subject)
            ->getSwiftMessage()->setBody($body, 'text/html');
    }

    /**
     * Return email's variables
     *
     * @return array
     */
    protected function getVars(): array
    {
        return [];
    }

}
