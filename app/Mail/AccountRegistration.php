<?php

namespace App\Mail;

use App\Models\EmailTemplate;

class AccountRegistration extends DatabaseMailable
{
    /** @inheritdoc */
    protected function getEmailTemplate(): EmailTemplate
    {
        return EmailTemplate::find(1);
    }

    /** @inheritdoc */
    protected function getVars(): array
    {
        return [
            'CUSTOMER_ID' => $this->user->id,
            'CUSTOMER_LOGIN' => $this->user->email,
            'CUSTOMER_NAME' => $this->user->first_name . ' ' . $this->user->last_name,
        ];
    }
}
