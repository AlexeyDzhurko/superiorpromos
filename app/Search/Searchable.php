<?php


namespace App\Search;


interface Searchable
{
    public function getElasticDocument(): array;
    public static function getElasticMapping(): array;
    public static function getIndexName(): string;
}
