<?php


namespace App\Search;


use App\Admin\Http\Controllers\BaseAdminController;
use Elasticsearch;
use Illuminate\Pagination\LengthAwarePaginator;

class SearchBuilder
{
    protected $query = [];
    protected $notQuery = [];
    protected $index;
    protected $type;
    protected $sort;

    private function __construct($index, $type)
    {
        $this->index = $index;
        $this->type = $type;
    }

    public static function createOrderQuery()
    {
        return new SearchBuilder('order', 'order');
    }

    public static function createBuilder($searchableClass)
    {
        return new SearchBuilder($searchableClass::getIndexName(), $searchableClass::getIndexName());
    }

    public function order($field, $direction = 'asc')
    {
        $this->sort = [
            $field => $direction,
        ];

        return $this;
    }

    public function sort($field, $direction = 'asc')
    {
        $this->sort = [
            'sort.' . $field => $direction,
        ];

        return $this;
    }

    public function match($fields, $query)
    {
        if (is_array($fields)){
            $this->query[] = [
                'multi_match' => [
                    'query' => $query,
                    'type' => 'cross_fields',
                    'fields' => $fields,
                ]
            ];
        } else {
            $this->query[] = [
                'match' => [
                    $fields => [
                        'query' => $query,
                        'operator' => 'and'
                    ]
                ]
            ];
        }

        return $this;
    }

    public function nestedMatch($field, array $nestedFields)
    {
        $conditions = [];
        foreach ($nestedFields as $nestedField => $value) {
            $conditions[] = ['match'=> [$field.'.'.$nestedField => $value]];
        }

        $this->query[] = ['nested' => [
            'path' => $field,
            'query' => [
                'bool' => [
                    'must' => $conditions
                ]
            ]
        ]];

        return $this;
    }

    public function matchPhrasePrefix($fields, $query)
    {
        $this->query[] = [
            'match_phrase_prefix' => [$fields => ["query" => $query,  "max_expansions" => 1000]]
        ];

        return $this;
    }

    public function matchPhrase($field, $query)
    {
        $this->query[] = [
            'match_phrase' => [$field => "*$query*"]
        ];

        return $this;
    }

    public function matchWildcard($field, array $queryWords)
    {
        $conditions = [];
        foreach ($queryWords as $queryWord) {
            $conditions[] = ['wildcard'=> [$field => mb_strtolower($queryWord) . '*']];
        }

        $this->query[] = $conditions;

        return $this;
    }

    public function term($field, $query)
    {
        $this->query[] = [
            'term' => [$field => $query]
        ];

        return $this;
    }

    public function terms($field, array $query)
    {
        $this->query[] = [
            'terms' => [$field => $query]
        ];

        return $this;
    }

    public function exists($field)
    {
        $this->query[] = [
            'exists' => [
                'field' => $field,
            ]
        ];
    }

    public function notExists($field)
    {
        $this->notQuery[] = [
            'exists' => [
                'field' => $field,
            ]
        ];
    }

    public function between($field, $start, $end)
    {
        $this->query[] = [
            'range' => [
                $field => [
                    'gte' => $start,
                    'lte' => $end,
                ]
            ]
        ];
        return $this;
    }

    public function nestedBetween($field, $start, $end, $nestedField)
    {
        $conditions = ['range' => [
            $field.'.'.$nestedField => [
                'gte' => $start,
                'lte' => $end,
            ]
        ]];

        $this->query[] = ['nested' => [
            'path' => $field,
            'query' => [
                'bool' => [
                    'must' => $conditions
                ]
            ]
        ]];

        return $this;
    }

    public function nestedMatchPhrasePrefix($field, array $nestedFields)
    {
        $conditions = [];
        foreach ($nestedFields as $nestedField => $value) {
            $conditions[] = ['match_phrase_prefix'=> [$field.'.'.$nestedField => $value]];
        }

        $this->query[] = ['nested' => [
            'path' => $field,
            'query' => [
                'bool' => [
                    'must' => $conditions
                ]
            ]
        ]];

        return $this;
    }

    public function nestedTerm($field, array $nestedFields)
    {
        $conditions = [];
        foreach ($nestedFields as $nestedField => $value) {
            $conditions[] = ['term'=> [$field.'.'.$nestedField => $value]];
        }

        $this->query[] = ['nested' => [
            'path' => $field,
            'query' => [
                'bool' => [
                    'must' => $conditions
                ]
            ]
        ]];

        return $this;
    }

    public function nestedTerms($field, array $nestedFields)
    {
        $conditions = [];
        foreach ($nestedFields as $nestedField => $value) {
            $conditions[] = ['terms'=> [$field.'.'.$nestedField => $value]];
        }

        $this->query[] = ['nested' => [
            'path' => $field,
            'query' => [
                'bool' => [
                    'must' => $conditions
                ]
            ]
        ]];

        return $this;
    }

    public function multiMatch ($query, $fields, $operator = 'or') {
        $this->query[] = [
            'multi_match' => [
                'query' => "$query",
//                'type' => 'phrase',
                'fields' => $fields,
                "operator" => $operator
            ]
        ];

        return $this;
    }

    public function matchSuggest($queryWords)
    {
        $this->query[] = $queryWords;
    }

    public function paginate($page = 1, $perPage = BaseAdminController::DEFAULT_DASHBOARD_PER_PAGE)
    {
        $searchParams = $this->buildSearchParams();

        $from = ($page - 1) * $perPage;

        $searchParams['size'] = $perPage;
        $searchParams['from'] = $from;

        $searchResult = Elasticsearch::search($searchParams);

        $paginator = new LengthAwarePaginator($searchResult['hits']['hits'], $searchResult['hits']['total'], $perPage, $page);

        $result = [];
        $result['data'] = array_pluck($searchResult['hits']['hits'], '_source');
        $result['meta']['pagination']['total'] = $paginator->total();
        $result['meta']['pagination']['total_pages'] = $paginator->lastPage();
        $result['meta']['pagination']['current_page'] = $paginator->currentPage();
        $result['meta']['pagination']['per_page'] = $paginator->perPage();

        return $result;
    }

    public function get()
    {
        $searchParams = $this->buildSearchParams();
        $searchResult = Elasticsearch::search($searchParams);

        return array_pluck($searchResult['hits']['hits'], '_source');
    }

    public function suggest()
    {
        $suggestParams = $this->buildSuggestParams();

        $suggestResult = Elasticsearch::suggest($suggestParams);

        return array_pluck(array_shift($suggestResult['search_suggest'])['options'], '_source');
    }

    public function getWithAggregations(array $aggregations)
    {
        $searchParams = $this->buildSearchParams();
        $searchParams['body']['aggs'] = $aggregations;

        $searchResult = Elasticsearch::search($searchParams);

        return $searchResult;
    }

    protected function buildSearchParams()
    {
        $searchParams = [
            'index' => $this->index,
            'type' => $this->type,
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => $this->query,
                    ]
                ]
            ]
        ];

        if ($this->sort) {
            $searchParams['body']['sort'] = [
                $this->sort,
                '_score',
            ];
        }

        if (!empty($this->notQuery)) {
            $searchParams['body']['query']['bool']['must_not'] = $this->notQuery;
        }

        return $searchParams;
    }

    protected function buildSuggestParams()
    {
        $suggestParams = [
            'index' => $this->index,
            'body' => [
                'search_suggest' => [
                    'text' => $this->query,
                    'completion' => [
                        'field' => 'suggest'
                    ]
                ]
            ]
        ];

        return $suggestParams;
    }
}
