<?php

namespace App\Enums;

class PaymentTypeEnum
{
    /**
     * @var string
     */
    public const CHECK_MONEY_ORDER = 'check';

    /**
     * @var string
     */
    public const APPLY_FOR_TERMS = 'apply';

    /**
     * @var string
     */
    public const CREDIT_CARD = 'card';

    /**
     * @var string
     */
    const INITIAL_TYPE = 'initial';

    /**
     * @var string
     */
    const OVERRUN_TYPE = 'overrun';

    /**
     * @var string
     */
    const OTHER_TYPE = 'other';

    /**
     * @var string
     */
    const CREDIT_TYPE = 'credit';

    /**
     * @return array
     */
    public static function toArray(): array
    {
        return [
            'CHECK_MONEY_ORDER' => self::CHECK_MONEY_ORDER,
            'APPLY_FOR_TERMS' => self::APPLY_FOR_TERMS,
            'CREDIT_CARD' => self::CREDIT_CARD,
        ];
    }
}
