<?php


namespace App\Enums;


class DiscountTypeEnum
{
    /**
     * @var string
     */
    public const DISPOSABLE = 'disposable';

    /**
     * @var string
     */
    public const REUSABLE = 'reusable';
}
