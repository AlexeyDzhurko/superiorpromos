<?php


namespace App\Enums;


class PaymentStatusEnum
{
    /**
     * @var string
     */
    const STATUS_PAID = 'paid';

    /**
     * @var string
     */
    const STATUS_PROCESSED = 'processed';

    /**
     * @var string
     */
    const STATUS_NOT_PAID = 'not_paid';

}
