<?php


namespace App\Notifications\BigMailer;


use App\Contracts\Notification;

class AdminNewPaymentProfileNotification extends BaseNotification implements Notification
{
    /**
     * @return \Illuminate\Foundation\Application|mixed|string
     */
    public function campaignId()
    {
        return config('bigmailer.campaign.admin_new_payment');
    }

    public function toBigMailer()
    {
        return [
            'CUSTOMER_ID' => $this->data['customer_id'] ?? '',
            'ACTION' => 'Card added',
            'CARD_HOLDER' => $this->data['card_holder'] ?? '',
            'ACCT' => $this->data['acct'] ?? '',
            'CREATED' => $this->data['created'] ?? '',
            'IP' => ''
        ];
    }
}
