<?php


namespace App\Notifications\BigMailer;


use App\Contracts\Notification;

class QuickQuoteNotification extends BaseNotification implements Notification
{
    /**
     * @return array
     */
    public function toBigMailer()
    {
        $emailData = [
            'ID'               => '',
            'TITLE'            => '',
            'DESCRIPTION'      => '',
            'IMAGE_LINK'       => '',
            'COLORS'           => 'Not present',
            'IMPRINTS'         => 'Not present',
            'OPTIONS'          => 'Not present',
            'SHIPPING'         => '',
            'QUANTITY'         => '',
            'BASE_UNIT_PRICE'  => '',
            'SHIPPING_PRICE'   => '',
            'FINAL_UNIT_PRICE' => '',
            'TOTAL'            => '',
            'PRICES_OPTIONS'   => '',
            'PRICES_COLORS'   => '',
            'PRICES_IMPRINTS'  => '',
            'PRICES_SETUP'     => '',
            'ENAME'            => '',
        ];

        if(empty($this->data)) {
            return $emailData;
        }

        $emailData['ID'] = '#Item ' . $this->data['product']['id'] ?? '';
        $emailData['TITLE'] = strip_tags($this->data['product']['title']) ?? '';
        $emailData['DESCRIPTION'] = strip_tags($this->data['product']['description'])?? '';
        $emailData['IMAGE_LINK'] = config('app.url') . '/' . $this->data['product']['img'] ?? '';

        if(!empty($this->data['colors'])) {
            $emailData['COLORS'] = $this->convertArrayToString($this->data['colors'], 'title', 'value') ?? '';
        }

        if(!empty($this->data['imprints'])) {
            $emailData['IMPRINTS'] = $this->convertArrayToString($this->data['imprints'], 'title', 'count_colors') ?? '';
        }

        if(!empty($this->data['options'])) {
            $emailData['OPTIONS'] = $this->convertArrayToString($this->data['options'], 'title', 'sub_option', 'title') ?? '';
        }

        if(!empty($this->data['shipping'])) {
            $emailData['SHIPPING'] = 'Service Type: ' . $this->data['shipping']['service_type'] . ' Cost: ' . $this->data['shipping']['cost'] . ' Time In Transit: ' . $this->data['shipping']['transit_time'] ?? '';
            $emailData['SHIPPING_PRICE'] = 'Shipping: ' . $this->data['shipping']['cost'] ?? '';
        }

        $emailData['QUANTITY'] = 'Quantity: ' . (string)$this->data['prices']['quantity'] ?? '';
        $emailData['BASE_UNIT_PRICE'] = 'Base Unit Price: $' . $this->data['prices']['price'] ?? '';
        $emailData['FINAL_UNIT_PRICE'] = 'Final Unit price: ' . $this->data['final_unit_price'] ?? '';
        $emailData['TOTAL'] = 'Total ' . $this->data['subtotal'] ?? '';

        if(!empty($this->data['prices_options'])) {
            $emailData['PRICES_OPTIONS'] = $this->convertArrayToString($this->data['prices_options'], 'title', 'value') ?? '';
        }

        if(!empty($this->data['prices_imprints'])) {
            $emailData['PRICES_IMPRINTS'] = $this->convertArrayToString($this->data['prices_imprints'], 'title', 'value') ?? '';
        }

        if(!empty($this->data['prices_setup'])) {
            $emailData['PRICES_SETUP'] = $this->convertArrayToString($this->data['prices_setup'], 'title', 'value') ?? '';
        }

        if(!empty($this->data['prices_colors'])) {
            $emailData['PRICES_COLORS'] = $this->convertArrayToString($this->data['prices_colors'], 'title', 'value') ?? '';
        }

        $emailData['ENAME'] = $this->data['user']['name'] ?? '';

        return $emailData;
    }

    /**
     * @return mixed
     */
    public function campaignId()
    {
        return config('bigmailer.campaign.quick_quote');
    }

    /**
     * @param array $data
     * @param $key
     * @param $valueKey
     * @param string $subKey
     * @param string $subValueKey
     * @return string
     */
    private function convertArrayToString(array $data, $key, $valueKey, $subKey = '',  $subValueKey = '') {
        $string = '';
        for ($i = 0; $i < count($data); $i++) {
            $subString = '';
            if(is_array($data[$i][$valueKey])) {
                for ($j = 0; $j < count($data[$i][$valueKey]); $j++) {
                    $subString .= $data[$i][$valueKey][$j][$subKey] . ' ';
                }
            }else{
                $subString = $data[$i][$valueKey];
            }
            $string .= $i+1 .') ' . $data[$i][$key] . ': ' . $subString . ' ';
        }

        return $string;
    }
}
