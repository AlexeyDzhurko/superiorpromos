<?php


namespace App\Notifications\BigMailer;


use App\Contracts\Notification;

class ExtraBillingConfirmationNotification extends BaseNotification implements Notification
{
    /**
     * @return \Illuminate\Foundation\Application|mixed|string
     */
    public function campaignId()
    {
        return config('bigmailer.campaign.extra_billing');
    }

    /**
     * @return array
     */
    public function toBigMailer()
    {
        return [
            'CUSTOMER_ID' => $this->data['customer_id'] ?? '',
            'CUSTOMER_NAME' => $this->data['customer_name'] ?? '',
            'CUSTOMER_LOGIN' => $this->data['customer_email'] ?? '',
            'ORDER_ITEM_ID' => $this->data['order_item_id'] ?? '',
            'ORDER_ITEM_NAME' => $this->data['order_item_name'] ?? '',
            'ORDER_ITEM_LINK' => config('app.url'),
            'ORDER_DATE' => $this->data['order_date'] ?? '',
            'PAYMENT_METHOD' => 'Credit cart ending in ' . $this->data['card_expire_date_year'] . '. Last 4 digits :' . substr($this->data['card_number'], -4) ?? '',
            'BILLING_ADDRESS' => $this->data['billing_address'] ?? '',
            'SHIPPING_ADDRESS' => $this->data['shipping_address'] ?? '',
            'PAYMENT_ADDRESS' => $this->data['payment_address'] ?? '',
            'NOTE' => htmlspecialchars($this->data['message']) ?? '',
            'CREDIT_AMOUNT' => $this->data['credit_amount'] ?? '',
            'SHIPPING_METHOD' => $this->data['shipping_method'] ?? '',
            'DATE_SHIPPED' => $this->data['shipping_date'] ?? '',
            'SHIPPING_QUANTITY' => $this->data['shipping_quantity'] ?? '',
            'OVERRUN_QUANTITY' => $this->data['overrun_quantity'] ?? '',
            'EXTRA_AMOUNT' => $this->data['extra_amount'] ?? '',
            'TOTALS' => $this->data['total'] ?? '',
        ];
    }
}
