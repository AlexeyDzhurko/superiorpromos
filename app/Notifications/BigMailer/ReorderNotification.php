<?php


namespace App\Notifications\BigMailer;


use App\Contracts\Notification;
use App\Models\Address;
use App\Models\CartItem;
use App\Models\Color;
use App\Models\Imprint;
use App\PaymentProfile;

class ReorderNotification extends BaseNotification implements Notification
{
    /**
     * @var boolean
     */
    private $isAdmin;

    /**
     * CouponCodeNotification constructor.
     * @param array $data
     * @param array $emails
     * @param bool $isAdmin
     */
    public function __construct(array $emails = [], array $data = [], bool $isAdmin = false)
    {
        $this->isAdmin = $isAdmin;
        parent::__construct($emails, $data);
    }

    /**
     * @return array
     */
    public function toBigMailer()
    {
        return $this->mapOrderItemsData($this->data);
    }

    /**
     * @return mixed
     */
    public function campaignId()
    {
        return config('bigmailer.campaign.reorder');
    }

    /**
     * @param array $orderItem
     * @return array
     */
    private function mapOrderItemsData(array $orderItem): array
    {
        $messageHeader = 'Thank you for your reorder request for the following item(s)
    If your order requires no actual order changes, we will setup the order for you and email you an order confirmation.
    If your reorder requires specific changes to the artwork, shipping, billing or we have any questions, a customer service representative will contact you within one business day to complete the order.
    Thank you again for your business we look forward to working with you on your promotion.
    Superior Promos Team.';
        $colors = '';
        $imprintColors = '';
        $imprints = '';

        $emailData = array(
            'CUSTOMER_LINK_NAME'    => config('app.url') . '/my-account',
            'CUSTOMER_NAME'         => auth()->user()->name,
            'CUSTOMER_ID'           => auth()->user()->id,
            'CLIENT_MESSAGE_HEADER' => $messageHeader,
            'SUB_ORDER_ID'          => '',
            'SUB_ORDER_LINK'        => config('app.url') . '/my-account/order-history',
            'PRODUCT_ID'            => '',
            'PRODUCT_NAME'          => '',
            'ANSWER_1'              => '',
            'ANSWER_1_1'            => '',
            'ANSWER_1_2'            => 'No',
            'ANSWER_1_3'            => 'No',
            'ANSWER_1_4'            => 'No',
            'ANSWER_2'              => '',
            'ANSWER_3'              => 'Yes',
            'ANSWER_4'              => 'Yes',
            'ANSWER_5'              => 'Yes',
            'COMMENT'               => '',
            'ORDER_COST'            => ''
        );

        $cartItem = CartItem::with('orderItem', 'product')->find($orderItem['order_item_id']);
        $emailData['SUB_ORDER_ID'] = $cartItem->orderItem->id ?? '';
        $emailData['PRODUCT_ID'] = $cartItem->product_id ?? '';
        $emailData['PRODUCT_NAME'] = strip_tags($cartItem->product->name) ?? '';

        $emailData['ANSWER_1'] = $orderItem['is_same_contents'] ? "Yes" : "No";
        $emailData['ANSWER_1_1'] = $orderItem['is_quantity_change'] ? "Yes. New quantity {$orderItem['quantity']}" : "No";

        if ($orderItem['is_item_color_change']) {
            foreach ($orderItem['product_colors'] ?? [] as $key => $colorData) {
                $colors .= $key + 1 . ')' . $colorData['color_group_name'] . ': ' . $colorData['color_name'] . '. ';
            }
            $emailData['ANSWER_1_2'] = "Yes. {$colors}";
        }

        if ($orderItem['is_imprint_color_change']) {
            foreach ($orderItem['product_imprints'] ?? [] as $key => $imprintData) {
                $imprint = Imprint::find($imprintData['imprint_id']) ?? null;
                if (is_null($imprint)) {
                    continue;
                }

                $imprintData['imprint_name'] = $imprint->name;
                if (!empty($imprintData['color_ids'])) {
                    foreach ($imprintData['color_ids'] ?? [] as $colorData) {
                        $color = Color::find($colorData) ?? null;
                        if (is_null($color)) {
                            continue;
                        }
                        $imprintColors .= $color->name . ', ';
                    }
                }

                $imprints .= $key + 1 . ')' . $imprintData['imprint_name'] . ': ' . $imprintColors . ' ';
            }

            $emailData['ANSWER_1_3'] = "Yes. {$imprints}";
        }

        $emailData['ANSWER_1_4'] = $orderItem['is_new_artwork_provide'] ? "Yes" : "No";
        $emailData['ANSWER_2'] = $orderItem['is_delivered_by_asap'] ? "ASAP" : "No";

        if (!$orderItem['is_shipping_same']) {
            $shippingAddress = Address::find($orderItem['shipping_address_id']);
            if (!is_null($shippingAddress)) {
                $firstName = "First Name: {$shippingAddress->first_name}";
                $lastName = "Last name: {$shippingAddress->last_name}";
                $title = "Title: {$shippingAddress->title}";
                $companyName = "Company name: {$shippingAddress->company_name}";
                $addrLine1 = "Adr. Line 1: {$shippingAddress->address_line_1}";
                $addrLine2 = "Adr. Line 1: {$shippingAddress->address_line_2}";
                $city = "City: {$shippingAddress->city}";
                $state = "State: {$shippingAddress->state}";
                $zip = "Zip: {$shippingAddress->zip} ";
                $phone = "Day telephone: {$shippingAddress->day_telephone}";
                $ext = "Ext: {$shippingAddress->ext}";
                $fax = "Fax: {$shippingAddress->fax}";
                $emailData['ANSWER_3'] = "No. " . $firstName . ', ' . $lastName . ', ' . $title . ', ' . $companyName . ', ' . $addrLine1 . ', ' . $addrLine2 . ', ' . $city . ', ' . $state . ', ' . $zip . ', ' . $phone . ', ' . $ext . ', ' . $fax;
            }
        }

        if (!$orderItem['is_billing_same']) {
            $billingAddress = Address::find($orderItem['billing_address_id']);
            if (!is_null($billingAddress)) {
                $firstName = "First Name: {$billingAddress->first_name}";
                $lastName = "Last name: {$billingAddress->last_name}";
                $title = "Title: {$billingAddress->title}";
                $companyName = "Company name: {$billingAddress->company_name}";
                $addrLine1 = "Adr. Line 1: {$billingAddress->address_line_1}";
                $addrLine2 = "Adr. Line 1: {$billingAddress->address_line_2}";
                $city = "City: {$billingAddress->city}";
                $state = "State: {$billingAddress->state}";
                $zip = "Zip: {$billingAddress->zip} ";
                $phone = "Day telephone: {$billingAddress->day_telephone}";
                $ext = "Ext: {$billingAddress->ext}";
                $fax = "Fax: {$billingAddress->fax}";
                $emailData['ANSWER_4'] = "No. " . $firstName . ', ' . $lastName . ', ' . $title . ', ' . $companyName . ', ' . $addrLine1 . ', ' . $addrLine2 . ', ' . $city . ', ' . $state . ', ' . $zip . ', ' . $phone . ', ' . $ext . ', ' . $fax;
            }
        }

        if (!$orderItem['is_payment_same']) {
            $paymentAddress = PaymentProfile::find($orderItem['payment_profile_id']);
            if (!is_null($paymentAddress)) {
                $firstName = "First Name: {$paymentAddress->first_name}";
                $lastName = "Last name: {$paymentAddress->last_name}";
                $companyName = "Company name: {$paymentAddress->company}";
                $addrLine1 = "Adr. Line 1: {$paymentAddress->address1}";
                $addrLine2 = "Adr. Line 1: {$paymentAddress->address2}";
                $city = "City: {$paymentAddress->city}";
                $state = "State: {$paymentAddress->state}";
                $zip = "Zip: {$paymentAddress->zip} ";
                $phone = "Phone: {$paymentAddress->phone}";
                $ext = "Ext: {$paymentAddress->phone_extension}";
                $cardNumber = "Card number:: {$paymentAddress->card_number}";
                $emailData['ANSWER_5'] = "No. " . $firstName . ', ' . $lastName . ', ' . $companyName . ', ' . $addrLine1 . ', ' . $addrLine2 . ', ' . $city . ', ' . $state . ', ' . $zip . ', ' . $phone . ', ' . $ext . ', ' . $cardNumber;
            }
        }

        if($this->isAdmin) {
            $emailData['CLIENT_MESSAGE_HEADER'] = '';
            $emailData['ORDER_COST'] = $cartItem->orderItem->price ?: '';
        }

        return $emailData;
    }
}
