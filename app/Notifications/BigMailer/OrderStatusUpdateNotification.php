<?php


namespace App\Notifications\BigMailer;


use App\Contracts\Notification;

class OrderStatusUpdateNotification extends BaseNotification implements Notification
{
    /**
     * @return \Illuminate\Foundation\Application|mixed|string
     */
    public function campaignId()
    {
        return config('bigmailer.campaign.order_status_update');
    }

    /**
     * @return array
     */
    public function toBigMailer()
    {
        return [
            'CUSTOMER_NAME' => $this->data['customer_name'] ?? '',
            'CUSTOMER_ID' => $this->data['customer_id'] ?? '',
            'CUSTOMER_LOGIN' => $this->data['customer_email'] ?? '',
            'ORDER_ITEM_ID' => $this->data['order_item_id'] ?? '',
            'ORDER_STAGE' => $this->data['order_stage'] ?? '',
        ];
    }
}
