<?php


namespace App\Notifications\BigMailer;


use App\Contracts\Notification;

class NewSampleOrderNotification extends BaseNotification implements Notification
{
    /**
     * @return \Illuminate\Foundation\Application|mixed|string
     */
    public function campaignId()
    {
        return config('bigmailer.campaign.new_sample_order');
    }

    /**
     * @return array
     */
    public function toBigMailer()
    {
        return [
            'CUSTOMER_NAME' => $this->data['customer_name'] ?? ''
        ];
    }
}
