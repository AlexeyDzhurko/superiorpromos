<?php


namespace App\Notifications\BigMailer;


use App\Contracts\Notification;

class NewArtProofNotification extends BaseNotification implements Notification
{
    /**
     * @return \Illuminate\Foundation\Application|mixed|string
     */
    public function campaignId()
    {
        return config('bigmailer.campaign.art_proof');
    }

    /**
     * @return array
     */
    public function toBigMailer()
    {
        return [
            'CUSTOMER_ID' => $this->data['customer_id'] ?? '',
            'CUSTOMER_NAME' => $this->data['customer_name'] ?? '',
            'ORDER_ITEM_ID' => $this->data['order_item_id'] ?? '',
        ];
    }
}
