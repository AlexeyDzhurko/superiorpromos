<?php


namespace App\Notifications\BigMailer;


use App\Contracts\Notification;

class PasswordResetNotification extends BaseNotification implements Notification
{
    /**
     * @return mixed
     */
    public function campaignId()
    {
        return config('bigmailer.campaign.reset_password');
    }

    /**
     * @return array
     */
    public function toBigMailer()
    {
        return [
            'CUSTOMER_NAME' => $this->data['name'] ?? '',
            'EMAIL'         => $this->data['email'] ?? '',
            'PASSWORD'      => $this->data['password'] ?? '',
            'LOGIN_LINK'    => config('app.url') ?? ''
        ];
    }
}
