<?php


namespace App\Notifications\BigMailer;


use App\Contracts\Notification;

class CouponCodeNotification extends BaseNotification implements Notification
{
    /**
     * @return mixed
     */
    public function campaignId()
    {
        return config('bigmailer.campaign.coupon');
    }

    /**
     * @return array
     */
    public function toBigMailer()
    {
        return [
            'COUPON_CODE' => $this->data['coupon_code'] ?? ''
        ];
    }
}
