<?php


namespace App\Notifications\BigMailer;


use App\Contracts\Notification;

class AdminOrderNotification extends BaseNotification implements Notification
{
    /**
     * @return \Illuminate\Foundation\Application|mixed|string
     */
    public function campaignId()
    {
        return config('bigmailer.campaign.admin_new_order');
    }

    /**
     * @return array
     */
    public function toBigMailer()
    {
        return [
          'CUSTOMER_ID' => $this->data['user_id'] ?? '',
          'MAIN_ORDER_ID' => $this->data['order_id'] ?? '',
          'SUBORDERS' => $this->data['sub_orders'] ?? '',
          'TOTAL' => $this->data['total'] ?? '',
          'ORDER_LINK' => config('app.url') . "/admin/order/{$this->data['order_id']}/edit"
        ];
    }
}
