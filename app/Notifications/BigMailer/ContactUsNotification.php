<?php


namespace App\Notifications\BigMailer;


use App\Contracts\Notification;

class ContactUsNotification extends BaseNotification implements Notification
{

    /**
     * @return \Illuminate\Foundation\Application|mixed|string
     */
    public function campaignId()
    {
        return config('bigmailer.campaign.contact_us');
    }

    /**
     * @return array
     */
    public function toBigMailer()
    {
        return [
            'EMAIL'=> $this->data['email'] ?? '-',
            'SERVER'=> $_SERVER['HTTP_HOST'],
            'NAME'=> $this->data['full_name'] ?? '-',
            'TOPIC'=> $this->data['topic'] ?? '-',
            'IP'=> $_SERVER['REMOTE_ADDR'],
            'MESSAGE'=> $this->data['message'] ?? '-'
        ];
    }
}
