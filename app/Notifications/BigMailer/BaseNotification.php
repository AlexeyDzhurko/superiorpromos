<?php


namespace App\Notifications\BigMailer;


abstract class BaseNotification
{
    /**
     * @var array
     */
    protected $emails;

    /**
     * @var array
     */
    protected $data;

    /**
     * CouponCodeNotification constructor.
     * @param array $emails
     * @param array $data
     */
    public function __construct(array $emails = [], array $data = [])
    {
        $this->emails = $emails;
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function emails()
    {
        return $this->emails;
    }
}
