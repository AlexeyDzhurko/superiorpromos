<?php


namespace App\Notifications\BigMailer;


use App\Contracts\Notification;

class AdminOrderUserNoteNotification extends BaseNotification implements Notification
{
    /**
     * @return \Illuminate\Foundation\Application|mixed|string
     */
    public function campaignId()
    {
        return config('bigmailer.campaign.admin_order_user_notes');
    }

    /**
     * @return array
     */
    public function toBigMailer()
    {
        return [
            'SUBORDER_ID'=>$this->data['sub_order_id'] ?? '',
            'NOTE_STATUS'=>$this->data['status'] ?? '',
            'CUSTOMER_LINK_NAME'=> config('app.url') . "/admin/customer/{$this->data['customer_id']}/edit",
            'CUSTOMER_NAME'=> $this->data['customer_name'] ?? '',
            'CUSTOMER_ID'=> $this->data['customer_id'] ?? '',
            'ORDER_ITEM_ID' => $this->data['order_item_id'] ?? '',
            'ORDER_ITEM_NAME' => $this->data['order_item_name'] ?? '',
            'ORDER_ITEM_LINK' => config('app.url') . "/admin/order_item/{$this->data['order_item_id']}/edit",
            'ADMIN_NOTE' => strip_tags($this->data['admin_note']) ?? '',
            'CLIENT_ANSWER' => strip_tags($this->data['customer_note']) ?? '',
            'IP' => $_SERVER['REMOTE_ADDR'] ?: ''
        ];
    }
}
