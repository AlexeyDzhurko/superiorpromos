<?php


namespace App\Notifications\BigMailer;


use App\Contracts\Notification;

class CustomerOrderNotification extends BaseNotification implements Notification
{
    /**
     * @return \Illuminate\Foundation\Application|mixed|string
     */
    public function campaignId()
    {
        return config('bigmailer.campaign.customer_new_order');
    }

    public function toBigMailer()
    {
        return [
            'CUSTOMER_ID' => $this->data['user_id'] ?? '',
            'CUSTOMER_NAME' => $this->data['user_name'] ?? '',
            'ORDER_DATE' => $this->data['order_date'] ?? '',
            'ORDER_ID' => $this->data['order_id'] ?? '',
            'PAYMENT_METHOD' => $this->data['payment_method'] ?? '',
            'BILLING_ADDRESS' => $this->data['billing_address'] ?? '',
            'SHIPPING_ADDRESS' => $this->data['shipping_address'] ?? '',
            'PAYMENT_ADDRESS' => $this->data['payment_address'] ?? '',
            'CUSTOMER_LOGIN' => $this->data['sub_orders'] ?? '',
            'PRODUCT_TOTAL' => $this->data['product_total'] ?? '',
            'DISCOUNT' => $this->data['discount'] ?? '',
            'SHIPPING_COST' => $this->data['shipping_cost'] ?? '',
            'BILLED_TOTAL' => $this->data['total'] ?? '',
            'ORDER_DETAILS_LINK' => config('app.url') . '/my-account/order-history'
        ];
    }
}
