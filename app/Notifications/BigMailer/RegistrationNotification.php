<?php


namespace App\Notifications\BigMailer;


use App\Contracts\Notification;

class RegistrationNotification extends BaseNotification implements Notification
{
    public function campaignId()
    {
        return config('bigmailer.campaign.registration');
    }

    public function toBigMailer()
    {
        return [
            'CUSTOMER_ID' => $this->data['id'] ?? '',
            'CUSTOMER_NAME' => $this->data['name'] ?? '',
            'CUSTOMER_LOGIN' => $this->data['email'] ?? '',
        ];
    }
}
