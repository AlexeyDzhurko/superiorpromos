<?php


namespace App\Notifications\BigMailer;


use App\Contracts\Notification;

class AdminUpdateAddressBookNotification extends BaseNotification implements Notification
{
    /**
     * @return \Illuminate\Foundation\Application|mixed|string
     */
    public function campaignId()
    {
        return config('bigmailer.campaign.admin_address_book');
    }

    /**
     * @return array
     */
    public function toBigMailer()
    {
        return [
            'CUSTOMER_ID'=> $this->data['customer_id'] ?? '',
            'CUSTOMER_LINK' => config('app.url') . "/admin/customer/{$this->data['customer_id']}/edit",
            'ADDRESS_TYPE' => $this->data['address_type'] ?? '',
            'SUBJECT' => 'Address updated',
            'FNAME' => $this->data['customer_first_name'] ?? '',
            'MNAME' => $this->data['customer_middle_name'] ?? '',
            'LNAME' => $this->data['customer_last_name'] ?? '',
            'COMPANY' => $this->data['company'] ?? '',
            'TITLE' => $this->data['title'] ?? '',
            'SUFFIX' => $this->data['suffix'] ?? '',
            'ADDRESS1' => $this->data['address1'] ?? '',
            'ADDRESS2' => $this->data['address2'] ?? '',
            'CITY' => $this->data['city'] ?? '',
            'STATE' => $this->data['state'] ?? '',
            'ZIP' => $this->data['zip'] ?? '',
            'COUNTRY' => $this->data['country'] ?? '',
            'EPHONE' => $this->data['phone'] ?? '',
            'IP' => '',
        ];
    }
}
