<?php


namespace App\Notifications\BigMailer;


use App\Contracts\Notification;

class TrackingInformationNotification extends BaseNotification implements Notification
{
    /**
     * @return \Illuminate\Foundation\Application|mixed|string
     */
    public function campaignId()
    {
        return config('bigmailer.campaign.tracking_info');
    }

    /**
     * @return array
     */
    public function toBigMailer()
    {
        return [
            'CUSTOMER_ID' => $this->data['customer_id'] ?? '',
            'CUSTOMER_NAME' => $this->data['customer_name'] ?? '',
            'ORDER_ITEM_ID' => $this->data['order_item_id'] ?? '',
            'SHIPPING_DATE' => $this->data['shipping_date'] ?? '',
            'CARRIER' => $this->data['carrier'] ?? '',
            'TRACKING_ID' => $this->data['tracking_id'] ?? '',
            'MESSAGE' => $this->data['note'] ?? '',
            'DISPLAY_RESELLER_IMAGE' => $this->data['display_reseller_image'] ?? '',
            'RESELLER_TEXT' => $this->data['reseller_text'] ?? '',
        ];
    }
}
