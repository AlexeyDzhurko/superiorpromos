<?php


namespace App\Notifications\BigMailer;


use App\Contracts\Notification;

class AdminDefaultPaymentProfileNotification extends BaseNotification implements Notification
{
    /**
     * @return \Illuminate\Foundation\Application|mixed|string
     */
    public function campaignId()
    {
        return config('bigmailer.campaign.admin_changed_payment');
    }

    /**
     * @return array
     */
    public function toBigMailer()
    {
        return [
            'CUSTOMER_ID' => $this->data['customer_id'] ?? '',
            'SUBJECT' => 'New default Payment profile set',
            'CARD_HOLDER' => $this->data['card_holder'] ?? '',
            'CARDTYPE' => $this->data['card_type'] ?? '',
            'ACCT' => $this->data['acct'] ?? '',
            'EXPDATE_MON' => $this->data['expire_mon'] ?? '',
            'EXPDATE_YEAR' => $this->data['expire_year'] ?? '',
            'FNAME' => $this->data['customer_first_name'] ?? '',
            'LNAME' => $this->data['customer_last_name'] ?? '',
            'COMPANY' => $this->data['company'] ?? '',
            'ADDRESS1' => $this->data['address'] ?? '',
            'CITY' => $this->data['city'] ?? '',
            'STATE' => $this->data['state'] ?? '',
            'ZIP' => $this->data['zip'] ?? '',
            'COUNTRY' => $this->data['country'] ?? '',
            'DPHONE' => $this->data['phone'] ?? '',
            'DPHONE_EXT' => $this->data['ext'] ?? '',
            'IP' => ''
        ];
    }
}
