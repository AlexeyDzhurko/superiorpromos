<?php


namespace App\Notifications\BigMailer;


use App\Contracts\Notification;

class OrderFeedbackNotification extends BaseNotification implements Notification
{
    /**
     * @return \Illuminate\Foundation\Application|mixed|string
     */
    public function campaignId()
    {
        return  config('bigmailer.campaign.order_feedback');
    }

    /**
     * @return array
     */
    public function toBigMailer()
    {
        return [
            'CUSTOMER_NAME' => $this->data['customer_name'] ?? '',
            'ORDER_ITEM_ID' => $this->data['order_item_id'] ?? ''
        ];
    }
}
