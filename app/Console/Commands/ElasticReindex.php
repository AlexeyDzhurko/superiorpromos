<?php

namespace App\Console\Commands;

use App\Search\Searchable;
use Illuminate\Console\Command;
use Elasticsearch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Filesystem\ClassFinder;

class ElasticReindex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:elastic:reindex';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $finder = new ClassFinder();
        $classes = $finder->findClasses(app_path('Models'));
        array_push($classes, 'App\User');

        foreach ($classes as $class) {
            $rClass = new \ReflectionClass($class);
            try{
                $this->indexClass($rClass, $class);
            }catch (\Exception $exception) {
                $this->error($exception->getMessage());
                continue;
            }
        }
        return;
    }

    protected function addToIndex(Searchable $document)
    {
        $data = [
            'body' => $document->getElasticDocument(),
            'index' => $document::getIndexName(),
            'type' => $document::getIndexName(),
            'id' => $document->id,
        ];

        return Elasticsearch::index($data);
    }

    /**
     * @param \ReflectionClass $rClass
     * @param Searchable $class
     */
    private function indexClass($rClass, $class): void
    {
        if ($rClass->implementsInterface(Searchable::class) && $rClass->isSubclassOf(Model::class)) {
            /** @var Searchable $class */
            $this->info('Creating index for: ' . $rClass->name);

            if (Elasticsearch::indices()->exists(['index' => $class::getIndexName()])) {
                Elasticsearch::indices()->delete(['index' => $class::getIndexName()]);
            }

            Elasticsearch::indices()->create([
                'index' => $class::getIndexName(),
                'body' => [
                    'mappings' => [
                        $class::getIndexName() => [
                            'properties' => $class::getElasticMapping(),
                        ]
                    ],
                    'settings' => [
                        'analysis' => [
                            'analyzer' => [
                                'autocomplete' => [
                                    'tokenizer' => 'standard',
                                    'filter' => ['standard', 'lowercase', 'stop', 'porter_stem', 'autocomplete']
                                ],
                                'autocomplete_search' => [
                                    'tokenizer' => 'standard',
                                    'filter' => ['standard', 'lowercase', 'stop', 'porter_stem']
                                ]
                            ],
                            'filter' => [
                                'autocomplete' => [
                                    'type' => 'edge_ngram',
                                    'min_gram' => 2,
                                    'max_gram' => 20
                                ]
                            ],
//                                'tokenizer' => [
//                                    'autocomplete' => [
//                                        'type' => 'ngram',
//                                        'min_gram' => 2,
//                                        'max_gram' => 15,
//                                        'token_chars' => ['letter', 'digit', 'symbol', 'punctuation']
//                                    ]
//                                ]
                        ]
                    ]
                ]
            ]);

            foreach ($class::all() as $document) {
                $this->info('Adding document ' . $document->id . ' to: ' . $class::getIndexName());
                $this->addToIndex($document);
            }
        }
    }
}
