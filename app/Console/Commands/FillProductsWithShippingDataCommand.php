<?php

namespace App\Console\Commands;

use App;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class FillProductsWithShippingDataCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:fill:products-shipping-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill empty or nulled shipping data (box weight and quantity per box) by selected values';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (App::environment('local')) {
            $boxWeight = $this->ask('Please insert default box_weight value for filling');
            $quantityPerBox = $this->ask('Please insert default quantity_per_box value for filling');

            $validator = Validator::make([
                'box_weight' => $boxWeight,
                'quantity_per_box' => $quantityPerBox,
            ], [
                'box_weight' => ['required', 'between:0.01,999999.99'],
                'quantity_per_box' => ['required', 'integer'],
            ]);

            if ($validator->fails()) {
                $this->info('Filling not started. See error messages below:');

                foreach ($validator->errors()->all() as $error) {
                    $this->error($error);
                }

                return 1;
            }

            $this->info("Start filling products table with inserted values (box_weight = $boxWeight, quantity_per_box = $quantityPerBox)");
            $boxWeightAffectedValuesCount = DB::update("UPDATE project.products SET project.products.box_weight = {$boxWeight} WHERE project.products.box_weight IS NULL OR project.products.box_weight = 0");
            $quantityPerBoxAffectedValuesCount = DB::update("UPDATE project.products SET project.products.quantity_per_box = {$quantityPerBox} WHERE project.products.quantity_per_box IS NULL OR project.products.quantity_per_box = 0");
            $this->info('Updated '. $boxWeightAffectedValuesCount . ' box_weight values');
            $this->info('Updated '. $quantityPerBoxAffectedValuesCount . ' quantity_per_box values');
        }
    }
}
