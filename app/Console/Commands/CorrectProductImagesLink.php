<?php

namespace App\Console\Commands;

use App\Admin\Services\ProductManager;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductExtraImage;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Image;
use Intervention\Image\Image as InterventionImage;

/**
 * Class CorrectProductImagesLink
 * @package App\Console\Commands
 */
class CorrectProductImagesLink extends Command
{
    const IMAGES_PATH = '/uploads/products/';
    const EXTRA_IMAGES_PATH = 'extra/';
    const COLOR_IMAGES_PATH = 'colors/';
//    const EXTRA_COLOR_IMAGES_PATH = '/uploads/products//';
    const IMAGE_THUMBNAILS_SIZES = [
        64,
        77,
        90,
        110,
        270,
    ];

    /**
     * @var ProductManager
     */
    public $productManager;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'correct-product-images-link';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->productManager = new ProductManager();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $wrongImages = json_decode(file_get_contents(storage_path('/app/images.json')));

        foreach ($wrongImages as $image) {
            try {
                $imgLink = sprintf('http://new.superiorpromos.com/img/ucart/images/pimage/%s/%s',
                    $image->id,
                    $image->image_src);

                $product = Product::find($image->id);

                $productExtraImage = new ProductExtraImage();
                $productExtraImage->product()->associate($product);
                $productExtraImage->save();

                $dir = sprintf('%s%s%s/%s%s',
                    public_path(),
                    self::IMAGES_PATH,
                    $product->id,
                    self::EXTRA_IMAGES_PATH,
                    $productExtraImage->id
                );

                if (!file_exists($dir)) {
                    mkdir($dir, 0777, true);
                }

                $imgPath = $dir . '/original.jpg';
                Image::make($imgLink)->save($imgPath);

                $product->image_src = sprintf(
                    '/uploads/products/%s/extra/%s/270_270.jpg',
                    $product->id,
                    $productExtraImage->id
                );

                $product->save();

                foreach (self::IMAGE_THUMBNAILS_SIZES as $size) {
                    $this->resizeImage($dir . '/original.jpg', $size, $size, $dir);
                }

                $this->saveExtraImages($product);
                $this->info($product->id . ' image corrected');
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
            }

        }
    }

    protected function saveExtraImages($product)
    {
        $extraImages = DB::connection('old_mysql')->select(
            'select * from product_extra_images where product_id = ?', [$product->id]
        );

        if (empty($extraImages)) {
            return;
        }

        foreach ($extraImages as $extraImage) {
            try {

                $imgLink = sprintf('http://new.superiorpromos.com/img/ucart/images/pimage/%s/%s',
                    $product->id,
                    str_replace(' ', '%20', $extraImage->image_src));

                if ($extraImage->pcolors_color_id != 0) {
                    if(($productColor = ProductColor::find($extraImage->pcolors_color_id)) != null) {
                        $this->productManager->createProductColorImage($productColor, $imgLink);
                    }

                    continue;
                }

                $productExtraImage = new ProductExtraImage();
                $productExtraImage->product()->associate($product);
                $productExtraImage->save();

                $dir = sprintf('%s%s%s/%s%s',
                    public_path(),
                    self::IMAGES_PATH,
                    $product->id,
                    self::EXTRA_IMAGES_PATH,
                    $productExtraImage->id
                );

                $imagePath = sprintf(
                    '/uploads/products/%s/extra/%s',
                    $product->id,
                    $productExtraImage->id
                );

                if (!file_exists($dir)) {
                    mkdir($dir, 0777, true);
                }

                $imgPath = $dir . '/original.jpg';
                Image::make($imgLink)->save($imgPath);

                $productExtraImage->image_src = $imagePath;
                $productExtraImage->save();

                foreach (self::IMAGE_THUMBNAILS_SIZES as $size) {
                    $this->resizeImage($dir . '/original.jpg', $size, $size, $dir);
                }

            } catch (\Exception $exception) {
                $this->error($extraImage->extra_image_id . ' ' . $exception->getMessage());
            }
        }

    }

    protected function resizeImage($file, $width, $height, $dir)
    {
        $name = implode('_', [$width, $height]);

        $backgroundImage = $this->createBackgroundImage($width, $height, '#fefefe');
        $resizedImage = Image::make($file)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $backgroundImage->insert($resizedImage, 'center')->save($dir . '/' . $name . '.jpg');
    }

    /**
     * @param int $width
     * @param int $height
     * @param null $backgroundColor
     *
     * @return InterventionImage
     */
    protected function createBackgroundImage(int $width, int $height, $backgroundColor = null): InterventionImage
    {
        return Image::canvas($width, $height, $backgroundColor);
    }
}
