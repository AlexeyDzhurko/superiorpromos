<?php

namespace App\Console\Commands;

use App\Jobs\OrderSyncJob;
use App\OldDBSync\SyncClasses\OrderItems;
use App\OldDBSync\SyncClasses\OrderItemStage;
use App\OldDBSync\SyncClasses\Orders;
use App\OldDBSync\SyncClasses\Stages;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\DB;

class OrdersSyncCommand extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:sync:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync orders from old database';

    public function handle()
    {
//        $this->dispatch(new OrderSyncJob());

        $this->dispatchNow(new Orders());
        $this->dispatchNow(new OrderItems());
        $this->dispatchNow(new Stages());
        $this->dispatchNow(new OrderItemStage());
    }
}

