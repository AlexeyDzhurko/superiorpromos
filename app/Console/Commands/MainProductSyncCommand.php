<?php


namespace App\Console\Commands;

use App\OldDBSync\SyncClasses\LocalMainProductImage;
use App\OldDBSync\SyncClasses\MainProductImage;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;


class MainProductSyncCommand extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:sync:main-product-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Integrate images from old database';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $localImageSource = $this->confirm('You are use local image source?');

        if($localImageSource) {
            $startTime = microtime(true);
            $this->info('Start copy images from local old site');
            $this->dispatchNow(new LocalMainProductImage());
            $endTime = microtime(true);
            $diff = round((($endTime - $startTime) / 60), 2);
            $this->info('Finish copy local images. Spend Time: '. $diff . ' min.');
            return true;
        }

        $startTime = microtime(true);
        $this->info('Start copy images from old site');
        $this->dispatchNow(new MainProductImage());
        $endTime = microtime(true);
        $diff = round((($endTime - $startTime) / 60), 2);
        $this->info('Finish copy images. Spend Time: '. $diff . ' min.');
        return true;
    }
}
