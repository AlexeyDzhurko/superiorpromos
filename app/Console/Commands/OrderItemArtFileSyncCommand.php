<?php


namespace App\Console\Commands;


use App\OldDBSync\SyncClasses\OrderItemArtFile;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;


class OrderItemArtFileSyncCommand extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:sync:order-item-art-files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync order item art files from old site';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startTime = microtime(true);
        $this->info('Start copy order item art files from old site');
        $this->dispatchNow(new OrderItemArtFile());
        $endTime = microtime(true);
        $diff = round((($endTime - $startTime) / 60), 2);
        $this->info('Finish copy order item art files. Spend Time: '. $diff . ' min.');
        return true;
    }
}
