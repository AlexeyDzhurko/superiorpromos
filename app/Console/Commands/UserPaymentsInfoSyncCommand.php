<?php


namespace App\Console\Commands;


use App\OldDBSync\SyncClasses\UserPaymentsInfo;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\DB;

class UserPaymentsInfoSyncCommand extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:user-payments-info-sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Integrate user payments information from old database';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('payment_profiles')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->dispatchNow(new UserPaymentsInfo());
    }
}
