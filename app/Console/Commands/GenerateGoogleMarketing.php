<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Repository\ProductRepository;
use App\Transformers\ProductBreadcrumbsTransformer;
use App\Transformers\ProductMarketingOptionsTransformer;
use App\Transformers\ProductSearchTransformer;
use Illuminate\Console\Command;

/**
 * Class GenerateGoogleMarketing
 * @package App\Console\Commands
 */
class GenerateGoogleMarketing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:google-marketing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the xml file to google marketing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $productCollection = Product::where(['active' => 1,])->get();
        $productData = (new ProductSearchTransformer($productCollection))->toArray();

        $products = fractal()->collection($productData,
            new ProductBreadcrumbsTransformer(['categories' => []]))
        ->toArray();

        $products = fractal()->collection($products,
            new ProductMarketingOptionsTransformer())
        ->toArray();

        $content = view('generated-templates.google', compact('products'))->render();
        \Storage::put('public/google.xml', $content);
    }
}
