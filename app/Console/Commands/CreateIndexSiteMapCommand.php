<?php


namespace App\Console\Commands;

use App\Jobs\CreateIndexSiteMap;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Log;


class CreateIndexSiteMapCommand extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:create-index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create site map index file';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            $this->dispatch(new CreateIndexSiteMap());
        }catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }
    }
}
