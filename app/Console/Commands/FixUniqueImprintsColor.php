<?php


namespace App\Console\Commands;


use App\Jobs\ClearImprintsJob;
use App\Models\Imprint;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\OldDBSync\SyncClasses\ImprintUniqueColors;

class FixUniqueImprintsColor extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:sync:imprints-individual-colors';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync imprint colors';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startTime = microtime(true);
        $this->info('Start sync');
        $this->dispatchNow(new ImprintUniqueColors());
        $endTime = microtime(true);
        $diff = round((($endTime - $startTime) / 60), 2);
        $this->info('Finish sync. Spend Time: '. $diff . ' min.');

//        $imprintsCount = Imprint::whereColorGroupId(null)->count();
//        if($imprintsCount > 0) {
//            $clearImprints = $this->confirm('Count of imprints without color ' . $imprintsCount . '. Do you wish clear imprints without colors?');
//            if($clearImprints) {
//                $startTime = microtime(true);
//                $this->info('Start clearing');
//                $this->dispatchNow(new ClearImprintsJob());
//                $endTime = microtime(true);
//                $diff = round((($endTime - $startTime) / 60), 2);
//                $this->info('Finish clearing. Spend Time: '. $diff . ' min.');
//            }
//        }
    }
}
