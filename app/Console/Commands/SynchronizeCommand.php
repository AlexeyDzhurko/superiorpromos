<?php

namespace App\Console\Commands;

use App\OldDBSync\SyncClasses\ArtProofs;
use App\OldDBSync\SyncClasses\BackNotes;
use App\OldDBSync\SyncClasses\CartItemImprintColors;
use App\OldDBSync\SyncClasses\CartItemImprints;
use App\OldDBSync\SyncClasses\CartItemProductOptions;
use App\OldDBSync\SyncClasses\CartItemProductSubOptions;
use App\OldDBSync\SyncClasses\CartItems;
use App\OldDBSync\SyncClasses\CartItemColors;
use App\OldDBSync\SyncClasses\Carts;
use App\OldDBSync\SyncClasses\Categories;
use App\OldDBSync\SyncClasses\ColorColorGroup;
use App\OldDBSync\SyncClasses\ColorGroups;
use App\OldDBSync\SyncClasses\Colors;
use App\OldDBSync\SyncClasses\ImprintPrices;
use App\OldDBSync\SyncClasses\Imprints;
use App\OldDBSync\SyncClasses\ImprintUniqueColors;
use App\OldDBSync\SyncClasses\OrderItems;
use App\OldDBSync\SyncClasses\OrderItemStage;
use App\OldDBSync\SyncClasses\OrderPaymentHistory;
use App\OldDBSync\SyncClasses\Orders;
use App\OldDBSync\SyncClasses\ProductColorGroups;
use App\OldDBSync\SyncClasses\ProductColorPrices;
use App\OldDBSync\SyncClasses\ProductColors;
use App\OldDBSync\SyncClasses\ProductExtraImages;
use App\OldDBSync\SyncClasses\ProductOptionPrices;
use App\OldDBSync\SyncClasses\ProductOptions;
use App\OldDBSync\SyncClasses\ProductPrices;
use App\OldDBSync\SyncClasses\Products;
use App\OldDBSync\SyncClasses\ProductsCategories;
use App\OldDBSync\SyncClasses\ProductsCategoriesSymlink;
use App\OldDBSync\SyncClasses\ProductSubOptionPrices;
use App\OldDBSync\SyncClasses\ProductSubOptions;
use App\OldDBSync\SyncClasses\ProductVendor;
use App\OldDBSync\SyncClasses\Reviews;
use App\OldDBSync\SyncClasses\SampleOrderItemColors;
use App\OldDBSync\SyncClasses\SampleOrderItems;
use App\OldDBSync\SyncClasses\SampleOrderItemStages;
use App\OldDBSync\SyncClasses\SampleOrderItemTrackingInfo;
use App\OldDBSync\SyncClasses\SampleOrders;
use App\OldDBSync\SyncClasses\Stages;
use App\OldDBSync\SyncClasses\TrackingInformations;
use App\OldDBSync\SyncClasses\UserNotes;
use App\OldDBSync\SyncClasses\OrderItemPaymentsHistory;
use App\OldDBSync\SyncClasses\UserPaymentsInfo;
use App\OldDBSync\SyncClasses\Users;
use App\OldDBSync\SyncClasses\Vendors;
use App\OldDBSync\SyncClasses\Wishes;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class SynchronizeCommand extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:sync:old-db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronise old and new databases';

    protected $oldDbConnection;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $isImportWithImages = $this->confirm('Do you wish import database with images?');

        config(['is_import_with_images'=>$isImportWithImages]);

        $startTime = microtime(true);
        $this->info('Start synchronise old data base with new data base');
        $this->runJobs();
        $endTime = microtime(true);
        $diff = round((($endTime - $startTime) / 60), 2);
        $this->info('Finish synchronise. Spend Time: '. $diff . ' min.');
    }

    /**
     * Run all jobs
     */
    private function runJobs()
    {
       $this->dispatchNow(new Users()); //important
       $this->dispatchNow(new Vendors()); //important
       $this->dispatchNow(new Categories()); //important
       $this->dispatchNow(new Products()); //important
       $this->dispatchNow(new ProductVendor()); //important
       $this->dispatchNow(new ProductsCategories()); //important
       $this->dispatchNow(new ProductsCategoriesSymlink()); //important
       $this->dispatchNow(new ProductPrices()); //important
       $this->dispatchNow(new ProductOptions()); //important
       $this->dispatchNow(new ProductOptionPrices()); //important
       $this->dispatchNow(new ProductSubOptions()); //important
       $this->dispatchNow(new ProductSubOptionPrices()); //important
       $this->dispatchNow(new ColorGroups()); //important
       $this->dispatchNow(new Colors()); //important
       $this->dispatchNow(new ColorColorGroup()); //important
       $this->dispatchNow(new ProductColorGroups()); //important
       $this->dispatchNow(new ProductColors()); //important
       $this->dispatchNow(new ProductColorPrices()); //important
       $this->dispatchNow(new Imprints()); //important
       $this->dispatchNow(new ImprintPrices()); //important
       $this->dispatchNow(new ImprintUniqueColors()); //important
       $this->dispatchNow(new Wishes()); //not-important
       $this->dispatchNow(new Reviews()); //not-important
       if(config('is_import_with_images')) $this->dispatchNow(new ProductExtraImages());
       $this->dispatchNow(new UserPaymentsInfo()); //important
       $this->dispatchNow(new Orders()); //important
       $this->dispatchNow(new Carts()); //important
       $this->dispatchNow(new CartItems()); //important
       $this->dispatchNow(new CartItemColors()); //important
       $this->dispatchNow(new CartItemProductOptions()); //important
       $this->dispatchNow(new CartItemProductSubOptions()); //important
       $this->dispatchNow(new CartItemImprints()); //important
       $this->dispatchNow(new CartItemImprintColors()); //important
       $this->dispatchNow(new Stages()); //important
       $this->dispatchNow(new OrderItems()); //important
       $this->dispatchNow(new UserNotes()); //important
       $this->dispatchNow(new BackNotes()); //important
       $this->dispatchNow(new ArtProofs()); //important

       $this->dispatchNow(new OrderItemStage()); //important
       $this->dispatchNow(new TrackingInformations()); //important
       $this->dispatchNow(new OrderItemPaymentsHistory()); //important
       $this->dispatchNow(new OrderPaymentHistory()); //important
       $this->dispatchNow(new SampleOrders()); //important
       $this->dispatchNow(new SampleOrderItems()); //important
       $this->dispatchNow(new SampleOrderItemColors()); //important
       $this->dispatchNow(new SampleOrderItemStages()); //important
       $this->dispatchNow(new SampleOrderItemTrackingInfo()); //important
    }
}
