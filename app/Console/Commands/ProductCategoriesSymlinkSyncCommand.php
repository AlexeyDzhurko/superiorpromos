<?php

namespace App\Console\Commands;

use App\OldDBSync\SyncClasses\ProductsCategoriesSymlink;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ProductCategoriesSymlinkSyncCommand extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:sync:categories-symlink-items';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync product into sub category ( already sets on app:sync:old-db )';

    public function handle()
    {
        $startTime = microtime(true);
        $this->info('Start sync product into sub category');
        $this->dispatchNow(new ProductsCategoriesSymlink());
        $endTime = microtime(true);
        $diff = round((($endTime - $startTime) / 60), 2);
        $this->info('Finish sync. Spend Time: '. $diff . ' min.');
    }
}
