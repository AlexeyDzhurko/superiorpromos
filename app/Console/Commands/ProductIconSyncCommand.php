<?php

namespace App\Console\Commands;

use App\Jobs\ProductIconSyncJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ProductIconSyncCommand extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:sync:product-icons';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import product icons from old site';

    public function handle()
    {
        $startTime = microtime(true);
        $this->info('Start import product icons from old site to current');
        $this->dispatchNow(new ProductIconSyncJob());
        $endTime = microtime(true);
        $diff = round((($endTime - $startTime) / 60), 2);
        $this->info('Finish import. Spend Time: '. $diff . ' min.');
    }
}
