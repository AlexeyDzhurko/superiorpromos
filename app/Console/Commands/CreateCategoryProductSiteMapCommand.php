<?php


namespace App\Console\Commands;


use App\Jobs\CreateCategorySiteMapJob;
use App\Jobs\CreateProductSiteMapJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Log;
use Storage;

class CreateCategoryProductSiteMapCommand extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:create-category-product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create site map files for category and product';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            Storage::disk('sitemap')->deleteDirectory('sitemaps');
            $this->dispatch(new CreateCategorySiteMapJob());
            $this->dispatch(new CreateProductSiteMapJob());
        }catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }
    }
}
