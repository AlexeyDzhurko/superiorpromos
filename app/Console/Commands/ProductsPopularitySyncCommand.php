<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\OldDBSync\SyncClasses\ProductsPopularitySync;

class ProductsPopularitySyncCommand extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:sync:product-priority';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy products priority from old database';
    
    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startTime = microtime(true);
        $this->info('Start copy');
        $this->dispatchNow(new ProductsPopularitySync());
        $endTime = microtime(true);
        $diff = round((($endTime - $startTime) / 60), 2);
        $this->info('Finish copy. Spend Time: '. $diff . ' min.');
    }
}
