<?php


namespace App\Console\Commands;


use App\OldDBSync\SyncClasses\UserBillingAddresses;
use App\OldDBSync\SyncClasses\UserBillingAddressFromCustomer;
use App\OldDBSync\SyncClasses\UserShippingAddresses;
use App\OldDBSync\SyncClasses\UserShippingAddressFromCustomer;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\DB;


class UserAddressesSyncCommand extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:user-addresses-sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Integrate user addresses from old database';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('addresses')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->dispatchNow(new UserShippingAddressFromCustomer());
        $this->dispatchNow(new UserBillingAddressFromCustomer());
        $this->dispatchNow(new UserShippingAddresses());
        $this->dispatchNow(new UserBillingAddresses());
    }
}
