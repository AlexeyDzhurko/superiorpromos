<?php


namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\OldDBSync\SyncClasses\LocalArtProofsFiles;

class ArtProofsSyncCommand extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:art-proofs-sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize art proofs from local path';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startTime = microtime(true);
        $this->info('Start copying art proofs');
        $this->dispatchNow(new LocalArtProofsFiles());
        $endTime = microtime(true);
        $diff = round((($endTime - $startTime) / 60), 2);
        $this->info('Finish copying. Spend Time: '. $diff . ' min.');
    }
}
