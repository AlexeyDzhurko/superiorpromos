<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Admin\Contracts\Sage;
use App\Repository\VendorRepository;

class VendorImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:vendor-import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import vendor from sage';

    protected $sage;
    /**
     * Create a new command instance.
     * @param Sage $sage
     */
    public function __construct(Sage $sage)
    {
        parent::__construct();
        $this->sage = $sage;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startTime = microtime(true);
        $this->info('Start import vendors from sage');

        $suppliers = $this->sage->getSuppliersList();
        foreach ($suppliers as $supplier) {
            $this->info('==>import: '.  $supplier['Company']);
            $this->insertUpdateVendor($supplier['SAGEID']);
        }

        $endTime = microtime(true);
        $diff = round((($endTime - $startTime) / 60), 2);
        $this->info('Finish import. Spend Time: '. $diff . ' min.');
    }

    /**
     * @param array $supplier
     */
    private function insertUpdateVendor($supplier)
    {
        $vendor = $this->sage->getSupplierInfo($supplier);

        VendorRepository::insertOrUpdate($vendor);

        unset($vendor);
    }
}
