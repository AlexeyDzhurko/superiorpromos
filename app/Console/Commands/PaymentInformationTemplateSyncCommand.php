<?php

namespace App\Console\Commands;

use App\Jobs\PaymentInformationTemplateSyncJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class PaymentInformationTemplateSyncCommand extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:sync:payment_information_templates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'SAGE pricing templates from old database';

    public function handle()
    {
        $this->dispatch(new PaymentInformationTemplateSyncJob());
    }
}

