<?php

namespace App\Console\Commands;

use App\OldDBSync\SyncClasses\LocalProductExtraImages;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\OldDBSync\SyncClasses\ProductExtraImages;
use App\Jobs\CategoriesSyncJob;

class ExtraProductImagesSyncCommand extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:sync:extra-product-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Integrate images from old database';
    
    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $localImageSource = $this->confirm('You are use local image source?');

        if($localImageSource) {
            $startTime = microtime(true);
            $this->info('Start copy images from local old site');
            $this->dispatchNow(new LocalProductExtraImages());
            $endTime = microtime(true);
            $diff = round((($endTime - $startTime) / 60), 2);
            $this->info('Finish copy local images. Spend Time: '. $diff . ' min.');
            return true;
        }

        $startTime = microtime(true);
        $this->info('Start copy images from old site');
        $this->dispatchNow(new ProductExtraImages());
        $endTime = microtime(true);
        $diff = round((($endTime - $startTime) / 60), 2);
        $this->info('Finish copy images. Spend Time: '. $diff . ' min.');
        return true;
    }
}
