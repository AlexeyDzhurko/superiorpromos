<?php

namespace App\Console\Commands;

use App\Jobs\PmsColorSyncJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class PmsColorSyncCommand extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:sync:pms_colors';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import PMS colors from old database';

    public function handle()
    {
        $this->dispatch(new PmsColorSyncJob());
    }
}
