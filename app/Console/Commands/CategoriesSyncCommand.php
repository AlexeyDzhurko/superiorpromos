<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\CategoriesSyncJob;

class CategoriesSyncCommand extends Command
{
    use DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:categories-sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Integrate categories from old database';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startTime = microtime(true);
        $this->info('Start synchronise old data base with new data base');
        $this->dispatch(new CategoriesSyncJob());
        $endTime = microtime(true);
        $diff = round((($endTime - $startTime) / 60), 2);
        $this->info('Finish synchronise. Spend Time: '. $diff . ' min.');
    }
}
