<?php

namespace App\Console;

use App\Console\Commands\ArtProofsSyncCommand;
use App\Console\Commands\CategoriesSyncCommand;
use App\Console\Commands\CorrectProductImagesLink;
use App\Console\Commands\CreateCategoryProductSiteMapCommand;
use App\Console\Commands\CreateIndexSiteMapCommand;
use App\Console\Commands\ElasticReindex;
use App\Console\Commands\FixUniqueImprintsColor;
use App\Console\Commands\ExtraProductImagesSyncCommand;
use App\Console\Commands\GenerateGoogleMarketing;
use App\Console\Commands\MainProductSyncCommand;
use App\Console\Commands\OrderItemArtFileSyncCommand;
use App\Console\Commands\PaymentInformationTemplateSyncCommand;
use App\Console\Commands\PmsColorSyncCommand;
use App\Console\Commands\ProductIconSyncCommand;
use App\Console\Commands\FillProductsWithShippingDataCommand;
use App\Console\Commands\SynchronizeCommand;
use App\Console\Commands\UserAddressesSyncCommand;
use App\Console\Commands\UserPaymentsInfoSyncCommand;
use App\Console\Commands\VendorImportCommand;
use App\Console\Commands\ProductCategoriesSymlinkSyncCommand;
use App\Console\Commands\OrdersSyncCommand;
use App\Console\Commands\ProductsPopularitySyncCommand;
use App\Jobs\ProductIconSyncJob;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        SynchronizeCommand::class,
        VendorImportCommand::class,
        CategoriesSyncCommand::class,
        PmsColorSyncCommand::class,
        PaymentInformationTemplateSyncCommand::class,
        ElasticReindex::class,
        ExtraProductImagesSyncCommand::class,
        ProductIconSyncCommand::class,
        ProductCategoriesSymlinkSyncCommand::class,
        OrdersSyncCommand::class,
        ProductsPopularitySyncCommand::class,
        FillProductsWithShippingDataCommand::class,
        UserAddressesSyncCommand::class,
        UserPaymentsInfoSyncCommand::class,
        FixUniqueImprintsColor::class,
        MainProductSyncCommand::class,
        ArtProofsSyncCommand::class,
        CreateCategoryProductSiteMapCommand::class,
        CreateIndexSiteMapCommand::class,
        OrderItemArtFileSyncCommand::class,
        GenerateGoogleMarketing::class,


        CorrectProductImagesLink::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command('app:vendor-import')->daily();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
