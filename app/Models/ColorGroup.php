<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\ColorGroup
 *
 * @property integer $id
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ColorGroup whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ColorGroup whereName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Color[] $colors
 * @property integer $product_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ColorGroup whereProductId($value)
 */
class ColorGroup extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * @return BelongsToMany
     */
    public function colors()
    {
        return $this->belongsToMany('App\Models\Color', 'color_color_group', 'group_id', 'color_id');
    }

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param array $data
     * @return array
     */
    public function addColors(array $data)
    {
        return $this->colors()->sync($data);
    }
}
