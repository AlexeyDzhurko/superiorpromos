<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TrackingInformation
 *
 * @property integer $id
 * @property \DateTime $tracking_shipping_date
 * @property string $tracking_number
 * @property string $tracking_shipping_company
 * @property string $tracking_note
 * @property string $tracking_url
 * @property integer $tracking_user_id
 * @property integer $order_item_id
 * @property bool $tracking_notify_customer
 * @property bool $tracking_request_rating
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $trackingUser
 * @property-read \App\Models\OrderItem $orderItem
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TrackingInformation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TrackingInformation whereTrackingShippingDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TrackingInformation whereTrackingNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TrackingInformation whereTrackingShippingCompany($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TrackingInformation whereTrackingNote($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TrackingInformation whereTrackingUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TrackingInformation whereTrackingUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TrackingInformation whereOrderItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TrackingInformation whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TrackingInformation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TrackingInformation extends Model
{
    protected $fillable = [
        'tracking_shipping_date',
        'tracking_number',
        'tracking_shipping_company',
        'tracking_note',
        'tracking_url',
        'tracking_notify_customer',
        'tracking_request_rating'
    ];

    protected $dates = [
        'tracking_shipping_date' => 'date:Y-m-d',
    ];

    public function trackingUser()
    {
        return $this->belongsTo(User::class, 'tracking_user_id');
    }

    public function orderItem()
    {
        return $this->belongsTo(OrderItem::class, 'order_item_id');
    }
}
