<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProductSubOptionPrice
 *
 * @property integer $id
 * @property integer $product_sub_option_id
 * @property integer $quantity
 * @property float $setup_price
 * @property float $item_price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\ProductSubOption $productSubOption
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductSubOptionPrice whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductSubOptionPrice whereProductSubOptionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductSubOptionPrice whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductSubOptionPrice whereSetupPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductSubOptionPrice whereItemPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductSubOptionPrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductSubOptionPrice whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ProductSubOptionPrice extends Model
{
    public function productSubOption()
    {
        return $this->belongsTo(ProductSubOption::class);
    }
}
