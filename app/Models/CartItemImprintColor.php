<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CartItemImprintColor
 *
 * @property integer $id
 * @property integer $color_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Color $color
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemImprintColor whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemImprintColor whereCartImprintId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemImprintColor whereColorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemImprintColor whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemImprintColor whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemImprintColor whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property integer $cart_item_imprint_id
 * @property-read \App\Models\CartItemImprint $cartItemImprint
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemImprintColor whereCartItemImprintId($value)
 */
class CartItemImprintColor extends Model
{
    public function color()
    {
        return $this->belongsTo(Color::class);
    }

    public function cartItemImprint()
    {
        return $this->belongsTo(CartItemImprint::class);
    }
}
