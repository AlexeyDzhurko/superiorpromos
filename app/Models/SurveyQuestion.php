<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SurveyQuestion
 *
 * @property integer $id
 * @property integer $survey_id
 * @property string $name
 * @property string $type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SurveyAnswer[] $surveyAnswers
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SurveyQuestion whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SurveyQuestion whereSurveyId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SurveyQuestion whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SurveyQuestion whereType($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Survey $survey
 */
class SurveyQuestion extends Model
{
    public $timestamps = false;

    public function surveyAnswers()
    {
        return $this->hasMany(SurveyAnswer::class);
    }

    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }
}
