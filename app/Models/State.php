<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\State
 *
 * @property integer $id
 * @property string $abbr
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\App\Models\State whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\State whereAbbr($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\State whereName($value)
 * @mixin \Eloquent
 * @property float $tax
 * @method static \Illuminate\Database\Query\Builder|\App\Models\State whereTax($value)
 */
class State extends Model
{
    public $timestamps = false;
}
