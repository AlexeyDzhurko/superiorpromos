<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Testimonial
 * @package App\Models
 *
 * @SWG\Definition (
 *      definition="Testimonial",
 * @SWG\Property (property="id", type="integer", example="2"),
 * @SWG\Property (property="item_name", type="string", example="Beach House"),
 * @SWG\Property (property="position_after", type="string", example="7"),
 * @SWG\Property (property="active", type="boolean", example="1"),
 * @SWG\Property (property="name", type="string", example="Lorem ipsum"),
 * @SWG\Property (property="content", type="string", example="Lorem ipsum dolor sit amet, consectetur adipiscing elit."),
 * @SWG\Property (property="product_link", type="string", example="http://test.com"),
 * @SWG\Property (property="image", type="string", example="http://superiorpromos.com/uploads/testimonials/4b391ab697a75530b46fd12b0e40dc81.png"),

 * ),
 */
/**
 * App\Models\Testimonial
 *
 * @property integer $id
 * @property string $item_name
 * @property integer $position_after
 * @property boolean $active
 * @property string $name
 * @property string $content
 * @property string $product_link
 * @property string $image
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Testimonial whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Testimonial whereItemName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Testimonial wherePositionAfter($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Testimonial whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Testimonial whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Testimonial whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Testimonial whereProductLink($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Testimonial whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Testimonial whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Testimonial whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Testimonial extends Model
{
    const ACTIVE = 1;
    const INACTIVE = 0;
    const TESTIMONIAL_DIR = 'testimonials';

    protected $fillable = [
        'item_name',
        'position_after',
        'active',
        'name',
        'content',
        'product_link',
        'image',
    ];
}
