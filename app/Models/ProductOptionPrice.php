<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProductOptionPrice
 *
 * @property integer $id
 * @property integer $product_option_id
 * @property integer $vendor_id
 * @property integer $quantity
 * @property float $setup_price
 * @property float $item_price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\ProductOption $productOption
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductOptionPrice whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductOptionPrice whereProductOptionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductOptionPrice whereVendorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductOptionPrice whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductOptionPrice whereSetupPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductOptionPrice whereItemPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductOptionPrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductOptionPrice whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ProductOptionPrice extends Model
{
    public function productOption()
    {
        return $this->belongsTo(ProductOption::class);
    }
}
