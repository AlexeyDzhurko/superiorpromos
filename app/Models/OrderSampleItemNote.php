<?php


namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OrderSampleItemNote
 *
 * @property integer $id
 * @property string $note
 * @property integer $order_sample_item_id
 * @property-read \App\Models\OrderSampleItem $orderSampleItem
 * @property integer $user_id
 * @property-read User $user
 *
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @mixin \Eloquent
 */
class OrderSampleItemNote extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_sample_item_notes';

    /**
     * @var array
     */
    protected $fillable = [
        'note'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderSampleItem()
    {
        return $this->belongsTo(OrderSampleItem::class);
    }
}
