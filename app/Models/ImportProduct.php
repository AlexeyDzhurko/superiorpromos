<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ImportProduct
 *
 * @property integer $id
 * @property string $name
 * @property string $picture
 * @property integer $vendor_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImportProduct whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImportProduct whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImportProduct whereVendorId($value)
 * @mixin \Eloquent
 * @property integer $sage_product_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImportProduct whereSageProductId($value)
 * @property integer $import_product_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImportProduct whereImportProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImportProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImportProduct whereUpdatedAt($value)
 * @property integer $status
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImportProduct whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImportProduct wherePicture($value)
 */
class ImportProduct extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'import_product_id', 'name', 'picture'
    ];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
