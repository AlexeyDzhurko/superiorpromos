<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Reminder
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $type
 * @property boolean $sent
 * @property string $date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Order $order
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Reminder whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Reminder whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Reminder whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Reminder whereSent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Reminder whereDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Reminder whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Reminder whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Reminder extends Model
{
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
