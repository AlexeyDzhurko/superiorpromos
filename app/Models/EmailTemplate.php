<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmailTemplate
 *
 * @property integer $id
 * @property string $title
 * @property string $subject
 * @property string $body
 * @property string $type
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EmailTemplate whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EmailTemplate whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EmailTemplate whereSubject($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EmailTemplate whereBody($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EmailTemplate whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EmailTemplate whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EmailTemplate whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EmailTemplate whereUpdatedAt($value)
 */
class EmailTemplate extends Model
{
    //
}
