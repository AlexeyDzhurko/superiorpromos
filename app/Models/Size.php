<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Size
 *
 * @property integer $id
 * @property string $name
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Size whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Size whereName($value)
 * @property integer $size_group_id
 * @property-read \App\Models\SizeGroup $sizeGroup
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Size whereSizeGroupId($value)
 */
class Size extends Model
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function sizeGroup()
    {
        return $this->belongsTo(SizeGroup::class);
    }
}
