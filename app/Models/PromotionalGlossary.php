<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

/**
 * @SWG\Definition (
 *      definition="PromotionalGlossary",
 *      @SWG\Property(property="title", type="string", example="Test"),
 *      @SWG\Property(property="body", type="string", example="Test"),
 *      @SWG\Property(property="position", type="integer", example="1")
 * ),
 *
 * App\Models\PromotionalGlossary
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property boolean $active
 * @property integer $position
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PromotionalGlossary whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PromotionalGlossary whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PromotionalGlossary whereBody($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PromotionalGlossary whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PromotionalGlossary wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PromotionalGlossary whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PromotionalGlossary whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PromotionalGlossary sorted()
 * @mixin \Eloquent
 */
class PromotionalGlossary extends Model
{
    use SortableTrait;

    protected $fillable = [
        'title',
        'body',
        'active',
    ];
}
