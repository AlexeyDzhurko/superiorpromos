<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CartItemProductSubOption
 *
 * @property integer $id
 * @property integer $cart_item_product_option_id
 * @property integer $product_sub_option_id
 * @property string $name
 * @property float $setup_price
 * @property float $item_price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemProductSubOption whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemProductSubOption whereCartItemProductOptionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemProductSubOption whereProductSubOptionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemProductSubOption whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemProductSubOption whereSetupPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemProductSubOption whereItemPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemProductSubOption whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemProductSubOption whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\ProductSubOption $productSubOption
 * @property-read \App\Models\CartItemProductOption $cartItemProductOption
 */
class CartItemProductSubOption extends Model
{
    public function productSubOption()
    {
        return $this->belongsTo(ProductSubOption::class);
    }

    public function cartItemProductOption()
    {
        return $this->belongsTo(CartItemProductOption::class);
    }
}
