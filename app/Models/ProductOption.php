<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProductOption
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $position
 * @property string $name
 * @property boolean $required
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductOptionPrice[] $productOptionPrices
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductSubOption[] $productSubOptions
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductOption whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductOption whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductOption wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductOption whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductOption whereRequired($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductOption whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductOption whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Product $product
 * @property string $show_as
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductOption whereShowAs($value)
 */
class ProductOption extends Model
{
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function productOptionPrices()
    {
        return $this->hasMany(ProductOptionPrice::class);
    }

    public function productSubOptions()
    {
        return $this->hasMany(ProductSubOption::class);
    }
}
