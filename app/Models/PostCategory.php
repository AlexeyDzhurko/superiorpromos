<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PostCategory
 *
 * @SWG\Definition (
 *      definition="PostCategory",
 *      @SWG\Property (property="id", type="integer", example="134"),
 *      @SWG\Property (property="title", type="string", example="Promotional job"),
 *      @SWG\Property (property="posts_count", type="integer", example="18"),
 *      @SWG\Property (property="created_at", type="string", example="2017-07-03 14:15:51"),
 *      @SWG\Property (property="updated_at", type="string", example="2017-07-04 14:15:51"),
 * ),
 *
 * @property integer $id
 * @property string $title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $posts
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PostCategory whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PostCategory whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PostCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PostCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PostCategory extends Model
{
    protected $fillable = [
        'title',
    ];

    /**
     * Get the posts for the post category.
     */
    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }
}
