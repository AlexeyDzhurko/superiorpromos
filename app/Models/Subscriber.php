<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * Class Subscriber
 * @package App\Models
 *
 * @property integer $id
 * @property string $email
 * @property string $coupon
 * @property $used
 * @property string $bigmailer_uuid
 *
 */
class Subscriber extends Model
{
    use Notifiable;

    protected $fillable = [
        'email',
        'coupon',
        'used',
        'bigmailer_uuid',
    ];
}
