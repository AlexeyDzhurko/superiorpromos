<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Wish
 *
 * @package App\Models
 * @property integer $id
 * @property integer $user_id
 * @property integer $product_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @property-read \App\Models\Product $product
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Wish whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Wish whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Wish whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Wish whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Wish whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Wish extends Model
{
    protected $fillable = ['product_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
