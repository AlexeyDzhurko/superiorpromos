<?php

namespace App\Models;

use App\PaymentProfile;
use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Payment
 *
 * @property integer $id
 * @property integer $order_item_id
 * @property integer $payment_profile_id
 * @property string $status
 * @property string $transaction_id
 * @property $shipping_date
 * @property string $type
 * @property float $tax
 * @property string $shipping_company
 * @property integer $shipping_quantity
 * @property integer $over_quantity
 * @property float $over_fees
 * @property float $price
 * @property float $credit_amount
 * @property boolean $notify_customer
 * @property string $notify_message
 * @property integer $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @property-read \App\Models\OrderItem $orderItem
 * @property-read \App\PaymentProfile $paymentProfile
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment whereOrderItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment wherePaymentProfileId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment whereTransactionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment whereShippingDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment whereTax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment whereShippingCompany($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment whereShippingQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment whereOverQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment whereOverFees($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment whereCreditAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment whereNotifyCustomer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment whereNotifyMessage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property integer $order_id
 * @property-read \App\Models\Order $order
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Payment whereOrderId($value)
 */
class Payment extends Model
{

    protected $fillable = [
        'shipping_date',
        'shipping_company',
        'shipping_quantity',
        'over_quantity',
        'over_fees',
        'notify_customer',
        'notify_message'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orderItem()
    {
        return $this->belongsTo(OrderItem::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function paymentProfile()
    {
        return $this->belongsTo(PaymentProfile::class);
    }
}
