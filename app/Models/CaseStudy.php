<?php

namespace App\Models;

use App\Search\Searchable;
use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

/**
 * App\Models\CaseStudy
 *
 * @SWG\Definition (
 *      definition="CaseStudy",
 * @SWG\Property (property="id", type="integer", example="2"),
 * @SWG\Property (property="title", type="string", example="Subject"),
 * @SWG\Property (property="body", type="string", example="Article content"),
 * @SWG\Property (property="active", type="boolean", example="1"),
 * @SWG\Property (property="position", type="integer", example="1"),
 * @SWG\Property (property="created_at", type="string", example="2017-07-03 14:15:51"),
 * @SWG\Property (property="updated_at", type="string", example="2017-07-03 14:15:51"),
 * ),
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property boolean $active
 * @property integer $position
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CaseStudy whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CaseStudy whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CaseStudy whereBody($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CaseStudy whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CaseStudy wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CaseStudy whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CaseStudy whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CaseStudy sorted()
 * @mixin \Eloquent
 */
class CaseStudy extends Model implements Searchable
{
    use SortableTrait;

    protected $fillable = [
        'title',
        'body',
        'active',
    ];

    public function getElasticDocument(): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'active' => $this->active,
            'created_at' => $this->created_at->toIso8601String(),
            'position' => $this->position,
        ];
    }

    public static function getElasticMapping(): array
    {
        return [
            'id' => [
                'type' => 'integer',
                'include_in_all' => FALSE
            ],
            'title' => [
                'type' => 'string',
                'analyzer' => 'standard'
            ],
            'active'=> [
                'type' => 'boolean',
            ],
            'created_at'=> [
                'type' => 'date',
            ],
            'position' => [
                'type' => 'integer',
            ],

        ];
    }

    public static function getIndexName(): string
    {
        return 'case_study';
    }
}
