<?php

namespace App\Models;

use App\Http\Filters\Filterable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Post
 *
 * @SWG\Definition (
 *      definition="Post",
 *      @SWG\Property (property="id", type="integer", example="134"),
 *      @SWG\Property (property="title", type="string", example="The fight against illegal deforestation with TensorFlow"),
 *      @SWG\Property (property="seo_title", type="string", example="The fight against illegal deforestation"),
 *      @SWG\Property (property="excerpt", type="string", example="For me, growing up in the 80s and 90s, the phrase “Save the Rainforest” was a directive that barely progressed over the years. The appeal was clear, but the threat was abstract and distant. And the solution (if there was one) seemed difficult to grasp. Since then, other worries—even harder to grasp in their immediacy and scope—have come to dominate our conversations: climate change, as an example."),
 *      @SWG\Property (property="body", type="string", example="For me, growing up in the 80s and 90s, the phrase “Save the Rainforest” was a directive that barely progressed over the years. The appeal was clear, but the threat was abstract and distant. And the solution (if there was one) seemed difficult to grasp. Since then, other worries—even harder to grasp in their immediacy and scope—have come to dominate our conversations: climate change, as an example. So many of us believe that technology has a crucial role to play in fighting climate change, but few are as aware that “Saving the Rainforest” and fighting climate change are nearly one and the same. By the numbers, destruction of forests accounts for nearly one-fifth of all greenhouse gas emissions every year. And in the tropical rainforest deforestation accelerated on the heels of rampant logging—up to 90 percent of which is done illegally and under the radar. Stopping illegal logging and protecting the world’s rainforests may be the fastest, cheapest way for humanity to slow climate change. And who’s best suited to protect the rainforest? The locals and the indigenous tribes that have lived there for generations. Rainforest Connection is a group of engineers and developers focused on building technology to help locals—like the Tembé tribe from central Amazon—protect their land, and in the process, protect the rest of us from the effects of climate change. Chief Naldo Tembé reached out to me a couple years ago seeking to collaborate on ways technology could help stop illegal loggers from destroying their land. Together, we embarked on an ambitious plan to address this issue using recycled cell phones and machine learning."),
 *      @SWG\Property (property="image", type="string", example="http://superiorpromos.com/uploads/blog/post/4b391ab697a75530b46fd12b0e40dc81.png"),
 *      @SWG\Property (property="slug", type="string", example="the-fight-against-illegal-deforestation-with-tensorflow"),
 *      @SWG\Property (property="meta_description", type="string", example="The fight against illegal deforestation with TensorFlow"),
 *      @SWG\Property (property="meta_keywords", type="string", example="Fight, TensorFlow"),
 *      @SWG\Property (property="created_at", type="string", example="2017-07-03 14:15:51"),
 * ),
 * @property integer $id
 * @property integer $author_id
 * @property integer $category_id
 * @property string $title
 * @property string $seo_title
 * @property string $excerpt
 * @property string $body
 * @property string $image
 * @property string $slug
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $status
 * @property boolean $featured
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereAuthorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereSeoTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereExcerpt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereBody($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereMetaDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereMetaKeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereFeatured($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property integer $post_category_id
 * @property-read \App\Models\PostCategory $postCategory
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post wherePostCategoryId($value)
 */
class Post extends Model
{
    use Filterable;

    protected $fillable = [
        'title',
        'seo_title',
        'excerpt',
        'body',
        'image',
        'slug',
        'meta_description',
        'meta_keywords',
        'status',
        'post_category_id'
    ];

    /**
     * Get the post that owns the category.
     */
    public function postCategory()
    {
        return $this->belongsTo('App\Models\PostCategory');
    }
}
