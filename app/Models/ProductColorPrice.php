<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProductColorPrice
 *
 * @property integer $id
 * @property integer $product_color_id
 * @property integer $quantity
 * @property float $setup_price
 * @property float $item_price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColorPrice whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColorPrice whereProductColorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColorPrice whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColorPrice whereSetupPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColorPrice whereItemPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColorPrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColorPrice whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\ProductColor $productColor
 */
class ProductColorPrice extends Model
{
    public function productColor()
    {
        return $this->belongsTo(ProductColor::class);
    }
}
