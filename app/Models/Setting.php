<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Setting
 *
 * @property integer $id
 * @property string $key
 * @property string $display_name
 * @property string $value
 * @property string $details
 * @property string $type
 * @property integer $order
 * @property string $group
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereKey($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereDisplayName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereDetails($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Setting whereGroup($value)
 * @mixin \Eloquent
 */
class Setting extends Model
{
    protected $table = 'settings';
    protected $fillable = ['key', 'display_name', 'value', 'options', 'type', 'order', 'details'];
    public $timestamps = false;

    public static function setting($key, $default = null)
    {
        $setting = Setting::where('key', '=', $key)->first();
        if (isset($setting->id)) {
            return $setting->value;
        }
        return $default;
    }
}
