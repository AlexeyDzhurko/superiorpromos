<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ImprintColor
 *
 * @property integer $id
 * @property integer $imprint_id
 * @property integer $color_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColor whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColor whereImprintId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColor whereColorId($value)
 * @mixin \Eloquent
 * @property-read Color $color
 * @property-read Imprint $imprint
 */
class ImprintColor extends Model
{
    protected $fillable = ['color_id'];

    public $timestamps = false;

    public function imprint()
    {
        return $this->belongsTo(Imprint::class);
    }

    public function color()
    {
        return $this->hasOne(Color::class, 'id', 'color_id');
    }
}
