<?php

namespace App\Models;

use App\Search\Searchable;
use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OrderSample
 *
 * @SWG\Definition (
 *      definition="OrderSample",
 * @SWG\Property (property="id", type="integer", example="1"),
 * @SWG\Property (property="shipping_method", type="string", example="ups"),
 * ),
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderSample whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderSample whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderSample whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderSample whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property integer $user_id
 * @property-read \App\User $user
 * 
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderSampleItem[] $orderSampleItems
 *
 * @property string $own_shipping_type
 * @property string $own_account_number
 * @property string $own_shipping_system
 * @property string $shipping_method
 *
 * @property string $quantity
 * @property string $attraction
 * @property string $event_date
 * @property string $frequency
 * @property string $sample
 * @property string $sample_comment
 * @property string $upcoming_event 
 * @property float $price
 * @property float $total_price
 */
class OrderSample extends Model implements Searchable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_sample';
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orderSampleItems()
    {
        return $this->hasMany(OrderSampleItem::class);
    }

    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    public function getElasticDocument(): array
    {
        $order_sample_items = $this->orderSampleItems->map(function($orderSampleItem) {

            return [
                'id'          => $orderSampleItem->id,
                'vendor_id'   => $orderSampleItem->product->vendor_id,
                'product_id'  => $orderSampleItem->product_id,
                'stage_id'    => $orderSampleItem->stage_id,
                'check_notes' => $orderSampleItem->check_notes,
                'price'       => $orderSampleItem->price,

            ];
        })->toArray();

        return [
            'id'                 => $this->id,
            'created_at'         => $this->created_at->toIso8601String(),
            'billing_first_name' => $this->user->addresses->where('type',1)->first()->first_name,
            'billing_last_name'  => $this->user->addresses->where('type',1)->first()->last_name,
            'user_id'            => $this->user_id,
            'order_sample_items' => $order_sample_items,
            'state'              => $this->user->addresses->where('type',2)->first()->state,
            'total_price'        => $this->total_price
        ];
    }

    public static function getElasticMapping(): array
    {
        return [
            'id' => [
                'type' => 'integer',
                'include_in_all' => FALSE
            ],
            'created_at'=> [
                'type' => 'date',
            ],
            'billing_first_name' => [
                'type' => 'string',
                'analyzer' => 'standard',
                'fielddata' => true,
            ],
            'billing_last_name' => [
                'type' => 'string',
                'analyzer' => 'standard'
            ],
            'user_id' => [
                'type' => 'long',
            ],
            'total_price' => [
                'type' => 'double',
            ],
            'state' => [
                'type' => 'string',
                'analyzer' => 'standard'
            ],

            'order_sample_items' => [
                'type' => 'nested',
                'properties' => [
                    'id' => [
                        'type' => 'integer',
                    ],
                    'product_id' => [
                        'type' => 'integer',
                    ],
                    'vendor_id' => [
                        'type' => 'integer',
                    ],
                    'check_notes' => [
                        'type' => 'boolean',
                    ],
                    'stage_id' => [
                        'type' => 'integer',
                    ],
                    'price' => [
                        'type' => 'double',
                    ]
                ]
            ],
        ];
    }

    public static function getIndexName(): string
    {
        return 'order_sample';
    }

    public static function getElasticAggregationsParams()
    {
        return [
            'by_stage_id' => [
                'nested' => [
                    'path' => 'order_sample_items'
                ],
                'aggs' => [
                    'by_stage_id' => [
                        'terms' => [
                            'field' => 'order_sample_items.id',
                            'size' => 999,
                        ]
                    ]
                ]
            ]
        ];
    }
}
