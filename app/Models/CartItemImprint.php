<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CartItemImprint
 *
 * @property integer $id
 * @property integer $cart_item_id
 * @property integer $imprint_id
 * @property string $imprint_name
 * @property float $setup_price
 * @property float $item_price
 * @property float $color_setup_price
 * @property float $color_item_price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\CartItem $cartItem
 * @property-read \App\Models\Imprint $imprint
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CartItemImprintColor[] $cartItemImprintColors
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemImprint whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemImprint whereCartItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemImprint whereImprintId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemImprint whereImprintName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemImprint whereSetupPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemImprint whereItemPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemImprint whereColorSetupPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemImprint whereColorItemPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemImprint whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemImprint whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CartItemImprint extends Model
{
    public function cartItem()
    {
        return $this->belongsTo(CartItem::class);
    }

    public function imprint()
    {
        return $this->belongsTo(Imprint::class);
    }

    public function cartItemImprintColors()
    {
        return $this->hasMany(CartItemImprintColor::class);
    }
}
