<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Block
 *
 * @property integer $id
 * @property string $key
 * @property string $display_name
 * @property string $content
 * @property boolean $active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Block whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Block whereKey($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Block whereDisplayName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Block whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Block whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Block whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Block whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Block extends Model
{
    protected $fillable = [
        'content',
        'active',
    ];
}
