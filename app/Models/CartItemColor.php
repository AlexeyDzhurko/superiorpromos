<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CartItemColor
 *
 * @property integer $id
 * @property integer $cart_item_id
 * @property integer $color_group_id
 * @property string $color_group_name
 * @property integer $color_id
 * @property string $color_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\CartItem $cartItem
 * @property-read \App\Models\ColorGroup $colorGroup
 * @property-read \App\Models\Color $color
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemColor whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemColor whereCartItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemColor whereColorGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemColor whereColorGroupName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemColor whereColorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemColor whereColorName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemColor whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemColor whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property integer $product_color_group_id
 * @property integer $product_color_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemColor whereProductColorGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemColor whereProductColorId($value)
 * @property-read \App\Models\ProductColorGroup $productColorGroup
 * @property-read \App\Models\ProductColor $productColor
 */
class CartItemColor extends Model
{
    public function cartItem()
    {
        return $this->belongsTo(CartItem::class);
    }

    public function productColorGroup()
    {
        return $this->belongsTo(ProductColorGroup::class);
    }

    public function productColor()
    {
        return $this->belongsTo(ProductColor::class);
    }
}
