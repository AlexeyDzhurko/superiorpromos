<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

/**
 * App\Models\ProductCategory
 *
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductCategory sorted()
 */
class ProductCategory extends Model
{
    use SortableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'category_id', 'type'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
}
