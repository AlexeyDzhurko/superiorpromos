<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * @property $url
 * @property $image_src
 * @property $path
 *
 * Class Promos
 * @package App\Models
 */
class Promos extends Model
{
    /**
     * @var string
     */
    protected $table = 'promos';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $appends = ['path'];

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'url',
        'image_src',
        'position'
    ];

    /**
     * @param $value
     * @return string
     */
    public function getImageSrcAttribute($value)
    {
        return \Storage::disk('uploads')->url($value);
    }

    /**
     * @return string
     */
    public function getPathAttribute()
    {
        return $this->attributes['image_src'];
    }
}
