<?php

namespace App\Models;

use Baum\Node;

/**
 * Category
 *
 * @SWG\Definition (
 *      definition="Category",
 * @SWG\Property (property="id", type="integer", example="1"),
 * @SWG\Property (property="title", type="string", example="Root category"),
 * @SWG\Property (property="img_src", type="string", example="/"),
 * @SWG\Property (property="keywords", type="string", example="Superiorpromos"),
 * @SWG\Property (property="meta_title", type="string", example="Superiorpromos"),
 * @SWG\Property (property="meta_description", type="string", example="Superiorpromos"),
 * @SWG\Property (property="slug", type="string", example="root-category"),
 * @SWG\Property (property="sub_categories_exist", type="boolean"),
 * ),
 * @property integer $id
 * @property integer $parent_id
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property string $name
 * @property string $slug
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Category $parent
 * @property-read \Baum\Extensions\Eloquent\Collection|\App\Models\Category[] $children
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereLft($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereRgt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereDepth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Baum\Node withoutNode($node)
 * @method static \Illuminate\Database\Query\Builder|\Baum\Node withoutSelf()
 * @method static \Illuminate\Database\Query\Builder|\Baum\Node withoutRoot()
 * @method static \Illuminate\Database\Query\Builder|\Baum\Node limitDepth($limit)
 * @mixin \Eloquent
 * @property integer $old_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereOldId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @property string $meta_title
 * @property string $meta_description
 * @property string $keywords
 * @property string $title
 * @property string $description
 * @property string $footer_text
 * @property string $image_src
 * @property boolean $active
 * @property boolean $sub_categories_exist
 * @property array $sub_categories
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereMetaTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereMetaDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereKeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereFooterText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereImageSrc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereActive($value)
 * @property string $froogle_description
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereFroogleDescription($value)
 * @property boolean $is_quick_link
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereIsQuickLink($value)
 * @property boolean $is_popular
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Category whereIsPopular($value)
 */
class Category extends Node
{

    const IMAGES_DIR = 'categories/';

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'categories';

    //////////////////////////////////////////////////////////////////////////////

    //
    // Below come the default values for Baum's own Nested Set implementation
    // column names.
    //
    // You may uncomment and modify the following fields at your own will, provided
    // they match *exactly* those provided in the migration.
    //
    // If you don't plan on modifying any of these you can safely remove them.
    //

    /**
     * Column name which stores reference to parent's node.
     *
     * @var string
     */
    protected $parentColumn = 'parent_id';

    /**
     * Column name for the left index.
     *
     * @var string
     */
    protected $leftColumn = 'lft';

    /**
     * Column name for the right index.
     *
     * @var string
     */
    protected $rightColumn = 'rgt';

    /**
     * Column name for the depth field.
     *
     * @var string
     */
    protected $depthColumn = 'depth';

    // /**
    //  * Column to perform the default sorting
    //  *
    //  * @var string
    //  */
    // protected $orderColumn = null;

    /**
     * With Baum, all NestedSet-related fields are guarded from mass-assignment
     * by default.
     *
     * @var array
     */
    protected $guarded = ['id', 'parent_id', 'lft', 'rgt', 'depth'];

    //
    // This is to support "scoping" which may allow to have multiple nested
    // set trees in the same database table.
    //
    // You should provide here the column names which should restrict Nested
    // Set queries. f.ex: company_id, etc.
    //

    // /**
    //  * Columns which restrict what we consider our Nested Set list
    //  *
    //  * @var array
    //  */
    // protected $scoped = array();

    //////////////////////////////////////////////////////////////////////////////

    //
    // Baum makes available two model events to application developers:
    //
    // 1. `moving`: fired *before* the a node movement operation is performed.
    //
    // 2. `moved`: fired *after* a node movement operation has been performed.
    //
    // In the same way as Eloquent's model events, returning false from the
    // `moving` event handler will halt the operation.
    //
    // Please refer the Laravel documentation for further instructions on how
    // to hook your own callbacks/observers into this events:
    // http://laravel.com/docs/5.0/eloquent#model-events

    public function products()
    {
        return $this->belongsToMany(Product::class, 'products_categories', 'category_id', 'product_id')
            ->withPivot('type');
    }

    public function getSubCategoriesExistAttribute()
    {
        return $this->children()->where('is_quick_link', 0)->where('active', 1)->exists();
    }

    public function getSubCategoriesAttribute()
    {
        return $this->children()->where('is_quick_link', 0)->where('active', 1)->get();
    }

    public function parentCategory()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }

    public function childsCategory()
    {
        return $this->hasMany(Category::class, 'id', 'parent_id');
    }

    /**
     * @param Category $category
     * @param array $parents
     * @return \Illuminate\Support\Collection
     */
    public function getParentsAndSelf(Category $category, $parents = [])
    {
        if (empty($parents)) {
            $parents = [$category];
        }

        if ($parent = $category->parentCategory) {
            array_unshift($parents, $parent);
        }

        if ($parent && $parent->parentCategory) {
            return $this->getParentsAndSelf($parent, $parents);
        } else {
            return collect($parents);
        }
    }

    /**
     * @param Category $category
     * @param array $childs
     * @return \Illuminate\Support\Collection
     */
    public function getChildsAndSelf(Category $category, $childs = [])
    {
        if (empty($childs)) {
            $childs = [$category];
        }

        if ($currentChilds = $category->children) {
            foreach ($currentChilds as $child) {
                array_push($childs, $child);
            }
        }

        if (!empty($currentChilds)) {
            foreach ($currentChilds as $child) {
                $childs = $this->getChildsAndSelf($child, $childs);
            }
        }

        return $childs;
    }

    public static function treeToList($categories, $nested = 0)
    {
        $data = [];
        $prefix = '';
        for ($i = 0; $i < $nested; $i++) {
            $prefix .= '- ';
        }

        foreach ($categories as $category) {
            /** @var Category $category */
            $data[] = [
                'id' => $category->id,
                'text' => $prefix . $category->name,
                'nested' => $nested,
            ];

            if ($category->children) {
                $data = array_merge($data, self::treeToList($category->children, ($nested + 1)));
            }
        }

        return $data;
    }
}
