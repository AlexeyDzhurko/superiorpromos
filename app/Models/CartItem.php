<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CartItem
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $quantity
 * @property float $regular_price
 * @property float $price
 * @property boolean $is_sale
 * @property boolean $later_size_breakdown
 * @property string $imprint_comment
 * @property boolean $tax_exemption
 * @property string $estimation_zip
 * @property string $estimation_shipping_method
 * @property string $estimation_shipping_code
 * @property float $estimation_shipping_price
 * @property string $own_account_number
 * @property string $own_shipping_system
 * @property string $shipping_method
 * @property string $received_date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $cart_id
 * @property-read \App\Models\Product $product
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereRegularPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereIsSale($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereLaterSizeBreakdown($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereImprintComment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereTaxExemption($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereEstimationZip($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereEstimationShippingMethod($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereEstimationShippingCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereEstimationShippingPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereOwnAccountNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereOwnShippingSystem($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereShippingMethod($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereReceivedDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereCartId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Cart $cart
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CartItemColor[] $cartItemColors
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CartItemProductOption[] $cartItemProductOptions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CartItemImprint[] $cartItemImprints
 * @property string $own_shipping_type
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereOwnShippingType($value)
 * @property string $custom_shipping_method
 * @property float $setup_price
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereCustomShippingMethod($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereSetupPrice($value)
 * @property-read \App\Models\CartItemSizeGroup $cartItemSizeGroup
 * @property string $art_file_name
 * @property string $art_file_path
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereArtFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereArtFilePath($value)
 * @property boolean $is_sample
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItem whereIsSample($value)
 * @property-read \App\Models\OrderItem $orderItem
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CartItemArtProof[] $cartItemArtProof
 */
class CartItem extends Model
{
    protected $fillable = [
        'later_size_breakdown',
        'imprint_comment',
        'tax_exemption',
        'estimation_zip',
        'estimation_shipping_method',
        'estimation_shipping_code',
        'estimation_shipping_price',
        'shipping_method',
        'quantity',
        'received_date',
        'is_sample',
    ];

    const ART_FILE_DIR = '/cart_items/art_files/';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    public function cartItemColors()
    {
        return $this->hasMany(CartItemColor::class);
    }

    public function cartItemProductOptions()
    {
        return $this->hasMany(CartItemProductOption::class);
    }

    public function cartItemImprints()
    {
        return $this->hasMany(CartItemImprint::class);
    }

    public function cartItemSizeGroup()
    {
        return $this->hasOne(CartItemSizeGroup::class);
    }

    public function getReceivedDateAttribute($received_date)
    {
        return ($received_date !== '0000-00-00' ? $received_date : null);
    }

    public function orderItem()
    {
        return $this->hasOne(OrderItem::class);
    }

    public function cartItemArtProof()
    {
        return $this->hasMany(CartItemArtProof::class);
    }
}
