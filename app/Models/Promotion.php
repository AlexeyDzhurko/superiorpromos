<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Promotion
 * @package App\Models
 */
class Promotion extends Model
{
    /**
     * Table name
     * @var string
     */
    protected $table = 'promotions';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $fillable = [
        'data'
    ];
}
