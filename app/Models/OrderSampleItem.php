<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OrderSampleItem
 *
 * @property integer $id
 * @property integer $product_id
 * @property-read \App\Models\Product $product
 * @property integer $order_sample_id
 * @property-read \App\Models\OrderSample $orderSample 
 * @property integer $stage_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Stage[] $stages
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereStageId($value)
 * @property-read \App\Models\Stage $stage
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderSampleItemColors[] $orderSampleItemColor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderSampleItemNote[] $note
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderSampleTrackingInformation[] $orderSampleTrackingInformation
 *
 * @property integer $quantity
 * @property float $price
 * @property bool $check_notes
 * @property boolean $auto_remind
 *
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @mixin \Eloquent
 */
class OrderSampleItem extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_sample_items';

    protected $fillable = [
        'auto_remind',
        'check_notes',
        'vendor_id',
        'shipping_first_name',
        'shipping_last_name',
        'shipping_middle_name',
        'shipping_title',
        'shipping_suffix',
        'shipping_company_name',
        'shipping_address_line_1',
        'shipping_address_line_2',
        'shipping_city',
        'shipping_state',
        'shipping_zip',
        'shipping_country',
        'shipping_province',
        'shipping_day_telephone',
        'billing_first_name',
        'billing_last_name',
        'billing_middle_name',
        'billing_title',
        'billing_suffix',
        'billing_company_name',
        'billing_address_line_1',
        'billing_address_line_2',
        'billing_city',
        'billing_state',
        'billing_zip',
        'billing_country',
        'billing_province',
        'billing_day_telephone',
    ];
    
    public function orderSample()
    {
        return $this->belongsTo(OrderSample::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function orderSampleItemColor()
    {
        return $this->hasMany(OrderSampleItemColors::class);
    }

    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    public function stages()
    {
        return $this->belongsToMany(Stage::class)->withPivot('created_at', 'user_id');
    }

    public function orderSampleTrackingInformation()
    {
        return $this->hasMany(OrderSampleTrackingInformation::class);
    }

    public function note()
    {
        return $this->hasMany(OrderSampleItemNote::class);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }
}
