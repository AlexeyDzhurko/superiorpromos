<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SizeGroup
 *
 * @property integer $id
 * @property string $name
 * @property array $sizes
 * @mixin \Eloquent
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SizeGroup whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SizeGroup whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SizeGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SizeGroup whereUpdatedAt($value)
 */
class SizeGroup extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'sizes'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function sizes()
    {
        return $this->hasMany(Size::class);
    }

    /**
     * @param array $data
     * @return array
     */
    public function addSizes(array $data)
    {
        return $this->sizes()->sync($data);
    }
}
