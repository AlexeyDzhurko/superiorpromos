<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ImprintPrice
 *
 * @property integer $id
 * @property integer $imprint_id
 * @property integer $vendor_id
 * @property integer $quantity
 * @property float $setup_price
 * @property float $item_price
 * @property float $color_setup_price
 * @property float $color_item_price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImprintPrice whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImprintPrice whereImprintId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImprintPrice whereVendorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImprintPrice whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImprintPrice whereSetupPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImprintPrice whereItemPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImprintPrice whereColorSetupPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImprintPrice whereColorItemPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImprintPrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ImprintPrice whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Imprint $imprint
 */
class ImprintPrice extends Model
{
    protected $fillable = [
        'quantity',
        'setup_price',
        'item_price',
        'color_setup_price',
        'color_item_price',
        'imprint_id'
    ];

    public function imprint()
    {
        return $this->belongsTo(Imprint::class);
    }
}
