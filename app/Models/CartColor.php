<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CartColor
 *
 * @property integer $id
 * @property integer $cart_id
 * @property integer $color_group_id
 * @property integer $color_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartColor whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartColor whereCartId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartColor whereColorGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartColor whereColorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartColor whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartColor whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CartColor extends Model
{
    //
}
