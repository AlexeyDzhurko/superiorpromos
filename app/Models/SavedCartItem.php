<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SavedCartItem
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $cart_item_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @property-read \App\Models\CartItem $cartItem
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SavedCartItem whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SavedCartItem whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SavedCartItem whereCartItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SavedCartItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SavedCartItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SavedCartItem extends Model
{
    protected $fillable = [
        'user_id',
        'cart_item_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cartItem()
    {
        return $this->belongsTo(CartItem::class);
    }
}
