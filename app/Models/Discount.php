<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

/**
 * App\Models\Discount
 *
 * @property integer $id
 * @property string $name
 * @property boolean $active
 * @property integer $position
 * @property string $type
 * @property string $discount_condition
 * @property string $discount_calc
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Discount whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Discount whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Discount whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Discount wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Discount whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Discount whereDiscountCondition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Discount whereDiscountCalc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Discount whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Discount whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Discount sorted()
 * @property string $code
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Discount whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Discount byCode($code)
 */
class Discount extends Model
{
    use SortableTrait;

    protected $fillable = [
        'name',
        'type',
        'discount_condition',
        'discount_calc',
        'code',
    ];

    public function scopeByCode($query, $code)
    {
        return $query->where('code', $code)
            ->where('active', true);
    }
}
