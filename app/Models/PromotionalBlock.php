<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PromotionalBlock
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category $category
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PromotionalBlock whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PromotionalBlock whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PromotionalBlock whereSlug($value)
 * @mixin \Eloquent
 */
class PromotionalBlock extends Model
{
    const TOP_BLOCK = 1;
    const MIDDLE_BLOCK = 2;
    const BOTTOM_BLOCK = 3;

    protected $table = 'promotional_blocks';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'slug',
    ];

    public static function getBlocksLabel($json = false)
    {
        if ($json) {
            return [
                self::TOP_BLOCK => 'top_block',
                self::MIDDLE_BLOCK => 'middle_block',
                self::BOTTOM_BLOCK => 'bottom_block',
            ];
        }

        return [
            self::TOP_BLOCK => 'Top Block',
            self::MIDDLE_BLOCK => 'Middle Block',
            self::BOTTOM_BLOCK => 'Bottom Block',
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'promotional_products', 'block_id', 'product_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'read_more_category_id');
    }
}
