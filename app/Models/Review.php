<?php

namespace App\Models;

use App\User;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition (
 *      definition="Review",
 *      @SWG\Property(property="text", type="string", example="Test"),
 *      @SWG\Property(property="reviewer_name", type="string", example="Test"),
 *      @SWG\Property(property="reviewer_email", type="string", example="Test"),
 *      @SWG\Property(property="rating", type="integer", example="1")
 * ),
 *
 * App\Models\Review
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $reviewer_name
 * @property string|null $reviewer_email
 * @property string|null $company_name
 * @property \DateTime|null $review_date
 * @property string $text
 * @property boolean $rating
 * @property boolean $approve
 * @property integer $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Product $product
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereReviewerName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereReviewerEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereRating($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereApprove($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Review whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Review extends Model
{
    protected $fillable = [
        'approve',
        'text',
        'reviewer_name',
        'reviewer_email',
        'company_name',
        'review_date',
        'rating',
        'product_id',
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'review_date' => 'date:Y-m-d'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
