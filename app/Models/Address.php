<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Address
 *
 * @SWG\Definition (
 *      definition="Address",
 *      @SWG\Property(property="user_id", type="string", example="3"),
 *      @SWG\Property(property="first_name", type="string", example="Tim"),
 *      @SWG\Property(property="middle_name", type="string", example="Aero"),
 *      @SWG\Property(property="last_name", type="string", example="White"),
 *      @SWG\Property(property="title", type="integer", example="Boxes"),
 *      @SWG\Property(property="suffix", type="string", example="123"),
 *      @SWG\Property(property="company_name", type="string", example="SkyTech"),
 *      @SWG\Property(property="address_line_1", type="string", example="70 Calle La Sombra, Camarillo, CA 93010, USA"),
 *      @SWG\Property(property="address_line_2", type="string", example="76 Calle La Sombra, Camarillo, CA 93010, USA"),
 *      @SWG\Property(property="city", type="string", example="Camarillo"),
 *      @SWG\Property(property="state", type="string", example="CA"),
 *      @SWG\Property(property="zip", type="string", example="93010"),
 *      @SWG\Property(property="country", type="string", example="USA"),
 *      @SWG\Property(property="province", type="string", example="California"),
 *      @SWG\Property(property="day_telephone", type="string", example="987-564-1111"),
 *      @SWG\Property(property="ext", type="string", example="988"),
 *      @SWG\Property(property="fax", type="string", example="227-514-1123"),
 *      @SWG\Property(property="is_po_box", type="string", example="1"),
 *      @SWG\Property(property="is_default", type="string", example="1"),
 *      @SWG\Property(property="type", type="string", example="1"),
 * ),
 * @property integer $id
 * @property integer $user_id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $title
 * @property string $suffix
 * @property string $company_name
 * @property string $address_line_1
 * @property string $address_line_2
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $country
 * @property string $province
 * @property string $day_telephone
 * @property string $ext
 * @property string $fax
 * @property boolean $is_po_box
 * @property string $type
 * @property boolean $is_default
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereMiddleName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereSuffix($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereCompanyName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereAddressLine1($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereAddressLine2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereState($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereZip($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereProvince($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereDayTelephone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereExt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereFax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereIsPoBox($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Address whereIsDefault($value)
 * @mixin \Eloquent
 */
class Address extends Model
{

    protected $table = 'addresses';

    const BILLING_TYPE = 1;
    const SHIPPING_TYPE = 2;
    const COMMON_ADDRESS = 0;
    const DEFAULT_ADDRESS = 1;

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'first_name',
        'middle_name',
        'last_name',
        'title',
        'suffix',
        'company_name',
        'address_line_1',
        'address_line_2',
        'city',
        'state',
        'zip',
        'country',
        'province',
        'day_telephone',
        'ext',
        'fax',
        'is_po_box',
        'is_default',
        'type',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
