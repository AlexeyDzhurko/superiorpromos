<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Cart
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $session
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cart whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cart whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cart whereSession($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cart whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cart whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CartItem[] $cartItems
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Discount $discount
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cart current($user)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cart withoutOrder()
 */
class Cart extends Model
{
    protected $fillable = [
        'user_id',
        'session',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cartItems()
    {
        return $this->hasMany(CartItem::class);
    }

    public function discount()
    {
        return $this->belongsTo(Discount::class);
    }

    /**
     * Cart that has not been processed
     *
     * @param $query
     * @param User $user
     * @return mixed
     *
     */
    public function scopeCurrent($query, User $user)
    {
        return $query
            ->withoutOrder()
            ->where(['carts.user_id' => $user->id]);
    }

    public function scopeWithoutOrder($query)
    {
        return $query
            ->selectRaw('carts.*')
            ->leftJoin('orders', 'orders.cart_id', '=', 'carts.id')
            ->whereRaw('orders.cart_id IS NULL');
    }
}
