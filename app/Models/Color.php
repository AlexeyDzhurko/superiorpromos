<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\Color
 *
 * @SWG\Definition (
 *      definition="Color",
 *      @SWG\Property (property="id", type="string", example="1"),
 *      @SWG\Property (property="title", type="string", example="White"),
 *      @SWG\Property (property="color_hex", type="string", example="#ffffff"),
 * ),
 * @property integer $id
 * @property string $name
 * @property string $color_hex
 * @property string $picture_src
 * @property integer $picture_width
 * @property integer $picture_height
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Color whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Color whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Color whereColorHex($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Color wherePictureSrc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Color wherePictureWidth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Color wherePictureHeight($value)
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Color whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Color whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ColorGroup[] $groups
 * @property integer $pms_color_id
 * @property-read \App\Models\PmsColor $pmsColor
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Color wherePmsColorId($value)
 */
class Color extends Model
{

    const IMAGES_DIR = 'colors/';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'color_hex', 'picture_src', 'picture_width', 'picture_height', 'pms_color_id'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    /**
     * @return BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany('App\Models\ColorGroup', 'color_color_group', 'color_id', 'group_id');
    }

    public function pmsColor()
    {
        return $this->belongsTo(PmsColor::class);
    }

    /**
     * @param array $data
     * @return array
     */
    public function addGroups(array $data)
    {
        return $this->groups()->sync($data);
    }
}
