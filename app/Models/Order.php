<?php

namespace App\Models;

use App\Search\Searchable;
use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Order
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property integer $user_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereUserId($value)
 * @property-read \App\User $user
 * @property-read \App\Models\Payment $payment
 * @property-read \App\Models\Stage $stage
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderItem[] $orderItems
 * @property string $shipping_first_name
 * @property string $shipping_middle_name
 * @property string $shipping_last_name
 * @property string $shipping_title
 * @property string $shipping_suffix
 * @property string $shipping_company_name
 * @property string $shipping_address_line_1
 * @property string $shipping_address_line_2
 * @property string $shipping_city
 * @property string $shipping_state
 * @property string $shipping_zip
 * @property string $shipping_country
 * @property string $shipping_province
 * @property string $shipping_day_telephone
 * @property string $shipping_ext
 * @property string $shipping_fax
 * @property string $billing_first_name
 * @property string $billing_middle_name
 * @property string $billing_last_name
 * @property string $billing_title
 * @property string $billing_suffix
 * @property string $billing_company_name
 * @property string $billing_address_line_1
 * @property string $billing_address_line_2
 * @property string $billing_city
 * @property string $billing_state
 * @property string $billing_zip
 * @property string $billing_country
 * @property string $billing_province
 * @property string $billing_day_telephone
 * @property string $billing_ext
 * @property string $billing_fax
 * @property string $multiple_location_comment
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereShippingFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereShippingMiddleName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereShippingLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereShippingTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereShippingSuffix($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereShippingCompanyName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereShippingAddressLine1($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereShippingAddressLine2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereShippingCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereShippingState($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereShippingZip($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereShippingCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereShippingProvince($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereShippingDayTelephone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereShippingExt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereShippingFax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereBillingFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereBillingMiddleName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereBillingLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereBillingTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereBillingSuffix($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereBillingCompanyName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereBillingAddressLine1($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereBillingAddressLine2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereBillingCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereBillingState($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereBillingZip($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereBillingCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereBillingProvince($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereBillingDayTelephone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereBillingExt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereBillingFax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereMultipleLocationComment($value)
 * @property integer $cart_id
 * @property-read \App\Models\Cart $cart
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereCartId($value)
 * @property string $discount_code
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereDiscountCode($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Reminder[] $reminders
 * @property integer $discount_id
 * @property-read \App\Models\Discount $discount
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereDiscountId($value)
 * @property float $price
 * @property float $total_price
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereTotalPrice($value)
 * @property float $discount_amount
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Order whereDiscountAmount($value)
 */
class Order extends Model implements Searchable
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    public function reminders()
    {
        return $this->hasMany(Reminder::class);
    }

    public function discount()
    {
        return $this->belongsTo(Discount::class);
    }

    public function payment()
    {
        return $this->hasOne(Payment::class);
    }

    public function getElasticDocument(): array
    {

        $reminders = $this->reminders->map(function (Reminder $reminder) {
            return [
                'type' => $reminder->type,
                'sent' => $reminder->sent,
                'date' => $reminder->date->toIso8601String(),
            ];
        })->toArray();

        $order_items = $this->orderItems->map(function($orderItem) {
            return [
                'id'          => $orderItem->id,
                'vendor_id'   => $orderItem->vendor_id,
                'product_id'  => $orderItem->product_id,
                'po_number'   => $orderItem->po_number ?? null,
                'not_paid'    => $orderItem->not_paid,
                'stage_id'    => $orderItem->stage_id,
                'check_notes' => $orderItem->check_notes,
//                'is_sample'   => $orderItem->cartItem['is_sample'],
            ];
        })->toArray();

        return [
            'id'                 => $this->id,
            'created_at'         => $this->created_at->toIso8601String(),
            'billing_first_name' => $this->billing_first_name,
            'billing_last_name'  => $this->billing_last_name,
            'billing_full_name'  => $this->billing_first_name . ' ' . $this->billing_last_name,
            'shipping_first_name' => $this->shipping_first_name,
            'shipping_last_name' => $this->shipping_last_name,
            'shipping_full_name'  => $this->shipping_first_name . ' ' . $this->shipping_last_name,
            'user_name' => $this->user ? $this->user->name : $this->shipping_first_name . ' ' . $this->shipping_last_name,
            'user_id'            => $this->user_id,
            'reminders'          => $reminders,
            'order_items'        => $order_items,
            'total_price'        => $this->total_price,
            'state'              => $this->shipping_state,
        ];
    }

    public static function getElasticMapping(): array
    {
        return [
            'id' => [
                'type' => 'integer',
                'include_in_all' => FALSE
            ],
            'created_at'=> [
                'type' => 'date',
            ],
            'billing_first_name' => [
                'type' => 'string',
                'analyzer' => 'standard',
                'fielddata' => true,
            ],
            'billing_last_name' => [
                'type' => 'string',
                'analyzer' => 'standard'
            ],
            'shipping_first_name' => [
                'type' => 'string',
                'analyzer' => 'standard'
            ],
            'shipping_last_name' => [
                'type' => 'string',
                'analyzer' => 'standard'
            ],
            'shipping_full_name' => [
                'type' => 'string',
                'analyzer' => 'standard'
            ],
            'user_name' => [
                'type' => 'string',
                'analyzer' => 'standard'
            ],
            /*'state' => [
                'type' => 'string',
                'analyzer' => 'standard'
            ],*/
            'user_id' => [
                'type' => 'long',
            ],
            'reminders' => [
                'type' => 'nested',
                'properties' => [
                    'type' => [
                        'type' => 'integer',
                    ],
                    'sent' => [
                        'type' => 'integer'
                    ],
                    'date' => [
                        'type' => 'date',
                    ]
                ]
            ],
            'total_price' => [
                'type' => 'double',
            ],
            'state' => [
                'type' => 'string',
                'analyzer' => 'standard'
            ],
            'order_items' => [
                'type' => 'nested',
                'properties' => [
                    'id' => [
                        'type' => 'integer',
                    ],
                    'product_id' => [
                        'type' => 'integer',
                    ],
                    'vendor_id' => [
                        'type' => 'integer',
                    ],
                    'po_number' => [
                        'type' => 'string',
                    ],
                    'not_paid' => [
                        'type' => 'boolean',
                    ],
                    'stage_id' => [
                        'type' => 'integer',
                    ],
                    'check_notes' => [
                        'type' => 'boolean',
                    ],
                    'is_sample' => [
                        'type' => 'boolean',
                    ]
                ]
            ],
        ];
    }

    public static function getIndexName(): string
    {
        return 'order';
    }

    public static function getElasticAggregationsParams()
    {
        return [
            'by_stage_id' => [
                'nested' => [
                    'path' => 'order_items'
                ],
                'aggs' => [
                    'by_stage_id' => [
                        'terms' => [
                            'field' => 'order_items.stage_id',
                            'size' => 999,
                        ]
                    ]
                ]
            ]
        ];
    }
}
