<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

/**
 * App\Models\Imprint
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $position
 * @property string $name
 * @property integer $max_colors
 * @property integer $color_group_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Product $product
 * @property \App\Models\ColorGroup $colorGroup
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ImprintColor $imprintColors
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ImprintPrice[] $imprintPrices
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Imprint whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Imprint whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Imprint wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Imprint whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Imprint whereMaxColors($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Imprint whereColorGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Imprint whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Imprint whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Imprint sorted()
 * @mixin \Eloquent
 */
class Imprint extends Model
{
    use SortableTrait;

    protected $fillable = [
        'name',
        'color_group_id',
        'max_colors'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function colorGroup()
    {
        return $this->belongsTo(ColorGroup::class);
    }

    public function imprintPrices()
    {
        return $this->hasMany(ImprintPrice::class);
    }

    public function imprintColors()
    {
        return $this->hasMany(ImprintColor::class);
    }
}
