<?php


namespace App\Models;


class Breadcrumb
{
    /** @var string|null $previousCategory */
    private static $previousCategory;
    /** @var array $previousList */
    private static $previousList;

    public static function render(Category $category)
    {
        return self::getCrumbs($category);
    }

    public static function getCrumbs(Category $category)
    {
        $crumbs = [];

        if (self::$previousCategory != $category || empty(self::$previousList)) {
            self::$previousCategory = $category;
            self::$previousList = $category->getParentsAndSelf($category);
        }

        foreach (self::$previousList as $node) {
            $crumbs[] = [
                'id' => $node->id,
                'name' => $node->name,
                'slug' => mb_strtolower(implode('/', self::$previousList->pluck('slug')->toArray())),
                'meta_title' => $node->meta_title,
                'meta_description' => $node->meta_description,
                'keywords' => $node->keywords,
            ];
        }

        return $crumbs;
    }
}
