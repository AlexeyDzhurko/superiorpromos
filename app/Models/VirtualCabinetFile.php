<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\VirtualCabinetFile
 *
 * @property integer $id
 * @property integer $order_item_id
 * @property string $file_name
 * @property string $file_path
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\OrderItem $orderItem
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VirtualCabinetFile whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VirtualCabinetFile whereOrderItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VirtualCabinetFile whereFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VirtualCabinetFile whereFilePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VirtualCabinetFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VirtualCabinetFile whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class VirtualCabinetFile extends Model
{
    public function orderItem()
    {
        return $this->belongsTo(OrderItem::class);
    }
}
