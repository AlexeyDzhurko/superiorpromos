<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Faq
 *
 * @SWG\Definition (
 *      definition="Faq",
 * @SWG\Property (property="id", type="integer", example="2"),
 * @SWG\Property (property="question", type="string", example="What new?"),
 * @SWG\Property (property="answer", type="string", example="New products"),
 * @SWG\Property (property="active", type="boolean", example="1"),
 * ),
 * @property integer $id
 * @property string $question
 * @property string $answer
 * @property boolean $active
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faq whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faq whereQuestion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faq whereAnswer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faq whereActive($value)
 * @mixin \Eloquent
 */
class Faq extends Model
{
    protected $table = 'faq';

    const ACTIVE = 1;
    const INACTIVE = 0;

    public $timestamps = false;

    protected $fillable = [
        'question',
        'answer',
        'active',
    ];

}
