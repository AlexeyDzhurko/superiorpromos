<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProductPrice
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $vendor_id
 * @property integer $quantity
 * @property float $setup_price
 * @property float $item_price
 * @property float $sale_item_price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductPrice whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductPrice whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductPrice whereVendorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductPrice whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductPrice whereSetupPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductPrice whereItemPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductPrice whereSaleItemPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductPrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductPrice whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Product $product
 */
class ProductPrice extends Model
{
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
