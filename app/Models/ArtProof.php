<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ArtProof
 *
 * @property integer $id
 * @property integer $order_item_id
 * @property boolean $customer_read
 * @property boolean $admin_read
 * @property integer $user_id
 * @property string $answered_at
 * @property string $note
 * @property string $customer_note
 * @property boolean $approved
 * @property boolean $approved_required
 * @property string $path
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\UserNote $user
 * @property-read \App\Models\OrderItem $orderItem
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ArtProof whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ArtProof whereOrderItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ArtProof whereCustomerRead($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ArtProof whereAdminRead($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ArtProof whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ArtProof whereAnsweredAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ArtProof whereNote($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ArtProof whereCustomerNote($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ArtProof whereApproved($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ArtProof whereApprovedRequired($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ArtProof wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ArtProof whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ArtProof whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property boolean $approve_required
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ArtProof whereApproveRequired($value)
 */
class ArtProof extends Model
{
    const ART_FILE_DIR = 'art_files/';
    const CUSTOMER_ART_FILE_DIR = '/customer_art_files/';

    protected $dates = [
        'answered_at',
    ];

    protected $fillable = [
        'note',
        'approve_required',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orderItem()
    {
        return $this->belongsTo(OrderItem::class);
    }
}
