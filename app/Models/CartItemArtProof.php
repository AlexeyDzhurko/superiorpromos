<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CartItemSize
 *
 * @property integer $id
 * @property integer $cart_item_id
 * @property string $name
 * @property string $path
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\CartItem $cartItem
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemSize whereId($value)
 * @mixin \Eloquent
 */
class CartItemArtProof extends Model
{
    public function cartItem()
    {
        return $this->belongsTo(CartItem::class);
    }
}
