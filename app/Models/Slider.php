<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

/**
 * App\Models\Slider
 *
 * @property integer $id
 * @property integer $position
 * @property string $url
 * @property string $image_src
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slider whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slider wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slider whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slider whereImageSrc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slider whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slider whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slider sorted()
 * @mixin \Eloquent
 */
class Slider extends Model
{
    use SortableTrait;
}
