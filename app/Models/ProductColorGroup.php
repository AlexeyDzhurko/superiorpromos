<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProductColorGroup
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColorGroup whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColorGroup whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColorGroup whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColorGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColorGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductColor[] $productColors
 * @property-read \App\Models\Product $product
 */
class ProductColorGroup extends Model
{
    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        // make sure to call the parent method
        parent::boot();

        static::addGlobalScope('ProductColorGroupRemoved', function (Builder $builder) {
            $builder->whereNull('deleted_at');
        });
    }

    public function productColors()
    {
        return $this->hasMany(ProductColor::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function delete()
    {
        $this->deleted_at = Carbon::now();

        return $this->save();
    }
}
