<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\ProductIcon
 *
 * @SWG\Definition (
 *      definition="ProductIcon",
 * @SWG\Property (property="icon_pic_src", type="string", example="clearance.png"),
 *      @SWG\Property(property="position", type="integer", example="1"),
 *      @SWG\Property(property="icon_pic_width", type="integer", example="200"),
 *      @SWG\Property(property="icon_pic_height", type="integer", example="100"),
 *      @SWG\Property(property="text", type="string", example="sale"),
 * ),
 * @property integer $id
 * @property string $text
 * @property string $icon_pic_src
 * @property integer $icon_pic_width
 * @property integer $icon_pic_height
 * @property integer $position
 * @property integer $old_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductIcon whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductIcon whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductIcon whereIconPicSrc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductIcon whereIconPicWidth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductIcon whereIconPicHeight($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductIcon wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductIcon whereOldId($value)
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductIcon whereName($value)
 */
class ProductIcon extends Model
{
    /**
     * Product Icon position code
     */
    const ICON_CODE_TOP_RIGHT = 1;
    const ICON_CODE_BOTTOM_RIGHT = 2;
    const ICON_CODE_TOP_LEFT = 3;
    const ICON_CODE_BOTTOM_LEFT = 4;

    /**
     * Product Icon position name
     */
    const ICON_NAME_TOP_RIGHT = 'Top right';
    const ICON_NAME_BOTTOM_RIGHT = 'Bottom right';
    const ICON_NAME_TOP_LEFT = 'Top left';
    const ICON_NAME_BOTTOM_LEFT = 'Bottom left';

    /**
     * Table name
     * @var string
     */
    protected $table = 'product_icons';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text', 'icon_pic_src', 'icon_pic_width', 'icon_pic_height', 'position', 'name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id', 'old_id'];

    /**
     * @return BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product');
    }
}
