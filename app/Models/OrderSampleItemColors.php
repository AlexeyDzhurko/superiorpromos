<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OrderSampleItemColors
 *
 * @property integer $id
 * @property integer $order_sample_item_id
 * @property integer $color_group_id
 * @property integer $color_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\OrderSampleItem $orderSampleItem
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderSampleItem whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderSampleItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderSampleItem whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\ProductColorGroup $colorGroup
 * @property-read \App\Models\ProductColor $color
 * */
class OrderSampleItemColors extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_sample_item_colors';

    protected $fillable = [
        'color_group_id',
        'color_id',
    ];

    public function orderSampleItem()
    {
        return $this->belongsTo(OrderSampleItem::class);
    }

    public function colorGroup()
    {
        return $this->belongsTo(ProductColorGroup::class, 'color_group_id');
    }

    public function color()
    {
        return $this->belongsTo(ProductColor::class, 'color_id');
    }
}
