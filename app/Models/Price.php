<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Price
 *
 * @property integer $id
 * @property integer $quantity
 * @property integer $percent
 * @property float $regular_price
 * @property float $catalog_price
 * @property float $net_price
 * @property float $sale_price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Price whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Price whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Price wherePercent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Price whereRegularPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Price whereCatalogPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Price whereNetPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Price whereSalePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Price whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Price whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Price extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quantity', 'percent', 'regular_price', 'catalog_price', 'net_price', 'sale_price',
    ];
}
