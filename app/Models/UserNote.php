<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserNote
 *
 * @SWG\Definition (
 *      definition="UserNote",
 *      @SWG\Property (property="id", type="string", example="1"),
 *      @SWG\Property (property="subject", type="string", example="Logo"),
 *      @SWG\Property (property="created_at", type="string", example="1258337531000"),
 *      @SWG\Property (property="note", type="string", example="<p>Hello, </p><p>Yes you may add a logo please email your logo to <a href='mailto:art@superiorpromos.com'>art@superiorpromos.com</a> </p><p>Thank you</p>"),
 *      @SWG\Property (property="customer_note", type="string", example="Thank you. We are very excited!"),
 *      @SWG\Property (property="approved_required", type="string", example="1"),
 *      @SWG\Property (property="answered_at", type="string", example="1258337531000"),
 *      @SWG\Property (property="status", type="string", example="approved"),
 * ),
 * @property integer $id
 * @property integer $order_item_id
 * @property boolean $customer_read
 * @property boolean $admin_read
 * @property integer $user_id
 * @property string $answered_at
 * @property string $subject
 * @property string $note
 * @property string $customer_note
 * @property boolean $approved
 * @property boolean $approved_required
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserNote whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserNote whereOrderItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserNote whereCustomerRead($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserNote whereAdminRead($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserNote whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserNote whereAnsweredAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserNote whereSubject($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserNote whereNote($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserNote whereCustomerNote($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserNote whereApproved($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserNote whereApprovedRequired($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserNote whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserNote whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\User $user
 * @property-read \App\Models\OrderItem $orderItem
 */
class UserNote extends Model
{
    protected $fillable = [
        'subject',
        'note',
        'approved',
        'approved_required',
        'customer_note'
    ];

    protected $dates = [
        'answered_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orderItem()
    {
        return $this->belongsTo(OrderItem::class);
    }
}
