<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OrderItem
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $quantity
 * @property float $price
 * @property float $options_price
 * @property boolean $check_notes
 * @property boolean $auto_remind
 * @property boolean $not_paid
 * @property string $art_file_name
 * @property string $art_file_path
 * @property string $po_number
 * @property string $invoice_file
 * @property boolean $tax_exempt
 * @property string $estimation_zip
 * @property string $estimation_shipping_method
 * @property string $estimation_price
 * @property string $own_shipping_type
 * @property string $own_account_number
 * @property string $own_shipping_system
 * @property string $own_shipping_method
 * @property integer $vendor_id
 * @property string $received_date
 * @property string $comment
 * @property string $shipping_first_name
 * @property string $shipping_middle_name
 * @property string $shipping_last_name
 * @property string $shipping_title
 * @property string $shipping_suffix
 * @property string $shipping_company_name
 * @property string $shipping_address_line_1
 * @property string $shipping_address_line_2
 * @property string $shipping_city
 * @property string $shipping_state
 * @property string $shipping_zip
 * @property string $shipping_country
 * @property string $shipping_province
 * @property string $shipping_day_telephone
 * @property string $shipping_ext
 * @property string $shipping_fax
 * @property string $billing_first_name
 * @property string $billing_middle_name
 * @property string $billing_last_name
 * @property string $billing_title
 * @property string $billing_suffix
 * @property string $billing_company_name
 * @property string $billing_address_line_1
 * @property string $billing_address_line_2
 * @property string $billing_city
 * @property string $billing_state
 * @property string $billing_zip
 * @property string $billing_country
 * @property string $billing_province
 * @property string $billing_day_telephone
 * @property string $billing_ext
 * @property string $billing_fax
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereOptionsPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereCheckNotes($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereAutoRemind($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereNotPaid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereArtFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereArtFilePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem wherePoNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereInvoiceFile($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereTaxExempt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereEstimationZip($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereEstimationShippingMethod($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereEstimationPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereOwnShippingType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereOwnAccountNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereOwnShippingSystem($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereOwnShippingMethod($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereVendorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereCartId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereReceivedDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereComment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereShippingFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereShippingMiddleName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereShippingLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereShippingTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereShippingSuffix($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereShippingCompanyName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereShippingAddressLine1($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereShippingAddressLine2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereShippingCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereShippingState($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereShippingZip($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereShippingCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereShippingProvince($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereShippingDayTelephone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereShippingExt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereShippingFax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereBillingFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereBillingMiddleName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereBillingLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereBillingTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereBillingSuffix($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereBillingCompanyName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereBillingAddressLine1($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereBillingAddressLine2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereBillingCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereBillingState($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereBillingZip($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereBillingCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereBillingProvince($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereBillingDayTelephone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereBillingExt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereBillingFax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $estimation_shipping_price
 * @property string $shipping_method
 * @property-read \App\Models\Product $product
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereEstimationShippingPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereShippingMethod($value)
 * @property string $imprint_comment
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereImprintComment($value)
 * @property integer $order_id
 * @property-read \App\Models\Order $order
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereOrderId($value)
 * @property string $tracking_shipping_date
 * @property string $tracking_number
 * @property string $tracking_shipping_company
 * @property string $tracking_note
 * @property integer $tracking_user_id
 * @property integer $stage_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Stage[] $stages
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereTrackingShippingDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereTrackingNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereTrackingShippingCompany($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereTrackingNote($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereTrackingUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereStageId($value)
 * @property-read \App\Models\Stage $stage
 * @property string $tracking_url
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereTrackingUrl($value)
 * @property-read \App\User $trackingUser
 * @property float $item_price
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereItemPrice($value)
 * @property float $shipping_price
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereShippingPrice($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BackNote[] $backNotes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VirtualCabinetFile[] $virtualCabinetFiles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserNote[] $userNotes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ArtProof[] $artProofs
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Payment[] $payments
 * @property integer $cart_item_id
 * @property-read \App\Models\CartItem $cartItem
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereCartItemId($value)
 * @property boolean $is_important
 * @property-read \App\Models\Vendor $vendor
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderItem whereIsImportant($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrackingInformation[] $trackingInformations
 */
class OrderItem extends Model
{
    protected $fillable = [
        'shipping_first_name',
        'shipping_last_name',
        'shipping_middle_name',
        'shipping_title',
        'shipping_suffix',
        'shipping_company_name',
        'shipping_address_line_1',
        'shipping_address_line_2',
        'shipping_city',
        'shipping_state',
        'shipping_zip',
        'shipping_country',
        'shipping_province',
        'shipping_day_telephone',
        'billing_first_name',
        'billing_last_name',
        'billing_middle_name',
        'billing_title',
        'billing_suffix',
        'billing_company_name',
        'billing_address_line_1',
        'billing_address_line_2',
        'billing_city',
        'billing_state',
        'billing_zip',
        'billing_country',
        'billing_province',
        'billing_day_telephone',
        'po_number',
        'vendor_id',
        'check_notes',
    ];

    public function setTrackingShippingDateAttribute($date)
    {
        return $this->attributes['tracking_shipping_date'] = Carbon::createFromFormat('d-m-Y', $date);
    }

    public function getTrackingShippingDateAttribute($date)
    {
        return $date ? $this->asDateTime($date)->format('m/d/y g:i A') : null;
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function stages()
    {
        return $this->belongsToMany(Stage::class)->withPivot('created_at', 'user_id');
    }

    public function backNotes()
    {
        return $this->hasMany(BackNote::class);
    }

    public function userNotes()
    {
        return $this->hasMany(UserNote::class);
    }

    public function virtualCabinetFiles()
    {
        return $this->hasMany(VirtualCabinetFile::class);
    }

    public function artProofs()
    {
        return $this->hasMany(ArtProof::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function cartItem()
    {
        return $this->belongsTo(CartItem::class);
    }

    public function trackingInformations()
    {
        return $this->hasMany(TrackingInformation::class);
    }

    public function getArtProofPathAttribute()
    {
        return \Storage::disk('public')->url($this->art_file_path);
    }
}
