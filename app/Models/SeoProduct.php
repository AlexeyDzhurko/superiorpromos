<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SeoProduct
 * old_var "treeman"
 *
 * @SWG\Definition (
 *      definition="SeoProduct",
 * @SWG\Property (property="url", type="string", example="bic-clic-stic-pen"),
 *      @SWG\Property(property="meta_title", type="string", example="Bic Clic Stic Pen - Promotional, Custom, Retractable Bic Pens"),
 *      @SWG\Property(property="keywords", type="string", example="Bic Clic Stic Pen - Promotional, Custom, Retractable Bic Pens"),
 *      @SWG\Property(property="meta_description", type="string", example="SuperiorPromos.com provides custom promotional pens including the retractable Bic Clic Stic Pen. Customize your Bic promotional pen today. "),
 * ),
 * @package App
 * @property integer $id
 * @property integer $product_id
 * @property string $name
 * @property string $url
 * @property string $meta_title
 * @property string $keywords
 * @property string $meta_description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SeoProduct whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SeoProduct whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SeoProduct whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SeoProduct whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SeoProduct whereMetaTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SeoProduct whereKeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SeoProduct whereMetaDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SeoProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SeoProduct whereUpdatedAt($value)
 */
class SeoProduct extends Model
{
    /**
     * Table name
     * @var string
     */
    protected $table = 'seo_products';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'url', 'meta_title', 'keywords', 'meta_description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id', 'product_id', 'created_at', 'updated_at', 'name'];

}
