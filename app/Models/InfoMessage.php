<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\InfoMessage
 *
 * @property integer $id
 * @property string $key
 * @property string $display_name
 * @property string $value
 * @property string $group
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InfoMessage whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InfoMessage whereKey($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InfoMessage whereDisplayName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InfoMessage whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InfoMessage whereGroup($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InfoMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InfoMessage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class InfoMessage extends Model
{
    //
}
