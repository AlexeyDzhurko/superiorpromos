<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Survey
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SurveyQuestion[] $surveyQuestions
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Survey whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Survey whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Survey whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Survey whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Survey extends Model
{
    public function surveyQuestions()
    {
        return $this->hasMany(SurveyQuestion::class);
    }
}
