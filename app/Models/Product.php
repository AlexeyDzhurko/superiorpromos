<?php

namespace App\Models;

use App\Search\Searchable;
use App\Support\ColorHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Rutorika\Sortable\BelongsToSortedManyTrait;

/**
 * App\Models\Product
 *
 * @SWG\Definition (
 *      definition="ProductList",
 *      @SWG\Property(property="total", type="integer", example="19575"),
 *      @SWG\Property(property="per_page", type="integer", example="10"),
 *      @SWG\Property(property="current_page", type="integer", example="1"),
 *      @SWG\Property(property="last_page", type="integer", example="1958"),
 *      @SWG\Property(property="next_page_url", type="string", example="http://localhost:8000/api/product?page=2"),
 *      @SWG\Property(property="prev_page_url", type="string", example="null"),
 *      @SWG\Property(property="from", type="integer", example="1"),
 *      @SWG\Property(property="to", type="integer", example="10"),
 *      @SWG\Property(property="data", type="array", items=@SWG\Schema(ref="#/definitions/ProductModel")),
 * ),
 * @SWG\Definition (
 *      definition="SearchResult",
 *      @SWG\Property(property="total", type="integer", example="19575"),
 *      @SWG\Property(property="per_page", type="integer", example="10"),
 *      @SWG\Property(property="current_page", type="integer", example="1"),
 *      @SWG\Property(property="total_pages", type="integer", example="1958"),
 *      @SWG\Property(property="data", type="array", items=@SWG\Schema(ref="#/definitions/Product")),
 * ),
 * @SWG\Definition (
 *      definition="ProductModel",
 *      @SWG\Property(property="id", type="integer", example="1"),
 *      @SWG\Property(property="name", type="string", example="Bic Clic Stic Revo Pen"),
 *      @SWG\Property(property="image_alt", type="string", example="promotional items and promotional products from SuperiorPromos.com"),
 *      @SWG\Property(property="image_src", type="string", example="CS.jpg"),
 *      @SWG\Property(property="additional_specifications", type="string", example="null"),
 *      @SWG\Property(property="dimensions", type="string", example="5L"),
 *      @SWG\Property(property="items_per_box", type="integer", example="0"),
 *      @SWG\Property(property="pricing_information", type="string", example="Price includes a one color imprint on the barrel. \r\n<li>For additional imprint colors on the barrel add a $.10 running charge per imprint color, per piece. </li>\r\n<li>Three imprint colors max on the barrel. </li>\r\n<li>A one color imprint is available on the clip for an additional $.13 per piece.</li>"),
 *      @SWG\Property(property="imprint_area", type="string", example="Barrel Imprint: 1 3/4\' W x 5/8\' H \r\n<li>Clip Imprint: 1\' W x 5/32\' H</li>"),
 *      @SWG\Property(property="description", type="string", example="\r\n<li>The most popular retractable in promotional products. </li>\r\n<li>1.2 miles of writing ink. </li>\r\n<li>Tungsten carbide ballpoint.</li>"),
 *      @SWG\Property(property="production_time_from", type="integer", example="1"),
 *      @SWG\Property(property="production_time_to", type="integer", example="1"),
 *      @SWG\Property(property="on_sale", type="integer", example="1"),
 *      @SWG\Property(property="product_icon_id", type="integer", example="1"),
 *      @SWG\Property(property="product_extra_images", type="array", items=@SWG\Schema(ref="#/definitions/ProductExtraImage")),
 *      @SWG\Property(property="seo_product", type="array", items=@SWG\Schema(ref="#/definitions/SeoProduct")),
 *      @SWG\Property(property="products_icon", type="array", items=@SWG\Schema(ref="#/definitions/ProductIcon")),
 *      @SWG\Property (property="product_color_groups", type="array", items=@SWG\Schema(ref="#/definitions/ProductColorGroup")),
 *      @SWG\Property (property="reviews", type="array", items=@SWG\Schema(ref="#/definitions/Review")),
 * ),
 * Class Product
 * old_var "dt_product"
 * @package App
 * @property integer $id
 * @property string $image_alt
 * @property string $additional_specifications
 * @property string $dimensions
 * @property string $items_per_box
 * @property string $pricing_information
 * @property string $imprint_area
 * @property string $description
 * @property integer $production_time_from
 * @property float $box_weight
 * @property integer $old_product_id
 * @property integer $production_time_to
 * @property boolean $on_sale
 * @property float $custom_shipping_cost
 * @property boolean $custom_shipping_method
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $product_icon_id
 * @property string $zip_from
 * @property string $name
 * @property boolean $active
 * @property-read \App\Models\SeoProduct $seoProduct
 * @property-read \App\Models\ProductIcon $product_icon
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductExtraImage[] $productExtraImages
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Option[] $options
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereImageAlt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereAdditionalSpecifications($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereDimensions($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereItemsPerBox($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product wherePricingInformation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereImprintArea($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereProductionTimeFrom($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereBoxWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereOldProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereProductionTimeTo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereOnSale($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereCustomShippingCost($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereCustomShippingMethod($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereProductIconId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereZipFrom($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereActive($value)
 * @property string $image_src
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereImageSrc($value)
 * @property string $sage_id
 * @property string $url
 * @property string $url_prefix
 * @property integer $vendor_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Price[] $prices
 * @property-read \Baum\Extensions\Eloquent\Collection|\App\Models\Category[] $categories
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ImprintLocation[] $imprintLocations
 * @property-read \App\Models\PromotionalBlock $promotionalBlock
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereSageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereUrlPrefix($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereVendorId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vendor[] $vendors
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ColorGroup[] $colorGroups
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductOption[] $productOptions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Imprint[] $imprints
 * @property string $keywords
 * @property string $meta_title
 * @property string $meta_description
 * @property integer $popularity
 * @property integer $quantity_per_box
 * @property string $brand
 * @property string $gender
 * @property string $age_group
 * @property string $material
 * @property string $pattern
 * @property string $google_product_category
 * @property string $shipping_additional_type
 * @property float $shipping_additional_value
 * @property string $video
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereKeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereMetaTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereMetaDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product wherePopularity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereQuantityPerBox($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereBrand($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereGender($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereAgeGroup($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereMaterial($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product wherePattern($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereGoogleProductCategory($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereShippingAdditionalType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereShippingAdditionalValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereVideo($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductPrice[] $productPrices
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductColorGroup[] $productColorGroups
 * @property integer $size_group_id
 * @property-read \App\Models\SizeGroup $sizeGroup
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Review[] $reviews
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereSizeGroupId($value)
 * @property string $video_url
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereVideoUrl($value)
 */
class Product extends Model implements Searchable
{
    use BelongsToSortedManyTrait;

    /**
     * Table name
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image_alt',
        'additional_specifications',
        'dimensions',
        'items_per_box',
        'pricing_information',
        'imprint_area',
        'description',
        'production_time_from',
        'production_time_to',
        'box_weight',
        'on_sale',
        'custom_shipping_cost',
        'url',
        'url_prefix',
        'sage_id',
        'custom_shipping_method',
        'old_product_id',
        'zip_from',
        'image_src',
        'name',
        'url',
        'meta_title',
        'keywords',
        'image_alt',
        'google_product_category',
        'brand',
        'gender',
        'age_group',
        'material',
        'pattern',
        'meta_description',
        'quantity_per_box',
        'box_weight',
        'shipping_additional_type',
        'shipping_additional_value',
        'production_time_from',
        'production_time_to',
        'sage_id',
        'zip_from',
        'dimensions',
        'popularity',
        'description',
        'imprint_area',
        'pricing_information',
        'active',
        'on_sale',
        'product_icon_id',
        'size_group_id',
        'video',
        'video_url',
        'custom_shipping_method',
        'custom_shipping_cost'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'box_weight',
        'old_product_id',
        'created_at',
        'updated_at',
        'active',
        'custom_shipping_method',
        'custom_shipping_cost',
        'zip_from',
        'additional_specifications'
    ];

    /**
     * Get productSeo data
     * @return HasOne
     */
    public function seoProduct()
    {
        return $this->hasOne('App\Models\SeoProduct', 'product_id');
    }

    /**
     * @return HasOne
     */
    public function product_icon()
    {
        return $this->hasOne(ProductIcon::class);
    }

    /**
     * @return HasMany
     */
    public function productExtraImages()
    {
        return $this->hasMany('App\Models\ProductExtraImage');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('App\Models\Category', 'products_categories', 'product_id', 'category_id')
            ->withPivot('type', 'position');
    }


    public function promotionalBlock()
    {
        return $this->belongsTo('App\Models\PromotionalBlock');
    }

    /**
     * @return HasMany
     */
    public function colorGroups()
    {
        return $this->hasMany(ColorGroup::class);

    }

    public function vendors()
    {
        return $this->belongsToSortedMany(Vendor::class, 'position', 'product_vendor')->withPivot(['sku', 'position']);
    }

    public function productOptions()
    {
        return $this->hasMany(ProductOption::class);
    }

    public function imprints()
    {
        return $this->hasMany(Imprint::class);
    }

    public function productPrices()
    {
        return $this->hasMany(ProductPrice::class)
            ->orderBy('quantity', 'asc');
    }

    public function productColorGroups()
    {
        return $this->hasMany(ProductColorGroup::class);
    }

    public function sizeGroup()
    {
        return $this->belongsTo(SizeGroup::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function scopeCategorized($query, Category $category = null)
    {

        if (is_null($category)) {
            return $query->with('categories');
        }

        $categoryIds = $category->getDescendantsAndSelf()->pluck('id');
        $result = $query->with('categories')
            ->join('products_categories', 'products_categories.product_id', '=', 'products.id')
            ->whereIn('products_categories.category_id', $categoryIds)
            ->where('products.active', 1);

        return $result;
    }

    public function getElasticDocument(): array
    {
        $vendors = $this->vendors->map(function ($vendor) {
            return [
                'vendor_id' => $vendor->id,
                'sku' => $vendor->pivot->sku,
            ];
        });

        $position = $this->categories->map(function ($pos) {
            return $pos->pivot->position;
        });

        $prices = $this->productPrices->map(function ($price) {
            return [
                'quantity' => $price->quantity,
                'setup_price' => $price->setup_price,
                'item_price' => $price->item_price,
                'sale_item_price' => $this->on_sale ? $this->sale_item_price : $this->item_price
            ];
        });

        $categories = $this->categories->map(function ($category) {
            return [
                'id' => $category->id,
                'name' => $category->name,
                'slug' => implode('/', $category->getAncestorsAndSelf()->pluck('slug')->toArray())
            ];
        });

        $colors = [];

        $this->productColorGroups->load('productColors');
        $colorImagesStatus = 'complete';
        foreach ($this->productColorGroups as $productColorGroup) {
            /** @var ProductColorGroup $productColorGroup */
            foreach ($productColorGroup->productColors as $productColor) {
                /** @var ProductColor $productColor */
                $hex = $productColor->color->color_hex;
                $colors[] = [
                    'color_id' => $productColor->color_id,
                    'image_src' => $productColor->image_src,
                    'hex' => $hex,
                    'color' => ColorHelper::getMatchColor($hex),
                ];
                if (is_null($productColor->image_src)) {
                    $colorImagesStatus = 'none';
                }
            }
        }

        if (count($colors) == 0) {
            $colorImagesStatus = 'empty';
        }

        $priceField = $this->on_sale ? 'sale_item_price' : 'item_price';
        $lowestPrice = $this->productPrices->min($priceField);

        return [
            'id' => $this->id,
            'name' => strlen($this->name) > 0 ? $this->name : 'empty',
            'active' => $this->active,
            'image_src' => $this->image_src,
            'sage_id' => $this->sage_id,
            'categories' => $categories,
            'position' => $position,
            'popularity' => $this->popularity,
            'vendors' => $vendors,
            'colors' => $colors,
            'created_at' => $this->created_at->toIso8601String(),
            'color_images_status' => $colorImagesStatus,
            'suggest' => [strval($this->id), (strlen($this->name) > 0 ? $this->name : 'empty')],
            'name_sort' => strlen($this->name) > 0 ? $this->name : 'empty',
            'prices' => $prices,
            'lowest_price' => $lowestPrice,
        ];
    }

    public static function getElasticMapping(): array
    {
        return [
            'id' => [
                'type' => 'integer',
                'include_in_all' => false
            ],
            'name' => [
                'type' => 'string',
                'analyzer' => 'autocomplete',
                'search_analyzer' => 'autocomplete_search'
            ],
            'image_src' => [
                'type' => 'string',
            ],
            'active' => [
                'type' => 'integer',
            ],
            'sage_id' => [
                'type' => 'string',
                'analyzer' => 'standard'
            ],
            'categories' => [
                'type' => 'nested',
                'properties' => [
                    'id' => [
                        'type' => 'integer',
                    ],
                    'name' => [
                        'type' => 'string',
                        'analyzer' => 'standard'
                    ],
                    'slug' => [
                        'type' => 'string'
                    ],
                ]
            ],
            'popularity' => [
                'type' => 'integer',
            ],
            'vendors' => [
                'type' => 'nested',
                'properties' => [
                    'vendor_id' => [
                        'type' => 'integer',
                    ],
                    'sku' => [
                        'type' => 'string',
                        'analyzer' => 'standard'
                    ],
                ]
            ],
            'colors' => [
                'type' => 'nested',
                'properties' => [
                    'color_id' => [
                        'type' => 'integer',
                    ],
                    'image_src' => [
                        'type' => 'string',
                        'analyzer' => 'simple'
                    ],
                    'hex' => [
                        'type' => 'string',
                        'analyzer' => 'simple'
                    ],
                    'color' => [
                        'type' => 'string',
                        'analyzer' => 'simple'
                    ],
                ]
            ],
            'created_at' => [
                'type' => 'date',
            ],
            'color_images_status' => [
                'type' => 'string',
                'analyzer' => 'simple'
            ],
            'suggest' => [
                'type' => 'completion',
            ],
            'name_sort' => [
                'type' => 'string',
                'index' => 'not_analyzed',
            ],
            'prices' => [
                'type' => 'nested',
                'properties' => [
                    'quantity' => [
                        'type' => 'integer',
                    ],
                    'setup_price' => [
                        'type' => 'float',
                    ],
                    'item_price' => [
                        'type' => 'float',
                    ],
                    'sale_item_price' => [
                        'type' => 'float',
                    ],
                ],
            ],
            'lowest_price' => [
                'type' => 'float',
            ]
        ];
    }

    public static function getIndexName(): string
    {
        return 'product';
    }

}
