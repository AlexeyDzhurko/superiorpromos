<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CartItemSize
 *
 * @property integer $id
 * @property string $qty
 * @property integer $size_id
 * @property integer $cart_item_size_group_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Size $size
 * @property-read \App\Models\CartItemSizeGroup $cartItemSizeGroup
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemSize whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemSize whereCartItemSizeGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemSize whereSizeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemSize whereQty($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemSize whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemSize whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CartItemSize extends Model
{
    public function size()
    {
        return $this->belongsTo(Size::class);
    }

    public function cartItemSizeGroup()
    {
        return $this->belongsTo(CartItemSizeGroup::class);
    }
}
