<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OrderSampleTrackingInformation
 *
 * @property integer $id
 * @property \DateTime $tracking_shipping_date
 * @property string $tracking_number
 * @property string $tracking_shipping_company
 * @property string $tracking_note
 * @property string $tracking_url
 * @property integer $tracking_user_id
 * @property integer $order_sample_item_id
 * @property bool $tracking_notify_customer
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $trackingUser
 * @property-read \App\Models\OrderSampleItem $orderSampleItem
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderSampleTrackingInformation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderSampleTrackingInformation whereTrackingShippingDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderSampleTrackingInformation whereTrackingNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderSampleTrackingInformation whereTrackingShippingCompany($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderSampleTrackingInformation whereTrackingNote($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderSampleTrackingInformation whereTrackingUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderSampleTrackingInformation whereTrackingUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderSampleTrackingInformation whereOrderSampleItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderSampleTrackingInformation whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrderSampleTrackingInformation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OrderSampleTrackingInformation extends Model
{
    protected $table = 'order_sample_tracking_info';

    protected $fillable = [
        'tracking_shipping_date',
        'tracking_number',
        'tracking_shipping_company',
        'tracking_note',
        'tracking_url',
        'tracking_notify_customer'
    ];

    protected $dates = [
        'tracking_shipping_date' => 'date:Y-m-d',
    ];

    public function trackingUser()
    {
        return $this->belongsTo(User::class, 'tracking_user_id');
    }

    public function orderSampleItem()
    {
        return $this->belongsTo(OrderSampleItem::class, 'order_sample_item_id');
    }
}
