<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\Vendor
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $fax
 * @property string $email
 * @property string $state
 * @property string $city
 * @property string $country
 * @property string $zipCode
 * @property string $address1
 * @property string $address2
 * @property string $supplierId
 * @property string $site
 * @property integer $paymentTemplate
 * @property integer $oldVendorId
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @mixin \Eloquent
 * @property string $zip_code
 * @property string $address_1
 * @property string $address_2
 * @property string $supplier_id
 * @property integer $payment_template
 * @property boolean $active
 * @property integer $old_vendor_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor whereFax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor whereState($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor whereCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor whereZipCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor whereAddress1($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor whereAddress2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor whereSupplierId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor whereSite($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor wherePaymentTemplate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor whereOldVendorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ImportProduct[] $importProducts
 * @property integer $state_id
 * @property integer $country_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\State[] $states
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor whereStateId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor whereCountryId($value)
 * @property integer $payment_information_template_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vendor wherePaymentInformationTemplateId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderItem[] $orderItems
 */
class Vendor extends Model
{
    const EMPTY_VENDOR_ID = null;

    protected $fillable = ['name', 'phone', 'fax', 'email', 'state', 'city', 'country', 'zip_code', 'address_1',
        'supplier_id', 'state_id', 'country_id', 'payment_information_template_id'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    /**
     * @return BelongsToMany
     */
    public function importProducts()
    {
        return $this->belongsToMany(ImportProduct::class, 'import_products');
    }

    public function states()
    {
        return $this->belongsToMany(State::class);
    }

    /**
     * @return BelongsToMany
     */
    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }
}
