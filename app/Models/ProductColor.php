<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProductColor
 *
 * @property integer $id
 * @property integer $product_color_group_id
 * @property integer $color_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColor whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColor whereProductColorGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColor whereColorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColor whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColor whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Color $color
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductColorPrice[] $productColorPrices
 * @property string $image_src
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductColor whereImageSrc($value)
 */
class ProductColor extends Model
{
    protected $fillable = [
        'color_id',
    ];


    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        // make sure to call the parent method
        parent::boot();

        static::addGlobalScope('ProductColorGroupRemoved', function (Builder $builder) {
            $builder->whereNull('deleted_at');
        });
    }

    public function color()
    {
        return $this->belongsTo(Color::class);
    }

    public function productColorPrices()
    {
        return $this->hasMany(ProductColorPrice::class);
    }

    public function delete()
    {
        $this->deleted_at = Carbon::now();

        return $this->save();
    }
}
