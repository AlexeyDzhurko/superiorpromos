<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CartItemSizeGroup
 *
 * @property integer $id
 * @property integer $size_group_id
 * @property integer $cart_item_id
 * @property-read \App\Models\CartItemSize $cartItemSizes
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\CartItem $cartItem
 * @property-read \App\Models\SizeGroup $sizeGroup
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemSizeGroup whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemSizeGroup whereCartItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemSizeGroup whereSizeGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemSizeGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemSizeGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CartItemSizeGroup extends Model
{
    public function cartItem()
    {
        return $this->belongsTo(CartItem::class);
    }

    public function sizeGroup()
    {
        return $this->belongsTo(SizeGroup::class);
    }

    public function cartItemSizes()
    {
        return $this->hasMany(CartItemSize::class);
    }
}
