<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

/**
 * App\Models\Stage
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $position
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stage whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stage whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stage whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stage wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stage whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stage sorted()
 * @mixin \Eloquent
 */
class Stage extends Model
{
    use SortableTrait;

    const COMPLETE_ID = 8;
    const DEFAULT_ID = 164;
}
