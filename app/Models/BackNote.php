<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BackNote
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $order_item_id
 * @property integer $user_id
 * @property string $note
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BackNote whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BackNote whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BackNote whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BackNote whereOrderItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BackNote whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\BackNote whereNote($value)
 * @mixin \Eloquent
 * @property-read \App\User $user
 * @property-read \App\Models\OrderItem $orderItem
 */
class BackNote extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orderItem()
    {
        return $this->belongsTo(OrderItem::class);
    }
}
