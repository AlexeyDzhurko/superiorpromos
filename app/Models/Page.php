<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

/**
 * App\Models\Page
 *
 * @property integer $id
 * @property integer $author_id
 * @property string $title
 * @property string $excerpt
 * @property string $body
 * @property string $image
 * @property string $slug
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $meta_title
 * @property integer $position
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereAuthorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereExcerpt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereBody($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereMetaDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereMetaKeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page whereMetaTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Page wherePosition($value)
 * @mixin \Eloquent
 */
class Page extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'body',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'status',
    ];

    public function save(array $options = [])
    {
        // Add the user id as the Author of the post
        $this->author_id = Auth::user()->id;
        parent::save();
        // after save code
    }
}
