<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PaymentInformationTemplate
 *
 * @property integer $id
 * @property string $name
 * @property string $single_body
 * @property string $multiple_body
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PaymentInformationTemplate whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PaymentInformationTemplate whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PaymentInformationTemplate whereSingleBody($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PaymentInformationTemplate whereMultipleBody($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PaymentInformationTemplate whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PaymentInformationTemplate whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PaymentInformationTemplate extends Model
{
    protected $fillable = [
        'name',
        'single_body',
        'multiple_body',
    ];
}
