<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PmsColor
 *
 * @property integer $id
 * @property string $name
 * @property string $color_hex
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PmsColor whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PmsColor whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PmsColor whereColorHex($value)
 * @mixin \Eloquent
 */
class PmsColor extends Model
{
    public $timestamps = false;
}
