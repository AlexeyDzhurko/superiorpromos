<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CartItemProductOption
 *
 * @property integer $id
 * @property integer $cart_item_id
 * @property integer $product_option_id
 * @property string $name
 * @property float $setup_price
 * @property float $item_price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemProductOption whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemProductOption whereCartItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemProductOption whereProductOptionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemProductOption whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemProductOption whereSetupPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemProductOption whereItemPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemProductOption whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CartItemProductOption whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\ProductOption $productOption
 * @property-read \App\Models\CartItem $cartItem
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CartItemProductSubOption[] $cartItemProductSubOptions
 */
class CartItemProductOption extends Model
{
    public function productOption()
    {
        return $this->belongsTo(ProductOption::class);
    }

    public function cartItem()
    {
        return $this->belongsTo(CartItem::class);
    }

    public function cartItemProductSubOptions()
    {
        return $this->hasMany(CartItemProductSubOption::class);
    }
}
