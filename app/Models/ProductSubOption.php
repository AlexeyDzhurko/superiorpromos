<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProductSubOption
 *
 * @property integer $id
 * @property integer $product_option_id
 * @property integer $position
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductSubOptionPrice[] $productSubOptionPrices
 * @property-read \App\Models\ProductOption $productOption
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductSubOption whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductSubOption whereProductOptionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductSubOption wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductSubOption whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductSubOption whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductSubOption whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ProductSubOption extends Model
{
    public function productSubOptionPrices()
    {
        return $this->hasMany(ProductSubOptionPrice::class);
    }

    public function productOption()
    {
        return $this->belongsTo(ProductOption::class);
    }
}
