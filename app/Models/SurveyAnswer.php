<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SurveyAnswer
 *
 * @property integer $id
 * @property integer $survey_question_id
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SurveyAnswer whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SurveyAnswer whereSurveyQuestionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SurveyAnswer whereName($value)
 * @mixin \Eloquent
 * @property-read \App\Models\SurveyQuestion $surveyQuestion
 */
class SurveyAnswer extends Model
{
    public $timestamps = false;

    public function surveyQuestion()
    {
        return $this->belongsTo(SurveyQuestion::class);
    }
}
