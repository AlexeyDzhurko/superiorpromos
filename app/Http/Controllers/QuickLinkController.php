<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Transformers\QuickLinkTransformer;

class QuickLinkController extends Controller
{
    /**
     *  @SWG\Get(
     *     path="/api/quick-links",
     *     tags={"Quick Links"},
     *     summary="Quick Links",
     *     description="Quick Links",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *              @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/QuickLink")),
     *         ),
     *     )
     * )
     * Display a listing of the Quick Links.
     *
     * @return \Spatie\Fractal\Fractal
     */
    public function index()
    {
        return fractal()->collection(Category::where('is_quick_link', 1)->where('active', 1)->get(), new QuickLinkTransformer());
    }
}
