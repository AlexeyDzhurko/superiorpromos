<?php


namespace App\Http\Controllers;


use App\Models\Block;
use App\Transformers\BlockTransformer;
use Illuminate\Http\Response;

/**
 *
 *  @SWG\Get(
 *     path="/api/block",
 *     tags={"Editable Blocks"},
 *     summary="List of Editable Blocks",
 *     description="List of active Editable Blocks",
 *     produces={"application/json"},
 *     consumes={"application/json"},
 *
 *     @SWG\Response(
 *         response=200,
 *         description="OK",
 *              @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/Block")),
 *         ),
 *     )
 * )
 * Display a listing of active Editable Blocks.
 *
 */
class BlockController extends Controller
{
    public function index()
    {
        return fractal()->collection(Block::where('active', true)->get(), new BlockTransformer());
    }

    /**
     *
     * @SWG\Get(
     *     path="/api/block/{key}",
     *     tags={"Editable Blocks"},
     *     summary="Get the Editable Block by the key",
     *     description="Editable Block by the key",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *          name="key",
     *          in="path",
     *          description="The key of the block",
     *          type="string"
     *      ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *              @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/Block")),
     *         ),
     *     )
     * )
     * Display Editable Block by the key.
     *
     * @param string $key
     * @return Response
     */
    public function getByKey(string $key)
    {
        return fractal()->collection(Block::where([['active', true],['key' , $key]])->get(), new BlockTransformer());
    }

}
