<?php

namespace App\Http\Controllers;

use App\Contracts\MailService;
use App\Http\Requests\QuickQuoteEmailRequest;
use App\Http\Requests\QuickQuoteRequest;
use App\Models\Category;
use App\Models\PromotionalBlock;
use App\Models\Breadcrumb;
use App\Notifications\BigMailer\QuickQuoteNotification;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Models\Product;
use App\Contracts\ShippingService;
use App\Http\Requests\ShippingCostRequest;
use App\Search\SearchBuilder;
use App\Services\PDFService;
use App\Transformers\ProductBreadcrumbsTransformer;
use App\Transformers\ProductSearchTransformer;
use App\Transformers\ProductTransformer;
use App\Transformers\PromotionalBlocksTransformer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Exception\NotFoundException;
use Symfony\Component\HttpFoundation\Request;


class ProductController extends Controller
{
    /**
     * UPS service
     * @var mixed
     */
    protected $shippingService;

    /**
     * @var int
     */
    protected $defaultWeight = 25;

    /**
     * ProductController constructor.
     * @param ShippingService $shippingService
     */
    public function __construct(ShippingService $shippingService)
    {
        $this->shippingService = $shippingService;
    }

    /**
     *  @SWG\Get(
     *     path="/api/product/",
     *     tags={"Products"},
     *     summary="List Products",
     *     description="List of Products",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDAwXC9hcGlcL2xvZ2luIiwiaWF0IjoxNDgxNjQxODQ0LCJleHAiOjE0ODE2NDU0NDQsIm5iZiI6MTQ4MTY0MTg0NCwianRpIjoiNjU3MmEzNDYxOWU1YjEyODIyNjdlYWQ4ODQ2NDI5NjIifQ.mpx7LhtwLtrb0ho7r5U5DVg0JGMlwmHA4-moOKhIMDw",
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *               @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/ProductList")),
     *     )
     * )
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $amount = env('PRODUCTS_ON_PAGE', 10);

        return ProductRepository::getProductsList($amount);
    }

    /**
     * @SWG\Get(
     *     path="/api/product/{currentProduct}",
     *     tags={"Products"},
     *     summary="Display product by id",
     *     description="Show some product and product options by product id",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="currentProduct",
     *          in="path",
     *          description="Product full slug",
     *          required=true,
     *          type="string"
     *      ),
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDAwXC9hcGlcL2xvZ2luIiwiaWF0IjoxNDgxNjQxODQ0LCJleHAiOjE0ODE2NDU0NDQsIm5iZiI6MTQ4MTY0MTg0NCwianRpIjoiNjU3MmEzNDYxOWU1YjEyODIyNjdlYWQ4ODQ2NDI5NjIifQ.mpx7LhtwLtrb0ho7r5U5DVg0JGMlwmHA4-moOKhIMDw",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *               @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/Product")),
     *     )
     * )
     * @param string $path
     * @return Response
     */
    public function show(string $path)
    {
        try {
            $segments = explode('/', $path);
            $productSlug = end($segments);
            $product = ProductRepository::getProductBySlug($productSlug);
            $categorySlug = prev($segments);
            $category = CategoryRepository::getCategoryBySlug($categorySlug);

            if (is_null($product) || is_null($category)) {
                throw new NotFoundException('', 404);
            }

            $parameters = [
                'categories' => [$category->id]
            ];

            $productData = fractal()->item($product, new ProductTransformer())->toArray();

            return fractal()->item($productData, new ProductBreadcrumbsTransformer($parameters));

        } catch (\Exception $exception) {
            throw new NotFoundException('', 404);
        }
    }

    /**
     * @SWG\Get(
     *     path="/api/product/viewed-products",
     *     tags={"Products"},
     *     summary="Display list of viewed products",
     *     description="Show list of viewed products",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="products",
     *          in="query",
     *          description="Array of product's ids",
     *          required=true,
     *          type="string"
     *      ),
     *     @SWG\Response(
     *         response=200,
     *         description = "Success"
     *     )
     * )
     * @param Request $request
     * @return mixed
     */
    public function view(Request $request)
    {
        $products = $request->get('products');

        return ProductRepository::getProductsByIds($products ?? []);
    }

    /**
     * @SWG\Get(
     *     path="/api/product/get-shipping-cost",
     *     tags={"Products"},
     *     summary="Get list of shipping cost of estimates",
     *     description="Get list of shipping cost of estimates",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="product_id",
     *         in="query",
     *         description="Product ID",
     *         required=true,
     *         type="string",
     *         default="1022",
     *     ),
     *     @SWG\Parameter(
     *         name="zip_code",
     *         in="query",
     *         description="zip code",
     *         required=true,
     *         type="string",
     *         default="11229",
     *     ),
     *     @SWG\Parameter(
     *         name="quantity",
     *         in="query",
     *         description="Quantity of products",
     *         required=true,
     *         type="string",
     *         default="72",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description = "Success",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="service", type="string", example="UPS Ground"),
     *             @SWG\Property(property="cost", type="float", example="13.19"),
     *             @SWG\Property(property="time_in_transit", type="string", example="4"),
     *             @SWG\Property(property="code", type="string", example="GND"),
     *         ),
     *     )
     * )
     *
     * @param ShippingCostRequest $request
     */
    public function shippingCost(ShippingCostRequest $request)
    {
        $shippingRate = $this->shippingService
            ->getShippingRate($request->zip_code, Product::findOrFail($request->product_id), $request->quantity);

        return \response()->json($shippingRate);
    }

    /**
     * @SWG\Get(
     *     path="/api/product/promotional-products/",
     *     tags={"Products"},
     *     summary="Display Promotional Products",
     *     description="Display Promotional Products",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *         @SWG\Schema(
     *              @SWG\Property(property="top_block", properties={
     *                 @SWG\Property(property="title", type="string", example="Promo"),
     *                 @SWG\Property(property="slug", type="string", example="/Promo"),
     *                 @SWG\Property (property="products", type="array", items=@SWG\Schema(ref="#/definitions/Product")),
     *                 @SWG\Property (property="category", type="array", items=@SWG\Schema(ref="#/definitions/Category")),
     *             }),
     *              @SWG\Property(property="middle_block", properties={
     *                 @SWG\Property(property="title", type="string", example="Sales"),
     *                 @SWG\Property(property="slug", type="string", example="/sales"),
     *                 @SWG\Property (property="products", type="array", items=@SWG\Schema(ref="#/definitions/Product")),
     *                 @SWG\Property (property="category", type="array", items=@SWG\Schema(ref="#/definitions/Category")),
     *             }),
     *              @SWG\Property(property="bottom_block", properties={
     *                 @SWG\Property(property="title", type="string", example="Products"),
     *                 @SWG\Property(property="slug", type="string", example="/products"),
     *                 @SWG\Property (property="products", type="array", items=@SWG\Schema(ref="#/definitions/Product")),
     *                 @SWG\Property (property="category", type="array", items=@SWG\Schema(ref="#/definitions/Category")),
     *             }),
     *         )
     *         ),
     *     )
     * )
     * Display promotional products.
     *
     * @return array
     */
    public function promotionalProducts()
    {
        $promotionalBlocks = PromotionalBlock::with(
            'products',
            'products.productPrices',
            'products.productColorGroups',
            'products.productColorGroups.productColors',
            'category'
        )->get();

        $result = [];

        foreach ($promotionalBlocks as $promotionalBlock) {
            $result[PromotionalBlock::getBlocksLabel(true)[$promotionalBlock->id]] = fractal()->item($promotionalBlock,
                new PromotionalBlocksTransformer())->toArray();
        }

        return $result;
    }

    /**
     * @SWG\Get(
     *     path="/api/quick-search/",
     *     tags={"Quick Search"},
     *     summary="Quick Search of products",
     *     description="Quick Search of Products",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *          name="name",
     *          in="formData",
     *          description="name of product",
     *          type="string"
     *      ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     * Display a listing of the resource.
     * @return Response
     */
    public function quickSearch(Request $request)
    {
        $searchableName = Product::class;
        $search = SearchBuilder::createBuilder($searchableName);
        $categories = [];
        $categoriesId = [];

        if ($request->has('name')) {
            $search->match('name', $request->get('name'));
        }

        if ($request->has('id')) {
            $search->term('id', $request->get('id'));
        }

        $result = $search->get();

        if (empty($result)) {
            return Response(['data' => []]);
        }

        for ($i = 0; $i < count($result); $i++) {
            if ($result[$i]['active'] === 1) {
                for ($k = 0; $k < count($result[$i]['categories']); $k++) {
                    if (!in_array($result[$i]['categories'][$k]['id'], $categoriesId)) {
                        $categories[] = [
                            'id' => $result[$i]['categories'][$k]['id'],
                            'name' => $result[$i]['categories'][$k]['name'],
                            'slug' => $result[$i]['categories'][$k]['slug'],
                        ];
                        $categoriesId[] = $result[$i]['categories'][$k]['id'];
                    }
                }
            }
        }

        return Response(['data' => $categories]);

    }

    /**
     * @SWG\Post(
     *     path="/api/search/",
     *     tags={"Product Search"},
     *     summary="List Products",
     *     description="Search of Products",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *          name="id",
     *          in="formData",
     *          description="product id",
     *          type="integer"
     *      ),
     *     @SWG\Parameter(
     *          name="page",
     *          in="formData",
     *          description="pagination",
     *          type="string"
     *      ),
     *     @SWG\Parameter(
     *          name="name",
     *          in="formData",
     *          description="name of product",
     *          type="string"
     *      ),
     *     @SWG\Parameter(
     *          name="category_id",
     *          in="formData",
     *          description="category id from product",
     *          type="integer"
     *      ),
     *     @SWG\Parameter(
     *          name="sku",
     *          in="formData",
     *          description="sku of product",
     *          type="string"
     *      ),
     *     @SWG\Parameter(
     *          name="color_ids",
     *          in="formData",
     *          description="Array of color ids from product",
     *          type="string"
     *      ),
     *     @SWG\Parameter(
     *          name="per_page",
     *          in="formData",
     *          description="products per page",
     *          type="integer"
     *      ),
     *     @SWG\Parameter(
     *          name="price_from",
     *          in="formData",
     *          description="min price of product",
     *          type="number"
     *      ),
     *     @SWG\Parameter(
     *          name="price_to",
     *          in="formData",
     *          description="max price of product",
     *          type="number"
     *      ),
     *     @SWG\Parameter(
     *          name="quantity_from",
     *          in="formData",
     *          description="min quantity of product",
     *          type="number"
     *      ),
     *     @SWG\Parameter(
     *          name="quantity_to",
     *          in="formData",
     *          description="max quantity of product",
     *          type="number"
     *      ),
     *     @SWG\Parameter(
     *          name="sorting",
     *          in="formData",
     *          description="product sorting type",
     *          type="string",
     *          default="prices",
     *      ),
     *     @SWG\Parameter(
     *          name="order",
     *          in="formData",
     *          description="product order sorting",
     *          type="string",
     *          default="asc",
     *      ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *               @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/SearchResult")),
     *     )
     * )
     * Display a listing of the resource.
     * @return Response
     */
    public function search(Request $request)
    {
        $searchableName = Product::class;
        $search = SearchBuilder::createBuilder($searchableName);
        $breadcrumbs = [];
        $categoryIds = [];

        $isRoot = true;

        foreach ([
                     'id',
                     'name',
                     'sku',
                     'category_id',
                     'category_slug',
                     'color_ids',
                     'price_from',
                     'price_to',
                     'quantity_from',
                     'quantity_to'
                 ] as $field) {
            if ($request->has($field)) {
                switch ($field) {
                    case 'id':
                        $search->term('id', $request->get('id'));
                        break;
                    case 'name':
                        $search->match('name', $request->get('name'));
                        break;
//                        $search->matchPhrasePrefix('name', $request->get('name'));
//                        break;
                    case 'sku':
                        $search->nestedMatchPhrasePrefix('vendors', ['sku' => $request->get('sku')]);
                        break;
//                    case 'active':
//                        $search->term('active', 1);
//                        break;
                    case 'category_id':
                        /** @var Category $category */
                        $category = Category::find($request->get('category_id'));
                        if (is_null($category)) {
                            break;
                        }
                        $categoryIds = $category->getDescendantsAndSelf()->pluck('id')->toArray();
                        $search->nestedTerms('categories', ['id' => $categoryIds]);

                        if (!$category->isRoot()) {
                            $isRoot = false;
                        }
                        /** @var Breadcrumb $breadcrumb */
                        $breadcrumb = Breadcrumb::class;
                        $breadcrumbs = $breadcrumb::render($category);
                        break;
                    case 'category_slug':
                        $categorySlug = $this->getCategorySlug($request->get('category_slug'));
                        $category = CategoryRepository::getCategoryBySlug($categorySlug);
                        if (is_null($category)) {
                            break;
                        }
                        $categoryIds = collect($category->getChildsAndSelf($category))->pluck('id')->toArray();
                        $search->nestedTerms('categories', ['id' => $categoryIds]);
                        if (!$category->isRoot()) {
                            $isRoot = false;
                        }
                        /** @var Breadcrumb $breadcrumb */
                        $breadcrumb = Breadcrumb::class;
                        $breadcrumbs = $breadcrumb::render($category);
                        break;
                    case 'color_ids':
                        $search->nestedTerms('colors', ['color_id' => $request->get('color_ids')]);
                        break;
                    case 'price_from':
                        $search->nestedBetween('prices', $request->get('price_from'), $request->get('price_to') ?? null,
                            'item_price');
                        break;
                    case 'price_to':
                        $search->nestedBetween('prices', $request->get('price_from') ?? null, $request->get('price_to'),
                            'item_price');
                        break;
                    case 'quantity_from':
                        $search->nestedBetween('prices', $request->get('quantity_from'),
                            $request->get('quantity_to') ?? null, 'quantity');
                        break;
                    case 'quantity_to':
                        $search->nestedBetween('prices', $request->get('quantity_from') ?? null,
                            $request->get('quantity_to'), 'quantity');
                        break;
                }
            }
        }
        $search->term('active', 1);
        if ($request->has('sorting')) {
            switch ($request->get('sorting')) {
                case 'prices':
                    $order = ['order' => $request->get('order'), 'unmapped_type' => 'float',];
                    $search->order("lowest_price", $order);
                    break;
                case 'name':
                    $search->order("name_sort", $request->get('order'));
                    break;
                default:
                    $search->order($request->get('sorting'), $request->get('order'));
                    break;
            }
        } else {
            $search->order("popularity",
                [
                    'mode' => 'min',
                    'order' => 'desc',
                ]
            );
        }

        $result = $search->paginate($request->get('page', 1),
            $request->per_page ?? (int)config('superiorpromos.items_per_page'));

        $ids = array_pluck($result['data'], 'id');

        if (count($ids)) {
            $rawOrder = \DB::raw(sprintf('FIELD(id, %s)', implode(',', $ids)));
            $collection = Product::whereIn('id', $ids)
                ->orderByRaw($rawOrder)
                ->get();
        } else {
            $collection = new Collection();
        }


        $productData = (new ProductSearchTransformer($collection))->toArray();

        $result['meta']['pagination']['data'] = fractal()->collection($productData,
            new ProductBreadcrumbsTransformer(['categories' => $categoryIds]));

        $result['meta']['pagination']['breadcrumbs'] = $breadcrumbs;

        return $result['meta']['pagination'];
    }

    /**
     * @SWG\POST(
     *     path="/api/product/quick-quote",
     *     tags={"Product Quick Quote"},
     *     summary="Product Quick Quote",
     *     description="Generate Quock Quote for product",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *          name="product_id",
     *          in="formData",
     *          description="Product id",
     *          type="integer",
     *          default="1067",
     *      ),
     *     @SWG\Parameter(
     *         name="quantity",
     *         in="formData",
     *         description="Quantity",
     *         required=true,
     *         type="integer",
     *         default=200
     *      ),
     *     @SWG\Parameter(
     *         name="product_colors",
     *         in="formData",
     *         description="Product Colors",
     *         required=false,
     *         type="string",
     *         default="[{""color_group_id"":""178"",""color_id"":""951""}]",
     *      ),
     *     @SWG\Parameter(
     *         name="product_options",
     *         in="formData",
     *         description="Product Options",
     *         required=false,
     *         type="string",
     *         default="[{""product_option_id"":""177"",""product_sub_option_ids"":[""5675""]},{""product_option_id"":""1629"",""product_sub_option_ids"":[""8357""]},{""product_option_id"":""2030"",""product_sub_option_ids"":[""9999""]}]",
     *      ),
     *     @SWG\Parameter(
     *          name="product_imprints",
     *         in="formData",
     *         description="Product Imprints",
     *         required=false,
     *         type="string",
     *         default="[{""imprint_id"":""206"",""color_ids"":[""1"",""4"",""5""]},{""imprint_id"":""4328"",""color_ids"":[""18""]}]",
     *      ),
     *     @SWG\Parameter(
     *          name="product_shipping",
     *         in="formData",
     *         description="Product Shipping",
     *         required=false,
     *         type="string",
     *         default="{""service"":""UPS Ground"",""cost"":""63.74"",""transit_time"":""3 business days""}",
     *      ),
     *     @SWG\Parameter(
     *         name="download_pdf",
     *         in="formData",
     *         description="Flag to download pdf",
     *         required=false,
     *         type="boolean",
     *         default="false",
     *      ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     *
     * @param Product $product
     * @param QuickQuoteRequest $request
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function quickQuote(QuickQuoteRequest $request)
    {

        $product = Product::find($request->get('product_id'));

        if (empty($product)) {
            return \response()->json(['message' => 'Product with id ' . $request->get('product_id') . ' not found'],
                404);
        }

        $quickQuote = $this->getQuickQuoteData($product, $request);

        if ($request->get('download_pdf')) {
            $fileName = 'product-' . $product->id;
            $pdfStream = PDFService::generatePdf('pdf.quick-quote-pdf', compact('quickQuote'));
            if (empty($pdfStream)) {
                return \response()->json(['message' => 'Something went wrong. Please, try again later.'], 400);
            }
            $path = 'quick-quote/' . $fileName . '.pdf';
            Storage::disk('public')->put($path, $pdfStream);


            return \response()->json([
                'url' => Storage::disk('public')->url($path)
            ]);
        }

        return \response()->json($quickQuote);
    }

    /**
     * @param Product $product
     * @param $request
     * @return array
     */
    protected function getQuickQuoteData(Product $product, $request)
    {
        $quickQuoteData = [
            'product' => [],
            'prices' => [],
            'prices_colors' => [],
            'prices_options' => [],
            'prices_imprints' => [],
            'prices_setup' => [],
            'colors' => [],
            'options' => [],
            'imprints' => [],
            'final_unit_price' => '',
            'subtotal' => '',
            'subtotal_float' => 0
        ];

        $setup_total = 0;
        $price_total = 0;

        $quantity = $request->get('quantity');
        $quickQuoteData['product'] = ProductRepository::getProductInfo($product);

        $prices = ProductRepository::getProductCost($product, $quantity);
        $quickQuoteData['prices'] = $prices;
        $productPrices = ProductRepository::getProductSetupPrices($prices);
        $quickQuoteData['product_prices'] = $productPrices['product_prices'];
        if (!empty($productPrices['prices_setup'])) {
            $quickQuoteData['prices_setup'] = array_merge($quickQuoteData['prices_setup'],
                $productPrices['prices_setup']);
        }
        $setup_total += $productPrices['setup_total'];
        $price_total += $productPrices['price_total'];

        if ($request->has('product_colors') && !empty($request->get('product_colors'))) {
            $productColorsRequest = $request->get('product_colors');
            $colors = ProductRepository::getProductColors($product, $productColorsRequest, $quantity);
            $colorsPrices = ProductRepository::getProductColorsSetupPrices($colors);
            $quickQuoteData['colors'] = $colors;
            $quickQuoteData['prices_colors'] = $colorsPrices['prices_colors'];
            if (!empty($colorsPrices['prices_setup'])) {
                $quickQuoteData['prices_setup'] = array_merge($quickQuoteData['prices_setup'],
                    $colorsPrices['prices_setup']);
            }
            $setup_total += $colorsPrices['setup_total'];
            $price_total += $colorsPrices['price_total'];

        }

        if ($request->has('product_options') && !empty($request->get('product_options'))) {
            $productOptionsRequest = $request->get('product_options');
            $options = ProductRepository::getProductOptions($product, $productOptionsRequest, $quantity);
            $optionsPrices = ProductRepository::getProductOptionsSetupPrices($options);
            $quickQuoteData['options'] = $options;
            $quickQuoteData['prices_options'] = $optionsPrices['prices_options'];
            if (!empty($optionsPrices['prices_setup'])) {
                $quickQuoteData['prices_setup'] = array_merge($quickQuoteData['prices_setup'],
                    $optionsPrices['prices_setup']);
            }
            $setup_total += $optionsPrices['setup_total'];
            $price_total += $optionsPrices['price_total'];
        }

        if ($request->has('product_imprints') && !empty($request->get('product_imprints'))) {
            $productImprintsRequest = $request->get('product_imprints');
            $imprints = ProductRepository::getProductImprints($product, $productImprintsRequest, $quantity);
            $imprintsPrices = ProductRepository::getProductImprintsSetupPrices($imprints);
            $quickQuoteData['imprints'] = $imprints;
            $quickQuoteData['prices_imprints'] = $imprintsPrices['prices_imprints'];
            if (!empty($imprintsPrices['prices_setup'])) {
                $quickQuoteData['prices_setup'] = array_merge($quickQuoteData['prices_setup'],
                    $imprintsPrices['prices_setup']);
            }
            $setup_total += $imprintsPrices['setup_total'];
            $price_total += $imprintsPrices['price_total'];
        }

        if ($request->has('product_shipping') && !empty($request->get('product_shipping'))) {
            $productShippingRequest = $request->get('product_shipping');
            $quickQuoteData['shipping']['service_type'] = $productShippingRequest['service'];
            $quickQuoteData['shipping']['cost'] = '$' . number_format($productShippingRequest['cost'], 2);
            $quickQuoteData['shipping']['transit_time'] = $productShippingRequest['time_in_transit'];
            $setup_total += number_format($productShippingRequest['cost'], 2);
        }

        $quickQuoteData['final_unit_price'] = '$' . number_format($price_total, 2);
        $quickQuoteData['subtotal_float'] = ($price_total * $quantity) + $setup_total;
        $quickQuoteData['subtotal'] = '$' . number_format($quickQuoteData['subtotal_float'], 2, '.', ',');

        return $quickQuoteData;
    }

    /**
     * @SWG\POST(
     *     path="/api/product/quick-quote/email",
     *     tags={"Product Quick Quote"},
     *     summary="Send Quick Quote Email",
     *     description="Send Quick Quote Email to users",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *          name="user",
     *          in="formData",
     *          description="User",
     *          required=true,
     *          type="string",
     *          default="[{""name"":""Boris"",""email"":[""boris@superiorpromos.com""]}]",
     *     ),
     *     @SWG\Parameter(
     *          name="product",
     *          in="formData",
     *          description="Product Info",
     *          required=false,
     *          type="string",
     *          default="[{""id"":""3612"",""title"":""Motivations Calender"",""description"":""The ideal calendar to promote optimism"",""img"":""""}]",
     *      ),
     *     @SWG\Parameter(
     *         name="prices",
     *         in="formData",
     *         description="Product Prices",
     *         required=false,
     *         type="string",
     *         default="[{""quantity"":""12"",""price"":""19.08""}]",
     *      ),
     *     @SWG\Parameter(
     *         name="colors",
     *         in="formData",
     *         description="Product Colors",
     *         required=false,
     *         type="string",
     *         default="[{""title"":""Item Color"",""valuse"":""Blue (dark)""}]",
     *      ),
     *     @SWG\Parameter(
     *         name="imprints",
     *         in="formData",
     *         description="Product Imprints",
     *         required=false,
     *         type="string",
     *         default="[{""title"":""Main Location"",""count_colors"":""1""}]",
     *      ),
     *     @SWG\Parameter(
     *         name="options",
     *         in="formData",
     *         description="Product Options",
     *         required=false,
     *         type="string",
     *         default="[{""title"":""Optional Envelopes"",sub_option[{""title"": ""Gift, not inserted""}]}]",
     *      ),
     *     @SWG\Parameter(
     *         name="shipping",
     *         in="formData",
     *         description="Shipping",
     *         required=false,
     *         type="string",
     *         default="[{""service_type"":""UPS Ground"",""cost"":""$30.63"",""transit_time"": ""3""}]",
     *      ),
     *     @SWG\Parameter(
     *          name="prices_options",
     *         in="formData",
     *         description="Product prices options",
     *         required=false,
     *         type="string",
     *         default="[{""title"":""Optional Envelopes Additional fee (per item)"",""value"":""$0.16""}]",
     *      ),
     *     @SWG\Parameter(
     *          name="prices_imprints",
     *         in="formData",
     *         description="Product prices imprints",
     *         required=false,
     *         type="string",
     *         default="",
     *      ),
     *     @SWG\Parameter(
     *          name="prices_setup",
     *         in="formData",
     *         description="Product prices setup",
     *         required=false,
     *         type="string",
     *         default="[{""title"":""Custom Cover ( $25 setup ) Setup"",""value"":""$25.00""},{""title"":""Main Location Setup"",""value"":""$20.00""}]",
     *      ),
     *     @SWG\Parameter(
     *          name="prices_colors",
     *         in="formData",
     *         description="Product prices colors",
     *         required=false,
     *         type="string",
     *         default="",
     *      ),
     *     @SWG\Parameter(
     *          name="final_unit_price",
     *         in="formData",
     *         description="Final unit price",
     *         required=false,
     *         type="string",
     *         default="{""final_unit_price"":""$3.62""}",
     *      ),
     *     @SWG\Parameter(
     *          name="subtotal",
     *         in="formData",
     *         description="Subtotal price",
     *         required=false,
     *         type="string",
     *         default="{""subtotal"":""$437.63""}",
     *      ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     *
     * @param QuickQuoteEmailRequest $request
     * @param MailService $mailService
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendQuickQuoteEmail(QuickQuoteEmailRequest $request, MailService $mailService)
    {
        $user = $request->input('user');

        $mailService->sendMultipleTransactionalEmail(new QuickQuoteNotification($user['emails'], $request->all()));

        return \response()->json(['message' => 'Instant Quote Emailed. Thank You!']);
    }

    /**
     * @param string $slug
     * @return string
     */
    protected function getCategorySlug(string $slug)
    {
        if (substr($slug, -1) == '/') {
            $slug = substr($slug, 0, -1);
        }

        $segments = explode('/', $slug);

        if (empty($segments)) {
            return $slug;
        }

        return end($segments);
    }

    /**
     * @SWG\Get(
     *     path="/api/product/{product}/pdf",
     *     tags={"Products"},
     *     summary="Generate pdf for product",
     *     description="Generate pdf for product",
     *     @SWG\Parameter(
     *          name="product",
     *          in="path",
     *          description="id of product",
     *          type="integer"
     *      ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     * Display a listing of the resource.
     * @param Product $product
     * @return mixed
     */
    public function pdf(Product $product)
    {
        $product = fractal()->item($product, new ProductTransformer())->toArray();
        $pdfStream = PDFService::generatePdf('pdf.product-pdf', compact('product'));
        if (empty($pdfStream)) {
            return \response()->json(['message' => 'Something went wrong. Please, try again later.'], 400);
        }

        return \response()->make($pdfStream, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="product-' . $product['id'] . '.pdf"'
        ]);
    }
}
