<?php

namespace App\Http\Controllers;

use App\Contracts\MailService;
use App\Contracts\PaymentGateway;
use App\Notifications\BigMailer\PasswordResetNotification;
use App\User;
use App\Models\Address;
use App\Models\Cart;
use App\Models\CartItem;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Password;
use JWTAuth;

class SecurityController extends Controller
{
    /**
     *  @SWG\Post(
     *     path="/api/login",
     *     tags={"Security"},
     *     summary="Login",
     *     description="Login user in our system",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="User email",
     *         required=true,
     *         type="string",
     *         default="admin@admin.com",
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *         in="formData",
     *         description="User password",
     *         required=true,
     *         type="string",
     *         default="password",
     *     ),
     *     @SWG\Parameter(
     *         name="remember_me",
     *         in="formData",
     *         description="Remember me",
     *         required=true,
     *         type="string",
     *         default="1",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="token", type="string", example="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDAwXC9hcGlcL2xvZ2luIiwiaWF0IjoxNDgwOTU0MTk4LCJleHAiOjE0ODA5NTc3OTgsIm5iZiI6MTQ4MDk1NDE5OCwianRpIjoiMWI0ZjJhZTRkMzRhZDc2ZWJmMDE4NmMxNzNhZWZhZTYifQ.ZVd1ydAl2j30NBYTnI-ih976kfDkiHZ7s0xuSq259Ac"),
     *         ),
     *     )
     * )
     *
     * Login action
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], Response::HTTP_UNAUTHORIZED);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        if($request->hasHeader('guest')) {
            $guestsCart = Cart::whereSession($request->header('guest'))->first();
            if(!is_null($guestsCart)) {
                $usersCart = Cart::current(JWTAuth::toUser($token))->first();
                if(!is_null($usersCart)) {
                    CartItem::whereCartId($guestsCart->id)->update(['cart_id' => $usersCart->id]);
                    $guestsCart->delete();
                } else {
                    Cart::whereId($guestsCart->id)->update(['user_id' => JWTAuth::toUser($token)->id, 'session' => null]);
                }
            }
        }

        return response()->json(compact('token'));
    }

    /**
     * @SWG\Post(
     *     path="/api/registration",
     *     tags={"Security"},
     *     summary="Registration",
     *     description="Register new user in our system",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *    @SWG\Parameter(
     *         name="users[name]",
     *         in="formData",
     *         description="User name",
     *         required=true,
     *         type="string",
     *         default="John Cena",
     *     ),
     *     @SWG\Parameter(
     *         name="users[email]",
     *         in="formData",
     *         description="User email",
     *         required=true,
     *         type="string",
     *         default="admin@admin.admin",
     *     ),
     *     @SWG\Parameter(
     *         name="users[password]",
     *         in="formData",
     *         description="Password",
     *         required=true,
     *         type="string",
     *         default="password",
     *     ),
     *     @SWG\Parameter(
     *         name="users[password_confirmation]",
     *         in="formData",
     *         description="Password confirmation",
     *         required=true,
     *         type="string",
     *         default="password",
     *     ),
     *     @SWG\Parameter(
     *         name="users[contact_telephone]",
     *         in="formData",
     *         description="User contact telephone",
     *         required=true,
     *         type="string",
     *         default="123-456-7890",
     *     ),
     *     @SWG\Parameter(
     *         name="users[ext]",
     *         in="formData",
     *         description="User extension",
     *         required=false,
     *         type="string",
     *         default="123",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *              @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/RegisterUser")),
     *         ),
     *     )
     * )
     *
     * Register User
     *
     * @param RegisterRequest $request
     * @param PaymentGateway $paymentGateway
     * @return mixed
     */
    public function register(RegisterRequest $request, PaymentGateway $paymentGateway)
    {
        $user = (new User($request->input('users')))->fill([
            'password' => bcrypt($request->input('users.password')),
            'active' => true
        ]);
        $user->save();

        $user->addresses()->saveMany([
            (new Address($request->input('users.shipping')))->fill([
                'user_id' => $user->id,
                'type' => 1,
            ]),
            (new Address($request->input('users.billing.address') == 'same_as_shipping' ? $request->input('users.shipping') : $request->input('users.billing')))->fill([
                'user_id' => $user->id,
                'type' => 2,
            ]),
        ]);

        try {
            $user->authorize_net_id = $paymentGateway->createCustomer($user);
        } catch (\Exception $e) {

            return response()->json([
                'user' => $user,
            ]);
        }

        $user->save();

        $credentials = $request->only('users.email', 'users.password')['users'];

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'Authorization failed, please try login manually'], Response::HTTP_UNAUTHORIZED);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json(compact('token'));
    }

    /**
     *
     * @SWG\Post(
     *     path="/api/password/reset",
     *     tags={"Security"},
     *     summary="Reset password",
     *     description="Send reset link email",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="User email",
     *         required=true,
     *         type="string",
     *         default="admin@admin.com",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="status", type="string", example="Your password has been reset!"),
     *         ),
     *     )
     * )
     *
     * Reset the given user's password.
     *
     * @param \Illuminate\Http\Request $request
     * @param MailService $mailService
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset(Request $request, MailService $mailService)
    {
        $this->validate($request, [
            'email' => 'required|email|exists:users,email',
        ]);

        $user = User::where('email', $request->input('email'))->first();
        $password = Str::random(11);
        $this->resetPassword($user, $password);

        $send = $mailService->sendSingleTransactionalEmail(
            new PasswordResetNotification([$user->email], [
                'email'    => $user->email,
                'name'     => $user->name,
                'password' => $password
            ])
        );

        if (!$send) {
            return response()->json([
                'message' => trans('passwords.reset-error'),
            ], Response::HTTP_BAD_REQUEST);
        }

        return response()->json([
            'message' => trans('passwords.reset'),
        ]);
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->forceFill([
            'password' => bcrypt($password),
            'remember_token' => Str::random(60),
        ])->save();
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker();
    }

    /**
     * @param $email
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkEmail($email)
    {
        $userEmail = User::where('email', $email)->first();

        if(is_null($userEmail)) {
            return response()->json([
                'success' => true,
            ]);
        }

        return response()->json([
            'success' => false,
        ]);
    }
}
