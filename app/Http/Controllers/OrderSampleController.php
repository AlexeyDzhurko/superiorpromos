<?php

namespace App\Http\Controllers;

use App\Repository\OrderSampleRepository;
use App\Transformers\OrderSampleTransformer;
use App\Http\Requests\AddOrderSampleRequest;
use App\Models\OrderSample;

class OrderSampleController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/order_sample",
     *     tags={"Orders"},
     *     summary="Display orders sample history",
     *     description="Display orders sample history for authenticated user",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description = "Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/OrderSample")),
     *     )
     * )
     *
     * Display orders sample history
     */
    public function index()
    {
        try {
            
            return fractal()->collection(OrderSampleRepository::getOrderSampleByUser(), new OrderSampleTransformer());
        } catch (\Exception $e) {

            return response('Error on the server side.', 500);
        }
    }

    public function store(AddOrderSampleRequest $request)
    {
        return fractal()->item(OrderSampleRepository::add($request->all()), new OrderSampleTransformer());
    }
}
