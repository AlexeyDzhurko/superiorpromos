<?php


namespace App\Http\Controllers;


use App\Models\Order;
use App\Models\OrderItem;
use App\Transformers\FullOrderItemTransformer;

class OrderItemController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/order/{orderId}/order-item/{id}  ",
     *     tags={"Orders"},
     *     summary="Display order item",
     *     description="Display detailed order item",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="orderId",
     *         in="path",
     *         description="Order Id",
     *         required=true,
     *         type="integer",
     *         default=1,
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Order Item Id",
     *         required=true,
     *         type="integer",
     *         default=1,
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description = "Success",
     *         @SWG\Schema(ref="#/definitions/FullOrderItem"),
     *     )
     * )
     *
     * Display detailed order item
     */
    public function show(Order $order, OrderItem $orderItem)
    {
        return fractal()->item($orderItem, new FullOrderItemTransformer());
    }

}
