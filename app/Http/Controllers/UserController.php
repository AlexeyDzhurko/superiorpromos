<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateAccountRequest;
use App\User;
use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use Storage;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     *@SWG\Get(
     *     path="/api/account",
     *     tags={"User"},
     *     summary="User Account",
     *     description="User Account",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDAwXC9hcGlcL2xvZ2luIiwiaWF0IjoxNDgxNjQxODQ0LCJleHAiOjE0ODE2NDU0NDQsIm5iZiI6MTQ4MTY0MTg0NCwianRpIjoiNjU3MmEzNDYxOWU1YjEyODIyNjdlYWQ4ODQ2NDI5NjIifQ.mpx7LhtwLtrb0ho7r5U5DVg0JGMlwmHA4-moOKhIMDw",
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *         @SWG\Schema(
     *             @SWG\Property(property="counters", properties={
     *                 @SWG\Property(property="addresses", type="integer", example="3"),
     *                 @SWG\Property(property="cards", type="integer", example="2"),
     *                 @SWG\Property(property="wish_list_items", type="integer", example="1"),
     *             }),
     *         )
     *     )
     * )
     *
     * Display User account.
     *
     * @return \Illuminate\Http\Response
     */
    public function myAccountPage()
    {
        $user = User::with('addresses', 'payment_profiles')->find(Auth::id());

        return response()->json([
            'counters' => [
                'addresses' => $user->addresses()->count(),
                'cards' => $user->payment_profiles()->count(),
                //todo: add counters for Wish List
                'wish_list_items' => 0,
            ],
        ]);
    }

    /**
     *  @SWG\Post(
     *     path="/api/account/account-information",
     *     tags={"User"},
     *     summary="User Account Information",
     *     description="Provide Logged User Account Information",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDAwXC9hcGlcL2xvZ2luIiwiaWF0IjoxNDgxNjQxODQ0LCJleHAiOjE0ODE2NDU0NDQsIm5iZiI6MTQ4MTY0MTg0NCwianRpIjoiNjU3MmEzNDYxOWU1YjEyODIyNjdlYWQ4ODQ2NDI5NjIifQ.mpx7LhtwLtrb0ho7r5U5DVg0JGMlwmHA4-moOKhIMDw",
     *     ),
     *    @SWG\Parameter(
     *         name="information[name]",
     *         in="formData",
     *         description="User name",
     *         required=false,
     *         type="string",
     *         default="John Cena",
     *     ),
     *     @SWG\Parameter(
     *         name="email[email]",
     *         in="formData",
     *         description="User email",
     *         required=false,
     *         type="string",
     *         default="admin@admin.admin",
     *     ),
     *     @SWG\Parameter(
     *         name="email[email_confirmation]",
     *         in="formData",
     *         description="Confirmation user email",
     *         required=false,
     *         type="string",
     *         default="admin@admin.admin",
     *     ),
     *     @SWG\Parameter(
     *         name="password[old_password]",
     *         in="formData",
     *         description="Old password",
     *         required=false,
     *         type="string",
     *         default="password",
     *     ),
     *     @SWG\Parameter(
     *         name="password[password]",
     *         in="formData",
     *         description="Password",
     *         required=false,
     *         type="string",
     *         default="password1",
     *     ),
     *     @SWG\Parameter(
     *         name="password[password_confirmation]",
     *         in="formData",
     *         description="Password confirmation",
     *         required=false,
     *         type="string",
     *         default="password1",
     *     ),
     *     @SWG\Parameter(
     *         name="information[contact_telephone]",
     *         in="formData",
     *         description="User contact telephone",
     *         required=false,
     *         type="string",
     *         default="123-456-7890",
     *     ),
     *     @SWG\Parameter(
     *         name="information[ext]",
     *         in="formData",
     *         description="User extension",
     *         required=false,
     *         type="string",
     *         default="123",
     *     ),
     *     @SWG\Parameter(
     *         name="information[fax]",
     *         in="formData",
     *         description="User fax",
     *         required=false,
     *         type="string",
     *         default="123",
     *     ),
     *
     *     @SWG\Response(
     *         response=204,
     *         description="OK",
     *         ),
     *     )
     * )
     *
     * Update logged User account information
     *
     * @param UpdateAccountRequest $request
     * @return Response
     */
    public function updateMyAccountInformation(UpdateAccountRequest $request)
    {
        $user = User::with('addresses', 'payment_profiles')->find(Auth::id());
        /*$file = $request->users['tax_exempt_certificate'];

        if (isset($file) && $file->isValid()) {
            $fullPath = User::CERT_DIR . Str::random(20) . '.' . $file->getClientOriginalExtension();
            Storage::put($fullPath, (string)$file, 'public');
            $user->certificate = $fullPath;
        }*/

        if ($request->has('information')) {
            /*$data = [
                'name',
                'contact_telephone',
                'ext',
                'fax',
            ];
            foreach ($data as $value) {
                if($request->has('information.' . $value)) {
                    $user->fill([$value => $request->input('information.' . $value)]);
                }
            }*/
            $user->fill([
                'name'              => $request->input('information.name'),
                'contact_telephone' => $request->input('information.contact_telephone'),
                'ext'               => $request->input('information.ext'),
                'fax'               => $request->input('information.fax'),
            ]);
        }

        if ($request->has('email')) {
            $user->fill([
                'email' => $request->input('email.email')
            ]);
        }

        if ($request->has('password')) {
            if(\Hash::check($request->input('password.old_password'), Auth::user()->password)) {
                $user->forceFill([
                    'password'       => bcrypt($request->input('password.password')),
                    'remember_token' => Str::random(60),
                ]);
            } else {
                throw new HttpException(Response::HTTP_BAD_REQUEST, 'Old password not match');
            }
        }

        try {
            $user->save();
        } catch (Exception $e) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, $e->getMessage());
        }

        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     *@SWG\Get(
     *     path="/api/account/account-information",
     *     tags={"User"},
     *     summary="User Account Information",
     *     description="Provide Logged User Account Information",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDAwXC9hcGlcL2xvZ2luIiwiaWF0IjoxNDgxNjQxODQ0LCJleHAiOjE0ODE2NDU0NDQsIm5iZiI6MTQ4MTY0MTg0NCwianRpIjoiNjU3MmEzNDYxOWU1YjEyODIyNjdlYWQ4ODQ2NDI5NjIifQ.mpx7LhtwLtrb0ho7r5U5DVg0JGMlwmHA4-moOKhIMDw",
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *              @SWG\Schema(ref="#/definitions/User"),
     *         ),
     *     )
     * )
     *
     * Provide logged User account information
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function myAccountInformation()
    {
        $user = User::with('addresses', 'payment_profiles')->find(Auth::id());
        return response()->json($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, $id)
    {
        $user->delete();
    }
}
