<?php


namespace App\Http\Controllers;


use App\Models\Promotion;

class PromotionsController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $promotions = Promotion::first();

        return response()->json($promotions);
    }
}
