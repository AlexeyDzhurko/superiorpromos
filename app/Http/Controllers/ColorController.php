<?php

namespace App\Http\Controllers;

use App\Models\Color;
use App\Transformers\ColorsTransformer;

class ColorController extends Controller
{

    /**
     *  @SWG\Get(
     *     path="/api/colors",
     *     tags={"Colors"},
     *     summary="List Colors",
     *     description="List Colors",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *              @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/Color")),
     *         ),
     *     )
     * )
     * Display a listing of the Colors.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return fractal()->collection(Color::get(), new ColorsTransformer())->toArray();
    }
}
