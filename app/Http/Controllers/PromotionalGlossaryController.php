<?php


namespace App\Http\Controllers;


use App\Models\PromotionalGlossary;
use App\Transformers\PromotionalGlossaryTransformer;

class PromotionalGlossaryController extends Controller
{
    /**
     *  @SWG\Get(
     *     path="/api/promotional-glossary",
     *     tags={"Promotional Glossary"},
     *     summary="Promotional Glossary",
     *     description="Promotional Glossary",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *              @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/PromotionalGlossary")),
     *         ),
     *     )
     * )
     * Display a listing of the Quick Links.
     *
     * @return \Spatie\Fractal\Fractal
     */
    public function index()
    {
        return fractal()->collection(PromotionalGlossary::whereActive(true)->sorted()->get(), new PromotionalGlossaryTransformer());
    }

}
