<?php


namespace App\Http\Controllers;


use App\Http\Requests\AddToCartRequest;
use App\Http\Requests\UpdateCartItemRequest;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\SavedCartItem;
use App\Transformers\CartItemTransformer;
use App\Transformers\SavedCartItemTransformer;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Auth;
use DB;
use Exception;
use Log;
use Illuminate\Http\Response;
use App\Contracts\CartManager;

class SavedCartController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/user/saved-cart",
     *     tags={"Orders"},
     *     summary="Display list of saved cart items",
     *     description="Display list of saved cart items for authenticated user",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description = "Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/SavedCartItem")),
     *     )
     * )
     *
     * Returns saved cart items for authenticated user
     *
     */
    public function index()
    {
        $savedCartItems = SavedCartItem::whereUserId(Auth::id())->get();
        return fractal()->collection($savedCartItems, new SavedCartItemTransformer());
    }

    /**
     * @SWG\Post(
     *     path="/api/user/saved-cart",
     *     tags={"Orders"},
     *     summary="Add cart items to saved cart items",
     *     description="Add cart items to saved cart items for authenticated user",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="cart_item_ids[]",
     *         in="formData",
     *         description="Array of Cart Item Ids",
     *         required=true,
     *         type="array",
     *         items="integer",
     *         default="1"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/SavedCartItem"))
     *     )
     * )
     *
     * Add cart items to saved cart
     *
     * @param Request $request
     * @return \Spatie\Fractal\Fractal
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'cart_item_ids' => 'required|array|exists:cart_items,id'
        ]);

        $cartItems = CartItem::whereIn('id', $request->get('cart_item_ids'))->get();

        foreach ($cartItems as $cartItem) {
            $this->authorize('view', $cartItem);
        }

        $savedCartItems = [];

        try {
            DB::transaction(function () use ($cartItems, &$savedCartItems) {
                foreach ($cartItems as $cartItem) { /** @var CartItem $cartItem */
                    $savedCartItem = SavedCartItem::firstOrCreate([
                        'user_id' => Auth::id(),
                        'cart_item_id' => $cartItem->id,
                    ]);
                    $savedCartItems[] = $savedCartItem;
                    $cartItem->cart()->dissociate()->save();
                }
            });
        } catch (Exception $exception) {
            Log::error('Save cart fail: '.$exception->getMessage(), ['cart_item_ids' => $request->get('cart_item_ids')]);
            return response('Something Wrong', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return fractal()->collection($savedCartItems, new SavedCartItemTransformer());
    }

    /**
     *
     * @SWG\Post(
     *     path="/api/user/saved-cart/{id}",
     *     tags={"Orders"},
     *     summary="Update saved cart item",
     *     description="Update saved cart item",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="_method",
     *         in="formData",
     *         description="Cart item id",
     *         required=true,
     *         type="string",
     *         default="PUT",
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Cart item id",
     *         required=true,
     *         type="integer",
     *         default=74,
     *     ),
     *     @SWG\Parameter(
     *         name="product_id",
     *         in="formData",
     *         description="Product id",
     *         required=true,
     *         type="integer",
     *         default="4",
     *     ),
     *     @SWG\Parameter(
     *         name="quantity",
     *         in="formData",
     *         description="Quantity",
     *         required=true,
     *         type="integer",
     *         default="500",
     *     ),
     *     @SWG\Parameter(
     *         name="later_size_breakdown",
     *         in="formData",
     *         description="Breakdown would be provided after placing order",
     *         required=false,
     *         type="string",
     *         enum={"0","1"},
     *         default="1",
     *     ),
     *     @SWG\Parameter(
     *         name="imprint_comment",
     *         in="formData",
     *         description="Imprint comment",
     *         required=false,
     *         type="string",
     *         default="In Hnd Date Nov 9th",
     *     ),
     *     @SWG\Parameter(
     *         name="tax_exemption",
     *         in="formData",
     *         description="Tax exemption",
     *         required=false,
     *         type="string",
     *         enum={"0","1"},
     *         default="0",
     *     ),
     *     @SWG\Parameter(
     *         name="art_files[0]",
     *         in="formData",
     *         description="The Art file (.jpg, .png, .pdf)",
     *         required=false,
     *         type="file",
     *         default="",
     *     ),
     *     @SWG\Parameter(
     *         name="art_files[1]",
     *         in="formData",
     *         description="The Art file (.jpg, .png, .pdf)",
     *         required=false,
     *         type="file",
     *         default="",
     *     ),
     *      @SWG\Parameter(
     *         name="art_files[2]",
     *         in="formData",
     *         description="The Art file (.jpg, .png, .pdf)",
     *         required=false,
     *         type="file",
     *         default="",
     *     ),
     *     @SWG\Parameter(
     *         name="estimation_zip",
     *         in="formData",
     *         description="Estimation ZIP code",
     *         required=false,
     *         type="string",
     *         default="11211",
     *     ),
     *     @SWG\Parameter(
     *         name="estimation_shipping_method",
     *         in="formData",
     *         description="Estimation Shipping Company",
     *         required=false,
     *         type="string",
     *         enum={"ups","fedex","dhl","usps","truck"},
     *         default="ups",
     *     ),
     *     @SWG\Parameter(
     *         name="shipping_method",
     *         in="formData",
     *         description="Shipping Method",
     *         required=false,
     *         type="string",
     *         enum={"own_account","us_shipping","outside_us","custom"},
     *         default="outside_us",
     *     ),
     *     @SWG\Parameter(
     *         name="estimation_shipping_code",
     *         in="formData",
     *         description="UPS shipping service code",
     *         required=false,
     *         type="string",
     *         default="GND",
     *     ),
     *     @SWG\Parameter(
     *         name="own_shipping_type",
     *         in="formData",
     *         description="Own Shipping Type",
     *         required=false,
     *         type="string",
     *         enum={"Ground","2 Day Air","3 Day Air","Overnight"},
     *         default="Ground",
     *     ),
     *     @SWG\Parameter(
     *         name="own_account_number",
     *         in="formData",
     *         description="Own Account Number",
     *         required=false,
     *         type="string",
     *         default="1123123123",
     *     ),
     *     @SWG\Parameter(
     *         name="own_shipping_system",
     *         in="formData",
     *         description="Own Shipping Company",
     *         required=false,
     *         type="string",
     *         enum={"ups","fedex","dhl","usps","truck"},
     *         default="ups",
     *     ),
     *     @SWG\Parameter(
     *         name="received_date",
     *         in="formData",
     *         description="Received Date",
     *         required=false,
     *         type="string",
     *         format="date",
     *         default="2017-07-14",
     *     ),
     *     @SWG\Parameter(
     *         name="product_options",
     *         in="formData",
     *         description="Product Options",
     *         required=false,
     *         type="string",
     *         default="[{""product_option_id"":""1"",""product_sub_option_ids"":[""6""]},{""product_option_id"":""9"",""product_sub_option_ids"":[""5""]}]",
     *     ),
     *
     *     @SWG\Parameter(
     *         name="product_colors",
     *         in="formData",
     *         description="Product Colors",
     *         required=false,
     *         type="string",
     *         default="[{""color_group_id"":""1"",""color_id"":""3""}]",
     *     ),
     *
     *     @SWG\Parameter(
     *         name="product_imprints",
     *         in="formData",
     *         description="Product Imprints",
     *         required=false,
     *         type="string",
     *         default="[{""imprint_id"":""1"",""color_ids"":[""35"", ""33""]}]",
     *     ),
     *
     *     @SWG\Parameter(
     *         name="product_sizes",
     *         in="formData",
     *         description="Product Sizes",
     *         required=false,
     *         type="string",
     *         default="[{""imprint_id"":""1"",""color_ids"":[""35"", ""33""]}]",
     *         default="[{""size_group_id"":""1"",""size_ids"":[{""id"" : ""3"", ""qty"" : ""50""}, {""id"" : ""4"", ""qty"" : ""150""}]}]",
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/CartItem"))
     *         ),
     *     )
     * )
     *
     * Update Saved Cart Item
     *
     * @param Request $request
     * @param SavedCartItem $savedCartItem
     * @param CartManager $cartManager
     * @return \Spatie\Fractal\Fractal
     * @throws AuthorizationException
     */
    public function update(SavedCartItem $savedCartItem, UpdateCartItemRequest $request, CartManager $cartManager)
    {
        $cartItem = &$savedCartItem->cartItem;

        if (auth()->check()) {
            $this->authorize('update', $savedCartItem);
        } else {
            throw new AuthorizationException('This action is unauthorized.');
        }

        DB::transaction(function () use ($request, $cartManager, &$cartItem){
            $cartItem->fill($request->all());
            $cartManager->setCartItemCost($cartItem, $cartItem->product, $request->get('quantity'));
            $cartManager->setCartItemColors($cartItem, $cartItem->product, json_decode($request->get('product_colors'), true));
            $cartManager->setCartItemProductOptions($cartItem, $cartItem->product, json_decode($request->get('product_options'), true), $request->get('quantity'));
            $cartManager->setCartItemImprints($cartItem, $cartItem->product, json_decode($request->get('product_imprints'), true), $request->get('quantity'));
            $cartManager->setCartItemSizes($cartItem, $cartItem->product, $request->later_size_breakdown != 0 ? null : json_decode($request->get('product_sizes'), true), $request->get('quantity'));
            $cartManager->setCartItemArtFiles($cartItem, $request->file('art_files'));

            switch ($request->get('shipping_method')) {
                case 'outside_us':
                    break;
                case 'own_account':
                    $cartManager->setCartItemOwnAccountFields(
                        $cartItem,
                        $request->get('own_shipping_type'),
                        $request->get('own_account_number'),
                        $request->get('own_shipping_system')
                    );
                    break;
                case 'us_shipping':
                    $cartManager->setCartItemShippingCost($cartItem,
                        $cartItem->product,
                        $request->get('estimation_zip'),
                        $request->get('quantity'),
                        $request->get('estimation_shipping_code')
                    );
                    break;
                case 'custom':
                    $cartManager->setCustomCartItemShippingCost($cartItem, $cartItem->product);
                    break;
            }

            $cartItem->save();
        });

        return fractal()->item($savedCartItem, new SavedCartItemTransformer());
    }

    /**
     * @SWG\Delete(
     *     path="/api/user/saved-cart/{id}",
     *     tags={"Orders"},
     *     summary="Remove cart item from saved cart",
     *     description="Remove cart item from saved cart for authenticated user",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Saved cart item id",
     *         required=true,
     *         type="integer",
     *         default=1,
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description = "No Content"
     *     )
     * )
     *
     * Remove cart item from saved cart
     *
     * @param SavedCartItem $savedCartItem
     * @return Response
     */
    public function destroy(SavedCartItem $savedCartItem)
    {
        $this->authorize('delete', $savedCartItem);
        $savedCartItem->cartItem->delete();
        $savedCartItem->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * @SWG\Post(
     *     path="/api/user/saved-cart/move-to-cart",
     *     tags={"Orders"},
     *     summary="Move saved cart item from saved cart to cart",
     *     description="Move saved cart item from saved cart to cart for authenticated user",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="saved_cart_item_ids[]",
     *         in="formData",
     *         description="Array of saved cart item ids",
     *         required=true,
     *         type="array",
     *         items="integer",
     *         default="1"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/CartItem"))
     *     )
     * )
     *
     * Move saved cart item from saved cart to cart
     *
     * @param Request $request
     * @return \Spatie\Fractal\Fractal
     */
    public function moveToCart(Request $request)
    {
        $this->validate($request, [
            'saved_cart_item_ids' => 'required|array|exists:saved_cart_items,id'
        ]);

        $user = Auth::user();

        /** @var Cart $cart */
        $cart = Cart::current($user)->first();

        $savedCartItems = SavedCartItem::whereIn('id', $request->get('saved_cart_item_ids'))
            ->where('user_id', $user->id)->get();

        $cartItems = [];

        DB::transaction(function () use ($savedCartItems, $cart, &$cartItems) {
            foreach ($savedCartItems as $savedCartItem) {
                $savedCartItem->cartItem->cart()->associate($cart)->save();
                $cartItems[] = $savedCartItem->cartItem;
                $savedCartItem->delete();
            }
        });

        return fractal()->collection($cartItems, new CartItemTransformer());
    }
}
