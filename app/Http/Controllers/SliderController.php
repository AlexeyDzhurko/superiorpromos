<?php


namespace App\Http\Controllers;


use App\Models\Slider;
use App\Transformers\SliderTransformer;

class SliderController extends Controller
{
    /**
     *  @SWG\Get(
     *     path="/api/slider",
     *     tags={"Main Page Carousel"},
     *     summary="List of images",
     *     description="List of images for slider",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *              @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/Slider")),
     *         ),
     *     )
     * )
     * Display a listing of active Editable Blocks.
     */
    public function index()
    {
        return fractal(Slider::sorted()->get(), new SliderTransformer());
    }

}