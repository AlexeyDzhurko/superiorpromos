<?php

namespace App\Http\Controllers;


use App;
use View;
use App\Models\Product;
use App\Transformers\ProductTransformer;

class GeneratePdfController extends Controller
{
    /**
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function getPdf(Product $product)
    {
        try {
            $outProduct = fractal()->item($product, new ProductTransformer())->toArray();
            $view = View::make('pdf.product-pdf', ['product' => $outProduct])->render();
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($view)->setPaper('a4');

            return $pdf->stream('product-' . $product->id . '.pdf');
        }catch (\Exception $exception) {
            abort(404);
        }
    }


    /**
     * @param $fileName
     * @return mixed
     */
    public function getQuickQuotePdf($fileName) {
        $path = storage_path().'/app/public/quick-quote/'. $fileName . '.pdf';

        if (!file_exists($path)) {
            abort(404);
        }

        $headers = [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'. $fileName .'"'
        ];

        return response()->file($path, $headers);
    }
}
