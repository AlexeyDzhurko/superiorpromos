<?php


namespace App\Http\Controllers;


use App\Models\Promos;

class PromosController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $promos = Promos::all()->groupBy('position');

        return response()->json($promos);
    }
}
