<?php

namespace App\Http\Controllers;

use App\Contracts\CartManager;
use App\Contracts\MailService;
use App\Contracts\PaymentGateway;
use App\Discounts\DiscountCalc;
use App\Discounts\DiscountCreator;
use App\Enums\DiscountTypeEnum;
use App\Enums\PaymentStatusEnum;
use App\Enums\PaymentTypeEnum;
use App\Exceptions\DiscountException;
use App\Http\Requests\CheckoutRequest;
use App\Mail\SendReorder;
use App\Models\Address;
use App\Models\ArtProof;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Discount;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Payment;
use App\Models\Stage;
use App\Models\Vendor;
use App\Notifications\BigMailer\ReorderNotification;
use App\Payment\CartItemCost;
use App\PaymentProfile;
use App\Services\OrderCalculation;
use App\Transformers\DiscountCalcTransformer;
use App\Transformers\OrderTransformer;
use App\Transformers\CartItemTransformer;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Http\Response;
use DB;
use net\authorize\api\contract\v1\PaymentType;

class OrderController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/order",
     *     tags={"Orders"},
     *     summary="Display orders history",
     *     description="Display orders history for authenticated user",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description = "Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/Order")),
     *     )
     * )
     *
     * Display orders history
     */
    public function index()
    {
        $orders = Order::whereUserId(Auth::id())
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('order_items')
                    ->whereRaw('order_items.order_id = orders.id');
            })
            ->with('orderItems', 'orderItems.trackingInformations')
            ->orderBy('created_at', 'desc')->get();

        return fractal()->collection($orders, new OrderTransformer());
    }


    protected function getNewOrder(OrderCalculation $orderCalculation, CartManager $cartManager): Order
    {
        $user = Auth::user();

        /** @var Cart $cart */
        $cart = Cart::current($user)->first();

        $order = new Order();
        $order->user()->associate($user);
        $order->cart()->associate($cart);
//        $order->save();

        foreach ($cart->cartItems as $cartItem) {
            /** @var CartItem $cartItem */

            $cartManager->recalculateCart($cartItem);

            $orderItem = new OrderItem();
//            $orderItem->order()->associate($order);
            $orderItem->product()->associate($cartItem->product);
            $orderItem->cartItem()->associate($cartItem);
            $orderItem->stage_id = Stage::DEFAULT_ID;

            $orderItem->quantity = $cartItem->quantity;
            $orderItem->item_price = $orderCalculation->getItemPrice($cartItem->product, $cartItem->quantity);
            $orderItem->price = ($cartItem->quantity * $orderItem->item_price) + ($cartItem->estimation_shipping_price ?? 0);

            //TODO: add calculation
            $orderItem->options_price = 0;

            $orderItem->estimation_zip = $cartItem->estimation_zip;
            $orderItem->estimation_shipping_method = $cartItem->estimation_shipping_method;
            $orderItem->estimation_shipping_price = $cartItem->estimation_shipping_price;
            $orderItem->own_shipping_type = $cartItem->own_shipping_type;
            $orderItem->own_account_number = $cartItem->own_account_number;
            $orderItem->own_shipping_system = $cartItem->own_shipping_system;
            $orderItem->shipping_method = $cartItem->shipping_method;
            $orderItem->received_date = $cartItem->received_date;
            $orderItem->imprint_comment = $cartItem->imprint_comment;

            $order->orderItems->add($orderItem);
        }

        return $order;
    }

    /**
     * @SWG\Post(
     *     path="/api/order/checkout",
     *     tags={"Orders"},
     *     summary="Order checkout",
     *     description="Order checkout",
     *     produces={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="shipping_address_id",
     *         in="formData",
     *         description="Shipping address id",
     *         required=true,
     *         type="integer",
     *         default=23,
     *     ),
     *     @SWG\Parameter(
     *         name="billing_address_id",
     *         in="formData",
     *         description="Billing address id",
     *         required=false,
     *         type="integer",
     *         default=42,
     *     ),
     *     @SWG\Parameter(
     *         name="payment_profile_id",
     *         in="formData",
     *         description="Payment profile id",
     *         required=true,
     *         type="integer",
     *         default=35,
     *     ),
     *
     *     @SWG\Response(
     *         response=204,
     *         description = "No Content"
     *     )
     * )
     *
     * Order checkout
     *
     * @param CheckoutRequest $request
     * @param OrderCalculation $orderCalculation
     * @param DiscountCreator $discountCreator
     * @param CartManager $cartManager
     * @param PaymentGateway $paymentGateway
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function checkout(
        CheckoutRequest $request,
        OrderCalculation $orderCalculation,
        DiscountCreator $discountCreator,
        CartManager $cartManager,
        PaymentGateway $paymentGateway
    ) {
        $result = null;

        $user = Auth::user();

        $payment_profile = $request->get('payment_profile');
        $paymentType = $payment_profile['type'];
        $cvv = $payment_profile['cvv'];

        /** @var PaymentProfile $paymentProfile */
        $paymentProfile = isset($payment_profile['id']) ? PaymentProfile::find($payment_profile['id']) : null;

        /** @var Cart $cart */
        $cart = Cart::current($user)->first();

        if (is_null($cart) || $cart->cartItems->count() == 0) {
            abort(Response::HTTP_BAD_REQUEST, 'Cart is empty');
        }

        $array = DB::transaction(function () use (
            $request,
            $cartManager,
            $orderCalculation,
            $user,
            $cart,
            $discountCreator,
            $paymentGateway,
            $paymentProfile,
            &$results,
            $paymentType,
            $cvv
        ) {
            $order = $this->getNewOrder($orderCalculation, $cartManager);
            $order->user()->associate($user);
            $order->cart()->associate($cart);
//            $order->save();


            foreach ($order->orderItems as $orderItem) {
                $vendor = Vendor::find($orderItem->cartItem->product->vendor_id);
                /** @var OrderItem $orderItem */
                $cartItemCost = new CartItemCost($orderItem->cartItem);
                $orderItem->quantity = $orderItem->cartItem->quantity;
                $orderItem->item_price = $cartItemCost->getBaseItemPrice();
                $orderItem->price = $cartItemCost->getTotalWithoutShipping();
                $orderItem->vendor_id = $vendor ? $vendor->id : Vendor::EMPTY_VENDOR_ID;

                if ($paymentType === PaymentTypeEnum::APPLY_FOR_TERMS || $paymentType === PaymentTypeEnum::CHECK_MONEY_ORDER) {
                    $orderItem->not_paid = true;
                }

                $order->price += $orderItem->price;
                $order->total_price += $cartItemCost->getTotal();


                /** @var Address $shippingAddress */
                $shippingAddress = Address::find($request->get('shipping_address_id'));

                /** @var Address $billingAddress */
                $billingAddress = $request->has('billing_address_id')
                    ? Address::find($request->get('billing_address_id')) : $shippingAddress;

                $addressFields = [
                    'first_name',
                    'middle_name',
                    'last_name',
                    'title',
                    'suffix',
                    'company_name',
                    'address_line_1',
                    'address_line_2',
                    'city',
                    'state',
                    'zip',
                    'country',
                    'province',
                    'day_telephone',
                    'ext',
                    'fax',
                ];

                foreach ($addressFields as $addressField) {
                    $order->setAttribute('shipping_' . $addressField, $shippingAddress->getAttribute($addressField));
                    $order->setAttribute('billing_' . $addressField, $billingAddress->getAttribute($addressField));
                    $orderItem->setAttribute('shipping_' . $addressField,
                        $shippingAddress->getAttribute($addressField));
                    $orderItem->setAttribute('billing_' . $addressField, $billingAddress->getAttribute($addressField));
                }

                $orderItem->save();
            }

            if ($cart->discount) {
                $order->discount()->associate($cart->discount->id);
                $order->discount_code = $cart->discount->code;
                $discountCalculations = $discountCreator->createCalcFromJson($cart->discount->discount_calc,
                    $order->cart);
                foreach ($discountCalculations as $discountCalculation) {
                    /** @var DiscountCalc $discountCalculation */
                    $order->discount_amount += $discountCalculation->getDiscountAmount();
                }
                $order->total_price = number_format(($order->total_price - $order->discount_amount), 2, '.', '');
            }

            if (!is_null($paymentProfile)) {
                $transactionResponse = $paymentGateway->createTransaction($user, $paymentProfile, $order,
                    'Initial Payment', $cvv);

                if (isset($transactionResponse['transaction_id'])) {
                    $order->save();
                    foreach ($order->orderItems as $orderItem) {
                        $orderItem->order()->associate($order);
                        $orderItem->save();
                    }

                    $payment = new Payment();
                    $payment->order()->associate($order);
                    $payment->paymentProfile()->associate($paymentProfile);
                    $payment->status = PaymentStatusEnum::STATUS_PAID;
                    $payment->transaction_id = $transactionResponse['transaction_id'];
                    $payment->type = PaymentTypeEnum::INITIAL_TYPE;
                    $payment->price = $transactionResponse['amount'];
                    $payment->save();
                } else {
                    $message = $transactionResponse['error_message'] ?: 'Something went wrong while payment. Please try another payment card.';
                    abort(Response::HTTP_BAD_REQUEST, $message);
                }
            } else {
                $order->save();
                foreach ($order->orderItems as $orderItem) {
                    $orderItem->order()->associate($order);
                    $orderItem->save();
                }
            }

            if (!is_null($order->discount)) {
                if ($order->discount->type === DiscountTypeEnum::DISPOSABLE) {
                    $order->discount->delete();
                }
            }

            $results = ['order' => $order];
        });

        return
            [
                'order' => fractal()->collection($results, new OrderTransformer()),
                'cartItems' => fractal()->collection($cart->cartItems, new CartItemTransformer()),
            ];

    }

    /**
     * @SWG\Post(
     *     path="/api/cart-item/reorder",
     *     tags={"Orders"},
     *     summary="Order reorder",
     *     description="Order reorder",
     *     produces={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="comment",
     *         in="formData",
     *         description="Comment to reorder",
     *         required=false,
     *         type="string",
     *         default="Please help me with selecting imprints",
     *     ),
     *     @SWG\Parameter(
     *         name="is_delivered_by_asap",
     *         in="formData",
     *         description="",
     *         required=true,
     *         type="boolean",
     *         default=true,
     *     ),
     *     @SWG\Parameter(
     *         name="received_date",
     *         in="formData",
     *         description="Required if 'is_delivered_by_asap' == false",
     *         required=false,
     *         type="string",
     *         default="2018-06-25",
     *     ),
     *     @SWG\Parameter(
     *         name="is_imprint_color_change",
     *         in="formData",
     *         description="",
     *         required=true,
     *         type="boolean",
     *         default=false,
     *     ),
     *     @SWG\Parameter(
     *         name="product_imprints[]",
     *         in="formData",
     *         description="Array of imprints. Required if 'is_imprint_color_change' == true",
     *         required=false,
     *         type="array",
     *         items="integer",
     *         default="[['imprint_id' => 4, 'color_ids' => [1, 2, 3]]]"
     *     ),
     *     @SWG\Parameter(
     *         name="is_item_color_change",
     *         in="formData",
     *         description="",
     *         required=true,
     *         type="boolean",
     *         default=false,
     *     ),
     *     @SWG\Parameter(
     *         name="product_colors[]",
     *         in="formData",
     *         description="Array of product colors id. Required if 'is_item_color_change' == true",
     *         required=false,
     *         type="array",
     *         items="integer",
     *         default="[['color_id' => 1, 'color_group_id' => 2], ['color_id' => 3, 'color_group_id' => 2]]"
     *     ),
     *     @SWG\Parameter(
     *         name="is_new_artwork_provide",
     *         in="formData",
     *         description="",
     *         required=true,
     *         type="boolean",
     *         default=false,
     *     ),
     *     @SWG\Parameter(
     *         name="is_quantity_change",
     *         in="formData",
     *         description="",
     *         required=true,
     *         type="boolean",
     *         default=false,
     *     ),
     *     @SWG\Parameter(
     *         name="quantity",
     *         in="formData",
     *         description="Required if 'is_quantity_change' == true",
     *         required=false,
     *         type="integer",
     *         default=false,
     *     ),
     *     @SWG\Parameter(
     *         name="is_same_contents",
     *         in="formData",
     *         description="",
     *         required=true,
     *         type="boolean",
     *         default=false,
     *     ),
     *     @SWG\Parameter(
     *         name="is_shipping_same",
     *         in="formData",
     *         description="",
     *         required=true,
     *         type="boolean",
     *         default=true,
     *     ),
     *     @SWG\Parameter(
     *         name="shipping_address_id",
     *         in="formData",
     *         description="Required if 'is_shipping_same' == false",
     *         required=false,
     *         type="integer",
     *         default=1,
     *     ),
     *     @SWG\Parameter(
     *         name="is_billing_same",
     *         in="formData",
     *         description="",
     *         required=true,
     *         type="boolean",
     *         default=true,
     *     ),
     *     @SWG\Parameter(
     *         name="billing_address_id",
     *         in="formData",
     *         description="Required if 'is_billing_same' == false",
     *         required=false,
     *         type="integer",
     *         default=2,
     *     ),
     *     @SWG\Parameter(
     *         name="is_payment_same",
     *         in="formData",
     *         description="",
     *         required=true,
     *         type="boolean",
     *         default=true,
     *     ),
     *     @SWG\Parameter(
     *         name="payment_profile_id",
     *         in="formData",
     *         description="Required if 'is_payment_same' == false",
     *         required=false,
     *         type="integer",
     *         default=3,
     *     ),
     *     @SWG\Parameter(
     *         name="order_item_id",
     *         in="formData",
     *         description="",
     *         required=false,
     *         type="integer",
     *         default=14,
     *     ),
     *
     *     @SWG\Response(
     *         response=204,
     *         description = "No Content"
     *     )
     * )
     *
     * Reorder existing order
     *
     * @param Request $request
     * @param MailService $mailService
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function reorder(Request $request, MailService $mailService)
    {
        $this->validate($request, [
            'orders.*.comment' => 'nullable|string',

            'orders.*.is_delivered_by_asap' => 'required|boolean',
            'orders.*.received_date' => 'required_if:is_delivered_by_asap,false',

            'orders.*.is_imprint_color_change' => 'required|boolean',
            'orders.*.product_imprints' => 'required_if:is_imprint_color_change,true|array',
            'orders.*.product_imprints.color_ids' => 'required_if:is_imprint_color_change,true|array',

            'orders.*.is_item_color_change' => 'required|boolean',
            'orders.*.product_colors' => 'required_if:is_item_color_change,true|array',

            'orders.*.is_new_artwork_provide' => 'required|boolean',

            'orders.*.is_quantity_change' => 'required|boolean',
            'orders.*.quantity' => 'required_if:is_quantity_change,true|numeric',

            'orders.*.is_same_contents' => 'required|boolean',

            'orders.*.is_shipping_same' => 'required|boolean',
            'shipping_address_id' => 'required_if:is_shipping_same,false|exists:addresses,id',

            'orders.*.is_billing_same' => 'required|boolean',
            'billing_address_id' => 'required_if:is_billing_same,false|exists:addresses,id',

            'orders.*.is_payment_same' => 'required|boolean',
            'payment_profile_id' => 'required_if:is_payment_same,false|exists:payment_profiles,id',

            'orders.*.order_item_id' => 'required|exists:cart_items,id',
        ]);
        if (empty($request->input('orders'))) {
            return \response('empty data', 422);
        }

        foreach ($request->input('orders') ?? [] as $order) {
            $sendToUser = $mailService->sendSingleTransactionalEmail(new ReorderNotification([auth()->user()->email],
                (array)$order));
            if ($sendToUser) {
                $recipients = config('site-settings.new_order_email') ? explode(',',
                    config('site-settings.new_order_email')) : [];
                $mailService->sendMultipleTransactionalEmail(new ReorderNotification($recipients, (array)$order, true));
            }
        }

        return \response('', Response::HTTP_NO_CONTENT);
    }
}
