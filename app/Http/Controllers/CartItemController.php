<?php

namespace App\Http\Controllers;

use App\Contracts\CartManager;
use App\Http\Requests\AddToCartRequest;
use App\Http\Requests\UpdateCartItemRequest;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Transformers\CartItemTransformer;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use Auth;

/**
 * Class CartItemController
 * @package App\Http\Controllers
 */
class CartItemController extends Controller
{
    /**
     * @SWG\Post(
     *     path="/api/cart-item",
     *     tags={"Orders"},
     *     summary="Add product to cart",
     *     description="Add product to cart",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="token",
     *         in="formData",
     *         description="User token",
     *         required=true,
     *         type="string",
     *         default="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjM0MTUyLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTU0MzkzMTcyMiwiZXhwIjoxNTQ1MTQxMzIyLCJuYmYiOjE1NDM5MzE3MjIsImp0aSI6IjRmYzdhMDBiNjcyYjMwZmJlYjUwMDgyZjY0ZTJiYTI2In0.uXQQ9MZKbcuwlNRU2xiX7MCMT1aL007Oe5UyWPrVIGo",
     *     ),
     *     @SWG\Parameter(
     *         name="product_id",
     *         in="formData",
     *         description="Product id",
     *         required=true,
     *         type="integer",
     *         default="1006",
     *     ),
     *     @SWG\Parameter(
     *         name="quantity",
     *         in="formData",
     *         description="Quantity",
     *         required=true,
     *         type="integer",
     *         default="500",
     *     ),
     *     @SWG\Parameter(
     *         name="later_size_breakdown",
     *         in="formData",
     *         description="Breakdown would be provided after placing order",
     *         required=false,
     *         type="string",
     *         enum={"0","1"},
     *         default="1",
     *     ),
     *     @SWG\Parameter(
     *         name="imprint_comment",
     *         in="formData",
     *         description="Imprint comment",
     *         required=false,
     *         type="string",
     *         default="In Hnd Date Nov 9th",
     *     ),
     *     @SWG\Parameter(
     *         name="tax_exemption",
     *         in="formData",
     *         description="Tax exemption",
     *         required=false,
     *         type="string",
     *         enum={"0","1"},
     *         default="0",
     *     ),
     *     @SWG\Parameter(
     *         name="art_files[0]",
     *         in="formData",
     *         description="The Art file (.jpg, .png, .pdf)",
     *         required=false,
     *         type="file",
     *         default="",
     *     ),
     *     @SWG\Parameter(
     *         name="art_files[1]",
     *         in="formData",
     *         description="The Art file (.jpg, .png, .pdf)",
     *         required=false,
     *         type="file",
     *         default="",
     *     ),
     *      @SWG\Parameter(
     *         name="art_files[2]",
     *         in="formData",
     *         description="The Art file (.jpg, .png, .pdf)",
     *         required=false,
     *         type="file",
     *         default="",
     *     ),
     *     @SWG\Parameter(
     *         name="estimation_zip",
     *         in="formData",
     *         description="Estimation ZIP code",
     *         required=false,
     *         type="string",
     *         default="11211",
     *     ),
     *     @SWG\Parameter(
     *         name="estimation_shipping_method",
     *         in="formData",
     *         description="Estimation Shipping Company",
     *         required=false,
     *         type="string",
     *         enum={"ups","fedex","dhl","usps","truck"},
     *         default="ups",
     *     ),
     *     @SWG\Parameter(
     *         name="shipping_method",
     *         in="formData",
     *         description="Shipping Method",
     *         required=false,
     *         type="string",
     *         enum={"own_account","us_shipping","outside_us","custom"},
     *         default="outside_us",
     *     ),
     *     @SWG\Parameter(
     *         name="estimation_shipping_code",
     *         in="formData",
     *         description="UPS shipping service code",
     *         required=false,
     *         type="string",
     *         default="GND",
     *     ),
     *     @SWG\Parameter(
     *         name="own_shipping_type",
     *         in="formData",
     *         description="Own Shipping Type",
     *         required=false,
     *         type="string",
     *         enum={"Ground","2 Day Air","3 Day Air","Overnight"},
     *         default="Ground",
     *     ),
     *     @SWG\Parameter(
     *         name="own_account_number",
     *         in="formData",
     *         description="Own Account Number",
     *         required=false,
     *         type="string",
     *         default="1123123123",
     *     ),
     *     @SWG\Parameter(
     *         name="own_shipping_system",
     *         in="formData",
     *         description="Own Shipping Company",
     *         required=false,
     *         type="string",
     *         enum={"ups","fedex","dhl","usps","truck"},
     *         default="ups",
     *     ),
     *     @SWG\Parameter(
     *         name="is_sample",
     *         in="formData",
     *         description="It order is a sample",
     *         required=false,
     *         type="boolean",
     *         default="false",
     *     ),
     *     @SWG\Parameter(
     *         name="received_date",
     *         in="formData",
     *         description="Received Date",
     *         required=false,
     *         type="string",
     *         format="date",
     *         default="2017-07-14",
     *     ),
     *     @SWG\Parameter(
     *         name="product_options",
     *         in="formData",
     *         description="Product Options",
     *         required=false,
     *         type="string",
     *         default="[{""product_option_id"":""1"",""product_sub_option_ids"":[""6""]},{""product_option_id"":""9"",""product_sub_option_ids"":[""5""]}]",
     *     ),
     *
     *     @SWG\Parameter(
     *         name="product_colors",
     *         in="formData",
     *         description="Product Colors",
     *         required=false,
     *         type="string",
     *         default="[{""color_group_id"":""1"",""color_id"":""3""}]",
     *     ),
     *
     *     @SWG\Parameter(
     *         name="product_imprints",
     *         in="formData",
     *         description="Product Imprints",
     *         required=false,
     *         type="string",
     *         default="[{""imprint_id"":""1"",""color_ids"":[""35"", ""33""]}]",
     *     ),
     *
     *     @SWG\Parameter(
     *         name="product_sizes",
     *         in="formData",
     *         description="Product Sizes",
     *         required=false,
     *         type="string",
     *         default="[{""imprint_id"":""1"",""color_ids"":[""35"", ""33""]}]",
     *         default="[{""size_group_id"":""1"",""size_ids"":[{""id"" : ""3"", ""qty"" : ""50""}, {""id"" : ""4"", ""qty"" : ""150""}]}]",
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/CartItem"))
     *         ),
     *     )
     * )
     *
     * Add item to cart
     *
     * @param AddToCartRequest $request
     * @param CartManager $cartManager
     * @return \Spatie\Fractal\Fractal
     */
    public function store(AddToCartRequest $request, CartManager $cartManager)
    {
        $product = Product::find($request->get('product_id'));

        $cartItem = new CartItem($request->all());

        DB::transaction(function () use ($request, $product, $cartManager, &$cartItem) {

            $cartManager->buildCart(
                $cartItem,
                $product,
                $request
            );
        });

        return fractal()->item($cartItem, new CartItemTransformer());
    }

    /**
     *
     * @SWG\Post(
     *     path="/api/cart-item/{id}",
     *     tags={"Orders"},
     *     summary="Update cart item",
     *     description="Update cart item",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="_method",
     *         in="formData",
     *         description="Cart item id",
     *         required=true,
     *         type="string",
     *         default="PUT",
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Cart item id",
     *         required=true,
     *         type="integer",
     *         default=74,
     *     ),
     *     @SWG\Parameter(
     *         name="product_id",
     *         in="formData",
     *         description="Product id",
     *         required=true,
     *         type="integer",
     *         default="4",
     *     ),
     *     @SWG\Parameter(
     *         name="quantity",
     *         in="formData",
     *         description="Quantity",
     *         required=true,
     *         type="integer",
     *         default="500",
     *     ),
     *     @SWG\Parameter(
     *         name="later_size_breakdown",
     *         in="formData",
     *         description="Breakdown would be provided after placing order",
     *         required=false,
     *         type="string",
     *         enum={"0","1"},
     *         default="1",
     *     ),
     *     @SWG\Parameter(
     *         name="imprint_comment",
     *         in="formData",
     *         description="Imprint comment",
     *         required=false,
     *         type="string",
     *         default="In Hnd Date Nov 9th",
     *     ),
     *     @SWG\Parameter(
     *         name="tax_exemption",
     *         in="formData",
     *         description="Tax exemption",
     *         required=false,
     *         type="string",
     *         enum={"0","1"},
     *         default="0",
     *     ),
     *     @SWG\Parameter(
     *         name="art_files[0]",
     *         in="formData",
     *         description="The Art file (.jpg, .png, .pdf)",
     *         required=false,
     *         type="file",
     *         default="",
     *     ),
     *     @SWG\Parameter(
     *         name="art_files[1]",
     *         in="formData",
     *         description="The Art file (.jpg, .png, .pdf)",
     *         required=false,
     *         type="file",
     *         default="",
     *     ),
     *      @SWG\Parameter(
     *         name="art_files[2]",
     *         in="formData",
     *         description="The Art file (.jpg, .png, .pdf)",
     *         required=false,
     *         type="file",
     *         default="",
     *     ),
     *     @SWG\Parameter(
     *         name="estimation_zip",
     *         in="formData",
     *         description="Estimation ZIP code",
     *         required=false,
     *         type="string",
     *         default="11211",
     *     ),
     *     @SWG\Parameter(
     *         name="estimation_shipping_method",
     *         in="formData",
     *         description="Estimation Shipping Company",
     *         required=false,
     *         type="string",
     *         enum={"ups","fedex","dhl","usps","truck"},
     *         default="ups",
     *     ),
     *     @SWG\Parameter(
     *         name="shipping_method",
     *         in="formData",
     *         description="Shipping Method",
     *         required=false,
     *         type="string",
     *         enum={"own_account","us_shipping","outside_us","custom"},
     *         default="outside_us",
     *     ),
     *     @SWG\Parameter(
     *         name="estimation_shipping_code",
     *         in="formData",
     *         description="UPS shipping service code",
     *         required=false,
     *         type="string",
     *         default="GND",
     *     ),
     *     @SWG\Parameter(
     *         name="own_shipping_type",
     *         in="formData",
     *         description="Own Shipping Type",
     *         required=false,
     *         type="string",
     *         enum={"Ground","2 Day Air","3 Day Air","Overnight"},
     *         default="Ground",
     *     ),
     *     @SWG\Parameter(
     *         name="own_account_number",
     *         in="formData",
     *         description="Own Account Number",
     *         required=false,
     *         type="string",
     *         default="1123123123",
     *     ),
     *     @SWG\Parameter(
     *         name="own_shipping_system",
     *         in="formData",
     *         description="Own Shipping Company",
     *         required=false,
     *         type="string",
     *         enum={"ups","fedex","dhl","usps","truck"},
     *         default="ups",
     *     ),
     *     @SWG\Parameter(
     *         name="received_date",
     *         in="formData",
     *         description="Received Date",
     *         required=false,
     *         type="string",
     *         format="date",
     *         default="2017-07-14",
     *     ),
     *     @SWG\Parameter(
     *         name="product_options",
     *         in="formData",
     *         description="Product Options",
     *         required=false,
     *         type="string",
     *         default="[{""product_option_id"":""1"",""product_sub_option_ids"":[""6""]},{""product_option_id"":""9"",""product_sub_option_ids"":[""5""]}]",
     *     ),
     *
     *     @SWG\Parameter(
     *         name="product_colors",
     *         in="formData",
     *         description="Product Colors",
     *         required=false,
     *         type="string",
     *         default="[{""color_group_id"":""1"",""color_id"":""3""}]",
     *     ),
     *
     *     @SWG\Parameter(
     *         name="product_imprints",
     *         in="formData",
     *         description="Product Imprints",
     *         required=false,
     *         type="string",
     *         default="[{""imprint_id"":""1"",""color_ids"":[""35"", ""33""]}]",
     *     ),
     *
     *     @SWG\Parameter(
     *         name="product_sizes",
     *         in="formData",
     *         description="Product Sizes",
     *         required=false,
     *         type="string",
     *         default="[{""imprint_id"":""1"",""color_ids"":[""35"", ""33""]}]",
     *         default="[{""size_group_id"":""1"",""size_ids"":[{""id"" : ""3"", ""qty"" : ""50""}, {""id"" : ""4"", ""qty"" : ""150""}]}]",
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/CartItem"))
     *         ),
     *     )
     * )
     *
     * Update Cart Item
     *
     * @param UpdateCartItemRequest $request
     * @param CartItem $cartItem
     * @param CartManager $cartManager
     * @return \Spatie\Fractal\Fractal
     */
    public function update(UpdateCartItemRequest $request, CartItem $cartItem, CartManager $cartManager)
    {
        $this->authorizeCartItem($cartItem, $request);

        DB::transaction(function () use ($request, $cartManager, &$cartItem){
            $cartItem->fill($request->all());
            $cartManager->setCartItemCost($cartItem, $cartItem->product, $request->get('quantity'));
            $cartManager->setCartItemColors($cartItem, $cartItem->product, json_decode($request->get('product_colors'), true));
            $cartManager->setCartItemProductOptions($cartItem, $cartItem->product, json_decode($request->get('product_options'), true), $request->get('quantity'));
            $cartManager->setCartItemImprints($cartItem, $cartItem->product, json_decode($request->get('product_imprints'), true), $request->get('quantity'));
            $cartManager->setCartItemSizes($cartItem, $cartItem->product, $request->later_size_breakdown != 0 ? null : json_decode($request->get('product_sizes'), true), $request->get('quantity'));
            $cartManager->setCartItemArtFiles($cartItem, $request->file('art_files'));

            switch ($request->get('shipping_method')) {
                case 'outside_us':
                    $cartManager->setCartItemShippingCost($cartItem,
                        $cartItem->product,
                        $request->get('estimation_zip'),
                        $request->get('quantity'),
                        $request->get('estimation_shipping_code')
                    );
                    $cartManager->setCartItemOwnAccountFields(
                        $cartItem,
                        $request->get('own_shipping_type'),
                        $request->get('own_account_number'),
                        $request->get('own_shipping_system')
                    );
                    break;
                case 'own_account':
                    $cartManager->setCartItemOwnAccountFields(
                        $cartItem,
                        $request->get('own_shipping_type'),
                        $request->get('own_account_number'),
                        $request->get('own_shipping_system')
                    );
                    break;
                case 'us_shipping':
                    $cartManager->setCartItemShippingCost($cartItem,
                        $cartItem->product,
                        $request->get('estimation_zip'),
                        $request->get('quantity'),
                        $request->get('estimation_shipping_code')
                    );
                    break;
                case 'custom':
                    $cartManager->setCustomCartItemShippingCost($cartItem, $cartItem->product);
                    break;
            }

            $cartItem->save();
        });

        return fractal()->item($cartItem, new CartItemTransformer());
    }

    /**
     * @SWG\Delete(
     *     path="/api/cart-item/{id}",
     *     tags={"Orders"},
     *     summary="Remove cart item from cart",
     *     description="Remove cart item from cart",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Cart item id",
     *         required=true,
     *         type="integer",
     *         default=74,
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description = "No Content"
     *     )
     * )
     *
     * Remove cart item from cart
     *
     * @param CartItem $cartItem
     * @param Request $request
     * @return Response
     */
    public function destroy(CartItem $cartItem, Request $request)
    {
        $this->authorizeCartItem($cartItem, $request);
        $cartItem->delete();
        
        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * @SWG\Get(
     *     path="/api/cart-item",
     *     tags={"Orders"},
     *     summary="Display list of current cart items for current user",
     *     description="Display list of current cart items for current user",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description = "Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/CartItem")),
     *     )
     * )
     *
     * @param Request $request
     * @return \Spatie\Fractal\Fractal
     *
     * Returns cart items from current cart for authenticated user
     */
    public function index(Request $request)
    {
        /** @var Cart $cart */
        $cart = Auth::check()
            ? Cart::current(Auth::user())->first()
            : Cart::whereSession($request->header('guest'))->first();

        $cartItems = $cart->cartItems ?? [];

        return fractal()->collection($cartItems, new CartItemTransformer());
    }

    /**
     * @param CartItem $cartItem
     * @param Request $request
     * @throws AuthorizationException
     */
    public function authorizeCartItem(CartItem $cartItem, Request $request)
    {
        if (Auth::check()) {
            $this->authorize('view', $cartItem);
        } elseif($cartItem->cart->session != $request->header('guest')) {
            throw new AuthorizationException('This action is unauthorized.');
        }
    }


    /**
     * @SWG\Post(
     *     path="/api/cart-item/import-from-order",
     *     tags={"Orders"},
     *     summary="Import cart items from exist order by order item ids",
     *     description="Import cart items from exist order by order item ids",
     *     produces={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="order_item_ids[]",
     *         in="formData",
     *         description="Array of Order Item Ids",
     *         required=true,
     *         type="array",
     *         items="integer",
     *         default="23"
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/CartItem"))
     *         ),
     *     )
     * )
     *
     * Import cart items from exist order by order item ids
     *
     * @param Request $request
     * @return $this
     * @throws AuthorizationException
     */
    public function importFromOrder(Request $request)
    {
        $this->validate($request, [
            'order_item_ids' => 'required|array',
            'order_item_ids.*' => 'required|exists:order_items,id'
        ]);

        $CartItems = [];

        foreach ($request->order_item_ids as $order_item_id) {
            $orderItem = OrderItem::findOrFail($order_item_id);

            $this->authorize('view', $orderItem);

            $CartItems[] = $orderItem->cartItem;
        }

        return fractal()->collection($CartItems, new CartItemTransformer);
    }
}
