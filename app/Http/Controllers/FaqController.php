<?php

namespace App\Http\Controllers;

use App\Models\Faq;

class FaqController extends Controller
{
    /**
     *  @SWG\Get(
     *     path="/api/faq",
     *     tags={"FAQ"},
     *     summary="List FAQ",
     *     description="List FAQ",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *              @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/Faq")),
     *         ),
     *     )
     * )
     * Display a listing of the FAQs.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faq = Faq::whereActive(Faq::ACTIVE)->get();
        return response()->json($faq);
    }
}
