<?php

namespace App\Http\Controllers;

use App\Admin\Transformers\PostTransformer;
use App\Http\Filters\PostFilter;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class PostController extends Controller
{
    /**
     *  @SWG\Get(
     *     path="/api/blog/posts",
     *     tags={"Blog"},
     *     summary="Blog Posts",
     *     description="Blog Posts",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *              @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/Post")),
     *         ),
     *     )
     * )
     *
     * Display a listing of the blog posts.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        try {
            $posts = Post::where('status', 1)->paginate(10);
            $posts->setCollection(new Collection(fractal($posts->getCollection(), new PostTransformer)));

            return $posts;
        } catch (\Exception $e) {
            return response('error on the server side', 500);
        }
    }

    /**
     * @SWG\Get(
     *     path="/api/blog/posts/search",
     *     tags={"Blog"},
     *     summary="Blog Posts",
     *     description="Search Blog Posts",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="text",
     *         in="path",
     *         description="Text",
     *         required=false,
     *         type="string",
     *         default="some text",
     *     ),
     *
     *     @SWG\Parameter(
     *         name="category_id",
     *         in="path",
     *         description="Post category id",
     *         required=false,
     *         type="integer",
     *         default=1,
     *     ),
     *
     *     @SWG\Parameter(
     *         name="date_from",
     *         in="path",
     *         description="date from",
     *         required=false,
     *         type="string",
     *         default="2017-07-03 14:15:51",
     *     ),
     *
     *     @SWG\Parameter(
     *         name="date_to",
     *         in="path",
     *         description="date to",
     *         required=false,
     *         type="string",
     *         default="2017-07-03 14:15:51",
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *              @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/Post")),
     *         ),
     *     )
     * )
     *
     * @param Request $request
     * @param PostFilter $filter
     * @return mixed
     */
    public function search(Request $request, PostFilter $filter)
    {
        $this->validate($request, [
            'date_from' => 'date|required_with:date_to',
            'date_to' => 'date|required_with:date_from',
            'text' => 'string',
            'category_id' => 'numeric|exists:post_categories,id',
        ]);

        $posts = Post::where('status', 1)->filter($filter)->paginate(10);
        $posts->setCollection(new Collection(fractal($posts->getCollection(), new PostTransformer)));

        return $posts;
    }
}
