<?php


namespace App\Http\Controllers;


use App\Contracts\MailService;
use App\Http\Requests\ContactUsRequest;
use App\Notifications\BigMailer\ContactUsNotification;
use Illuminate\Http\Response;

class ContactUsController extends Controller
{
    /**
     * @param ContactUsRequest $request
     * @param MailService $mailService
     * @return Response
     */
    public function store(ContactUsRequest $request, MailService $mailService)
    {
        $recipients = config('site-settings.contact_us_email') ? explode(',', config('site-settings.contact_us_email')) : [];
        $mailService->sendMultipleTransactionalEmail(new ContactUsNotification(
            $recipients,
            [
                'email' => $request->input('email'),
                'full_name' => $request->input('full_name'),
                'topic' => $request->input('subject'),
                'message' => $request->input('message')
            ]
        ));

        return new Response(['message' => 'Thank you for submitting your request. A customer service representative will contact you within 24-48 hours. Please feel free to browse our great selection of promotional products.'], Response::HTTP_OK);
    }
}
