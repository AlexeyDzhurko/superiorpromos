<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Contracts\TaxExemptionServiceInterface;
use App\Http\Requests\UploadTaxExemptionCertificateRequest;

class TaxExemptionCertificateController extends Controller
{
    protected $taxExemptionService;

    public function __construct(TaxExemptionServiceInterface $taxExemptionService)
    {
        $this->taxExemptionService = $taxExemptionService;
    }

    public function uploadCertificate(UploadTaxExemptionCertificateRequest $request)
    {
        $certificate = $request->file('tax_exemption_certificate', null);
        $provideLater = $request->provide_later;

        $this->taxExemptionService->setUser(Auth::user());

        if ($provideLater) {
            $this->taxExemptionService->provideTaxExemptionCertificateLater();
        } else {
            $this->taxExemptionService->setCertificate($certificate);
            $this->taxExemptionService->uploadCertificate();
        }

        return response()->json(['message' => 'Tax exemption is updated']);
    }
}
