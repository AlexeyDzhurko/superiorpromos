<?php

namespace App\Http\Controllers;

use App\Models\PostCategory;

class PostCategoriesController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/blog/categories",
     *     tags={"Blog"},
     *     summary="Blog Categories",
     *     description="Blog Categories",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *              @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/PostCategory")),
     *         ),
     *     )
     * )
     *
     * Display a listing of the blog categories.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        return PostCategory::withCount('posts')->get();
    }
}
