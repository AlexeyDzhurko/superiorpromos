<?php

namespace App\Http\Controllers;

use App\Models\Testimonial;

class TestimonialController extends Controller
{
    /**
     *  @SWG\Get(
     *     path="/api/testimonial",
     *     tags={"Testimonials"},
     *     summary="List Testimonials",
     *     description="List Testimonials",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *              @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/Testimonial")),
     *         ),
     *     )
     * )
     * Display a listing of the Testimonials.
     *
     * @return \Illuminate\Http\Response
     */
    //todo: write transformer
    public function index()
    {
        $testimonials = Testimonial::whereActive(Testimonial::ACTIVE)->get();
        return response()->json($testimonials);
    }
}
