<?php

namespace App\Http\Controllers;

use App\Models\Wish;
use App\Transformers\WishTransformer;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class WishController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/wish-list",
     *     tags={"Wish List"},
     *     summary="Display wish list",
     *     description="Display wish list for authenticated user",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description = "Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/Wish")),
     *     )
     * )
     *
     * Returns wish list for authenticated user
     *
     */
    public function index()
    {
        $wish_list = Wish::with(['product'])->whereUserId(Auth::user()->id)
            ->orderBy('created_at', 'desc')->get();
        return fractal()->collection($wish_list, new WishTransformer());
    }

    /**
     * @SWG\Post(
     *     path="/api/wish-list",
     *     tags={"Wish List"},
     *     summary="Store wish",
     *     description="Store wish for authenticated user",
     *     produces={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="product_id",
     *         in="formData",
     *         description="Product id",
     *         required=true,
     *         type="integer",
     *         default=1,
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/Wish"))
     *     ),
     * )
     *
     * Store wish for authenticated user
     *
     * @return \Spatie\Fractal\Fractal
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|exists:products,id',
        ]);

        $wish = new Wish($request->all());
        $wish->user_id = Auth::user()->id;
        $wish->save();

        return fractal()->item($wish, new WishTransformer());
    }

    /**
     * @SWG\Delete(
     *     path="/api/wish-list/{id}",
     *     tags={"Wish List"},
     *     summary="Delete wish",
     *     description="Delete wish for authenticated user",
     *     produces={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Wish id",
     *         required=true,
     *         type="integer",
     *         default=1,
     *     ),
     *
     *     @SWG\Response(
     *         response=204,
     *         description="OK",
     *     ),
     * )
     *
     * Delete wish for authenticated user
     *
     * @param Wish $wish
     * @return Response
     */
    public function destroy(Wish $wish)
    {
        $this->authorize('delete', $wish);

        $wish->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * @SWG\Post(
     *     path="/api/wish-list/clear",
     *     tags={"Wish List"},
     *     summary="Delete wish list",
     *     description="Delete wish list for authenticated user",
     *     produces={"application/json"},
     *
     *     @SWG\Response(
     *         response=204,
     *         description="OK",
     *     ),
     * )
     *
     * Delete wish for authenticated user
     *
     * @return Response
     */

    public function clearList()
    {
        if (Auth::check()) Wish::whereUserId(Auth::user()->id)->delete();

        return response('', Response::HTTP_NO_CONTENT);
    }
}
