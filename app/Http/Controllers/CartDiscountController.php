<?php


namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Discount;
use App\Payment\CartItemCost;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Discounts\DiscountCreator;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\DiscountException;
use Illuminate\Database\QueryException;
use App\Transformers\DiscountCalcTransformer;

class CartDiscountController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/cart-discount",
     *     tags={"Cart"},
     *     summary="Get cart discount code",
     *     description="Get cart discount code",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description = "Success"
     *     )
     * )
     *
     * Get cart discount code
     *
     * @param Request $request
     * @param DiscountCreator $discountCreator
     * @return null
     * @throws DiscountException
     */
    public function index(Request $request, DiscountCreator $discountCreator)
    {
        /**@var $cart Cart**/
        $cart = Auth::check() ? Cart::current(Auth::user())->first() : Cart::whereSession($request->header('guest'))->first();

        if(is_null($cart)) {
            abort(Response::HTTP_BAD_REQUEST, 'Cart is empty');
        }

        if(is_null($cart->discount)) {
            abort(Response::HTTP_BAD_REQUEST, "");
        }

        $discountCalculations = $discountCreator->createCalcFromJson($cart->discount->discount_calc, $cart);

        return fractal()->collection($discountCalculations, new DiscountCalcTransformer());
    }

    /**
     * @SWG\Post(
     *     path="/api/cart-discount",
     *     tags={"Cart"},
     *     summary="Check discount code validity",
     *     description="Check discount code validity",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="discount_code",
     *         in="formData",
     *         description="Discount Code",
     *         required=true,
     *         type="string",
     *         default="spring10off",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description = "Success"
     *     )
     * )
     *
     * Check discount code validity for current cart
     *
     * @param Request $request
     * @param DiscountCreator $discountCreator
     * @return \Spatie\Fractal\Fractal
     * @throws \ReflectionException
     */
    public function store(Request $request, DiscountCreator $discountCreator)
    {
        $this->validate($request, [
            'discount_code' => 'required'
        ]);

        try{
            /** @var Discount $discount */
            $discount = Discount::byCode($request->get('discount_code'))->first();

            if (!$discount) {
                throw new DiscountException('Invalid discount code');
            }

            /**@var $cart Cart**/
            $cart = Auth::check() ? Cart::current(Auth::user())->first() : Cart::whereSession($request->header('guest'))->first();

            if(is_null($cart)) {
                throw new DiscountException('Cart is empty');
            }

            $cart->discount()->associate($discount);
            $cart->save();

            $discountCondition = $discountCreator->createFromJson($discount->discount_condition, $cart);

            if ($discountCondition->isAccepted()) {
                $discountCalculations = $discountCreator->createCalcFromJson($discount->discount_calc, $cart);
                return fractal()->collection($discountCalculations, new DiscountCalcTransformer());
            }
            $cart->discount()->dissociate();
            $cart->save();
            throw new DiscountException('Invalid discount code');

        }catch (DiscountException $exception) {
            abort(Response::HTTP_BAD_REQUEST, $exception->getMessage());
        }catch (QueryException $exception) {
            abort(Response::HTTP_BAD_REQUEST, 'Please try another discount code');
        }
    }


    /**
     * @SWG\Delete(
     *     path="/api/cart-discount",
     *     tags={"Cart"},
     *     summary="Remove cart discount code",
     *     description="Remove cart discount code",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description = "Success"
     *     )
     * )
     *
     * Remove cart discount code
     *
     * @param Request $request
     * @return null
     */
    public function destroy(Request $request)
    {
        /**@var $cart Cart**/
        $cart = Auth::check() ? Cart::current(Auth::user())->first() : Cart::whereSession($request->header('guest'))->first();

        if(is_null($cart)) {
            abort(Response::HTTP_BAD_REQUEST, 'Cart is empty');
        }

        if(is_null($cart->discount)) {
            abort(Response::HTTP_BAD_REQUEST, "This Cart doesn't have discount");
        }

        $cart->discount()->dissociate();
        $cart->save();

        $totalCost = 0;
        foreach ($cart->cartItems as $cartItem) { /** @var CartItem */
            $totalCost += (new CartItemCost($cartItem))->getTotal();
        }

        return \response(['total' => $totalCost], 200);
    }

}
