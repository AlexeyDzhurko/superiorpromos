<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddressRequest;
use App\Models\Address;
use App\Repository\AddressRepository;
use Illuminate\Http\Response;
use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Auth;

class AddressController extends Controller
{
    /**
     *  @SWG\Get(
     *     path="/api/address",
     *     tags={"Address"},
     *     summary="List Addresses",
     *     description="List of User Addresses",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDAwXC9hcGlcL2xvZ2luIiwiaWF0IjoxNDgxNjQxODQ0LCJleHAiOjE0ODE2NDU0NDQsIm5iZiI6MTQ4MTY0MTg0NCwianRpIjoiNjU3MmEzNDYxOWU1YjEyODIyNjdlYWQ4ODQ2NDI5NjIifQ.mpx7LhtwLtrb0ho7r5U5DVg0JGMlwmHA4-moOKhIMDw",
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *              @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/Address")),
     *         ),
     *     )
     * )
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $addresses = Address::where('user_id' , Auth::id())->get();
        return response()->json($addresses);
    }

    /**
     *  @SWG\Post(
     *     path="/api/address",
     *     tags={"Address"},
     *     summary="Create Address",
     *     description="Create User Address",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDAwXC9hcGlcL2xvZ2luIiwiaWF0IjoxNDgxNjQxODQ0LCJleHAiOjE0ODE2NDU0NDQsIm5iZiI6MTQ4MTY0MTg0NCwianRpIjoiNjU3MmEzNDYxOWU1YjEyODIyNjdlYWQ4ODQ2NDI5NjIifQ.mpx7LhtwLtrb0ho7r5U5DVg0JGMlwmHA4-moOKhIMDw",
     *     ),
     *     @SWG\Parameter(
     *         name="first_name",
     *         in="formData",
     *         description="Address first name",
     *         required=true,
     *         type="string",
     *         default="David",
     *     ),
     *     @SWG\Parameter(
     *         name="last_name",
     *         in="formData",
     *         description="Address last name",
     *         required=true,
     *         type="string",
     *         default="Smith",
     *     ),
     *     @SWG\Parameter(
     *         name="middle_name",
     *         in="formData",
     *         description="Address middle name",
     *         required=false,
     *         type="string",
     *         default="Aron",
     *     ),
     *     @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="Address title",
     *         required=false,
     *         type="string",
     *         default="Boxes",
     *     ),
     *     @SWG\Parameter(
     *         name="suffix",
     *         in="formData",
     *         description="Address suffix",
     *         required=false,
     *         type="string",
     *         default="123",
     *     ),
     *     @SWG\Parameter(
     *         name="company_name",
     *         in="formData",
     *         description="Address company name",
     *         required=false,
     *         type="string",
     *         default="SkyTech",
     *     ),
     *     @SWG\Parameter(
     *         name="is_default",
     *         in="formData",
     *         description="Is address default?",
     *         required=false,
     *         type="boolean",
     *         default="0",
     *     ),
     *     @SWG\Parameter(
     *         name="address_line_1",
     *         in="formData",
     *         description="Address address 1",
     *         required=true,
     *         type="string",
     *         default="62 Calle La Sombra, Camarillo, CA 93010, USA",
     *     ),
     *     @SWG\Parameter(
     *         name="address_line_2",
     *         in="formData",
     *         description="Address address 2",
     *         required=false,
     *         type="string",
     *         default="63 Calle La Sombra, Camarillo, CA 93010, USA",
     *     ),
     *     @SWG\Parameter(
     *         name="city",
     *         in="formData",
     *         description="Address city",
     *         required=true,
     *         type="string",
     *         default="Camarillo",
     *     ),
     *     @SWG\Parameter(
     *         name="state",
     *         in="formData",
     *         description="Address state",
     *         required=true,
     *         type="string",
     *         default="CA",
     *     ),
     *     @SWG\Parameter(
     *         name="zip",
     *         in="formData",
     *         description="Address zip",
     *         required=true,
     *         type="string",
     *         default="93010",
     *     ),
     *     @SWG\Parameter(
     *         name="country",
     *         in="formData",
     *         description="Address country",
     *         required=true,
     *         type="string",
     *         default="USA",
     *     ),
     *     @SWG\Parameter(
     *         name="province",
     *         in="formData",
     *         description="Address province",
     *         required=false,
     *         type="string",
     *         default="California",
     *     ),
     *     @SWG\Parameter(
     *         name="day_telephone",
     *         in="formData",
     *         description="Address day telephone",
     *         required=true,
     *         type="string",
     *         default="111-111-1111",
     *     ),
     *     @SWG\Parameter(
     *         name="ext",
     *         in="formData",
     *         description="Address extension",
     *         required=false,
     *         type="string",
     *         default="333",
     *     ),
     *     @SWG\Parameter(
     *         name="fax",
     *         in="formData",
     *         description="Address fax",
     *         required=false,
     *         type="string",
     *         default="333-333-3333",
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="formData",
     *         description="Address type",
     *         required=true,
     *         type="integer",
     *         default="1",
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/Address"))
     *         ),
     *     )
     * )
     *
     * Store a newly created Address in storage.
     *
     * @param  \App\Http\Requests\AddressRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddressRequest $request)
    {
        $address = new Address($request->all());
        $address->user_id = Auth::id();

        try {
            \DB::transaction(function () use ($address, $request) {
                if($request->is_default == Address::DEFAULT_ADDRESS) {
                    Address::where('type', $request->type)->update(['is_default' => 0]);
                } else {
                    $address->is_default = Address::COMMON_ADDRESS;
                }
                $address->save();
            });

            return response($address, 200);
        } catch (Exception $e) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, $e->getMessage());
        }
    }

    /**
     *  @SWG\Get(
     *     path="/api/address/{id}",
     *     tags={"Address"},
     *     summary="List Addresses",
     *     description="List of User Addresses",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDAwXC9hcGlcL2xvZ2luIiwiaWF0IjoxNDgxNjQxODQ0LCJleHAiOjE0ODE2NDU0NDQsIm5iZiI6MTQ4MTY0MTg0NCwianRpIjoiNjU3MmEzNDYxOWU1YjEyODIyNjdlYWQ4ODQ2NDI5NjIifQ.mpx7LhtwLtrb0ho7r5U5DVg0JGMlwmHA4-moOKhIMDw",
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="2",
     *     ),
     *     @SWG\Parameter(
     *         name="address",
     *         in="formData",
     *         description="Address id",
     *         required=true,
     *         type="integer",
     *         default="1",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *              @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/Address")),
     *         ),
     *     )
     * )
     *
     * Display Address resource.
     *
     * @param  \App\Models\Address $address
     * @return \Illuminate\Http\Response
     */
    public function show(Address $address)
    {
        return response()->json($address);
    }

    /**
     *  @SWG\Put(
     *     path="/api/address/{id}",
     *     tags={"Address"},
     *     summary="Update Address",
     *     description="Update User Address",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDAwXC9hcGlcL2xvZ2luIiwiaWF0IjoxNDgxNjQxODQ0LCJleHAiOjE0ODE2NDU0NDQsIm5iZiI6MTQ4MTY0MTg0NCwianRpIjoiNjU3MmEzNDYxOWU1YjEyODIyNjdlYWQ4ODQ2NDI5NjIifQ.mpx7LhtwLtrb0ho7r5U5DVg0JGMlwmHA4-moOKhIMDw",
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="2",
     *     ),
     *     @SWG\Parameter(
     *         name="first_name",
     *         in="formData",
     *         description="Address first name",
     *         required=true,
     *         type="string",
     *         default="David",
     *     ),
     *     @SWG\Parameter(
     *         name="last_name",
     *         in="formData",
     *         description="Address last name",
     *         required=true,
     *         type="string",
     *         default="Smith",
     *     ),
     *     @SWG\Parameter(
     *         name="middle_name",
     *         in="formData",
     *         description="Address middle name",
     *         required=false,
     *         type="string",
     *         default="Aron",
     *     ),
     *     @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="Address title",
     *         required=false,
     *         type="string",
     *         default="Boxes",
     *     ),
     *     @SWG\Parameter(
     *         name="suffix",
     *         in="formData",
     *         description="Address suffix",
     *         required=false,
     *         type="string",
     *         default="123",
     *     ),
     *     @SWG\Parameter(
     *         name="company_name",
     *         in="formData",
     *         description="Address company name",
     *         required=false,
     *         type="string",
     *         default="SkyTech",
     *     ),
     *     @SWG\Parameter(
     *         name="is_default",
     *         in="formData",
     *         description="Is address default?",
     *         required=true,
     *         type="boolean",
     *         default="0",
     *     ),
     *     @SWG\Parameter(
     *         name="address_line_1",
     *         in="formData",
     *         description="Address address 1",
     *         required=true,
     *         type="string",
     *         default="62 Calle La Sombra, Camarillo, CA 93010, USA",
     *     ),
     *     @SWG\Parameter(
     *         name="address_line_2",
     *         in="formData",
     *         description="Address address 2",
     *         required=false,
     *         type="string",
     *         default="63 Calle La Sombra, Camarillo, CA 93010, USA",
     *     ),
     *     @SWG\Parameter(
     *         name="city",
     *         in="formData",
     *         description="Address city",
     *         required=true,
     *         type="string",
     *         default="Camarillo",
     *     ),
     *     @SWG\Parameter(
     *         name="state",
     *         in="formData",
     *         description="Address state",
     *         required=true,
     *         type="string",
     *         default="CA",
     *     ),
     *     @SWG\Parameter(
     *         name="zip",
     *         in="formData",
     *         description="Address zip",
     *         required=true,
     *         type="string",
     *         default="93010",
     *     ),
     *     @SWG\Parameter(
     *         name="country",
     *         in="formData",
     *         description="Address country",
     *         required=true,
     *         type="string",
     *         default="USA",
     *     ),
     *     @SWG\Parameter(
     *         name="province",
     *         in="formData",
     *         description="Address province",
     *         required=false,
     *         type="string",
     *         default="California",
     *     ),
     *     @SWG\Parameter(
     *         name="day_telephone",
     *         in="formData",
     *         description="Address day telephone",
     *         required=true,
     *         type="string",
     *         default="111-111-1111",
     *     ),
     *     @SWG\Parameter(
     *         name="ext",
     *         in="formData",
     *         description="Address extension",
     *         required=false,
     *         type="string",
     *         default="333",
     *     ),
     *     @SWG\Parameter(
     *         name="fax",
     *         in="formData",
     *         description="Address fax",
     *         required=false,
     *         type="string",
     *         default="333-333-3333",
     *     ),
     *     @SWG\Parameter(
     *         name="type",
     *         in="formData",
     *         description="Address type",
     *         required=true,
     *         type="integer",
     *         default="1",
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/Address"))
     *         ),
     *     )
     * )
     *
     * Update Address model in storage.
     *
     * @param  \App\Http\Requests\AddressRequest $request
     * @param  \App\Models\Address $address
     * @return \Illuminate\Http\Response
     */
    public function update(AddressRequest $request, Address $address)
    {
        $this->authorize('update', $address);

        if($request->get('is_default') == 1 ){
            AddressRepository::setDefault($address);
        }
        
        $address->fill($request->all());

        try {
            $address->save();
            return response($address, 200);
        } catch (Exception $e) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, $e->getMessage());
        }
    }

    /**
     *  @SWG\Delete(
     *     path="/api/address/{id}",
     *     tags={"Address"},
     *     summary="Delete Address",
     *     description="Delete User Address",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDAwXC9hcGlcL2xvZ2luIiwiaWF0IjoxNDgxNjQxODQ0LCJleHAiOjE0ODE2NDU0NDQsIm5iZiI6MTQ4MTY0MTg0NCwianRpIjoiNjU3MmEzNDYxOWU1YjEyODIyNjdlYWQ4ODQ2NDI5NjIifQ.mpx7LhtwLtrb0ho7r5U5DVg0JGMlwmHA4-moOKhIMDw",
     *     ),
     *    @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="2",
     *     ),
     *
     *     @SWG\Response(
     *         response=204,
     *         description="OK",
     *         ),
     *     )
     * )
     *
     * Remove Address from storage.
     *
     * @param  \App\Models\Address $address
     * @return \Illuminate\Http\Response
     */
    public function destroy(Address $address)
    {
        $this->authorize('delete', $address);
        $address->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     *@SWG\Patch(
     *     path="/api/address/{id}/default",
     *     tags={"Address"},
     *     summary="Address as default",
     *     description="Set Address as default",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDAwXC9hcGlcL2xvZ2luIiwiaWF0IjoxNDgxNjQxODQ0LCJleHAiOjE0ODE2NDU0NDQsIm5iZiI6MTQ4MTY0MTg0NCwianRpIjoiNjU3MmEzNDYxOWU1YjEyODIyNjdlYWQ4ODQ2NDI5NjIifQ.mpx7LhtwLtrb0ho7r5U5DVg0JGMlwmHA4-moOKhIMDw",
     *     ),
     *    @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="2",
     *     ),
     *
     *     @SWG\Response(
     *         response=204,
     *         description="OK",
     *         ),
     *     )
     * )
     *
     * Set address as default
     *
     * @param \App\Models\Address $address
     * @return \Illuminate\Http\Response
     */

    public function makeDefault(Address $address)
    {
        $this->authorize('update', $address);
        AddressRepository::setDefault($address);
        return response('', Response::HTTP_NO_CONTENT);
    }
}
