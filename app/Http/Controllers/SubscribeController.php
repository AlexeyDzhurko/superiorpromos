<?php


namespace App\Http\Controllers;

use App\Contracts\MailService;
use App\Enums\DiscountTypeEnum;
use App\Http\Requests\SubscribeRequest;
use App\Models\Subscriber;
use App\Notifications\BigMailer\CouponCodeNotification;
use App\Repository\DiscountRepository;
use App\Repository\SubscriberRepository;
use App\Repository\UserRepository;

class SubscribeController extends Controller
{
    /**
     *
     * @SWG\Post(
     *     path="/api/subscribe/coupon",
     *     tags={"Subscribe"},
     *     summary="Subscribing",
     *     description="Subscribing for the getting coupon code",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          description="email",
     *          type="string"
     *      ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *         ),
     *     )
     * )
     *
     * @param SubscribeRequest $request
     * @param MailService $mailService
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Symfony\Component\HttpFoundation\Response
     */
    public function subscribeCoupon(SubscribeRequest $request, MailService $mailService)
    {
        $customer = $this->isCustomer($request->get('email'));

        if($customer->isEmpty()) {
            $couponCode = $this->generateCouponCode();
            $subscriber = SubscriberRepository::create($request->get('email'));
            $discount = $this->saveDiscount($couponCode);

            if($discount) {
                $this->setCouponCode($subscriber, $couponCode);
                $mailService->sendSingleTransactionalEmail(
                    new CouponCodeNotification([$subscriber->email], [
                        'coupon_code' => $subscriber->coupon
                    ])
                );

            if (!empty($contactId)) {
                $this->setBigMailerUuid($subscriber, $contactId);
            }

            return response(['message' => 'Thanks!'], 200);
            }
        }

        return response(['message' => 'This email already exist'], 400);
    }

    /**
     * @SWG\Post(
     *     path="/api/subscribe/newsletter",
     *     tags={"Subscribe"},
     *     summary="Subscribing",
     *     description="Subscribing for the getting newsletters",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          description="email",
     *          type="string"
     *      ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *         ),
     *     )
     * )
     *
     * @param SubscribeRequest $request
     * @param MailService $mailService
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Symfony\Component\HttpFoundation\Response
     */
    public function subscribeNewsletter(SubscribeRequest $request, MailService $mailService)
    {
        $customer = $this->isCustomer($request->get('email'));

        if($customer->isEmpty()) {
            $contactId = $mailService->createContact($request->get('email'));

            if (!empty($contactId)) {
                $subscriber = SubscriberRepository::create($request->get('email'));
                $this->setBigMailerUuid($subscriber, $contactId);

                return response(['message' => 'Thank you for signing up to Superior Promos Specials.'], 200);
            }

            return response(['message' => 'Something went wrong. Please, try again later.'], 200);
        }

        return response(['message' => 'This email already exist.'], 409);
    }


    /**
     * @param $code
     * @return \App\Models\Discount
     */
    private function saveDiscount($code)
    {
        $firstOrderDiscount = env('DISCOUNT_PERCENT_FOR_FIRST_ORDER', 10);
        $data = [
            'name' => $code,
            'type' => DiscountTypeEnum::DISPOSABLE,
            'discount_condition' => '{"class":"App\\\Discounts\\\CouponCodeDiscountCondition","parameters":{"discountCode":"'. $code .'"}}',
            'discount_calc' => '[{"class":"App\\\Discounts\\\TotalDiscountCost","parameters":{"percent":"'.$firstOrderDiscount.'"}}]',
            'code' => $code
       ];

        return DiscountRepository::create($data);
    }

    /**
     * @param Subscriber $subscriber
     * @param $code
     */
    private function setCouponCode(Subscriber $subscriber, $code)
    {
        SubscriberRepository::setCode($subscriber, $code);
    }

    /**
     * @param Subscriber $subscriber
     * @param $uuid
     */
    private function setBigMailerUuid(Subscriber $subscriber, $uuid)
    {
        SubscriberRepository::setBigMailerUuid($subscriber, $uuid);
    }

    /**
     * @param string $email
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    private function isCustomer(string $email)
    {
        return UserRepository::getCustomerByParameters(['email' => $email]);
    }

    /**
     * @return string
     */
    private function generateCouponCode()
    {
        return strtoupper('M217-' . date('m') . '-' . rand(1, 9999));
    }
}
