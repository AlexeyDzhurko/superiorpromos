<?php

namespace App\Http\Controllers;

use App\Contracts\MailService;
use App\Models\ArtProof;
use App\Transformers\ArtProofTransformer;
use App\Transformers\UnreadArtProofTransformer;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Storage;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ArtProofController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/art-proofs",
     *     tags={"Art Proofs"},
     *     summary="Display list of art proofs",
     *     description="Display list of art proofs for authenticated user",
     *     produces={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="statuses[]",
     *         in="query",
     *         description="needed status",
     *         required=false,
     *         type="array",
     *         items="string",
     *         default="['approved','declined']"
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description = "Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/ArtProof")),
     *     )
     * )
     *
     * Returns art proofs for authenticated user
     *
     * @param Request $request
     * @return $this|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     *
     */
    public function index(Request $request)
    {
        $where = null;
        $whereNull = "0";
        
        if (isset($request->statuses)) {
            foreach ($request->statuses as $status) {
                switch ($status) {
                    case "approved":
                        $where[] = 1;
                        break;
                    case "declined":
                        $where[] = 0;
                        break;
                    case "pending":
                        $whereNull = 'approved IS null';
                        break;
                }
            }
        }

        //DB::connection()->enableQueryLog();
        $artProofs = ArtProof::whereHas('orderItem.order', function ($q) {
            $q
                ->where('user_id', Auth::user()->id)
            ;
        })
            ->where(function($query) use ($where, $whereNull)  {
                if(isset($whereNull)) {
                    $query->whereRaw($whereNull);
                }
                if(isset($where)) {
                    foreach ($where as $status) {
                        $query->orWhere('approved', $status);
                    }
                }
            })
            ->get();

        //$queries = DB::getQueryLog();
        return fractal()->collection($artProofs, new ArtProofTransformer());
    }

    /**
     * @SWG\Get(
     *     path="/api/art-proofs/{id}/approve",
     *     tags={"Art Proofs"},
     *     summary="Approve art proof",
     *     description="Approve art proof for authenticated user",
     *     produces={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Art proof id",
     *         required=true,
     *         type="integer",
     *         default=1,
     *     ),
     *     @SWG\Parameter(
     *         name="note",
     *         in="formData",
     *         description="Note for art proof",
     *         required=true,
     *         type="string",
     *         default="my note about this art proof",
     *     ),
     *
     *     @SWG\Response(
     *         response=204,
     *         description="OK",
     *     ),
     * )
     *
     * Approve art proof for authenticated user
     *
     * @param ArtProof $artProof
     * @param Request $request
     * @param MailService $mailService
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Symfony\Component\HttpFoundation\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function approve(ArtProof $artProof, Request $request, MailService $mailService)
    {
        $this->authorize('update', $artProof);

        if(!is_null($artProof->approved)) {
            throw new HttpException(400, 'This art proof already approved or denied!');
        }

        $artProof->approved = 1;
        $artProof->customer_note = $request->input('note');
        $artProof->answered_at = Carbon::now();
        $artProof->save();

        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * @SWG\Get(
     *     path="/api/art-proofs/{id}/denied",
     *     tags={"Art Proofs"},
     *     summary="Denied art proof",
     *     description="Denied art proof for authenticated user",
     *     produces={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Art proof id",
     *         required=true,
     *         type="integer",
     *         default=1,
     *     ),
     *     @SWG\Parameter(
     *         name="note",
     *         in="formData",
     *         description="Note for art proof",
     *         required=true,
     *         type="string",
     *         default="my note about this art proof",
     *     ),
     *
     *     @SWG\Response(
     *         response=204,
     *         description="OK",
     *     ),
     * )
     *
     * Denied art proof for authenticated user
     *
     * @param ArtProof $artProof
     * @param Request $request
     * @param MailService $mailService
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Symfony\Component\HttpFoundation\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function denied(ArtProof $artProof, Request $request, MailService $mailService)
    {
        $this->authorize('update', $artProof);

        if(!is_null($artProof->approved)) {
            throw new HttpException(400, 'This art proof already approved or denied!');
        }

        $artProof->approved = 0;
        $artProof->customer_note = $request->input('note');
        $artProof->answered_at = Carbon::now();
        $artProof->save();

        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * @SWG\Get(
     *     path="/api/art-proofs/{id}/file",
     *     tags={"Art Proofs"},
     *     summary="Get art proof file",
     *     description="Get art proof file for authenticated user",
     *     produces={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Art proof id",
     *         required=true,
     *         type="integer",
     *         default=1,
     *     ),
     *
     *     @SWG\Response(
     *         response=204,
     *         description="OK",
     *     ),
     * )
     *
     * Get art proof file for authenticated user
     *
     * @param ArtProof $artProof
     * @return Response
     */
    public function file(ArtProof $artProof)
    {
        $this->authorize('view', $artProof);

        $prefix = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();

        return response()->file($prefix . ArtProof::ART_FILE_DIR . $artProof->path);
    }

    /**
     * @SWG\Get(
     *     path="/api/art-proofs/unread",
     *     tags={"Art Proofs"},
     *     summary="Display list of unread user's art proofs",
     *     description="Display list of unread user's art proofs for authenticated",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description = "Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/UnreadArtProof")),
     *     )
     * )
     *
     * Returns unread user's art proofs for authenticated
     *
     * @return $this|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getUnread()
    {
        try {
            $unreadArtProofs = ArtProof::where('customer_read', 0)->whereHas('orderItem.order', function ($q) {
                $q->where('user_id', auth()->user()->id);
            })->get();

            return fractal()->collection($unreadArtProofs,  new UnreadArtProofTransformer());
        } catch (\Exception $e) {
            abort(500, 'error on server side');
        }
    }

    /**
     * @SWG\Patch(
     *     path="/api/art-proofs/{id}/read",
     *     tags={"Art Proofs"},
     *     summary="Set art proof status as read",
     *     description="Set art proof status as read",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *    @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="45",
     *     ),
     *
     *     @SWG\Response(
     *         response=204,
     *         description="OK",
     *         ),
     *     )
     * )
     *
     * @param ArtProof $artProof
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function setCustomerReadFlagAsRead(ArtProof $artProof)
    {
        try {
            $this->authorize('update', $artProof);

            $artProof->customer_read = 1;
            $artProof->save();

            return response('', 204);
        } catch (\Exception $e) {
            abort(500, 'error on server side');
        }
    }
}
