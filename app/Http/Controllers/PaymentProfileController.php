<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePaymentProfileRequest;
use App\Http\Requests\UpdateExpirationDateRequest;
use App\Http\Requests\UpdatePaymentProfileRequest;
use App\PaymentProfile;
use App\Transformers\PaymentProfileTransformer;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Auth;

class PaymentProfileController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/user/payment_profile",
     *     tags={"Payment Profiles"},
     *     summary="Display list of payment profiles",
     *     description="Show list of payment profiles for authenticated user",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description = "Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/PaymentProfile")),
     *     )
     * )
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        return fractal()->collection($user->payment_profiles, new PaymentProfileTransformer());
    }

    /**
     * @SWG\Post(
     *     path="/api/user/payment_profile",
     *     tags={"Payment Profiles"},
     *     summary="Add Payment Profile",
     *     description="Add Payment Profile for authenticated user",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="card_holder",
     *         in="formData",
     *         description="Credit card holder",
     *         required=true,
     *         type="string",
     *         default="FirstName LastName",
     *     ),
     *     @SWG\Parameter(
     *         name="card_number",
     *         in="formData",
     *         description="Credit card number",
     *         required=true,
     *         type="string",
     *         default="4007000000027",
     *     ),
     *     @SWG\Parameter(
     *         name="expiration_month",
     *         in="formData",
     *         description="Credit card expiration month",
     *         required=true,
     *         type="integer",
     *         default="11",
     *     ),
     *     @SWG\Parameter(
     *         name="expiration_year",
     *         in="formData",
     *         description="Credit card expiration year",
     *         required=true,
     *         type="integer",
     *         default="2019",
     *     ),
     *     @SWG\Parameter(
     *         name="cvc",
     *         in="formData",
     *         description="Credit card verification code (CVV2)",
     *         required=true,
     *         type="integer",
     *         default="123",
     *     ),
     *     @SWG\Parameter(
     *         name="first_name",
     *         in="formData",
     *         description="First name for payment address",
     *         required=true,
     *         type="string",
     *         default="FirstName",
     *     ),
     *     @SWG\Parameter(
     *         name="last_name",
     *         in="formData",
     *         description="Last name for payment address",
     *         required=true,
     *         type="string",
     *         default="LastName",
     *     ),
     *     @SWG\Parameter(
     *         name="company",
     *         in="formData",
     *         description="Company",
     *         required=false,
     *         type="string",
     *         default="SuperiorPromos",
     *     ),
     *     @SWG\Parameter(
     *         name="address1",
     *         in="formData",
     *         description="Address line 1",
     *         required=true,
     *         type="string",
     *         default="address 1",
     *     ),
     *     @SWG\Parameter(
     *         name="address2",
     *         in="formData",
     *         description="Address line 2",
     *         required=true,
     *         type="string",
     *         default="address 2",
     *     ),
     *     @SWG\Parameter(
     *         name="city",
     *         in="formData",
     *         description="City for payment address",
     *         required=true,
     *         type="string",
     *         default="New York",
     *     ),
     *     @SWG\Parameter(
     *         name="state",
     *         in="formData",
     *         description="State for payment address",
     *         required=true,
     *         type="string",
     *         default="Arizona",
     *     ),
     *     @SWG\Parameter(
     *         name="zip",
     *         in="formData",
     *         description="Postal code for payment address",
     *         required=true,
     *         type="string",
     *         default="11211",
     *     ),
     *     @SWG\Parameter(
     *         name="country",
     *         in="formData",
     *         description="Country for payment address",
     *         required=true,
     *         type="string",
     *         default="US",
     *     ),
     *     @SWG\Parameter(
     *         name="phone",
     *         in="formData",
     *         description="Phone number for payment address",
     *         required=true,
     *         type="string",
     *         default="333-333-1212",
     *     ),
     *     @SWG\Parameter(
     *         name="phone_extension",
     *         in="formData",
     *         description="Extension for phone number",
     *         required=false,
     *         type="string",
     *         default="123",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description = "Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/PaymentProfile"))
     *     )
     * )
     *
     * Store a newly created resource in storage.
     *
     * @param StorePaymentProfileRequest $request
     * @return \Spatie\Fractal\Fractal
     */
    public function store(StorePaymentProfileRequest $request)
    {
        $user = Auth::user();

        $paymentProfile = new PaymentProfile($request->all());
        $paymentProfile->user()->associate($user);

        try {
            $paymentProfile->save();
            return fractal()->item($paymentProfile, new PaymentProfileTransformer());
        } catch (Exception $e) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, $e->getMessage());
        }
    }

    /**
     * @SWG\Get(
     *     path="/api/user/payment_profile/{id}",
     *     tags={"Payment Profiles"},
     *     summary="Display Payment Profile",
     *     description="Display specific Payment Profile",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payment profile id",
     *         required=true,
     *         type="integer",
     *         default=1802637466,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description = "OK",
     *         @SWG\Schema(ref="#/definitions/PaymentProfile"),
     *     )
     * )
     *
     * Display the specified resource.
     *
     * @param PaymentProfile $paymentProfile
     * @return \Spatie\Fractal\Fractal
     * @internal param int $id
     */
    public function show(PaymentProfile $paymentProfile)
    {
        return fractal()->item($paymentProfile, new PaymentProfileTransformer());
    }

    /**
     * @SWG\Put(
     *     path="/api/user/payment_profile/{id}",
     *     tags={"Payment Profiles"},
     *     summary="Update Payment Profile",
     *     description="Update specific Payment Profile",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payment profile id",
     *         required=true,
     *         type="integer",
     *         default=1802649115,
     *     ),
     *     @SWG\Parameter(
     *         name="card_holder",
     *         in="formData",
     *         description="Credit card holder",
     *         required=true,
     *         type="string",
     *         default="FirstName LastName",
     *     ),
     *     @SWG\Parameter(
     *         name="card_number",
     *         in="formData",
     *         description="Credit card number",
     *         required=true,
     *         type="string",
     *         default="4007000000027",
     *     ),
     *     @SWG\Parameter(
     *         name="expiration_month",
     *         in="formData",
     *         description="Credit card expiration month",
     *         required=true,
     *         type="integer",
     *         default="11",
     *     ),
     *     @SWG\Parameter(
     *         name="expiration_year",
     *         in="formData",
     *         description="Credit card expiration year",
     *         required=true,
     *         type="integer",
     *         default="2019",
     *     ),
     *     @SWG\Parameter(
     *         name="cvc",
     *         in="formData",
     *         description="Credit card verification code (CVV2)",
     *         required=true,
     *         type="integer",
     *         default="123",
     *     ),
     *     @SWG\Parameter(
     *         name="first_name",
     *         in="formData",
     *         description="First name for payment address",
     *         required=true,
     *         type="string",
     *         default="FirstName",
     *     ),
     *     @SWG\Parameter(
     *         name="last_name",
     *         in="formData",
     *         description="Last name for payment address",
     *         required=true,
     *         type="string",
     *         default="LastName",
     *     ),
     *     @SWG\Parameter(
     *         name="company",
     *         in="formData",
     *         description="Company",
     *         required=false,
     *         type="string",
     *         default="SuperiorPromos",
     *     ),
     *     @SWG\Parameter(
     *         name="address1",
     *         in="formData",
     *         description="Address line 1",
     *         required=true,
     *         type="string",
     *         default="address 1",
     *     ),
     *     @SWG\Parameter(
     *         name="address2",
     *         in="formData",
     *         description="Address line 2",
     *         required=true,
     *         type="string",
     *         default="address 2",
     *     ),
     *     @SWG\Parameter(
     *         name="city",
     *         in="formData",
     *         description="City for payment address",
     *         required=true,
     *         type="string",
     *         default="New York",
     *     ),
     *     @SWG\Parameter(
     *         name="state",
     *         in="formData",
     *         description="State for payment address",
     *         required=true,
     *         type="string",
     *         default="Arizona",
     *     ),
     *     @SWG\Parameter(
     *         name="zip",
     *         in="formData",
     *         description="Postal code for payment address",
     *         required=true,
     *         type="string",
     *         default="11211",
     *     ),
     *     @SWG\Parameter(
     *         name="country",
     *         in="formData",
     *         description="Country for payment address",
     *         required=true,
     *         type="string",
     *         default="US",
     *     ),
     *     @SWG\Parameter(
     *         name="phone",
     *         in="formData",
     *         description="Phone number for payment address",
     *         required=true,
     *         type="string",
     *         default="333-333-1212",
     *     ),
     *     @SWG\Parameter(
     *         name="phone_extension",
     *         in="formData",
     *         description="Extension for phone number",
     *         required=false,
     *         type="string",
     *         default="123",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description = "Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/PaymentProfile"))
     *     )
     * )
     *
     * Update the specified resource in storage.
     *
     * @param UpdatePaymentProfileRequest $request
     * @param PaymentProfile $paymentProfile
     * @return \Spatie\Fractal\Fractal
     * @internal param int $id
     */
    public function update(UpdatePaymentProfileRequest $request, PaymentProfile $paymentProfile)
    {
        $paymentProfile->fill($request->except(['card_number', 'card_type']));

        try {
            $paymentProfile->save();
            return fractal()->item($paymentProfile, new PaymentProfileTransformer());
        } catch (Exception $e) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, $e->getMessage());
        }
    }

    /**
     * @SWG\Delete(
     *     path="/api/user/payment_profile/{id}",
     *     tags={"Payment Profiles"},
     *     summary="Delete Payment Profile",
     *     description="Delete specific Payment Profile",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payment profile id",
     *         required=true,
     *         type="integer",
     *         default=1802637466,
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description = "No Content"
     *     )
     * )
     *
     * Remove the specified resource from storage.
     *
     * @param PaymentProfile $paymentProfile
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(PaymentProfile $paymentProfile)
    {
        try {
            $paymentProfile->expiration_month = ($paymentProfile->expiration_month < 10) ? '0' . $paymentProfile->expiration_month : $paymentProfile->expiration_month;
            $paymentProfile->delete();
            return response('', Response::HTTP_NO_CONTENT);
        } catch (Exception $e) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, $e->getMessage());
        }
    }

    /**
     * @SWG\Post(
     *     path="/api/user/payment_profile/{id}/expiration_date",
     *     tags={"Payment Profiles"},
     *     summary="Update Payment Profile expiration date",
     *     description="Update Payment Profile(Credit Card) expiration date",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Payment profile id",
     *         required=true,
     *         type="integer",
     *         default=1802637466,
     *     ),
     *     @SWG\Parameter(
     *         name="expiration_month",
     *         in="formData",
     *         description="Credit card expiration month",
     *         required=true,
     *         type="integer",
     *         default="11",
     *     ),
     *     @SWG\Parameter(
     *         name="expiration_year",
     *         in="formData",
     *         description="Credit card expiration year",
     *         required=true,
     *         type="integer",
     *         default="2019",
     *     ),
     *     @SWG\Parameter(
     *         name="cvc",
     *         in="formData",
     *         description="Credit card verification code (CVV2)",
     *         required=true,
     *         type="integer",
     *         default="123",
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description = "No Content"
     *     )
     * )
     *
     * Update Payment Profile expiration date
     *
     * @param UpdateExpirationDateRequest $request
     * @param PaymentProfile $paymentProfile
     * @return Response
     */
    public function updateExpirationDate(UpdateExpirationDateRequest $request, PaymentProfile $paymentProfile)
    {
        $paymentProfile->fill($request->all());
        try {
            $paymentProfile->save();
        } catch (Exception $e) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, $e->getMessage());
        }
        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     *@SWG\Patch(
     *     path="/api/user/payment_profile/{id}/default",
     *     tags={"Payment Profiles"},
     *     summary="Payment Profile as default",
     *     description="Set Payment Profile as default",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDAwXC9hcGlcL2xvZ2luIiwiaWF0IjoxNDgxNjQxODQ0LCJleHAiOjE0ODE2NDU0NDQsIm5iZiI6MTQ4MTY0MTg0NCwianRpIjoiNjU3MmEzNDYxOWU1YjEyODIyNjdlYWQ4ODQ2NDI5NjIifQ.mpx7LhtwLtrb0ho7r5U5DVg0JGMlwmHA4-moOKhIMDw",
     *     ),
     *    @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="2",
     *     ),
     *
     *     @SWG\Response(
     *         response=204,
     *         description="OK",
     *         ),
     *     )
     * )
     *
     * Set Payment Profile as default
     *
     * @param \App\PaymentProfile $paymentProfile
     * @return \Illuminate\Http\Response
     */
    public function makeDefault(PaymentProfile $paymentProfile)
    {
        $this->authorize('update', $paymentProfile);

        PaymentProfile::where('user_id', Auth::id())
            ->update(['is_default' => 0]);

        $paymentProfile->is_default = 1;
        $paymentProfile->expiration_month = ($paymentProfile->expiration_month < 10) ? '0' . $paymentProfile->expiration_month : $paymentProfile->expiration_month;

        try {
            $paymentProfile->save();
            return response($paymentProfile, 200);
        } catch (Exception $e) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, $e->getMessage());
        }
    }
}
