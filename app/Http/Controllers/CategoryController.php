<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Transformers\CategoryTransformer;
use App\Transformers\ProductBreadcrumbsTransformer;
use App\Transformers\ProductTransformer;
use Illuminate\Support\Collection;
use Intervention\Image\Exception\NotFoundException;

class CategoryController extends Controller
{

    /**
     *  @SWG\Get(
     *     path="/api/categories",
     *     tags={"Categories"},
     *     summary="List Categories",
     *     description="List Categories",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *              @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/Category")),
     *         ),
     *     )
     * )
     * Display a listing of the Categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /** @var Category $categories */
        $categories = CategoryRepository::getAllActiveCategories();

        return fractal()->collection($categories, new CategoryTransformer())->toArray();
    }


    /**
     * @SWG\Get(
     *     path="/api/categories/{category}/product",
     *     tags={"Categories"},
     *     summary="List Category Products",
     *     description="List Category Products",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *
     *     @SWG\Parameter(
     *          name="category",
     *          in="path",
     *          description="Slug of category",
     *          required=true,
     *          type="string"
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *         @SWG\Schema(
     *              @SWG\Property(property="total", type="integer", example="19575"),
     *              @SWG\Property(property="per_page", type="integer", example="10"),
     *              @SWG\Property(property="current_page", type="integer", example="1"),
     *              @SWG\Property(property="last_page", type="integer", example="1958"),
     *              @SWG\Property(property="data", type="array", items=@SWG\Schema(ref="#/definitions/Product")),
     *         ),
     *     )
     * )
     * Display a listing category products.
     *
     * @param string $path
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function categoryProduct(string $path)
    {
        try {
            $segments = explode('/', $path);
            $categorySlug = end($segments);
            $category = CategoryRepository::getCategoryBySlug($categorySlug);
            if (is_null($category)) {
                throw new NotFoundException('', 404);
            }

            $categoriesIds = $category->getDescendantsAndSelf()->pluck('id')->toArray();

            $parameters = [
                'categories' => $categoriesIds
            ];
            $productsData = ProductRepository::getProductsByParameters($parameters);
            $productsData->setCollection(new Collection(fractal($productsData->getCollection(), new ProductTransformer)));
            $products = $productsData->setCollection(new Collection(fractal($productsData->getCollection(), new ProductBreadcrumbsTransformer($parameters))));

            return $products;
        } catch (\Exception $e) {
            return response($e->getMessage(), $e->getCode());
        }
    }

        /**
     * @SWG\Get(
     *     path="/api/categories/popular",
     *     tags={"Categories"},
     *     summary="List Popular Categories",
     *     description="List Popular Categories",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     * 
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *         @SWG\Schema(
     *              @SWG\Property(property="total", type="integer", example="19575"),
     *              @SWG\Property(property="per_page", type="integer", example="10"),
     *              @SWG\Property(property="current_page", type="integer", example="1"),
     *              @SWG\Property(property="last_page", type="integer", example="1958"),
     *              @SWG\Property(property="data", type="array", items=@SWG\Schema(ref="#/definitions/Category")),
     *         ),
     *     )
     * )
     * Display a listing popular categories.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getPopularCategories()
    {
        /** @var Category $categories */
        $categories = CategoryRepository::getCategoriesByParameters([
            'active' => 1,
            'is_popular' => 1
        ]);

        return fractal()->collection($categories, new CategoryTransformer())->toArray();
    }

    /**
     * @SWG\Get(
     *     path="/api/categories/{category}/sub-categories",
     *     tags={"Categories"},
     *     summary="List Sub-Categories",
     *     description="List Sub-Categories",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Parameter(
     *          name="category",
     *          in="path",
     *          description="ID of category",
     *          required=true,
     *          type="integer"
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *          @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/Category")),
     *     )
     * )
     * Display a listing subcategories.
     * @param Category $category
     * @return array
     */
    public function getSubCategories(Category $category) 
    {
        return fractal()->collection($category->sub_categories, new CategoryTransformer())->toArray();
    }   
}
