<?php


namespace App\Http\Controllers;


use App\Models\CaseStudy;

class CaseStudyController extends Controller
{
    /**
     *  @SWG\Get(
     *     path="/api/case-study",
     *     tags={"Case Study"},
     *     summary="List of Case Studies",
     *     description="List of active Case Studies",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *              @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/CaseStudy")),
     *         ),
     *     )
     * )
     * Display a listing of active Case Studies.
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function index()
    {
        return CaseStudy::sorted()->where('active', true)->get();
    }
}
