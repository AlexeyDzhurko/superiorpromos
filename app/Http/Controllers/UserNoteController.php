<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserNoteRequest;
use App\Models\UserNote;
use App\Transformers\UnreadUserNoteTransformer;
use App\Transformers\UserNoteTransformer;
use Carbon\Carbon;

class UserNoteController extends Controller
{
    /**
     * @SWG\Put(
     *     path="/api/order/user-note/{userNoteId}",
     *     tags={"Orders"},
     *     summary="Answer note",
     *     description="answer to the note for order item",
     *     produces={"application/json"},
     *
     *     @SWG\Parameter(
     *         name="userNoteId",
     *         in="path",
     *         description="User Note Id",
     *         required=true,
     *         type="integer",
     *         default=1,
     *     ),
     *     @SWG\Parameter(
     *         name="customer_note",
     *         in="formData",
     *         description="Note of customer",
     *         required=false,
     *         type="string",
     *         default="Thank you.  We are very excited!",
     *     ),
     *     @SWG\Parameter(
     *         name="approved",
     *         in="formData",
     *         description="Product id",
     *         required=true,
     *         type="boolean",
     *         default=1,
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/UserNote"))
     *     ),
     * )
     *
     * Update the specified resource in storage.
     *
     * @param  UserNoteRequest $request
     * @param  UserNote  $userNote
     * @return \Spatie\Fractal\Fractal
     */
    public function update(UserNoteRequest $request, UserNote $userNote)
    {
        try {
            $this->authorize('update', $userNote);

            $userNote->customer_note = $request->input('customer_note');
            $userNote->approved = $request->input('approved') === 'true' ? 1 : 0;
            $userNote->customer_read = 1;
            $userNote->answered_at = Carbon::now();
            $userNote->save();

            return fractal()->item($userNote, new UserNoteTransformer());
        } catch (\Exception $e) {
            return response('error on server side', 500);
        }
    }

    /**
     * @SWG\Get(
     *     path="/api/order/user-note/unread",
     *     tags={"User Notes"},
     *     summary="Display list of unread user's notes",
     *     description="Display list of unread user's notes for authenticated",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description = "Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(ref="#/definitions/UnreadUserNote")),
     *     )
     * )
     *
     * Returns unread user's notes for authenticated
     *
     * @return $this|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getUnread()
    {
        try {
            $unreadUserNotes = UserNote::where('customer_read', 0)->whereHas('orderItem.order', function ($q) {
                $q->where('user_id', auth()->user()->id);
            })->get();

            return fractal()->collection($unreadUserNotes,  new UnreadUserNoteTransformer());
        } catch (\Exception $e) {
            abort(500, 'error on server side');
        }
    }

    /**
     * @SWG\Patch(
     *     path="/api/order/user-note/{id}/read",
     *     tags={"User Notes"},
     *     summary="Set note status as read",
     *     description="Set note status as read",
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *
     *    @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="string",
     *         default="23",
     *     ),
     *
     *     @SWG\Response(
     *         response=204,
     *         description="OK",
     *         ),
     *     )
     * )
     *
     * @param UserNote $userNote
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function setCustomerReadFlagAsRead(UserNote $userNote)
    {
        try {
            $this->authorize('update', $userNote);

            $userNote->customer_read = 1;
            $userNote->save();

            return response('', 204);
        } catch (\Exception $e) {
            abort(500, 'error on server side');
        }
    }
}
