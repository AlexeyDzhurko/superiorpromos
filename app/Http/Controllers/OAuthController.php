<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Socialite;
use JWTAuth;

class OAuthController extends Controller
{
    public $providers = [
        'facebook'
    ];

    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Response
     */
    /*public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();
    }*/

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @SWG\Get(
     *     path="/api/oauth/{provider}/login",
     *     tags={"OAuth"},
     *     summary="Oauth authorization",
     *     description="Oauth authorization through socials",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="code",
     *         in="query",
     *         description="Authorization Code",
     *         required=true,
     *         type="string",
     *         default="Jd8dfjdfjJSskded9isdskdjdjdd8reudjiddiIDidids9SJDFCNCV8323034r5r3JKDJ",
     *     ),
     *     @SWG\Parameter(
     *         name="provider",
     *         in="path",
     *         description="Social Provider",
     *         required=true,
     *         type="string",
     *         default="facebook",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description = "Success",
     *         @SWG\Schema(type="array", items=@SWG\Schema(
     *             @SWG\Property(property="token", type="string", example="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"),
     *         )),
     *     )
     * )
     *
     * @param $provider
     * @param Request $request
     * @return Response
     */
    public function loginByToken($provider, Request $request)
    {
        try {
            if(!in_array($provider, $this->providers)) {
                return response('This provider not supported', 500);
            }

            $this->validate($request, [
               'code' => 'required'
            ]);

            try {
                // this user we get back is not our user model, but a special user object that has all the information we need
                $providerUser = Socialite::driver($provider)->stateless()->user();
            } catch (\Exception $e) {
                return response('Wrong attempt authorization', 403);
            }

            if(is_null($providerUser->getEmail())) {
                return response('You didn\'t authenticated because you haven\'t email in you social account!', 401);
            }

            // for example we might do something like... Check if a user exists with the email and if so, log them in.
            $user = User::query()->firstOrNew(['email' => $providerUser->getEmail()]);

            // maybe we can set all the values on our user model if it is new... right now we only have name
            // but you could set other things like avatar or gender
            if (!$user->exists) {
                $user->name = $providerUser->getName();
                $user->email = $providerUser->getEmail();
                $user->save();
            }

            /**
             * At this point we done.  You can use whatever you are using for authentication here...
             * for example you might do something like this if you were using JWT
             */
            $token = JWTAuth::fromUser($user);
            return response()->json(['token' => $token]);
        } catch (\Exception $e) {
            return response('Error on the server side', 500);
        }
    }
}
