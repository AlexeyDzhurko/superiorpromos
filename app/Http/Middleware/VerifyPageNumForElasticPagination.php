<?php

namespace App\Http\Middleware;

use Closure;

class VerifyPageNumForElasticPagination
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->page > 200) {
            return response(['Please specify you search query for continue pagination'], 422);
        } else {
            return $next($request);
        }
    }
}
