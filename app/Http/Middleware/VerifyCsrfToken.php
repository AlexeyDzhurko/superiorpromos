<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     * @todo need removed before move to live server
     * @var array
     */
    protected $except = [
        'product/*',
        'api/*',
        'admin/api/*',
    ];
}
