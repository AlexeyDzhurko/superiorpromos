<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;

class PostFilter extends QueryFilter
{
    /**
     * @param int $category_id
     */
    public function categoryId(int $category_id)
    {
        $this->builder->where('post_category_id', $category_id);
    }

    /**
     * @param string $title
     */
    public function text(string $title)
    {
        $words = array_filter(explode(' ', $title));

        $this->builder->where(function (Builder $query) use ($words) {
            foreach ($words as $word) {
                $query->where('body', 'like', "%$word%")
                    ->where('title', 'like', "%$word%");
            }
        });
    }

    /**
     * @param string $dateFrom
     * @param string $dateTo
     */
    public function dateFrom()
    {
        $this->builder->where('created_at','>=', $this->request->date_from)
            ->where('created_at','<=', $this->request->date_to);
    }
}