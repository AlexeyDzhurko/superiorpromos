<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            // Account information
            'users.name' => 'required|max:255',
            'users.email' => 'required|email|min:6|unique:users',
            'users.password' => 'required|min:8|confirmed|password',
            'users.contact_telephone' => 'required|phone',
            'users.ext' => 'max:3',
            
            'users.shipping.first_name' => 'required|max:255',
            'users.shipping.last_name' => 'required|max:255',
            'users.shipping.middle_name' => 'max:255',
            'users.shipping.company_name' => 'max:255',
            'users.shipping.address_line_1' => 'required|max:255',
            'users.shipping.address_line_2' => 'max:255',
            'users.shipping.city' => 'required|max:255',
            'users.shipping.state' => 'required_if:users.shipping.country,US|max:255',
            'users.shipping.country' => 'required|max:255',
            'users.shipping.zip' => 'required|zip',
            'users.shipping.province' => 'max:255',
            'users.shipping.day_telephone' => 'required|phone',
            'users.shipping.ext' => 'max:3',
            'users.shipping.fax' => 'nullable|string',    
            
            'users.billing.first_name' => 'required_if:users.billing.address,new_address|max:255',
            'users.billing.last_name' => 'required_if:users.billing.address,new_address|max:255',
            'users.billing.middle_name' => 'max:255',
            'users.billing.company_name' => 'max:255',
            'users.billing.address_line_1' => 'required_if:users.billing.address,new_address|max:255',
            'users.billing.address_line_2' => 'max:255',
            'users.billing.city' => 'required_if:users.billing.address,new_address|max:255',
            'users.billing.state' => 'required_if:users.billing.address,new_address|required_if:users.billing.country,US|max:255',
            'users.billing.country' => 'required_if:users.billing.address,new_address|max:255',
            'users.billing.zip' => 'required_if:users.billing.address,new_address|zip',
            'users.billing.province' => 'max:255',
            'users.billing.day_telephone' => 'required_if:users.billing.address,new_address|phone',
            'users.billing.ext' => 'max:3',
            'users.billing.fax' => 'nullable|string',
        ];
    }
}
