<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateExpirationDateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'expiration_month' => 'required|numeric|digits:2|expiration_month_with_year:expiration_year',
            'expiration_year' => 'required|numeric|digits:4',
            'cvc' => 'required|numeric|digits_between:3,4',
        ];
    }
}
