<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Response;

class ShippingCostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'zip_code' => 'required|regex:/^[0-9]{5}$/',
            'product_id' => 'required|exists:products,id',
            'quantity' => 'required|integer|min_quantity:product_id',
        ];
    }
}
