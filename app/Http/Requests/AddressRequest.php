<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'middle_name' => 'max:255',
            'is_default' => 'boolean',
            'title' => 'max:255',
            'suffix' => 'max:255',
            'company_name' => 'max:255',
            'address_line_1' => 'required|max:255',
            'address_line_2' => 'nullable|max:255',
            'city' => 'required|max:255',
            'state' => 'required_if:country,US|max:255',
            'zip' => 'required|zip',
            'country' => 'required|max:255',
            'province' => 'max:255',
            'day_telephone' => 'nullable|string',
            'ext' => 'max:3',
            'fax' => 'nullable|string',
            'is_po_box' => 'boolean',
            'type' => 'required|in:1,2',
        ];
    }
}
