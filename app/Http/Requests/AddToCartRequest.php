<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddToCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required|exists:products,id|product_options_set:product_options|product_colors_set:product_colors|product_imprints_set:product_imprints',
            'product_options' => 'json|nullable',
            'product_colors' => 'json|nullable',
            'product_imprints' => 'json|nullable',
            'product_sizes' => 'json|nullable',
            'quantity' => 'required|integer|min_quantity:product_id',
            'later_size_breakdown' => 'boolean',
            'imprint_comment' => 'string',
            'tax_exemption' => 'boolean',
            'art_files.*' => 'mimes:jpg,jpeg,png,pdf,psd,cals,gif,eps,ai,tiff',

            'shipping_method' => 'required|in:own_account,us_shipping,outside_us,custom',

            'own_shipping_type' => 'required_if:shipping_method,own_account',
            'own_account_number' => 'required_if:shipping_method,own_account',
            'own_shipping_system' => 'required_if:shipping_method,own_account',

            'estimation_zip' => 'required_if:shipping_method,us_shipping|zip',
            'estimation_shipping_code' => 'required_if:shipping_method,us_shipping',

            'received_date' => 'date|nullable',
        ];
    }
}
