<?php


namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class QuickQuoteEmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user' => 'required|array',
            'user.emails' => 'required|array',
            'user.emails.*' => 'required|email',
            'user.name' => 'required|string',
            'product' => 'required|array',
            'prices' => 'array|nullable',
            'colors' => 'array|nullable',
            'imprints' => 'array|nullable',
            'options' => 'array|nullable',
            'shipping' => 'array|nullable',
            'prices_colors' => 'array|nullable',
            'prices_options' => 'array|nullable',
            'prices_imprints' => 'array|nullable',
            'prices_setup' => 'array|nullable',
            'final_unit_price' => 'required|string',
            'subtotal' => 'required|string',
        ];
    }
}
