<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePaymentProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'card_holder'      => 'required|string|max:255',
            'card_number'      => 'sometimes|required|credit_card',
            'expiration_month' => 'required|numeric|digits:2',
            'expiration_year'  => 'required|numeric|digits:4',
            'cvc'              => 'required|numeric|digits_between:3,4',
            'first_name'       => 'required|string|max:255',
            'last_name'        => 'required|string|max:255',
            'company'          => 'string|max:255',
            'address1'         => 'required|string|max:255',
            'address2'         => 'nullable|string|max:255',
            'city'             => 'required|string|max:255',
            'state'            => 'required_if:country,US|string|max:255',
            'zip'              => 'required|zip',
            'country'          => 'string|max:255',
            'phone'            => 'required|phone',
            'phone_extension'  => 'string|max:3',
            'card_type'        => 'string|max:255',
        ];
    }
}
