<?php


namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class QuickQuoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required',
            'product_options' => 'array|nullable',
            'product_colors' => 'array|nullable',
            'product_imprints' => 'array|nullable',
            'product_sizes' => 'array|nullable',
            'product_shipping' => 'required|array',
            'quantity' => 'required|integer'
        ];
    }
}
