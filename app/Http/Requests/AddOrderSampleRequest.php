<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddOrderSampleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'products.*.product_id' => 'required|exists:products,id',
            'products.*.product_id.selected_colors.*.color_group_id' => 'required|exists:product_color_groups,id',
            'products.*.product_id.selected_colors.*.color_id' => 'required|exists:product_colors,id',
            
            'additionalInfo.attraction' => 'required|array|nullable',
            'additionalInfo.event_date' => 'required|string',
            'additionalInfo.frequency' => 'required|string',
            'additionalInfo.quantity' => 'required|string',
            'additionalInfo.sample' => 'required|string',
            'additionalInfo.sampleComment' => 'nullable|string',
            'additionalInfo.upcomingEvent' => 'required|string',

            'shipping_method' => 'required|in:own_account,us_shipping',

            'shipping_data.own_shipping_type' => 'required_if:shipping_method,own_account',
            'shipping_data.own_account_number' => 'required_if:shipping_method,own_account',
            'shipping_data.own_shipping_system' => 'required_if:shipping_method,own_account',
        ];
    }
}
