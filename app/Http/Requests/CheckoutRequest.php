<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shipping_address_id' => 'required|exists:addresses,id',
            'billing_address_id' => 'exists:addresses,id',
//            'payment_profile_id' => 'required|exists:payment_profiles,id',
            'payment_profile.type' => 'required|in:card,check,apply',
            'payment_profile.id' => 'required_if:payment_profile.type,card|exists:payment_profiles,id|nullable',
            'payment_profile.cvv' => 'required_if:payment_profile.type,card|digits_between:3,4|nullable',

            'ship_to_multiple_locations.type' => 'required|in:multiple,single',
            'ship_to_multiple_locations.details' => 'required_if:ship_to_multiple_locations.type,multiple|string',
            'discount_code' => 'string|nullable',
        ];
    }
}
