<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email.email' => 'sometimes|required|email|confirmed|min:6|unique:users,email',
            'information.name' => 'sometimes|required|max:255',
            'information.contact_telephone' => 'sometimes|required|phone',
            'information.ext' => 'sometimes|required|max:3',
            'information.fax' => 'sometimes|required|phone',
            'password.old_password' => 'required_with:password.password',
            'password.password' => 'required_with:password.old_password|min:6|confirmed|password',
            'information.tax_exempt_certificate' => 'file|mimes:jpeg,bmp,png,pdf',
        ];
    }
}
