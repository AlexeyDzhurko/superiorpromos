var path = require('path'),
    webpack = require('webpack'),
    UglifyJSPlugin = require('uglifyjs-webpack-plugin'),
    CompressionPlugin = require("compression-webpack-plugin"),
    CleanObsoleteChunks = require('webpack-clean-obsolete-chunks'),
    autoprefixer = require('autoprefixer'),
    ExtractTextPlugin = require("extract-text-webpack-plugin")
    cleanOptions = {
        verbose: true
    };
module.exports = function (env) {
    return {
        devtool:env.NODE_ENV == 'development' ? '#eval' : '#source-map',
        entry: {
            vendors: './webpack.vendors.js',
            superior: './frontend-src/index.js'
        },
        resolve: {
            modules: [
                path.resolve('./frontend-src'),
                path.resolve('./node_modules')
            ]
        },
        plugins: [
            new CompressionPlugin({
                filename: '[path].gz[query]',
                algorithm: "gzip",
                test: /\.js$|\.html$/,
                threshold: 10240,
                minRatio: 0.8
            }),
            new webpack.LoaderOptionsPlugin({
                minimize: true,
                debug: false
            }),
            new UglifyJSPlugin(
                {
                    parallel: 4,
                    uglifyOptions: {
                        output: {
                            comments: false,
                            beautify: false
                        }
                    }
                }
            ),
            new webpack.LoaderOptionsPlugin({
                minimize: true,
                debug: false
            }),
            new CleanObsoleteChunks(cleanOptions),
            new webpack.optimize.OccurrenceOrderPlugin(),
            // uncomment, if you use ES6 syntax
            // {
            //     test: /\.js$/,
            //     use: {
            //         loader: 'babel-loader',
            //         options: {
            //             presets: ['babel-preset-env']
            //         }
            //     }
            // },
            new CleanObsoleteChunks(cleanOptions),
            new ExtractTextPlugin('[name].min.css')
        ],
        output: {
            filename: '[name].min.js',
            path: path.join(__dirname, 'public/dist')
        },
        module: {
            rules: [
                // uncomment, if you use ES6 syntax
                // {
                //     test: /\.js$/,
                //     exclude: /(bower_components)/,
                //     use: {
                //         loader: 'babel-loader',
                //         options: {
                //             presets: ['babel-preset-env']
                //         }
                //     }
                // },
                {
                    test: /\.html$/,
                    use: [
                        {
                            loader: 'html-loader'
                        }
                    ]
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    use: [
                        'url-loader?limit=300&name=dist/assets/img/[name].[ext]',
                        'img-loader'
                    ]
                },
                {
                    test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    loader: 'url-loader?name=assets/fonts/[name].[ext]'
                },
                {
                    test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    loader: 'file-loader?name=assets/fonts/[name].[ext]'
                },
                {
                    test: /\.(css|scss)$/,
                    use:[
                        {
                            loader: 'style-loader'
                        },
                        {
                            loader: 'css-loader'
                        },
                        {
                            loader:'sass-loader'
                        },
                        {
                            loader:'postcss-loader',
                            options: {
                                plugins: function () {
                                    return [
                                        autoprefixer
                                    ];
                                }
                            }
                        }
                    ]
                }
            ]
        }
    }
};