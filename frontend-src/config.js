import headerView from './modules/header/header.html';
import headerCtrl from './modules/header/controller.js';
import footerView from './modules/footer/footer.html';
import footerCtrl from './modules/footer/controller.js';
import sidebarView from './modules/sidebar/sidebar.view.html'
import sidebarCtrl from './modules/sidebar/sidebar.controller.js';
import welcomeTextView from './modules/welcome-text/welcome-text.html'
import welcomeTextCtrl from "./modules/welcome-text/controller";

import contentView from './modules/content/content.html';

config.$inject = ['$stateProvider', '$urlRouterProvider', '$urlMatcherFactoryProvider',
    '$uiViewScrollProvider', 'localStorageServiceProvider', 'uiMask.ConfigProvider',
    '$locationProvider', '$sceProvider'];
    
export default function config($stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider,
                               $uiViewScrollProvider, localStorageServiceProvider, uiMaskConfigProvider,
                               $locationProvider, $sceProvider) {
    uiMaskConfigProvider.maskDefinitions({ '9': /\d/, 'A': /[a-zA-Z]/, '*': /[0-9*]/ });
    uiMaskConfigProvider.allowInvalidValue(true);
    // GooglePlusProvider.init({
    //     clientId: 'YOUR_CLIENT_ID',
    //     apiKey: 'YOUR_API_KEY'
    // });
    // FacebookProvider.init('YOUR_APP_ID');
    //todo: here4
    $sceProvider.enabled(false);
    $locationProvider.hashPrefix('');
    // $locationProvider.html5Mode(true);
    localStorageServiceProvider
        .setPrefix('superior')
        .setStorageType('localStorage');

    $uiViewScrollProvider.useAnchorScroll();
    $urlMatcherFactoryProvider.type('UriPath', {
        encode: function(v){ return v || '';},
        decode: function(v){ return v || '';},
        is:     function(v){ return true;},
        raw:    true
    });
    $urlRouterProvider.otherwise('/');
    // Application routes
    $stateProvider
        .state('router', {
            views: {
                'header': {
                    template: headerView,
                    controller: headerCtrl
                },
                'sidebar': {
                    template: sidebarView,
                    controller: sidebarCtrl
                },
                'content': {
                    // template:'<div ui-view autoscroll="true" ng-class="{whitebg: change}"></div><http-indicator></http-indicator>',
                    template: contentView,
                    controller: footerCtrl
                },
                'welcome' : {
                    template: welcomeTextView,
                    controller: welcomeTextCtrl
                },
                'footer': {
                    template: footerView,
                    controller: footerCtrl
                }
            },
            ncyBreadcrumb: {
                label: 'Home'
            }
        });

    $locationProvider.html5Mode({
       enabled: true,
       requireBase: false
    });
}
