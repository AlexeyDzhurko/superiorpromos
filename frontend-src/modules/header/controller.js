import "./header.scss";
headerCtrl.$inject = [
  "$search",
  "$stateParams",
  "appUserService",
  "cartService",
  "$scope",
  "apiService",
  "LoginRegistrationService",
  "$http",
  "$rootScope",
  "$state",
  "FlashMessagesService",
  "paymentProfileService",
  "addressBookService",
  "$rollBack",
  "customizeOrderService",
  "$document",
  "$timeout",
  "$categoriesData",
  "$home",
  '$colorsData',
  '$categoriesData'
];
export default function headerCtrl(
  $search,
  $stateParams,
  $appUser,
  $cartService,
  $scope,
  apiService,
  loginRegistrationService,
  $http,
  $rootScope,
  $state,
  $flashMessage,
  $paymentProfile,
  $addressBook,
  $rollBack,
  orderService,
  $document,
  $timeout,
  $categories,
  $home,
  $colorsData,
  $categoriesData
) {
  $scope.$appUser = $appUser;
  $scope.$search = $search;
  $scope.$cartService = $cartService;
  $scope.$home = $home;
  $scope.$paymentProfile = $paymentProfile;
  $scope.$addressBook = $addressBook;
  $scope.loginRegistrationService = loginRegistrationService;
  $scope.flashMessage = $flashMessage;
  $scope.$orderService = orderService;
  $scope.showPopUpMenu = false;
  $scope.showQuickBlock = false;
  $rootScope.showQuickSearch = false;
  $scope.quickSearchItems = [];
  $scope.totalResults = 0;
  $scope.search_query = '';

  
  $home.getData();

  
  $scope.getAllColors = function () {
    $colorsData.getData();
  }

  $scope.moveToPrevScreen = function () {
    $rollBack.goTo();
  };

  $scope.toggleQuickBlock = function () {
    $scope.showQuickBlock = !$scope.showQuickBlock;
  };

  $document.on("click", function (event) {
    // if (event.target.className.includes("quick-button")) {
    if (event.target.classList.contains("quick-button")) {
      $scope.toggleQuickBlock();
    } else {
      $scope.showQuickBlock = false;
    }
    $scope.$apply();
  });

  $scope.toggleMenu = function () {
    $scope.showPopUpMenu = !$scope.showPopUpMenu;
    $scope.refreshMenu = $scope.showPopUpMenu;
    /*var $body = angular.element(document.body);

        if ($scope.showPopUpMenu) {
            $body.addClass('overflow-hidden');
        }
        else {
            $body.removeClass('overflow-hidden');
        }*/
  };

  $scope.toggleLogin = function () {
    $flashMessage.clearAll();
    $appUser.showLoginModal = !$appUser.showLoginModal;
    $scope.loginRegistrationService.goToStep("login");
    var $body = angular.element(document.body);

    if ($appUser.showLoginModal) {
      $body.addClass("overflow-hidden");
    } else {
      $body.removeClass("overflow-hidden");
    }
  };

  $rootScope.openLoginModal = function () {
    $scope.toggleLogin();
  };

  $scope.resolveAuth = function () {
    if ($scope.$appUser.user) {
      $scope.$appUser.logout();
    } else {
      $scope.toggleLogin();
    }
    $scope.toggleMobileMenu();
  };

  $scope.toggleAll = function () {
    $scope.showPopUpMenu = false;
    $appUser.showLoginModal = false;
    angular.element(document.body).removeClass("overflow-hidden");
  };

  $scope.toggleMobileMenu = function () {
    $rootScope.mobileMenuActive = !$rootScope.mobileMenuActive;
    $rootScope.filterMenuActive = false;
  };

  $scope.toggleFilterMobileMenu = function () {
    // $scope.filterMenuActive = !$scope.filterMenuActive;
    $rootScope.mobileMenuActive = false;
    $rootScope.filterMenuActive = !$rootScope.filterMenuActive;
    $categoriesData.getData();
  }

  $scope.search = function (event) {
    $search.clearQuery();

    if (!isNaN(+$scope.search_query)) {
      // if user search product by id
      $search.setQueryData(null, null, null, $scope.search_query, { per_page: 30 });
      $search.findById($scope.showQuickSearch);
      $categories.getData();
    } else {
      if ($scope.selectedCategory && event.type === 'mousedown') {
        //if user search product by name and category and click view all
        $search.setQueryData($scope.search_query, $scope.selectedCategory, null, null, { per_page: 30 });
      } else {
        // if user search product by name and press enter
        $search.setQueryData($scope.search_query, null, null, null, { per_page: 30 });
      }
      console.log($search)
      $search.find().then(result => {
        if ($search.hasResult()) {
          $state.go("router.search.global", { viewMode: "tile", slug: "search" });
        } else {
          $state.go("router.home");
        }
      });

      $scope.showQuickSearch = false;
      $scope.search_query = '';
      $scope.quickSearchCategories = [];
      $scope.quickSearchItems = [];
      $scope.selectedCategory = null;
      document.activeElement.blur();
    }
  };

  $scope.quickSearchForCategories = function () {
    if ($scope.search_query) {
      apiService.quickSearch($scope.search_query).then(result => {
        $scope.quickSearchCategories = result.data.data;
        if ($scope.quickSearchCategories.length) {
          $scope.quickSearchForProducts(
            null,
            event
          );
        }
      });
    } else {
      $scope.quickSearchCategories = [];
      $scope.quickSearchItems = [];
      $scope.selectedCategory = null;
    }
  };

  $scope.quickSearchForProducts = function (id, event) {
    const data = {};

    $scope.quickSearchItems = [];
    event.preventDefault();
    $scope.quickSearchDataLoading = true;
    $scope.hideSpinner = true;

    if (!isNaN(+$scope.search_query)) {
      data.id = $scope.search_query;
    } else {
      data.name = $scope.search_query
    }

    if (id) {
      data.category_id = id;
    }

    apiService.search(data).then(result => {
      $scope.quickSearchDataLoading = false;
      $scope.selectedCategory = id;
      $scope.hideSpinner = false;
      $scope.quickSearchItems = result.data.data.slice(0, 8);
      $scope.totalResults = result.data.total;
    });
  };

  $scope.showsQuickSearch = function () {
    $scope.showQuickSearch = true;
  };

  $scope.hideQuickSearch = function () {
    $scope.showQuickSearch = false;
  };

  $scope.placeOrder = function (id) {
    $state.go(
      "router.customize-order.options",
      {
        mode: "product",
        slug: id
      },
      {
        reload: true
      }
    );
    $scope.showQuickSearch = false;
  };

  $scope.toggleChat = function () {
    var frame = document.getElementsByTagName("iframe")[1];
    frame.contentWindow.document
      .getElementById("tawkchat-minified-container")
      .click();
  };

  $scope.activeListNumber;
  $scope.toggleLinksList = function (listNumber) {
    if ($scope.activeListNumber !== listNumber) {
      $scope.activeListNumber = listNumber;
    } else {
      $scope.activeListNumber = null;
    }
  };
}
