import config from './config.js';
export default angular.module('successScreen', [])
    .config(config)
    .name;