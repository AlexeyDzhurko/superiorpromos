import successCtrl from './controller.js';
import view from './success-screen.html';
editProfileConfig.$inject = ['$stateProvider'];
export default function editProfileConfig($stateProvider){
    $stateProvider
        .state('router.my-account.success', {
            url: '/my-account/success',
            controller: successCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Success'
            }
        })
}