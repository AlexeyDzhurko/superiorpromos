artProofsCtrl.$inject = ['$scope', 'artProofService'];

export default function artProofsCtrl($scope, $artProofs) {
    $scope.$artProofs = $artProofs;
    $scope.artProofModalIsOpened = false;

    $scope.openArtProofModal = (e) => {
        e.preventDefault();
        e.stopPropagation();
        $scope.artProofModalIsOpened = true;
    }

    $scope.closeArtProofModal = () => {
        $scope.artProofModalIsOpened = false;
    }
}
