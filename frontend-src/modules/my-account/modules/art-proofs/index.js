import './art-proofs.scss';
import config from './config.js';
export default angular.module('artProofs', [])
    .config(config)
    .name;