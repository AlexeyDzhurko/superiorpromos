import artProofsCtrl from './controller.js';
import view from './art-proofs.html';
artProofsConfig.$inject = ['$stateProvider'];
export default function artProofsConfig($stateProvider){
    $stateProvider
        .state('router.my-account.art-proofs', {
            url: '/my-account/art-proofs',
            controller: artProofsCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Artproofs'
            }
        })
}