import './reorder.scss';

import reorderConfig from './config';
import thankPageReorder from './modules/thank-page-reorder';

export default angular.module('reorder', [thankPageReorder])
	.config(reorderConfig)
	.name;