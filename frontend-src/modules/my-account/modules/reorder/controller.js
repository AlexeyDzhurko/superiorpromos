reorderCtrl.$inject = ['$scope', '$reorder', '$usStates', '$countriesList','$uibModal', '$uibModalStack', 'paymentProfileService'];

export default function reorderCtrl($scope, $reorder, $usStates, $countries, $uibModal, $uibModalStack, $paymentProfile){
    $scope.$reorder = $reorder;
    $scope.$usStates = $usStates;
    $scope.isOpen = false;
    $scope.paymentProfileList = [];
    $scope.$countriesList = $countries;
    $scope.imprintSelector = {};
    $scope.paymentProfileList = $paymentProfile.profiles;

    $scope.$watch('$paymentProfile.profiles', function (item) {
        if(item) {
            $scope.paymentProfileList = $paymentProfile.profiles;

            $scope.paymentProfileList.forEach( profile => {
                profile.selected = false;
            })
        }
    });

    $scope.toggleImprintItemBody = function (id, state) {
        if (typeof state === 'boolean'){
            $scope.imprintSelector[id].expanded = state;
        } else {
            $scope.imprintSelector[id].expanded = !$scope.imprintSelector[id].expanded;
        }
    };

    $scope.checkCountry = function(data) {
        if(data.country !== 'US') {
            data.state = ''
        }
    };

    $scope.onImprintsBlankChange = function (data) {
        $scope.productImprints = angular.copy(data.getData().imprints);
        $scope.selectedImprints = {};

        rewriteImprint($scope.productImprints);
    };

    $scope.toggleColorSelector = function ($event, selector) {
        $event.stopPropagation();
        selector.colorSelectorVisible = !selector.colorSelectorVisible;
    };

    $scope.updateShipTime = function (shipTime, reorder) {
        reorder.cartItem.saveOrderParams();
    };

    $scope.colorStyle = function (color) {
        return {
            'background-color': color
        };
    };

    $scope.replaceImprintColor = function (imprint, color, index) {
        var selectedColors = $scope.selectedImprints[imprint.id].data.colors;
        selectedColors.splice(index, 1, color);
    };

    var rewriteImprint = function (arr) {
        if (arr && arr.length > 0) {
            arr.map
        }
    };

    $scope.openAddNewCardModal = function (orderId) {
        $scope.modalInstance = $uibModal.open({
            templateUrl: './views/partials/payment-card-modal/payment-card-modal.html',
            scope: $scope
        });
        $scope.currentOrderId = orderId;
    };

    $scope.cancel = function () {
        $uibModalStack.dismissAll();
    };

    $scope.assignNewPaymentProfile = function (item) {
        $scope.paymentProfileList = $paymentProfile.profiles;
        $scope.paymentProfile = $scope.paymentProfileList[$scope.setModel(item.id, $scope.paymentProfileList)];
        $scope.paymentProfile.selected = true;
        $scope.cancel();
        $scope.validateInput();
    };

    $scope.validateInput = function () {
        for (var k in $scope.checkoutParams){
            if ($scope.checkoutParams.hasOwnProperty(k) && !angular.isObject($scope.checkoutParams[k])){
                if (k === 'paymentProfile' && $scope.checkoutParams.paymentProfile === null && ($scope.paymentType === 'check' || $scope.paymentType === 'apply')) {
                    $scope.errors[k] = false;
                } else {
                    $scope.errors[k] = true;
                }
            } else {
                $scope.errors[k] = false;
            }
        }
        for (var k in $scope.errors){
            if($scope.errors.hasOwnProperty(k) && $scope.errors[k]){
                return false
            }
        }
        return true;
    };

    $scope.setModel = function(id, collection) {
        for (let i=0; i < collection.length; i++) {
            if (collection[i].id === id) {
                return i;
            }
        }
        return 0;
    };
}
