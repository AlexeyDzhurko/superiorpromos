import thankPageReorderCtrl from './controller.js';
import view from './thank-page-reorder.html';
thankPageReorderConfig.$inject = ['$stateProvider'];
export default function thankPageReorderConfig($stateProvider){
    $stateProvider
        .state('router.my-account.thank-page-reorder', {
            url: '/my-account/thank-page-reorder',
            controller: thankPageReorderCtrl,
            template:view,
            ncyBreadcrumb: {
                parent:'router.my-account.reorder',
                label: 'Thank you'
            }
        })
}