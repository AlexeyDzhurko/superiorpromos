import './thank-page-reorder.scss';
import thankPageReorderConfig from './config.js';
export default angular.module('thankPageReorder', [])
    .config(thankPageReorderConfig)
    .name;