import view from './reorder.html';
import reorderCtrl from './controller';
reorderConfig.$inject = ['$stateProvider'];

export default function reorderConfig($stateProvider){
    $stateProvider
        .state('router.my-account.reorder', {
            url: '/my-account/reorder',
            controller: reorderCtrl,
            template: view,
            ncyBreadcrumb: {
                label: 'Reorder'
            }
        })
}