import config from './config.js';
export default angular.module('editAddress', [])
    .config(config)
    .name;