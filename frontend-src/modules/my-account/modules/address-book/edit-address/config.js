import editAddressCtrl from './controller.js';
import view from './edit-address.view.html';
addressBookConfig.$inject = ['$stateProvider'];
export default function addressBookConfig($stateProvider){
    $stateProvider
        .state('router.my-account.edit-address', {
            url: '/my-account/address-book/:mode/:addressId',
            params:{
                addressId:null
            },
            controller: editAddressCtrl,
            template:view,
            ncyBreadcrumb: {
                parent:'router.my-account.address-book',
                label: 'Edit address'
            }
        })
}