editAddressCtrl.$inject = ['$scope', 'addressBookService', '$stateParams'];
export default function editAddressCtrl($scope, $addressBook, $stateParams) {
    $scope.$addressBook = $addressBook;
    if ($stateParams.addressId){
        $scope.$addressBook.getAddress($stateParams.addressId).then(function (address) {
            $scope.address = address;
        })
    } else {
        $scope.address = {}
    }
}