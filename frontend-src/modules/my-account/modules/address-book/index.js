import config from './config.js';
import editAddress from './edit-address';
export default angular.module('addressBook', [editAddress])
    .config(config)
    .name;