import addressBookCtrl from './controller.js';
import view from './address-book.html';
addressBookConfig.$inject = ['$stateProvider'];
export default function addressBookConfig($stateProvider){
    $stateProvider
        .state('router.my-account.address-book', {
            url: '/my-account/address-book',
            controller: addressBookCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Address book'
            }
        })
}