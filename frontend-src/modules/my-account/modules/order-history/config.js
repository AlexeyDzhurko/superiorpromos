import orderHistoryCtrl from './controller.js';
import view from './order-history.html';
orderHistoryConfig.$inject = ['$stateProvider'];
export default function orderHistoryConfig($stateProvider){
    $stateProvider
        .state('router.my-account.order-history', {
            url: '/my-account/order-history',
            controller: orderHistoryCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Order history'
            }
        })
}