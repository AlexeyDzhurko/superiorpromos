import './order-history.scss';
import config from './config.js';
export default angular.module('orderHistory', [])
    .config(config)
    .name;