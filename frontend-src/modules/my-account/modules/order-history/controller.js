orderHistoryCtrl.$inject = ['$scope', 'cartService', '$notifications', 'customizeOrderService', 'apiService', '$reorder', 'artProofService', '$location'];

export default function orderHistoryCtrl($scope, $cartService, $notifications, customizeOrderService, apiService, $reorder, $artProofs, $location) {
    $scope.$cartService = $cartService;
    $scope.$notifications = $notifications;
    $scope.$artProofs = $artProofs;
    $scope.artProofModalIsOpened = false;

    angular.element(document).ready(function(){
        if($location.$$hash) {
            let chosenOrder = angular.element(document.querySelector(`#${$location.$$hash} .order-items`));

            if(chosenOrder.length) {
                chosenOrder[0].click();
            }
        }
    });
    
    $scope.searchReset = function () {
        $scope.start_date = '';
        $scope.end_date = '';
        $scope.title_search = '';
    };

    //calculate max height of the block depending on what's inside it
    var _calculateMaxHeight = function (order) {
        let height = 40;
        if (order.cart_items.length) {
            height =  order.cart_items.length * 900;
            for (let i = 0; i < order.cart_items.length; i++) {
                if (order.cart_items[i].art_proofs.length || order.cart_items[i].art_proofs.length) {
                    let expandedArr = order.cart_items[i].proofsOpened ? order.cart_items[i].art_proofs : order.cart_items[i].notes;
                    height += expandedArr.length * (order.cart_items[i].proofsOpened ?  80 : 66) ; //80 - proofs row height, 66 - same for notes
                    height += expandedArr.reduce((sum, current) => { //change height when artproofs/notes blocks items are expanded
                        if (current.opened) {
                            if (current.status === 'approved') {
                                if (current.customer_note.length) {
                                   sum += 88; //88 is dialog row height
                                }
                                if (current.note.length) {
                                    sum += 88;
                                }
                            } else sum += 127; // height of textarea with controls (approve/decline)
                        }
                        return sum;
                    }, 0);
                }
            }
        }
        return height + 'px';
    };

    $scope.expandBlock = function (order) {
        order.style = _calculateMaxHeight(order);
    };

    $scope.log = function(item) {
        console.log(item);
    };

    $scope.resetHeight = function(order) {
        order.style = _calculateMaxHeight(order);
    };

    $scope.specifyOrder = function (order, cartItem) {
        if(!order.hasOwnProperty('reorderCartItems')) order.reorderCartItems = [];

        if(cartItem.reorder) {
            order.reorderCartItems.push(cartItem.id);
        } else {
            var ind = order.reorderCartItems.indexOf(cartItem.id);
            ind > -1 && order.reorderCartItems.splice(ind, 1);
        }

        order.reorderCartItems.length ? order.isReorderActive = true : order.isReorderActive = false;

        $cartService.ordersForReorder = [];
        angular.forEach($cartService.orderHistoryItems, function (item) {
            if (item.reorderCartItems && item.reorderCartItems.length) $cartService.ordersForReorder = $cartService.ordersForReorder.concat(item.reorderCartItems);
        });

        $cartService.isReorderBtnActive = $cartService.ordersForReorder.length;
    };

    $scope.proofsOpener = function (cartItem) {
        var newArtproofs = $notifications.getNewArtproofs();
        cartItem.proofsOpened = true;
        cartItem.detailsOpened = false;
        if (cartItem.art_proofs.length && newArtproofs.length) {
            angular.forEach(cartItem.art_proofs, function (element) {
                angular.forEach(newArtproofs, function (newElement, index) {
                    if (element.id == newElement.id) {
                        $notifications.deleteReadArtproof(index);
                        apiService.sendArtproofStatusRead(element.id);
                        return;
                    }
                });
            });
        }
    };

    $scope.notesOpener = function (cartItem) {
        cartItem.proofsOpened = false;
        cartItem.detailsOpened = false;
    };

    $scope.detailsOpener = function (cartItem) {
        cartItem.detailsOpened = true;
    };

    $scope.toggleOrder = function (cartItem) {
        cartItem.opened=!cartItem.opened;
        
        if (cartItem.opened) {
            cartItem.detailsOpened = true;
        }
    };

    $scope.switchOpener = function (note) {
        note.opened = !note.opened;
        var newNotes = $notifications.getNewNotes();
        newNotes.length && angular.forEach(newNotes, function (element, index) {
            if (element.id == note.id) {
                $notifications.deleteReadNote(index);
                apiService.sendNoteStatusRead(note.id);
                return;
            }
        });
    };

    $scope.hasPending = function (proofsList) {
        return proofsList.filter(v => v.status === 'N/A').length > 0;
    };

    $scope.reorder = function () {
        $reorder.reorder($cartService.ordersForReorder);
    };

    $scope.markClosedRows = function (rows, currentOpened) {
        if (rows.filter(v => v.opened).length) {
            return !currentOpened;
        }
        return false;
    };

    $scope.openArtProofModal = (e) => {
        e.preventDefault();
        e.stopPropagation();
        $scope.artProofModalIsOpened = true;
    }

    $scope.closeArtProofModal = () => {
        $scope.artProofModalIsOpened = false;
    }
}
