import config from './config.js';
export default angular.module('savedCart', [])
    .config(config)
    .name;