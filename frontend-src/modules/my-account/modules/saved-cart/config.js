import savedCartCtrl from './controller.js';
import view from './saved-cart.html';
savedCartConfig.$inject = ['$stateProvider'];
export default function savedCartConfig($stateProvider){
    $stateProvider
        .state('router.my-account.saved-cart', {
            url: '/my-account/saved-cart',
            controller: savedCartCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Saved cart'
            }
        })
}