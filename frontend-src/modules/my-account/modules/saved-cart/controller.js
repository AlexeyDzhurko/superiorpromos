savedCartCtrl.$inject = ['$scope', 'cartService', '$state'];
export default function savedCartCtrl($scope, cartService, $state) {
    $scope.cartService = cartService;
    $scope.moveToShoppingCart = function (idArray) {
        cartService.moveSavedCartToCart(idArray).then(function () {
            $state.go('router.shopping-cart');
        })
    }
}
