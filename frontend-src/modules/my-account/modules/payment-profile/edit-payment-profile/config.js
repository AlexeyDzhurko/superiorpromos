import editPaymentProfileCtrl from './controller.js';
import view from './edit-payment-profile.view.html';
editPaymentProfileConfig.$inject = ['$stateProvider'];
export default function editPaymentProfileConfig($stateProvider){
    $stateProvider
        .state('router.my-account.edit-payment-profile', {
            url: '/my-account/payment-profile/:mode/:profileId',
            params:{
                profileId: null
            },
            controller: editPaymentProfileCtrl,
            template:view,
            ncyBreadcrumb: {
                parent:'router.my-account.payment-profile',
                label: 'Edit payment profile'
            }
        })
}