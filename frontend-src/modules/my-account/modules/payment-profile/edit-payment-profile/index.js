import config from './config.js';
export default angular.module('editPaymentProfileModule', [])
    .config(config)
    .name;