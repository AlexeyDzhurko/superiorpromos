editPaymentProfileCtrl.$inject = ['$scope', 'paymentProfileService', '$stateParams'];
export default function editPaymentProfileCtrl($scope, $paymentProfile, $stateParams) {
    $scope.$paymentProfile = $paymentProfile;
    if ($stateParams.profileId){
        $scope.$paymentProfile.getPaymentProfile($stateParams.profileId).then(function (profile) {
            $scope.profile = profile;
        })
    } else {
        $scope.profile = {}
    }
}