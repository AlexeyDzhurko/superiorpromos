import './payment-profile.scss';
import config from './config.js';
import editPaymentProfileModule from './edit-payment-profile'
export default angular.module('paymentProfile', [editPaymentProfileModule])
    .config(config)
    .name;