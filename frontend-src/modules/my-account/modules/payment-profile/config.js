import paymentProfileCtrl from './controller.js';
import view from './payment-profile.html';
paymentProfileConfig.$inject = ['$stateProvider'];
export default function paymentProfileConfig($stateProvider){
    $stateProvider
        .state('router.my-account.payment-profile', {
            url: '/my-account/payment-profile',
            controller: paymentProfileCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Payment profile'
            }
        })
}