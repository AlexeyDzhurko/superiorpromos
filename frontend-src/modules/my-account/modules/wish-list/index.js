import './wish-list.scss';
import config from './config.js';
export default angular.module('wishList', [])
    .config(config)
    .name;