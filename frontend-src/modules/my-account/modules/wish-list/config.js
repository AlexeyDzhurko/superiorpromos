import wishListCtrl from './controller.js';
import view from './wish-list.html';
wishListConfig.$inject = ['$stateProvider'];
export default function wishListConfig($stateProvider){
    $stateProvider
        .state('router.my-account.wish-list', {
            url: '/my-account/wish-list',
            controller: wishListCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Whish list'
            }
        })
}