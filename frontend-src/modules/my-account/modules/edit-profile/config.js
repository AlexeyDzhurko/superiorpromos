import editProfileCtrl from './controller.js';
import view from './edit-profile.html';
editProfileConfig.$inject = ['$stateProvider'];
export default function editProfileConfig($stateProvider){
    $stateProvider
        .state('router.my-account.edit-profile', {
            url: '/my-account/edit-profile',
            controller: editProfileCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Edit profile'
            }
        })
}