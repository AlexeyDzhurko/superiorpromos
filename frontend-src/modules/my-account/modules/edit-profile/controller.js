editProfileCtrl.$inject = ['$scope', 'appUserService', 'apiService'];
export default function editProfileCtrl($scope, $appUser, $apiService) {
    $scope.$appUser = $appUser;
    $scope.success = {};
    $scope.getProfile  = function () {
        $scope.$appUser.getEditableProfile().then(function (res) {
            $scope.information = res.information;
            $scope.email = res.email;
            $scope.password = res.password;
        });
    };

    $scope.getProfile();

    $scope.handleErrors = function (formName, errors) {
        for (var key in errors.data){
            if (errors.data.hasOwnProperty(key) && $scope[formName][key]){
                $scope[formName][key].$error.backend = errors.data[key].join(' ');
            }
        }
    };

    $scope.resetError = function (field) {
        field.$error = {}
    };

    $scope.changeContactInfo = function(){
        $scope.$appUser.updateUserProfile({information:$scope.information}).then(function () {
        }, function (error) {
            $scope.handleErrors('contactInformationForm', error);
        });
    };

    $scope.changePassword = function(){
        $scope.$appUser.updateUserProfile({password:$scope.password}).then(function () {
            $scope.success = {
                visible: true,
                message:'Password changed!'
            };
        }, function (error) {
            $scope.handleErrors('changePasswordForm', error);
        });
    };

    $scope.changeEmail = function(){
        $scope.$appUser.updateUserProfile({email:$scope.email}).then(function () {
            $scope.success = {
                visible: true,
                message:'Email changed!'
            };
        }, function (error) {
            $scope.handleErrors('changeEmailForm', error);
        });
    };

    $scope.submitSuccess = function () {
        $scope.success = {};
        $scope.getProfile();
    };

    $scope.isAllPdfError;
    $scope.addFile = function (files) {
        $scope.isAllPdfError = false;
        
        let isAllPdf = Array.prototype.slice.call(files).every(e => e.type === 'application/pdf');
        if (!isAllPdf) {
            $scope.isAllPdfError = true;
            return;
        }

        let fd = new FormData();
        fd.append("tax_exemption_certificate", files[0]);
        fd.append("provide_later", 0);
        $apiService.uploadTaxExemptionCertificate(fd).then(refreshMe, handleError);

        function refreshMe() {
            $appUser.getUserAccount();
        }

        function handleError(err) {
            console.log('error', err);
        }
    }
}
