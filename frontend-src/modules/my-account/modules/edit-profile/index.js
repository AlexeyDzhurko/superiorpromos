import config from './config.js';
export default angular.module('editProfile', [])
    .config(config)
    .name;