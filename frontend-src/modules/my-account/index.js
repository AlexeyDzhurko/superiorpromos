import './my-account.scss';
import myAccountConfig from './config';

import addressBook from './modules/address-book';
import artProofs from './modules/art-proofs';
import editProfile from './modules/edit-profile';
import orderHistory from './modules/order-history';
import paymentProfile from './modules/payment-profile';
import savedCart from './modules/saved-cart';
import wishList from './modules/wish-list';
import reorder from './modules/reorder';

export default angular.module('myAccountModule', [
	addressBook,
	artProofs,
	editProfile,
	orderHistory,
	paymentProfile,
	savedCart,
	wishList,
	reorder
]).config(myAccountConfig).name;
