import myAccountLayout from './my-account.html';
import myAccountCtrl from './controller';

myAccountConfig.$inject = ['$stateProvider'];

export default function myAccountConfig($stateProvider){
    $stateProvider
        .state('router.my-account', {
            abstrac: true,
            template: myAccountLayout,
            controller: myAccountCtrl,
            ncyBreadcrumb: {
                label: 'Account'
            }
        })
}
