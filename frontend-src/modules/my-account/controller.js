myAccountCtrl.$inject = ['$scope', 'cartService', 'appUserService', 'paymentProfileService', 'addressBookService', 'artProofService', '$state', '$location', '$anchorScroll', '$rootScope', '$timeout', 'apiService', '$notifications'];
export default function myAccountCtrl($scope, $cartService, $appUser, $paymentProfile, $addressBook, $artProofs, $state, $location, $anchorScroll, $rootScope, $timeout, apiService, $notifications) {
    $scope.$paymentProfile = $paymentProfile;
    $scope.$addressBook = $addressBook;
    $scope.$appUser = $appUser;
    $scope.$cartService = $cartService;
    $scope.$artProofs = $artProofs;
    $scope.userNotesData = {};
    $scope.$notifications = $notifications;

    $scope.sendUserNotesApprovement = function (status, note) {
        note.status = 'pending';
        switch (status) {
            case 'approved':
                note.approved = true;
                break;
            case 'declined':
                note.approved = false;
                break;
        }

        $notifications.approveUserNotes(note.id, note.approved, note.customer_note).then(function () {
            note.status = status;
        }, function () {
            note.status = 'N/A';
        });
    }


}
