import view from './guarantee.html';
import guaranteeCtrl from './controller';
guaranteeConfig.$inject = ['$stateProvider'];

export default function guaranteeConfig($stateProvider){
    $stateProvider
        .state('router.guarantee', {
            url: '/guarantee',
            controller: guaranteeCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Guarantee'
            }
        })
}