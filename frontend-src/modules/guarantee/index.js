import './guarantee.scss';

import guaranteeConfig from './config';

export default angular.module('guaranteeModule', [])
	.config(guaranteeConfig)
	.name;
