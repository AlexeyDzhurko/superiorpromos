
import view from './terms-conditions.html';
import termsCtrl from './controller';
termsConfig.$inject = ['$stateProvider'];

export default function termsConfig($stateProvider){
    $stateProvider
        .state('router.terms-conditions', {
            url: '/terms-conditions',
            controller: termsCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Terms And Conditions'
            }
        })
}