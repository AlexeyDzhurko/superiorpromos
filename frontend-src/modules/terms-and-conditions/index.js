import './terms-conditions.scss';

import termsConfig from './config';

export default angular.module('termsConditionsModule', [])
	.config(termsConfig)
	.name;
