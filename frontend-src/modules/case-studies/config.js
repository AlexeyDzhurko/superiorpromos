import view from './case-studies.html';
import caseStudiesCtrl from './controller';
caseStudiesConfig.$inject = ['$stateProvider'];

export default function caseStudiesConfig($stateProvider){
    $stateProvider
        .state('router.case-studies', {
            url: '/case-studies',
            controller: caseStudiesCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Case studies'
            }
        })
}