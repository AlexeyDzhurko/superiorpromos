caseStudiesCtrl.$inject = ['$scope', '$staticPages'];
export default function caseStudiesCtrl($scope, $staticPages){
    $scope.oneAtATime = false;
    $scope.$staticPages = $staticPages;
}
