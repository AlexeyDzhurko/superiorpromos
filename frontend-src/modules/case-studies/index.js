import './case-studies.scss';

import caseStudiesConfig from './config';

export default angular.module('caseStudiesModule', [])
	.config(caseStudiesConfig)
	.name;
