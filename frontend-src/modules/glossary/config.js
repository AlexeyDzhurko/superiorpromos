import view from './glossary.html';
import glossaryCtrl from './controller';
glossaryConfig.$inject = ['$stateProvider'];

export default function glossaryConfig($stateProvider){
    $stateProvider
        .state('router.glossary', {
            url: '/glossary',
            controller: glossaryCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Glossary'
            }
        })
}