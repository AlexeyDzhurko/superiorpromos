glossaryCtrl.$inject = ['$scope', '$staticPages'];
export default function glossaryCtrl($scope, $staticPages){
    $scope.oneAtATime = false;
    $scope.$staticPages = $staticPages;
}
