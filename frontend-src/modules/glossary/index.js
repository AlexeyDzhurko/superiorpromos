import './glossary.scss';

import glossaryConfig from './config';

export default angular.module('glossaryModule', [])
	.config(glossaryConfig)
	.name;
