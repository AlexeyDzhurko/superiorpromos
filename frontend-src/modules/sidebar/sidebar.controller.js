import './sidebar.scss';

sidebarCtrl.$inject = ['$rootScope', '$scope', '$state'];

export default function sidebarCtrl($rootScope, $scope, $state) {

    $scope.isShown = false;

    $rootScope.$on('$viewContentLoaded',
        function(event, toState, toParams, fromState, fromParams){

            switch ($state.current.name) {
                case 'router.categories' :
                    $scope.isShown = true;
                    break;
                case 'router.home':
                    $scope.isShown = true;
                    break;
                case 'router.search.global':
                    $scope.isShown = true;
                    break;
                default : {
                    $scope.isShown = false;
                }
            }});
}
