promotionCtrl.$inject = ['$scope', 'apiService'];
export default function promotionCtrl($scope, apiService){
    apiService.getPromotions().then(function (res){
        $scope.promotions = res.data;
    });
}
