import view from './promotion.html';
import promotionCtrl from './controller';
promotionConfig.$inject = ['$stateProvider'];

export default function promotionConfig($stateProvider){
    $stateProvider
        .state('router.promotion', {
            url: '/promotion',
            controller: promotionCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Current Promotion'
            }
        })
}