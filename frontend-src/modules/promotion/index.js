import './promotion.scss';

import promotionConfig from './config';

export default angular.module('promotionModule', [])
    .config(promotionConfig)
    .name;
