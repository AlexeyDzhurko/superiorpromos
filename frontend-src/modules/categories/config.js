import view from "./categories.view.html";
import categoriesCtrl from "./categories.controller";

categoriesConfig.$inject = ["$stateProvider"];

export default function categoriesConfig($stateProvider) {
  $stateProvider.state("router.categories", {
    url: "/:slug",
    reloadOnSearch: false,
    controller: categoriesCtrl,
    template: view,
    params: {
      slug: { type: "string", raw: true },
      viewMode: 'grid',
      searchQuery: {}
    },
    ncyBreadcrumb: {
      label: "Category"
    },
    onEnter: ['$search', '$transition$', function ($search, $transition$) {
      $search.query.category_slug = $transition$.params().slug;
      $search.find();
    }]
  });
}
