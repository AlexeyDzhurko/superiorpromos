import './categories.scss';

import categoriesConfig from './config';

export default angular.module('categoriesModule', [])
	.config(categoriesConfig)
	.name;
