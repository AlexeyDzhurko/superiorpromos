import './about-us.scss';

import aboutConfig from './config';

export default angular.module('aboutModule', [])
	.config(aboutConfig)
	.name;
