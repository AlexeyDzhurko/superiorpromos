import view from './about.html';
import aboutCtrl from './controller';
aboutConfig.$inject = ['$stateProvider'];

export default function aboutConfig($stateProvider){
    $stateProvider
        .state('router.about', {
            url: '/about',
            controller: aboutCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'About'
            }
        })
}