shoppingCartCtrl.$inject = ['$scope', 'cartService', '$state', '$transitions', 'LoginRegistrationService'];
export default function shoppingCartCtrl($scope, cartService, $state, $transitions, $login) {
    $scope.cartService = cartService;
    $scope.breadcrumbs = [
        { id: 1, name: 'Home', path: 'router.home'},
        { id: 2, name: 'Shopping Cart', path: 'router.shopping-cart'}
    ];
    $scope.checkout = function () {
        if ($login.checkAuth() === 'authorized') {
            $state.go('router.cart-checkout');
        }
    };
    $transitions.onStart(null, function(transition) {
        if ((transition.to().name === 'router.ready-order' || transition.to().name === 'router.cart-checkout') && !cartService.cartItems.length) {
            return false;
        }
    });
}