import checkoutCtrl from './controller.js';
import view from './checkout.html';
checkoutConfig.$inject = ['$stateProvider'];
export default function checkoutConfig($stateProvider){
    $stateProvider
        .state('router.cart-checkout', {
            url: '/cart/checkout',
            controller: checkoutCtrl,
            template:view,
            ncyBreadcrumb: {
                parent:'router.shopping-cart',
                label: 'Checkout'
            }
        })
}