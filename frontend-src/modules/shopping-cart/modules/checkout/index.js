import './checkout.scss';
import checkoutConfig from './config.js';
export default angular.module('checkout', [])
    .config(checkoutConfig)
    .name;