checkoutCtrl.$inject = ['$scope', '$state', '$stateParams', '$timeout', 'addressBookService', 'cartService', 'paymentProfileService', 'LoginRegistrationService', 'appUserService', 'apiService'];

export default function checkoutCtrl($scope, $state, $stateParams, $timeout, $addressBook, $cart, $paymentProfile, $login, $appUser, $apiService) {
    $scope.$addressBook = $addressBook;
    $scope.appUser = $appUser;
    $scope.$cart = $cart;
    $scope.$paymentProfile = $paymentProfile;
    $scope.multipleLocations = {
        type: 'single'
    };
    $scope.taxExemption = {
        type: 'no',
        file: ''
    };
    $scope.paymentType = 'card';
    $scope.shippingAddressList = [];
    $scope.billingAddressList = [];
    $scope.paymentProfileList = [];
    $scope.checkoutParams = {
        shippingAddress: $addressBook.getDefaultAddresses()['shipping'],
        billingAddress: $addressBook.getDefaultAddresses()['billing'],
        paymentProfile: $paymentProfile.getDefaultProfile(),
        multipleLocations: {
            type: 'single',
            details: null
        },
        cvv: '',
        expired: false
    };

    $cart.getCart().then(setTimeout(() => {
        $scope.displayMultipleLocations = $cart.cartItems.length > 1
    }));

    $scope.breadcrumbs = [
        {id: 1, name: 'Home', path: 'router.home'},
        {id: 2, name: 'Shopping Cart', path: 'router.shopping-cart'},
        {id: 3, name: 'Checkout', path: 'router.cart-checkout'}
    ];

    $scope.watchShippingAddress = true;
    $scope.watchBillingAddress = true;
    $scope.watchPaymentMethod = true;

    $scope.$watch('$addressBook.getShippingAddresses()[0]', function (item) {
        if ($scope.watchShippingAddress) {
            $scope.checkoutParams.shippingAddress = item;
            $scope.shippingAddressList = $addressBook.getShippingAddresses();
        }
    });

    $scope.$watch('$addressBook.getBillingAddresses()[0]', function (item) {
        if ($scope.watchBillingAddress) {
            $scope.checkoutParams.billingAddress = item;
            $scope.billingAddressList = $addressBook.getBillingAddresses();
        }
    });

    $scope.$watch('$paymentProfile.profiles[0]', function (item) {
        if ($scope.watchPaymentMethod) {
            $scope.checkoutParams.paymentProfile = item;
            $scope.paymentProfileList = $paymentProfile.profiles;
        }
    });

    $scope.toggleCreateNewBillingAddress = function () {
        $scope.showAddBillingAddress = !$scope.showAddBillingAddress;
        $scope.watchBillingAddress = false;
    };

    $scope.toggleCreateNewShippingAddress = function () {
        $scope.showAddShippingAddress = !$scope.showAddShippingAddress;
        $scope.watchShippingAddress = false;
    };

    $scope.toggleAddPaymentProfile = function () {
        $scope.showAddPaymentProfile = !$scope.showAddPaymentProfile;
        if (!$scope.showAddPaymentProfile) {
            $scope.checkoutParams.paymentProfile = null;
            $scope.validateInput();
        }
    };

    $scope.assignNewBillingAddress = function (item) {
        $scope.toggleCreateNewBillingAddress();

        $scope.billingAddressList = $addressBook.getBillingAddresses();
        $scope.checkoutParams.billingAddress = $scope.billingAddressList[$scope.setModel(item.id, $scope.billingAddressList)];
        $scope.validateInput();
    };

    $scope.assignNewShippingAddress = function (item) {
        $scope.toggleCreateNewShippingAddress();

        $scope.shippingAddressList = $addressBook.getShippingAddresses();
        $scope.checkoutParams.shippingAddress = $scope.shippingAddressList[$scope.setModel(item.id, $scope.shippingAddressList)];
        $scope.validateInput();
    };

    $scope.addDisabled = function() {
        document.getElementById('checkout').style.backgroundColor = "#9e9e9e"
        document.getElementById('checkout').classList.add('disabled')
    }

    $scope.removeDisabled = function() {
        document.getElementById('checkout').style.backgroundColor = "#4ac9ff"
        document.getElementById('checkout').classList.remove('disabled')
    }

    $scope.setModel = function (id, collection) {
        for (let i = 0; i < collection.length; i++) {
            if (collection[i].id === id) {
                return i;
            }
        }
        return 0;
    };

    $scope.assignNewPaymentProfile = function (item) {
        $scope.toggleAddPaymentProfile();

        $scope.paymentProfileList = $paymentProfile.profiles;
        $scope.checkoutParams.paymentProfile = $scope.paymentProfileList[$scope.setModel(item.id, $scope.paymentProfileList)];
        $scope.validateInput();
    };

    $scope.errors = {};
    $scope.validateInput = function () {
        $scope.errors = {};
        if (($scope.paymentType !== 'check' && $scope.paymentType !== 'apply')
            && (!$scope.checkoutParams.paymentProfile || $scope.checkoutParams.paymentProfile.is_expired)
        ) {
            $scope.errors['expired'] = true;
            $scope.addDisabled()

        } else {
            $scope.errors['expired'] = false;
            $scope.removeDisabled()

            function findPos(obj) {
                var curtop = -150;
                if (obj.offsetParent) {
                    do {
                        curtop += obj.offsetTop;
                    } while (obj = obj.offsetParent);
                    return [curtop];
                }
            }
            window.scroll(-150, findPos(document.getElementById("err")));
        }
        for (var k in $scope.checkoutParams) {
            $scope.errors[k] = false;
            if ($scope.checkoutParams.hasOwnProperty(k)
                && !angular.isObject($scope.checkoutParams[k])
                && ($scope.paymentType !== 'check' && $scope.paymentType !== 'apply')
                
            ) {
                if (k === 'paymentProfile' && $scope.checkoutParams.paymentProfile === null) {                
                    $scope.errors[k] = true;
                }
                if (k === 'cvv' && !/^[0-9]{3,4}$/.test($scope.checkoutParams.cvv)) {
                    $scope.errors[k] = true;
                }
                if (k === 'expired' && ($scope.checkoutParams.paymentProfile != null && $scope.checkoutParams.paymentProfile.is_expired)) {
                    $scope.errors[k] = true;
                }
            } else {
                $scope.errors[k] = false;
            }
        }

        for (var j in $scope.errors) {
            if ($scope.errors.hasOwnProperty(j) && $scope.errors[j]) {
                return false;
            }
        }
        return true;
    };

    $scope.switchPaymentType = function () {
        if ($scope.paymentType === 'check' || $scope.paymentType === 'apply') {
            $scope.checkoutParams.paymentProfile = null;
        }
        $scope.validateInput();
    };

    $scope.hideCardNumber = function (cardNumber) {
        return cardNumber && '*****' + cardNumber.substr(cardNumber.length - 5, cardNumber.length);
    };

    $scope.checkExpiredDate = function () {
        var date = new Date();
        if (20 + $scope.checkoutParams.paymentProfile.expiration_year > date.getFullYear()
            || ($scope.checkoutParams.paymentProfile.expiration_month >= date.getMonth()
                && 20 + $scope.checkoutParams.paymentProfile.expiration_year == date.getFullYear())
        ) {
            $scope.checkoutParams.paymentProfile.is_expired = false;
            $scope.errors['expired'] = false;
        } else {
            $scope.checkoutParams.paymentProfile.is_expired = true;
            $scope.errors['expired'] = true;
        }
    }

    $scope.addFile = function (files) {
        let fd = new FormData();
        fd.append("tax_exemption_certificate", files[0]);
        fd.append("provide_later", 0);
        $apiService.uploadTaxExemptionCertificate(fd).then(refreshMe, handleError());
    };

    $scope.sendProvideLater = function () {
        if ($scope.taxExemption.type === 'provide later') {
            let fd = new FormData();
            fd.append("provide_later", 1);
            $apiService.uploadTaxExemptionCertificate(fd).then(handleSuccess, handleError);
        }
    };

    function refreshMe() {
        $appUser.getUserAccount();
    }

    function handleSuccess(res) {
    }

    function handleError(err) {
        console.log('error', err);
    }


    var _checkout = function (items) {
        if ($scope.validateInput()) {
            if (!$scope.checkoutParams || !$scope.checkoutParams.shippingAddress) {
                $scope.errors['shipping'] = true;
            } else {
                $scope.errors['shipping'] = false;
            }

            if (!$scope.checkoutParams || !$scope.checkoutParams.billingAddress) {
                $scope.errors['billing'] = true;
            } else {
                $scope.errors['billing'] = false;
            }

            if ($scope.errors['shipping'] || $scope.errors['billing']) {
                return false;
            }

            $cart.readyOrder.items = items;
            $cart.readyOrder.checkoutParams = $scope.checkoutParams;
            $cart.readyOrder.paymentType = $scope.paymentType;
            $cart.readyOrder.multipleLocations = $scope.multipleLocations;

            $state.go('router.ready-order');
        }
    };

    $scope.checkout = function (items) {
        if ($login.checkAuth() === 'authorized') {
            _checkout(items);
        }
    };
}
