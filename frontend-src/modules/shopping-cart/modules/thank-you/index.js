import './thank-you.scss';
import thankYouConfig from './config.js';
export default angular.module('thankYou', [])
    .config(thankYouConfig)
    .name;