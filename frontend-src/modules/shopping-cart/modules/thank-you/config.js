import thankYouCtrl from './controller.js';
import view from './thank-you.html';
thankYouConfig.$inject = ['$stateProvider'];
export default function thankYouConfig($stateProvider){
    $stateProvider
        .state('router.thank-you', {
            url: '/thank-you',
            controller: thankYouCtrl,
            template:view,
            ncyBreadcrumb: {
                parent:'router.cart-checkout',
                label: 'Thank you'
            }
        })
}