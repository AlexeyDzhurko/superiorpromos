thankYouCtrl.$inject = ['$scope', '$state', '$window', 'cartService', 'appUserService'];
export default function thankYouCtrl($scope, $state, $window, $cart, $appUserService) {
    // NOTE: if empty cart redirect to home page
    if (!($cart.orderedCartItems && $cart.orderedCartItems.length)) { 
        $state.go('router.home');
    }

    console.log($cart.orderedCartInfo.orderInfo);

    $scope.$cart = $cart;
    $scope.userData;

    $scope.breadcrumbs = [
        { id: 1, name: 'Home', path: 'router.home'},
        { id: 2, name: 'Order Status', path: 'router.thank-you'}
    ];

    $appUserService.getUserAccount().then(data => {
        $scope.userData = data;

        if ($cart.orderedCartInfo) {
            window._rrES = {
                seller_id: 52232,
                email: data.email,
                invoice: $cart.orderedCartInfo.orderInfo.id
            };
        }

        const s = document.createElement('script');s.type='text/javascript';
        s.async = true;
        s.src = "https://www.resellerratings.com/popup/include/popup.js";
        const ss = document.getElementsByTagName('script')[0];
        ss.parentNode.insertBefore(s,ss);
    });

    $scope.printPage = function () {
        $window.print();
    };
}
