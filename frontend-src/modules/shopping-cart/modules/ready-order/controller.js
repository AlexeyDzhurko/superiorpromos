readyOrderCtrl.$inject = ['$scope', '$state', '$stateParams', 'addressBookService', 'cartService', 'paymentProfileService', 'LoginRegistrationService'];

export default function readyOrderCtrl($scope, $state, $stateParams, $addressBook, $cart, $paymentProfile, $login) {
    $scope.$addressBook = $addressBook;
    $scope.$cart = $cart;
    $scope.$paymentProfile = $paymentProfile;
    $scope.multipleLocations = $cart.readyOrder.multipleLocations;
    $scope.paymentType = $cart.readyOrder.paymentType;
    $scope.checkoutParams = $cart.readyOrder.checkoutParams;
    $scope.paymentError = '';

    $scope.breadcrumbs = [
        { id: 1, name: 'Home', path: 'router.home'},
        { id: 2, name: 'Shopping Cart', path: 'router.shopping-cart'},
        { id: 3, name: 'Checkout', path: 'router.cart-checkout'},
        { id: 4, name: 'Place Order', path: 'router.ready-order'}
    ];

    if (!$scope.checkoutParams
        || !$scope.checkoutParams.shippingAddress
        || !$scope.checkoutParams.billingAddress
    ) {
        $state.go('router.cart-checkout');
    }

    $scope.errors = {};
    // $scope.validateInput = function () {
    //     for (var k in $scope.checkoutParams){
    //         if ($scope.checkoutParams.hasOwnProperty(k) && !angular.isObject($scope.checkoutParams[k])){
    //             if (k === 'paymentProfile' && $scope.checkoutParams.paymentProfile === null) {
    //                 $scope.errors[k] = false;
    //             } else if (k === 'cvv' && /^[0-9]{3,4}$/.test($scope.checkoutParams.cvv)) {
    //                 $scope.errors[k] = false;
    //             } else {
    //                 $scope.errors[k] = true;
    //             }
    //         } else {
    //             $scope.errors[k] = false;
    //         }
    //     }
    //     for (var k in $scope.errors){
    //         if($scope.errors.hasOwnProperty(k) && $scope.errors[k]){
    //             return false
    //         }
    //     }
    //     return true;
    // };

    $scope.hideCardNumber = function (cardNumber) {
        return cardNumber && '*****' + cardNumber.substr(cardNumber.length-5, cardNumber.length);
    };

    var _checkout = function (items) {
        $scope.paymentError = '';
        // if ($scope.validateInput()){
        $cart.orderedCartItems = angular.copy($cart.cartItems);
        $cart.orderedCartInfo = angular.copy($scope.checkoutParams);
        $cart.orderedCartTotal = angular.copy($cart.totalSelectedPrice);
        $cart.checkout(items, $scope.checkoutParams, $scope.paymentType, $scope.multipleLocations).then(function (res) {
            $cart.orderedCartInfo.orderInfo = {
                id: res[0].id,
                discount_amount: res[0].discount_amount,
                price: res[0].price,
                created_at: res[0].created_at
            }
            $state.go('router.thank-you');
        }).catch((err) => {
            $scope.paymentError = err.data.message;
        });
        // }
    };

    $scope.checkout = function (items) {
        if ($login.checkAuth() === 'authorized') {
            _checkout(items);
        }
    };

    $scope.goToCheckout = function () {
        $state.go('router.cart-checkout');
    };
}
