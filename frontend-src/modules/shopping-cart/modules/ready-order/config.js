import readyOrderCtrl from './controller.js';
import view from './ready-order.html';

readyOrderConfig.$inject = ['$stateProvider'];
export default function readyOrderConfig($stateProvider){
    $stateProvider.state('router.ready-order', {
            url: '/ready-order',
            controller: readyOrderCtrl,
            template:view,
            ncyBreadcrumb: {
                parent:'router.cart-checkout',
                label: 'Ready order'
            }
        })
}
