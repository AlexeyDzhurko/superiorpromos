import './ready-order.scss';
import readyOrderConfig from './config.js';
export default angular.module('readyOrder', [])
    .config(readyOrderConfig)
    .name;