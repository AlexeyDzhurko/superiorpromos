import view from './shopping-cart.html';
import shoppingCartCtrl from './controller';
shoppingCartConfig.$inject = ['$stateProvider'];

export default function shoppingCartConfig($stateProvider){
    $stateProvider
    .state('router.shopping-cart', {
        url: '/shopping-cart',
        controller: shoppingCartCtrl,
        template: view,
        data: {
            requireLogin: true
        },
        ncyBreadcrumb: {
            label: 'Cart'
        }
    })
}