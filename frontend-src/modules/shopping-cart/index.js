import './shopping-cart.scss';

import shoppingCartConfig from './config';
import checkout from './modules/checkout';
import thankYou from './modules/thank-you';
import readyOrder from './modules/ready-order';

export default angular.module('shoppingCartModule', [
    checkout,
    thankYou,
    readyOrder
]).config(shoppingCartConfig).name;
