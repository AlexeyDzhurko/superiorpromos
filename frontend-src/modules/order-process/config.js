import view from './order-process.html';
import orderProcessCtrl from './controller';
orderProcessConfig.$inject = ['$stateProvider'];

export default function orderProcessConfig($stateProvider){
    $stateProvider
    .state('router.order-process', {
        url: '/order-process',
        controller: orderProcessCtrl,
        template: view,
        ncyBreadcrumb: {
            label: 'Order process'
        }
    })
}