import './order-process.scss';

import orderProcessConfig from './config';

export default angular.module('orderProcessModule', [])
	.config(orderProcessConfig)
	.name;
