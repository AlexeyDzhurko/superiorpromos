import view from './site-map.html';
import siteMapCtrl from './controller';
siteMapConfig.$inject = ['$stateProvider'];

export default function siteMapConfig($stateProvider){
    $stateProvider.state('router.site-map', {
        url: '/site-map',
        controller: siteMapCtrl,
        template: view,
        ncyBreadcrumb: {
            label: 'Site Map'
        }
    })
}
