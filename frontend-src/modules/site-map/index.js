import './site-map.scss';
import siteMapConfig from "./config";

export default angular.module('siteMapModule', [])
    .config(siteMapConfig)
    .name;
