siteMapCtrl.$inject = ['$scope', 'apiService'];

export default function siteMapCtrl($scope, apiService){
    $scope.$categories = {};

    apiService.getCategories().then(function (res){
        $scope.$categories = res.data;
    });
}
