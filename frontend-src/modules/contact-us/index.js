import './contact-us.scss';
import contactUsConfig from './config';

export default angular.module('contactUsModule', [])
	.config(contactUsConfig)
	.name;
