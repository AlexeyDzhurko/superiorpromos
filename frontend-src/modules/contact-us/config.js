import view from './contact-us.html';
import contactUsCtrl from './controller';
contactUsConfig.$inject = ['$stateProvider'];

export default function contactUsConfig($stateProvider){
    $stateProvider
        .state('router.contact-us', {
            url: '/contact-us',
            controller: contactUsCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Contact us'
            }
        })
}