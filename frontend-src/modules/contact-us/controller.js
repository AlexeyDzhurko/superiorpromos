contactUsCtrl.$inject = ['$scope', '$http'];

export default function contactUsCtrl($scope, $http) {
    $scope.contactData = {
        email: '',
        full_name: '',
        subject: '',
        message: ''
    };

    $scope.captcha = false;

    $scope.subjects = [
        {
            id: 'Samples',
            name: 'Samples'
        },
        {
            id: 'Artwork',
            name: 'Artwork'
        },
        {
            id: 'Billing',
            name: 'Billing'
        },
        {
            id: 'Orders',
            name: 'Orders'
        },
        {
            id: 'Shipping',
            name: 'Shipping'
        },
        {
            id: 'Comments',
            name: 'Comments'
        },
        {
            id: 'Technical',
            name: 'Technical'
        },
        {
            id: 'Other',
            name: 'Other'
        }
    ];

    $scope.contactUsForm = {};
    $scope.successMessage = false;

    $scope.sendContactUs = function () {
        $http.post('/api/contact-us', $scope.contactData).then(
            function (response) {
                $scope.successMessage = response.data.message;
            }, function (error) {
                for (var key in error.data) {
                    if (error.data.hasOwnProperty(key)) {
                        $scope.contactUsForm[key] = Object.assign(
                            ($scope.contactUsForm[key] || {}),
                            {
                                '$error': {
                                    backend: error.data[key].join(' ')
                                }
                            });
                    }
                }
            }
        );
    }

    $scope.setCaptchaResponse = function (response)
    {
        if (response.length) {
            $scope.captcha = true;
        }
    }

    $scope.setCaptchaExpiration = function ()
    {
        $scope.captcha = false;
    }
}
