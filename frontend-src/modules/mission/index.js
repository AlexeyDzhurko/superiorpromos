import './mission.scss';

import missionConfig from './config';

export default angular.module('missionModule', [])
	.config(missionConfig)
	.name;
