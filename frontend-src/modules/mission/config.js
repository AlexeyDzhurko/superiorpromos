import view from './mission.html';
import missionCtrl from './controller';
missionConfig.$inject = ['$stateProvider'];

export default function missionConfig($stateProvider){
    $stateProvider
        .state('router.mission', {
            url: '/mission',
            controller: missionCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Mission'
            }
        })
}