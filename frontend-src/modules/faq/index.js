import './faq.scss';
import faqConfig from './config';

export default angular.module('faqModule', [])
	.config(faqConfig)
	.name;
