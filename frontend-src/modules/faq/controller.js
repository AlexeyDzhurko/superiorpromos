faqCtrl.$inject = ['$scope', '$staticPages'];
export default function faqCtrl($scope, $staticPages){
    $scope.oneAtATime = false;
    $scope.$staticPages = $staticPages;
}
