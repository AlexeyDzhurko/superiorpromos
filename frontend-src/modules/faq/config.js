import view from './faq.html';
import faqCtrl from './controller';
faqConfig.$inject = ['$stateProvider'];

export default function faqConfig($stateProvider){
    $stateProvider
        .state('router.faq', {
            url: '/faq',
            controller: faqCtrl,
            template: view,
            ncyBreadcrumb: {
                label: 'FAQ'
            }
        })
}