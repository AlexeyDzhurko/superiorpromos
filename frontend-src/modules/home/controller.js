import discountModalView from "../../common/discountModal/discount-modal.html";
import discountModalController from "../../common/discountModal/discount-modal-controller";
import "../../common/discountModal/discount-modal.scss";

homeCtrl.$inject = [ "$scope", "$rootScope", "apiService", "$home", "$stateParams", "cartService", "$state", "appUserService", "customizeOrderService", "$search", "$window", "ModalService", "$timeout" ];

export default function homeCtrl( $scope, $rootScope, apiService, $home, $stateParams, cartService, $state, $appUser, orderService, $search, $window, $modal, $timeout) {
  $scope.$search = $search;
  $scope.$appUser = $appUser;
  $scope.slideTransition = true;
  $scope.carouselInterval = 5000;
  // $scope.promoProducts = [];
  // $scope.promos;
  // $scope.popularCategories;
  $scope.promoProducts = $home.getPromProducts();
  $scope.popularCategories = $home.getPopularCategories();
  $scope.promos = $home.getPromos();

  $scope.$on('broadcastPromProducts', function() {
    $scope.promoProducts = $home.getPromProducts();
  });

  $scope.$on('broadcastPopularCategories', function() {
    $scope.popularCategories = $home.getPopularCategories();
  });

  $scope.$on('broadcastPromos', function() {
    $scope.promos = $home.getPromos();
  });

  $scope.testimonialsInfo = [
    {
      productId: "",
      img: "/img/brands-img/12744.jpg",
      author: "Pete Smith - Bernie's Beach House",
      feedback:
        "We want to thank you for the excellent work, product, price and service you have provided. We are very happy."
    },
    {
      productId: "",
      img: "/img/brands-img/ipm.jpg",
      author: "Karen Heiting - Integrated Project Management Company, Inc.",
      feedback:
        "The imprinted mugs arrived on time, and thanks to your help, everyone is thrilled with them. I appreciate all the effort put forth to get the order taken care of. Thank you sincerely for making this a great experience."
    },
    {
      productId: "",
      img: "/img/brands-img/charity.jpg",
      author: "Michael Campbell, President - Charity Blends Coffee & Tea, Inc.",
      feedback:
        "We received our order today and they are perfect. It was a pleasure to work with you and we look forward to a long working relationship."
    },
    {
      productId: "",
      img: "/img/brands-img/education.jpg",
      author: "Colette B. - Sandia National Laboratory",
      feedback:
        "Got everything as promised. I commend you on your excellent customer service. It's been great doing business with you."
    },
    {
      productId: "",
      img: "/img/brands-img/tampa.jpg",
      author: "Manny P. - Telework Tampa Bay",
      feedback:
        "Just wanted to let you know how happy I am with my order and what a great system you have for placing orders and checking on the status of the order. I will certainly use SuperiorPromos again, and recommend you to my co-workers and others."
    },
    {
      productId: "",
      img: "/img/brands-img/unity.jpg",
      author: "Tania C. - SCFNA Unity Day Subcommittee",
      feedback:
        "We received our cups the other day and they look really great. They exceeded all my expectations. Thanks so much for your fantastic work!"
    }
  ];

  $timeout(() => {
    if (localStorage.getItem("newsletterShown") !== "true") {
      $modal
        .showModal({
          template: discountModalView,
          controller: discountModalController
        })
        .then(modal => {
          modal.close.then(() => {
            localStorage.setItem("newsletterShown", "true");
          });
        });
    }
  }, 5000);

  $scope.testimonialsPosition = 0;
  $scope.testimonialsSlide = function(left) {
    if (
      left &&
      $scope.testimonialsPosition <=
        Math.ceil($scope.testimonialsInfo.length / 2 - 2)
    ) {
      $scope.testimonialsPosition++;
    } else if (!left && $scope.testimonialsPosition > 0) {
      $scope.testimonialsPosition--;
    }
  };

  $scope.addToWishList = function($event, product) {
    $event.stopPropagation();
    cartService.addWishListItem(product.id).then(
      function(success) {
        orderService.animateAddToWishList();
      },
      function(error) {
        console.log(error);
      }
    );
  };

  $scope.placeOrder = function(slug) {
    $state.go("router.customize-order.options", { slug: slug });
  };

  // apiService.getPromoProducts().then(function(response) {
  //   $scope.promoProducts = response.data;
  // });

  // apiService.getPopularCategories().then(function(response) {
  //   $scope.popularCategories = response.data;
  // });

  // apiService.getPromos().then(function(response) {
  //   $scope.promos = response.data;
  // });

  $scope.getWindWidth = function(w) {
    if (w > 1440) {
      $scope.limTo = 5;
    } else if (w > 1100) {
      $scope.limTo = 4;
    } else {
      $scope.limTo = 3;
    }
  };

  $scope.getWindWidth(window.innerWidth);

  $window.onresize = function(e) {
    $scope.getWindWidth(e.target.innerWidth);
  };

  apiService.getSlideImages().then(function(response) {
      $scope.slideImages = response.data;
  });

  $scope.getImageUrl = function (src) {
    if (!src || !src.length) {
      return src;
    }

    if (!src.includes('http', 0)) {
      src = window.location.origin + '/' + src;
    }

    return `/cdn-cgi/image/format=auto,quality=75/${src}`;
  }
}
