import './home.scss';

import homeConfig from './config';

export default angular.module('homeModule', [])
	.config(homeConfig)
	.name;
