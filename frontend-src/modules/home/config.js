import view from './home.html';
import homeCtrl from './controller';

homeConfig.$inject = ['$stateProvider'];

export default function homeConfig($stateProvider){
    $stateProvider
        .state('router.home', {
            url: '/',
            template: view,
            controller: homeCtrl,
            ncyBreadcrumb: {
                label: 'Main'
            },
            onEnter: ['$rootScope', function ($rootScope) {
                $rootScope.$broadcast('$clearCategoriesTree');
            }]
        });
}
