export default function sibs() {
    return {
        link: function(scope, element, attrs) {
            element.bind('click', function() {
                element.parent().children().toggleClass('awards-info-full');
            })
        }
    }
}
