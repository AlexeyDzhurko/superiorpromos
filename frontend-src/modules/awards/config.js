import view from './awards.html';
import awardsCtrl from './controller';
awardsConfig.$inject = ['$stateProvider'];

export default function awardsConfig($stateProvider){
    $stateProvider
        .state('router.awards', {
            url: '/awards',
            controller: awardsCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Awards'
            }
        })
}