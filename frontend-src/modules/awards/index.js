import './awards.scss';

import awardsConfig from './config';
import sibs from './directives.js';

export default angular.module('awardsModule', [])
	.config(awardsConfig)
	.directive('sibs', sibs)
	.name;
