import './welcome-text.scss';

welcomeTextCtrl.$inject = ['$rootScope', '$state', '$timeout', '$window'];

export default function welcomeTextCtrl($scope, $state, $timeout, $window) {
    $scope.clientsList = [
        {
            img: "/img/brands-img/12744.jpg",
        },
        {
            img: "/img/brands-img/ipm.jpg",
        },
        {
            img: "/img/brands-img/charity.jpg",
        },
        {
            img: "/img/brands-img/education.jpg",
        },
        {
            img: "/img/brands-img/tampa.jpg",
        },
        {
            img: "/img/brands-img/unity.jpg",
        },
        {
            img: "/img/brands-img/12744.jpg",
        },
        {
            img: "/img/brands-img/ipm.jpg",
        },
        {
            img: "/img/brands-img/charity.jpg",
        },
        {
            img: "/img/brands-img/education.jpg",
        },
        {
            img: "/img/brands-img/tampa.jpg",
        },
        {
            img: "/img/brands-img/unity.jpg",
        }
    ];

    $timeout(function() {
        $window.dispatchEvent(new Event("resize"));
    }, 100);

    $scope.isVisible = false;
    $scope.$watch('$state.current.name', function (newValue) {
        if(newValue === 'router.home') {
            $scope.isVisible = true;
        } else {
            $scope.isVisible = false;
        }
    });
}
