customizeOrderCtrl.$inject = ['$scope', '$rootScope', 'Carousel', 'ProductItem', 'apiService', 'cartService',
    '$stateParams', 'customizeOrderService', '$state', '$previousState', '$rollBack', 'taOptions', '$galeryOptions',
    '$categoriesData', '$location', '$anchorScroll', '$timeout'];

export default function customizeOrderCtrl($scope, $rootScope, Carousel, ProductItem, apiService, cartService,
                                           $stateParams, orderService, $state, $previousState, $rollBack, taOptions,
                                           $galeryOptions, $categories, $location, $anchorScroll, $timeout){
    var _breadcrumbs = [
            { id: 1, name: 'Home', path: 'router.home'}
        ],
        _createBreadcrumbs = function (breadcrumbs) {
            $scope.breadcrumbs = _breadcrumbs.concat(breadcrumbs);
        },
        _createRelatedProducts = function (catId) {
            var _query = {
                category_id: catId
            };
            apiService.search(_query).then(function (res) {
                if (res && res.data && Array.isArray(res.data.data)) {
                    $scope.relatedProductItems  = res.data.data.slice(0, 9);
                    $scope.initSlider = '/views/partials/relatedItemsSlider/carousel.tpl.html';
                }
            });
        },
        _setImg = function (rawProduct) {
            var colorsImgs = [];
            orderService.productImages = [];
            orderService.productImages = rawProduct.product_extra_images.map(item => ({
                link: item.original,
                thumbnail: item.small,
                medium: item.original, // TODOs
                id: item.id
            }));

            if(rawProduct.product_color_groups[0] && rawProduct.product_color_groups[0].product_colors.length > 0) {
                rawProduct.product_color_groups[0].product_colors.forEach( item => {

                    if(item.image_src)
                        colorsImgs.push( {
                        link: item.image_src,
                        // color_id: item.color_id,
                        // color_name: item.color_name,
                        // color_group_name: item.group_name
                    });
                });
            }

            orderService.productImages = [...orderService.productImages, ...colorsImgs];

            $rootScope.$broadcast('ASG-gallery-edit', {
                id: 'product',
                update: orderService.productImages
            });
        },
        _generateAll = function (product) {
            _setImg(product);
            if (product.breadcrumbs.breadcrumbs && Array.isArray(product.breadcrumbs.breadcrumbs) && product.breadcrumbs.breadcrumbs.length) {
                _createBreadcrumbs(product.breadcrumbs.breadcrumbs);
                _createRelatedProducts(product.breadcrumbs.product_category_id);
            }
        },
        _setColorsActive = function (cartItem) {
            cartItem.orderData.selectedColors = cartItem.colors;
            if(cartItem.allColors.length)
            cartItem.allColors.forEach(function (colorGroup) {
                if(colorGroup.product_colors.length) {
                    colorGroup.product_colors.forEach(function (colorItem) {
                        cartItem.colors.forEach(function (selColor) {
                            if(colorItem.id === selColor.color_id) {
                                colorItem.isActive = true;
                            }
                        });
                    });
                }
            });
        };

    $scope.$on('UPDATE_GALLERY_IMAGE', function (event, color) {
        if (color.image_src && !$scope.orderService.productImages.find(v => v.link && v.link === color.image_src)) {
            let newObj = {
                link: color.image_src
            };
            $scope.orderService.productImages.unshift(newObj);
            $rootScope.$broadcast('ASG-gallery-edit', {
                id: 'product',
                selected: 0,
                update: $scope.orderService.productImages
            });
        }
    });

    $scope.totalUnitPrice = 0;
    $scope.orderService = orderService;
    $scope.$galeryOptions = $galeryOptions;

    $scope.setActiveGalleryImage = function(index) {
        $rootScope.$broadcast('ASG-gallery-edit', {
            id: 'product',
            selected: index
        });
    };

    $rootScope.$on('ASG-change-image-product', function (event, additional) {
        $scope.selectedImage = additional.index;
    });

    $rootScope.$on('ASG-modal-close-product', function (event, additional) {
        $scope.selectedImage = additional.index;
        $scope.showModal = false;
    });

    $scope.downloadImage = function(img) {
        window.open(orderService.productImages[img].link, '_blank');
    };

    $scope.openGallery = function() {
        $scope.showModal = true;
    };

    $scope.scrollToElem = function (event, item) {
        event.preventDefault();
        $location.hash(item);
        $anchorScroll();
    }

    $scope.closeGallery = function () {
        setTimeout(function() {
            angular.element(document.getElementsByClassName("asg-modal")).triggerHandler('click');
        });
    };

    taOptions.toolbar = [
      ['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'justifyLeft', 'justifyCenter', 'justifyRight', 'insertLink']
    ];

    $scope.showModal = false;
    $scope.productImages = [];

    $scope.goToRelatedProduct = function (slug) {
        $state.go('router.customize-order.options', {
            mode: '',
            slug: slug
        }, {
            reload: true
        });
        $categories.getData();
    };

    /*$scope.validateFiles = function () { //to handle selecting more than 3 files, seems like perfectly works without it
        $scope.displayFileUploadError = false;
        if ($scope.orderService.orderItem.orderData.art_files.length > 3) {
            $scope.displayFileUploadError = true;
            $scope.orderService.orderItem.orderData.art_files = null;
        }
    };*/

    $scope.removeSelectedColors = function (productImprints) {
        var orderImprints = $scope.orderService.orderItem.orderData.imprints,
            _clearColors = function (selected, all) {
                for (var i = 0; i<selected.length; i++){
                    for (var j = 0; j<all.length; j++){
                        if (selected[i].id == all[j].id){
                            all.splice(j,1);
                        }
                    }
                }
            };
        for (var key in orderImprints){
            if (orderImprints.hasOwnProperty(key)){
                for (var i = 0; i < productImprints.length; i++){
                    if (key == productImprints[i].id){
                        _clearColors(orderImprints[key].data.colors, productImprints[i].color_group.colors);
                    }
                }
            }
        }
    };

    let relatedItemsBlock = angular.element(document.querySelector('.product-slider'))[0];
    angular.element(window).on('scroll', function() {
        let rect = relatedItemsBlock.getBoundingClientRect();
        if (rect.bottom < 135) {
            $scope.cartStatusFloat = true;
        } else {
            $scope.cartStatusFloat = false;
        }
    });

    $scope.prepareImprints = function () {
        $scope.productImprints = angular.copy($scope.orderService.orderItem.getData().imprints); // productImprints will needed on artwork step
        $scope.removeSelectedColors($scope.productImprints);
    };

    $scope.createProductInstance = function(rawProduct){
        $scope.orderService.orderItem = new ProductItem(rawProduct);
        $scope.prepareImprints();
        $scope.orderService.init($scope.orderService.orderItem);
        _generateAll(rawProduct);

        $timeout(function () {
            $scope.$apply();
        }, 0);
    };

    $scope.createCartItemInstance = function(cartItem){
        $scope.orderService.orderItem = cartItem;
        $scope.prepareImprints();
        $scope.orderService.init($scope.orderService.orderItem);
        _generateAll(cartItem.productData);
        _setColorsActive(cartItem);
    };

    $scope.isItCustomizeOrder = true;
    switch ($stateParams.view) {
        case 'edit-cart-item' : {
            cartService.getCartItemById($stateParams.slug).then($scope.createCartItemInstance);
            break;
        }
        case 'edit-saved-cart-item' : {
            cartService.getSavedCartItemById($stateParams.slug).then($scope.createCartItemInstance);
            break;
        }
        default : {
            apiService.getProductBySlug($stateParams.slug).then(function (res) {
                $rootScope.title = 'Superiorpromos | ' + res.title;
                $scope.createProductInstance(res);
            }) .catch((err) => {
                    $state.go('router.not-found');
                });
            break;
        }
    };

    $rollBack.setCustomizedOrderId($stateParams.orderId);

    $scope.customizeOrderCancel = function (order) {
        order.deleteOrderParams();
        $state.go($previousState.name, $previousState.params, {location:'replace'});
    };

    $scope.checkPrices = function () {
        var selCost = $scope.orderService.orderItem.orderData.selectedCost;
        if (selCost && (+selCost.sale_price || +selCost.regular_price) ) {
            return selCost.sale_price || selCost.regular_price;
        } else {
            let unitPrice = $scope.orderService.orderItem.cart_item_cost? $scope.orderService.orderItem.cart_item_cost.base_item_price : $scope.orderService.orderItem.productData.lowest_price
            return unitPrice;
        }
    };
    
    $scope.imprintColorsCheck = function () {
        if ($scope.orderService.orderItem.orderData.isImprintsBlank) return true;
        var i,
            imps = $scope.orderService.orderItem.orderData.imprints;
        for (i in imps){
            if(imps[i].data && imps[i].data.colors.length) {
                return false;
            }
        };
        return true;
    };

    $scope.$watch('orderService.activeStepIndex', function (data) {
        $scope.activeStepIndex = orderService.getActiveStep();
    });

    $scope.$watchCollection('orderItem.orderData.imprints', function (data) {
        let totalImprints = 0;
        for( let key in data) {
            if(data[key].data && data[key].data.colors.length >= 2) {
                totalImprints += data[key].data.prices.color_item_price * (data[key].data.colors.length - 1);
            }
        }
        $scope.totalUnitPrice = totalImprints;
    });

}
