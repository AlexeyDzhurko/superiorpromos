import './customize-order.scss';

import customizeOrderService from './customize-order-service'
import customizeOrderConfig from './config';

import optionsQuantityStep from './modules/options-quantity-step';
import dateShippingStep from './modules/date-shipping-step';
import imprintStep from './modules/imprint-step';
import orderSample from './modules/sample';
import artworkStep from './modules/artwork-step';
import successSampleModule from './modules/success-sample'

export default angular.module('customizeOrderModule', [
    customizeOrderService,
    optionsQuantityStep,
    dateShippingStep,
    imprintStep,
    orderSample,
    artworkStep,
    successSampleModule
]).config(customizeOrderConfig).name;
