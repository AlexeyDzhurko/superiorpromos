import sampleCtrl from './controller.js';
import view from './sample.html';
sampleConfig.$inject = ['$stateProvider'];
export default function sampleConfig($stateProvider){
    $stateProvider
        .state('router.customize-order.order-sample', {
            url: '/:slug',
            controller: sampleCtrl,
            template: view,
            inherit: true,
            params: {
                slug: { type: "string", raw: true },
                viewMode: null,
            },
            ncyBreadcrumb: {
                label: 'Order Sample'
            }
        })
}
