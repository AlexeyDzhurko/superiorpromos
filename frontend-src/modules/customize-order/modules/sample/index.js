import './sample.scss';
import config from './config.js';
export default angular.module('orderSample', [])
    .config(config)
    .name;