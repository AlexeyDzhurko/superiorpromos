sampleCtrl.$inject = ['$scope', 'apiService', '$state', '$stateParams', 'customizeOrderService', '$sampleInfo'];
export default function sampleCtrl($scope, apiService, $state, $stateParams, orderService, $sampleInfo) {
    $scope.orderService = orderService;
    $scope.additionalInfo = $sampleInfo;

    $scope.$watch('orderItem', function (data) {
        if (data) {
            $scope.searchBar = data.productData.id + ' - ' + data.productData.title;
        }
    });

    $scope.getProduct = function (product) {
        if (product.searchBar && product.searchBar.length > 3) {
            product.data = [];

            let request = {
                id: product.searchBar,
                name: null
            }
            apiService.search(request).then(result => {
                if(result.data.data.length > 0) {
                    product.data.push(result.data.data[0]);
                    product.visible = true;
                }
            });
            // apiService.getProductById(product.searchBar).then(function (res) {
            //     if (res.id) {
            //         product.data.push(res);
            //         product.visible = true;
            //     }
            // });
        }
    }

    $scope.getProductBySlug = function () {
        if ($state.params.slug) {
            apiService.getProductBySlug($stateParams.slug).then($scope.createProductInstance)
                .catch((err) => {
                    $state.go('router.not-found');
                });

        }
    }
    $scope.getProductBySlug();

    $scope.addNewField = function (orderItem) {
        $scope.orderService.orderItem.orderSample.products.push({
            selectedColors: []
        });
    }
    
    $scope.deleteProduct = function (index, source) {
        source.splice(index, 1);
    }

    $scope.setProduct = function (products, item, index) {
        item.searchBar = item.id + ' - ' + item.title;
        item.selectedColors = [];
        _setSampleColorsGrid(item, index);
        products.splice(index, 1, item);
    }

    var _setSampleColorsGrid = function (item) {
        if (item.product_color_groups) {
            item.product_color_groups.forEach(function(colorGroup) {
                angular.forEach(colorGroup.product_colors, function(colorItem){
                    colorItem.color_group_id = colorGroup.id;
                });
            });
        }
    };

    $scope.clearData = function (product) {
        product.visible = false;
    }

    $scope.updateShipMethod = function (orderItem) {
        if (orderItem.orderSample.shipping_method === 'us_shipping') {
            orderItem.orderSample.shipping_data = {};
        }
    }

    $scope.chooseEvent = function (data, source, name) {
        _processArray(source, data, name);
    }

    $scope.chooseEventMultiple = function (data) {
        data.active = !data.active;
        var newAttraction = [];
        $scope.additionalInfo.attraction.forEach(function (item) {
            if (item.active) {
                newAttraction.push(item.title);
            }
        });
        $scope.orderService.orderItem.orderSample.additionalInfo.attraction = newAttraction;
    }

    var _processArray = function (rawArray, data, name) {
        rawArray.forEach(function (item) {
            if (item.id === data.id) {
                item.active = true;
                $scope.orderService.orderItem.orderSample.additionalInfo[name] = data.title;
            } else {
                item.active = false;
            }
        });
    };
}
