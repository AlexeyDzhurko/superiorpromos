imprintStepCtrl.$inject = ['$scope', 'apiService', 'customizeOrderService'];

export default function imprintStepCtrl($scope, apiService, orderService) {
    $scope.Date = Date;
    $scope.orderService = orderService;
    // $scope.orderService.setStep(1);

    if (orderService.orderItem.orderData.isImprintsBlank) {
        $scope.imprintOption = 'blank'
    } else {
        $scope.imprintOption = 'decorated'
    }

    $scope.imprintSelector = {};

    $scope.validate = function () {
        $scope.orderService.validateStep()
    };

    $scope.toggleImprintItemBody = function (id, state) {
        if (typeof state === 'boolean'){
            $scope.imprintSelector[id].expanded = state;

        } else {
            $scope.imprintSelector[id].expanded = !$scope.imprintSelector[id].expanded;

        }
        $scope.validate();
    };

    $scope.onImprintsBlankChange = function (data) {
        $scope.$parent.productImprints = angular.copy($scope.$parent.orderService.orderItem.getData().imprints);
        $scope.$parent.orderService.orderItem.handleImprintsBlankChange();
        for (var key in $scope.imprintSelector){
            if ($scope.imprintSelector.hasOwnProperty(key)){
                $scope.imprintSelector[key].expanded = false;
            }
        }

        if ($scope.imprintOption === 'blank') {
            $scope.orderService.orderItem.orderData.imprints = {};
            data.isImprintsBlank = true;
        } else {
            data.isImprintsBlank = false;
        }

        $scope.validate();
    };

    $scope.toggleColorSelector = function (selector) {
        selector.colorSelectorVisible = !selector.colorSelectorVisible;
        $scope.validate();
    };

    $scope.imprintTogle = function () {
        $scope.validate();
    }

    $scope.checkImprintErrors = function (id, data) {
        var source = data && data.imprintsErrors;
        if(source && source.imprints) {
            for (var i = 0; i < source.imprints.length; i++) {
                if (source.imprints[i] === id
                    && !$scope.orderService.orderItem.orderData.imprints[source.imprints[i]].data.colors.length
                ) {
                    return id;
                }
            }
        } else if (source && source.option) {
            return true;
        }
    };

    $scope.checkQuantityError = function (imprint, data) {
        // if (imprint.selected && data.quantity <= imprint.imprint_prices[0].quantity) {
        //     return imprint.id;
        // }
    }

    $scope.colorStyle = function (color) {
        return {
            'background-color': color
        };
    };

    $scope.deleteAttachedFile = function(index) {
        $scope.orderService.orderItem.orderData.art_files.splice(index, 1);
    };

    $scope.checkFileCountLimit = function() {
        $scope.displayFileUploadError = false;
        if ($scope.orderService.orderItem.orderData.art_files.length > 3) {
            $scope.displayFileUploadError = true;
            $scope.orderService.orderItem.orderData.art_files = $scope.orderService.orderItem.orderData.art_files.slice(0, 3);
        }
    };
}
