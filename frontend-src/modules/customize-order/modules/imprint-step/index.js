import './imprint-step.scss';
import config from './config.js';

export default angular.module('imprintStep', [])
    .config(config)
    .name;
