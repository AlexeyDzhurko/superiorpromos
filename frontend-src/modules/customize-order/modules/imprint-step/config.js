import imprintStepCtrl from './controller.js';
import view from './imprint-step.html';

imprintStepConfig.$inject = ['$stateProvider'];

export default function imprintStepConfig($stateProvider){
    $stateProvider
        .state('router.customize-order.step-2', {
            url: '/:slug?view',
            controller: imprintStepCtrl,
            template: view,
            inherit: true,
            ncyBreadcrumb: {
                label: 'Step 2'
            },
            params: {
                slug: { type: 'string', raw: true }
            },
            onEnter: ['customizeOrderService', function (orderService) {
                orderService.setStep(1);
            }]
        })
}
