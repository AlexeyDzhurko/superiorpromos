import view from './success-sample.view.html';
import successSampleCtrl from './success-sample.controller.js';

successSampleConfig.$inject = ['$stateProvider'];
export default function successSampleConfig($stateProvider){
    $stateProvider
        .state('router.success-sample', {
            url: '/success-sample',
            controller: successSampleCtrl,
            template: view,
            inherit: true,
            params: {
                // slug: { type: "string", raw: true },
                orderItem: null,
                viewMode: null,
            },
            ncyBreadcrumb: {
                label: 'Order Sample'
            }
        })
}
