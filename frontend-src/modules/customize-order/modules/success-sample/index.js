import './success-sample.scss';
import config from './config.js';

export default angular.module('successSampleModule', [])
    .config(config)
    .name;
