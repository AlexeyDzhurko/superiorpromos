import './artwork-step.scss';
import config from './config.js';

export default angular.module('artworkStep', [])
    .config(config)
    .name;
