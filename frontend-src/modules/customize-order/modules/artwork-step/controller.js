artworkCtrl.$inject = ['$scope', 'apiService', 'customizeOrderService', 'FileUploader'];
export default function artworkCtrl($scope, apiService, orderService, FileUploader) {
    $scope.uploader = new FileUploader(
        {
            queueLimit: 3,
            onAfterAddingAll: function (item) {
                $scope.orderService.orderItem.orderData.art_files = item;
            }
        }
    );
    $scope.Date = Date;
    $scope.orderService = orderService;
    $scope.validate = function () {
        $scope.orderService.validateStep()
    };
    $scope.imprintOption = 'decorated';

    $scope.imprintSelector = {};

    $scope.toggleImprintItemBody = function (id, state) {
        if (typeof state === 'boolean'){
            $scope.imprintSelector[id].expanded = state;
        } else {
            $scope.imprintSelector[id].expanded = !$scope.imprintSelector[id].expanded;
        };
        $scope.validate();
    };

    $scope.onImprintsBlankChange = function (data) {
        $scope.$parent.productImprints = angular.copy($scope.$parent.orderService.orderItem.getData().imprints);
        $scope.$parent.orderService.orderItem.handleImprintsBlankChange();
        for (var key in $scope.imprintSelector){
            if ($scope.imprintSelector.hasOwnProperty(key)){
                $scope.imprintSelector[key].expanded = false;
            }
        };
        data.isImprintsBlank = $scope.imprintOption === 'blank';
        $scope.validate();
    };

    $scope.toggleColorSelector = function (selector) {
        selector.colorSelectorVisible = !selector.colorSelectorVisible;
        $scope.validate();
    };

    $scope.checkImprintErrors = function (id, data) {
        var source = data && data.imprintsErrors;
        if(source && source.imprints) {
            for (var i = 0; i < source.imprints.length; i++) {
                if (source.imprints[i] === id) {
                    return id;
                }
            };
        } else if (source && source.option) {
            return true;
        }
    };

    $scope.colorStyle = function (color) {
        return {
            'background-color': color
        };
    };
}
