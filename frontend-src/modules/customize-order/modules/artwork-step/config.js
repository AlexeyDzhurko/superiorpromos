import artworkStepCtrl from './controller.js';
import view from './artwork-step.html';

artworkStepConfig.$inject = ['$stateProvider'];

export default function artworkStepConfig($stateProvider){
    $stateProvider
        .state('router.customize-order.step-3', {
            url: '/:slug?view',
            controller: artworkStepCtrl,
            template: view,
            inherit: true,
            ncyBreadcrumb: {
                label: 'Artwork'
            },
            params: {
                slug: { type: 'string', raw: true }
            },
            onEnter: ['customizeOrderService', function (orderService) {
                orderService.setStep(2);
            }]
        })
}
