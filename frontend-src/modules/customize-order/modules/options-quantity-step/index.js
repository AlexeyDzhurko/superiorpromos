import './options-quantity-step.scss';
import config from './config.js';
export default angular.module('optionsQuantityStep', [])
    .config(config)
    .name;
