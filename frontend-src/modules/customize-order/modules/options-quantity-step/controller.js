import orderDateInfoView from "../../../../common/order-date-info/order-date-info.html";
import orderDateInfoController from "../../../../common/order-date-info/order-date-info-controller";
import "../../../../common/order-date-info/order-date-info.scss";

optionsCtrl.$inject = ['$scope', '$rootScope', 'apiService', '$state', 'customizeOrderService', 'ModalService'];
export default function optionsCtrl($scope, $rootScope,  apiService, $state, orderService, $modal) {
    $scope.orderDateModalShown = false;
    $scope.isOpen = false;
    $scope.options = {
        integer: {
            integer: true
        }
    };

    $scope.orderService = orderService;
    $scope.setQuantity = function(option){
        $scope.orderService.orderItem.selectQuantity(option.quantity);
        document.getElementById('error_quantity').innerHTML = ''
        document.getElementById('qty-selected').classList.add('selected-qty')

        if ($scope.orderWithZip) {
            $scope.updateShipMethod($scope.orderWithZip);
        }
    };
    $scope.setDeliveryDate = function() {
        if($scope.orderService.orderItem.orderData.shipTimeValue) {
            $scope.orderService.orderItem.orderData.shipTime = 'byEndOfTheDay';
        } else {
            $scope.orderService.orderItem.orderData.shipTime = 'regular';
        }
    };

    $scope.setRawQuantity = function (quantity) {
        quantity = parseInt(quantity.replace(/[^0-9]/g,''));
        $scope.orderService.orderItem.orderData.quantity = quantity;
        var option;
        let minItemQuantity = $scope.orderService.orderItem.getData().min_quantity;

        if($scope.orderService.orderItem.orderData.quantity <= minItemQuantity) {
            quantity = minItemQuantity;
        }

        option = $scope.orderService.orderItem.productData.price_grid.filter( item => {
            return item.quantity <= quantity;
        });

        if (option.length > 0) {
            var price =  Object.assign({}, option.pop());
            price.quantity = quantity;
            $scope.orderService.orderItem.selectPrice(price);
        }

        $scope.orderService.orderItem.saveOrderParams();
        if ($scope.orderWithZip) {
            $scope.updateShipMethod($scope.orderWithZip);
        }
    };

    $scope.setColor = function (color) {
        $scope.orderService.orderItem.saveOrderParams();
        $rootScope.$broadcast('UPDATE_GALLERY_IMAGE', color);
        if (Object.keys(color).length) {
            document.getElementById('colors-selector').classList.add('selected')
            document.getElementById('colors-selector').innerHTML = 'Color Selected!'

            document.getElementById('colors-selectors').classList.add('selected')
        }
    };

    $scope.updateSizeBreakdown = function (data) {
        $scope.orderService.orderItem.saveOrderParams();
    };

    $scope.setOptions = function (data) {
        $scope.orderService.orderItem.saveOrderParams();
    };

    $scope.updateShipTime = function (shipTime) {
        $scope.orderService.orderItem.saveOrderParams();
        if (!$scope.orderDateModalShown) {
            $modal.showModal({
                template: orderDateInfoView,
                controller: orderDateInfoController,
            });
            $scope.orderDateModalShown = true;
        }
    };

    $scope.updateShipMethod = function (orderItem) {
      $scope.orderWithZip = orderItem;

      $scope.orderService.orderItem.saveOrderParams();
      orderItem.orderData && orderItem.orderData.shipMethod != 'us_shipping' ? orderItem.orderData.shipData.zipCode = '' : null;

      if( orderItem.orderData
        && orderItem.orderData.shipMethod == 'us_shipping'
        && orderItem.orderData.shipData.zipCode
        && orderItem.orderData.shipData.zipCode.toString().length === 5)
      {
        console.log('test zip zip');

        apiService.getShippingData(orderItem.productData.id, orderItem.orderData.shipData.zipCode, orderItem.orderData.quantity)
          .then(function (res) {
            orderItem.shippingData = res.data;
            $scope.isOptionsVisible = orderService.validateShipping(orderItem.shippingData);
          });
      } else {
        if( orderItem.orderData ) {
          $scope.isOptionsVisible = undefined;
          orderItem.shippingData = [];
          orderItem.orderData.shipData.shippingCode = null;
          orderItem.orderData.shipData.shippingCost = null;
        }
      }
    };

    $scope.switchShipping = function (orderItem) {
        $scope.shippingOptionSelected = true;
      angular.forEach(orderItem.shippingData, function (item) {
        if(item.code == orderItem.orderData.shipData.shippingCode) {
          orderItem.orderData.shipData.shippingCost = item.cost;
        }
      });
      $scope.orderService.orderItem.selectShipping();
    };

    var _clearErrs = function () {
        orderService.errors.options = false;
        orderService.errors.color = false;
        orderService.errors.quantity = false;
        orderService.errors.breakdown = false;
        orderService.errors.shipTime = false;
        orderService.errors.shipAccNumber = false;
        orderService.errors.shipZip = false;
    };

    $scope.toggleChat = function () {
        var frame = document.getElementsByTagName('iframe')[1];
        frame.contentWindow.document.getElementById('tawkchat-minified-container').click();
    };

    $scope.$watch('orderItem.estimZip', function (newItem) {
        if (newItem && $scope.orderService.orderItem.estimCode) {
            $scope.updateShipMethod($scope.orderService.orderItem);
            $scope.orderService.orderItem.orderData.shipData.shippingCode = $scope.orderService.orderItem.estimCode;
        }
    });

    $scope.$on('$destroy', _clearErrs);
}
