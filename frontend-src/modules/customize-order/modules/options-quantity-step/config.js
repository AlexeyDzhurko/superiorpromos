import optionsStepCtrl from './controller.js';
import view from './options-quantity-step.html';

optionsStepConfig.$inject = ['$stateProvider'];

export default function optionsStepConfig($stateProvider){
    $stateProvider
        .state('router.customize-order.options', {
            url: '/:slug?view',
            controller: optionsStepCtrl,
            template: view,
            inherit:true,
            ncyBreadcrumb: {
                label: 'Step 1'
            },
            params: {
                slug: { type: 'string', raw: true },
                view: { type: 'string', raw: true }
            },
            onEnter: ['customizeOrderService', function (orderService) {
                orderService.setStep(0);
            }]
        })
}
