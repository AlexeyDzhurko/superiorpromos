import dateShippingStepCtrl from './controller.js';
import view from './date-shipping-step.html';

dateShippingStepConfig.$inject = ['$stateProvider'];

export default function dateShippingStepConfig($stateProvider){
    $stateProvider
        .state('router.customize-order.step-4', {
            url: '/:slug?view',
            controller: dateShippingStepCtrl,
            template: view,
            inherit: true,
            ncyBreadcrumb: {
                label: 'Step 4'
            },
            params: {
                slug: { type: 'string', raw: true }
            },
            onEnter: ['customizeOrderService', '$transition$', function (orderService, $transition$) {
                orderService.setStep(3);
            }]
        })
}
