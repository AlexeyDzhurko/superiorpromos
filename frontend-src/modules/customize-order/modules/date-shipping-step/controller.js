import orderDateInfoView from "../../../../common/order-date-info/order-date-info.html";
import orderDateInfoController from "../../../../common/order-date-info/order-date-info-controller";
import "../../../../common/order-date-info/order-date-info.scss";

dateShippingStepCtrl.$inject = ['$scope', '$rootScope', 'apiService', '$state', 'customizeOrderService', 'ModalService'];
export default function dateShippingStepCtrl($scope, $rootScope,  apiService, $state, orderService, $modal) {
    $scope.orderDateModalShown = false;
    $scope.isOpen = false;
    $scope.orderService = orderService;
    $scope.today = new Date();
    $scope.minDate = new Date($scope.today.getFullYear(), $scope.today.getMonth(), $scope.today.getDate() + 1);
    // $scope.orderService.setStep(0);

    $scope.$watch('orderService.orderItem.orderData.shipMethod', function (newValue, oldValue, scope) {
        if(newValue === 'custom' && oldValue !== 'custom') {
            var shipMethod = {};
            shipMethod.title = $scope.orderService.orderItem.productData.custom_shipping.method;
            shipMethod.price = $scope.orderService.orderItem.productData.custom_shipping.cost;
            $scope.orderService.orderItem.orderData.shipingMethod = shipMethod;
        }
        if(newValue !== 'custom' && oldValue === 'custom') {
            $scope.orderService.orderItem.orderData.shipingMethod = null;
        }
    });

    $scope.setQuantity = function(option){
        $scope.orderService.orderItem.selectQuantity(option.quantity);
        if ($scope.orderWithZip) {
            $scope.updateShipMethod($scope.orderWithZip);
        }
    };

    $scope.setDeliveryDate = function() {
        if($scope.orderService.orderItem.orderData.shipTimeValue) {
            $scope.orderService.orderItem.orderData.shipTime = 'byEndOfTheDay';
        } else {
            $scope.orderService.orderItem.orderData.shipTime = 'regular';
        }
    };

    $scope.setRawQuantity = function (quantity, minQuantity ) {
        var option;

        if($scope.orderService.orderItem.orderData.quantity <= $scope.orderService.orderItem.getData().min_quantity) {
            quantity = minQuantity
        }

        option = $scope.orderService.orderItem.productData.price_grid.filter( item => {
            return item.quantity <= quantity;
        });

        if (option.length > 0) {
            $scope.orderService.orderItem.selectPrice(option.pop());
        }

        $scope.orderService.orderItem.saveOrderParams();
        if ($scope.orderWithZip) {
            $scope.updateShipMethod($scope.orderWithZip);
        }
    };

    $scope.setColor = function (color) {
        $scope.orderService.orderItem.saveOrderParams();
        $rootScope.$broadcast('UPDATE_GALLERY_IMAGE', color);
    };

    $scope.updateSizeBreakdown = function (data) {
        $scope.orderService.orderItem.saveOrderParams();
    };

    $scope.setOptions = function (data) {
        $scope.orderService.orderItem.saveOrderParams();
    };

    $scope.updateShipTime = function (shipTime) {
        $scope.orderService.orderItem.saveOrderParams();
        if (!$scope.orderDateModalShown) {
            $modal.showModal({
                template: orderDateInfoView,
                controller: orderDateInfoController,
            });
            $scope.orderDateModalShown = true;
        }
    };

    $scope.setCustomShipping = function (item, price) {
        console.log(item, price);
    }

    $scope.updateShipMethod = function (orderItem) {
        $scope.orderWithZip = orderItem;

        if( orderItem.orderData
            && orderItem.orderData.shipMethod == 'us_shipping'
            && orderItem.orderData.shipData.zipCode
            && orderItem.orderData.shipData.zipCode.toString().length === 5) 
        {
            apiService.getShippingData(orderItem.productData.id, orderItem.orderData.shipData.zipCode, orderItem.orderData.quantity)
                .then(function (res) {
                    orderItem.shippingData = res.data;
                    $scope.isOptionsVisible = orderService.validateShipping(orderItem.shippingData);
                });
        } else {
            if( orderItem.orderData ) {
                $scope.isOptionsVisible = undefined;
                orderItem.shippingData = [];
                orderItem.orderData.shipData.shippingCode = null;
                orderItem.orderData.shipData.shippingCost = null;
                orderItem.orderData.shipData.zipCode = null;
            }
        }
        $scope.orderService.orderItem.saveOrderParams();
    };

    $scope.switchShipping = function (orderItem) {
        $scope.shippingOptionSelected = true;
        angular.forEach(orderItem.shippingData, function (item) {
            if(item.code == orderItem.orderData.shipData.shippingCode) {
                orderItem.orderData.shipData.shippingCost = item.cost;
                document.getElementById('shipping-selector').innerHTML = ''
            }
        });
        $scope.orderService.orderItem.selectShipping();
    };

    var _clearErrs = function () {
        orderService.errors.options = false;
        orderService.errors.color = false;
        orderService.errors.quantity = false;
        orderService.errors.breakdown = false;
        orderService.errors.shipTime = false;
        orderService.errors.shipAccNumber = false;
        orderService.errors.shipZip = false;
    };

    $scope.toggleChat = function () {
        var frame = document.getElementsByTagName('iframe')[1];
        frame.contentWindow.document.getElementById('tawkchat-minified-container').click();
    };

    $scope.$watch('orderItem.estimZip', function (newItem) {
        if (newItem && $scope.orderService.orderItem.estimCode) {
            $scope.updateShipMethod($scope.orderService.orderItem);
            $scope.orderService.orderItem.orderData.shipData.shippingCode = $scope.orderService.orderItem.estimCode;
        }
    });

    $scope.$on('$destroy', _clearErrs);
}
