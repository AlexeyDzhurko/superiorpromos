import './date-shipping-step.scss';
import config from './config.js';

export default angular.module('dateShippingStep', [])
    .config(config)
    .name;
