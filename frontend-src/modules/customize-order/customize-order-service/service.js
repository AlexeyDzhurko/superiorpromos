customizeOrderService.$inject = ['$rootScope', 'apiService', '$state', '$stateParams', 'cartService', 'LoginRegistrationService', '$timeout', '$q'];
export default function customizeOrderService($rootScope, apiService, $state, $stateParams, cartService, loginRegistrationService, $timeout, $q) {
    var service = {},
        scrollToError = function (location) {
            for (var i in service.errors) {
                if (service.errors[i] === true) {
                    function findPos(obj) {
                        var curtop = -150;
                        if (obj.offsetParent) {
                            do {
                                curtop += obj.offsetTop;
                            } while (obj = obj.offsetParent);
                        return [curtop];
                        }
                    }
                    window.scroll(-150,findPos(document.getElementById("err")));
                }
            }
        },
        detectTime = function (time) {
            switch (typeof time) {
                case 'string' :
                    return new Date(time).getTime();
                case 'object' :
                    return time.getTime();
                default:
            }
        };
    service.finishStep = false;
    service.errors = {
        quantity: false,
        breakdown: false,
        color: false,
        options: false,
        shipping: null,
        imprints: null,
        shipZip: null,
        zipCode: null,
        productImages: []
    };
    service.selectedProduct = {};
    service.orderItem = {};
    service.carriers = [
        'ups',
        'dhl',
        'fedex',
        'usps',
        'truck'
    ];
    service.shipMethods = [
        'Ground',
        '2 Day Air',
        '3 Day Air',
        'Overnight'
    ];

    service.setStep = function (step) {
        service.activeStepIndex = step;
    };

    service.init = function (product) {
        service.selectedProduct = product;
        service.selectedProduct.getSavedOrderParams();
    };

    service.getSelectedProduct = function () {
        return service.selectedProduct;
    };

    service.steps = [
        {
            id: 1,
            title: 'Product Options and Quantity',
            state: 'router.customize-order.options',
            validate: function (order) {
                if (order.orderData.quantity >= (order.productData.min_quantity || order.productData.price_grid[0].quantity)) {
                    service.errors.quantity = false;
                } else {
                    service.errors.quantity = true;
                }

                if (!order.isLaterBreakdown() && order.orderData.breakdownBySizeQuantities && order.hasSizes()) {
                    var breakdownQuantities = 0;
                    for (var i in order.orderData.breakdownBySizeQuantities) {
                        breakdownQuantities += parseInt(order.orderData.breakdownBySizeQuantities[i]) || 0;
                    }
                    service.errors.breakdown = breakdownQuantities !== order.orderData.quantity;
                } else {
                    service.errors.breakdown = false;
                }

                service.errors.quantity = order.orderData.quantity < order.getData().min_quantity;

                if (order.allColors.length) {
                    service.errors.color = !(order.orderData.selectedColors && order.orderData.selectedColors.length && order.orderData.selectedColors.length == order.allColors.length);
                }

                if (order.productData.product_options.length) {
                    var req_option_count = 0,
                        req_option_current_count = 0;
                    order.orderData && order.orderData.selectedOptions && angular.forEach(order.orderData.selectedOptions, function (el) {
                        if (el.required) req_option_current_count += 1;
                    });
                    angular.forEach(order.productData.product_options, function (el) {
                        if (el.required) req_option_count += 1;
                    });
                    service.errors.options = !(order.orderData.selectedOptions.length && req_option_current_count === req_option_count)
                }

                return service.errors.options
                    || service.errors.color
                    || service.errors.quantity
                    || service.errors.breakdown;
            }
        },
        {
            id: 2,
            title: 'Imprint Options ',
            state: 'router.customize-order.step-2',
            validate: function (order) {
                var _validateSelectedImprints = function (imprints) {
                    var invalidIprints = [], counterImprints = 0;
                    order.orderData.imprintsErrors = {};
                    for (var key in imprints) {
                        if (imprints.hasOwnProperty(key) && imprints[key].selected && !imprints[key].data.colors.length) {
                            invalidIprints.push(+key);
                        } else if (imprints.hasOwnProperty(key) && !imprints[key].selected) {
                            counterImprints += 1;

                        }

                        // Validation of imprint quantity. Commented out at the request of Boris
                        // if (imprints.hasOwnProperty(key)
                        //     && imprints[key].selected
                        //     && (!imprints[key].data.colors.length || imprints[key].data.prices.quantity > order.orderData.quantity)
                        // ) {
                        //     invalidIprints.push(+key);
                        // } else if (imprints.hasOwnProperty(key) && !imprints[key].selected) {
                        //     counterImprints += 1;
                        // }
                        
                    }

                    if (counterImprints === Object.keys(imprints).length && !order.orderData.isImprintsBlank) {
                        order.orderData.imprintsErrors.option = true;
                        return true;
                    } else if (invalidIprints.length > 0) {
                        order.orderData.imprintsErrors.imprints = invalidIprints;
                        return true;
                    }

                    return false;
                };

                if (order.orderData.isImprintsBlank && !Object.keys(order.orderData.imprints).length) {
                    service.errors.imprints = false;
                    order.orderData.imprintsErrors = {};
                } else if (!order.hasImprints()) {
                    service.errors.imprints = false;
                    order.orderData.imprintsErrors = {};
                } else {
                    service.errors.imprints = _validateSelectedImprints(order.orderData.imprints);
                }

                return service.errors['imprints'];
            }
        },
        {
            id: 3,
            title: 'Artwork / Comments',
            state: 'router.customize-order.step-3',
            validate: function (order) {
            }
        },
        {
            id: 4,
            title: 'In Hand Date and Shipping Selection',
            state: 'router.customize-order.step-4',
            validate: function (order) {
                var validateShipTime = function () {
                        var now = new Date(),
                            today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

                        if (order.orderData.shipTime === 'byEndOfTheDay'
                            && order.orderData.shipTimeValue
                            && today.getTime() <= detectTime(order.orderData.shipTimeValue)
                        ) {
                            return true;
                        } else if (order.orderData.shipTime === 'regular') {
                            order.orderData.shipTimeValue = null;
                            return true;
                        }

                        return false;
                    },
                    validateAccountNumber = function () {
                        if (order.orderData.shipMethod === 'own_account') {
                            if (order.orderData.shipData.accountId &&
                                order.orderData.shipData.carrier &&
                                order.orderData.shipData.method) {
                                return true;
                            }
                        } else if (order.orderData.shipMethod === 'us_shipping') {
                            if (order.orderData.shipData.zipCode
                                && order.orderData.shipData.zipCode.length >= 5
                                && order.orderData.shipData.shippingCode) {
                                return true;
                            }
                            return true;
                        } else if (order.orderData.shipMethod === 'custom') {
                            return true;
                        } else {
                            return order.orderData.shipMethod === 'outside_us';
                        }
                    },
                    validateZip = function () {
                        if (order.orderData.shipMethod === 'us_shipping') {
                            return order.orderData.shipData.zipCode
                                && order.orderData.shipData.zipCode.length >= 5
                                && order.orderData.shipData.shippingCode;
                        }
                        return true;
                    };

                service.errors.shipTime = !validateShipTime();
                service.errors.shipAccNumber = !validateAccountNumber();
                service.errors.zipCode = !validateZip();

                return service.errors.shipTime
                    || service.errors.shipAccNumber
                    || service.errors.zipCode;
            }
        },
    ];

    service.goToStep = function (stepId) {
        if (stepId < service.activeStepIndex) {
            service.activeStepIndex = stepId;
            $state.go(service.steps[stepId].state, $state.params);
        } else if (!service.validateStep(service.activeStepIndex)) {
            service.activeStepIndex = stepId;
            $state.go(service.steps[stepId].state, $state.params);
        }
    };

    service.goToBack = function () {
        const stepId = service.activeStepIndex - 1;
        $state.go(service.steps[stepId].state, $state.params)
    };

    service.goNext = function () {
        service.goToStep(service.activeStepIndex + 1);
    };

    service.getActiveStep = function () {
        return service.steps[service.activeStepIndex];
    };

    service.validateStep = function (stepIndex) {
        !stepIndex ? stepIndex = service.activeStepIndex : null;
        return service.steps[stepIndex].validate(service.selectedProduct);
    };

    service.handleProductAddToCart = function (order) {
        if (!service.steps[0].validate(order) && !service.steps[1].validate(order) && !service.steps[3].validate(order)) {
            if (!$stateParams.view || ($stateParams.view == '')) {
                cartService.addCartItem(order.prepareFormData()).then(function () {
                    service.animateShoppingCartPlusOne();
                    $rootScope.$broadcast('updateCoupon');
                    $state.go('router.shopping-cart', null, {location: 'replace'});
                }, function (error) {
                    console.log(error);
                });
            } else if ($stateParams.view && ($stateParams.view == 'edit-cart-item')) {
                cartService.updateCartItem(order.prepareFormData('update'), order.getId()).then(function () {
                    service.animateShoppingCartPlusOne();
                    $rootScope.$broadcast('updateCoupon');
                    $state.go('router.shopping-cart', null, {location: 'replace'});
                }, function (error) {
                    console.log(error);
                });
            } else if ($stateParams.view && ($stateParams.view == 'edit-saved-cart-item')) {
                cartService.updateSavedCartItem(order.prepareFormData('update'), $stateParams.orderId).then(function () {
                    service.animateShoppingCartPlusOne();
                    order.deleteOrderParams();
                    $rootScope.$broadcast('updateCoupon');
                    $state.go('router.my-account.saved-cart', null, {location: 'replace'});
                }, function (error) {
                    console.log(error);
                });
            }
        }
    };

    service.isObjectEmpty = function (obj) {
        for (var i in obj) {
            if (obj.hasOwnProperty(i)) {
                return false;
            }
        }
        return true;
    };

    service.returnFileSize = function (number) {
        if (number < 1024) {
            return number + 'bytes';
        } else if (number > 1024 && number < 1048576) {
            return (number / 1024).toFixed(1) + 'KB';
        } else if (number > 1048576) {
            return (number / 1048576).toFixed(1) + 'MB';
        }
    };

    service.animateShoppingCartPlusOne = function () {
        service.addToCart = true;

        $timeout(function () {
            service.addToCart = false;
        }, 1000);
    };

    service.animateAddToWishList = function (id) {
        var defered = $q.defer();
        service.addToFav = id;

        $timeout(function () {
            service.addToFav = false;
            defered.resolve(true);
        }, 500);

        return defered.promise;
    };

    service.canAddToCart = function () {
        return service.activeStepIndex === 3;
    };

    service.showToBack = function () {
        return service.activeStepIndex !== 0;
    };

    service.submit = function (isOrderSample) {
        if(service.validateStep() && !isOrderSample) {
            scrollToError();
            return false;
        }

        if (this.canAddToCart() && !isOrderSample) {
            this.handleProductAddToCart(this.selectedProduct)
        } else if (!this.canAddToCart() && !isOrderSample) {
            this.goNext();
        } else if (isOrderSample) {
            this.validateSample();
        }
        scrollToError();
    };

    service.validateSample = function () {
        var self = this,
            sample = self.selectedProduct.orderSample,
            info = self.selectedProduct.orderSample.additionalInfo,
            orderSample = {
                products: []
            },
            _checkColorErr = function (sample) {
                sample.colorErrors = false;
                if (sample.products && Array.isArray(sample.products)) {
                    sample.products.forEach(function (item) {
                        if (item.colorErrors) {
                            sample.colorErrors = true;
                        }
                    });
                }
                return sample.colorErrors;
            };

        if (sample.products && Array.isArray(sample.products)) {
            sample.products.forEach(function (item) {
                if (item.product_color_groups && item.product_color_groups.length === item.selectedColors.length) {
                    item.colorErrors = false;
                    orderSample.products.push({
                        "product_id": item.id,
                        "selected_colors": item.selectedColors
                    });
                } else {
                    item.colorErrors = true;
                    return;
                }
            });
        }

        if (sample.shipMethod === 'us_shipping') {
            sample.shipErrors = false;
            orderSample.shipping_method = sample.shipMethod;
        } else {
            var ship = sample.shipping_data;
            if (ship && ship.own_account_number && ship.own_account_number.length > 3 && ship.own_shipping_system && ship.own_shipping_type) {
                sample.shipErrors = false;
                orderSample.shipping_method = sample.shipMethod;
                orderSample.shipping_data = sample.shipping_data;
            } else {
                sample.shipErrors = true;
            }
        }

        if (info.attraction
            && info.attraction.length
            && info.frequency
            && info.quantity
            && info['event_date']
            && info.upcomingEvent
            // && info.sampleComment
        ) {
            sample.infoErrors = false;
            orderSample.additionalInfo = info;
        } else {
            sample.infoErrors = true;
        }

        if (!_checkColorErr(sample) && !sample.shipErrors && !sample.colorErrors && !sample.infoErrors) {
            // var product =  sample.products.shift();
            var product = sample.products[0];
            apiService.orderSample(orderSample)
                .then(function (res) {
                        sample.additionalInfo = {};
                        sample.shipData = {};
                        sample.shipMethod = '';
                        sample.products = [];
                        $state.go('router.success-sample', {orderItem: res.data.id})
                    },
                    function (res) {
                        console.log(res);
                    });
        }
    };

    service.validateShipping = function (arg) {
        if (arg.length && arg[0] && angular.isObject(arg[0])) {
            document.getElementById('zip-input').classList.remove('zip-invalid')
            document.getElementById('zip-input').classList.add('zip-selected')
            return 'options';
        } else if (arg.length && arg[0] && angular.isString(arg[0])) {
            document.getElementById('zip-input').classList.remove('zip-selected')
            document.getElementById('zip-input').classList.add('zip-invalid')
            return 'warn_message';
        } else if (arg.length && arg[0] && arg[0] === false) {
            return 'wrong_zip';
        }
    };

    return service;
}
