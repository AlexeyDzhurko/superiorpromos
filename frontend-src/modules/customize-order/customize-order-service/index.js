import customizeOrderService from './service';

export default angular.module('customizeOrderService', [])
    .factory('customizeOrderService', customizeOrderService)
    .name