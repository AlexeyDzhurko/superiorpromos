/* eslint-disable angular/controller-as-route */
import view from './customize-order.html';
import customizeOrderCtrl from './controller';

customizeOrderConfig.$inject = ['$stateProvider'];

export default function customizeOrderConfig($stateProvider){
    $stateProvider
        .state('router.customize-order', {
            url: ':mode',
            abstract: true,
            params: {
                mode: '',
                orderData: {}
            },
            controller: customizeOrderCtrl,
            template: view,
            reloadOnSearch: false,
            resolve: {
                cartServiceLoaded :['cartService', 'appUserService', function (cartService, appUser) {
                    if (appUser.getUserType() != 'guest'){
                        return cartService.getAll()
                    } else {
                        return cartService.getCart()
                    }
                }],
                $previousState: ["$state", function ($state) {
                    if (!$state.includes('router.customize-order')){
                        return {
                            name: $state.current.name,
                            params: angular.copy($state.params)
                        };
                    } else {
                        return {
                            name: 'router.home',
                            params: null
                        }
                    }
                }]
            },
            ncyBreadcrumb: {
                label: 'Order'
            },
            onExit: ['$rootScope', 'customizeOrderService', function ($rootScope, orderService) {
                if(orderService.selectedProduct) {
                    orderService.selectedProduct;
                }
            }]
        })
}
