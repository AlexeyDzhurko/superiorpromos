import './registration-success.scss';
import registrationSuccessConfig from './config';

export default angular.module('registrationSuccessModule', [])
	.config(registrationSuccessConfig)
	.name;