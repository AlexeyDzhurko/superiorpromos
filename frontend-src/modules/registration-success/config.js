import view from './registration-success.html';
import registrationSuccessCtrl from './controller';
registrationSuccessConfig.$inject = ['$stateProvider'];

export default function registrationSuccessConfig($stateProvider){
    $stateProvider
        .state('router.registration-success', {
            url: '/registration-success',
            controller: registrationSuccessCtrl,
            template: view,
            ncyBreadcrumb: {
                label: 'Registration Success'
            }
        })
}