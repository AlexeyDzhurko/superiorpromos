import searchView from './search.html';
import searchCtrl from './controller';

searchConfig.$inject = ['$stateProvider'];

export default function searchConfig($stateProvider){
    $stateProvider
        .state('router.search', {
            abstract: true,
            controller: searchCtrl,
            reloadOnSearch: false,
            ncyBreadcrumb: {
                label: 'Search'
            }

        })
}
