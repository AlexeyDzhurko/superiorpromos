import './search.scss';

import searchConfig from './config';

import globalSearchModule from './modules/global';
import advancedSearchModule from './modules/advanced';

export default angular.module('searchModule', [
    globalSearchModule,
    advancedSearchModule
]).config(searchConfig)
  .name;
