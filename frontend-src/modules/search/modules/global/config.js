import globalSearchView from './global.html';
import globalSearchCtrl from './controller';

globalSearchConfig.$inject = ['$stateProvider'];

export default function globalSearchConfig($stateProvider){
    $stateProvider
        .state('router.search.global', {
            url: '/search?viewMode',
            controller: globalSearchCtrl,
            template: globalSearchView,
            reloadOnSearch: false,
            params:{
                viewMode: null,
                searchQuery: {}
            },
            ncyBreadcrumb: {
                label: 'Search'
            },
            onEnter: ['$rootScope', function ($rootScope) {
                $rootScope.$broadcast('$clearCategoriesTree')
            }]
        })
}
