import './global.scss';

import globalSearchConfig from './config';

export default angular.module('globalSearchModule', [])
	.config(globalSearchConfig)
	.name;
