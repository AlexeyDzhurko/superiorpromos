globalSearchCtrl.$inject = ['$scope', '$search', '$state', '$transitions', '$rootScope'];
export default function globalSearchCtrl($scope, $search, $state, $transitions, $rootScope){
    $scope.$search = $search;
    // if (!$scope.$search.hasResult()) {
    //     $state.go('router.home');
    // }
    // $scope.$search.setQueryData(null, null, $state.params.slug, null);
    // $scope.$search.find();

    $scope.$on('$destroy', $search.clearQuery);
}
