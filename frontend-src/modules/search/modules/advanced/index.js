import './advanced.scss';

import advancedSearchConfig from './config';

export default angular.module('advancedSearchModule', [])
    .config(advancedSearchConfig)
    .name;
