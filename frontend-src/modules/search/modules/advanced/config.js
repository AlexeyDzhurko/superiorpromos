import advancedSearchView from './advanced.html';
import advancedSearchCtrl from './controller';

advancedSearchConfig.$inject = ['$stateProvider'];

export default function advancedSearchConfig($stateProvider){
    $stateProvider
        .state('router.search.advanced', {
            url: '/search-advanced',
            controller: advancedSearchCtrl,
            template: advancedSearchView,
            reloadOnSearch:false,
            params:{
                viewMode: null,
                searchQuery: {}
            }
        })
}
