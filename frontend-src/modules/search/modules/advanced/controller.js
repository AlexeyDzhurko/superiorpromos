advancedSearchCtrl.$inject = ['$scope', '$search', '$colorsData', '$window', '$timeout', '$state'];

export default function advancedSearchCtrl($scope, $search, $colorsData, $window, $timeout, $state){
    $scope.$search = $search;
    $scope.$colorsData = $colorsData;
    $scope.searchText = '';
    $scope.searchQuery = '';
    $scope.selectedCategory = null;
    $scope.breadcrumbs = [
        { id: 1, name: 'Home', path: 'router.home'},
        { id: 2, name: 'Advanced search', path: 'router.search-advanced'}
    ];

    $scope.refreshSlider = function() {
        $timeout(function() {
            $scope.$broadcast('rzSliderForceRender')
        })
    };
    $scope.refreshSlider();
    $scope.sliderTime = {
        min: 1,
        max: '∞',
        options: {
            showTicks: false,
            hideLimits: true,
            stepsArray: [
                {value: 1},
                {value: 5},
                {value: 10},
                {value: 20},
                {value: 30},
                {value: 60},
                {value: '∞'}
            ],
            onEnd: function (sliderId, modelValue, highValue) {
                $search.query.time_from = modelValue === '∞' ? null : modelValue;
                $search.query.time_to = highValue === '∞' ? null : highValue;
            }
        }
    };
    $scope.sliderPrice = {
        min: 0.1,
        max: '∞',
        options: {
            showTicks: false,
            hideLimits: true,
            stepsArray: [
                {value: 0.1},
                {value: 10},
                {value: 25},
                {value: 100},
                {value: 250},
                {value: 400},
                {value: '∞'}
            ],
            onEnd: function (sliderId, modelValue, highValue) {
                $search.query.price_from = modelValue === '∞' ? null : modelValue;
                $search.query.price_to = highValue === '∞' ? null : highValue;
            }
        }
    };

    $scope.sliderQuantity = {
        min: 1,
        max: '∞',
        options: {
            showTicks: false,
            hideLimits: true,
            stepsArray: [
                {value: 1},
                {value: 50},
                {value: 100},
                {value: 300},
                {value: 500},
                {value: 700},
                {value: '∞'}
            ],
            onEnd: function (sliderId, modelValue, highValue) {
                $search.query.quantity_from = modelValue === '∞' ? null : modelValue;
                $search.query.quantity_to = highValue === '∞' ? null : highValue;
            }
        }
    };
    $scope.setSelectedCategory = function (id) {
        $scope.selectedCategory = id;
    };

    $scope.search = function () {
        $search.setAdvancedQueryData($scope.searchQuery, $scope.selectedCategory);
        $search.find();
        $state.go('router.search.global', {viewMode: 'grid'});
    };

    $scope.goTo = function () {
        $window.history.back();
    };

    $scope.$on('$destroy', $search.clearQuery);
}
