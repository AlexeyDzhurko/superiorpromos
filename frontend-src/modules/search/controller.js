searchCtrl.$inject = ['$scope', '$search', '$state', '$transitions', '$rootScope'];
export default function searchCtrl($scope, $search, $state, $transitions, $rootScope){
    $scope.$search = $search;
    // if (!$scope.$search.hasResult()) {
        // $scope.$search.setQueryData(null, null, $state.params.slug);
        // $scope.$search.find();
    // }
    // $scope.breadcrumbs = [
    //     { id: 1, title: 'Home'},
    //     { id: 18752, title: 'Best sellers'},
    //     { id: 44386, title: 'Auto Accessories'}
    // ];
    $scope.$on('$destroy', $search.clearQuery);
}
