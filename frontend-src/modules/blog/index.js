import './blog.scss';

import blogConfig from './config';

export default angular.module('blogModule', [])
    .config(blogConfig)
    .name;
