import view from './blog.html';
import blogCtrl from './controller';
blogConfig.$inject = ['$stateProvider'];

export default function blogConfig($stateProvider){
    $stateProvider
        .state('router.blog', {
            url: '/blog',
            controller: blogCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Promotional Blog'
            }
        })
}