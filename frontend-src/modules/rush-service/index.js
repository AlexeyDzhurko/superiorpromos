import './rush-service.scss';

import rushServiceConfig from './config';

export default angular.module('rushServiceModule', [])
    .config(rushServiceConfig)
    .name;
