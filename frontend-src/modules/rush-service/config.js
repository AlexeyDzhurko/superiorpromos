import view from './rush-service.html';
import rushServiceCtrl from './controller';
rushServiceConfig.$inject = ['$stateProvider'];

export default function rushServiceConfig($stateProvider){
    $stateProvider
        .state('router.rush-service', {
            url: '/rush-service',
            controller: rushServiceCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Rush Service'
            }
        })
}