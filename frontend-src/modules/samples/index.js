import './samples.scss';

import samplesConfig from './config';

export default angular.module('samplesModule', [])
	.config(samplesConfig)
	.name;
