import view from './samples.html';
import samplesCtrl from './controller';
samplesConfig.$inject = ['$stateProvider'];

export default function samplesConfig($stateProvider){
    $stateProvider
        .state('router.samples', {
            url: '/samples',
            controller: samplesCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Samples'
            }
        })
}