import view from './delivery.html';
import deliveryCtrl from './controller';
deliveryConfig.$inject = ['$stateProvider'];

export default function deliveryConfig($stateProvider){
    $stateProvider
        .state('router.delivery', {
            url: '/delivery',
            controller: deliveryCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Delivery'
            }
        })
}