import './delivery.scss';

import deliveryConfig from './config';

export default angular.module('deliveryModule', [])
	.config(deliveryConfig)
	.name;
