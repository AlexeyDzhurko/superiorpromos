import './not-found.scss';
import notFoundConfig from './config';

export default angular.module('notFoundModule', [])
	.config(notFoundConfig)
	.name;