import view from './not-found.html';
import notFoundCtrl from './controller';
notFoundConfig.$inject = ['$stateProvider'];

export default function notFoundConfig($stateProvider){
    $stateProvider
        .state('router.not-found', {
            url: '/not-found',
            controller: notFoundCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Not Found'
            }
        })
}