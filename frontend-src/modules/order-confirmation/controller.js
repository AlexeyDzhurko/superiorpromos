orderConfirmationCtrl.$inject = ['$scope', 'cartService', 'addressBookService', '$state', '$location', 'apiService', 'customizeOrderService', '$notifications', '$stateParams'];
export default function orderConfirmationCtrl($scope, $cartService, $addressBook, $state, $location, apiService, customizeOrderService, $notifications, $stateParams) {
    //$scope.$cartService = $cartService;
    //$scope.$notifications = $notifications;
    $scope.order = null;
    $scope.$notifications = $notifications;
    if ($stateParams.orderDetails) {
        $scope.order = $stateParams.orderDetails;
        if ($scope.order.order_confirmation.length) {
            $scope.order.order_confirmation[0].order_total = $scope.order.total;
        }
    } else $state.go('router.my-account.order-history');
    $scope.hideCardNumber = function (cardNumber) {
        return cardNumber && '*****' + cardNumber.substr(cardNumber.length-5, cardNumber.length);
    };

}
