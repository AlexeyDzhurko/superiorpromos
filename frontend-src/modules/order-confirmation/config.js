import orderConfirmationLayout from './order-confirmation.html';
import orderConfirmationCtrl from './controller';
orderConfirmationConfig.$inject = ['$stateProvider'];
export default function orderConfirmationConfig($stateProvider){
    $stateProvider
        .state('router.order-confirmation', {
            url: '/order-confirmation',
            template: orderConfirmationLayout,
            controller: orderConfirmationCtrl,
            params: {orderDetails: null},
            ncyBreadcrumb: {
                label: 'Order Сonfirmation'
            }
        })
}