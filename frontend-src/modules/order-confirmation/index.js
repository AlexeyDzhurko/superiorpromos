import './order-confirmation.scss';
import orderConfirmationConfig from './config';

export default angular.module('orderConfirmationModule', [])
	.config(orderConfirmationConfig)
	.name;
