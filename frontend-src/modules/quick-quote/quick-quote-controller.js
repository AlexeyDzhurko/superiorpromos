import productItem from "../../factories/product-item-factory";

quickQuoteController.$inject = ['$scope', 'close', 'source', 'apiService', 'customizeOrderService'];
export default function quickQuoteController($scope, close, source, apiService, orderService) {
    $scope.orderService = orderService;
    $scope.imprintOption = 'decorated';
    $scope.imprintSelector = {};

    $scope.options = {
        numeric: {
            numeral: true
        }
    };

    $scope.source = source.source;
    $scope.sourceCopy = angular.copy(source);
    $scope.productItem = source.productItem;
    $scope.priceData = {};
    $scope.displayMailBlock = false;
    $scope.emailList = '';
    $scope.emailFromName= '';
    $scope.cancel = function () {
        close(null);
    };
    $scope.confirm = function () {
        close(true);
    };
    $scope.shippingData = null;
    $scope.zipCode = null;
    $scope.quantity = null;
    $scope.productItem.orderData.quantity = $scope.productItem.productData.min_quantity;
    $scope.showShippingOptions = null;
    $scope.selectedShipping = null;
    $scope.selectedcolor = null;
    $scope.errors = {};
    $scope.successMessage = {};

    $scope.validate = function() {
        $scope.errors.quantity = $scope.quantity < ($scope.productItem.productData.min_quantity || $scope.productItem.productData.price_grid[0].quantity);

        if ($scope.productItem.allColors.length) {
            $scope.errors.color = !($scope.productItem.orderData.selectedColors && $scope.productItem.orderData.selectedColors.length && $scope.productItem.orderData.selectedColors.length == $scope.productItem.allColors.length);
        }

        if ($scope.productItem.productData.product_options.length) {
            var req_option_count = 0,
                req_option_current_count = 0;
            $scope.productItem.orderData && $scope.productItem.orderData.selectedOptions && angular.forEach($scope.productItem.orderData.selectedOptions, function (el) {
                if(el.required) req_option_current_count += 1;
            });
            angular.forEach($scope.productItem.productData.product_options, function (el) {
                if(el.required) req_option_count += 1;
            });
            $scope.errors.options = !($scope.productItem.orderData.selectedOptions.length && req_option_current_count === req_option_count);
        }

        if(!$scope.selectedShipping) {
            $scope.errors.shipping = true;

        }

        let validateSelectedImprints = function (imprints) {
            var invalidIprints = [], counterImprints = 0;
            $scope.productItem.orderData.imprintsErrors = {};

            for (var key in imprints){
                if (imprints.hasOwnProperty(key) && imprints[key].selected && !imprints[key].data.colors.length){
                    invalidIprints.push(+key);
                } else if (imprints.hasOwnProperty(key) && !imprints[key].selected) {
                    counterImprints += 1;
                }
            };

            if (counterImprints === Object.keys(imprints).length && !$scope.productItem.orderData.isImprintsBlank) {
                $scope.productItem.orderData.imprintsErrors.option = true;
                return true;
            } else if (invalidIprints.length > 0) {
                $scope.productItem.orderData.imprintsErrors.imprints = invalidIprints;
                return true;
            }
            return false;
        };

        if ($scope.productItem.orderData.isImprintsBlank || ($scope.productItem.orderData.imprints.imprints && $scope.productItem.orderData.imprints.imprints.length > 0)){
            $scope.errors.imprints = false;
            $scope.productItem.orderData.imprintsErrors = {};
        } else {
            $scope.errors.imprints = validateSelectedImprints($scope.productItem.orderData.imprints);
        }

        $scope.errors.zipCode = !($scope.zipCode && $scope.zipCode.length >= 5);
        $scope.errors.shipping = !$scope.selectedShipping;


        return $scope.errors.options
            || $scope.errors.color
            || $scope.errors.quantity
            || $scope.errors.zipCode
            || $scope.errors.imprints
    };

    $scope.updateShipMethod = function () {
        $scope.errors.shipping = false;

        
        if ($scope.showShippingOptions) {
            $scope.showShippingOptions = false;
        }

        apiService.getShippingData($scope.productItem._id, $scope.zipCode, $scope.quantity)
            .then(function (res) {
                $scope.shippingData = res.data;
                // $scope.showShippingOptions = true;
                $scope.showShippingOptions = orderService.validateShipping($scope.shippingData);
            });
    };
    
    $scope.switchShipping = function (item, data) {
        $scope.selectedShipping = data.find(v => v.code === item);
    };

    $scope.setQuantity = function (quantity) {
        $scope.quantity = quantity.quantity;
        $scope.productItem.orderData.quantity = quantity.quantity;
        if ($scope.showShippingOptions) {
            $scope.updateShipMethod();
        }
    };

    $scope.setColor = function (color) {
    };

    $scope.updateSizeBreakdown = function (data) {
        $scope.orderItem.saveOrderParams();
    };

    $scope.imprintOption = 'decorated';

    $scope.imprintSelector = {};

    $scope.toggleImprintItemBody = function (id, state) {
        if (typeof state === 'boolean'){
            $scope.imprintSelector[id].expanded = state;
        } else {
            $scope.imprintSelector[id].expanded = !$scope.imprintSelector[id].expanded;
        };
        $scope.validate();
    };

    $scope.onImprintsBlankChange = function (data) {
        $scope.productImprints = angular.copy($scope.productItem.productData.imprints);
        for (var key in $scope.imprintSelector) {
            if ($scope.imprintSelector.hasOwnProperty(key)){
                $scope.imprintSelector[key].expanded = false;
            }
        };
        data.isImprintsBlank = $scope.imprintOption === 'blank';
        console.log($scope.productItem.productData)
    };

    $scope.onImprintsBlankChange($scope.productItem.orderData);

    $scope.toggleColorSelector = function (selector) {
        selector.colorSelectorVisible = !selector.colorSelectorVisible;
        $scope.validate();
    };

    $scope.checkImprintErrors = function (id, data) {
        var source = data && data.imprintsErrors;

        if(source && source.imprints) {
            for (var i = 0; i < source.imprints.length; i++) {
                if (source.imprints[i] === id) {
                    return id;
                }
            };
        } else if (source && source.option) {
            return true;
        }
    };

    $scope.colorStyle = function (color) {
        return {
            'background-color': color
        };
    };

    $scope.transformSelectedImprints = function (orderData) {
        var imprintsArr = [],
            _getColorIds = function (colors) {
                var _colorIds = [];
                if (colors && colors.length) {
                    for (var i = 0; i < colors.length; i++) {
                        _colorIds.push(colors[i].id);
                    }
                }
                return _colorIds;
            };

        for (var key in orderData.imprints){
            if (orderData.imprints.hasOwnProperty(key) && orderData.imprints[key].selected){
                imprintsArr.push({
                    imprint_id: key ,
                    color_ids: _getColorIds(orderData.imprints[key].data.colors)
                });
            }
        }
        return imprintsArr;
    };

    $scope.transformProductOptions = function (options) {
        var optionsArr = [];

        for (var i = 0; i < options.length; i++){
            optionsArr.push({
                product_option_id: options[i].id,
                product_sub_option_ids: [options[i].chosen_sub_option.id]
            });
        }

        return optionsArr;
    };


    $scope.generateQuote = function(pdf) {
        $scope.clearErrors();
        if (!$scope.validate()) {
            const data = {
                product_id: $scope.productItem._id,
                quantity: $scope.quantity,
                product_colors: $scope.productItem.orderData.selectedColors,
                product_options: $scope.transformProductOptions($scope.productItem.orderData.selectedOptions),
                product_imprints: $scope.transformSelectedImprints($scope.productItem.orderData),
                product_shipping: $scope.selectedShipping,
                download_pdf: pdf
            };
            apiService.generateQuickQuote(data).then(response => {
                if (pdf) {
                    window.open(`${response.data.url}`, '_blank');
                } else {
                    $scope.report = response.data;
                }
            })
        } else console.log($scope.errors);
    };

    $scope.clearErrors = function() {
        for(let key in $scope.errors) {
            if ($scope.errors.hasOwnProperty(key)) {
                $scope.errors[key] = null;
            }
        }
    };

    $scope.reset = function() {
        $scope.source = $scope.sourceCopy.source;
        $scope.productItem = $scope.sourceCopy.productItem;
        $scope.priceData = {};
        $scope.shippingData = null;
        $scope.zipCode = null;
        $scope.quantity = $scope.productItem.productData.min_quantity;
        $scope.productItem.orderData.quantity = $scope.productItem.productData.min_quantity;
        $scope.showShippingOptions = null;
        $scope.selectedShipping = null;
        $scope.selectedcolor = null;
        $scope.errors = {};
        $scope.successMessage = {};
        $scope.report = null;
        $scope.displayMailBlock = false;
        $scope.emailList = '';
        $scope.emailFromName= '';
    };

    $scope.toggleMailBlock = function () {
        $scope.displayMailBlock = !$scope.displayMailBlock;
    };

    $scope.sendEmail = function () {
        const data = $scope.report;
        data.user = {
            'name' : $scope.emailFromName,
            'emails' : $scope.emailList.split(',')
        };

        apiService.sendQuickQUteByEmail(data).then( res => {
            $scope.successMessage.email = res.data.message;
        }, err => {
            $scope.errors.email = err;
        });
    }

}
