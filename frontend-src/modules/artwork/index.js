import './artwork.scss';
import artworkConfig from './config';
export default angular.module('artWorkModule', [])
	.config(artworkConfig)
	.name;
