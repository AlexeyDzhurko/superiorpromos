import view from './artwork.html';
import artworkCtrl from './controller';
artworkConfig.$inject = ['$stateProvider'];

export default function artworkConfig($stateProvider){
    $stateProvider
        .state('router.artwork', {
            url: '/artwork',
            controller: artworkCtrl,
            template:view,
            ncyBreadcrumb: {
                label: 'Artwork'
            }
        })
}