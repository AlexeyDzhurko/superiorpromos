/*import styles*/
import "./scss/style.scss";

/*import vendors*/
// import "../node_modules/@fortawesome/fontawesome-free/css/all.min.css";
// import "../node_modules/@fortawesome/fontawesome-free/css/brands.min.css";
import "../node_modules/@fortawesome/fontawesome-free/css/fontawesome.min.css";
// import "../node_modules/@fortawesome/fontawesome-free/css/regular.min.css";
import "../node_modules/@fortawesome/fontawesome-free/css/solid.min.css";
// import "../node_modules/@fortawesome/fontawesome-free/css/svg-with-js.min.css";

/*import modules*/
import appControllersModule from "./controllers";
import appDirectivesModule from "./directives";
import appConstantsModule from "./constants";
import appFactoriesModule from "./factories";
import appFiltersModule from "./filters";
import appServicesModule from "./services";

import aboutModule from "./modules/about";
import termsConditionsModule from "./modules/terms-and-conditions";
import artWorkModule from "./modules/artwork";
import awardsModule from "./modules/awards";
import blogModule from "./modules/blog";
import caseStudiesModule from "./modules/case-studies";
import contactUsModule from "./modules/contact-us";
import notFoundModule from './modules/not-found';
import registrationSuccessModule from './modules/registration-success';
import customizeOrderModule from "./modules/customize-order";
import deliveryModule from "./modules/delivery";
import faqModule from "./modules/faq";
import glossaryModule from "./modules/glossary";
import guaranteeModule from "./modules/guarantee";
import homeModule from "./modules/home";
import missionModule from "./modules/mission";
import myAccountModule from "./modules/my-account";
import orderConfirmationModule from "./modules/order-confirmation";
import orderProcessModule from "./modules/order-process";
import promotionModule from "./modules/promotion";
import rushServiceModule from "./modules/rush-service";
import samplesModule from "./modules/samples";
import searchModule from "./modules/search";
import shoppingCartModule from "./modules/shopping-cart";
import categoriesModule from "./modules/categories";
import siteMapModule from "./modules/site-map";

import httpLoadingIndicatorModule from "./common/http-loading-indicator";

import run from "./run";
import config from "./config";

var superior = angular.module("superiorApp", [
  "ui.router",
  "ngAnimate",
  "ui.bootstrap",
  "rzModule",
  "ui.select",
  "ngMaterial",
  "720kb.tooltips",
  "ui.mask",
  "LocalStorageModule",
  "ngMessages",
  "angularModalService",
  "ncy-angular-breadcrumb",
  "ui.tree",
  "textAngular",
  "ngTouch",
  "angularSuperGallery",
  "ui.carousel",
  "cleave.js",
  "angularFileUpload",
  "angularLazyImg",
  'vcRecaptcha',
  'ngSanitize',
  'ngCookies',
  appControllersModule,
  appDirectivesModule,
  appConstantsModule,
  appFactoriesModule,
  appFiltersModule,
  appServicesModule,
  httpLoadingIndicatorModule,
  aboutModule,
  termsConditionsModule,
  artWorkModule,
  awardsModule,
  blogModule,
  caseStudiesModule,
  contactUsModule,
  notFoundModule,
  registrationSuccessModule,
  customizeOrderModule,
  deliveryModule,
  faqModule,
  glossaryModule,
  guaranteeModule,
  homeModule,
  missionModule,
  myAccountModule,
  orderConfirmationModule,
  orderProcessModule,
  promotionModule,
  rushServiceModule,
  samplesModule,
  searchModule,
  shoppingCartModule,
  categoriesModule,
  siteMapModule
]);

superior.config(config).run(run);
