import registrationCtrl from "./registration";

export default angular.module('appControllersModule', [])
    .controller('RegistrationController', registrationCtrl)
    .name;
