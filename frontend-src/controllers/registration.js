registrationCtrl.$inject = [
  'appUserService',
  '$scope',
  'LoginRegistrationService',
  '$http',
  '$state',
  '$rootScope',
  'FlashMessagesService',
  'cartService',
  '$countriesList',
  '$usStates',
  'paymentProfileService',
  'addressBookService',
  'artProofService',
    'indicatorStatusService',
    '$window',
    '$cookies'
];

export default function registrationCtrl(
    $appUser, $scope, loginRegistrationService, $http, $state, $rootScope,
    $flashMessage, cartService, $countriesList, $usStates, $paymentProfile,
    $addressBook, $artProofs, $indicatorStatusService, $window, $cookies) {
  $scope.loginRegistrationService = loginRegistrationService;
  $scope.flashMessage = $flashMessage;
  $scope.$appUser = $appUser;
  $scope.$countriesList = $countriesList;
  $scope.$usStates = $usStates;

  $scope.setData = function() {
    $scope.dataSend = {};
    $scope.dataSend.users = {};
    $scope.dataSend.users.shipping = {};
    $scope.dataSend.users.billing = {};
    $scope.billing = {
      address: 'same_as_shipping',
    };
    $scope.dataSend.users.shipping.country = 'US';
    $scope.dataSend.users.billing.country = 'US';
  };
  $scope.setData();

  $scope.registerUser = function() {
    $flashMessage.clearAll();

    if ($scope.dataSend.users.shipping.country !== 'US') {
      $scope.dataSend.users.shipping.state = '';
    }

    switch ($scope.billing.address) {
      case 'same_as_shipping' :
        $scope.dataSend.users.billing = {
          address: 'same_as_shipping',
        };
        break;
      case 'new_address' :
        $scope.dataSend.users.billing.address = 'new_address';
        if ($scope.dataSend.users.billing.country !== 'US') {
          $scope.dataSend.users.billing.state = '';
        }
        break;
    }
    $indicatorStatusService.setStatus(true);
    $http.post('/api/registration', $scope.dataSend).then(function(response) {
      $flashMessage.setSuccessMessage('Registration successful!');

      $appUser.setToken(response);
      $appUser.getUserAccount().then(function () {
        cartService.getAll().then(function () {

          $addressBook.getAddressBook();
          $paymentProfile.getPaymentProfilesList();
          $artProofs.getArtProofs();
          $scope.setData();

          $indicatorStatusService.setStatus(false);

          $scope.toggleAll();
          $cookies.remove('cart_id');
          $window.localStorage.removeItem('redirectingRout');

          if ($state.current.name === 'router.shopping-cart') {
            $state.go('router.cart-checkout');
          }
          location.reload()
        });
      });
    }, function(response) {
      for (var field in response.data) {
        if (response.data[field].length > 0) {
          var formFieldName = field.replace('users.', '');
          $scope.ctrl.userForm[formFieldName].$setValidity(formFieldName,
              false);
          $scope.ctrl.userForm[formFieldName].$error.errorMessage = response.data[field][0];
          $indicatorStatusService.setStatus(false);
        }
      }
    }, function(err) {
      toastr.success(err);
      $indicatorStatusService.setStatus(false);
    });
  };

  $scope.resetError = function(field) {
    field.$error = {};
  };

  $scope.resetData = {};
  $scope.resetData.users = {};
  $scope.forgotPassword = function() {
    $flashMessage.clearAll();
    $http.post('/api/password/reset', $scope.resetData.users).
        then(function(response) {
          $scope.loginRegistrationService.goToStep('report');
        }, function(response) {
          for (var i in response.data) {
            $flashMessage.setErrorMessage(response.data[i][0]);
            break;
          }
        });
  };

    $scope.authorizationError;
    $scope.loginData = {};

  $scope.login = function () {
    $scope.authorizationError = false;
    $flashMessage.clearAll();
    $appUser.login($scope.loginData).then(function (success) {
      angular.element(document.body).removeClass('overflow-hidden');
      angular.element(document.querySelector('.fixed-wrap')).addClass('headroom');
      angular.element(document.querySelector('header')).removeClass('open-login');
      $cookies.remove('cart_id');
      $appUser.showLoginModal = false;
    }, function (data) {
      $scope.authorizationError = true;
      if (data.data && data.data.error) {
        $flashMessage.setErrorMessage(data.data.error);
      }
    });

    if ($state.current.name === 'router.shopping-cart') {
      $state.go('router.cart-checkout');
    } else {
      let redirectingRout = JSON.parse(localStorage.getItem('redirectingRout'));

      if (redirectingRout) {
        $state.go(redirectingRout.state, {slug: redirectingRout.slug});
        $rootScope.activated = 2;
      }
    }

    localStorage.removeItem('redirectingRout');
  };

  $scope.goToLogin = function() {
    $flashMessage.clearAll();
    $scope.loginRegistrationService.goToStep('login');
  };

  $scope.isExistingEmail = false;
  $scope.moveToScreen = async function(arg, e) {
    e.preventDefault();

    if ($scope.reg_screen === 'general-info') {
      $scope.isExistingEmail = false;
      $appUser.checkEmail($scope.dataSend.users.email).then((res) => {
        if (!res.data.success) {
          $scope.isExistingEmail = true;
          return;
        }
        $scope.changeScreen(arg);
      });
    } else {
      $scope.changeScreen(arg);
    }
  };

  $scope.changeScreen = function(arg) {
    $scope.reg_screen = arg;
    switch (arg) {
      case 'general-info' :
        $scope.sub_header_text = 'General information';
        break;
      case 'shipping-info' :
        $scope.sub_header_text = 'Shipping information';
        break;
      case 'billing-info' :
        $scope.sub_header_text = 'Billing information';
        break;
    }
  };

  $scope.goToRegistration = function(event) {
    $flashMessage.clearAll();
    $scope.loginRegistrationService.goToStep('registration');
    $scope.reg_screen = 'general-info';
    $scope.sub_header_text = 'General information';
  };

  $scope.testPattern = function(arg) {
    /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test(
        arg) ? $scope.passpattern = false : $scope.passpattern = true;
  };

};
