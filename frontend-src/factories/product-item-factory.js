

productItem.$inject = ['localStorageService', 'LoginRegistrationService', 'customizeOrderService'];

export default function productItem($localStorage, $login, orderService) {
    var item = function (product) {
        if(product) {
        //   var self = this;
          this.initOrderVariables();
          this.setId(product.id);
          this.setData(product);
          this.setAllColorsGrid();
          this.setSizeGroupId();
          if(product.price_grid && product.price_grid.length){
            var minPriceValue = Math.min.apply(Math, product.price_grid.map(function(o){return parseFloat(o.sale_price);}));
            var minQquantityValue = Math.min.apply(Math, product.price_grid.map(function(o){return parseFloat(o.quantity);}));

            // if (product.lowest_price){
            //   product.lowest_price = (parseFloat(product.lowest_price) > minPriceValue) ? minPriceValue : product.lowest_price;
            // } else {
            //   product.lowest_price = minPriceValue;
            // }

            if (product.min_quantity){
              product.min_quantity = product.min_quantity > minQquantityValue ? minQquantityValue : product.min_quantity;
            } else {
              product.min_quantity = minQquantityValue;
            }
          }
        }

    };

    item.prototype.setSizeGroupId = function () {
        if (this.hasSizes()){
            this.orderData.sizeGroupId = this.getData().size_group.id;
        }
    };

    item.prototype.initOrderVariables = function () {
        this.$localStorage = $localStorage;
        this.allColors = [];
        this.orderData = {
            quantity: null,
            selectedColor : null,
            imprinted: [],
            selectedColors : [],
            breakdownBySizeQuantities : {},
            laterBreakdown : 0,
            sizeGroupId : null,
            isImprintsBlank : false,
            imprints : {},
            imprintArtFile : null,
            imprintComment : '',
            artworkCost : 0,
            shipMethod : 'us_shipping',
            shipData : {},
            zipOptionShipCost : null,
            shipTime : 'regular',
            shipTimeValue : null,
            taxExemption : false,
            selectedOptions : [],
            selectedCost : {},
            reviews: {},
            imprintsToggle: false,
        };
        this.orderSample = {
            products: [],
            shipMethod: 'us_shipping',
            shipData: {},
            additionalInfo: {}
        }
    };

    item.prototype.setAllColorsGrid = function () {
        var self = this;
        this.allColors = [];
        if (this.productData.product_color_groups) {
            this.productData.product_color_groups.forEach(function(colorGroup) {
                angular.forEach(colorGroup.product_colors, function(colorItem){
                    colorItem.color_group_id = colorGroup.id;
                });
            });
            self.allColors = angular.copy(this.productData.product_color_groups);
        }
    };

    item.prototype.selectColor = function (color, colorGroupName) {
        var self = this,
            index = -1,
            pushToArray = function (color_group_id, color_id, color_name, group_name) {
                self.orderData.selectedColors.push(
                    {
                        color_group_id: color_group_id,
                        color_id: color_id,
                        color_name: color_name,
                        color_group_name: group_name
                    }
                );
        }

        if(this.orderData.selectedColors && this.orderData.selectedColors.length) {
            angular.forEach(this.orderData.selectedColors, function (item, ind) {
                if (item.color_group_id === color.color_group_id) {
                    index = ind;
                }
            });
        }

        if(index > -1) {
            this.orderData.selectedColors.splice(index, 1);
            pushToArray(color.color_group_id, color.id, color.color.title, colorGroupName);
        } else {
            pushToArray(color.color_group_id, color.id, color.color.title, colorGroupName);
        }
        this.saveOrderParams();
    };

    item.prototype.selectColorSample = function (color, itemIndex) {
        var self = this,
            index = -1,
            pushToArray = function (color_group_id, color_id) {
                self.orderSample.products[itemIndex].selectedColors.push({color_group_id: color_group_id, color_id: color_id});
            }

        if(this.orderSample.products[itemIndex].selectedColors && this.orderSample.products[itemIndex].selectedColors.length) {
            angular.forEach(this.orderSample.products[itemIndex].selectedColors, function (item, ind) {
                if (item.color_group_id === color.color_group_id) {
                    index = ind;
                }
            });
        }

        if(index > -1) {
            this.orderSample.products[itemIndex].selectedColors.splice(index, 1);
            pushToArray(color.color_group_id, color.id);
        } else {
            pushToArray(color.color_group_id, color.id);
        }
    }

    item.prototype.selectColorById = function (colorId) {
        var self = this;
        this.allColors.forEach(function (color) {
            color.id == colorId ? self.orderData.selectedColor = color : null;
        });
        this.saveOrderParams();
    };

    item.prototype.selectQuantity = function (quantity) {
        this.orderData.quantity = parseInt(quantity);
        this.saveOrderParams();
    };

    item.prototype.selectPrice = function (price) {
        this.orderData.selectedCost = price;
        this.saveOrderParams();
    };

    item.prototype.getSelectedPriceGrid = function () {
        var priceGrid = this.getData().price_grid;

        for (var i in priceGrid) {
            if (this.orderData.quantity < priceGrid[i].quantity) {
                // modified
                if (i == 0) {
                    return priceGrid[i];
                }
                // modified
                return priceGrid[i - 1] != undefined ? priceGrid[i - 1] : null;
            }
        }

        return priceGrid[priceGrid.length - 1];
    };

    item.prototype.sum = function (list) {
        var total = 0;
        angular.forEach(list , function(item){
            total += parseInt(item) || 0;
        });
        return total;
    };

    item.prototype.getNextPriceGrid = function () {
        var priceGrid = this.getData().price_grid;
        var nextPriceGridIndex = 0;
        for (var i = 0; i < priceGrid.length; i++) {
            nextPriceGridIndex = i;

            if (this.orderData.quantity > priceGrid[i].quantity) {
                continue;
            }
            else if (this.orderData.quantity == priceGrid[i].quantity) {
                nextPriceGridIndex++;
            }

            break;
        }

        return priceGrid[nextPriceGridIndex];
    };

    item.prototype.getSelectedPrice = function () {
        var priceGrid = this.getSelectedPriceGrid();
        if (priceGrid) {
            return parseFloat(priceGrid.sale_price) || parseFloat(priceGrid.regular_price);
        }
    };

    item.prototype.getChosenSubOptionsPrice = function () {
        var sub_options_total_price = 0;
        angular.forEach(this.orderData.selectedOptions, function (option_item) {
            angular.forEach(option_item.product_sub_options, function (sub_option_item) {
                if(option_item.chosen_sub_option.id === sub_option_item.id){
                    angular.forEach(sub_option_item.product_sub_option_prices, function (sub_option_price_item) {
                        sub_options_total_price += parseFloat(sub_option_price_item.item_price)*100;
                    });
                }
            });
        });
        return sub_options_total_price/100;
    };

    item.prototype.getChosenSubOptionsName = function () {
        var names = '';
        angular.forEach(this.orderData.selectedOptions, function (option_item) {
            names += option_item.name;
        });
        return names;
    };

    item.prototype.getShippingPrice = function () {
        return this.orderData.shipData.shippingCost;
    };

    item.prototype.getSetupPrice = function () {
        var priceGrid = this.getSelectedPriceGrid();
        if (priceGrid) {
            return parseFloat(priceGrid.setup_price || 0);
        }
    };

    item.prototype.getShippingMethodPrice = function () {
        return parseFloat(this.orderData.shipingMethod ? this.orderData.shipingMethod.price : 0);
    };

    item.prototype.getTotalPrice = function () {
        var zipOptionShipCost = this.orderData.zipOptionShipCost ? parseFloat(this.orderData.zipOptionShipCost) : 0,
            imprintsTotalPrice = this.getSelectedImprintsTotalPrice();
            
        return ( ( this.getSelectedPrice() + this.getChosenSubOptionsPrice() || 0 ) * parseFloat(this.orderData.quantity))
            + this.getSetupPrice()
            + this.getShippingMethodPrice()
            + (this.getShippingPrice() || 0)
            + zipOptionShipCost
            + imprintsTotalPrice;
    };

    item.prototype.getTotalGridPrice = function () {
        return (this.getSelectedPrice() * this.orderData.quantity);
    };

    item.prototype.isLaterBreakdown = function () {
        return Boolean(this.orderData.laterBreakdown);  //true means later breakdown false means breakdown now
    };

    item.prototype.hasSizes = function () {
        return this.getData().size_group && this.getData().size_group.sizes && this.getData().size_group.sizes.length > 0;
    };

    item.prototype.hasImprints = function () {
        return this.getData().imprints && this.getData().imprints.length > 0;
    };

    item.prototype.handleImprintsBlankChange = function(){
        if (this.orderData.isImprintsBlank){
            this.saveOrderParams();
        }
    };

    item.prototype.selectImprint = function (imprint) {
        var _this = this, 
        _getSelectedPrices = function (prices) {
            if (prices && prices.length){
                for (var i=0; i< prices.length; i++){
                    // if (_this.orderData.quantity <= prices[i].quantity){
                        return prices[i]
                    // }
                }
            } else {
                return null;
            }
        },
        _revertSelectedColors = function (data, imprint) {
            for (var i = 0; i< data.length; i++){
                imprint.color_group.colors.push(data[i]);
            }
        };
        if (this.orderData.imprints[imprint.id].selected){
            this.orderData.imprints[imprint.id].data = {
                id: imprint.id,
                name: imprint.name,
                prices: _getSelectedPrices(imprint.imprint_prices),
                max_colors: imprint.max_colors,
                colors:[]
            };
        } else {
            _revertSelectedColors(this.orderData.imprints[imprint.id].data.colors, imprint);
            this.orderData.imprints[imprint.id].data = null;
        }
        this.saveOrderParams();
    };

    item.prototype.isImprintMaxColorsSelected = function(selectedId, maxColors) {
        return this.orderData.imprints[selectedId] &&
            this.orderData.imprints[selectedId].data &&
            this.orderData.imprints[selectedId].data.colors &&
            this.orderData.imprints[selectedId].data.colors.length == maxColors;
    };

    item.prototype.replaceImprintColor = function (imprint, color, index) {
        var selectedColors = this.orderData.imprints[imprint.id].data.colors;
        selectedColors.splice(index, 1, color);
        this.saveOrderParams();
    };

    item.prototype.addImprintColor = function(imprint, color){
        if (!this.orderData.imprints[imprint.id]){
            this.orderData.imprints[imprint.id] = {
                selected: true
            };
            this.selectImprint(imprint);
        } else if (!this.orderData.imprints[imprint.id].data) {
            this.orderData.imprints[imprint.id].selected = true;
            this.selectImprint(imprint);
        }
        if(color) {
            this.orderData.imprints[imprint.id].data.colors.push(color);
        }
        this.orderData.imprints[imprint.id].chosen = null;

        const imprintsData = angular.copy(this.orderData.imprints);
        this.orderData.imprints = imprintsData;
        this.saveOrderParams();
    };

    item.prototype.removeImprintColor = function(imprint, color){
        var _getDeleteIndex = function (data, id) {
                for (var i = 0; i< data.length; i++){
                    if (id == data[i].id){
                        return i;
                    }
                }
            },
            selectedColors = this.orderData.imprints[imprint.id].data.colors;
        selectedColors.splice(_getDeleteIndex(selectedColors, color.id), 1);
        const imprintsData = angular.copy(this.orderData.imprints);
        this.orderData.imprints = imprintsData;
        this.saveOrderParams();
    };

    item.prototype.getSelectedImprintsTotalPrice = function () {
        var imprintsPriceTotal = 0,
            _imprints = this.orderData.imprints;
        for (var key in _imprints) {
            if (_imprints.hasOwnProperty(key) && _imprints[key].selected && _imprints[key].data.prices) {
                var colorSetupPrice = +_imprints[key].data.prices.color_setup_price;
                var setupPrise = +_imprints[key].data.prices.setup_price;
                var pricePerProduct = +_imprints[key].data.prices.item_price;
                var priceOfColors = (_imprints[key].data.colors.length >= 2) ? (_imprints[key].data.colors.length - 1) * _imprints[key].data.prices.color_item_price : 0;

                /*
                    imprintUnitPrice = sum of:
                    1) priceOfColors = all selected colors for imprint location ( 1 color free by default ) multiplied to color item price (property which set at admin panel);
                    2) pricePerProduct - price of item (property which set at admin panel)
                * */
                var imprintUnitPrice = priceOfColors + pricePerProduct;
                var imprintProductPrice = this.orderData.quantity * imprintUnitPrice;
                var totalColorsSetupPrice = (_imprints[key].data.colors.length >= 2)? (_imprints[key].data.colors.length - 1) * colorSetupPrice : 0;


                /*
                    imprintsPriceTotal = sum of:
                    1) imprintProductPrice - total price of all imprint property for one item
                    2) setupPrise - property which set at admin panel
                    3) totalColorsSetupPrice = all selected colors for imprint location ( 1 color free by default ) multiplied to color setup price (property which set at admin panel);
                 **/
                imprintsPriceTotal += imprintProductPrice + setupPrise + totalColorsSetupPrice;
            }
        }

        return imprintsPriceTotal;
    };

    item.prototype.hasProductOptions = function () {
        return this.getData().product_options && this.getData().product_options.length > 0;
    };

    item.prototype.setId = function(id) {
        if (id)  {
            this._id = id;
        }
    };

    item.prototype.getId = function(){
        return this._id;
    };

    item.prototype.setData = function(data){
        if (data) {
            this.productData = data;
            this.setSampleProducts(data);
        }
    };

    item.prototype.setSampleProducts = function (data) {
        this.orderSample.products = [data].map(function (item) {
            return item;
        });
        this.setFields(this.orderSample.products);
    };

    item.prototype.setFields = function (rawArray) {
        angular.forEach(rawArray, function (item) {
            item.searchBar = item.id ? item.id + " - " + item.title : '';
            item.selectedColors = [];
        });
    };

    item.prototype.getData = function(){
        return this.productData;
    };

    item.prototype.selectShipping = function () {
        this.saveOrderParams();
    };

    item.prototype.selectOption = function (option) {
        var _this = this,
            getSelectedIndex = function (o) {
                for (var i=0; i < _this.orderData.selectedOptions.length; i++){
                    if (_this.orderData.selectedOptions[i].id == o.id){
                        return i;
                    }
                }
                return -1;
            },
             selectedIndex = getSelectedIndex(option);
        if (selectedIndex !== -1){
            if(option.show_as === 'checkbox' && !option.is_checked_option) {
                this.orderData.selectedOptions.splice(selectedIndex, 1);
            } else if(option.show_as === 'radio' || option.show_as === 'drop_down') {
                this.orderData.selectedOptions.splice(selectedIndex, 1, option);
            }
        } else {
            if(option.show_as === 'checkbox' && option.is_checked_option) {
                option.chosen_sub_option.id = option.product_sub_options[0].id;
                this.orderData.selectedOptions.push(option);
            } else if (option.show_as === 'radio' || option.show_as === 'drop_down') {
                this.orderData.selectedOptions.push(option);
            }

        }
        this.saveOrderParams();
    };

    item.prototype.setSelectedOptions = function (options) {
        var allOptions = this.getData().product_options;
        if (allOptions && allOptions.length){
            for (var i = 0; i< options.length; i++){
                for (var j = 0; j < allOptions.length; j++){
                    if (options[i].id == allOptions[j].id){
                        allOptions[j].isSelected = !allOptions[j].isSelected;
                    }
                }
            }
        }
    };

    item.prototype.prepareFormData = function (isEdit) {
        var formData = new FormData(),
            formatDate = function(date){
                var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();
                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;
                return [year, month, day].join('-');
            },
            transformSelectedColor = function (color) {
                return JSON.stringify(color);
            },
            transformSelectedImprints = function (orderData) {
                var imprintsArr = [],
                    _getColorIds = function (colors) {
                        var _colorIds = [];
                        if (colors && colors.length) {
                            for (var i = 0; i < colors.length; i++) {
                                _colorIds.push(colors[i].id);
                            }
                        }
                        return _colorIds;
                    };

                for (var key in orderData.imprints){
                    if (orderData.imprints.hasOwnProperty(key) && orderData.imprints[key].selected){
                        imprintsArr.push({
                            imprint_id: key ,
                            color_ids: _getColorIds(orderData.imprints[key].data.colors)
                        });
                    }
                }
                return JSON.stringify(imprintsArr);
            },
            transformProductOptions = function (options) {

                var optionsArr = [];
                for (var i = 0; i < options.length; i++){
                    optionsArr.push({
                        product_option_id: options[i].id,
                        product_sub_option_ids: [options[i].chosen_sub_option.id]
                    });
                }
                return JSON.stringify(optionsArr);
            },
            transformSizes = function (orderData) {
                var sizeIdsArr = [], sizesArr = [];
                if (orderData.laterBreakdown == 0 && orderData.sizeGroupId){
                    angular.forEach(orderData.breakdownBySizeQuantities, function(size, sizeKey){
                        if (size && size.length) {
                            sizeIdsArr.push({"id" : sizeKey, "qty" : size});
                        }
                    });
                    sizesArr.push({"size_group_id": orderData.sizeGroupId, "size_ids": sizeIdsArr});
                }
                return JSON.stringify(sizesArr);
            };

        isEdit ? formData.append('_method','PUT') : null;
        formData.append('product_id', this.getData().id);

        formData.append('quantity', this.orderData.quantity);
        formData.append('later_size_breakdown', Number(!!this.orderData.laterBreakdown));
        formData.append('imprint_comment', this.orderData.imprintComment || '');
        formData.append('tax_exemption', Number(!!this.orderData.taxExemption));
        if (this.orderData.art_files) {
            for (let i = 0; i < this.orderData.art_files.length; i++) {
                formData.append("art_files[]", this.orderData.art_files[i]._file);
            }
        }
        this.orderData.shipData.zipCode ? formData.append('estimation_zip', this.orderData.shipData.zipCode) : null;
        this.orderData.shipData.shippingMethod ? formData.append('estimation_shipping_method', this.orderData.shipData.shippingMethod ) : null;
        this.orderData.shipData.shippingCode ? formData.append('estimation_shipping_code', this.orderData.shipData.shippingCode ) : null;
        this.orderData.shipMethod ? formData.append('shipping_method', this.orderData.shipMethod ) : null;
        this.orderData.shipData.method ? formData.append('own_shipping_type', this.orderData.shipData.method ) : null;
        this.orderData.shipData.accountId ? formData.append('own_account_number', this.orderData.shipData.accountId ) : null;
        this.orderData.shipData.carrier ? formData.append('own_shipping_system', this.orderData.shipData.carrier) : null;
        this.orderData.shipTimeValue ? formData.append('received_date', formatDate(this.orderData.shipTimeValue)) : null;
        this.orderData.selectedOptions ? formData.append('product_options', transformProductOptions(this.orderData.selectedOptions)) :null;
        this.orderData.selectedColors ? formData.append('product_colors', transformSelectedColor(this.orderData.selectedColors)) : null;
        this.orderData.imprints ? formData.append('product_imprints', transformSelectedImprints(this.orderData)) : null;
        (this.orderData.breakdownBySizeQuantities && this.orderData.laterBreakdown == 0) ? formData.append('product_sizes', transformSizes(this.orderData)) : null;
        return formData;
    };

    item.prototype.saveOrderParams = function () {
        var storageData = Object.assign({}, this.orderData);
        if (storageData.art_files){
            storageData.art_files = [];
        }
        
        this.$localStorage.set('orderData'+this.getId(), storageData);

    };

    item.prototype.deleteOrderParams = function () {
        this.$localStorage.remove('orderData'+this.getId());
    };

    item.prototype.getSavedOrderParams = function () {
        var _orderData = this.$localStorage.get('orderData'+this.getId());
        if (_orderData) {
            this.orderData = _orderData;
            if (_orderData.selectedOptions.length){
                this.setSelectedOptions(_orderData.selectedOptions)
            }
        }
    };

    return item;
}
