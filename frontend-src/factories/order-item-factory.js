import productItem from './product-item-factory';

orderItem.$inject = ['localStorageService'];
export default function orderItem($localStorage) {
    var item = function (cart_item) {
        this.initOrderVariables();
        this.setId(cart_item.id);
        this.setData(cart_item.product);
        this.setAllColorsGrid();
        this.fillOrderData(cart_item);
        this.setSizeGroupId();
        this.colors = cart_item.colors || [];
        this.colorNames = '';
        this.cart_item_cost = cart_item.cart_item_cost;
        this.imprints = cart_item.imprints || [];
        this.imprint_comment = cart_item.imprint_comment;
        this.receivedDate = cart_item.received_date;
        this.productImage = cart_item.product.thumbnail;
        this.productTitle = cart_item.product.title;
        this.productId = cart_item.product.id;
        this.isOpened = false;
        this.isCheckedSelected = true;
        this.estimCode = cart_item.estimation_shipping_code;
        this.estimMethod = cart_item.estimation_shipping_method;
        this.estimZip = cart_item.estimation_zip;
    };

    item.prototype = Object.create(productItem($localStorage).prototype);
    item.prototype.fillOrderData = function (cartItem) {
        var
            _this = this,
            transformSelectedColor = function (allColors, colors) {
                if (allColors.length && colors.length){
                    for (var i = 0; i<allColors.length; i++){
                        if (allColors[i].id == colors[0].color_id) {
                            _this.orderData.selectedColor = allColors[i];
                        }
                    }
                }
            },
            transformProductOptions = function (allOptions, selectedOptions) {
            if (allOptions.length && selectedOptions.length)
                for (var i = 0; i< selectedOptions.length; i++){
                    for (var j=0; j<allOptions.length; j++){
                        if (selectedOptions[i].product_option_id == allOptions[j].id){
                            allOptions[j].isSelected = !allOptions[j].isSelected;
                            selectedOptions[i].product_sub_options[0].id = selectedOptions[i].product_sub_options[0].product_sub_option_id;
                            allOptions[j].chosen_sub_option = selectedOptions[i].product_sub_options[0];
                            _this.orderData.selectedOptions.push(allOptions[j]);
                        }
                    }
                }
            },
            transformSizes = function (sizes) {
                for (var key in sizes){
                    if (sizes.hasOwnProperty(key)){
                        _this.orderData.breakdownBySizeQuantities[sizes[key].product_size_id] = sizes[key].qty;
                    }
                }
            },
            transformSelectedImprints = function (imprints) {
                var _getImprintPrices = function (imprintId) {
                        var productImprints = _this.getData().imprints;
                        if (productImprints && productImprints.length){
                            for (var i = 0; i < productImprints.length; i++){
                                if (productImprints[i].id == imprintId){
                                    return  productImprints[i].imprint_prices;
                                }
                            }
                        } else {
                            return [];
                        }
                    },
                    _getSelectedPrices = function (prices) {
                        if (prices && prices.length){
                            for (var i=0; i< prices.length; i++){
                                // if (_this.orderData.quantity <= prices[i].quantity){
                                    return prices[i]
                                // }
                            }
                        } else {
                            return [];
                        }
                    },
                    _getOriginalColor = function (imprint) {
                        var _colors = [],
                            _getOriginalImprint = function (allImprints, currentId) {
                                for (var i = 0; i < allImprints.length; i++){
                                    if (allImprints[i].id == currentId){
                                        return allImprints[i];
                                    }
                                }
                                return null;
                            },
                            originalImprint = _getOriginalImprint(_this.getData().imprints, imprint.product_imprint_id);


                        for (var i = 0; i < imprint.imprint_colors.length; i++){
                            for (var j = 0 ; j < originalImprint.color_group.colors.length; j++){
                                if (originalImprint.color_group.colors[j].id == imprint.imprint_colors[i].color_id){
                                    _colors.push(originalImprint.color_group.colors[j])
                                }
                            }
                        }
                        return _colors;
                    },
                    _getOriginalName = function (imprint) {
                             var _getOriginalImprint = function (allImprints, currentId) {
                                for (var i = 0; i < allImprints.length; i++){
                                    if (allImprints[i].id == currentId){
                                        return allImprints[i];
                                    }
                                }
                                return null;
                            },
                            originalImprint = _getOriginalImprint(_this.getData().imprints, imprint.product_imprint_id);

                        return originalImprint.name;

                    };

                if (imprints && imprints.length){
                    for (var i = 0; i < imprints.length; i++){
                        _this.orderData.imprints[imprints[i].product_imprint_id] = {
                            selected: true,
                            data:{
                                colors:[],
                                prices:{}
                            }
                        };
                        _this.orderData.imprints[imprints[i].product_imprint_id]['data']['prices'] = _getSelectedPrices(_getImprintPrices(imprints[i].product_imprint_id));
                        _this.orderData.imprints[imprints[i].product_imprint_id]['data']['colors'] = _getOriginalColor(imprints[i]);
                        _this.orderData.imprints[imprints[i].product_imprint_id]['data']['name'] = _getOriginalName(imprints[i])
                    }
                }
            };
        this.orderData.quantity = cartItem.cart_item_cost.quantity;
        this.orderData.laterBreakdown = +cartItem.later_size_breakdown;
        this.orderData.imprintComment = cartItem.imprint_comment;
        this.orderData.taxExemption = Boolean(cartItem.tax_exemption);
        this.orderData.shipData.zipCode = cartItem.estimation_zip;
        this.orderData.shipData.shippingCode = cartItem.estimation_shipping_code;
        this.orderData.shipData.shippingMethod = cartItem.estimation_shipping_method;
        this.orderData.shipMethod = cartItem.shipping_method;
        this.orderData.shipData.method = cartItem.own_shipping_type;
        this.orderData.shipData.accountId = cartItem.own_account_number;
        this.orderData.shipData.carrier = cartItem.own_shipping_system;
        if (cartItem.received_date) {
            this.orderData.shipTimeValue = new Date(cartItem.received_date);
            this.orderData.shipTime = 'byEndOfTheDay';
        } else {
            this.orderData.shipTimeValue = null;
            this.orderData.shipTime = 'regular';
        }

        transformSelectedColor(this.allColors, cartItem.colors);
        transformProductOptions(this.getData().product_options, cartItem.product_options);
        transformSelectedImprints(cartItem.imprints);
        cartItem.cart_item_size_group ? transformSizes(cartItem.cart_item_size_group.cart_item_sizes) : null;
    };
    return item;
}
