import productItem from './product-item-factory';
import orderItem from "./order-item-factory";

export default angular.module('appFactoriesModule', [])
    .factory('ProductItem', productItem)
    .factory('OrderItem', orderItem)
    .name;
