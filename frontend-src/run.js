import cartService from "./services/cart-service/cart-service";

run.$inject = [
    '$rootScope',
    '$state',
    'cartService',
    '$transitions',
    'appUserService',
    'addressBookService',
    'paymentProfileService',
    'artProofService',
    '$colorsData',
    '$categoriesData',
    '$blog',
    '$staticPages',
    'apiService',
    '$rollBack',
    '$document',
    'LoginRegistrationService',
];

export default function run(
    $rootScope,
    $state,
    cartService,
    $transitions,
    $appUser,
    $addressBook,
    $paymentProfile,
    $artProofs,
    $colorsData,
    $categoriesData,
    $blog,
    $staticPages,
    apiService,
    $rollBack,
    $document,
    loginRegistrationService
) {
    $rootScope.$state = $state;
    $blog.getData($blog.pagination.current_page);
    $staticPages.setAwards();
    $staticPages.getGroupsStudies();
    $staticPages.getGroupsGlossary();
    $staticPages.getGroupsFAQs();
    apiService.getStaticData();
    $rollBack.detectRouts();
    $appUser.rememberMe();

    if (/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        return
      } else if (/iPad/i.test(navigator.userAgent)) {
        $categoriesData.getData();
      } else {
        $categoriesData.getData();
    }

    $rootScope.$on("$locationChangeStart", function(event, next, current) {
        $rootScope.$watch('$state.current.name', function(newValue, oldValue) {
            if (newValue.search('categories') < 0 || newValue.search('customize-order') < 0) {
                $rootScope.title = 'Superiorpromos';
                $rootScope.description  = 'Promotional products, promotional items made easy by www.superiorpromos.com. ' +
                    'Advertise your logo with the biggest selection of promotional pens, koozies & more."';
            } else {
                $rootScope.title = 'Superiorpromos | ' + $rootScope.title;
                $rootScope.description  = '';
            }

            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());
            gtag('config', 'UA-2202411-2', {'page_path': window.location.pathname});
        });
    });

    // $rootScope.$on('$locationChangeSuccess', function (src) {
    //     $window.scrollTo(0,0);
    // });

    // $rootScope.$on('$viewContentLoaded', function() {
    //     // jQuery('html, body').animate({ scrollTop: 0 }, 200);
    //     window.scrollTo(0, 0);
    // });

    // keep user logged in after page refresh
    switch ($appUser.resetToken()) {
        case 'auth':
            $appUser.getUserAccount()
                .then(cartService.getAll)
                .then($addressBook.getAddressBook)
                .then($paymentProfile.getPaymentProfilesList)
                .then($artProofs.getArtProofs);
            break;
        case 'logged-guest':
            cartService.getCart();
            break;
        case 'token-expired':
            cartService.getCart();
            loginRegistrationService.goToStep('login');
            $appUser.showLoginModal = true
            angular.element($document.body).addClass('overflow-hidden');
            break;
        case 'new-guest':
            cartService.getCart();
    }
    
    $transitions.onSuccess({}, function () {
        // document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
}
