import addressBookService from './address-book-service';
import paymentProfileService from './payment-profile-service';
import artProofService from './art-proof-service';

export default angular.module('userAccountServiceModule', [])
    .service('addressBookService', addressBookService)
    .service('paymentProfileService', paymentProfileService)
    .service('artProofService', artProofService)
    .name
