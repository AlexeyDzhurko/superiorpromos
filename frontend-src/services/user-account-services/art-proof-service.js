artProofService.$inject = ['apiService', '$q', '$sce', '$state', '$location', '$anchorScroll'];
export default function artProofService(apiService, $q, $sce, $state, $location, $anchorScroll) {
    var artProofService = {
            APPROVED:'approved',
            DECLINED:'declined',
            PENDING:'pending'
        },
        errorHandler = function (error) {
            return error;
        },
        transformArtProofs = function (data) {
            for (var i = 0; i < data.length; i++){
                if (!data[i].status || data[i].status.toLowerCase() === 'n/a'){
                    data[i].status = artProofService.PENDING;
                }
                if (data[i].note){
                    data[i].note = $sce.trustAsHtml(data[i].note);
                }
            }
            return data;
        },
        updateLocalArtProofItem = function (id, field, value) {
            for (var i = 0; i < artProofService.artProofs.length; i++){
                if (artProofService.artProofs[i].id == id){
                    return artProofService.artProofs[i][field] = value
                }
            }
        },
        findLocalArtProof = function (id) {
            for (var i = 0; i < artProofService.artProofs.length; i++){
                if (id == artProofService.artProofs[i].id){
                    return artProofService.artProofs[i];
                }
            }
            return false;
        },
        addLocalArtProof = function (data) {
            artProofService.artProofs.push(data);
            return data;
        },
        deleteLocalArtProof = function (id) {
            var deletedItem = null;
            for (var i = 0; i< artProofService.artProofs.length; i++){
                if(artProofService.artProofs[i].id == id){
                    deletedItem = artProofService.artProofs[i];
                    artProofService.artProofs.splice(i, 1);
                }
            }
            return deletedItem;
        };

    artProofService.artProofs = [];
    artProofService.setArtProofs = function(response){
        artProofService.artProofs = transformArtProofs(response.data);
    };

    artProofService.getArtProofs = function () {
        apiService.getArtProofs()
            .then(artProofService.setArtProofs, errorHandler)
    };

    artProofService.getArtProof = function (id) {
        var address = angular.copy(findLocalArtProof(id)),
            defered = $q.defer();
        defered.resolve(address);
        return defered.promise;
    };

    artProofService.approve = function (id, text) {
        apiService.approveArtProof(id, text).then(function (e) {
            updateLocalArtProofItem(id, 'status', artProofService.APPROVED);
            updateLocalArtProofItem(id, 'customer_note', text);
        }, errorHandler)
    };

    artProofService.decline = function (id, text) {
        if (!text || text.length === 0) {
            console.log('block');
            return false;
        }
        apiService.declineArtProof(id, text).then(function (e) {
            updateLocalArtProofItem(id, 'status', artProofService.DECLINED);
            updateLocalArtProofItem(id, 'customer_note', text);
        }, errorHandler)
    };

    artProofService.note = function (id, text) {
        apiService.noteArtProof(id, text).then(function (e) {
            updateLocalArtProofItem(id, 'customer_note', text);
        }, errorHandler);
    };

    artProofService.clearArtProofs = function () {
        artProofService.artProofs = [];
    };

    artProofService.showChosenOrder = function (item, id) {
        $state.go('router.my-account.order-history').then(function () {
            $location.hash('product_' + id);
            
        });
    }

    return artProofService;
}
