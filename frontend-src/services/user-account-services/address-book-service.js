addressBookService.$inject = ['apiService', '$q'];
export default function addressBookService(apiService, $q) {
    var addressBookService = {},
        errorHandler = function (error) {
            return error;
        },
        updateLocalAddressItem = function (response) {
            for (var i=0; i<addressBookService.addressBook.length; i++){
                if (response.data.id == addressBookService.addressBook[i].id){
                    return addressBookService.addressBook[i] = response.data;
                }
            }
        },
        updateLocalDefaultAddress = function (address) {
            for (var i=0; i<addressBookService.addressBook.length; i++){
                if (address.type == addressBookService.addressBook[i].type){
                    if (addressBookService.addressBook[i].id == address.id && addressBookService.addressBook[i].is_default == 0) {
                        addressBookService.addressBook[i].is_default = 1;
                    } else {
                        addressBookService.addressBook[i].is_default = 0;
                    }
                }
            }
        },
        findLocalAddress = function (id) {
            for (var i=0; i<addressBookService.addressBook.length; i++){
                if (id == addressBookService.addressBook[i].id){
                    return addressBookService.addressBook[i];
                }
            }
            return false;
        },
        addLocalAddress = function (data) {
            addressBookService.addressBook.push(data);
            return data;
        },
        deleteLocalAddress = function (id) {
            var deletedItem = null;
            for (var i = 0; i< addressBookService.addressBook.length; i++){
                if(addressBookService.addressBook[i].id == id){
                    deletedItem = addressBookService.addressBook[i];
                    addressBookService.addressBook.splice(i, 1);
                }
            }
            return deletedItem;
        };

    addressBookService.addressBook = [];
    addressBookService.setAddressBook = function(response){
        addressBookService.addressBook = response.data;
    };
    addressBookService.getAddressBook = function () {
        apiService.getUserAddressInfo()
            .then(addressBookService.setAddressBook)
    };

    addressBookService.setDefaultAddress = function(address){
        apiService.setDefaultAddressInfo(address.id).then(function(response) {
            updateLocalDefaultAddress(address);
        });
    };

    addressBookService.getAddress = function (id) {
        var address = angular.copy(findLocalAddress(id)),
            defered = $q.defer();
        if (!address){
            return apiService.getUserAddressById(id).then(function (res) {
                return res.data;
            })
        } else {
            defered.resolve(address);
            return defered.promise;
        }
    };

    addressBookService.updateAddress = function (id, data) {
        return apiService.updateUserAddress(id, data).then(updateLocalAddressItem)
    };

    addressBookService.createAddress = function (data) {
        return apiService.addUserAddressInfo(data).then(function (res) {
            return addLocalAddress(res.data)
        })
    };
    addressBookService.createTemporaryAddress = function (data) {
        data.id = 'temporary';
        return addLocalAddress(data)
    };
    addressBookService.deleteTemporaryAddresses = function () {
        return deleteLocalAddress('temporary');
    };
    addressBookService.deleteAddress = function (id) {
        return apiService.deleteUserAddressInfo(id).then(function () {
            return deleteLocalAddress(id);
        })
    };

    addressBookService.getDefaultAddresses = function () {
        var found = {};
        for (var i= 0 ; i < addressBookService.addressBook.length; i++){
            if (addressBookService.addressBook[i].is_default){
                switch (+addressBookService.addressBook[i].type){
                    case 1 :{
                        found['shipping'] = addressBookService.addressBook[i];
                        break;
                    }
                    case 2 :{
                        found['billing'] = addressBookService.addressBook[i];
                        break;
                    }
                }
            }
        }
        return found;
    };

    addressBookService.getShippingAddresses = function () {
        var found = [];
        for (var i= 0 ; i < addressBookService.addressBook.length; i++){
                if (addressBookService.addressBook[i].type == 1){
                    found.push(addressBookService.addressBook[i]);
                }
            }
            // set default addresses first
            found.sort(function (a, b) {
               return b.is_default - a.is_default;
            });
        return found;
    };

    addressBookService.getBillingAddresses = function () {
        var found = [];
        for (var i= 0 ; i < addressBookService.addressBook.length; i++){
            if (addressBookService.addressBook[i].type == 2){
                found.push(addressBookService.addressBook[i]);
            }
        }
        // set default addresses first
        found.sort(function (a, b) {
            return b.is_default - a.is_default;
        });
        return found;
    };
    addressBookService.clearAddresses = function () {
        addressBookService.addressBook = [];
    };

    return addressBookService;
}
