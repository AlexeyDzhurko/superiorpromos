paymentProfileService.$inject = ['apiService', '$q', 'FlashMessagesService'];

export default function paymentProfileService(apiService, $q, $flashMessage) {
    var paymentProfileService = {},
        errorHandler = function (error) {
            return error;
        },
        updateLocalPaymentProfile = function (response) {
            for (var i = 0; i < paymentProfileService.profiles.length; i++) {
                if (response.data.id == paymentProfileService.profiles[i].id) {
                    return paymentProfileService.profiles[i] = response.data;
                }
            }
        },
        updatelocalDefaultPaymentProfile = function (id) {
            for (var i = 0; i < paymentProfileService.profiles.length; i++) {
                paymentProfileService.profiles[i].is_default = 0;
            }
            ;
            for (var i = 0; i < paymentProfileService.profiles.length; i++) {
                if (paymentProfileService.profiles[i].id == id) {
                    paymentProfileService.profiles[i].is_default = 1;
                }
                ;
            }
            ;
        },
        findLocalPaymentProfile = function (id) {
            for (var i = 0; i < paymentProfileService.profiles.length; i++) {
                if (id == paymentProfileService.profiles[i].id) {
                    return paymentProfileService.profiles[i];
                }
            }
            return false;
        },
        
        deleteLocalPaymentProfile = function (id) {
            var deletedItem = null;
            for (var i = 0; i < paymentProfileService.profiles.length; i++) {
                if (paymentProfileService.profiles[i].id == id) {
                    deletedItem = paymentProfileService.profiles[i];
                    paymentProfileService.profiles.splice(i, 1);
                }
            }
            return deletedItem;
        };

    paymentProfileService.profiles = [];
    paymentProfileService.setPaymentProfile = function (response) {
        paymentProfileService.profiles = response.data;
    };

    paymentProfileService.getPaymentProfilesList = function () {
        apiService.getUserPaymentInfo()
            .then(paymentProfileService.setPaymentProfile)
    };

    paymentProfileService.setDefaultPaymentProfile = function (id) {
        var errorMessage = '';
        apiService.setDefaultPaymentInfo(id).then(function (response) {
            updatelocalDefaultPaymentProfile(id);
        });
    };

    paymentProfileService.getPaymentProfile = function (id) {
        var profile = angular.copy(findLocalPaymentProfile(id)),
            deferred = $q.defer();
        if (!profile) {
            return apiService.getUserPaymentInfoById(id).then(function (res) {
                return res.data;
            })
        } else {
            deferred.resolve(profile);
            return deferred.promise;
        }
    };

    paymentProfileService.createPaymentProfile = function (data) {
        return apiService.addUserPaymentInfo(data);
    };

    paymentProfileService.updatePaymentProfile = function (id, data) {
        return apiService.updateUserPaymentInfo(id, data).then(updateLocalPaymentProfile)
    };

    paymentProfileService.addLocalPaymentProfile = function (data) {
        paymentProfileService.profiles.push(data);
        return data;
    },

    paymentProfileService.deletePaymentProfile = function (id) {
        return apiService.deleteUserPaymentInfo(id).then(function () {
            return deleteLocalPaymentProfile(id);
        })
    };

    paymentProfileService.getDefaultProfile = function () {
        for (var i = 0; i < paymentProfileService.profiles.length; i++) {
            if (paymentProfileService.profiles[i].is_default) {
                return paymentProfileService.profiles[i];
            }
        }
    };

    paymentProfileService.clearProfiles = function () {
        paymentProfileService.profiles = [];
    };

    return paymentProfileService;
}
