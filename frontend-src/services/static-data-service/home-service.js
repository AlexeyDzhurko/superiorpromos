homeService.$inject = ['apiService', '$rootScope'];

export default function homeService(apiService, $rootScope) {
    var service = {
          promoProducts: [],
          popularCategories: [],
          promos: []
        },
        setPromoProducts = function (res) {
            service.promoProducts = res.data;
            broadcastPromProducts();
        },
        setPopularCategories = function (res) {
            service.popularCategories = res.data;
            broadcastPopularCategories()
        },
        setPromos = function (res) {
            service.promos = res.data;
            broadcastPromos();
        },
        errorHandler = function (error) {
            return error;
        },
        broadcastPromProducts = function() {
            $rootScope.$broadcast('broadcastPromProducts');
        },
        broadcastPopularCategories = function() {
            $rootScope.$broadcast('broadcastPopularCategories');
        },
        broadcastPromos = function() {
            $rootScope.$broadcast('broadcastPromos');
        };

    service.getData = function () {
        apiService.getPromoProducts()
            .then(setPromoProducts)
            .catch(errorHandler);
        apiService.getPopularCategories()
            .then(setPopularCategories)
            .catch(errorHandler);
        apiService.getPromos()
            .then(setPromos)
            .catch(errorHandler);
    };

    service.getPromProducts = function () {
        return service.promoProducts;
    };

    service.getPopularCategories = function () {
        return service.popularCategories;
    };

    service.getPromos = function () {
        return service.promos;
    };
    
    return service;
}
