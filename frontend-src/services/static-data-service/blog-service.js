blogService.$inject = ['apiService'];
export default function blogService(apiService) {
    var
        service = {
            blogs:[],
            pagination: {
              current_page: 1
            },
            mockBlogGroups: [
                {
                    title: 'Promotional Blankets',
                    quantity: 3
                },
                {
                    title: 'Promotional Buttons',
                    quantity: 54
                },
                {
                    title: 'Promotional Causes',
                    quantity: 1
                },
                {
                    title: 'Promotional Calendars',
                    quantity: 225
                },
                {
                    title: 'Promotional Coffee Mug',
                    quantity: 8
                }
            ]
        },
        setBlogs = function (res) {
            service.blogs = res.data.data;
            service.pagination = {
                current_page: res.data.current_page,
                total_pages: res.data.last_page,
                total_items: res.data.total
            }
        },
        setPages = function (res) {
            service.blogs = res.data.data;
        },
        errorHandler = function (error) {
            return error;
        };

    service.getData = function (page) {
        apiService.getBlogData(page)
            .then(setBlogs)
            .catch(errorHandler)
    };

    service.getPages = function (page) {
        apiService.getBlogData(page)
            .then(setPages)
            .catch(errorHandler)
    };

    service.paginate = function () {
        service.getPages(service.pagination.current_page);
    };

    return service;
}