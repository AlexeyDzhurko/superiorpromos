var staticFaqs = [
    {
        title: 'Samples',
        content: 'Most products require that a silk screen or plate be made in order to apply your custom imprint. If your artwork is more than one color, ' +
        'a screen must be made for each color. Screen charges are one-time charges per product, per imprint. New imprints will require new screens and charges.'
    },
    {
        title: 'Artwork requerements',
        content: 'Most products require that a silk screen or plate be made in order to apply your custom imprint. If your artwork is more than one color, ' +
        'a screen must be made for each color. Screen charges are one-time charges per product, per imprint. New imprints will require new screens and charges.'
    },
    {
        title: 'Shipping',
        content: 'Most products require that a silk screen or plate be made in order to apply your custom imprint. If your artwork is more than one color, ' +
        'a screen must be made for each color. Screen charges are one-time charges per product, per imprint. New imprints will require new screens and charges.'
    },
    {
        title: 'Over Runs& Under Runs',
        content: 'Most products require that a silk screen or plate be made in order to apply your custom imprint. If your artwork is more than one color, ' +
        'a screen must be made for each color. Screen charges are one-time charges per product, per imprint. New imprints will require new screens and charges.'
    },
    {
        title: 'Payment Options',
        content: 'Most products require that a silk screen or plate be made in order to apply your custom imprint. If your artwork is more than one color, ' +
        'a screen must be made for each color. Screen charges are one-time charges per product, per imprint. New imprints will require new screens and charges.'
    },
    {
        title: 'Cancellations',
        content: 'Most products require that a silk screen or plate be made in order to apply your custom imprint. If your artwork is more than one color, ' +
        'a screen must be made for each color. Screen charges are one-time charges per product, per imprint. New imprints will require new screens and charges.'
    }
]
export default staticFaqs;