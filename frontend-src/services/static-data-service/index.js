import colorsService from './colors-service';
import categoriesService from './categories-service';
import notificationService from './notification-service';
import blogService from './blog-service';
import staticPagesService from './static.pages-service';
import staticGroups from "./static-groups";
import staticAwards from "./static-awards";
import staticFaqs from "./static-faqs";
import homeService from "./home-service"


export default angular.module('staticDataServiceModule', [])
    .service('$colorsData', colorsService)
    .service('$categoriesData', categoriesService)
    .service('$notifications', notificationService)
    .service('$blog', blogService)
    .service('$home', homeService)
    .service('$staticPages', staticPagesService)
    .constant('$staticGroups', staticGroups)
    .constant('$staticAwards', staticAwards)
    .constant('$staticFaqs', staticFaqs)
    .name
