var staticAwards = [
        {
            id: 1,
            image: 'davey.jpg',
            content: "<p>Our company's goal is to provide unparallel customer service and offer an array of products and services that will help companies with their marketing needs. We are honored to receive Davey 2009 Silver Award for Outstanding Customer Products and Services. To view our listing click here.</p>"+
                            "<p>The Davey Awards is an international creative award focused exclusively on honoring outstanding creative work from the best small firms worldwide. The 2009 Davey Awards received over 4,000 entries from ad agencies, interactive agencies, production firms, in-house creative professionals, graphic designers, design firms, and public relations firms.  </p>"+
                            "<p>The Davey is judged and overseen by the International Academy of the Visual Arts (IAVA). The IAVA is an invitation-only member-based organization of leading professionals from various disciplines of the visual arts dedicated to embracing progress and the evolving nature of traditional and interactive media. Current membership represents a Who\'s Who of acclaimed media, advertising, and marketing firms including: Sotheby\'s Institute of Art, Yahoo!, Estee Lauder, Wired, Insight Interactive, The Webby Awards, Passaic Parc, Gershoni, Theory, Polo Ralph Lauren, ADWEEK, RANDWEEK, Alloy, Coach, iNDELIBLE, MTV, Victoria\'s Secret, HBO, the Ellen Degeneres Show, Myspace.com, and many others.</p>"
        },
        {
            id: 2,
            image: 'inc-5000_2.jpg',
            content: '<p>Superior Promos Inc has been recognized by Inc. Magazine as one of the fastest growing private companies in America in 2009. Inc.com, the daily online resource for entrepreneurs, ranks businesses according to precentage revenue growth from 2005-2008. Superior Promos is #3573 on this prestigious list. SuperiorPromos.com has been a leader in innovation and customer satisfaction. With seemless order processing, unmatched guarantees and world class support, superiorpromos.com continues to be the leaders in promotional products.</p>'
        },
        {
            id: 3,
            image: 'top10_fastest_growing2_2.jpg',
            content: '<p>ASI also known as the Advertising Specialty Institute has awarded Superior Promos Inc. as one of the fastest growing distributors in the promotional products industry. We are very proud of this award and strive every day to create a shopping experience like no other. Our amazing customer service and unbeatable pricing has allowed us to grow into a top distributor of promotional products on the web.</p>'
        },
        {
            id: 4,
            image: 'syphon_2.jpg',
            content: '<p>The Syphon Group a marketing research company has awarded SuperiorPromos.com it\'s prestigious Top Navigational Website Award. We are extremely honored to be recognized for this category. With having over 10,000 products online, finding exactly what you need and fast is very hard. We achieved this with intuitive navagational controls and easy to use product filters and search functions. Our goal here is to make your shopping experince for promotional products easy and rewarding.</p>'
        },
        {
            id: 5,
            image: 'asi_1_2.jpg',
            content: '<p>In 2007 we were honored to receive recognition from ASI for having one of the best promotional products websites. Our customer service first approach is what makes us different in every way.</p>'
        },
        {
            id: 6,
            image: 'galaxy_2.jpg',
            content: '<p>For 4 years in a row Galaxy Visions a leading web host provider recognized SuperiorPromos.com as a leader in innovation and technological advancements in ecommerce websites. Our industry leading shopping cart is unmatched. Easy navigation and order processing is hands down the best on the market. We make it extremely easy to find and purchase promotional products right on the web.</p>'
        }
    ]

export default staticAwards;