staticPagesService.$inject = ['$staticGroups', '$staticAwards', 'apiService'];

export default function staticPagesService($staticGroups, $staticAwards, apiService) {
    var service = {
            awards: [],
            groupsStudies: [],
            groupsGlossary: [],
            groupsFAQs: []
        };
        
    service.setAwards = function() {
        service.awards = angular.copy($staticAwards);
    };

    service.getGroupsStudies = function() {
        apiService.getCaseStudies().then(service.setGroupsStudies);
    };

    service.setGroupsStudies = function(res) {
        service.groupsStudies = res.data;
    };

    service.setGroupsGlossary = function(res) {
        service.groupsGlossary = res.data;
    };

    service.getGroupsGlossary = function() {
        apiService.getPromotionalGlossary().then(service.setGroupsGlossary);
    };

    service.getGroupsFAQs = function() {
        apiService.getFAQs().then(service.setGroupsFAQs);
    };

    service.setGroupsFAQs = function(res) {
        service.groupsFAQs = res.data;
    };

    service.toggleContent = function(award) {
        award.expanded = !award.expanded;
    };

    service.toggleAllItems = function(page) {
        switch (page) {
            case 'studies' :
                service.groupsStudies.map(function (group) {
                    group.open = !service.hideStudies;
                });
                service.hideStudies = !service.hideStudies;
                break;
            case 'glossary' :
                service.groupsGlossary.map(function (group) {
                    group.open = !service.hideGlossary;
                });
                service.hideGlossary = !service.hideGlossary;
                break;
        }
    };

    return service;
}
