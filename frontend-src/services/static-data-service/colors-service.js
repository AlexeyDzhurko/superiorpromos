colorsService.$inject = ['apiService'];
export default function colorsService(apiService) {
    var
        service = {
            colors:[]
        },
        setColors = function (res) {
            return service.colors = res.data;
        },
        errorHandler = function (error) {
            return error;
        };
    service.getData = function () {
        if (!service.colors.length) {
            apiService.getColors()
            .then(setColors)
            .catch(errorHandler)
        }
    };

    return service;
}