categoriesService.$inject = ['apiService', '$search', '$state', '$rootScope'];
export default function categoriesService(apiService, $search, $state, $rootScope) {
    var service = {
            categories :[],
            history : {
                cats: [],
                ids: [],
                showBackArrow: false
            },
            quickLinks: []
        },
        setCategories = function (res) {
            service.categories = res.data;
            broadcastCategories();
        },
        setSubCategories = function (res, parentId) {
            Object.keys(service.categories).forEach(function (key) {
                if (service.categories[key].id === parentId) {
                    service.categories[key]['sub_categories'] = res;
                    return true;
                }

                if (service.categories[key].sub_categories) {
                    setExpandCategories(service.categories[key], res, parentId);
                }
            });

            broadcastCategories();
        },
        setExpandCategories = function (category, data, parentId) {
            Object.keys(category.sub_categories).forEach(function (key) {
                if (category.sub_categories[key].id === parentId) {
                    category.sub_categories[key]['sub_categories'] = data;
                    return true;
                }

                if (category.sub_categories[key].sub_categories) {
                    setExpandCategories(category.sub_categories[key], data, parentId);
                }
            });
        },
        setQuickLinks = function (res) {
            return service.quickLinks = res.data;
        },
        errorHandler = function (error) {
            return error;
        },
        broadcastCategories = function() {
            $rootScope.$broadcast('broadcastCategories');
        };
    service.getData = function () {
        if (!service.categories.length) {
            apiService.getCategories()
            .then(setCategories)
            .catch(errorHandler);
            apiService.getQuickLinks()
                .then(setQuickLinks)
                .catch(errorHandler);
        }
    };
    service.getCategories = function () {
        return service.categories;
    };
    service.getSubCategories = function (parentId) {
        return apiService.getSubCategories(parentId)
            .then(function (res) {
                return res.data;
            })
            .catch(errorHandler);
    };
    service.getQuickLinks = function () {
        return service.quickLinks;
    };
    service.getHistory = function () {
        return service.history;
    };
    service.getHistoryItemId = function () {
        return service.history.ids[service.history.ids.length-1]
    };
    service.showCategory = function (category) {
        if(category.sub_categories.length) {
            service.categories ? service.history.cats.push(service.categories) : service.history.cats.push(service.sub_categories);
            service.categories = category.sub_categories;
            service.history.ids.push(category.id);
            service.history.showBackArrow = true;
        } else {
            $search.query = {
                category_slug : category.slug
            };
            $state.go('router.categories',{viewMode:'grid'})
        }
    };
    service.goBack = function () {
        if(service.history.cats.length){
            service.categories = service.history.cats[service.history.cats.length - 1];
            service.history.cats.pop();
            service.history.ids.pop();
            if(service.history.cats.length === 0) service.history.showBackArrow = false;
        }
    };
    service.showBackArrow = function () {
        return service.history.showBackArrow;
    };
    return service;
}
