notificationService.$inject = ['apiService', '$state', '$timeout', '$location', '$anchorScroll'];
export default function notificationService(apiService, $state, $timeout, $location, $anchorScroll) {
    var
        service = {
            artproofs: [],
            notes: []
        },
        setArtproofs = function (res) {
            return service.artproofs = res.data;
        },
        setNotes = function (res) {
            return service.notes = res.data;
        },
        errorHandler = function (error) {
            return error
        };

    service.getArtproofs = function () {
        apiService.getNewArtproofs()
            .then(setArtproofs)
            .catch(errorHandler);
    }

    service.getNotes = function () {
        apiService.getNewNotes()
            .then(setNotes)
            .catch(errorHandler);
    }

    service.getNewArtproofs = function () {
        return service.artproofs;
    }

    service.getNewNotes = function () {
        return service.notes;
    }

    service.deleteReadNote = function (index) {
        service.notes.splice(index, 1);
    }

    service.deleteReadArtproof = function (index) {
        service.artproofs.splice(index, 1);
    }

    service.showChosenNotification = function (id, name, cartItem, index, note_id, allItems) {

        if($state.includes('**.order-history') || $state.includes('**.order-history#*')) {
            angular.forEach(allItems, function (el) {
                if(el.id === id) {
                    angular.forEach(el.cart_items, function (cart_el) {
                        if(cart_el.id === cartItem) {
                            cart_el.opened = true;
                            switch (name) {
                                case 'Artproofs':
                                    cart_el.proofsOpened = true;
                                    service.deleteReadArtproof(index);
                                    apiService.sendArtproofStatusRead(note_id);
                                    break;
                                case 'Notes':
                                    cart_el.proofsOpened = false;
                                    angular.forEach(cart_el.notes, function (note_el) {
                                        if(note_el.id === note_id){
                                            note_el.opened = true;
                                            service.deleteReadNote(index);
                                            apiService.sendNoteStatusRead(note_id);
                                        }
                                    });
                                    break;
                            }
                        }
                    });
                }
            });

            $location.hash('product_' + id);
            $anchorScroll.yOffset = 0;
        } else {
            $state.go('router.my-account.order-history').then(function () {
                service.showChosenNotification(id, name, cartItem, index, note_id, allItems);
            });
        }
    }

    service.showChosenArtproof = function (id) {
        $state.go('router.my-account.art-proofs').then(function () {
            $location.hash('artproof_' + id);
            $anchorScroll.yOffset = 180;
        });
    }

    service.approveUserNotes = function (id, isApproved, noteMessage) {
        var data = {
                approved : isApproved,
                customer_note : noteMessage
            }
        return apiService.userNotesApprovement(id, data);
    }

    return service;

}