import appUserService from './app-user-service';

export default angular.module('appUserService', [])
    .service('appUserService', appUserService)
    .name
