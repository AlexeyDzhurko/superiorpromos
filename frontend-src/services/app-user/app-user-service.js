appUserService.$inject = [
  '$q',
  'apiService',
  'cartService',
  '$http',
  'localStorageService',
  '$state',
  'addressBookService',
  'paymentProfileService',
  'artProofService'];

export default function appUserService(
    $q, apiService, cartService, $http, localStorageService, $state,
    $addressBook, $paymentProfile, $artProofs) {
  var appUser = {},
      _generateGuestToken = function() {
        console.log('_generateGuestToken')
        var token = JSON.parse(localStorage.getItem('superior.token'));

        if (token && token.guest) {
          return token.guest;
        }
        if (token && token.auth) {
          return token.auth;
        }

        return crypt(navigator.useragent, Date.now().toString()).toString();
      },
      updateLocalAccountInformation = function(data) {
        for (var key in data) {
          if (data.hasOwnProperty(key) && key !== 'password') {
            for (var subKey in data[key]) {
              if (data[key].hasOwnProperty(subKey) && subKey !== 'email_confirmation') {
                appUser.user[subKey] = data[key][subKey];
              }
            }
          }
        }
        return appUser.user;
      },
      prepareEditableProfile = function(userData) {
        return {
          information: {
            name: userData.name,
            contact_telephone: userData.contact_telephone,
            ext: userData.ext,
            fax: userData.fax,
          },
          email: {
            email: '',
            email_confirmation: '',
          },
          password: {
            old_password: '',
            password: '',
            password_confirmation: '',
          },
        };
      },
      errorHandler = function(error) {
        return error;
      };

  appUser.user = null;
  appUser.remember = false;

  appUser.setUserAccount = function(response) {
    appUser.user = response.data;
    return appUser.user;
  };

  appUser.getUserAccount = function() {
    return $http.get('/api/account/account-information').
        then(appUser.setUserAccount);
  };

  appUser.getEditableProfile = function() {
    var deferred = $q.defer();
    if (appUser.user) {
      deferred.resolve(prepareEditableProfile(appUser.user));
      return deferred.promise;
    } else {
      return appUser.getUserAccount().then(prepareEditableProfile);
    }
  };

  appUser.setToken = function(tokenData, mode) {
    var token = {};
    if (!mode) {
      mode = 'auth';
    }
    (tokenData.data && tokenData.data.token) ?
        token[mode] = tokenData.data.token :
        token[mode] = tokenData;

    localStorageService.set('token', token);
    localStorageService.set('time', new Date().getTime());

    switch (mode) {
      case 'guest': {
        $http.defaults.headers.common.Guest = token[mode];
        break;
      }
      case 'auth': {
        $http.defaults.headers.common.Guest = '';
        $http.defaults.headers.common.Authorization = 'Bearer ' + token[mode];
        appUser.remember ?
            localStorage.setItem('superior.token',
                JSON.stringify({'token': tokenData.data.token})) :
            null;
        break;
      }
    }
    return true;
  };

  appUser.getUser = function() {
    return appUser.user;
  };

  appUser.getUserType = function() {
    var token = localStorageService.get('token');
    if (token && token.auth) {
      return 'auth';
    } else {
      return 'guest';
    }
  };

  appUser.resetToken = function () {
    let
        token = localStorageService.get('token'),
        time = localStorageService.get('time'),
        waitTime = 1000 * 60 * 60 * 24 * 30,
        isTimeNotExpired = time > new Date().getTime() - waitTime;

    if (isTimeNotExpired && token && token.auth) {
      $http.defaults.headers.common.Authorization = 'Bearer ' + token.auth;
      return 'auth';
    } else if (isTimeNotExpired && token && token.guest) {
      $http.defaults.headers.common.Guest = token.guest;
      return 'logged-guest';
    } else if (token && token.auth && !isTimeNotExpired) {
      return 'token-expired';
    } else {
      appUser.setToken(_generateGuestToken(), 'guest');
      return 'new-guest';
    }
  }

  appUser.logout = function() {
    appUser.user = null;
    localStorageService.remove('token');
    $http.defaults.headers.common.Authorization = '';
    appUser.setToken(_generateGuestToken(), 'guest');
    cartService.clearCarts();
    $artProofs.clearArtProofs();
    $addressBook.clearAddresses();
    $paymentProfile.clearProfiles();
    $state.go('router.home');
  };

  appUser.updateUserProfile = function(data) {
    return apiService.setUserAccountInformation(data).then(function() {
      return updateLocalAccountInformation(data);
    });
  };

  appUser.login = function(cred) {
    cred.remember_me = appUser.remember;
    return $http.post('/api/login', cred).
        then(appUser.setToken).
        then(appUser.getUserAccount).
        then(cartService.getAll).
        then($addressBook.getAddressBook).
        then($paymentProfile.getPaymentProfilesList).
        then($artProofs.getArtProofs).
        then(function(response) {

          if ($state.current.name === 'router.shopping-cart') {
            appUser.showLoginModal = false;
            return res;
          }
          // $state.go('router.my-account.order-history');
        });
  };

  appUser.rememberMe = function() {
    var savedUser = {};
    if (!!localStorage.getItem('superior.token')) {
      savedUser = {
        data: JSON.parse(localStorage.getItem('superior.token')),
      };
    }

    if (savedUser.data && savedUser.data.token) {
      appUser.setToken(savedUser, 'auth');
      appUser.getUserAccount().
          then(cartService.getAll).
          then($addressBook.getAddressBook).
          then($paymentProfile.getPaymentProfilesList).
          then($artProofs.getArtProofs);
    }
  };

  appUser.checkEmail = function(email) {
    return $http.get(`/api/check-email/${email}`);
  };

  // appUser.socialLogin = function (provider) {
  //     switch (provider){
  //         case 'google-plus':{
  //             return GooglePlus.login()
  //                 .then(function (authResult) {
  //                     console.log(authResult);
  //                     // GooglePlus.getUser().then(function (user) {
  //                     //     console.log(user);
  //                     // });
  //                     return authResult.code; // TODO: define responce format;
  //                 },function (error) {
  //                     return error;
  //                 }).then(function (code) {
  //                     return apiService.socialLogin(provider, code)
  //                 }).then(appUser.setToken)
  //                     .then(appUser.getUserAccount)
  //                     .then(cartService.getAll)
  //                     .then($addressBook.getAddressBook)
  //                     .then($paymentProfile.getPaymentProfilesList);
  //         }
  //         case 'facebook':{
  //             return $q(function (resolve, reject) {
  //                 Facebook.login(resolve, reject)
  //             }).then(function (code) {
  //                 return apiService.socialLogin(provider, code)
  //             }).then(appUser.setToken)
  //                 .then(appUser.getUserAccount)
  //                 .then(cartService.getAll)
  //                 .then($addressBook.getAddressBook)
  //                 .then($paymentProfile.getPaymentProfilesList);
  //         }
  //     }
  // };

  return appUser;
}
