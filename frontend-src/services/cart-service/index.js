import cartService from './cart-service';

export default angular.module('cartService', [])
    .factory('cartService', cartService)
    .name