cartService.$inject = ['apiService', 'OrderItem', 'paymentProfileService', 'addressBookService',
    '$q', '$notifications', '$window', '$cookies'];

export default function cartService(apiService, OrderItem, $paymentProfile, $addressBook,
                                    $q, $notifications, $window, $cookies) {
    var cart = {},
        errorHandler = function (error) {
            return error;
        },
        updateLocalItem = function (id, data, storage) {
            for (var i = 0; i< cart[storage].length; i++){
                if(cart[storage][i].id == id){
                    cart[storage][i].cartItem = new OrderItem(data);
                }
            }
            return true;
        },
        deleteLocalItem = function (id, storage) {
            var deletedItem = null;
            for (var i = 0; i< cart[storage].length; i++){
                if(cart[storage][i].id == id){
                    deletedItem = cart[storage][i];
                    cart[storage].splice(i, 1);
                }
            }
            return deletedItem;
        },
        getLocalItem = function (slug, storage) {
            for (var i = 0; i< cart[storage].length; i++){
                if(cart[storage][i].cartItem.productData.full_slug === slug){
                    return angular.copy(cart[storage][i].cartItem);
                }
            }
        },
        deleteLocalItemsMultiple = function (idArray, storage) {
            var deletedItems = [];
            for (var i= 0; i< idArray.length; i++){
                deletedItems.push(deleteLocalItem(idArray[i], storage));
            }
            return deletedItems;
        },
        createCartItems = function (rawItems) {
            var cartItems = [];
            var currentDate = new Date(),
                expireDate = new Date(currentDate.getFullYear() + 1, currentDate.getUTCMonth(), currentDate.getUTCDay()),
                expired = {'expires': expireDate}

            for (var i= 0; i< rawItems.length; i++){
                if (rawItems[i].cart_item){
                    $cookies.put('cart_id', rawItems[i].cart_item.cart_id, expired);
                    cartItems.push({
                        id: rawItems[i].id,
                        cartItem: new OrderItem(rawItems[i].cart_item)
                    })
                } else {
                    $cookies.put('cart_id', rawItems[i].cart_id, expired);

                    cartItems.push({
                        id: rawItems[i].id,
                        cartItem: new OrderItem(rawItems[i])
                    })
                }

            }
            return cartItems;
        },
        checkingObject = function (obj) {
            if (obj !== null && angular.isObject(obj)){
                return true;
            } else {
                return false;
            }
        },
        gatherWishListIds = function () {
            var wishIds = [];
            if (cart.wishListItems.length) {
                cart.wishListItems.forEach(function (item) {
                    wishIds.push(item.id);
                });
                return wishIds;
            }
        };

    cart.loaded = false;
    cart.cartItems = [];
    cart.savedCartItems = [];
    cart.wishListItems = [];
    cart.orderHistory = [];
    cart.reorderItems = [];
    cart.readyOrder = {};

    cart.getReorderItems = function (res) {
        cart.reorderItems = createCartItems(res.data);
        return cart.reorderItems;
    };

    cart.getCart = function () {
        if ($cookies.get('cart_id')) {
            return apiService.updateCart($cookies.get('cart_id'))
                .then(apiService.getCartItems().then(function (res) {
                        cart.cartItems = createCartItems(res.data);
                        return true;
                    }, function (error) {
                        return error
                    })
                );
        } else {
            return apiService.getCartItems()
                .then(function (res) {
                    cart.cartItems = createCartItems(res.data);
                    return true;
                },function (error) {
                    return error
                });
        }
    };

    cart.getSavedCart = function () {
        return apiService.getSavedCartItems()
            .then(function (res) {
                cart.savedCartItems = createCartItems(res.data);
                return true;
            }, errorHandler)
    };

    cart.getWishList = function () {
        return apiService.getWishList()
            .then(function (res) {
                cart.wishListItems = angular.copy(res.data);
                return true;
            }, errorHandler)
    };

    cart.getArtproofs = function () {
        $notifications.getArtproofs();
    };

    cart.getNotes = function () {
        $notifications.getNotes();
    };

    cart.getAll = function () {
        var deferred = $q.defer();
        if (cart.loaded) {
            deferred.resolve(true);
            return deferred.promise;
        } else {
            return cart.getCart()
                .then(cart.getSavedCart)
                .then(cart.getWishList)
                .then(cart.getOrderHistory)
                .then(cart.getArtproofs)
                .then(cart.getNotes)
                .then(function () {
                    return cart.loaded = true;
                });
        }
    };

    cart.updateCartItem = function (params, id) {
        return apiService.updateCartItem(params, id).then(function (res) {
            updateLocalItem(id, res.data, 'cartItems');
        }, errorHandler)
    };

    cart.updateSavedCartItem = function (params, savedCartItemId) {
        return apiService.updateSavedCartItem(params, savedCartItemId).then(function (res) {
            updateLocalItem(savedCartItemId, res.data.cart_item, 'savedCartItems');
        }, errorHandler)
    };

    cart.deleteCartItem = function (id) {
        var deferred = $q.defer();
        apiService.deleteCartItem(id).then(function () {
            deleteLocalItem(id, 'cartItems');
            deferred.resolve(true);
        }, errorHandler);
       return deferred.promise;
    };

    cart.deleteSavedCartItem = function (id) {
        var deferred = $q.defer();
        apiService.deleteSavedCartItem(id).then(function () {
            deleteLocalItem(id, 'savedCartItems');
            deferred.resolve(true);
        });
        return deferred.promise;
    };

    cart.deleteWishListItem = function (id) {
        return apiService.deleteWishListItem(id).then(function () {
            deleteLocalItem(id, 'wishListItems');
        }, errorHandler)
    };

    cart.addCartItem = function (params) {
        return apiService.addItemToCart(params).then(function (res) {
            var currentDate = new Date(),
                expireDate = new Date(currentDate.getFullYear() + 1, currentDate.getUTCMonth(), currentDate.getUTCDay()),
                expired = {'expires': expireDate}

            $cookies.put('cart_id', res.data.cart_id, expired);

            cart.cartItems.push({
                id:res.data.id,
                cartItem: new OrderItem(res.data)
            });
            return true;
        }, errorHandler)
    };

    cart.reorderCartItems = function (params) {
        var data = {
            order_item_ids: params
        }
        return apiService.reorderCartItems(data).then(function (res) {
            angular.isArray(res.data) && angular.forEach(res.data, function (el) {
                cart.cartItems.push({
                    id: el.id,
                    cartItem: new OrderItem(el)
                });
            });
            return true;
        }, errorHandler)
    };

    cart.addSavedCartItem = function (idArray) {
        var params = {
            cart_item_ids: idArray
        };
        return apiService.addToSavedCart(params).then(function (res) {
            cart.savedCartItems = cart.savedCartItems.concat(createCartItems(res.data));
            return true;
        },errorHandler)
    };

    cart.moveCartItemToSavedCart = function (idArray) {
        var params = {
            cart_item_ids: idArray
        };
        return apiService.addToSavedCart(params).then(function (res) {
            cart.savedCartItems = cart.savedCartItems.concat(createCartItems(res.data));
            deleteLocalItemsMultiple(idArray, 'cartItems');
            return true;
        },errorHandler)
    };

    cart.deleteAllCartItems = function (idArray) {
        apiService.deleteAllCartItems().then(function () {
            deleteLocalItemsMultiple(idArray, 'cartItems');
        }, errorHandler);
    };

    cart.deleteAllWhishListItems = function () {
        apiService.deleteAllWishListItems(gatherWishListIds())
             .then(function () {
                 cart.wishListItems = [];
             },errorHandler);
    };

    cart.addWishListItem = function (params) {
        return apiService.addItemToWishList(params).then(function (res) {
            cart.wishListItems.push(res.data);
            return true;
        }, errorHandler);
    };

    cart.moveSavedCartToCart = function (idArray) {
        var params = {
            saved_cart_item_ids: idArray
        };
        return apiService.moveSavedCartToCart(params).then(function (res) {
            cart.cartItems =  cart.cartItems.concat(createCartItems(res.data));
            deleteLocalItemsMultiple(idArray, 'savedCartItems');
        }, errorHandler);
    };

    cart.clearCarts = function () {
        cart.loaded = false;
        cart.cartItems = [];
        cart.savedCartItems = [];
        cart.wishListItems = [];
        cart.orderHistoryItems = [];
    };

    cart.checkout = function (idArray, checkoutParams, paymentType, multipleLocation) {
        var params = {
            shipping_address_id:checkoutParams.shippingAddress.id,
            billing_address_id:checkoutParams.billingAddress.id,
            payment_profile: {
                id: checkingObject(checkoutParams.paymentProfile) ? checkoutParams.paymentProfile.id : null,
                type: paymentType,
                cvv: paymentType === 'card' ? checkoutParams.cvv : null
            },
            ship_to_multiple_locations: {
                type: multipleLocation.type,
                details: multipleLocation.details
            }
        };
        return apiService.orderCheckout(params).then(function () {
            return deleteLocalItemsMultiple(idArray, 'cartItems');
        }).then(cart.getOrderHistory);
    };

    cart.getOrderHistory = function () {
        return apiService.getOrderHistory().then(function (res) {
            // TODOs: get additional data about order from Back-end
            return cart.orderHistoryItems = res.data;
        })
    };

    cart.getCartItemById = function (id) {
        var deferred = $q.defer();
            deferred.resolve(getLocalItem(id, 'cartItems'));
        return deferred.promise;
    };

    cart.getSavedCartItemById = function (id) {
        var deferred = $q.defer();
            deferred.resolve(getLocalItem(id, 'savedCartItems'));
        return deferred.promise;
    };

    return cart;
}
