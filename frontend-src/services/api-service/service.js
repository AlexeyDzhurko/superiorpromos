apiService.$inject = [ "$http", "$httpParamSerializer", "$httpParamSerializerJQLike"];

export default function apiService($http, $httpParamSerializer, $httpParamSerializerJQLike) {
  return {
    getCategories: function() {
      return $http.get("/api/categories");
    },
    getSubCategories: function(categoryId) {
      return $http.get(`/api/categories/${categoryId}/sub-categories`);
    },
    getPopularCategories: function() {
      return $http.get("/api/categories/popular"); 
    },
    getProductById: function(id) {
      return $http.get("/api/product/" + id).then(function(res) {
        return res.data;
      });
    },
    getProductBySlug: function(slug) {
      return $http.get("/api/product/" + slug).then(function(res) {
        if (res && res.data) {
          return res.data;
        }

        return null;
      });
    },
    getTopProducts: function() {
      return $http.get("/api/product/promotional-products");
    },
    getPromoProducts: function() {
      return $http.get("/api/product/promotional-products");
    },
    getCartItems: function() {
      return $http.get("/api/cart-item");
    },
    updateCart: function(cartId) {
      return $http.put("/api/cart/" + cartId);
    },
    addItemToCart: function(params) {
      return $http.post("/api/cart-item", params, {
        transformRequest: angular.identity,
        headers: {
          "Content-Type": undefined
        }
      });
    },
    orderSample: function(params) {
      return $http.post("/api/cart-item/order-sample", params);
    },
    reorderCartItems: function(params) {
      return $http.post("/api/cart-item/import-from-order", params);
    },
    reorderCustomizedItems: function(params) {
      return $http.post("/api/cart-item/reorder", params);
    },
    addItemToWishList: function(productId) {
      return $http.post(
        "/api/wish-list",
        $httpParamSerializer({ product_id: productId }),
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        }
      );
    },
    getWishList: function() {
      return $http.get("/api/wish-list");
    },
    deleteWishListItem: function(id) {
      return $http.delete("/api/wish-list/" + id);
    },
    deleteAllWishListItems: function() {
      return $http.post("/api/wish-list/clear");
    },
    updateCartItem: function(params, itemId) {
      return $http.post("/api/cart-item/" + itemId, params, {
        transformRequest: angular.identity,
        headers: {
          "Content-Type": undefined
        }
      });
    },
    updateSavedCartItem: function(params, itemId) {
      return $http.post("/api/user/saved-cart/" + itemId, params, {
        transformRequest: angular.identity,
        headers: {
          "Content-Type": undefined
        }
      });
    },
    deleteCartItem: function(itemId) {
      return $http.delete("/api/cart-item/" + itemId);
    },
    deleteAllCartItems: function() {
      return $http.delete("/api/cart");
    },
    getSavedCartItems: function() {
      return $http.get("/api/user/saved-cart");
    },
    addToSavedCart: function(params) {
      return $http.post(
        "/api/user/saved-cart",
        $httpParamSerializerJQLike(params),
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        }
      );
    },
    moveSavedCartToCart: function(params) {
      return $http.post("/api/user/saved-cart/move-to-cart", params);
    },
    deleteSavedCartItem: function(itemId) {
      return $http.delete("/api/user/saved-cart/" + itemId);
    },
    orderCheckout: function(params) {
      return $http.post("/api/order/checkout", params);
    },
    getOrderHistory: function() {
      return $http.get("/api/order");
    },
    setUserAccountInformation: function(params) {
      return $http.post(
        "/api/account/account-information",
        $httpParamSerializerJQLike(params),
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        }
      );
    },
    getUserPaymentInfo: function() {
      return $http.get("/api/user/payment_profile");
    },
    addUserPaymentInfo: function(data) {
      return $http.post(
        "/api/user/payment_profile",
        $httpParamSerializerJQLike(data),
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        }
      );
    },
    updateUserPaymentInfo: function(id, data) {
      return $http.put(
        "/api/user/payment_profile/" + id,
        $httpParamSerializerJQLike(data),
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        }
      );
    },
    setDefaultPaymentInfo: function(itemId) {
      return $http.patch("/api/user/payment_profile/" + itemId + "/default");
    },
    deleteUserPaymentInfo: function(itemId) {
      return $http.delete("/api/user/payment_profile/" + itemId);
    },
    getUserPaymentInfoById: function(id) {
      return $http.get("/api/user/payment_profile/" + id);
    },
    getUserAddressInfo: function() {
      return $http.get("/api/address");
    },
    getUserAddressById: function(id) {
      return $http.get("/api/address/" + id);
    },
    addUserAddressInfo: function(data) {
      return $http.post("/api/address", $httpParamSerializerJQLike(data), {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        }
      });
    },
    updateUserAddress: function(id, data) {
      return $http.put("/api/address/" + id, $httpParamSerializerJQLike(data), {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        }
      });
    },
    deleteUserAddressInfo: function(itemId) {
      return $http.delete("/api/address/" + itemId);
    },
    setDefaultAddressInfo: function(itemId) {
      return $http.patch("/api/address/" + itemId + "/default");
    },
    search: function(params) {
      return $http.post("/api/search", params);
    },
    quickSearch: function(queryParams) {
      const params = {};
      if(!isNaN(+queryParams)) {
        params.id = queryParams;
      } else {
        params.name = queryParams;
      }
      return $http.post("/api/quick-search", params);
    },
    generateQuickQuote: function(data) {
      return $http.post("/api/product/quick-quote", data);
    },
    sendQuickQUteByEmail: function(data) {
      return $http.post("/api/product/quick-quote/email", data);
    },
    socialLogin: function(provider, authCode) {
      return $http.get("/api/oauth/" + provider + "/login?code=" + authCode);
    },
    getArtProofs: function() {
      return $http.get("/api/art-proofs?statuses[]=pending");
    },
    approveArtProof: function(id, text) {
      return $http.patch("/api/art-proofs/" + id + "/approve", {
        note: text
      });
    },
    declineArtProof: function(id, text) {
      return $http.patch("/api/art-proofs/" + id + "/denied", {
        note: text
      });
    },
    noteArtProof: function(id, text) {
      return $http.post("/api/art-proofs/" + id + "/store-note", {
        note: text
      });
    },
    uploadTaxExemptionCertificate: function(data) {
      return $http.post("/api/tax-exemption-certificate/upload", data, {
        withCredentials: true,
        headers: { "Content-Type": undefined },
        transformRequest: angular.identity
      });
    },
    getColors: function() {
      return $http.get("/api/colors");
    },
    getQuickLinks: function() {
      return $http.get("/api/quick-links");
    },
    userNotesApprovement: function(id, data) {
      return $http.put(
        "/api/order/user-note/" + id,
        $httpParamSerializerJQLike(data),
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        }
      );
    },
    getNewArtproofs: function() {
      return $http.get("/api/art-proofs/unread");
    },
    getNewNotes: function() {
      return $http.get("/api/order/user-note/unread");
    },
    sendNoteStatusRead: function(id) {
      return $http.patch("/api/order/user-note/" + id + "/read");
    },
    sendArtproofStatusRead: function(id) {
      return $http.patch("/api/art-proofs/" + id + "/read");
    },
    getShippingData: function(id, zipcode, quantity) {
      return $http.get(
        "/api/product/get-shipping-cost?product_id=" +
          id +
          "&zip_code=" +
          zipcode +
          "&quantity=" +
          quantity
      );
    },
    getBlogData: function(page) {
      return $http.get("/api/blog/posts?page=" + page);
    },
    getCaseStudies: function() {
      return $http.get("/api/case-study");
    },
    getFAQs: function() {
      return $http.get("/api/faq");
    },
    getStaticData: function() {
      $http.get("/api/testimonial");
    },
    getNewsletterSubscriptionBlock: function(type) {
      return $http.get(`/api/block/${type}`);
    },
    subscribeToNewsletter: function(email) {
      return $http.post("/api/subscribe/coupon", { email });
    },
    addDiscountCoupon: function(discount_code) {
      return $http.post("/api/cart-discount", { discount_code });
    },
    deleteDiscountCoupon: function() {
      return $http.delete("/api/cart-discount");
    },
    checkDiscountCoupon: function() {
      return $http.get("/api/cart-discount");
    },
    subscribe: function(email) {
      return $http.post("/api/subscribe/newsletter", { email });
    },
    getPromos: function() {
      return $http.get("/api/promos");
    },
    getProductPdf: function(productId) {
      return $http.get(`/api/product/${productId}/pdf`, {responseType: 'arraybuffer'});
    },
    getSlideImages: function() {
      return $http.get(`/api/slider`);
    },
    getPromotionalGlossary: function() {
      return $http.get(`/api/promotional-glossary`);
    },
    getPromotions: function() {
      return $http.get(`/api/promotions`);
    }
  };
}
