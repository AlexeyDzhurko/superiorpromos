import apiService from './service';

export default angular.module('apiService', [])
    .service('apiService', apiService)
    .name