flashMessagesService.$inject = [];
export default function flashMessagesService() {

    this.message = '';
    this.fields = {};
    this.isSuccess = true;

    this.setSuccessMessage = function (message) {
        this.message = message;
        this.isSuccess = true;
    };

    this.setErrorMessage = function (message) {
        this.message = message;
        this.isSuccess = false;
    };

    this.setFieldErrorMessage = function (fieldName, message) {
        this.fields[fieldName] = message;
        this.isSuccess = false;
    };

    this.clearAll = function () {
        this.message = '';
        this.isSuccess = true;
    }

}