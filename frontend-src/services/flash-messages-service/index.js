import flashMessagesService from './service';

export default angular.module('flashMessagesService', [])
    .service('FlashMessagesService', flashMessagesService)
    .name
