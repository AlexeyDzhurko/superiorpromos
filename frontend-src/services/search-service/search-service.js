searchService.$inject = ['apiService', '$state', '$rootScope'];
export default function searchService(apiService, $state, $rootScope) {
    var search = {
        pagination:{
            pages:[]
        },
        itemsPerPage: 30,
        isPaginateVisible: false,
        query:{},
        results:[],
        breadcrumbs: [],
        sortOptions:{  //default sort options
            sortBy:'lowest_price',
            direction:'ascending'
        },
        catSearchQuery:""
    },
        _ascSort = function (a,b, field) {
            return a[field] - b[field];
        },
        _dscSort = function (a,b, field) {
            return b[field] - a[field];
        },
        _doSort = function (sortOptions) {
            switch (sortOptions.direction){
                case 'ascending': {
                    search.results.sort(function (a,b) {
                        return _ascSort(a,b, sortOptions.sortBy);
                    });
                    break;
                }
                case 'descending' : {
                    search.results.sort(function (a,b) {
                        return _dscSort(a,b, sortOptions.sortBy);
                    });
                    break;
                }

            }
        },
    _generatePages = function (data) {
        search.pagination = {};
        if (data && data.total_pages > 0){
            search.pagination.current_page = data.current_page;
            search.pagination.total = data.total;
            search.pagination.total_pages = data.total_pages;
        } else {
          search.pagination.current_page = 1;
          search.pagination.total = data.total;
          search.pagination.total_pages = data.total_pages;
        }
        search.isPaginateVisible = false;
    };
    
    search.find = function () {
        //todo:here 7
        search.results = [];
        sessionStorage.setItem('lastQuery', JSON.stringify(search.query));

        let
            view = $state.params.view,
            slug = search.query.category_slug;

        return apiService.getProductBySlug(slug).then(
            function (res) {
                if (res && !res.data) {
                    if (!view) {
                        $state.go('router.customize-order.options', {slug: slug});
                    } else {
                        $state.go('router.customize-order.options', {slug: slug, view: view});
                    }
                } else {
                    return apiService.search(search.query).then(function (res) {
                        _generatePages(res.data);
                        search.results = res.data.data;
                        search.breadcrumbs = res.data.breadcrumbs;
                        return search.results;
                    })
                }
            }, function (err) {
                search.query = JSON.parse(sessionStorage.getItem('lastQuery'));
                return apiService.search(search.query).then(function (res) {
                    _generatePages(res.data);
                    search.results = res.data.data;
                    search.breadcrumbs = res.data.breadcrumbs;
                    return search.results;
                })
            });
    };

    search.findById = function (showQuickSearch) {
        apiService.search(search.query).then(function (res) {
            if (res.data.data.length) {
                var product = res.data.data.shift();
                $state.go("router.customize-order.options",
                    {
                        mode: "",
                        slug: product.full_slug
                    },
                    {
                        reload: true
                    });
            }
            document.querySelector('.inp-quick-search').blur();
        });
    };

    search.findByCategory = function () {
        sessionStorage.setItem('lastQuery', JSON.stringify(search.query));
        return apiService.search(search.query).then(function (res) {
            _generatePages(res.data);
            search.results = res.data.data;
            search.breadcrumbs = res.data.breadcrumbs;
            $state.go('router.categories', {viewMode: 'grid', slug: search.query.category_slug});
            // _doSort(search.sortOptions);
            return search.results;
        })
    };

    search.page = 1;
    search.loadOneMorePage = function () {
        const query = {
            order: search.query.order,
            category_id: search.query.category_id,
            sorting: search.query.sorting,
            page: ++search.page,
            per_page: 5
        };
        return apiService.search(query).then(function (res) {
            return res.data.data;
        })
    };

    search.queryAddColor = function (id) {
        if (!search.query.color_ids){
            search.query.color_ids = [];
        }
        search.query.color_ids.push(id);
    };

    search.queryRemoveColor = function (id) {
        search.query.color_ids.splice(search.query.color_ids.indexOf(id), 1);
    };
    
    search.sortResults = function (sortOptions) {
        search.sortOptions = sortOptions;
        _doSort(search.sortOptions);
    };

    search.paginate = function () {
        search.query = JSON.parse(sessionStorage.getItem('lastQuery'));
        search.query.page = search.pagination.current_page;
        search.find();
        search.switchPagination();
        search.clearQuery();
    };

    search.setItemPerPage = function (item) {
      search.itemsPerPage = item;
    };

    search.switchPagination = function () {
      search.isPaginateVisible = !search.isPaginateVisible;
    };

    search.hasQuery = function () {
        return Object.keys(search.query).length;
    };

    search.clearQuery = function () {
        search.query = {};
        search.clearSearchInsideCategory();
    };

    search.setQueryData = function (name, category, slug, id, params = {}) {
        search.query.name = name;
        if (category) {
            search.query.category_id = category;
        }
        if (slug) {
            search.query.category_slug = slug;
        }
        if (id) {
            search.query.id = id;
        }
        search.query = Object.assign(search.query, params);

        return search.query;
    };
    search.setAdvancedQueryData = function(query, catId) {
        search.query.category_id = catId;
        if (isNaN(+query)) {
            search.query.name = query;
            search.query.id = null;
        } else {
            search.query.name = null;
            if (query.length === 0) {
                search.query.id = null;
            } else
                search.query.id = +query;
        }
    };
    search.searchInsideCategory = function (query) {
      search.catSearchQuery = query;
    };
    search.clearSearchInsideCategory = function () {
        search.catSearchQuery = "";
    };
    search.hasResult = function () {
      return search.results.length !== 0;
    };

    return search;
}
