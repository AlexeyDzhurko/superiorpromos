import searchService from './search-service';

export default angular.module('searchService', [])
    .service('$search', searchService)
    .name
