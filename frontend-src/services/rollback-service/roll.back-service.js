rollBackService.$inject = ['$transitions', '$search', '$state', 'localStorageService'];
export default function rollBackService($transitions, $search, $state, $localStorage) {
    var service = {
        route: 'router.home',
        routeFrom: null,
        customizedOrderId: null
    };

    service.detectRouts = function () {

        $transitions.onSuccess(null, function(trans) {

            switch(trans.from().name) {
                case 'router.search' :
                case 'router.shopping-cart' :
                case 'router.my-account.saved-cart' :
                case 'router.my-account.wish-list' :
                    service.route = trans.from().name;
                    break;
                case 'router.customize-order.options' :
                    service.routeFrom = trans.from().name;
            }

        });

    };

    service.goTo = function () {

        if (service.route === 'router.search') {
            $search.query = JSON.parse(sessionStorage.getItem('lastQuery'));
            $search.find();
            $state.go('router.search');
            $localStorage.remove('orderData' + service.customizedOrderId);
        } else {
            service.routeFrom === 'router.customize-order.options' ? $localStorage.remove('orderData' + service.customizedOrderId) : null;
            $state.go(service.route);
        }

    };

    service.setCustomizedOrderId = function (id) {
        service.customizedOrderId = id;
    };

    return service;
}