import rollBackService from './roll.back-service';

export default angular.module('rollBackService', [])
    .service('$rollBack', rollBackService)
    .name
