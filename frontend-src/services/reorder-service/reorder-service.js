reorderService.$inject = ['apiService', '$state', '$anchorScroll', 'cartService', 'customizeOrderService', 'appUserService', 'localStorageService'];

export default function reorderService(apiService, $state, $anchorScroll, cartService, orderService, appUserService, $localStorage) {
    var service = {
        reorders: [],
        paymentData: {},
        reordersToSend: [],
        reorderedItems: []
    };
    service.imprintSelector = {};

    service.saveAllChanges = function (reorder) {
        reorder.editIsActive = true;
    };

    service.editRequest = function (reorder) {
        reorder.editIsActive = false;
    };

    service.clearReorders = function () {
        service.reorders = [];
    };

    service.reorder = function (params) {
        var data = {
            order_item_ids: params
        };
        apiService.reorderCartItems(data).then(function (res) {
            service.reorders = cartService.getReorderItems(res);
            $state.go('router.my-account.reorder', null, {location:'replace'}).then(function () {
                $anchorScroll.yOffset = 0;
                $anchorScroll();
            });
            service.setDefaultParams();
        });
    };

    service.setDefaultParams = function () {
        service.paymentData = angular.copy(appUserService.user.payment_profiles);
        var i, len = service.paymentData.length;
        for (i = 0; i < len; i++) {
           if (service.paymentData[i].is_default) {
               service.defaultPaymentId = service.paymentData[i].id;
               break;
           }
        }
        angular.forEach(service.reorders, function (item) {
            item.sameContents = 'yes';
            item.isQuantityChange = 'no';
            item.isItemColorChange = 'no';
            item.isImprintColorChange = 'no';
            item.isNewArtwork = 'no';
            item.deliveredByAsap = 'asap';
            item.isShippingSame = 'yes';
            item.isBillingSame = 'yes';
            item.isPaymentSame = 'yes';
            item.comment = '';
            item.shippingData = {};
            item.shippingData.country = 'US';
            item.billingData = {};
            item.billingData.country = 'US';
            item.shippingSendData = {};
            item.billingSendData = {};
            item.payment = service.defaultPaymentId;
            item.selectedImprints = {};
            item.productImprints = angular.copy(item.cartItem.getData().imprints);
            orderService.init(item.cartItem);
        });
    };

    service.submitShipping = function (reorder) {
        reorder.shippingData.type = 1;
        apiService.addUserAddressInfo(reorder.shippingData).then(function (res) {
            reorder.shippingAddressId = res.data.id;
        });
    };

    service.submitBilling = function (reorder) {
        reorder.billingData.type = 2;
        apiService.addUserAddressInfo(reorder.billingData).then(function (res) {
            reorder.billingAddressId = res.data.id;
        });
    };

    service.formatDate = function(order){
        if(order && order.shipTimeValue) {
            var d = new Date(order.shipTimeValue),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();
            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            return [year, month, day].join('-');
        } else {
            return null;
        }
    };

    service.transformSelectedImprints = function (selectedImprints) {
        if(selectedImprints) {
            let imprintsArr = [];
            const _getColorIds = function (colors) {
                    var _colorIds = [];
                    if (colors && colors.length) {
                        for (var i = 0; i < colors.length; i++) {
                            _colorIds.push(colors[i].id);
                        }
                    }
                    return _colorIds;
                };

            for (var key in selectedImprints) {
                if (selectedImprints.hasOwnProperty(key) && selectedImprints[key].selected) {
                    imprintsArr.push({
                        imprint_id: key,
                        color_ids: _getColorIds(selectedImprints[key].data.colors)
                    });
                }
            }
            return imprintsArr;
        } else {
            return [];
        }
    };

    service.convertDataToSend = function (value) {
        return value === 'no' ? false : true;
    };

    service.addImprintColor = function(imprint, color, reorderId) {
        const reorderIndex = service.reorders.findIndex(function (elem) {
            return elem.id === reorderId;
        });
        if (!service.reorders[reorderIndex].selectedImprints[imprint.id]) {
            service.reorders[reorderIndex].selectedImprints[imprint.id] = {
                selected: true
            };
            service.selectImprint(imprint, reorderId);
        } else if (!service.reorders[reorderIndex].selectedImprints[imprint.id].data) {
            service.reorders[reorderIndex].selectedImprints[imprint.id].selected = true;
            service.selectImprint(imprint, reorderId);
        }
        service.reorders[reorderIndex].selectedImprints[imprint.id].data.colors.push(color);
        service.reorders[reorderIndex].selectedImprints[imprint.id].chosen = null;
    };

    service.replaceImprintColor = function (imprint, color, index, reorderId) {
        const reorderIndex = service.reorders.findIndex(function (elem) {
           return elem.id === reorderId
        });
        let selectedColors = service.reorders[reorderIndex].selectedImprints[imprint.id].data.colors;
        selectedColors.splice(index, 1, color);
    };

    service.selectImprint = function (imprint, reorderId) {
        const reorderIndex = service.reorders.findIndex(function (elem) {
            return elem.id === reorderId;
        });
        const _this = this, _getSelectedPrices = function (prices) {
                if (prices && prices.length){
                    for (var i=0; i< prices.length; i++){
                        // if (_this.orderData.quantity <= prices[i].quantity){
                        return prices[i]
                        // }
                    }
                } else {
                    return null;
                }
            },
            _revertSelectedColors = function (data, imprint) {
                for (var i = 0; i< data.length; i++){
                    imprint.color_group.colors.push(data[i]);
                }
            };
        if (service.reorders[reorderIndex].selectedImprints[imprint.id].selected) {
            service.reorders[reorderIndex].selectedImprints[imprint.id].data = {
                id: imprint.id,
                name: imprint.name,
                prices: _getSelectedPrices(imprint.imprint_prices),
                max_colors: imprint.max_colors,
                colors:[]
            };
        } else {
            _revertSelectedColors(service.reorders[reorderIndex].selectedImprints[imprint.id].data.colors, imprint);
            service.reorders[reorderIndex].selectedImprints[imprint.id].data = null;
        }
    };

    service.isImprintMaxColorsSelected = function(selectedId, maxColors, reorderId) {
        const reorderIndex = service.reorders.findIndex(function (elem) {
           return elem.id === reorderId;
        });
        return service.reorders[reorderIndex].selectedImprints[selectedId] &&
            service.reorders[reorderIndex].selectedImprints[selectedId].data &&
            service.reorders[reorderIndex].selectedImprints[selectedId].data.colors &&
            service.reorders[reorderIndex].selectedImprints[selectedId].data.colors.length == maxColors;
    };

    service.removeImprintColor = function(imprint, color, reorderId) {
        const reorderIndex = service.reorders.findIndex(function (elem) {
            return elem.id === reorderId;
        });
        const _getDeleteIndex = function (data, id) {
                for (var i = 0; i< data.length; i++){
                    if (id === data[i].id) {
                        return i;
                    }
                }
            };
        let selectedColors = service.reorders[reorderIndex].selectedImprints[imprint.id].data.colors;

        selectedColors.splice(_getDeleteIndex(selectedColors, color.id), 1);
    };

    service.prepareReorderToSend = function () {
        service.reordersToSend = [];
        angular.forEach(service.reorders, function (item) {
            var orderData = $localStorage.get('orderData' + item.id);

            service.reordersToSend.push({
                'order_item_id' : item.id || null,
                'is_same_contents' : service.convertDataToSend(item.sameContents),
                'is_quantity_change' : service.convertDataToSend(item.isQuantityChange),
                'quantity' : item.cartItem.orderData.quantity || null,
                'is_item_color_change' : service.convertDataToSend(item.isItemColorChange),
                'product_colors' : orderData ? orderData.selectedColors : [],
                'is_imprint_color_change' : service.convertDataToSend(item.isImprintColorChange),
                'product_imprints' : service.transformSelectedImprints(item.selectedImprints),
                'is_new_artwork_provide' : service.convertDataToSend(item.isNewArtwork),
                'is_delivered_by_asap' : service.convertDataToSend(item.deliveredByAsap),
                'received_date' : service.formatDate(orderData),
                'is_shipping_same' : service.convertDataToSend(item.isShippingSame),
                'shipping_address_id' : item.shippingAddressId || null,
                'is_billing_same' : service.convertDataToSend(item.isBillingSame),
                'billing_address_id' : item.billingAddressId || null,
                'is_payment_same' : service.convertDataToSend(item.isPaymentSame),
                'payment_profile_id' : item.payment || null,
                'comment' : item.comment || null
            });

            $localStorage.remove('orderData' + item.id);

        });

        apiService.reorderCustomizedItems({ orders: service.reordersToSend }).then(function (res) {
            service.reorderedItems = res.data;
            $state.go('router.my-account.thank-page-reorder');
        });

    };

    service.goToOrderHistory = function () {
        $state.go('router.my-account.order-history');
    }

    return service;
}
