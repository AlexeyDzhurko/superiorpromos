import reorderService from './reorder-service';

export default angular.module('reorderService', [])
    .service('$reorder', reorderService)
    .name
