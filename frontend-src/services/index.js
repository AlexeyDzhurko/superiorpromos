import apiService from "./api-service";
import appUserService from "./app-user";
import cartService from "./cart-service";
import flashMessagesService from "./flash-messages-service";
import loginRegistrationService from "./login-registration-service";
import reorderService from "./reorder-service";
import rollBackService from "./rollback-service";
import searchService from "./search-service";
import staticDataServiceModule from "./static-data-service";
import userAccountServiceModule from "./user-account-services";

export default angular.module("appServicesModule", [
  apiService,
  appUserService,
  cartService,
  flashMessagesService,
  loginRegistrationService,
  reorderService,
  rollBackService,
  searchService,
  staticDataServiceModule,
  userAccountServiceModule
]).name;
