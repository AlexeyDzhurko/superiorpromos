loginRegistrationService.$inject = ['$http', 'appUserService'];
export default function loginRegistrationService($http, $appUser) {
    this.steps = {
        'login': {
            id: 'login',
            templateUrl: '/views/partials/loginRegistrationSteps/login.html',
            cssClass: 'login-step'
        },
        'registration': {
            id: 'registration',
            templateUrl: '/views/partials/loginRegistrationSteps/registration.html',
            cssClass: 'registration-step'
        },
        'forgotPassword': {
            id: 'forgotPassword',
            templateUrl: '/views/partials/loginRegistrationSteps/forgotPassword.html',
            cssClass: 'forgotpassw-step'
        },
        'report': {
            id: 'report',
            templateUrl: '/views/partials/loginRegistrationSteps/report.html',
            cssClass: 'report-step'
        }
    };

    this.activeStepView = {};

    this.goToStep = function (stepId) {
        var step = this.getStepById(stepId);
        if (step) {
            this.activeStepView = step;
        }
        if (stepId === 'login') {
            var $body = angular.element(document.body);

            if (this.showLoginModal) {
                $body.addClass('overflow-hidden');
            } else {
                $body.removeClass('overflow-hidden');
            }
        }
    };

    this.checkAuth = function () {
        switch ($appUser.resetToken()) {
            case 'logged-guest' :
                $appUser.showLoginModal = true;
                this.goToStep('login');
                return 'guest';
            case 'auth' :
                return 'authorized';
            default:
        };
    };

    this.checkAuthSecond = function () {
        switch ($appUser.resetToken()) {
            case 'logged-guest' :
                return 'guest';
            case 'auth' :
                return 'authorized';
            default:
        };
    };

    this.getStepById = function (id) {
        return this.steps[id];
    };

    this.getActiveStep = function () {
        return this.getStepById(this.activeStepView.id);
    };
    return this;
}