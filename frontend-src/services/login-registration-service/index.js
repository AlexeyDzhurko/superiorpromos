import loginRegistrationService from './service';

export default angular.module('loginRegistrationService', [])
    .service('LoginRegistrationService', loginRegistrationService)
    .name
