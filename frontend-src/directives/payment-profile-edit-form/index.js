import './edit-form.scss';

import paymentProfileEditFormDirective from './payment-profile-edit-form.directive';

export default angular.module('paymentProfileEditFormDirective', [])
    .directive('paymentProfileEditForm', paymentProfileEditFormDirective)
    .name
