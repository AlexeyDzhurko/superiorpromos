import '../../common/confirm-delete/confirm-delete.scss';
import '../../common/ccv-info/ccv-info.scss';

import view from './payment-profile-edit-form.view.html';

import confirmDeleteView from '../../common/confirm-delete/confirm-delete.html';
import ccvInfoView from '../../common/ccv-info/ccv-info.html';

import confirmDeleteCtrl from '../../common/confirm-delete/confirm-delete-controller';
import ccvInfoController from '../../common/ccv-info/ccv-info-controller';

paymentProfileEditFormDirective.$inject = ['paymentProfileService', '$stateParams', '$state', 'ModalService', '$usStates', 'addressBookService', '$countriesList', '$anchorScroll', '$location'];

export default function paymentProfileEditFormDirective($paymentProfile, $stateParams, $state, $modal, $usStates, $addressBook, $countries, $anchorScroll, $location) {
    return {
        restrict: 'E',
        scope: {
            stayOnPage: '@',
            source: '=?',
            onConfirm: '=',
            onCancel: '='
        },
        template: view,
        link: function (scope) {
            if (!scope.source) {
                scope.source = {}
            }
            scope.source.country = 'US';
            scope.$usStates = $usStates;
            scope.$stateParams = $stateParams;
            scope.$state = $state;
            scope.$addressBook = $addressBook;
            scope.monthYear = '';
            scope.$countriesList = $countries;

            scope.switchAddressForm = function (data) {
                if (scope.isNewAddressActive) {
                    scope.source = Object.assign({}, scope.source, data);
                    scope.source.address1 = scope.source.address_line_1,
                        scope.source.address2 = scope.source.address_line_2,
                        scope.source.company = scope.source.company_name,
                        scope.source.phone = scope.source.day_telephone,
                        scope.source.phone_extension = scope.source.ext
                }
            };

            setTimeout(() => {
                if (scope.source.expiration_month) {
                    let formatExpMonth = scope.source.expiration_month < 10 ? scope.source.expiration_month : scope.source.expiration_month;
                    scope.monthYear = formatExpMonth + '/' + scope.source.expiration_year;
                    scope.$apply();
                }
            });

            scope.openWhatsThis = function () {
                $modal.showModal({
                    template: ccvInfoView,
                    controller: ccvInfoController,
                })
            };        


            scope.setMonthAndYear = function (event) {
                event.preventDefault()
                if (event.keyCode === 8) {
                    if (scope.monthYear.length === 3) {
                        scope.monthYear = scope.monthYear.slice(0, -2);
                    } else if (scope.monthYear.length >= 1) {
                        scope.monthYear = scope.monthYear.slice(0, -1);
                    }
                } else {
                    if (Number.isInteger(+event.key) && scope.monthYear.length === 1) {
                        scope.monthYear += event.key + '/';
                    } else {
                        if (Number.isInteger(+event.key) && scope.monthYear.length < 5) {
                            scope.monthYear += event.key;
                        }
                    }
                }
                const month = scope.monthYear.split('/')[0];
                const year = scope.monthYear.split('/')[1];
                if (month && month.length === 2 && +month > 0 && +month < 13) {
                    this.source.expiration_month = month;
                } else {
                    this.source.expiration_month = '';
                }
                if (year && year.length === 2 && +year > 0 && +year < 100) {
                    this.source.expiration_year = '20' + year.toString();
                } else {
                    this.source.expiration_year = '';
                }
            };

            scope.newAddressToggler = function () {
                scope.isNewAddressActive = !scope.isNewAddressActive;
            };

            scope.resetError = function (field) {
                field.$error = {};
            };

            scope.handleErrors = function (errors) {
                for (var key in errors.data) {
                    if (errors.data.hasOwnProperty(key)) {
                        if (key === 'expiration_month' || key === 'expiration_year') {
                            scope.paymentProfileEditForm['expiration_month_and_year'].$error.backend = 'The expiration month and year  field is required.';
                        } else if (key === 'message') {
                            scope.paymentProfileEditForm['message'] = 'A duplicate customer payment profile already exists.';

                            function findPos(obj) {
                                var curtop = -150;
                                if (obj.offsetParent) {
                                    do {
                                        curtop += obj.offsetTop;
                                    } while (obj = obj.offsetParent);
                                    return [curtop];
                                }
                            }
                            window.scroll(-150, findPos(document.getElementById("err")));
                        } else {
                            scope.paymentProfileEditForm[key].$error.backend = errors.data[key].join(' ');

                            function findPos(obj) {
                                var curtop = -150;
                                if (obj.offsetParent) {
                                    do {
                                        curtop += obj.offsetTop;
                                    } while (obj = obj.offsetParent);
                                    return [curtop];
                                }
                            }
                            window.scroll(-150, findPos(document.getElementById("err")));
                        }
                    }
                }
            };

            scope.submit = function () {
                if (scope.source.country !== 'US') {
                    scope.source.state = '';
                }
                scope.busy = true;
                if (scope.$stateParams.mode == 'edit') {
                    $paymentProfile.updatePaymentProfile($stateParams.profileId, scope.source).then(function () {
                        $state.go('router.my-account.payment-profile');
                    }, scope.handleErrors)
                } else {
                    $paymentProfile.createPaymentProfile(scope.source).then(
                    function (res) {
                        var profile = $paymentProfile.addLocalPaymentProfile(res.data)
                        if (typeof scope.onConfirm === 'function') {
                            scope.onConfirm(profile)
                            location.reload()
                        }
                        scope.source = {};
                        if (!scope.stayOnPage) {
                            $state.go('router.my-account.payment-profile');
                            location.reload()
                        }
                    }, scope.handleErrors);
                }
            };


            scope.back = function () {
                if (typeof scope.onCancel === 'function') {
                    scope.onCancel();
                } else {
                    $state.go('router.my-account.payment-profile')
                }
            };

            scope.delete = function (id) {
                $paymentProfile.deletePaymentProfile(id)
                .then(function () {
                    $state.go('router.my-account.payment-profile', { location: 'replace' })
                }, scope.handleErrors);
            };

            scope.confirmDelete = function (id) {
                $modal.showModal({
                    template: confirmDeleteView,
                    controller: confirmDeleteCtrl,
                    inputs: {
                        source: 'Are you sure want to remove this payment?'
                    }
                }).then(function (modal) {
                    modal.close.then(function (confirmed) {
                        if (confirmed) {
                            scope.delete(id);
                        }
                    });
                });
            };

            scope.flipCardToBack = function (data) {
                scope.resetError(data);
                scope.cardState = 'flipped';
            };

            scope.flipCardToFront = function (data) {
                scope.resetError(data);
                scope.cardState = '';
                scope.paymentProfileEditForm['message']
            };

            scope.deleteDuplicateError = function (data) {
                scope.paymentProfileEditForm['message'] = ''
            };

            scope.getMaskType = function (cardType) {
                var masks = {
                    'mastercard': '**** **** **** ****',
                    'visa': '**** **** **** ****',
                    'amex': '**** ****** *****',
                    'diners': '**** **** **** **',
                    'discover': '**** **** **** ****',
                    'unknown': '**** **** **** ****',
                    '': '**** **** **** ****'
                };
                return masks[cardType.toLowerCase()];
            };

            scope.getCreditCardType = function (creditCardNumber) {
                // start without knowing the credit card type
                var result = 'Unknown';
                if (/\*/.test(creditCardNumber)) {
                    result = ''
                }
                // first check for MasterCard
                if (/^5[1-5]/.test(creditCardNumber)) {
                    result = "Mastercard";
                }
                // then check for Visa
                else if (/^4/.test(creditCardNumber)) {
                    result = "Visa";
                }
                // then check for AmEx
                else if (/^3[47]/.test(creditCardNumber)) {
                    result = "Amex";
                }
                // // then check for Diners
                // else if (/3(?:0[0-5]|[68][0-9])[0-9]{11}/.test(creditCardNumber)) {
                //     result = "Diners"
                // }
                // then check for Discover
                else if (/6(?:011|5[0-9]{2})[0-9]{0,12}/.test(creditCardNumber)) {
                    result = "Discover";
                }
                return result;
            };

            scope.setMask = function (val) {
                scope.mask = scope.getMaskType(scope.getCreditCardType(val));
            };

            scope.$watchCollection('source', function (val) {
                scope.setMask(val.card_number);
            });

            scope.getCardClassName = function (card) {
                return scope.getCreditCardType(card.card_number).toLowerCase() || card.card_type;
            };

            scope.validateMonth = function (month) {
                switch (_validateMM(month)) {
                    case true: month.expiration_month = '0' + month.expiration_month;
                        break;
                    case false: month.expiration_month = undefined;
                        break;
                    default:
                }
            };

            var _validateMM = function (month) {
                if (month && month.expiration_month) {
                    if (month.expiration_month.length === 1
                        && /[1-9]/.test(month.expiration_month.substr(0, 1))) {
                        return true;
                    } else if (
                        month.expiration_month.length === 2
                        && !/[0,1]/.test(month.expiration_month.substr(0, 1))
                    ) {
                        return false;
                    } else if (
                        month.expiration_month.length === 2
                        && /1/.test(month.expiration_month.substr(0, 1))
                        && !/[0-2]/.test(month.expiration_month.substr(1, 2))
                    ) {
                        return false;
                    } else if (
                        month.expiration_month.length === 2
                        && /[0-1]/.test(month.expiration_month.substr(0, 1))
                        && /[1-9]/.test(month.expiration_month.substr(1, 2))
                    ) {
                        return 1;
                    }
                    return false;
                }
                return false;
            };
        }
    }
}
