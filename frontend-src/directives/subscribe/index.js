import './subscribe.scss';

import subscribeDirective from './subscribe.directive.js';

export default angular.module('subscribeDirective', [])
    .directive('subscribe', subscribeDirective)
    .name
