import view from './subscribe.view.html';

// import apiService from "../../services/api-service/service";

subscribeDirective.$inject = ['$state', 'apiService'];

export default function subscribeDirective($state, $apiService) {
    return {
        restrict: 'E',
        scope: {
            source:'=',
            onConfirm: '=',
            mode: '@'
        },
        template:view,
        link:function (scope) {
            scope.email = '';
            scope.isSuccessSubscribe = false;
            scope.regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            scope.subscribe = function () {
                /*todo: check catch method  and add api Endpoint */
                scope.isSuccessSubscribe = true;
                $apiService.subscribe(scope.email).then(response => {
                    scope.isSuccessSubscribe = true;
                }).catch(error => {

                })
            };
        }
    }
}
