import view from './category-tree.view.html';

categoryListDirective.$inject = ['$search', '$categoriesData', '$state'];

export default function categoryListDirective($search, $categories, $state) {
    return {
        restrict: 'E',
        template: view,
        link: function (scope) {
            scope.$categories = $categories;
            scope.data = [''];
            scope.$search = $search;

            scope.$on('broadcastCategories', function () {
                scope.productCategories = $categories.getCategories();
            });

            if (!scope.productCategories || !scope.productCategories.length) {
                scope.productCategories = $categories.getCategories();
            }

            scope.remove = function (self) {
                self.remove();
            };

            scope.tree = {
                enabled: true,
                disabled: false,
            };

            scope.switchCategory = function (category) {
                scope.expandSuspendCategory(scope.productCategories, category.id).childCategories;
            };

            scope.expandSuspendCategoryTree = function (categories, id) {
                $categories.getSubCategories(id).then(function (subCategories) {
                    for (let key in scope.productCategories) {
                        if (scope.productCategories[key].id === id) {
                            scope.productCategories[key]['sub_categories'] = subCategories;
                            scope.productCategories[key]['expanded'] = true;
                        } else {
                            scope.productCategories[key]['expanded'] = false;
                        }

                        if (scope.productCategories[key].sub_categories && scope.productCategories[key].id !== id) {
                            scope.productCategories[key]['expanded'] = scope.setExpandCategories(scope.productCategories[key], subCategories, id);
                        }
                    }
                });
            };

            scope.moveLastToTheBeginning = function () {
                var a = scope.data.pop();
                scope.data.splice(0, 0, a);
            };

            scope.newSubItem = function (self) {
                var nodeData = self.$modelValue;
                nodeData.nodes.push({
                    id: nodeData.id * 10 + nodeData.nodes.length,
                    title: nodeData.title + '.' + (nodeData.nodes.length + 1),
                    nodes: []
                });
            };

            scope.search = function (category) {
                scope.showFilters = false;

                if (category.expanded) {
                    category.expanded = !category.expanded;
                    return false;
                } else {
                    scope.expandSuspendCategoryTree(scope.productCategories, category.id);
                }

                //TODO: here2
                $search.query = {
                    category_slug: category.slug
                };

                if (typeof scope.onSearch === 'function') {
                    scope.onSearch();
                }

                _expandCategoriesTree($categories.getCategories(), category.id);

                $state.go('router.categories', {viewMode: 'grid', slug: category.slug});
            };

            scope.$watch('$search.catSearchQuery', function (newVal) {
                if (newVal) {
                    _processCategoryTree($categories.getCategories(), null, null, true); //expand all matched nodes
                } else {
                    _processCategoryTree($categories.getCategories(), null, null, false); //collaps all nodes
                }
            });

            scope.$on('$clearCategoriesTree', function (event, data) {
                _processCategoryTree($categories.getCategories(), null, null, false);
            });

            //expand nodes related to pressed node
            var _expandCategoriesTree = function (data, id) {
                angular.forEach(data, function (item) {
                    if (id === item.id) {
                        item.expanded = true;
                        _expandCategoriesTree($categories.getCategories(), item.parentId);
                    }
                    if (item.sub_categories && item.sub_categories.length) {
                        _expandCategoriesTree(item.sub_categories, id);
                    }
                });
            };

            //make refs for parent obj by adding parentId key for child els, make active pressed node, collapse all nodes
            var _processCategoryTree = function (data, id, parentId, isExpanded) {
                angular.forEach(data, function (item) {
                    if (parentId) {
                        item.parentId = parentId;
                    }
                    if (id === item.id) {
                        item.active = true;
                    } else {
                        item.active = false;
                        item.expanded = isExpanded;
                    }
                    if (item.sub_categories && item.sub_categories.length) {
                        _processCategoryTree(item.sub_categories, id, item.id, isExpanded);
                    }
                })
            };
        }
    }
}
