import './category-tree.scss';
import categoryTreeDirective from './category-tree.directive.js';

export default angular.module('categoryTreeDirective', [])
  .directive('categoryTree', categoryTreeDirective)
  .name
