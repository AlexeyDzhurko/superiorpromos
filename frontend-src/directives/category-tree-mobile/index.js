import './category-tree-mobile.scss';
import categoryListMobileDirective from './category-tree-mobile.directive.js';

export default angular.module('mobileCategoryTreeDirective', [])
  .directive('mobileCategoryTree', categoryListMobileDirective)
  .name
