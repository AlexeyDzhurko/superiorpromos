import view from './category-tree-mobile.view.html';

categoryListMobileDirective.$inject = ['$rootScope', '$search', '$document', '$categoriesData', '$state'];

export default function categoryListMobileDirective($rootScope, $search, $document, $categories, $state) {
    return {
        restrict: 'E',
        template: view,
        link: function (scope) {
            scope.$categories = $categories;
            scope.data = [''];
            scope.$search = $search;
            scope.collapsedStepsCounter = 0;
            scope.categoryIdsList = [];

            scope.$on('broadcastCategories', function () {
                scope.productCategories = $categories.getCategories();
            });

            if (!scope.productCategories || !scope.productCategories.length) {
                scope.productCategories = $categories.getCategories();
            }

            scope.remove = function (self) {
                self.remove();
            };

            scope.tree = {
                enabled: true,
                disabled: false,
            };

            scope.switchCategory = function (event, category) {
                event.stopPropagation();

                if (!category.sub_categories_exist) {
                    scope.mobileSearch(category);
                    return ;
                }

                if (category.expanded) {
                    category.expanded = !category.expanded;
                } else {
                    scope.expandSuspendCategory(category);
                }
            };

            scope.closeCategory = function (category) {
                category.expanded = false;
                scope.collapsedStepsCounter -= 1;

                if (scope.categoryIdsList[scope.categoryIdsList.length - 1] === category.id) {
                    scope.categoryIdsList.pop();
                }
            };

            scope.expandSuspendCategory = function (category) {
                return $categories.getSubCategories(category.id).then(function (data) {
                    return scope.setSubCategories(data, category);
                });
            };

            scope.setSubCategories = function (res, category) {
                category.sub_categories = res;
                category.expanded = true;
            };

            scope.setExpandCategories = function (category, data, parentId) {
                let expandex = false;

                for (let key in category.sub_categories) {
                    if (category.sub_categories[key].id === parentId) {
                        category.sub_categories[key]['sub_categories'] = data;
                        category.expanded = true;
                        expandex = true;

                        break;
                    } else {
                        category.expanded = false;
                    }

                    if (category.sub_categories[key].sub_categories && category.sub_categories[key].id !== parentId) {
                        category.expanded = scope.setExpandCategories(category.sub_categories[key], data, parentId);
                        expandex = category.expanded;
                    }
                }

                return expandex;
            };

            scope.moveLastToTheBeginning = function () {
                var a = scope.data.pop();
                scope.data.splice(0, 0, a);
            };

            scope.newSubItem = function (self) {
                var nodeData = self.$modelValue;
                nodeData.nodes.push({
                    id: nodeData.id * 10 + nodeData.nodes.length,
                    title: nodeData.title + '.' + (nodeData.nodes.length + 1),
                    nodes: []
                });
            };

            scope.mobileSearch = function (category) {
                scope.showFilters = false;
                scope.categoryIdsList.push(category.id);
                //TODO: here2
                $search.query = {
                    category_slug: category.slug
                };

                if (typeof scope.onSearch === 'function') {
                    scope.onSearch();
                }

                _expandCategories($categories.getCategories(), category.id);

                // NOTE: scrolls filter panel to top
                setTimeout(() => {
                    document.querySelector(`#scrollable-anchor-${category.id}`).scrollIntoView();
                }, 1000)
                // NOTE: if category has sub-categories so avoid call to API
                if (category.sub_categories && category.sub_categories.length) {
                    scope.collapsedStepsCounter += 1;
                    return;
                }
                // NOTE: closes filter panel
                $rootScope.filterMenuActive = false;

                $state.go('router.categories', {viewMode: 'grid', slug: category.slug}).then(function () {
                    scope.collapsedStepsCounter = 0;
                    scope.categoryIdsList = [];
                    _processCategoryTree($categories.getCategories(), null, null, false);
                });
            };

            scope.$watch('$search.catSearchQuery', function (newVal) {
                if (newVal) {
                    _processCategoryTree($categories.getCategories(), null, null, true); //expand all matched nodes
                } else {
                    _processCategoryTree($categories.getCategories(), null, null, false); //collaps all nodes
                }
            });

            scope.$on('$clearCategoriesTree', function (event, data) {
                _processCategoryTree($categories.getCategories(), null, null, false);
            });

            //expand nodes related to pressed node
            var _expandCategories = function (data, id) {
                angular.forEach(data, function (item) {
                    if (id === item.id) {
                        item.expanded = true;
                        _expandCategories($categories.getCategories(), item.parentId);
                    }
                    if (item.sub_categories && item.sub_categories.length) {
                        _expandCategories(item.sub_categories, id);
                    }
                });
            };

            //make refs for parent obj by adding parentId key for child els, make active pressed node, collapse all nodes
            var _processCategoryTree = function (data, id, parentId, isExpanded) {
                angular.forEach(data, function (item) {
                    if (parentId) {
                        item.parentId = parentId;
                    }
                    if (id === item.id) {
                        item.active = true;
                    } else {
                        item.active = false;
                        item.expanded = isExpanded;
                    }
                    if (item.sub_categories && item.sub_categories.length) {
                        _processCategoryTree(item.sub_categories, id, item.id, isExpanded);
                    }
                })
            };

        }
    }
}
