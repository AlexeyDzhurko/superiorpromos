import view from './color-filter.view.html';

colorFilterDirective.$inject = ['$search'];

export default function colorFilterDirective($search) {
    return {
        restrict: 'E',
        template: view,
        scope: {
            colors:'='
        },
        link: function (scope) {
            scope.maxVisible = 50;
            scope.$search = $search;
            scope.selectColor = function (color) {
                color.isActive = !color.isActive;
                color.isActive ? $search.queryAddColor(color.id) : $search.queryRemoveColor(color.id);
                $search.find();
            }
        }
    }
}
