// import './search-filters.scss';
// import './quick-filters.scss';

import colorFilterDirective from './color-filter.directive';

export default angular.module('colorFilterDirective', [])
    .directive('colorFilter', colorFilterDirective)
    .name
