closeModalDirective.$inject = ['$document'];

export default function closeModalDirective($document) {
    return {
        restrict: 'A',
        link: function (scope, element) {
            $document.on('click', function (event) {
                if (!angular.element(event.target).hasClass('close-outside')) {
                    angular.element(element).addClass('hidden');
                }
            });
        }
    }
}
