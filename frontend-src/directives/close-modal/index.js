import closeModalDirective from './close-modal.directive.js';

export default angular.module('closeModalDirective', [])
    .directive('closeModal', closeModalDirective)
    .name
