import './main-view-toggle.scss';
import mainViewToggleDirective from './main-view-toggle.directives.js';

export default angular.module('mainViewToggleDirective', [])
    .directive('mainViewToggle', mainViewToggleDirective)
    .name
