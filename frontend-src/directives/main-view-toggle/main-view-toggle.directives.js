mainViewToggle.$inject = ['$state', '$rootScope'];

export default function mainViewToggle($state, $rootScope) {
    return {
        restrict: 'A',
        replace: true,
        scope: {},
        link: function(scope, element) {

            $rootScope.$on('$viewContentLoaded',
                function(event, toState, toParams, fromState, fromParams){
                    switch ($state.current.name) {
                        case 'router.customize-order.options':
                            element[0].classList.add('grey-background');
                            break;
                        case 'router.customize-order.step-1':
                            element[0].classList.add('grey-background');
                            break;
                        case 'router.customize-order.step-2':
                            element[0].classList.add('grey-background');
                            break;
                        case 'router.customize-order.step-3':
                            element[0].classList.add('grey-background');
                            break;
                        case 'router.customize-order.step-4':
                            element[0].classList.add('grey-background');
                            break;
                        case 'router.customize-order.artwork':
                            element[0].classList.add('grey-background');
                            break;
                        case 'router.customize-order.order-sample':
                            element[0].classList.add('grey-background');
                            break;
                        case 'router.cart-checkout':
                            element[0].classList.add('grey-background');
                            break;
                        case 'router.ready-order':
                            element[0].classList.add('grey-background');
                            break;
                        case 'router.shopping-cart':
                            element[0].classList.add('grey-background');
                            break;
                        default:
                            element[0].classList.remove('grey-background');
                            break;

                    }

            });
    }
}
}
