import './pagination.scss';
import paginationDirective from './pagination.directive';

export default angular.module('paginationDirective', [])
    .directive('pagination', paginationDirective)
    .name
