import view from './pagination.view.html';
paginationDirective.$inject = ['$search', '$window'];

export default function paginationDirective($search, $window) {
    return {
        restrict: 'E',
        template:view,
        link:function (scope) {
            scope.$search = $search;
            scope.$window = $window;
            scope.settings = {
                maxItems:4
            };
            scope.$watch('$window.innerWidth', function (width) {
                if (width <=991){
                    scope.settings.maxItems = 4;
                } else {
                    scope.settings.maxItems = 7;
                }
            });
        }
    }
}
