export default function fallbackImageSourceDirective(){
    return {
        restrict: 'A',
        scope: {
            fallbackSrc:'=fallbackSrc'
        },
        link: function(scope, element, attr){
            var defaultSrc = attr.src;
            element.bind('error', function() {
                if(attr.fallbackSrc) {
                    element.attr('src', attr.fallbackSrc);
                }
                else if(attr.src) {
                    element.attr('src', defaultSrc);
                }
            });
        }
    }
}
