import fallbackImageSourceDirective from './directive.js';

export default angular.module('fallbackImageSourceDirective', [])
    .directive('fallbackSrc', fallbackImageSourceDirective)
    .name
