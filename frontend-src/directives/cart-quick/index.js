import './cart-quick.scss';
import cartQuickDirective from './cart-quick.directive.js';

export default angular.module('cartQuickDirective', [])
    .directive('cartQuick', cartQuickDirective)
    .name
