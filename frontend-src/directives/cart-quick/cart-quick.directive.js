import view from './cart-quick.view.html';
import confirmDeleteView from '../../common/confirm-delete/confirm-delete.html';
import confirmDeleteCtrl from '../../common/confirm-delete/confirm-delete-controller';

cartQuickDirective.$inject = [
    '$rootScope',
    'cartService',
    '$state',
    'LoginRegistrationService',
    'ModalService',
    'apiService',
];

export default function cartQuickDirective(
    $rootScope,
    $cartService,
    $state,
    $login,
    $modal,
    $apiService
) {
    return {
        restrict: 'E',
        template: view,
        link: function (scope) {

            $rootScope.$on('updateCoupon', function () {
                scope.checkCoupon();
            });

            scope.$watch('$cartService.cartItems', function (items) {
                scope.loadCartItems(items);
                selectAll(items);
            });

            scope.loadCartItems = function (items) {
                scope.cartItems = items || $cartService.cartItems;
            }

            var _deleteCartItem = function (item) {
                var recalculateCart = function () {
                    scope.totalSelectedPrice -= item.cartItem.cart_item_cost.total;
                }
                $cartService.deleteCartItem(item.id).then(recalculateCart);
            };

            scope.quickCartToggle = function () {
                scope.quickCartVisible = !scope.quickCartVisible;
                selectAll($cartService.cartItems);
            };

            scope.viewCart = function () {
                scope.quickCartToggle();
                $state.go('router.shopping-cart');
            };

            let selectAll = function (items) {
                scope.totalSelectedPrice = 0;
                angular.forEach(items, function (item) {
                    scope.totalSelectedPrice += +item.cartItem.cart_item_cost.total;
                });
            }

            scope.removeCartItem = function (item) {
                $modal.showModal({
                    template: confirmDeleteView,
                    controller: confirmDeleteCtrl,
                    inputs:{
                        source: 'Are you sure want to remove this cart item?'
                    }
                }).then(function(modal) {
                    modal.close.then(function(confirmed) {
                        if (confirmed) {
                            _deleteCartItem(item);
                        }
                    });
                });
            };

            scope.editOrder = function(data) {
                $state.go('router.customize-order.options',{
                    view: 'edit-cart-item',
                    slug: data.cartItem.productData.full_slug,
                }, {
                    reload: true   
                });
            };

            scope.checkout = function () {
                scope.quickCartToggle();
                if ($login.checkAuth() === 'authorized') {
                    $state.go('router.cart-checkout');
                }
            };

            scope.goHome = function () {
                scope.quickCartToggle();
                $state.go('router.home');
            };

            // scope.goToProduct = function (id) {
            //     scope.quickCartVisible = false;
            //     $state.go('router.customize-order.options', {
            //         orderId: id,
            //         mode: 'product'
            //     });
            // }

            scope.goToProduct = function (slug) {
                scope.quickCartVisible = false;
                $state.go('router.customize-order.options', {
                    slug: slug,
                    mode: 'product'
                });
            }

            scope.checkCoupon = function () {
                scope.coupon = {};
                scope.couponError = '';
                $apiService.checkDiscountCoupon().then(response => {
                    scope.coupon = response.data[0];
                }, error => {
                    scope.couponError = error.data.message;
                })
            }

            scope.checkCoupon();
        }
    }
}
