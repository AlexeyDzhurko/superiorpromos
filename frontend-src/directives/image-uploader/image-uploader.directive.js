import view from './image-uploader.view.html';

imageUploaderDirective.$inject = [];

export default function imageUploaderDirective() {
    return {
        restrict: 'E',
        scope: {
            imgUrl: '=?',
            imgAlt: '=?',
            imgClass: '=?',
            forceLoad: '=?'
        },
        template: view,
        replace: true,
        link: function (scope, element) {
            scope.image = '';

            scope.$watch(function() {
                scope.getImageUrl();
            });

            scope.getImageUrl = function (src) {
                var imgUrl = src || scope.imgUrl,
                    width = element[0].offsetWidth !== 0 ? element[0].offsetWidth : 254;

                if (!imgUrl || !imgUrl.length) {
                    return imgUrl;
                }

                if (!imgUrl.includes('http', 0)) {
                    imgUrl = window.location.origin + '/' + imgUrl;
                }

                // scope.image = `/cdn-cgi/image/format=auto,width=${width}/${imgUrl}`;
                scope.image = imgUrl;
                return  scope.image;
            }
        }
    }
}
