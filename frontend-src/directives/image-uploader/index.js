import imageUploaderDirective from './image-uploader.directive';
import './image-uploader.scss';

export default angular.module('c', [])
    .directive('imageUploader', imageUploaderDirective)
    .name;
