import view from './category-list.view.html';

categoryListDirective.$inject = ['$categoriesData', '$state', '$search'];

export default function categoryListDirective($categories, $state, $search) {
    return {
        restrict: 'E',
        template:view,
        scope:{
            instantSearch:'=',
            onSearch:'=',
            text: '@',
            passSelectedCategoryBack: '@',
            title:'@',
            moreIcon: '@',
            subCatsDisabled: '@',
            dial: '&'
        },
        link:function (scope) {
            scope.$categories = $categories;
            scope.selectedItem = '';
            scope.back = function () {
                $categories.goBack();
                if (scope.instantSearch){
                    scope.search($categories.getHistoryItemId());
                }
            };
            scope.search = function (slug) {
                if (typeof scope.onSearch === 'function'){
                    scope.onSearch();
                }
                $search.query.category_slug = slug;
                $state.go('router.categories', {viewMode: 'grid', slug: slug});
            };
            scope.showCategory = function (category) {
                if (scope.subCatsDisabled) {
                    if (scope.passSelectedCategoryBack) {
                        scope.selectedItem = category.id;
                    } else {
                        angular.element(document.body).removeClass('overflow-hidden');
                        scope.search(category.slug);
                    }
                } else {
                    $categories.showCategory(category);
                    if (scope.instantSearch || !category.sub_categories || !category.sub_categories.length){
                        scope.search(category.id);
                    }
                }
            };
            scope.searchFilter = function (data) {
                return data.filter(v => v.title.toLowerCase().includes(scope.text));
            }
        }
      
    }
}
