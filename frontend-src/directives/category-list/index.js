import './category-list.scss';
import categoryListDirective from './category-list.directive.js';

export default angular.module('categoryListDirective', [])
    .directive('categoryList', categoryListDirective)
    .name
