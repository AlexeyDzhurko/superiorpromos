import './toggler.scss';
import togglerDirective from './toggler.directive.js';

export default angular.module('togglerDirective', [])
  .directive('toggler', togglerDirective)
  .name;
