import view from './toggler.view.html';

export default function togglerDirective() {
    return {
        restrict: 'E',
        template: view,
        scope: {
        source: '=',
        product: '=',
        expanded: '@',
        instance: '=',
        bool: '@',
        name: '@'
        },
        replace:true,
        link:function (scope) {
            scope.changeSource = function () {
                if(scope.expanded === 'true' && scope.name !== 'imprints-reorder') {
                    scope.source.selected = !scope.source.selected;
                    if (!scope.product.orderData.imprints[scope.instance.imprint.id]) {
                        scope.product.orderData.imprints[scope.instance.imprint.id] = {};
                        scope.product.orderData.imprints[scope.instance.imprint.id].selected = scope.source.selected;
                    } else {
                        scope.product.orderData.imprints[scope.instance.imprint.id].selected = scope.source.selected;
                    }

                    scope.product.selectImprint(scope.instance.imprint);
                    scope.instance.toggleImprintItemBody(scope.instance.imprint.id, scope.product.orderData.imprints[scope.instance.imprint.id].selected);

                    if (scope.source.selected) {
                        scope.product.addImprintColor(
                            scope.instance.imprint,
                            scope.instance.imprint.color_group.colors.length ? scope.instance.imprint.color_group.colors[0] : null);
                    } else {
                        scope.product.orderData.imprints[scope.instance.imprint.id] = {};
                    }
                }

                if(scope.expanded === 'false' && scope.name !== 'imprints-reorder') {
                    scope.bool ? scope.source = Number(!scope.source) : scope.source = !scope.source;
                    scope.product.orderData.imprints[scope.instance.imprint.id] = {};
                }

                if (scope.name === 'imprints-reorder') {
                    scope.source.selected = !scope.source.selected;
                    if(scope.source.selected) {
                        scope.product.selectedImprints[scope.instance.imprint.id] = {};
                        scope.product.selectedImprints[scope.instance.imprint.id].selected = scope.source.selected;
                        scope.instance.$parent.$parent.$reorder.selectImprint(scope.instance.imprint, scope.product.id);
                    } else {
                        scope.product.selectedImprints[scope.instance.imprint.id] = {}; // clear selectedImprints
                        scope.product.selectedImprints[scope.instance.imprint.id].selected = scope.source.selected;
                    }
                }

                if(scope.expanded === 'false' && scope.name !== 'breakdown') {
                    scope.source.selected = !scope.source.selected;
                }
            };
            scope.$watch('product', function (prod) {
                if (prod && prod.orderData) {
                    switch (scope.name) {
                        case 'tax' :
                            scope.sourceModel = prod && prod.orderData && prod.orderData.taxExemption;
                            break;
                        case 'imprints' :
                            if(prod.orderData.imprints[scope.instance.imprint.id]) {
                                scope.sourceModel = prod.orderData.imprints[scope.instance.imprint.id].selected;
                            }
                            break;
                        case 'breakdown' :
                            scope.sourceModel = prod && prod.orderData && Boolean(prod.orderData.laterBreakdown);
                            break;
                        default:
                    }
                }
            });

        }
    }
}
