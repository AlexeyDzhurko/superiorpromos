
/*import directives*/
import breadcrumbsDirective from './breadcrumbs';
import cartListDirective from './cart-list';
import cartQuickDirective from './cart-quick';
import categoryBtnDirective from './category-btn';
import categoryListDirective from './category-list';
import categoryListMobileDirective from './category-tree-mobile';
import categoryTreeDirective from './category-tree';
import closeModalDirective from './close-modal';
import fallbackImageSourceDirective from './fallback-image-source';
import headerAutoHideDirective from './header-auto-hide';
import paginationDirective from './pagination';
import productPreorderPreviewDirective from './product-preorder-preview';
import pricingInfoDirective from './pricing-info';
import productColorSelectorDirective from './product-color-selector';
import productOptionSelectorDirective from './product-option-selector';
import productViewInputDirective from './product-view-input';
import scrollToElementDirective from "./scroll-to";
import quickLinksDirective from './quick-links';
import colorFilterDirective from './color-filter';
import quickFiltersDirective from './quick-filters';
import viewFiltersDirective from './view-filters';
import searchFiltersDirective from './search-filters';
import subscribeDirective from './subscribe';
import togglerDirective from './toggler';
import addressEditFormDirective from './address-edit-form';
import addressListDirective from './address-list';
import paymentProfileEditFormDirective from './payment-profile-edit-form';
import paymentProfilesListDirective from './payment-profiles-list'
import wishListLikeDirective from './wish-list-like';
import productListDirective from './product-list';
import paginationBlogDirective from './pagination-blog';
import navMenuDirective from './nav-menu';
import mainViewToggleDirective from './main-view-toggle';
import ratingStars from './rating-stars';
import imageUploaderDirective from './image-uploader';


export default angular.module('appDirectivesModule', [
    breadcrumbsDirective,
    cartListDirective,
    cartQuickDirective,
    categoryBtnDirective,
    categoryListDirective,
    categoryListMobileDirective,
    categoryTreeDirective,
    closeModalDirective,
    fallbackImageSourceDirective,
    headerAutoHideDirective,
    paginationDirective,
    productPreorderPreviewDirective,
    pricingInfoDirective,
    productColorSelectorDirective,
    productOptionSelectorDirective,
    productViewInputDirective,
    scrollToElementDirective,
    quickLinksDirective,
    colorFilterDirective,
    quickFiltersDirective,
    viewFiltersDirective,
    searchFiltersDirective,
    subscribeDirective,
    togglerDirective,
    addressEditFormDirective,
    addressListDirective,
    paymentProfileEditFormDirective,
    paymentProfilesListDirective,
    wishListLikeDirective,
    productListDirective,
    paginationBlogDirective,
    navMenuDirective,
    mainViewToggleDirective,
    ratingStars,
    imageUploaderDirective,
]).name;
