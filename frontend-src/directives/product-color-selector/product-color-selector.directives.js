import view from './product-color-selector.view.html';

productColorSelectorDirective.$inject = ['$rootScope', 'customizeOrderService'];

export default function productColorSelectorDirective($rootScope, $customize) {
    return {
        restrict: 'E',
        replace:true,
        template: view,
        scope: {
            selectType: '@',
            defaultFirstSelected: '@',
            productItem: '=',
            typeItem: '=',
            displayMode: '@',
            onColorSelected: '=',
            itemIndex: '=',
            sample: '@',
            error: '='
        },
        link: function (scope) {
            scope.$watch('productItem', function (item) {
                if (item && item.orderData.selectedColor) {
                    scope.typeItem.product_colors.forEach(function(colorItem) {
                        colorItem.color_id == item.orderData.selectedColor.color.id ? color.isActive = true : null;
                    });
                }
            });

            if (scope.defaultFirstSelected && scope.productItem.allColors.length > 0) {
                var defaultColor = scope.productItem.allColors[0];
                defaultColor.isActive = true;
                scope.productItem.selectColor(defaultColor);
            }

            scope.getColorStyles = function (color) {
                var style;

                if (color.image) {
                    style = {
                        'background-image': 'url(' + color.image + ')'
                    };
                }
                else {
                    style = {
                        'background-color': color.color_hex
                    };
                }

                if (color.title.toLowerCase() == 'white' || color.color_hex.slice(0,4) == '#fff') {
                    style['border'] = '1px solid #b5c0c7';
                }
                return style;
            };

            scope.selectColor = function (color) {
                if (scope.selectType == 'radio') {
                    scope.typeItem.product_colors.forEach(function(item) {
                        item.isActive = false;
                    });
                    color.isActive = true;
                    scope.sample ? scope.productItem.selectColorSample(color, scope.itemIndex) : scope.productItem.selectColor(color, scope.typeItem.name);
                    scope.onColorSelected ? scope.onColorSelected(color) : null;
                } else {
                    color.isActive = !color.isActive;
                }
                /*if (color.image_src) {
                    $rootScope.$broadcast('ASG-gallery-edit', {
                        id: 'product',
                        selected: 0,
                        update: [color.image_src].concat($customize.productImages)
                    });
                }*/
            }
        }
    }
}
