import './product-color-selector.scss';
import productColorSelectorDirective from './product-color-selector.directives.js';

export default angular.module('productColorSelectorDirective', [])
    .directive('productColorSelector', productColorSelectorDirective)
    .name
