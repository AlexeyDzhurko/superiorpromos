import view from './wish-list-like.view.html';

wishListLikeDirective.$inject = ['cartService', 'customizeOrderService', '$state', 'LoginRegistrationService'];

export default function wishListLikeDirective($cart, $customize, $state, $login) {
    return {
        restrict: 'E',
        template: view,
        scope: {
            productId: '@'
        },
        replace:true,
        link: function (scope) {
            scope.likedImg = 'like_active.svg';
            scope.unlikedImg = 'like_inactive.svg';
            scope.$customize = $customize;
            scope.$cart = $cart;
            scope.$state = $state;

            var _checkRepeat = function (wishItems) {
                var items = wishItems || $cart.wishListItems;
                for (var i = 0; i < items.length; i++) {
                    if (+scope.productId === items[i].product.id) {
                        return items[i].id;
                    };
                };
                return false;
            },
                _checkItemAtWishList = function (items) {
                    if (_checkRepeat(items || $cart.wishListItems)) {
                        scope.name = scope.likedImg;
                    } else {
                        scope.name = scope.unlikedImg;
                    }
                },
                _likeClicked = function () {
                    if (_checkRepeat($cart.wishListItems)) {
                        $cart.deleteWishListItem(_checkRepeat())
                            .then(function () {
                                $customize.animateAddToWishList(scope.productId).then(function () {
                                    scope.name = scope.unlikedImg;
                                });
                            });
                    } else {
                        $cart.addWishListItem(scope.productId)
                            .then(function () {
                                $customize.animateAddToWishList(scope.productId).then(function () {
                                    scope.name = scope.likedImg;
                                });
                            });
                    };
                };

            scope.switchLike = function () {
                if ($login.checkAuth() === 'authorized') {
                    _likeClicked();
                };
            };

            scope.$watch('productId', function () {
                _checkItemAtWishList();
            });

            scope.$watch('$cart.wishListItems', function (items) {
                _checkItemAtWishList(items);
            });
        }
    }
}
