import './wish-list-like.scss';

import wishListLikeDirective from './wish-list-like.directive.js';

export default angular.module('wishListLikeDirective', [])
    .directive('wishLike', wishListLikeDirective)
    .name;
