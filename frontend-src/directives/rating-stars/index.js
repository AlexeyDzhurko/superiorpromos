import './rating-stars.scss';
import ratingStarsDirective from './rating-stars.directive';

export default angular.module('ratingStarsDirective', [])
    .directive('ratingStars', ratingStarsDirective)
    .name;
