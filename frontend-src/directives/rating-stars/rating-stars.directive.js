import view from './rating-stars.view.html';

ratingStarsDirective.$inject = [];

export default function ratingStarsDirective() {
    return {
        restrict: 'E',
        template: view,
        scope: {
            source: '=',
            template: '=',
            average: '='
        },
        replace: true,
        link: function (scope) {
            scope.averagerating = null;
            scope.ceilAverage = null;
            scope.reviewsCount = null;

            scope.setRating = function (number) {
                let setClass = '';

                if (typeof scope.source === 'number') {
                    setClass = scope.source >= number ? 'rated' : '';
                    scope.ceilAverage = scope.source;
                }

                if (typeof scope.source === 'object') {
                    scope.reviewsCount = scope.source.reviews.length;

                    if (!scope.averagerating) {
                        var count = 0,
                        avg = scope.source.reviews.reduce(function (avg, review) {
                            avg = (count * avg + review.rating) / (++count);

                            return Math.round(avg);
                        }, 0);

                        scope.averagerating = avg;
                        setClass = avg >= number ? 'rated' : '';
                    } else {
                        setClass = scope.averagerating >= number ? 'rated' : '';
                    }
                }

                return setClass;
            }
        }
    }
}
