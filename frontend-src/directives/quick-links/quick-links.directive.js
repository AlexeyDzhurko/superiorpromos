import view from './quick-links.view.html';

quickLinksDirective.$inject = ['apiService', '$search', '$state', '$categoriesData'];

export default function quickLinksDirective($api, $search, $state, $categoriesData) {
    return {
        restrict: 'E',
        template: view,
        scope: {
            source: '=',
            isHeaderDisabled: '@'
        },
        replace: true,
        link: function (scope) {
            scope.isVisible = true;

            scope.$parent.$parent.$watch('$state.current.name', function (newValue) {
                if(newValue === 'router.home') {
                    scope.isVisible = false;
                } else {
                    scope.isVisible = true;
                }
            });

            var _search = function (slug) {
                if (typeof scope.onSearch === 'function'){
                    scope.onSearch();
                }
                $search.query.category_slug = slug;
                $state.go('router.categories', { viewMode: 'grid', slug: slug });
            };

            scope.$categoriesData = $categoriesData;

            scope.$watch('$categoriesData.getQuickLinks()', function (quickLinks) {
                scope.quickLinks = quickLinks;
            });

            scope.goToLinkRef = function (slug) {
                _search(slug);
            };
        }
    }
}
