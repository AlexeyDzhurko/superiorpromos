import './quick-links.scss';
import quickLinksDirective from './quick-links.directive.js';

export default angular.module('quickLinksDirective', [])
    .directive('quickLinks', quickLinksDirective)
    .name;
