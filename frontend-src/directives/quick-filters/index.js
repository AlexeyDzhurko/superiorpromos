import './quick-filters.scss';

import quickFiltersDirective from './quick-filters.directive';

export default angular.module('quickFiltersDirective', [])
    .directive('quickFilters', quickFiltersDirective)
    .name
