import view from './quick-filters.view.html';

quickFiltersDirective.$inject = ['$search', '$state'];

export default function quickFiltersDirective($search) {
    return {
        restrict: 'E',
        template: view,
        link: function (scope) {
            scope.$search = $search;
        }
    }
};
