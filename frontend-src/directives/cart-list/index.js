import './cart-list.scss';
import cartListDirective from './cart-list.directive.js';

export default angular.module('cartListDirective', [])
    .directive('cartList', cartListDirective)
    .name
