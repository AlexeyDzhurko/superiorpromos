import view from './cart-list.view.html';
import confirmDeleteView from '../../common/confirm-delete/confirm-delete.html';
import confirmDeleteCtrl from '../../common/confirm-delete/confirm-delete-controller';
import '../../common/confirm-delete/confirm-delete.scss';
import apiService from "../../services/api-service/service";

cartListDirective.$inject = [
    '$rootScope',
    'cartService',
    '$state',
    'LoginRegistrationService',
    'ModalService',
    'apiService',
    '$timeout',
];

export default function cartListDirective(
    $rootScope,
    $cart,
    $state,
    $login,
    $modal,
    $apiService,
    $timeout
) {
    return {
        restrict: 'E',
        scope: {
            source:'=',
            order: '=',
            onConfirm: '=',
            mode: '@'
        },
        template:view,
        link: function (scope) {
            scope.$cart = $cart;
            scope.totalShippingCost = null;
            scope.totalProduct = null;
            scope.totalOrder = null;

            scope.checkMode = function (mode) {
                return mode === 'shopping-cart' || mode === 'checkout' || mode === 'ready-order';
            };

            scope.checkSaveCardVisible = function (mode) {
                return mode === 'shopping-cart' || mode === 'ready-order';
            };

            scope.goTo = function (mode) {
                switch (mode) {
                    case 'shopping-cart' :
                        if ($login.checkAuth() === 'authorized') $state.go('router.my-account.order-history');
                        break;
                    case 'checkout' :
                        $state.go('router.shopping-cart');
                        break;
                    case 'ready-order' :
                        $state.go('router.cart-checkout');
                        break;
                    default:
                }
            };

            var _getIds = function (source) {
                    var _cartItems = [];
                    if (source && source.length) {
                        _cartItems = source.map(function (item) {
                            return item.id;
                        });
                    }
                    return _cartItems;
                },
                _deleteCartItem = function (item, mode) {
                    var recalculateCart = function () {
                        scope.selectedIds.splice(scope.selectedIds.indexOf(item.id), 1);
                        $cart.totalSelectedPrice -= item.cartItem.cart_item_cost.total;
                        scope.totalProduct -= item.cartItem.cart_item_cost.total - item.cartItem.cart_item_cost.shipping;
                        scope.totalShippingCost -= item.cartItem.cart_item_cost.shipping;
                    }
                    if(mode === 'saved-cart') {
                        $cart.deleteSavedCartItem(item.id).then(recalculateCart);
                    } else {
                        $cart.deleteCartItem(item.id).then(recalculateCart);
                    }
                };

            scope.all = true;
            scope.selectedIds = [];
            $cart.totalSelectedPrice = 0;

            scope.isAllItemsSelected = function () {
                for(var i = 0; i < scope.source.length; i++){
                    if (!scope.source[i].cartItem.isCheckedSelected){
                        scope.all = false;
                        return;
                    }
                }
                scope.all = true;
            };

            scope.selectAll = function (value) {
                if (scope.mode !== 'order-confirmation') {
                    scope.selectedIds = [];
                    $cart.totalSelectedPrice = 0;
                    angular.forEach(scope.source, function (item) {
                        item.cartItem.isCheckedSelected = value;
                        if (value){
                            scope.selectedIds.push(item.id);
                            $cart.totalSelectedPrice += +item.cartItem.cart_item_cost.total;
                        }
                    })
                }
            };

            scope.selectOne = function (item) {
                if (item.cartItem.isCheckedSelected){
                    scope.selectedIds.push(item.id);
                    $cart.totalSelectedPrice += item.cartItem.cart_item_cost.total;
                } else {
                    scope.selectedIds.splice(scope.selectedIds.indexOf(item.id), 1);
                    $cart.totalSelectedPrice -= item.cartItem.cart_item_cost.total;
                }
                scope.isAllItemsSelected()
            };

            scope.toggleAdditional = function(cartItem){
                cartItem.isOpened = !cartItem.isOpened;
            };

            scope.removeCartItem = function (item, mode) {
                $modal.showModal({
                    template: confirmDeleteView,
                    controller: confirmDeleteCtrl,
                    inputs: {
                        source: `Are you sure you want to remove ${ item.cartItem.productTitle}?`
                    }
                }).then(function(modal) {
                    modal.close.then(function(confirmed) {
                        if (confirmed) {
                            _deleteCartItem(item, mode);
                        }
                    });
                });
            };

            scope.editOrder = function(data, mode){
                $state.go('router.customize-order.options',{
                    slug: data.cartItem.productData.full_slug,
                    view: (mode == 'saved-cart') ? 'edit-saved-cart-item' : 'edit-cart-item',
                });
            };

            scope.saveForLater = function (item) {
                if ($login.checkAuth() === 'authorized') {
                    var recalculateCart = function () {
                        $timeout(function () {
                            scope.selectedIds.splice(scope.selectedIds.indexOf(item.id), 1);
                            $cart.totalSelectedPrice -= item.cartItem.cart_item_cost.total;
                            scope.totalProduct -= item.cartItem.cart_item_cost.total - item.cartItem.cart_item_cost.shipping;
                            scope.totalShippingCost -= item.cartItem.cart_item_cost.shipping;
                        }, 0);
                    }

                    $cart.moveCartItemToSavedCart([item.id]).then(recalculateCart);
                }
            };

            scope.saveForLaterAllCart = function (source) {
                if ($login.checkAuth() === 'authorized') {
                    $cart.moveCartItemToSavedCart(_getIds(source));
                    $state.go('router.my-account.saved-cart');
                };
            };

            scope.deleteCart = function (source) {
                $cart.deleteAllCartItems(_getIds(source));
            };

            scope.deleteAllCartItems  = function (id) {
                $modal.showModal({
                    template: confirmDeleteView,
                    controller: confirmDeleteCtrl,
                    inputs:{
                        source: 'Are you sure want to empty cart?'
                    }
                }).then(function(modal) {
                    modal.close.then(function(confirmed) {
                        if (confirmed) {
                            scope.deleteCart(id);
                        }
                    });
                });
            };

            scope.$watch('source', function (source) {
                if (source && Array.isArray(source)) source.length ? scope.selectAll(true) : null;
            });

            scope.makeGreyNoActive = function (cartlist, isCurrentOpened) {
                if (cartlist.filter(v => v.cartItem.isOpened).length) {
                    return !isCurrentOpened;
                }
                return false
            };

            scope.makeGreyNoActiveOrderConfirmation = function (cartlist, isCurrentOpened) {
                if (cartlist.filter(v => v.isOpened).length) {
                    return !isCurrentOpened;
                }
                return false
            };

            scope.giftNubmer = null;

            scope.deleteCoupon = function() {
                $apiService.deleteDiscountCoupon().then(response => {
                    scope.coupon = response.data[0];
                    scope.coupon = null;
                    $rootScope.$broadcast('updateCoupon');
                }, error => {
                    scope.couponError = error.data.message;
                })
            };

            scope.addCoupon = function(code) {
                scope.couponCode = code;
                scope.couponError = '';
                $apiService.addDiscountCoupon(code).then(response => {
                    scope.coupon = response.data[0];
                    $rootScope.$broadcast('updateCoupon');
                }, error => {
                    scope.couponError = error.data.message;
                })
            };

            scope.checkCoupon = function() {
                scope.couponError = '';
                $apiService.checkDiscountCoupon().then(response => {
                    scope.coupon = response.data[0];
                }, error => {
                    scope.couponError = error.data.message;
                })
            };

            scope.checkCoupon();

            scope.$watch('source', function (data) {
                if(data) {
                    scope.totalShippingCost = 0;
                    scope.totalProduct = 0;
                    scope.totalOrder = 0;
                    scope.checkCoupon();
                    data.forEach( item => {
                        if (item.cartItem) {
                            scope.totalShippingCost += item.cartItem.cart_item_cost.shipping;
                            scope.totalProduct += item.cartItem.cart_item_cost.total - item.cartItem.cart_item_cost.shipping;
                            scope.totalOrder += item.cartItem.cart_item_cost.total;
                        } else {
                            scope.totalShippingCost += item.cart_item_cost.shipping;
                            scope.totalProduct += item.cart_item_cost.total - item.cart_item_cost.shipping;
                        }
                    });
                }
            });

        }
    }
}
