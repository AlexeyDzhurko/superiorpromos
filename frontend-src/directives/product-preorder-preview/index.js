import './product-preorder-preview.scss';
import productPreorderPreviewDirective from './directive.js';

export default angular.module('productPreorderPreviewDirective', [])
    .directive('productPreorderPreview', productPreorderPreviewDirective)
    .name
