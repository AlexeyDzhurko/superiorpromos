import view from './product-preorder-preview.html';
import quickQuoteView from '../../modules/quick-quote/quick-quote-view.html';
import confirmDeleteView from '../../common/confirm-delete/confirm-delete.html';
import quickQuoteCtrl from '../../modules/quick-quote/quick-quote-controller';
import confirmDeleteCtrl from '../../common/confirm-delete/confirm-delete-controller';
import '../../common/confirm-delete/confirm-delete.scss';
import '../../modules/quick-quote/quick-quote.scss';

productPreorderPreviewDirective.$inject = ['ProductItem', 'cartService', 'LoginRegistrationService', 'ModalService', 'apiService', 'customizeOrderService', '$state', '$stateParams', '$window'];

export default function productPreorderPreviewDirective(ProductItem, cartService, loginRegistrationService, $modal, apiService, orderService, $state, $stateParams, $window){
    return {
        restrict: 'E',
        scope: {
            source:'=',
            isWishListItem:'=',
            customizeOrder: '='
        },
        replace: true,
        template: view,
        link: function (scope) {
            var path = '/img/font-awesome-pro-5/';

            scope.preorderLinks = [
                {
                    id: 1,
                    title: 'Pricing/Customize Item',
                    path: path + 'tag.svg',
                    pathActive: path + 'tag-blue.svg',
                    active: false,
                    handleClick: function (productItem) {
                      productItem.chosenLink = 'pricingInfo';
                      scope.$root.activated = this.id;
                      goToCustomizeOrder(productItem);
                    }
                },
                {
                    id: 2,
                    title: 'Order a Sample',
                    icon: 'info_outline',
                    path: path + 'person-carry.svg',
                    pathActive: path + 'person-carry-blue.svg',
                    active: true,
                    handleClick: function (productItem, link) {
                        if (loginRegistrationService.checkAuth() === 'guest') {
                            let redirectingRout = {
                                state: 'router.customize-order.order-sample',
                                slug: productItem.productData.full_slug
                            };
                            let redirectingRoutJSON = JSON.stringify(redirectingRout);
                            
                            localStorage.setItem('redirectingRout', redirectingRoutJSON);
                            return;
                        }
                        productItem.chosenLink = 'orderSample';
                        scope.$root.activated = this.id;
                        $state.go('router.customize-order.order-sample', { slug: productItem.productData.full_slug });
                    }
                },
                {
                    id: 3,
                    title: 'Shipping Estimate',
                    path: path + 'shipping-timed.svg',
                    pathActive: path + 'shipping-timed-blue.svg',
                    active: false,
                    handleClick: function (productItem) {
                        productItem.chosenLink = 'shippingEstimate';
                        scope.$root.activated = this.id;
                        goToCustomizeOrder(productItem);
                    }
                },
                {
                    id: 4,
                    title: 'Item Description',
                    path: path + 'list-alt.svg',
                    pathActive: path + 'list-alt-blue.svg',
                    active: true,
                    handleClick: function (productItem, link) {
                      productItem.chosenLink = 'itemDescription';
                      scope.$root.activated = this.id;
                      goToCustomizeOrder(productItem);
                    }
                },
                {
                    id: 5,
                    title: 'Item Details',
                    path: path + 'list-alt.svg',
                    pathActive: path + 'list-alt-blue.svg',
                    active: true,
                    handleClick: function (productItem, link) {
                        productItem.chosenLink = 'itemDetails';
                        scope.$root.activated = this.id;
                        goToCustomizeOrder(productItem);
                    }
                },
                {
                    id: 6,
                    arrayItem: [
                        {
                            title: 'Quick Quote',
                            path: path + 'clipboard-check.svg',
                            pathActive: path + 'clipboard-check-blue.svg',
                            active: false,
                            handleClick: function (productItem) {
                                scope.openQuickQuote(productItem);
                            }
                        },
                        {
                            title: 'PDF',
                            path: path + 'file-pdf.svg',
                            pathActive: path + 'file-pdf.svg',
                            active: false,
                            handleClick: function (productItem, link) {
                                $window.open('/product-pdf/'+ productItem._id)
                                // apiService.getProductPdf(productItem._id).then(function (res){
                                //     let file = new Blob([res.data], {type: 'application/pdf'});
                                //     let fileURL = URL.createObjectURL(file);
                                //     $window.open(fileURL);
                                // });
                            }
                        }
                    ],
                },
            ];

            scope.$root.activated = 1;

            var goToCustomizeOrder = function (product) {
                if ($state.includes('**.order-sample')) {
                    var _data = {slug: product.getData().full_slug},
                      _options = 'router.customize-order.options',
                      _step = `router.customize-order.step-${orderService.activeStepIndex + 1}`;

                    if (orderService.activeStepIndex === undefined || orderService.activeStepIndex == 0) {
                        $state.go(_options, _data);
                    } else {
                        $state.go(_step, _data);
                    }
              };
            }

            scope.$watch('source', function (source) {
                scope.productItem = new ProductItem(source && source.product ? source.product : source);
                scope.productItem.chosenLink = ifSamplePage();
            });

            scope.openQuickQuote = function(inputData) {
                $modal.showModal({
                    template: quickQuoteView,
                    controller: quickQuoteCtrl,
                    inputs:{
                        source: {
                            source: scope.source,
                            productItem: inputData
                        }
                    }
                }).then(function(modal) {
                    modal.close.then(function(confirmed) {
                    });
                });
            };

            var ifSamplePage = function () {
                if ($state.includes('**.order-sample')) {
                  scope.$root.activated = 2;
                  return 'orderSample';
                } else {
                  scope.$root.activated = 1;
                  return 'pricingInfo';
                }
            };

            scope.hideInfo = function(){
                scope.product.$$showInfo = false;
            };

            scope.delete = function (id) {
                cartService.deleteWishListItem(id);
            };

            scope.confirmDelete = function (id) {
                $modal.showModal({
                    template: confirmDeleteView,
                    controller: confirmDeleteCtrl,
                    inputs:{
                        source: 'Are you sure you want to delete this item from your wish list?'
                    }
                }).then(function(modal) {
                    modal.close.then(function(confirmed) {
                        if (confirmed) {
                            scope.delete(id);
                        }
                    });
                });
            };

            scope.addToWishList = function(product){
                cartService.addWishListItem(product.id)
                    .then(function(success){
                            orderService.animateAddToWishList();
                        },
                        function(error){
                            console.log(error)
                        });
            };

            scope.getShippingInfo = function (item) {
                scope.quantityErr = null;
                apiService.getShippingData(scope.productItem.productData.id, scope.productItem.zipCode, scope.productItem.quantity)
                    .then(function (res) {
                        item.shippingData = res.data;
                        if (Object.keys(res.data).length > 0) {
                            document.getElementById('shipment-header').classList.add('showed')
                        }
                        scope.isOptionsVisible = orderService.validateShipping(item.shippingData);
                    },
                        function (err) {
                            scope.quantityErr = err;
                        }
                    );
            }

        }
    }
}
