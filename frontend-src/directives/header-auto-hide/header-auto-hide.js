headerAutoHideDirective.$inject = ['$window', '$transitions'];

export default function headerAutoHideDirective($window, $transitions) {
  return {
    restrict: 'A',
    link: function (scope) {
      scope.prevScrollpos = $window.pageYOffset;

      $window.onscroll = function() {

        if($window.pageYOffset > 120) {
          scope.currentScrollPos = $window.pageYOffset;

          if (scope.prevScrollpos < scope.currentScrollPos) {
            scope.isHeaderHidden = true;
          } else {
            scope.isHeaderHidden = false;
          }

          scope.prevScrollpos = scope.currentScrollPos;
          scope.$apply();
        }

      }

      $transitions.onSuccess(null, function() {
        scope.isHeaderHidden = false;
      });
    }
  }
}
