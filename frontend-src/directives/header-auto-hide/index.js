import headerAutoHideDirective from './header-auto-hide.js';

export default angular.module('headerAutoHideDirective', [])
  .directive('headerAutoHide', headerAutoHideDirective)
  .name
