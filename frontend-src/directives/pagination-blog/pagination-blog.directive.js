import view from './pagination-blog.view.html';
paginationBlogDirective.$inject = ['$blog'];

export default function paginationBlogDirective($blog) {
    return {
        restrict: 'E',
        template: view,
        link: function (scope) {
            scope.$blog = $blog;
            scope.settings = {
                maxItems:7
            };
        }
    }
}
