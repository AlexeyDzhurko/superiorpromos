import './pagination-blog.scss';
import paginationBlogDirective from './pagination-blog.directive';

export default angular.module('paginationBlogDirective', [])
    .directive('paginationBlog', paginationBlogDirective)
    .name
