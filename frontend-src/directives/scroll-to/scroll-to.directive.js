scrollToElementDirective.$inject = ['$anchorScroll', '$location'];

export default function scrollToElementDirective($anchorScroll, $location) {
    return {
        restrict: 'A',
        scope: {
          source: '='
        },
        link: function (scope, element, attrs) {
            scope.$watch('source', function (src) {
                if(src) {
                    // $location.hash(attrs.id);
                    $anchorScroll.yOffset = 200;
                    $anchorScroll();
                }
            })

        }
    }
}
