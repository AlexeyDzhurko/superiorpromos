import scrollToElementDirective from './scroll-to.directive.js';

export default angular.module('scrollToElementDirective', [])
    .directive('scrollToElement', scrollToElementDirective)
    .name
