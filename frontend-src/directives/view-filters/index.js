// import './search-filters.scss';
// import './quick-filters.scss';

import viewFiltersDirective from './view-filters.directive';

export default angular.module('viewFiltersDirective', [])
    .directive('viewFilters', viewFiltersDirective)
    .name
