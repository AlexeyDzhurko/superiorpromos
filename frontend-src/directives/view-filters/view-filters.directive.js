import view from './view-filters.view.html';

viewFiltersDirective.$inject = ['$search', '$state', '$rootScope', '$location', '$anchorScroll'];

export default function viewFiltersDirective($search, $state, $rootScope, $location, $anchorScroll) {
    return {
        restrict: 'E',
        template: view,
        link: function (scope) {
            $rootScope.$on('scroll', () => {
                if (scope.selectedPerPage === 'All') {
                    $rootScope.$emit('allowInfinityScroll');
                }
            });
            scope.$search = $search;
            scope.productViewOptions = [
                {
                    key: 1,
                    viewModeName: 'list',
                    materialIcon: 'view_stream',
                    name: 'List'
                },
                /*{
                    key: 2,
                    viewModeName: 'tile',
                    materialIcon: 'view_quilt',
                    name: 'Tile'
                },*/
                {
                    key: 3,
                    viewModeName: 'grid',
                    materialIcon: 'view_module',
                    name: 'Grid'
                }
            ];

            scope.prodSortOptions = [
                {
                    order: 'asc',
                    value: 'Price: Low-High',
                    name: 'sort-amount-up',
                    sorting: 'prices'
                },
                {
                    order: 'desc',
                    value: 'Price: High-Low',
                    name: 'sort-amount-down',
                    sorting: 'prices'
                },
                {
                    order: 'asc',
                    value: 'Alphabetically: A-Z',
                    name: 'sort-amount-up',
                    sorting: 'name'
                },
                {
                    order: 'desc',
                    value: 'Alphabetically: Z-A',
                    name: 'sort-amount-down',
                    sorting: 'name'
                },
                {
                    order: 'asc',
                    value: 'Popularity: Low-High',
                    name: 'sort-amount-up',
                    sorting: 'popularity'
                },
                {
                    order: 'desc',
                    value: 'Popularity: High-Low',
                    name: 'sort-amount-down',
                    sorting: 'popularity'
                },

            ];

            scope.perPageOptions = [15, 30, 45, 60, 75, 'All'];
            scope.selectedSortOptions = scope.prodSortOptions[5];
            scope.selectedPerPage = scope.perPageOptions[0];

            scope.onSortSelectPrice = function($item) {
                scope.selectedSortOptions = $item;
                $search.query.sorting = $item.sorting;
                $search.query.order = $item.order;
                $search.query.category_slug = $state.params.slug;

                $search.find();
            };

            scope.onSortSelectPerPage = function(item) {
                $location.hash('main-view-block');
                $anchorScroll();

                scope.selectedPerPage = item;
                $search.query.category_slug = $state.params.slug;

                if (item === 'All') {
                    $search.setItemPerPage(15);
                    $search.query.per_page = 15;
                } else {
                    $search.setItemPerPage(item);
                    $search.query.per_page = item;
                }
                $search.find();
            };

            scope.selectView = function (view) {
                $state.go('.', {viewMode:view.viewModeName})
            };

            scope.togglePager = function () {
                $search.switchPagination();
            };

            scope.nextPage = function (pagination) {
                $location.hash('main-view-block');
                $anchorScroll();

                pagination.current_page++;
                $search.paginate();
                $search.isPaginateVisible = false;
            };

            scope.previousPage = function (pagination) {
                $location.hash('main-view-block');
                $anchorScroll();

                pagination.current_page--;
                $search.paginate();
                $search.isPaginateVisible = false;
            };
        }
    }
}
