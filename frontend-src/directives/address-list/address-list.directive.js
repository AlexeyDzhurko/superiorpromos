import view from './address-list.view.html';

addressListDirective.$inject = ['$stateParams', 'addressBookService'];

export default function addressListDirective($stateParams, $addressBook) {
    return {
        restrict: 'E',
        scope:{
            source:'='
        },
        replace: true,
        template:view,
        link:function (scope) {
            scope.$addressBook = $addressBook;
            scope.$mode = $stateParams.mode;
            scope.checkActive = function (id) {
                return id == $stateParams.addressId;
            };
            scope.splitIntoColumns = function (address) {
                if (address.length > 1) {
                    let isFirstDefault = false;
                    let isSecondDefault = false;
                    for (let i = 0, j = 3, k = 4; i < address.length; i++) {
                        if (address[i].type === '1') {
                            if (address[i].is_default) {
                                isFirstDefault = true;
                                address[i].order = 1;
                            } else {
                                address[i].order = j;
                                j += 2;
                            }
                        } else {
                            if (address[i].is_default) {
                                address[i].order = 2;
                                isSecondDefault = true;
                            } else {
                                address[i].order = k;
                                k += 2;
                            }
                        }
                    }
                    if (!isFirstDefault) {
                        address = address.map(v => {
                           if (v.type === '1') {
                               v.order -= 2;
                           }
                            return v;
                        });
                    }
                    if (!isSecondDefault) {
                        address = address.map(v => {
                            if (v.type === '2') {
                                v.order -= 2;
                            }
                             return v;
                        });
                    }
                    address = address.sort((a, b) => a.order - b.order);
                    let specialCounter = 999;
                    for (let i = 0; i < address.length; i++) {
                        address[i].emptyNeighbor = false;
                        if (((address[i].type === '1' && i !== address.length - 1 &&
                            address[i + 1].order !== address[i].order + 1) ||
                            (address[i].type === '2' && i !== 0 && address[i - 1].order !== address[i].order - 1)) && !address[i].is_default) {
                            address[i].emptyNeighbor = true;
                            address[i].order = (address[i].type === '1') ? specialCounter : specialCounter % 2 === 0 ? specialCounter + 2 : specialCounter + 3;
                            specialCounter += 2;
                        } else if (address[i].type === '1' && i === address.length - 1 || i === 0 && address[i].type === '2') {
                            address[i].emptyNeighbor = true;
                            address[i].order = (address[i].type === '1') ? specialCounter : specialCounter % 2 === 0 ? specialCounter + 2 : specialCounter + 3;
                            specialCounter += 2;
                        }
                    }
                }
                return address;
            }
        }
    }
}
