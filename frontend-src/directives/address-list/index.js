import addressListDirective from './address-list.directive';

export default angular.module('addressListDirective', [])
    .directive('addressList', addressListDirective)
    .name
