import  productViewInputDirective from './product-view-input.directives.js';

export default angular.module('productViewInputDirective', [])
    .directive('input', productViewInputDirective)
    .name
