import addressEditFormDirective from './address-edit-form.directive';

export default angular.module('addressEditFormDirective', [])
    .directive('addressEditForm', addressEditFormDirective)
    .name
