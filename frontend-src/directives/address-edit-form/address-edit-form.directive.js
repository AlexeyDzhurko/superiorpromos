import view from './address-edit-form.view.html';

import confirmDeleteView from '../../common/confirm-delete/confirm-delete.html';
import confirmDeleteCtrl from '../../common/confirm-delete/confirm-delete-controller';
import '../../common/confirm-delete/confirm-delete.scss';

addressEditFormDirective.$inject = ['addressBookService', '$stateParams', '$state', 'ModalService', '$usStates', '$countriesList'];

export default function addressEditFormDirective($addressBook, $stateParams, $state, $modal, $usStates, $countriesList) {
    return {
        restrict: 'E',
        scope: {
            stayOnPage: '@',
            addressType: '@',
            source: '=?',
            onConfirm: '=',
            onCancel: '='
        },
        template: view,
        link: function (scope) {
            if (!scope.source){
                scope.source = {
                    type: scope.addressType
                };
            }
            scope.source.country = 'US';
            scope.$usStates = $usStates;
            scope.$countriesList = $countriesList;
            scope.$stateParams = $stateParams;

            scope.resetError = function (field) {
                field.$error = {};
            };

            scope.handleErrors = function (errors) {
                for (var key in errors.data){
                    if (errors.data.hasOwnProperty(key)){
                        scope.addressEditForm[key].$error.backend = errors.data[key].join(' ');

                        function findPos(obj) {
                            var curtop = -150;
                            if (obj.offsetParent) {
                                do {
                                    curtop += obj.offsetTop;
                                } while (obj = obj.offsetParent);
                                return [curtop];
                            }
                        }
                        window.scroll(-150, findPos(document.getElementById("err")));
                    }
                }
                scope.busy = false;
            };

            scope.submit = function () {
                if(scope.source.country !== 'US'){
                    scope.source.state = '';
                }

                if (scope.$stateParams.mode == 'edit'){
                    $addressBook.updateAddress($stateParams.addressId, scope.source).then(function () {
                        $addressBook.getAddressBook();
                        $state.go('router.my-account.address-book',{location:'replace'})
                    }, scope.handleErrors )
                } else {
                    $addressBook.createAddress(scope.source).then(function (address) {
                        if (typeof scope.onConfirm === 'function'){
                            scope.onConfirm(address);
                            scope.source = {
                                type:scope.addressType
                            }
                        } else {
                            scope.source = {
                                type:scope.addressType
                            };
                        }
                        $addressBook.getAddressBook();
                        if (!scope.stayOnPage) {
                            $state.go('router.my-account.address-book',{location:'replace'})
                        }
                    }, scope.handleErrors );
                }
            };

            scope.back = function () {
                if (typeof scope.onCancel === 'function'){
                    scope.source = {
                        type:scope.addressType
                    };
                    scope.onCancel()
                } else {
                    $state.go('router.my-account.address-book');
                }
            };

            scope.delete = function (id) {
                $addressBook.deleteAddress(id)
                    .then(function () {
                        $state.go('router.my-account.address-book',{location:'replace'})
                    }, scope.handleErrors);
            };

            scope.confirmDelete = function (id) {
                $modal.showModal({
                    template: confirmDeleteView,
                    controller: confirmDeleteCtrl,
                    inputs:{
                        source: 'Are you sure want to remove this address?'
                    }
                }).then(function(modal) {
                    modal.close.then(function(confirmed) {
                        if (confirmed) {
                            scope.delete(id);
                        }
                    });
                });
            }
            
        }
    }
}
