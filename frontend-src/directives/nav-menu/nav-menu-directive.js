import view from './nav-menu.html';
import './nav-menu.scss';
import navMenuCtrl from './nav-menu-controller'

export default function navMenuDirective() {
    return {
        template:view,
        scope: {
            isOpened: '='
        },
        controller: navMenuCtrl,
    }
}
