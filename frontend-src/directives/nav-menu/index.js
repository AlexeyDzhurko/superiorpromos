import navMenuDirective from './nav-menu-directive';

export default angular.module('navMenuDirective',[])
    .directive('navMenu', navMenuDirective).name;
