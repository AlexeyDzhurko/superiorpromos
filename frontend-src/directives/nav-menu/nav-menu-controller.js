navMenuCtrl.$inject = ['$scope', 'apiService', '$state', '$search', '$categoriesData'];
export default function navMenuCtrl($scope, apiService, $state, $search, $categories) {

    $scope.$categories = $categories;
    $scope.featuredItems = [];
    // $scope.history = {
    //     cats: [],
    //     ids: []
    // };

    apiService.getTopProducts().then(function(response) {
        var products = response.data ? response.data : response;
        for (var key in products){
            if (products.hasOwnProperty(key) && products[key].products){
                $scope.featuredItems.push(products[key].products[0]);
            }
        }

    });

    $scope.showAllItems = function () {
        $scope.closeMenu();
        $search.query = {};
        $search.find().then(function () {
            $state.go('router.search',{viewMode:'tile'})
        });
    };

    $scope.closeMenu = function () {
        var $body = angular.element(document.body);

        if ($scope.showPopUpMenu) {
            $body.addClass('overflow-hidden');
        }
        else {
            $body.removeClass('overflow-hidden');
        }
        $scope.isOpened = !$scope.isOpened;
    };

    $scope.showCategory = function (category) {
        $scope.$categories.showCategory(category);
    };

    $scope.goBack = function () {
        $scope.$categories.goBack();
        // if($scope.history.cats.length){
        //     $scope.$processData.categories = $scope.history.cats[$scope.history.cats.length - 1];
        //     $scope.history.cats.pop();
        //     $scope.history.ids.pop();
        //     if($scope.history.cats.length === 0) $scope.showBackArrow = false;
        // }
    }

    $scope.goToItemFromCat = function (id) {
        $scope.searchQuery(id);
    }

    $scope.searchQuery = function (id) {
        $scope.closeMenu();
        $search.query = {
            category_id : id
        };
        $search.find();
        $state.go('router.search',{viewMode:'grid'})
    }


    $scope.customizeOrder = function (id) {
        $scope.closeMenu();
        $state.go('router.customize-order.options',{orderId:id});
    };

    /*var links = [
        {
            title: "Custom T-Shirts",
            url: "auto-accessories/cd-holders",
            icon: "img/menu-icons/t-shirt.svg"
        },
        {
            title: "Promotional Tote Bags",
            url: "auto-accessories/cd-holders",
            icon: "img/menu-icons/tote-bag.svg"
        },
        {
            title: "Promotional Golf Balls",
            url: "auto-accessories/cd-holders",
            icon: "img/menu-icons/golf-ball.svg"
        },
        {
            title: "Promotional Keychains",
            url: "auto-accessories/cd-holders",
            icon: "img/menu-icons/key-chain.svg"
        },
        {
            title: "Magnetic Business Cards",
            url: "auto-accessories/cd-holders",
            icon: "img/menu-icons/mag-card.svg"
        },
        {
            title: "Custom Ceramic Mugs",
            url: "auto-accessories/cd-holders",
            icon: "img/menu-icons/ceramic-mug.svg"
        },
        {
            title: "Promotional Pens",
            url: "auto-accessories/cd-holders",
            icon: "img/menu-icons/pen.svg"
        },
        {
            title: "Imprinted Stress Balls",
            url: "auto-accessories/cd-holders",
            icon: "img/menu-icons/stress-balls.svg"
        },
        {
            title: "Custom Polo Shirts",
            url: "auto-accessories/cd-holders",
            icon: "img/menu-icons/polo.svg"
        },
        {
            title: "Logo Hats",
            url: "auto-accessories/cd-holders",
            icon: "img/menu-icons/hat.svg"
        },
        {
            title: "Power Banks",
            url: "auto-accessories/cd-holders",
            icon: "img/menu-icons/power-bank.svg"
        },
        {
            title: "Luggage Tags",
            url: "auto-accessories/cd-holders",
            icon: "img/menu-icons/tag.svg"
        },
        {
            title: "Mint Candies",
            url: "auto-accessories/cd-holders",
            icon: "img/menu-icons/candy.svg"
        },
        {
            title: "Imprinted Travel Mugs",
            url: "auto-accessories/cd-holders",
            icon: "img/menu-icons/travel-mug.svg"
        },
        {
            title: "Custom Sticky Notes",
            url: "auto-accessories/cd-holders",
            icon: "img/menu-icons/sticky-notes.svg"
        },
        {
            title: "Logo Koozies",
            url: "auto-accessories/cd-holders",
            icon: "img/menu-icons/koozie.svg"
        },
        {
            title: "Promotional Hand Fans",
            url: "auto-accessories/cd-holders",
            icon: "img/menu-icons/hand-fan.svg"
        },
        {
            title: "Women's Apparel",
            url: "auto-accessories/cd-holders",
            icon: "img/menu-icons/womens-apparel.svg"
        }
    ];*/
}
