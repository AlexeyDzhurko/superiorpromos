import './pricing-info.scss';
import pricingInfoDirective from './pricing-info.directives.js';

export default angular.module('pricingInfoDirective', [])
    .directive('pricingInfo', pricingInfoDirective)
    .name
