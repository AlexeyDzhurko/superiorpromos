import view from './pricingInfo.view.html';

pricingInfoDirective.$inject = ['$state'];

export default function pricingInfoDirective($state) {
    return {
        restrict: 'E',
        replace: true,
        template: view,
        scope:{
            source: '=',
            customizeOrder: '=',
            onSelect: '='
        },
        link:function(scope){
            scope.$watch('source', function (item) {
                item ? scope.priceGrid = item.productData && (item.productData.price_grid || item.productData.productData.price_grid) : scope.priceGrid = [];
                if (item && item.productData) {
                    scope.onSale = item.productData.on_sale;
                }

                if(scope.source.orderData && scope.source.orderData.quantity) {
                    var options = scope.priceGrid.filter(function (item) {
                        return scope.source.orderData.quantity === item.quantity;
                    })[0];

                    if (options) {
                        scope.proceedWithOption(options)
                    }
                }
            });

            scope.proceedWithOption = function(option) {
                scope.source.selectQuantity(option.quantity);
                scope.source.selectPrice(option);
                scope.onSelect ? scope.onSelect(option) : null;
                if (scope.customizeOrder && !$state.includes('**.options')){
                    $state.go('router.customize-order.options', {
                        orderId: scope.source.getId()
                    });
                }
            };

        }
    }
}
