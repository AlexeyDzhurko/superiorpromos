import view from './category-btn.view.html';

categoryBtnDirective.$inject = ["$categoriesData"];

export default function categoryBtnDirective($categoriesData) {
    return {
        restrict: 'E',
        template: view,
        scope: {},
        link: function (scope) {
            scope.toggleCategories = function () {
                $categoriesData.getData();
                scope.isCatsActive = !scope.isCatsActive;
                var $body = angular.element(document.body);
                if (scope.isCatsActive) {
                    $body.addClass('overflow-hidden');
                }
                else {
                    $body.removeClass('overflow-hidden');
                }
            };
        }
    }
}
