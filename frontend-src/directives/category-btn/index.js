import './category-btn.scss';
import categoryBtnDirective from './category-btn.js';

export default angular.module('categoryBtnDirective', [])
    .directive('categoryBtn', categoryBtnDirective)
    .name
