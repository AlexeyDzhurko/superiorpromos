import view from './payment-profiles-list.view.html';

paymentProfilesListDirective.$inject = ['$stateParams', 'paymentProfileService', 'appUserService'];

export default function paymentProfilesListDirective($stateParams, $paymentProfile, $appUser) {
    return {
        restrict: 'E',
        scope: {
            source: '='
        },
        template: view,
        replace: true,
        link: function (scope) {
            scope.userInfo;
            $appUser.getUserAccount().then((res) => {
                scope.userInfo = res;
            });
            scope.$appUser = $appUser;
            scope.$paymentProfile = $paymentProfile;
            scope.$mode = $stateParams.mode;
            scope.checkActive = function (id) {
                return id == $stateParams.profileId;
            };

            scope.getAuthorizeNet = function() {
                console.log('GET Authorize Net');
            }
        }
    }
}
