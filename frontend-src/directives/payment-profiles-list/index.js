import './payment-profiles-list.scss';
import paymentProfilesListDirective from './payment-profiles-list.directive';

export default angular.module('paymentProfilesListDirective', [])
    .directive('paymentProfileList', paymentProfilesListDirective)
    .name
