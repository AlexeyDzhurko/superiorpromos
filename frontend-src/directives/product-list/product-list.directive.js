import view from './product-list.view.html';
productListDirective.$inject = ['$stateParams', 'cartService', '$state', 'customizeOrderService', '$search', '$rootScope'];

export default function productListDirective($stateParams, cartService, $state, orderService, $search, $rootScope) {
    return {
        restrict: 'E',
        scope: {
            source: '='
        },
        template: view,
        replace: true,
        link: function (scope, element) {
            const debounce = (func, delay) => {
                let debounceTimer;
                return function() {
                    const context = this;
                    const args = arguments;
                    clearTimeout(debounceTimer);
                    debounceTimer = setTimeout(() => func.apply(context, args), delay)
                }
            };
            scope.scrollElement = angular.element(document.querySelector('.search-results'))[0];
            element.bind("wheel", debounce(function() {
                if (window.innerHeight + window.scrollY - 100 > scope.scrollElement.clientHeight) {
                    $rootScope.$emit('scroll');
                }
            }, 200));
            $rootScope.$on('allowInfinityScroll', debounce(function() {
                $search.loadOneMorePage().then(res => {
                    scope.source.push(...res);
                });
            }, 100));
            var _breadcrumbs = [
                    { id: 1, name: 'Home', path: 'router.home'}
                ],
                _search = [
                    { id: 2, name: 'Search', path: 'router.home'}
                ],
                _createBreadcrumbs = function (breadcrumbs) {
                    if (breadcrumbs.length) {
                        scope.breadCrumbs = _breadcrumbs.concat(breadcrumbs);
                    } else {
                        scope.breadCrumbs = _breadcrumbs.concat(_search);
                    }
                };

            scope.stateParams = $stateParams;
            scope.placeOrder = function (slug) {
                $state.go('router.customize-order.options', {slug: slug})
            };

            scope.addToWishList = function($event, product){
                $event.stopPropagation();
                cartService.addWishListItem(product.id)
                    .then(function(success){
                            orderService.animateAddToWishList();
                        },
                        function(error){
                            console.log(error);
                        });
            };
            
            scope.$watch('$search.breadcrumbs', function (item) {
                _createBreadcrumbs(item);
            });

            scope.getImage = function (url) {
                return fetch('url', {})
            }

            scope.getImageUrl = function (src) {
                if (!src || !src.length) {
                    return src;
                }

                if (!src.includes('http', 0)) {
                    src = window.location.origin + '/' + src;
                }

                return `/cdn-cgi/image/format=auto,quality=75/${src}`;
            }
        }
    }
}
