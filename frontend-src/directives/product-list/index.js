import productListDirective from './product-list.directive';

export default angular.module('productListDirective', [])
    .directive('productList', productListDirective)
    .name;
