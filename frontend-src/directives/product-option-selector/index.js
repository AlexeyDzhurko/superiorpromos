import './product-option-selector.scss';
import productOptionSelectorDirective  from './product-option-selector.directives';

export default angular.module('productOptionSelectorDirective', [])
    .directive('productOptionSelector', productOptionSelectorDirective)
    .name
