import view from './product-option-selector.view.html';

productOptionSelectorDirective.$inject = [];

export default function productOptionSelectorDirective() {
    return {
        restrict:'E',
        scope:{
            productItem:'=',
            onOptionSelected:'='
        },
        template: view,
        link:function (scope) {
            scope.select = function (option) {
                //scope.onOptionSelected(scope.productItem.selectOption(option));
                scope.productItem.selectOption(option)
            }
        }
    }
}
