import './search-filters.scss';

import searchFiltersDirective from './search-filters.directive.js';

export default angular.module('searchFiltersDirective', [])
    .directive('searchFilters', searchFiltersDirective)
    .name
