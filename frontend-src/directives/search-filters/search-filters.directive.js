import view from './search-filters.view.html';

searchFiltersDirective.$inject = ['$rootScope', '$search', '$colorsData', '$state'];

export default function searchFiltersDirective($rootScope, $search, $colorsData, $state) {
    return {
        restrict: 'E',
        template: view,
        scope: false,
        link:function (scope) {
            scope.$search = $search;
            scope.$colorsData = $colorsData;
            scope.search_query = '';
            scope.$on('$destroy', $search.clearQuery);
            scope.showFilters = false;
            scope.sliderPrice = {
                min: 0.1,
                max: 500,
                options: {
                    showTicks: false,
                    hideLimits: true,
                    stepsArray: [
                        {value: 0.1},
                        {value: 10},
                        {value: 25},
                        {value: 100},
                        {value: 250},
                        {value: 400},
                        {value: '500+'}
                    ],
                    onEnd: function (sliderId, modelValue, highValue) {
                        $search.query.price_from = modelValue;
                        $search.query.price_to = highValue == '500+' ? null : highValue;
                        $search.find();
                    }
                }
            };

            scope.sliderQuantity = {
                min: 25,
                max: 1000,
                options: {
                    showTicks: false,
                    hideLimits: true,
                    stepsArray: [
                        {value: 25},
                        {value: 50},
                        {value: 100},
                        {value: 300},
                        {value: 500},
                        {value: 700},
                        {value: '1000+'}
                    ],
                    onEnd: function (sliderId, modelValue, highValue) {
                        $search.query.quantity_from = modelValue;
                        $search.query.quantity_to = highValue == '1000+' ? null : highValue;
                        $search.find();
                    }
                }
            };

            scope.getAllColors = function () {
                $colorsData.getData();
            }

            scope.showSubCategory = function (id) {
                $search.query.category_id = id;
                $search.find();
            };

            scope.toggleFilters = function () {
                $rootScope.filterMenuActive = !$rootScope.filterMenuActive;
                $rootScope.mobileMenuActive = false;
            };

            scope.closeFilter = function() {
                $rootScope.filterMenuActive = false;
            };

            scope.clearSearch = function () {
              scope.catSearchQuery = '';
              $search.clearQuery();
            };

            scope.searchForGoods = function() {
                $search.clearQuery();
                if(!isNaN(+scope.search_query)) {
                    $search.setQueryData(null, null, null, scope.search_query);
                } else {
                    $search.setQueryData(scope.search_query, null, null, null);
                }
                $search.find().then(result => {
                    if($search.hasResult()) {
                      $state.go("router.search.global", { viewMode: "tile", slug: "search" });
                    } else {
                      $state.go("router.home");
                    }
                });

                scope.search_query = '';
            }
        }
    }
}
