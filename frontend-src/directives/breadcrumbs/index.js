import './breadcrumbs.scss';

import breadcrumbsDirective from './breadcrumbs.directive.js';

export default angular.module('breadcrumbsDirective', [])
    .directive('breadCrumbs', breadcrumbsDirective)
    .name
