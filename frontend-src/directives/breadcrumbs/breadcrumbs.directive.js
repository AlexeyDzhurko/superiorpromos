import view from "./breadcrumbs.view.html";

breadcrumbsDirective.$inject = ["$state", "$search"];

export default function breadcrumbsDirective($state, $search) {
  return {
    restrict: "E",
    scope: {
      source: "=",
      hideCategoryButton: "@",
      showSeoTag: "@",
    },
    template: view,
    link: function(scope) {
      scope.goToLink = function(item) {
        if (item.path) {
          $state.go(item.path);
        } else {
          $search.query.category_slug = item.slug;
          $state.go("router.categories", { viewMode: "grid", slug: item.slug });
        }
      };
    }
  };
}
