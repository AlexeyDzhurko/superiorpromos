var galleryOptions = {
    "debug": false,
    "baseUrl": "",
    "fields": {
        "source": {
            "modal": "link",
            "image": "medium",
            "panel": "thumbnail"
        }
    },
    "theme": "superior",
    "thumbnail": {
        "height": 54,
        "dynamic": false,
        "index": false,
        "autohide": false,
        "click": {
            "select": true, // set selected image when true
            "modal": false // open modal when true
        },
        "hover": {
            "preload": true, // preload image on mouseover
            "select": false // set selected image on mouseover when true
        }
    },
    "modal": {
        "caption": {
            "disabled": true, // disable image caption
        },
        "header": {
            "enabled": true,
            "dynamic": true,
            "buttons": ['playstop', 'index', 'prev', 'next', 'pin', 'close'],
        },
        "transition": "fadeInOut",
        "thumbnail": {
            "height": 90,
            "index": false,
            "dynamic": true
        },
        "size": "contain",
        "placeholder": "none"
    },
    "image": {
        "size": "contain",
        "placeholder": "none",
        "height": 320,
        "click": {
            "modal": true
        },
        "transition": "fadeInOut"
    }
};
export default galleryOptions;
