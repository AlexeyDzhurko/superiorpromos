var sampleInfo = {
        eventDate: [
            {
                id: 1,
                title :'Within a week',
            },
            {
                id: 2,
                title :'2-3 weeks'
            },
            {
                id: 3,
                title :'No specific date'
            }
        ],
        quantity: [
            {
                id: 1,
                title :'Minimum',
            },
            {
                id: 2,
                title :'500-1000'
            },
            {
                id: 3,
                title :'1000-2500'
            },
            {
                id: 4,
                title :'2500+'
            }
        ],
        attraction: [
            {
                id: 1,
                title :'Price',
            },
            {
                id: 2,
                title :'Style'
            },
            {
                id: 3,
                title :'Color'
            },
            {
                id: 4,
                title :'Production times'
            }
        ],
        sample: [
            {
                id: 1,
                title :'Blanc',
            },
            {
                id: 2,
                title :'Random'
            }
        ],
        frequency: [
            {
                id: 1,
                title :'Once',
            },
            {
                id: 2,
                title :'2-3'
            },
            {
                id: 3,
                title :'4+'
            }
        ]
};
export default sampleInfo;