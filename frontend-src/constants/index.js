import usStates from "./us-states.constant";
import galeryOptions from "./galery-options.constant";
import sampleInfo from "./sample-info.constant";
import countriesList from "./countries-list.constant";

export default angular.module('appConstantsModule', [])
    .constant('$usStates', usStates)
    .constant('$countriesList', countriesList)
    .constant('$galeryOptions', galeryOptions)
    .constant('$sampleInfo', sampleInfo)
    .name;
