import trustFilter from "./trust.filter";
import creditNumber from "./credit-number.filter";
import dateRangeFilter from "./range-date.filter";
import titleSearchFilter from "./title-search.filter";
import dateSuffixFilter from "./suffix.filter";
import highlightText from "./highlight.filter";


export default angular.module('appFiltersModule', [])
    .filter('trust', trustFilter)
    .filter('creditNumber', creditNumber)
    .filter('dateRange', dateRangeFilter)
    .filter('titleSearch', titleSearchFilter)
    .filter('dateSuffix', dateSuffixFilter)
    .filter('highlightText', highlightText)
    .name;
