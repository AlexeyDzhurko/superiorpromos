export default function titleSearch() {
    return function (items, title) {
        var filtered = [];

        if (!title) {
            return items;
        }
        angular.forEach(items, function (obj) {
            var len = obj.cart_items.length;
            for(var i = 0; i < len; i++){
                if (isNaN(+title)
                    && typeof obj.cart_items[i].product.title == 'string'
                    && obj.cart_items[i].product.title.toLowerCase().indexOf(title.toLowerCase()) > -1) {

                    filtered.push(obj);
                    return;

                } else if (!isNaN(+title)
                    && obj.cart_items[i].id &&
                    obj.cart_items[i].id.toString().indexOf(title) > -1) {

                    filtered.push(obj);
                    return;

                }
            }
            if (!isNaN(+title)
                && obj.id
                && obj.id.toString().indexOf(title) > -1) {
                filtered.push(obj);
            }
        });

        return filtered;
    }
}