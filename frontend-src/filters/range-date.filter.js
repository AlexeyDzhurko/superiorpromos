dateRange.$inject = [];
export default function dateRange() {
    return function (items, startDate, endDate) {
        var retArray = [];

        if (!startDate && !endDate) {
            return items;
        }
        if (endDate) {
            endDate.setHours(23);
            endDate.setMinutes(59);
        }
        angular.forEach(items, function (obj) {
            var receivedDate = obj.created_at;
            if ((startDate ? (receivedDate >= startDate) : true) && (endDate ? (receivedDate <= endDate) : true)) {
                retArray.push(obj);
            }
        });
        return retArray;
    }
}
