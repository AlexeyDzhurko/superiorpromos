dateSuffix.$inject = ['$filter'];
export default function dateSuffix($filter) {
    var suffixes = ["th", "st", "nd", "rd"];
    return function(input) {
        var dtfilter = $filter('date')(input, 'MMMM dd'),
            year = $filter('date')(input, 'yyyy'),
            day = parseInt(dtfilter.slice(-2)),
            relevantDigits = (day < 30) ? day % 20 : day % 30,
            suffix = (relevantDigits <= 3) ? suffixes[relevantDigits] : suffixes[0];
        return dtfilter + suffix + ', ' + year;
    };
}