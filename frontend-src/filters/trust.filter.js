trust.$inject = ['$sce'];
export default function trust($sce) {
    return function(htmlCode){
        return $sce.trustAsHtml(htmlCode);
    }
}