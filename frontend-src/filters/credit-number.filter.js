creditNumber.$inject = [];
export default function creditNumber() {
    return function(value){
        var modified = null;
        if (value){
            value = value.replace(/(.{4})/g, '$1 ').trim();
        }
        return value;
    }
}