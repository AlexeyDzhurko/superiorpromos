indicatorInterceptor.$inject = ['$q', 'indicatorStatusService'];

export default function indicatorInterceptor($q, $status) {
    var xhrInProgress = 0,
        inProgress = function() {
            return xhrInProgress > 0;
        };
    return {
        request: function (config) {
            xhrInProgress++;
            $status.setStatus(inProgress());
            return config;
        },
        requestError: function (rejection) {
            xhrInProgress--;
            $status.setStatus(inProgress());
            return $q.reject(rejection);
        },
        response: function (response) {
            xhrInProgress--;
            $status.setStatus(inProgress());
            return response;
        },
        responseError: function (rejection) {
            xhrInProgress--;
            $status.setStatus(inProgress());
            return $q.reject(rejection);
        }
    };
}