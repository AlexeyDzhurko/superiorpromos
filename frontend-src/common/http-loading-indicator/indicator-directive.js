import view from './indicator-view.html';

httpIndicator.$inject = ['indicatorStatusService', '$rootScope', '$state', '$transitions'];

export default function httpIndicator($status, $rootScope, $state, $transitions) {
    return {
        restrict: 'E',
        template: view,
        replace: true,
        link: function (scope) {
            scope.isVisible = true;

            scope.$status = $status;
            var showLoaderLogo = function(state) {
                if(state === 'router.ready-order') {
                    scope.$showTextInfo = true;
                }

                return state === 'router.home';
            };

            var showTextInfo = function(state) {
                return state === 'router.ready-order';
            };

            scope.$showLoaderLogo =  showLoaderLogo($state.current.name);
            scope.$showTextInfo = showTextInfo($state.current.name);

            $transitions.onEnter( {} , function (transition, state) {
                scope.$showLoaderLogo = showLoaderLogo(state.name);
                scope.$showTextInfo = showTextInfo(state.name);
            });
        }
    }
}
