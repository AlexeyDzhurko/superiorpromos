ccvInfoController.$inject = ['$scope', 'close'];
export default function ccvInfoController($scope, close) {
    $scope.cancel = function () {
        close(null)
    };
    $scope.confirm = function () {
        close(true);
    }
}
