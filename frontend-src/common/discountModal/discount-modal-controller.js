discountModalController.$inject = ['$scope', 'close', 'apiService'];
export default function discountModalController($scope, close, apiService) {
    $scope.discountModalData = {
        header: 'Sign up for specials and get 10% off',
        buttonText: 'Send'
    };
    $scope.modalData = '';
    $scope.email = '';
    $scope.errorMessage = '';
    $scope.successMessage = '';

    apiService.getNewsletterSubscriptionBlock('coupon_popup').then(data=> {
        $scope.modalData = data.data[0].content;
    });

    $scope.close = function () {
        close();
    };
    $scope.handleClick = function(event) {
        event.stopImmediatePropagation();
    };
    $scope.sendEmail = function() {
        $scope.errorMessage = '';
        $scope.successMessage = '';
        if ($scope.email) {
            apiService.subscribeToNewsletter($scope.email).then(success => {
                $scope.successMessage = 'Success';
            }, err => {
                $scope.errorMessage = err.data.message;
            })
        } else  {
            $scope.errorMessage = 'Field is required';
        }
    };
}
