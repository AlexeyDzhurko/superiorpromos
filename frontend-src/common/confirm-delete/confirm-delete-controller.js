confirmDeleteCtrl.$inject = ['$scope', 'close', 'source'];
export default function confirmDeleteCtrl($scope, close, source) {
    $scope.source = source;
    $scope.cancel = function () {
        close(null)
    };
    $scope.confirm = function () {
        close(true);
    }
}