orderDateInfoController.$inject = ['$scope', 'close'];
export default function orderDateInfoController($scope, close) {
    $scope.close = function () {
        close(null);
    };
    $scope.handleClick = function(event) {
        event.stopImmediatePropagation();
    };
}
