<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Big Mailer
    |--------------------------------------------------------------------------
    |
    | https://www.bigmailer.io/
    |
    */

    'api_key' => env('BIGMAILER_API_KEY'),
    'api_url' => env('BIGMAILER_API_URL'),
    'brand_id' => env('BIGMAILER_BRAND_ID'),
    'contact_list' => env('BIGMAILER_ALL_CONTACTS_LIST_ID'),

    'campaign' => [
        'coupon' => env('BIGMAILER_COUPON_CAMPAIGN_ID'),
        'quick_quote' => env('BIGMAILER_QUICK_QUOTE_CAMPAIGN_ID'),
        'reset_password' => env('BIGMAILER_PASSWORD_RESET_CAMPAIGN_ID'),
        'reorder' => env('BIGMAILER_REORDER_CAMPAIGN_ID'),
        'registration' => env('BIGMAILER_REGISTRATION_CAMPAIGN_ID'),
        'art_proof' => env('BIGMAILER_ART_PROOF_CAMPAIGN_ID'),
        'art_proof_status' => env('BIGMAILER_ADMIN_SUBORDER_ARTPROOF_CAMPAIGN_ID'),
        'admin_new_order' => env('BIGMAILER_ADMIN_NEW_ORDER_CAMPAIGN_ID'),
        'customer_new_order' => env('BIGMAILER_USER_NEW_ORDER_CAMPAIGN_ID'),
        'tracking_info' => env('BIGMAILER_TRACKING_INFO_CAMPAIGN_ID'),
        'new_sample_order' => env('BIGMAILER_SAMPLE_ORDER_CAMPAIGN_ID'),
        'customer_order_user_notes' => env('BIGMAILER_ORDER_AVAILABLE_CAMPAIGN_ID'),
        'order_status_update' => env('BIGMAILER_ORDER_STATUS_UPDATE_CAMPAIGN_ID'),
        'order_feedback' => env('BIGMAILER_ORDER_FEEDBACK_CAMPAIGN_ID'),
        'admin_new_payment' => env('BIGMAILER_ADMIN_PAYMENT_ADD_CAMPAIGN_ID'),
        'admin_changed_payment' => env('BIGMAILER_ADMIN_PAYMENT_CHANGED_CAMPAIGN_ID'),
        'admin_address_book' => env('BIGMAILER_ADMIN_ADDRESS_BOOK_CAMPAIGN_ID'),
        'final_invoice' => env('BIGMAILER_FINAL_INVOICE_CONFIRMATION_CAMPAIGN_ID'),
        'contact_us' => env('BIGMAILER_CONTACT_FORM_CAMPAIGN_ID'),
        'admin_order_user_notes' => env('BIGMAILER_ADMIN_SUBORDER_NOTE_CAMPAIGN_ID'),
        'extra_billing' => env('BIGMAILER_EXTRA_BILLING_CONFIRMATION_CAMPAIGN_ID'),
        'extra_billing_shipping' => env('BIGMAILER_EXTRA_BILLING_SHIPPING_CONFIRMATION_CAMPAIGN_ID'),
        'credit_confirmation' => env('BIGMAILER_CREDIT_ORDER_CAMPAIGN_ID')
    ]
);
