<?php

return [
    'storage' => [
        'tax_exemption_certificates_path' => 'tax_exemption_certificates/',
    ],

    'items_per_page' => env('PRODUCTS_ON_PAGE', 15)
];
