var gulp = require('gulp');
var sass = require('gulp-sass');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var order = require("gulp-order");
var autoprefixer = require('gulp-autoprefixer');
var minify = require('gulp-minify');
var ngAnnotate = require('gulp-ng-annotate');
var isValidGlob = require('is-valid-glob');

var autoprefixerOptions = {
    browsers: ['> 1%']
};

// gulp.task('admin-icons', function() {
//     return gulp.src("bower_components/font-awesome/fonts/**.*")
//         .pipe(gulp.dest('public/admin-assets/fonts'));
// });

// gulp.task('sass', function() {
//     return gulp.src('resources/assets/sass/style.sass')
//         .pipe(sass().on('error', sass.logError))
//         .pipe(autoprefixer({
//             browsers: ['last 40 versions'],
//             cascade: false
//         }))
//         .pipe(cssmin())
//         .pipe(rename({suffix: '.min'}))
//         .pipe(gulp.dest('public/css'));
// });

// gulp.task('css', function() {
//     return gulp.src([
//         "bower_components/bootstrap/dist/css/bootstrap.min.css",
//         "bower_components/angular-carousel/dist/angular-carousel.min.css",
//         "bower_components/angularjs-slider/dist/rzslider.min.css",
//         "bower_components/slick-carousel/slick/slick.css",
//         "bower_components/angular-ui-select/dist/select.min.css",
//         "bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css",
//         "bower_components/animate.css/animate.min.css",
//         "bower_components/angular-material/angular-material.min.css",
//         "bower_components/angular-tooltips/dist/angular-tooltips.min.css"
//     ])
//         .pipe(concat('assets.css'))
//         .pipe(cssmin())
//         .pipe(rename({suffix: '.min'}))
//         .pipe(gulp.dest('public/css'));
// });

// gulp.task('js', function(){
//     return gulp.src([
//         "resources/assets/js/app.js",
//         "resources/assets/js/controllers/*.js",
//         "resources/assets/js/factories/*.js",
//         "resources/assets/js/directives/*.js",
//         "resources/assets/js/services/*.js",
//         "resources/assets/js/components/*.js"
//     ])
//         .pipe(concat('javascript.js'))
//         .pipe(ngAnnotate())
//         .pipe(minify())
//         .pipe(gulp.dest('public/dist'));
// });

gulp.task('admin-js', function(){
    return gulp.src([
        "resources/assets/admin/js/app.js",
        "resources/assets/admin/js/controllers/*.js",
        "resources/assets/admin/js/controllers/*/*.js",
        "resources/assets/admin/js/services/*.js",
        "resources/assets/admin/js/directives/*.js"
    ])
        .pipe(concat('javascript.js'))
        .pipe(ngAnnotate())
        .pipe(minify())
        .pipe(gulp.dest('public/admin-assets/dist'));
});

gulp.task('admin-js-assets', function(){
    return gulp.src([
        // "bower_components/jquery/dist/jquery.min.js",
        "bower_components/angular/angular.js",
        "bower_components/sweetalert/dist/sweetalert.min.js",
        "bower_components/ngSweetAlert/SweetAlert.js",
        "bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js",
        "bower_components/angular-ui-router/release/angular-ui-router.min.js",
        "bower_components/angular-animate/angular-animate.min.js",
        "bower_components/angular-carousel/dist/angular-carousel.min.js",
        "bower_components/angular-carousel/src/directives/rn-carousel-auto-slide.js",
        "bower_components/angularjs-slider/dist/rzslider.min.js",
        "bower_components/slick-carousel/slick/slick.min.js",
        "bower_components/angular-slick/dist/slick.min.js",
        "bower_components/angular-ui-select/dist/select.min.js",
        "bower_components/ngSticky/dist/sticky.min.js",
        "bower_components/angular-scroll/angular-scroll.js",
        "bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js",
        "bower_components/ng-scrollbars/dist/scrollbars.min.js",
        "bower_components/angular-aria/angular-aria.min.js",
        // "bower_components/headroom.js/dist/headroom.min.js",
        // "bower_components/headroom.js/dist/angular.headroom.min.js",
        "bower_components/angular-modal-service/dst/angular-modal-service.min.js",
        "bower_components/angular-xeditable/dist/js/xeditable.js",
        "node_modules/moment/moment.js",
        // "node_modules/angular-bootstrap-datetimepicker/src/js/datetimepicker.js",
        // "node_modules/angular-bootstrap-datetimepicker/src/js/datetimepicker.templates.js",
        "bower_components/ng-file-upload/ng-file-upload-all.js",
        // "bower_components/tinymce/tinymce.js",
        "bower_components/angular-ui-tinymce/src/tinymce.js",
        "bower_components/ng-sortable/dist/ng-sortable.min.js",
        "bower_components/angular-ui-tree/dist/angular-ui-tree.min.js",
        "bower_components/angular-ui-tree-filter/dist/angular-ui-tree-filter.min.js",
        "bower_components/selectize/dist/js/standalone/selectize.min.js",
        "bower_components/angular-selectize2/dist/angular-selectize.js",
        "bower_components/tinycolor/dist/tinycolor-min.js",
        "bower_components/angular-color-picker/dist/angularjs-color-picker.min.js",
        "bower_components/angular-cookies/angular-cookies.min.js"
    ])
        // .src(badInvalidGlobs, { allowEmpty: true })
        .pipe(concat('js-assets.js'))
        .pipe(ngAnnotate())
        .pipe(minify())
        .pipe(gulp.dest('public/admin-assets/dist'));
});

gulp.task('admin-css', function() {
    return gulp.src([
        "bower_components/sweetalert/dist/sweetalert.css",
        "bower_components/font-awesome/css/font-awesome.css",
        // "node_modules/angular-bootstrap-datetimepicker/src/css/datetimepicker.css",
        "bower_components/angular-xeditable/dist/css/xeditable.css",
        "bower_components/ng-sortable/dist/ng-sortable.min.css",
        // "bower_components/ng-sortable/dist/ng-sortable.min.style.css",
        "bower_components/angular-ui-select/dist/select.css",
        "bower_components/angular-ui-tree/dist/angular-ui-tree.min.css",
        "bower_components/selectize/dist/css/selectize.default.css",
        "bower_components/angular-color-picker/dist/angularjs-color-picker.min.css",
        "bower_components/angular-color-picker/dist/themes/angularjs-color-picker-bootstrap.min.css",
        "resources/assets/admin/css/custom.css",
        "bower_components/angular-cookies/angular-cookies.min.js"
    ])
        .pipe(concat('assets.css'))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('public/admin-assets/css'));
});

// gulp.task('js-assets', function(){
//     return gulp.src([
//         "bower_components/jquery/dist/jquery.min.js",
//         "bower_components/angular/angular.js",
//         "bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js",
//         "bower_components/angular-ui-router/release/angular-ui-router.min.js",
//         "bower_components/angular-animate/angular-animate.min.js",
//         "bower_components/angular-carousel/dist/angular-carousel.min.js",
//         "bower_components/angular-carousel/src/directives/rn-carousel-auto-slide.js",
//         "bower_components/angularjs-slider/dist/rzslider.min.js",
//         "bower_components/slick-carousel/slick/slick.min.js",
//         "bower_components/angular-slick/dist/slick.min.js",
//         "bower_components/angular-ui-select/dist/select.min.js",
//         "bower_components/ngSticky/dist/sticky.min.js",
//         "bower_components/angular-scroll/angular-scroll.js",
//         "bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js",
//         "bower_components/ng-scrollbars/dist/scrollbars.min.js",
//         "bower_components/angular-aria/angular-aria.min.js",
//         "bower_components/headroom.js/dist/headroom.min.js",
//         "bower_components/headroom.js/dist/angular.headroom.min.js",
//         "bower_components/angular-tooltips/dist/angular-tooltips.min.js",
//         "bower_components/angularjs-social-login/angularjs-social-login.js",
//         "bower_components/angular-ui-mask/dist/mask.min.js",
//         "bower_components/ngstorage/ngStorage.min.js",
//         "bower_components/angular-cookies/angular-cookies.min.js"
//         "bower_components/angular-local-storage/dist/angular-local-storage.min.js",
//         "bower_components/angular-notify/dist/angular-notify.min.js",
// 		"bower_components/angular-sanitize/angular-sanitize.min.js"
//     ])
//         .pipe(concat('js-assets.js'))
//         .pipe(ngAnnotate())
//         .pipe(minify())
//         .pipe(gulp.dest('public/dist'));
// });

// gulp.task('watch', function(){
//     gulp.watch('resources/assets/sass/modules/*.sass', ['sass']);
//     gulp.watch('resources/assets/js/**/*.js', ['js']);
// });

