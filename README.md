#Deployment

### Requirements

`docker` and `docker-compose` are required for installation.

### Preparations

To prevent issues with elastic container please execute command down below:
```sh
sudo sysctl vm.max_map_count=262144
```

Set write permissions:
```sh
$ sudo chmod o+w -R storage
$ sudo chmod o+w -R public/uploads/
```

### Steps

1. `cp .env.example .env`
2. Set required environment in `.env` file (`PROJECT_ENV=development` by default); 
3. `make`


After building of project you should see the site markup, but without data.

To get data you have 2 options:

1. create database, import dump of old database (database for the old version of superior project) and update it for a new version by call of `sync` command;

Create database
```sh
$ docker exec -i superior_mysql_1  mysql -uroot -psecret < docker/mysql/init.sql
```

Import dump
```sh
$ docker exec -ti superior_mysql_1 bash
$ zcat /tmp/dump.sql.gz | mysql -uroot -psecret superior_old;
$ exit
```

Sync old database
1. Import old database dump and configure OLD_DB_* environments  
2. Check Synchronize commands in SynchronizeCommand
3. Run ```php artisan app:sync:old-db```
4. Run ```php artisan app:user-addresses-sync```
5. Run ```php artisan app:user-payments-info-sync```
6. Run ```php artisan app:elastic:reindex```

Local Sync Product Images
1. Download product images from old site in to the storage/app/old-images folder
2. Run ```php artisan app:sync:main-product-images``` in local mode
2. Run ```php artisan app:sync:extra-product-images``` in local mode

Local Sync Art Proofs files
1. Download art proofs from old site in to the storage/app/old-art-proofs folder
2. Run ```php artisan app:art-proofs-sync``` in local mode

Sync Order item art files
1. Run ```app:sync:order-item-art-files```


2. create database, import dump of already updated database (database for the new version of superior project);

`cat backup.sql | docker exec -i superior_mysql_1 /usr/bin/mysql -u root --password=secret project`

`backup.sql` - name of new project backup

### Commands

- `app:fill:products-shipping-data`- fill empty or nulled shipping data (box weight and quantity per box) by selected values; Run this command to prevent issues with products shipping cost calculation.
