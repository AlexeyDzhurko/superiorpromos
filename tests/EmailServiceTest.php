<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Notification\SendGrid;
use App\User;
use App\Contracts\EmailService;

class EmailServiceTest extends TestCase
{
    /**
     * SendGrid integration
     *
     * @return void
     */
    public function testSendgrid()
    {
        $user = new User();
        $user->id = 53;
        $user->first_name = 'FirstName';
        $user->last_name = 'LastName';
        $user->email = 'dexm238@gmail.com';

        $sg = new Sendgrid();

        $sg->addUser($user);

        $this->assertTrue($sg->addUser($user));
    }

    /**
     * SendGrid Contract
     */
    public function testEmailServiceContract()
    {
        /** @var EmailService $emailService */
        $emailService = $this->app[EmailService::class];

        $user = new User();
        $user->id = 53;
        $user->first_name = 'FirstName';
        $user->last_name = 'LastName';
        $user->email = 'dexm238@gmail.com';

        $this->assertTrue($emailService->addUser($user));
    }
}
