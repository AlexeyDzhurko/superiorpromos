<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Mail\AccountRegistration;
use App\User;

class EmailTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;
    /**
     * Email sending interface
     *
     * @return void
     */
    public function testMailInterface()
    {
        $this->seed();

        $user = User::find(1);

        Mail::sendFromDatabase(new AccountRegistration($user));
    }

}
