<?php

use App\Contracts\PaymentGateway;
use App\Exceptions\PaymentGatewayException;
use App\Payment\AuthorizeNet;
use App\User;
use App\PaymentProfile;

class AuthorizeNetTest extends TestCase
{

    /**
     * Authorize.Net Creating a Customer Profile
     *
     * @return void
     */
    public function testAuthorizeNet()
    {
        $authorize = new AuthorizeNet();

        $user = new User();
        $user->id = 57;
        $user->first_name = 'FirstName';
        $user->last_name = 'LastName';
        $user->email = 'dexm238@gmail.com';

        $authorizeNetId = $authorize->createCustomer($user);
        $this->assertInternalType('int', $authorizeNetId);

        $user->authorize_net_id = $authorizeNetId;
        $this->assertTrue($authorize->deleteCustomer($user));
    }

    /**
     * Authorize.Net a Payment Profile
     */
    public function testAuthorizeNetPaymentProfile()
    {
        $authorize = new AuthorizeNet();

        $user = new User();
        $user->id = 57;
        $user->first_name = 'FirstName';
        $user->last_name = 'LastName';
        $user->email = 'dexm238@gmail.com';

        $authorizeNetId = $authorize->createCustomer($user);
        $user->authorize_net_id = $authorizeNetId;

        $paymentProfile = new PaymentProfile();
        $paymentProfile->card_number = '4007000000027';
        $paymentProfile->expiration_year = '2018';
        $paymentProfile->expiration_month = '11';
        $paymentProfile->first_name = 'FirstName';
        $paymentProfile->last_name = 'LastName';
        $paymentProfile->company = 'CompanyTest';
        $paymentProfile->address = 'address 1';
        $paymentProfile->city = 'New York';
        $paymentProfile->state = 'New York';
        $paymentProfile->country = 'US';
        $paymentProfile->phone = '+1734-565-1212';
        $paymentProfile->zip = '46282';

        $paymentProfileId = $authorize->storePaymentProfileForUser($user, $paymentProfile);
        $this->assertInternalType('int', $paymentProfileId);
        $paymentProfile->id = $paymentProfileId;

        //Update Payment Profile
        $this->assertEquals($paymentProfileId, $authorize->storePaymentProfileForUser($user, $paymentProfile));

        //Invalid request
        $paymentProfile->expiration_year = 'asfas';

        try {
            $authorize->storePaymentProfileForUser($user, $paymentProfile);
        } catch (PaymentGatewayException $e) {
            $this->assertContains('element is invalid', $e->getMessage());
        }

        $this->assertTrue($authorize->deletePaymentProfile($user, $paymentProfile));

        //Test duplicate delete
        try {
            $authorize->deletePaymentProfile($user, $paymentProfile);
        } catch (PaymentGatewayException $e) {
            $this->assertContains('record cannot be found', $e->getMessage());
        }

        $authorize->deleteCustomer($user);
    }

    /**
     * Authorize.Net Creating a Customer Profile with existed customer id
     *
     * @return void
     */
    public function testAuthorizeNetDuplicateProfile()
    {
        $authorize = new AuthorizeNet();

        $user = new User();
        $user->id = 57;
        $user->first_name = 'FirstName';
        $user->last_name = 'LastName';
        $user->email = 'dexm238@gmail.com';

        $authorizeNetId = $authorize->createCustomer($user);
        $user->authorize_net_id = $authorizeNetId;

        try {
            $authorize->createCustomer($user);
        } catch (PaymentGatewayException $e) {
            $this->assertContains('A duplicate record with ID', $e->getMessage());
            $authorize->deleteCustomer($user);
        }

        try {
            $authorize->deleteCustomer($user);
        } catch (PaymentGatewayException $e) {
            $this->assertContains('The record cannot be found', $e->getMessage());
        }
    }

    /**
     * Payment Contract
     *
     * @return void
     */
    public function testPaymentGatewayContract()
    {
        /** @var PaymentGateway $paymentGateway */
        $paymentGateway = $this->app[PaymentGateway::class];

        $user = new User();
        $user->id = 57;
        $user->first_name = 'FirstName';
        $user->last_name = 'LastName';
        $user->email = 'dexm238@gmail.com';

        $customerId = $paymentGateway->createCustomer($user);
        $this->assertInternalType('int', $customerId);

        $user->authorize_net_id = $customerId;

        $paymentProfile = new PaymentProfile();
        $paymentProfile->card_number = '4007000000027';
        $paymentProfile->expiration_year = '2018';
        $paymentProfile->expiration_month = '11';
        $paymentProfile->first_name = 'FirstName';
        $paymentProfile->last_name = 'LastName';
        $paymentProfile->company = 'CompanyTest';
        $paymentProfile->address = 'address 1';
        $paymentProfile->city = 'New York';
        $paymentProfile->country = 'US';
        $paymentProfile->phone = '+1734-565-1212';
        $paymentProfile->zip = '46282';

        $paymentProfileId = $paymentGateway->storePaymentProfileForUser($user, $paymentProfile);
        $this->assertInternalType('int', $paymentProfileId);
        $paymentProfile->id = $paymentProfileId;

        $this->assertTrue($paymentGateway->deletePaymentProfile($user, $paymentProfile));

        $this->assertTrue($paymentGateway->deleteCustomer($user));
    }

}
