<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\Models\Order;
use App\User;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\CartItem;
use App\Services\OrderCalculation;
use App\Models\Price;
use App\Discounts\OrderContainsProductDiscountCondition;
use App\Discounts\OrderCostAboveDiscountCondition;
use App\Discounts\AllOfConditionsDiscountCondition;
use App\Discounts\CouponCodeDiscountCondition;
use App\Discounts\AnyOfDiscountConditions;
use App\Discounts\OrderContainsProductFromCategoryDiscountCondition;
use App\Discounts\DifferentProductCountAboveDiscountCondition;
use App\Discounts\DifferentProductCountBelowDiscountCondition;
use App\Discounts\FirstOrderForUserDiscountCondition;
use App\Discounts\OrderBetweenDateDiscountCondition;
use App\Discounts\OrderCostBelowDiscountCondition;
use App\Discounts\OrderCostWithoutShippingAboveDiscountCondition;
use App\Discounts\OrderCostWithoutShippingBelowDiscountCondition;
use App\Discounts\ProductCountAboveDiscountCondition;
use App\Discounts\ProductCountBelowDiscountCondition;
use App\Discounts\DiscountCreator;
use Carbon\Carbon;

class DiscountTest extends BaseTestCase
{
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    public function testOrderContainsProduct()
    {
        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getOrderContainsProductDiscountString(4)), $this->getOrder());
        $this->assertTrue($discount->isAccepted());

        $discount = $discountCreator->createFromJson(json_encode($this->getOrderContainsProductDiscountString(5)), $this->getOrder());
        $this->assertFalse($discount->isAccepted());
    }

    public function testOrderCostAbove()
    {
        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getOrderCostAboveDiscountString(100)), $this->getOrder());
        $this->assertTrue($discount->isAccepted());

        $discount = $discountCreator->createFromJson(json_encode($this->getOrderCostAboveDiscountString(10000000)), $this->getOrder());
        $this->assertFalse($discount->isAccepted());
    }

    public function testOrderCostBelow()
    {
        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getOrderCostBelowDiscountString(100)), $this->getOrder());
        $this->assertFalse($discount->isAccepted());

        $discount = $discountCreator->createFromJson(json_encode($this->getOrderCostBelowDiscountString(10000000)), $this->getOrder());
        $this->assertTrue($discount->isAccepted());
    }

    public function testOrderWithoutShippingCostAbove()
    {
        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getOrderCostWithoutShippingAboveDiscountString(100)), $this->getOrder());
        $this->assertTrue($discount->isAccepted());

        $discount = $discountCreator->createFromJson(json_encode($this->getOrderCostWithoutShippingAboveDiscountString(10000000)), $this->getOrder());
        $this->assertFalse($discount->isAccepted());
    }

    public function testOrderCostWithoutShippingBelow()
    {
        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getOrderCostWithoutShippingBelowDiscountString(100)), $this->getOrder());
        $this->assertFalse($discount->isAccepted());

        $discount = $discountCreator->createFromJson(json_encode($this->getOrderCostWithoutShippingBelowDiscountString(10000000)), $this->getOrder());
        $this->assertTrue($discount->isAccepted());
    }

    public function testAllOfConditions()
    {
        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getAllOfConditionsDiscountString(4, 100)), $this->getOrder());
        $this->assertTrue($discount->isAccepted());

        $discount = $discountCreator->createFromJson(json_encode($this->getAllOfConditionsDiscountString(5, 100)), $this->getOrder());
        $this->assertFalse($discount->isAccepted());

        $discount = $discountCreator->createFromJson(json_encode($this->getAllOfConditionsDiscountString(4, 100000000)), $this->getOrder());
        $this->assertFalse($discount->isAccepted());
    }

    public function testAnyOfConditions()
    {
        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getAnyOfConditionsDiscountString(4, 100)), $this->getOrder());
        $this->assertTrue($discount->isAccepted());

        $discount = $discountCreator->createFromJson(json_encode($this->getAnyOfConditionsDiscountString(4, 1000000000)), $this->getOrder());
        $this->assertTrue($discount->isAccepted());

        $discount = $discountCreator->createFromJson(json_encode($this->getAnyOfConditionsDiscountString(5, 100)), $this->getOrder());
        $this->assertTrue($discount->isAccepted());

        $discount = $discountCreator->createFromJson(json_encode($this->getAnyOfConditionsDiscountString(5, 10000000)), $this->getOrder());
        $this->assertFalse($discount->isAccepted());
    }

    public function testCouponCode()
    {
        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getCouponCodeDiscountString('valid')), $this->getOrder());
        $this->assertTrue($discount->isAccepted());

        $discount = $discountCreator->createFromJson(json_encode($this->getCouponCodeDiscountString('invalid')), $this->getOrder());
        $this->assertFalse($discount->isAccepted());
    }

    public function testOrderContainsProductFromCategory()
    {
        $discountCreator = app(DiscountCreator::class);

        $discount = $discountCreator->createFromJson(json_encode($this->getOrderContainsProductFromCategoryDiscountString(4)), $this->getOrder());
        $this->assertTrue($discount->isAccepted());

        $discount = $discountCreator->createFromJson(json_encode($this->getOrderContainsProductFromCategoryDiscountString(18)), $this->getOrder());
        $this->assertFalse($discount->isAccepted());
    }

    public function testFirstOrderForUser()
    {
        $discountCreator = app(DiscountCreator::class);

        $discount = $discountCreator->createFromJson(json_encode($this->getFirstOrderForUserDiscountString()), $this->getOrder());
        $this->assertTrue($discount->isAccepted());
    }

    public function testOrderBetweenDate()
    {
        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getOrderBetweenDateDiscountString('2017-03-02', '2017-05-02')), $this->getOrder());
        $this->assertTrue($discount->isAccepted());

        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getOrderBetweenDateDiscountString('2016-03-02', '2016-05-02')), $this->getOrder());
        $this->assertFalse($discount->isAccepted());
    }

    public function testDifferentProductCountAbove()
    {
        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getDifferentProductCountAboveDiscountCondition(1)), $this->getOrder());
        $this->assertTrue($discount->isAccepted());

        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getDifferentProductCountAboveDiscountCondition(2)), $this->getOrder());
        $this->assertFalse($discount->isAccepted());

        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getDifferentProductCountAboveDiscountCondition(3)), $this->getOrder());
        $this->assertFalse($discount->isAccepted());
    }

    public function testDifferentProductCountBelow()
    {
        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getDifferentProductCountBelowDiscountCondition(1)), $this->getOrder());
        $this->assertFalse($discount->isAccepted());

        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getDifferentProductCountBelowDiscountCondition(2)), $this->getOrder());
        $this->assertFalse($discount->isAccepted());

        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getDifferentProductCountBelowDiscountCondition(3)), $this->getOrder());
        $this->assertTrue($discount->isAccepted());
    }

    public function testProductCountAbove()
    {
        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getProductCountAbove(4, 500)), $this->getOrder());
        $this->assertTrue($discount->isAccepted());

        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getProductCountAbove(4, 1000)), $this->getOrder());
        $this->assertFalse($discount->isAccepted());
    }

    public function testProductCountBelow()
    {
        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getProductCountBelow(4, 500)), $this->getOrder());
        $this->assertFalse($discount->isAccepted());

        $discountCreator = app(DiscountCreator::class);
        $discount = $discountCreator->createFromJson(json_encode($this->getProductCountBelow(4, 1000)), $this->getOrder());
        $this->assertTrue($discount->isAccepted());
    }

    public function getProductCountAbove($productId, $quantity)
    {
        $data = new stdClass();
        $data->class = ProductCountAboveDiscountCondition::class;
        $data->parameters = new stdClass();
        $data->parameters->product = $productId;
        $data->parameters->quantity = $quantity;

        return $data;
    }

    public function getProductCountBelow($productId, $quantity)
    {
        $data = new stdClass();
        $data->class = ProductCountBelowDiscountCondition::class;
        $data->parameters = new stdClass();
        $data->parameters->product = $productId;
        $data->parameters->quantity = $quantity;

        return $data;
    }

    public function getDifferentProductCountAboveDiscountCondition($count)
    {
        $data = new stdClass();
        $data->class = DifferentProductCountAboveDiscountCondition::class;
        $data->parameters = new stdClass();
        $data->parameters->count = $count;

        return $data;
    }

    public function getDifferentProductCountBelowDiscountCondition($count)
    {
        $data = new stdClass();
        $data->class = DifferentProductCountBelowDiscountCondition::class;
        $data->parameters = new stdClass();
        $data->parameters->count = $count;

        return $data;
    }

    public function getOrderBetweenDateDiscountString($from, $to)
    {
        $data = new stdClass();
        $data->class = OrderBetweenDateDiscountCondition::class;
        $data->parameters = new stdClass();
        $data->parameters->from = $from;
        $data->parameters->to = $to;

        return $data;
    }

    public function getFirstOrderForUserDiscountString()
    {
        $data = new stdClass();
        $data->class = FirstOrderForUserDiscountCondition::class;

        return $data;
    }

    protected function getOrderContainsProductDiscountString($productId)
    {
        $data = new stdClass();
        $data->class = OrderContainsProductDiscountCondition::class;
        $data->parameters = new stdClass();
        $data->parameters->product = $productId;

        return $data;
    }

    protected function getOrderCostAboveDiscountString($cost)
    {
        $data = new stdClass();
        $data->class = OrderCostAboveDiscountCondition::class;
        $data->parameters = new stdClass();
        $data->parameters->cost = $cost;

        return $data;
    }

    protected function getOrderCostBelowDiscountString($cost)
    {
        $data = new stdClass();
        $data->class = OrderCostBelowDiscountCondition::class;
        $data->parameters = new stdClass();
        $data->parameters->cost = $cost;

        return $data;
    }

    protected function getOrderCostWithoutShippingAboveDiscountString($cost)
    {
        $data = new stdClass();
        $data->class = OrderCostWithoutShippingAboveDiscountCondition::class;
        $data->parameters = new stdClass();
        $data->parameters->cost = $cost;

        return $data;
    }

    protected function getOrderCostWithoutShippingBelowDiscountString($cost)
    {
        $data = new stdClass();
        $data->class = OrderCostWithoutShippingBelowDiscountCondition::class;
        $data->parameters = new stdClass();
        $data->parameters->cost = $cost;

        return $data;
    }

    protected function getAllOfConditionsDiscountString($productId, $cost)
    {
        $data = new stdClass();
        $data->class = AllOfConditionsDiscountCondition::class;
        $data->parameters = new stdClass();
        $data->parameters->discountConditions = [];
        $data->parameters->discountConditions[] = $this->getOrderContainsProductDiscountString($productId);
        $data->parameters->discountConditions[] = $this->getOrderCostAboveDiscountString($cost);

        return $data;
    }

    protected function getAnyOfConditionsDiscountString($productId, $cost)
    {
        $data = new stdClass();
        $data->class = AnyOfDiscountConditions::class;
        $data->parameters = new stdClass();
        $data->parameters->discountConditions = [];
        $data->parameters->discountConditions[] = $this->getOrderContainsProductDiscountString($productId);
        $data->parameters->discountConditions[] = $this->getOrderCostAboveDiscountString($cost);

        return $data;
    }

    protected function getOrderContainsProductFromCategoryDiscountString($categoryId)
    {
        $data = new stdClass();
        $data->class = OrderContainsProductFromCategoryDiscountCondition::class;
        $data->parameters = new stdClass();
        $data->parameters->category = $categoryId;
        return $data;
    }

    protected function getCouponCodeDiscountString($code)
    {
        $data = new stdClass();
        $data->class = CouponCodeDiscountCondition::class;
        $data->parameters = new stdClass();
        $data->parameters->discountCode = $code;

        return $data;
    }

    protected function getOrder()
    {
        $order = new Order();
        $order->discount_code = 'valid';
        $order->created_at = Carbon::createFromFormat('Y-m-d', '2017-04-15');
        $user = new User();
        $order->user()->associate($user);

        /** @var OrderCalculation $orderCalculation */
        $orderCalculation = app(OrderCalculation::class);

        $price = new Price();
        $price->quantity = 1;
        $price->regular_price = 10;
        $price->sale_price = 9;

        $product = new Product();
        $product->on_sale = false;
        $product->id = 4;
        $product->prices[] = $price;

        $product2 = new Product();
        $product2->on_sale = false;
        $product2->id = 12;
        $product2->prices[] = $price;

        $orderItem1 = new OrderItem();
        $orderItem1->product()->associate($product);
        $orderItem1->quantity = 700;
        $orderItem1->cartItem()->associate(new CartItem());

        $orderItem1->item_price = $orderCalculation->getItemPrice($product, $orderItem1->quantity);
        $orderItem1->price = $orderItem1->quantity * $orderItem1->item_price;

        $orderItem2 = new OrderItem();
        $orderItem2->product()->associate($product2);
        $orderItem2->quantity = 700;
        $orderItem2->cartItem()->associate(new CartItem());

        $orderItem2->item_price = $orderCalculation->getItemPrice($product2, $orderItem2->quantity);
        $orderItem2->price = $orderItem2->quantity * $orderItem2->item_price;

        $order->orderItems[] = $orderItem1;
        $order->orderItems[] = $orderItem2;
        return $order;
    }
}
