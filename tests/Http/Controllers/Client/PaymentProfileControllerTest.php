<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Contracts\PaymentGateway;
use App\User;
use App\PaymentProfile;

class PaymentProfileControllerTest extends TestCase
{
    use DatabaseTransactions;

    public $validData = [
        'card_holder' => 'FirstName LastName',
        'card_number' => '4007000000027',
        'expiration_month' => 11,
        'expiration_year' => 2021,
        'cvc' => '123',
        'first_name' => 'FirstName',
        'last_name' => 'LastName',
        'address' => 'address 1',
        'city' => 'New York',
        'state' => 'New York',
        'zip' => '11211',
        'country' => 'US',
        'phone' => '333-333-1212',
    ];

    /**
     * Test read Payment Profile
     *
     * @return void
     */
    public function testIndex()
    {
        //test empty data
        $this->json('GET', '/api/user/payment_profile')->assertResponseStatus(200)->seeJson([]);

        $paymentProfileId1 = $this->addPaymentProfile($this->validData);

        $creditCard1 = $this->validData;
        $creditCard1['id'] = (int)$paymentProfileId1;
        $creditCard1['card_number'] = '*********0027';
        $creditCard1['phone_extension'] = null;
        unset($creditCard1['cvc']);

        //test one card
        $this->json('GET', '/api/user/payment_profile')->assertResponseStatus(200)->seeJson([$creditCard1]);

        $creditCard2 = $this->validData;
        $creditCard2['first_name'] = 'FirstName2';
        $paymentProfileId2 = $this->addPaymentProfile($creditCard2);

        $creditCard2['id'] = (int)$paymentProfileId2;
        $creditCard2['card_number'] = '*********0027';
        $creditCard2['phone_extension'] = null;
        unset($creditCard2['cvc']);

        //test multiple cards
        $this->json('GET', '/api/user/payment_profile')->assertResponseStatus(200)->seeJson([$creditCard1, $creditCard2]);
    }

    /**
     * Test create Payment Profile
     *
     * @return void
     */
    public function testStore()
    {
        $validData = $this->validData;

        //test creating with valid params
        $server = $this->transformHeadersToServerVars(['Accept' => 'application/json']);
        $response = $this->call('POST', '/api/user/payment_profile', $validData, [], [], $server);
        $this->assertEquals(201, $response->getStatusCode());

        $paymentProfileLocation = $response->headers->get('Location');
        $this->assertContains('api/user/payment_profile', $paymentProfileLocation);

        $paymentProfileId1 = basename($paymentProfileLocation);
        $this->assertNotEmpty($paymentProfileId1);

        $this->seeInDatabase('payment_profiles', [
            'card_holder' => $validData['card_holder'],
            'first_name' =>  $validData['first_name'],
        ]);

        $validData['first_name'] = 'FirsName2';
        $validData['last_name'] = 'LastName2';
        $validData['expiration_month'] = '02';
        $validData['expiration_year'] = 2024;

        //test second valid params
        $response = $this->call('POST', '/api/user/payment_profile', $validData, [], [], $server);

        $this->assertEquals(201, $response->getStatusCode());


        $paymentProfileLocation = $response->headers->get('Location');
        $paymentProfileId2 = basename($paymentProfileLocation);
        $this->assertNotEmpty($paymentProfileId2);

        $this->seeInDatabase('payment_profiles', [
            'card_holder' => $validData['card_holder'],
            'first_name' =>  $validData['first_name'],
            'expiration_month' =>  $validData['expiration_month'],
            'expiration_year' =>  $validData['expiration_year'],
        ]);


        //test invalid expiration date
        $invalidData = $validData;
        $invalidData['expiration_month'] = '03';
        $invalidData['expiration_year'] = '2014';
        $this->json('POST', '/api/user/payment_profile/', $invalidData)->assertResponseStatus(422);

        //test empty fields
        $invalidData = $validData;
        unset($invalidData['first_name']);
        unset($invalidData['last_name']);
        $this->json('POST', '/api/user/payment_profile/', $invalidData)->assertResponseStatus(422);

        //test invalid phone number
        $invalidData = $validData;
        $invalidData['phone'] = 'wrong phone number';
        $this->json('POST', '/api/user/payment_profile/', $invalidData)->assertResponseStatus(422);
    }

    /**
     * Test read a specific Payment Profile
     *
     * @return void
     */
    public function testShow()
    {
        $paymentProfileId1 = $this->addPaymentProfile($this->validData);

        $creditCard1 = $this->validData;
        $creditCard1['id'] = (int)$paymentProfileId1;
        $creditCard1['card_number'] = '*********0027';
        $creditCard1['phone_extension'] = null;
        unset($creditCard1['cvc']);

        //test valid route
        $this->json('GET', '/api/user/payment_profile/'.$paymentProfileId1)
            ->assertResponseStatus(200)
            ->seeJson($creditCard1);

        //test invalid route
        $this->json('GET', '/api/user/payment_profile/fake')
            ->assertResponseStatus(404);
    }

    /**
     * Test update a specific Payment Profile
     *
     * @return void
     */
    public function testUpdate()
    {
        $validData = $this->validData;
        $paymentProfileId1 = $this->addPaymentProfile($validData);

        $this->seeInDatabase('payment_profiles', [
            'card_holder' => $validData['card_holder'],
            'first_name' =>  $validData['first_name'],
            'expiration_month' =>  $validData['expiration_month'],
            'expiration_year' =>  $validData['expiration_year'],
        ]);

        $validData['first_name'] = 'FirstNameUpdated';
        $validData['expiration_year'] = '2022';

        $this->json('PUT', '/api/user/payment_profile/'.$paymentProfileId1, $validData)->assertResponseStatus(204);

        $this->seeInDatabase('payment_profiles', [
            'first_name' =>  $validData['first_name'],
            'expiration_year' =>  $validData['expiration_year'],
        ]);
    }

    /**
     * Test delete a specific Payment Profile
     *
     * @return void
     */
    public function testDestroy()
    {
        $validData = $this->validData;
        $paymentProfileId1 = $this->addPaymentProfile($validData);

        $this->seeInDatabase('payment_profiles', [
            'card_holder' => $validData['card_holder'],
            'first_name' =>  $validData['first_name'],
            'expiration_month' =>  $validData['expiration_month'],
            'expiration_year' =>  $validData['expiration_year'],
        ]);

        $this->json('GET', '/api/user/payment_profile/'.$paymentProfileId1)->assertResponseStatus(200);

        $this->delete('/api/user/payment_profile/'.$paymentProfileId1)->assertResponseStatus(204);

        $this->json('GET', '/api/user/payment_profile/'.$paymentProfileId1)->assertResponseStatus(404);
    }

    /**
     * Test updating Payment Profile expiration date
     *
     * @return void
     */
    public function testUpdateExpirationDate()
    {
        $paymentProfileId1 = $this->addPaymentProfile($this->validData);

        $this->seeInDatabase('payment_profiles', [
            'id' => $paymentProfileId1,
            'expiration_month' =>  $this->validData['expiration_month'],
            'expiration_year' =>  $this->validData['expiration_year'],
        ]);

        $this->json('POST', '/api/user/payment_profile/'.$paymentProfileId1.'/expiration_date', [
            'expiration_month' => '02',
            'expiration_year' => '2020',
            'cvc' => '123'
        ])->assertResponseStatus(204);

        $this->seeInDatabase('payment_profiles', [
            'id' => $paymentProfileId1,
            'expiration_month' =>  2,
            'expiration_year' =>  2020,
        ]);

        //test invalid request
        $this->json('POST', '/api/user/payment_profile/'.$paymentProfileId1.'/expiration_date', [
            'expiration_month' => '02',
            'expiration_year' => '2004',
            'cvc' => '123'
        ])->assertResponseStatus(422);

        $this->json('POST', '/api/user/payment_profile/'.$paymentProfileId1.'/expiration_date', [
            'expiration_month' => '02',
            'expiration_year' => '2020',
        ])->assertResponseStatus(422);
    }

    /**
     * @inheritdoc
     */
    protected function tearDown()
    {
        $this->clearAllPayments();
        parent::tearDown();
    }

    /**
     * Add Payment Profile to Payment Gateway
     *
     * @param $data
     * @return int
     */
    protected function addPaymentProfile($data): int
    {
        $user = User::find(1);
        $paymentProfile = new PaymentProfile($data);
        $paymentProfile->user()->associate($user);
        $paymentProfile->save();
        return $paymentProfile->id;
    }

    /**
     * Remove all Payment Profiles from Payment Gateway
     */
    protected function clearAllPayments()
    {
        /** @var PaymentGateway $paymentGateway */
        $paymentGateway = $this->app[PaymentGateway::class];

        $user = User::find(1);

        $paymentProfiles = \App\PaymentProfile::all();
        foreach ($paymentProfiles as $paymentProfile) {
            $paymentGateway->deletePaymentProfile($user, $paymentProfile);
        }
    }
}
