/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('CustomerListController', ['$scope', '$http', '$cookies', 'ModalService', 'modalHelper', '$window', 'validationErrorsHandler',
    function ($scope, $http, $cookies, ModalService, modalHelper, $window, validationErrorsHandler) {

    $scope.pagination = {
        currentPage: 1,
        totalItems: 0,
        maxSize: 3,
        lastPage: 1
    };

    $scope.searchData = {};

    $scope.statuses = [
        {id: '1', text: 'Active only'},
        {id: '0', text: 'Inactive only'}
    ];

    $scope.createPages = function(min, max, step) {
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
            input.push(i);
        }
        return input;
    };

    $scope.range = [];

    $scope.getListData = function (page) {
        $scope.hideLoader = false;
        var params = typeof $scope.searchData != 'undefined' ? $scope.searchData : {};

        if (page) {
            params.page = page;
        }

        if($scope.listData == undefined) {
            $scope.searchData.order = 'created_at';
            $scope.searchData.orderDirection = 'desc';
        }

        $http({
            method: 'GET',
            url: '/admin/api/customer/_search',
            params: params
        }).then(function (res) {
            $scope.listData = res.data.data;
            $scope.pagination.totalItems = res.data.meta.pagination.total;
            $scope.pagination.lastPage = res.data.meta.pagination.total_pages;
            $scope.pagination.currentPage = res.data.meta.pagination.current_page;
            $scope.pagination.per_page = res.data.meta.pagination.per_page;
            $scope.range = $scope.createPages(1, $scope.pagination.lastPage);

            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error occurred while customer search');
            $scope.hideLoader = true;
        });
    };

    $scope.getListData(1);

    $scope.search = function () {
        $scope.getListData(1);
    };

    $scope.sort = function (field, order) {
        $scope.searchData.order = field;
        $scope.searchData.orderDirection = order;
        $scope.getListData(1);
    };

    $scope.currentSort = 'created_at';

    $scope.clearSearch = function () {
        $scope.searchData = {};
    };

    $scope.resetSearch = function () {
        $scope.searchData = {};
        $scope.search();
    };

    $scope.deleteItem = function(itemId) {
        modalHelper.delete('/admin/api/customer/' + itemId, 'Customer')
            .then(function(modal) {
                modal.element.modal();
                modal.close.then(function(res) {
                    $scope.listData = $scope.listData.filter(function (item) {
                        return item.id != itemId;
                    });
                });
            });
    };
    
    $scope.loginByUser = function ($customerId) {

        $cookies.remove('cart_id', { path: '/' });
        localStorage.removeItem('cart_id');

        $http({
            method: 'GET',
            url: '/admin/api/customer/' + $customerId + '/login'
        }).then(function (res) {
            let token = {};
            (res.data && res.data.token) ? token['auth'] = res.data.token : token['auth'] = null;

            localStorage.setItem('superior.time', new Date().getTime().toString());
            localStorage.setItem('superior.token', JSON.stringify(token));
            localStorage.setItem('newsletterShown', 'true');
            setTimeout(function (){
                window.open("/");
            }, 500);
        });
    };

    // $scope.showEditAddForm = function (item) {
    //     ModalService.showModal({
    //         templateUrl: "/admin-assets/views/main/partials/case-study.html",
    //         controller: function ($scope, close, $http, $element) {
    //             $scope.item = {active: 0};

    //             if (item) {
    //                 $http({
    //                     method: 'GET',
    //                     url: '/admin/api/case-study/' + item.id
    //                 }).then(function (res) {
    //                     $scope.item = res.data;
    //                 }, function errorCallback(response) {
    //                     validationErrorsHandler.errorsHandling(response, 'Error on server');
    //                 });

    //                 $scope.item = item;
    //             }

    //             $scope.tinymceOptions = {
    //                 plugins: 'link image code',
    //                 toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
    //             };

    //             $scope.saveItem = function () {
    //                 if (item) {
    //                     $http({
    //                         method: 'PUT',
    //                         url: '/admin/api/case-study/' + item.id,
    //                         data: $scope.item
    //                     }).then(function (res) {
    //                         $element.modal('hide');
    //                         close({
    //                             item: res.data,
    //                             isNew: false
    //                         }, 200);
    //                     }, function errorCallback(response) {
    //                         validationErrorsHandler.errorsHandling(response, 'Error on server');
    //                     });
    //                 } else {
    //                     $http({
    //                         method: 'POST',
    //                         url: '/admin/api/case-study',
    //                         data: $scope.item
    //                     }).then(function (res) {
    //                         $element.modal('hide');
    //                         close({
    //                             item: res.data,
    //                             isNew: true
    //                         }, 200);
    //                     }, function errorCallback(response) {
    //                         validationErrorsHandler.errorsHandling(response, 'Error on server');
    //                     });
    //                 }
    //             }
    //         }
    //     }).then(function(modal) {
    //         modal.element.modal();
    //         modal.close.then(function(data) {
    //             if (data.isNew) {
    //                 $scope.listData.push(data.item);
    //             }
    //         });
    //     });
    // };
}]);
