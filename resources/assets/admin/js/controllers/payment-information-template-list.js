/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('PaymentInformationTemplateListController', ['$scope', '$http', 'ModalService', 'modalHelper', 'validationErrorsHandler',
    function ($scope, $http, ModalService, modalHelper, validationErrorsHandler) {

    $scope.getListData = function () {

        $http({
            method: 'GET',
            url: '/admin/api/payment-information-template'
        }).then(function (res) {
            $scope.listData = res.data;
            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    $scope.getListData();

    $scope.deleteItem = function(itemId) {
        modalHelper.delete('/admin/api/payment-information-template/' + itemId, 'Vendor Payment Info Template')
            .then(function(modal) {
                modal.element.modal();
                modal.close.then(function(res) {
                    $scope.listData = $scope.listData.filter(function (item) {
                        return item.id != itemId;
                    });
                    $scope.getListData();
                });
            });
    };
    
    $scope.showEditAddForm = function (item) {
        ModalService.showModal({
            templateUrl: "/admin-assets/views/main/partials/payment-information-template.html",
            appendElement: angular.element('.page-content'),
            controller: function ($scope, close, $http, $element, validationErrorsHandler) {
                $scope.errors = null;

                if (item) {
                    $scope.item = item;
                }

                $scope.saveItem = function () {
                    if (item) {
                        $http({
                            method: 'PUT',
                            url: '/admin/api/payment-information-template/' + item.id,
                            data: $scope.item
                        }).then(function (res) {
                            $element.modal('hide');
                            close({
                                item: res.data,
                                isNew: false
                            }, 200);
                        }, function errorCallback(response) {
                            $scope.errors = response.data;
                            validationErrorsHandler.errorsHandling(response, 'Error on server');
                        });
                    } else {
                        $http({
                            method: 'POST',
                            url: '/admin/api/payment-information-template/',
                            data: $scope.item
                        }).then(function (res) {
                            $element.modal('hide');
                            close({
                                item: res.data,
                                isNew: true
                            }, 200);
                        }, function errorCallback(response) {
                            $scope.errors = response.data;
                            validationErrorsHandler.errorsHandling(response, 'Error on server');
                        });
                    }
                }
            }
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(data) {
                if (data.isNew) {
                    $scope.listData.push(data.item);
                }
            });
        });
    };


}]);