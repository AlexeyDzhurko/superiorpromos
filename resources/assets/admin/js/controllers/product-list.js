app.controller('ProductListController', ['$scope', '$http', 'ModalService', 'modalHelper', 'validationErrorsHandler',
    function ($scope, $http, ModalService, modalHelper, validationErrorsHandler) {

    $scope.pagination = {
        currentPage: 1,
        totalItems: 0,
        maxSize: 20,
        lastPage: 1
    };

    $scope.searchData = {};

    $scope.vendors = [];
    $http({
        method: 'GET',
        url: '/admin/api/vendors-list'
    }).then(function (res) {
        $scope.vendors = res.data;
    }, function errorCallback(response) {
        validationErrorsHandler.errorsHandling(response, 'Error on server');
    });

    $scope.categories = [];
    $http({
        method: 'GET',
        url: '/admin/api/category'
    }).then(function (res) {
        $scope.categories = res.data;
    }, function errorCallback(response) {
        validationErrorsHandler.errorsHandling(response, 'Error on server');
    });

    $scope.statuses = [
        {id: '1', text: 'Active only'},
        {id: '0', text: 'Hidden only'}
    ];

    $scope.sageStatuses = [
        {id: '1', text: 'Yes'},
        {id: '0', text: 'No'}
    ];

    $scope.colorImagesStatuses = [
        {id: 'complete', text: 'Complete'},
        {id: 'empty', text: 'Empty'},
        {id: 'none', text: 'None'}
    ];

    $scope.getVendorTitle = function(vendorId)
    {
        var vendor = $scope.vendors.find(function(vendor) {
            return vendor.id == vendorId;
        });
        return vendor.text;

    };

    $scope.createPages = function(min, max, step) {
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
            input.push(i);
        }
        return input;
    };

    $scope.range = [];

    $scope.getColorImageStatus = function (colors) {
        if (colors.length == 0) {
            return 'empty';
        }
        var status = 'complete';
        colors.forEach(function (color) {
            if(color.image_src == null){
                status = 'none';
            }
        });
        return status;
    };

    $scope.getListData = function (page) {

        $scope.hideLoader = false;

        var params = typeof $scope.searchData != 'undefined' ? $scope.searchData : {};
        if (page) {
            params.page = page;
        }

        if($scope.listData == undefined) {
            $scope.searchData.order = 'created_at';
            $scope.searchData.orderDirection = 'desc';
        }

        $http({
            method: 'GET',
            url: '/admin/api/product/_search',
            params: params,
        }).then(function (res) {
            $scope.listData = res.data.data;

            res.data.data.forEach(function (item, i) {
                res.data.data[i].colorImageStatus = $scope.getColorImageStatus(item.colors);
            });

            $scope.pagination.totalItems = res.data.meta.pagination.total;
            $scope.pagination.lastPage = res.data.meta.pagination.total_pages;
            $scope.pagination.currentPage = res.data.meta.pagination.current_page;
            $scope.pagination.per_page = res.data.meta.pagination.per_page;
            $scope.range = $scope.createPages(1, $scope.pagination.lastPage);

            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
            $scope.hideLoader = true;
        });
    };

    $scope.getListData(1);

    $scope.search = function () {
        $scope.getListData(1);
    };

    $scope.sort = function (field, order) {
        $scope.searchData.order = field;
        $scope.searchData.orderDirection = order;
        $scope.getListData(1);
    };

    $scope.currentSort = 'created_at';

    $scope.resetSearch = function () {
        $scope.search();
        $scope.searchData = {};
    };

    $scope.deleteItem = function(itemId) {
        modalHelper.delete('/admin/api/product/' + itemId, 'Product')
            .then(function(modal) {
                modal.element.modal();
                modal.close.then(function(res) {
                    $scope.listData = $scope.listData.filter(function (item) {
                        return item.id != itemId;
                    });
                });
            });
    };

    $scope.showEditAddForm = function (item) {

        ModalService.showModal({
            templateUrl: "/admin-assets/views/main/partials/case-study.html",
            controller: function ($scope, close, $http, $element, validationErrorsHandler) {

                $scope.item = {active: 0};

                if (item) {

                    $http({
                        method: 'GET',
                        url: '/admin/api/case-study/' + item.id
                    }).then(function (res) {
                        $scope.item = res.data;
                    }, function errorCallback(response) {
                        validationErrorsHandler.errorsHandling(response, 'Error on server');
                    });

                    $scope.item = item;
                }

                $scope.tinymceOptions = {
                    plugins: 'link image code',
                    toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                };

                $scope.saveItem = function () {

                    if (item) {
                        $http({
                            method: 'PUT',
                            url: '/admin/api/case-study/' + item.id,
                            data: $scope.item
                        }).then(function (res) {
                            $element.modal('hide');
                            close({
                                item: res.data,
                                isNew: false
                            }, 200);
                        }, function errorCallback(response) {
                            validationErrorsHandler.errorsHandling(response, 'Error on server');
                        });
                    } else {
                        $http({
                            method: 'POST',
                            url: '/admin/api/case-study',
                            data: $scope.item
                        }).then(function (res) {
                            $element.modal('hide');
                            close({
                                item: res.data,
                                isNew: true
                            }, 200);
                        }, function errorCallback(response) {
                            validationErrorsHandler.errorsHandling(response, 'Error on server');
                        });
                    }
                }
            }
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(data) {
                if (data.isNew) {
                    $scope.listData.push(data.item);
                }
            });
        });
    };

    $scope.clearSearch = function () {
        $scope.searchData = {};
    };
}]);