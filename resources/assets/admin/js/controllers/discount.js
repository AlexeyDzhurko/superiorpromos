/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('DiscountController', ['$scope', '$http', '$interpolate', 'formHelper', '$window', 'validationErrorsHandler',
    function ($scope, $http, $interpolate, formHelper, $window, validationErrorsHandler) {
    $scope.discount = {};

    if (typeof discount != 'undefined') {
        $scope.discount = discount;
    }

    var findCondition = function (className) {
        return $scope.discountConditions.find(function(condition) {
            return condition.class == className;
        });
    };

    var findCalc = function (className) {
        return $scope.discountCalc.find(function(calc) {
            return calc.class == className;
        });
    };

    $scope.discountConditions = [];

    $http({
        method: 'GET',
        url: '/admin/api/discount/discount-condition'
    }).then(function (res) {
        $scope.discountConditions = res.data;

        if (typeof discount != 'undefined') {
            var selectedCondition = angular.fromJson(discount.discount_condition);
            $scope.selectedConditions.push(getConditionsTree(selectedCondition));
        }

        $scope.hideLoader = true;
    }, function errorCallback(response) {
        validationErrorsHandler.errorsHandling(response, 'Error on server');
    });

    var getConditionsTree = function(selected) {
        var condition = findCondition(selected.class);
        var node = {
            title: condition.title,
            discount_condition: condition
        };
        if(condition.allow_nested) {
            var nodes = [];
            selected.parameters.discountConditions.forEach(function (internalSelected) {
                nodes.push(getConditionsTree(internalSelected));
            });
            node.nodes = nodes
        } else {
            node.nodes = [];
            node.params = selected.parameters;
        }

        return node;
    };


    $scope.discountCalc = [];
    $http({
        method: 'GET',
        url: '/admin/api/discount/discount-calc'
    }).then(function (res) {
        $scope.discountCalc = res.data;

        if (typeof discount != 'undefined') {
            var selectedCalc = angular.fromJson(discount.discount_calc);
            selectedCalc.forEach(function (selected) {
                var calc = findCalc(selected.class);
                $scope.addCalc(calc, selected.parameters);
            });
        }
    }, function errorCallback(response) {
        validationErrorsHandler.errorsHandling(response, 'Error on server');
    });

    $scope.getConditionNodeTitle = function (node) {
        var exp = $interpolate(node.discount_condition.parametrized_title);
        return exp(node.params);
    };

    $scope.getCalcNodeTitle = function (node) {
        var exp = $interpolate(node.discount_calc.parametrized_title);
        return exp(node.params);
    };

    $scope.addFirstCondition = function (condition, params) {
        return $scope.selectedConditions = [
            {
                'title': condition.title,
                'discount_condition': condition,
                'nodes': [],
                'params': params,
            }
        ];
    };

    $scope.addCalc = function (calc, params) {
        $scope.selectedCalc.push(
            {
                'title': calc.title,
                'discount_calc': calc,
                'params': params
            }
        );
    };

    $scope.searchRes = [];
    $scope.searchMedia = function($select) {
        return $http.get('/admin/api/product', {
            params: {
                query: $select.search
            }
        }).then(function (res) {
            $scope.searchRes = res.data
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    $scope.addCondition = function (choice, scope) {
        var nodeData = scope.$parent.$modelValue;
        nodeData.nodes.push({
            title: choice.title,
            discount_condition: choice,
            nodes: []
        });
    };

    $scope.remove = function (scope) {
        scope.remove();
    };

    var extractParams = function (node) {
        var data = {};
        data.class = node.discount_condition.class;
        if (node.discount_condition.allow_nested) {
            data.parameters = {discountConditions:[]};
            node.nodes.forEach(function (internalNode) {
                data.parameters.discountConditions.push(extractParams(internalNode));
            });
        } else {
            data.parameters = node.params;
        }
        return data;
    };

    $scope.saveDiscount = function () {
        if (typeof $scope.selectedConditions[0] == 'undefined') {
            console.log('todo');
            return;
        }

        var calc = [];
        
        $scope.selectedCalc.forEach(function (node) {
            calc.push({
                class: node.discount_calc.class,
                parameters: node.params
            });
        });

        var data = {};
        data.name = $scope.discount.name;
        data.code = $scope.discount.code;
        data.active = typeof $scope.discount.active != 'undefined' ? 1 : 0;
        data.discount_condition = angular.toJson(extractParams($scope.selectedConditions[0]));
        data.discount_calc = angular.toJson(calc);

        var httpData = {};
        httpData.data = data;

        if (typeof discount != 'undefined') {
            httpData.url = '/admin/api/discount/' + discount.id;
            httpData.method = 'PUT';
        } else {
            httpData.url = '/admin/api/discount/';
            httpData.method = 'POST';
        }

        $http(httpData).then(function (res) {
            $window.location.href = '/admin/discount/' + res.data.id + '/edit';
        }, function (res) {
            formHelper.setErrors($scope.discountForm, res.data);
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };


    $scope.selectedConditions = [];
    $scope.selectedCalc = [];
}]);