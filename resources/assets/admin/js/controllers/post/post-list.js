/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('PostController', ['$scope', '$http', 'ModalService', 'modalHelper', 'validationErrorsHandler', '$sce',
    function ($scope, $http, ModalService, modalHelper, validationErrorsHandler, $sce) {

        $scope.trustAsHtml = $sce.trustAsHtml;

        $scope.getListData = function () {
            $http({
                method: 'GET',
                url: '/admin/api/post'
            }).then(function (res) {
                $scope.listData = res.data.data;
                $scope.hideLoader = true;
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error occurred while get posts list');
            });
        };

        $scope.getListData();

        $scope.pagination = {
            currentPage: 1,
            totalItems: 0,
            maxSize: 10,
            lastPage: 1
        };

        $scope.deleteItem = function(itemId) {
            modalHelper.delete('/admin/api/post/' + itemId, 'Post')
                .then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(res) {
                        $scope.listData = $scope.listData.filter(function (item) {
                            return item.id != itemId;
                        });
                    });
                }, function (res) {
                    validationErrorsHandler.errorsHandling(res, 'Error on server');
                });
        };

        $scope.showEditAddForm = function (item) {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/main/partials/post.html",
                inputs: {
                    items: $scope.listData
                },
                controller: function ($scope, close, $http, $element, Upload, items) {
                    $scope.errors = null;

                    $scope.getCategories = function () {
                        $http({
                            method: 'GET',
                            url: '/admin/api/post-category/all'
                        }).then(function (res) {
                            $scope.categories = res.data;
                        }, function errorCallback(response) {
                            validationErrorsHandler.errorsHandling(response, 'Error occurred while get categories list');
                        });
                    };

                    $scope.getCategories();

                    $scope.postStatuses = [
                        {id: 'published', text: 'Published'},
                        {id: 'draft', text: 'Draft'},
                        {id: 'pending', text: 'Pending'}
                    ];

                    $scope.image = false;

                    if(items) {
                        $scope.items = items;
                    }

                    if (item) {
                        $scope.item = item;
                    } else {
                        $scope.item = {};
                    }

                    $scope.tinymceOptions = {
                        plugins: 'link image code',
                        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                    };

                    $scope.saveItem = function () {
                        if($scope.image) {
                            $scope.item.image = $scope.image;
                        } else {
                            delete $scope.item.image;
                        }

                        if (item) {
                            $scope.item._method = 'put';
                            Upload.upload({
                                url: '/admin/api/post/' + item.id,
                                data: $scope.item
                            }).then(function (res) {
                                toastr.success('Successfully updated post');
                                $element.modal('hide');
                                close({
                                    item: res.data,
                                    isNew: false
                                }, 200);
                            }, function (res) {
                                $scope.errors = res.data;
                                validationErrorsHandler.errorsHandling(res, 'Error on server');
                            });
                        } else {
                            Upload.upload({
                                method: 'POST',
                                url: '/admin/api/post/',
                                data: $scope.item
                            }).then(function (res) {
                                toastr.success('Successfully created post');
                                $element.modal('hide');
                                close({
                                    item: res.data,
                                    isNew: true
                                }, 200);
                            }, function (res) {
                                $scope.errors = res.data;
                                validationErrorsHandler.errorsHandling(res, 'Error on server');
                            });
                        }
                    }
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(data) {
                    if (data.isNew) {
                        $scope.listData.push(data.item);
                    }
                });
            });
        };
    }]);