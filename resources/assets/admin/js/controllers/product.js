/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('ProductController', ['$scope', '$http', 'ModalService', '$filter', 'modalHelper', '$parse', 'formHelper', 'Upload', 'validationErrorsHandler',
    function ($scope, $http, ModalService, $filter, modalHelper, $parse, formHelper, Upload, validationErrorsHandler) {
        $scope.colors = null;
        $scope.newColorGroup = {};
        $scope.newColorGroupName = null;

        $scope.product = product ? product : {};

        $scope.mainCategoryProducts = [];
        $scope.currentColors = [];
        $scope.productIcons = [];
        $scope.sizeGroups = [];
        $scope.colorImage = [];

        var findProductOption = function (id) {
            return $scope.product.product_options.find(function (productOption) {
                return productOption.id == id;
            });
        };

        var findImprint = function (id) {
            return $scope.product.imprints.find(function (imprint) {
                return imprint.id == id;
            });
        };

        var findColorGroup = function (id) {
            return $scope.product.product_color_groups.find(function (colorGroup) {
                return colorGroup.id == id;
            });
        };

        var getMainCategoryProducts = function (categoryId) {
            $http({
                method: 'GET',
                url: '/admin/api/category/' + categoryId + '/product'
            }).then(function (res) {
                $scope.mainCategoryProducts = res.data;
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.updateColorGroupTitle = function (colorGroupId) {
            var selected = findColorGroup(colorGroupId);
            $http({
                method: 'PATCH',
                url: '/admin/api/product/' + $scope.product.id + '/product-color-group/' + colorGroupId,
                data: {
                    name: selected.name
                }
            }).then(function () {
                //
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.deleteColorGroup = function (colorGroupId) {
            $http({
                method: 'DELETE',
                url: '/admin/api/product/' + $scope.product.id + '/product-color-group/' + colorGroupId
            }).then(function (res) {
                $scope.product.product_color_groups = $scope.product.product_color_groups.filter(function (colorGroup) {
                    return colorGroup.id != colorGroupId;
                });
                toastr.success("Successfully deleted colors group");
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.addColorToColorGroup = function (colorGroupId) {
            $http({
                method: 'POST',
                url: '/admin/api/product/' + $scope.product.id + '/product-color-group/' + colorGroupId + '/product-color',
                data: {
                    color_id: $scope.product.newColor[colorGroupId]
                }
            }).then(function (res) {
                var colorGroup = findColorGroup(colorGroupId);
                colorGroup.product_colors.push(res.data);
                $scope.product.newColor[colorGroupId] = null;
                toastr.success("Successfully added color to custom colors group");
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.addColorPrice = function (colorGroupId) {
            $scope.currentColors[colorGroupId].product_color_prices.push({
                id: 0,
                quantity: 1,
                setup_price: 0,
                item_price: 0
            });
        };

        $scope.colorGroupForm = false;
        $scope.colorGroups = [];
        $scope.colors = [];

        $scope.searchColors = function ($select) {
            return $http.get('/admin/color/search', {
                params: {
                    query: $select.search
                }
            }).then(function (res) {
                $scope.colors = res.data
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $http({
            method: 'GET',
            url: '/admin/api/product-icon/'
        }).then(function (res) {
            $scope.productIcons = res.data;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });

        $http({
            method: 'GET',
            url: '/admin/api/size-group/'
        }).then(function (res) {
            $scope.sizeGroups = res.data;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });

        $http({
            method: 'GET',
            url: '/admin/api/all-color-groups/'
        }).then(function (res) {
            $scope.colorGroups = res.data;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });

        $scope.createColorGroup = function () {
            $scope.colorGroup = 1;
            if ($scope.colorGroup != null) {
                $http({
                    method: 'GET',
                    url: '/admin/api/color/',
                    params: {
                        color_group_id: $scope.colorGroup
                    }
                }).then(function (res) {
                    $scope.colorGroupColors = res.data;
                    $scope.step_1 = false;
                    $scope.step_2 = true;
                }, function errorCallback(response) {
                    validationErrorsHandler.errorsHandling(response, 'Error on server');
                });
            } else {
                $scope.step_1 = false;
                $scope.step_2 = true;
            }
        };

        $scope.addColorGroup = function () {
            $scope.saveProduct().then(function () {
                var data = {};

                if ($scope.newColorGroup.item && $scope.newColorGroup.item.id) {
                    data.color_group_id = $scope.newColorGroup.item.id;
                } else {
                    data.name = $scope.product.newCustomColorGroup;
                }

                $http({
                    method: 'POST',
                    url: '/admin/api/product/' + $scope.product.id + '/product-color-group',
                    data: data
                }).then(function (res) {
                    $scope.product.newCustomColorGroup = null;
                    $scope.product.newColorGroup = null;
                    $scope.product.product_color_groups.push(res.data);
                    $scope.hideColorGroupForm();
                    toastr.success("Successfully added custom colors group");
                }, function errorCallback(response) {
                    validationErrorsHandler.errorsHandling(response, 'Error on server');
                })
            });
        };


        $scope.getVendorById = function (vendorId) {
            return $scope.vendors.find(function (vendor) {
                return vendor.id == vendorId;
            });
        };

        $scope.getCategoryById = function (categoryId) {
            return $scope.categories.find(function (category) {
                return category.id == categoryId;
            });
        };

        $scope.$watch(function () {
            return $scope.product.categories;
        }, function (newValue, oldValue) {
            if (typeof newValue != 'undefined' && typeof newValue[0] != 'undefined') {
                getMainCategoryProducts(newValue[0]);
            } else {
                $scope.mainCategoryProducts = [];
            }
        });

        var getProduct = function () {
            return $http({
                method: 'GET',
                url: '/admin/api/product/' + $scope.product.id
            }).then(function (res) {
                $scope.product = res.data;
                $scope.hideLoader = true;
                $scope.product.product_color_groups.forEach(function (colorGroup) {
                    $scope.currentColors[colorGroup.id] = colorGroup.product_colors[0];
                });

            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };


        $scope.setCurrentColor = function (colorGroupId, color) {
            $scope.currentColors[colorGroupId] = color;
        };

        if (typeof product.id != 'undefined') {
            getProduct();
        } else {
            $scope.hideLoader = true;
        }

        $scope.vendors = [];
        $http({
            method: 'GET',
            url: '/admin/api/vendors-list'
        }).then(function (res) {
            $scope.vendors = res.data;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });

        $scope.categories = [];
        $http({
            method: 'GET',
            url: '/admin/api/category'
        }).then(function (res) {
            $scope.categories = res.data;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });

        $scope.deleteImageFile = function (image) {
            $scope.images = $scope.images.filter(function (item) {
                return item != image;
            });
        };

        $scope.deleteProductExtraImage = function (image) {
            $scope.product.product_extra_images = $scope.product.product_extra_images.filter(function (item) {
                return item != image;
            });
        };

        $scope.selectThumbnailProductExtraImage = function (image) {
            $scope.product.product_thumbnail = image.id;
        };

        $scope.saveProduct = function () {
            var data = $scope.product;
            data._method = 'patch';
            data.images = $scope.images;
            data.additional_specifications = $scope.additional_specifications;
            data.video = $scope.video;
            // delete properties from the request if they are NULL
            if (!data.product_icon_id) {
                delete data.product_icon_id;
            }
            if (!data.size_group_id) {
                delete data.size_group_id;
            }

            return Upload.upload({
                url: '/admin/api/product/' + ($scope.product.id || ''),
                data: data
            }).then(function (res) {
                $scope.product.id = res.data.id;
                formHelper.reset($scope.productForm);
                $scope.images = [];
                $scope.additional_specifications = null;
                $scope.video = null;
                return getProduct().then(function () {
                    toastr.success("Successfully updated product");
                });
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error occurred while update product');
                formHelper.reset($scope.productForm);
                formHelper.setErrors($scope.productForm, response.data);
            });
        };

        $scope.itemArray = [
            {id: 1, name: 'first'},
            {id: 2, name: 'second'},
            {id: 3, name: 'third'},
            {id: 4, name: 'fourth'},
            {id: 5, name: 'fifth'},
        ];

        $scope.selected = {value: $scope.itemArray[0]};

        $scope.deleteOptionPrice = function (productOptionId, id) {
            var selected = findProductOption(productOptionId);
            $http({
                method: 'DELETE',
                url: '/admin/api/product/' + $scope.product.id + '/product-option/' + productOptionId + '/option-price/' + id
            }).then(function (res) {
                selected.product_option_prices = selected.product_option_prices.filter(function (productOptionPrice) {
                    return productOptionPrice.id != id;
                });
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.addProductOptionPrice = function (productOptionId) {
            var selected = findProductOption(productOptionId);
            selected.product_option_prices.push({
                quantity: 1,
                setup_price: null,
                item_price: null,
                id: null
            });
        };

        $scope.cancelOptionPrices = function (productOptionId) {
            var selected = findProductOption(productOptionId);
            selected.product_option_prices = selected.product_option_prices.filter(function (productOptionPrice) {
                return productOptionPrice.id != null;
            });
        };

        $scope.cancelColorPrices = function (colorGroupId, colorId) {
            $scope.colorImage[colorGroupId] = null;
        };

        $scope.saveColor = function (colorGroupId) {
            var selected = findColorGroup(colorGroupId);
            var color = $scope.currentColors[selected.id];
            var data = {
                product_color_prices: color.product_color_prices,
                image: $scope.colorImage[selected.id],
                _method: 'PATCH'
            };

            $scope.saveProduct().then(function () {

                Upload.upload({
                    url: '/admin/api/product/' + $scope.product.id + '/product-color-group/' + colorGroupId + '/product-color/' + color.id,
                    data: data
                }).then(function (res) {
                    color = res.data;
                    $scope.currentColors[selected.id] = color;
                    toastr.success("Successfully updated color info");
                }, function errorCallback(response) {
                    validationErrorsHandler.errorsHandling(response, 'Error on server');
                });

                $http({
                    method: 'PATCH',
                    url: '/admin/api/product/' + $scope.product.id + '/product-color-group/' + colorGroupId,
                    data: {
                        name: selected.name
                    }
                }).then(function () {
                    //
                }, function errorCallback(response) {
                    validationErrorsHandler.errorsHandling(response, 'Error on server');
                });
            });
        };

        $scope.getOptionFieldNameByShowAsAttribute = function (name) {
            return $scope.addProductOptionShowAsSelect.filter(function (item) {
                return (item.id === name);
            })[0].text;
        }

        // save edits
        $scope.saveOptionPrices = function (productOption) {
            var selected = findProductOption(productOption.id);

            if (!selected) {
                selected = productOption;
            }

            var data = {
                data: {
                    name: selected.name,
                    show_as: selected.show_as,
                    required: selected.required,
                    product_option_prices: selected.product_option_prices
                }
            };

            if (productOption.id == 'new') {
                data.method = 'POST';
                data.url = '/admin/api/product/' + $scope.product.id + '/product-option';
            } else {
                data.method = 'PATCH';
                data.url = '/admin/api/product/' + $scope.product.id + '/product-option/' + productOption.id;
            }

            $scope.saveProduct().then(function () {
                $http(data).then(function (res) {
                    window.location.reload();
                }, function errorCallback(response) {
                    validationErrorsHandler.errorsHandling(response, 'Error on server');
                });
            });
        };

        $scope.deleteOption = function (productOptionId) {
            modalHelper.delete('/admin/api/product/' + $scope.product.id + '/product-option/' + productOptionId, 'Product Option')
                .then(function (modal) {
                    modal.element.modal();
                    modal.close.then(function (result) {
                        $scope.product.product_options = $scope.product.product_options.filter(function (productOption) {
                            return productOption.id != productOptionId;
                        });
                    });
                });

        };

        $scope.addProductOption = function () {
            if (!$scope.product.product_options) {
                $scope.product.product_options = [];
            }

            $scope.product.product_options.push({
                id: 'new',
                name: null,
                required: 1,
                show_as: 'drop_down',
                product_option_prices: []
            });
        };

        $scope.addProductOptionShowAsSelect = [
            {id: 'radio', text: 'Radio'},
            {id: 'drop_down', text: 'Drop Down'},
            {id: 'checkbox', text: 'Checkbox'},
        ];

        $scope.showProductSubOptionsForm = function (productOptionId) {
            var selected = findProductOption(productOptionId);
            ModalService.showModal({
                templateUrl: "/admin-assets/views/product/partials/product-suboption.html",
                controller: "ProductSubOptionController",
                inputs: {
                    productOption: selected
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showColorGroupForm = function () {
            $scope.colorGroupForm = true;
        };

        $scope.hideColorGroupForm = function () {
            $scope.colorGroupForm = false;
        };

        //save prices grid
        $scope.savePrices = function () {
            var data = {
                product_prices: $scope.product.product_prices
            };

            $scope.saveProduct().then(function () {

                $http({
                    method: 'PATCH',
                    url: '/admin/api/product/' + $scope.product.id + '/product-price',
                    data: data
                }).then(function (res) {
                    $scope.product.product_prices = res.data;
                }, function errorCallback(response) {
                    validationErrorsHandler.errorsHandling(response, 'Error on server');
                });
            });
        };

        $scope.cancelPrices = function () {
            $scope.product.product_prices = $scope.product.product_prices.filter(function (productPrice) {
                return productPrice.id != null;
            });
        };

        $scope.deleteColorPrice = function (colorGroupId, colorId, colorPriceId) {
            $http({
                method: 'DELETE',
                url: '/admin/api/product/' + $scope.product.id + '/product-color-group/' + colorGroupId + '/product-color/' + colorId + '/product-color-price/' + colorPriceId
            }).then(function (res) {
                colorGroupIndex = $scope.product.product_color_groups.findIndex(function (item) {
                    return item.id == colorGroupId;
                });

                colorPriceIndex = $scope.product.product_color_groups[colorGroupIndex].product_colors.findIndex(function (item) {
                    return item.id == colorId;
                });

                $scope.product.product_color_groups[colorGroupIndex].product_colors[colorPriceIndex].product_color_prices = $scope.product.product_color_groups[colorGroupIndex].product_colors[colorPriceIndex].product_color_prices.filter(function (item) {
                    return item.id != colorPriceId;
                });
                toastr.success("Successfully deleted color price");
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.deleteColor = function (colorGroupId, colorId) {
            $http({
                method: 'DELETE',
                url: '/admin/api/product/' + $scope.product.id + '/product-color-group/' + colorGroupId + '/product-color/' + colorId
            }).then(function (res) {
                colorGroupIndex = $scope.product.product_color_groups.findIndex(function (item) {
                    return item.id == colorGroupId;
                });
                $scope.product.product_color_groups[colorGroupIndex].product_colors = $scope.product.product_color_groups[colorGroupIndex].product_colors.filter(function (item) {
                    return item.id != colorId;
                });
                toastr.success("Successfully deleted color");
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.deletePrice = function (id, index) {
            if (id === null) {
                $scope.product.product_prices.splice(index, 1);
            } else {
                $http({
                    method: 'DELETE',
                    url: '/admin/api/product/' + $scope.product.id + '/product-price/' + id
                }).then(function (res) {
                    $scope.product.product_prices = $scope.product.product_prices.filter(function (productPrice) {
                        return productPrice.id != id;
                    });
                }, function errorCallback(response) {
                    validationErrorsHandler.errorsHandling(response, 'Error on server');
                });
            }
        };

        $scope.addPrice = function () {

            if(!$scope.product.product_prices || !$scope.product.product_prices.length) {
                $scope.product.product_prices = [];
            }

            $scope.product.product_prices.push({
                quantity: 1,
                setup_price: null,
                item_price: null,
                sale_item_price: null,
                id: null
            });
        };

        $scope.addImprint = function () {
            if (!$scope.product.imprints) {
                $scope.product.imprints = [];
            }

            $scope.product.imprints.push({
                id: 'new',
                name: null,
                max_colors: 1,
                color_group_id: null,
                imprint_prices: []
            });
        };

        $scope.saveImprintPrices = function (imprint) {
            var selected = findImprint(imprint.id);

            if (!selected) {
                selected = imprint;
            }
            var data = {
                data: {
                    name: selected.name,
                    max_colors: selected.max_colors,
                    color_group_id: selected.color_group_id,
                    imprint_prices: selected.imprint_prices
                }
            };

            $scope.saveProduct().then(function () {

                if (imprint.id == 'new') {
                    data.method = 'POST';
                    data.url = '/admin/api/product/' + $scope.product.id + '/imprint';
                } else {
                    data.method = 'PATCH';
                    data.url = '/admin/api/product/' + $scope.product.id + '/imprint/' + imprint.id;
                }

                $http(data).then(function (res) {
                    selected = res.data;

                    $scope.product.imprints = $scope.product.imprints.map(function (imprint) {
                        if (imprint.id != res.data.id) {
                            return imprint;
                        } else {
                            return res.data;
                        }
                    });

                }, function errorCallback(response) {
                    validationErrorsHandler.errorsHandling(response, 'Error on server');
                });
            });
        };

        $scope.cancelImprintPrices = function (imprintId) {
            var selected = findImprint(imprintId);
            selected.imprint_prices = selected.imprint_prices.filter(function (imprintPrice) {
                return imprintPrice.id != null;
            });

        };

        $scope.deleteImprintPrice = function (imprintId, id) {
            var selected = findImprint(imprintId);
            $http({
                method: 'DELETE',
                url: '/admin/api/product/' + $scope.product.id + '/imprint/' + imprintId + '/imprint-price/' + id
            }).then(function (res) {
                selected.imprint_prices = selected.imprint_prices.filter(function (imprintPrice) {
                    return imprintPrice.id != id;
                });
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.addImprintPrice = function (imprintId) {
            var selected = findImprint(imprintId);
            selected.imprint_prices.push({
                quantity: 1,
                setup_price: null,
                item_price: null,
                color_setup_price: null,
                color_item_price: null,
                id: null
            });
        };

        $scope.deleteImprint = function (imprintId) {
            modalHelper.delete('/admin/api/product/' + $scope.product.id + '/imprint/' + imprintId, 'Imprint')
                .then(function (modal) {
                    modal.element.modal();
                    modal.close.then(function (result) {
                        $scope.product.imprints = $scope.product.imprints.filter(function (imprint) {
                            return imprint.id != imprintId;
                        });
                    });
                });
        };

        /**
         *
         * @returns {*}
         */
        $scope.addCustomColor = function (imprintId) {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/colors/partials/find-color.html",
                controller: "FindColorController"
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (data) {
                    $scope.addColor(data.item.id, imprintId);
                });
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        }

        $scope.addColor = function (id, imprintId) {
            $http.post(
                '/admin/api/product/' + $scope.product.id + '/imprint/' + imprintId + '/imprint-color',
                {'color_id': id}
            ).then(function (res) {
                getProduct();
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        }

        $scope.dropColor = function (id, imprintId) {
            $http.delete(
                '/admin/api/product/' + $scope.product.id + '/imprint/' + imprintId + '/imprint-color/' + id
            ).then(function (res) {
                getProduct();
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        }

        $scope.getCategoryStyle = function(nested) {
            switch (nested) {
                case 0:
                    return "background: #ffffff !important; color: #000 !important";
                case 1:
                    return "background: #ffeb3b !important; color: #000 !important";
                case 2:
                    return "background: #cddc39 !important; color: #000 !important";
                case 3:
                    return "background: #ffc107 !important; color: #000 !important";
                case 4:
                    return "background: #8bc34a !important; color: #000 !important";
                default:
                    return "background: #03a9f4 !important; color: #000 !important";
            }
        }
    }]);
