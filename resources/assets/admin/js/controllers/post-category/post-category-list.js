/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('PostCategoryController', ['$scope', '$http', 'ModalService', 'modalHelper', 'validationErrorsHandler', '$sce',
    function ($scope, $http, ModalService, modalHelper, validationErrorsHandler, $sce) {

        $scope.trustAsHtml = $sce.trustAsHtml;

        $scope.getListData = function () {
            $http({
                method: 'GET',
                url: '/admin/api/post-category'
            }).then(function (res) {
                $scope.listData = res.data.data;
                $scope.hideLoader = true;
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error occurred while get post categories list');
            });
        };

        $scope.getListData();

        $scope.pagination = {
            currentPage: 1,
            totalItems: 0,
            maxSize: 10,
            lastPage: 1
        };

        $scope.deleteItem = function(itemId) {
            modalHelper.delete('/admin/api/post-category/' + itemId, 'Post Category')
                .then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(res) {
                        $scope.listData = $scope.listData.filter(function (item) {
                            return item.id != itemId;
                        });
                    });
                }, function (res) {
                    validationErrorsHandler.errorsHandling(res, 'Error on server');
                });
        };

        $scope.showEditAddForm = function (item) {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/main/partials/post-category.html",
                inputs: {
                    items: $scope.listData
                },
                controller: function ($scope, close, $http, $element, Upload) {
                    $scope.errors = null;

                    if (item) {
                        $scope.item = item;
                    } else {
                        $scope.item = {};
                    }

                    $scope.saveItem = function () {
                        if (item) {
                            $scope.item._method = 'put';
                            Upload.upload({
                                url: '/admin/api/post-category/' + item.id,
                                data: $scope.item
                            }).then(function (res) {
                                toastr.success('Successfully updated post category');
                                $element.modal('hide');
                                close({
                                    item: res.data,
                                    isNew: false
                                }, 200);
                            }, function (res) {
                                $scope.errors = res.data;
                                validationErrorsHandler.errorsHandling(res, 'Error on server');
                            });
                        } else {
                            Upload.upload({
                                method: 'POST',
                                url: '/admin/api/post-category/',
                                data: $scope.item
                            }).then(function (res) {
                                toastr.success('Successfully created post category');
                                $element.modal('hide');
                                close({
                                    item: res.data,
                                    isNew: true
                                }, 200);
                            }, function (res) {
                                $scope.errors = res.data;
                                validationErrorsHandler.errorsHandling(res, 'Error on server');
                            });
                        }
                    }
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(data) {
                    if (data.isNew) {
                        $scope.listData.push(data.item);
                    }
                });
            });
        };
    }]);