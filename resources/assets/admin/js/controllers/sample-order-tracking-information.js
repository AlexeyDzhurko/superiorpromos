app.controller('SampleOrderTrackingInformationController', ['$scope', 'close', '$http', 'orderItem', '$element', 'dictionary', 'validationErrorsHandler',
    function ($scope, close, $http, orderItem, $element, dictionary, validationErrorsHandler) {

    $scope.tracking_information = {}
    $scope.orderItem = angular.copy(orderItem);
    $scope.orderItem.tracking_notify_customer = 0;
    $scope.orderItem.tracking_request_rating = 0;

    var getTrackingInformations = function() {
        $http({
            method: 'GET',
            url: '/admin/api/order_sample_item/'+orderItem.id+'/tracking_information'
        }).then(function (res) {
            $scope.getTrackingInformations = res.data;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    getTrackingInformations();

    $scope.updateTrackingInformation = function () {
        $http({
            method: 'POST',
            url: '/admin/api/order_sample_item/' + orderItem.id + '/tracking_information',
            data: $scope.tracking_information
        }).then(function(res){
            $element.modal('hide');
            close($scope.tracking_information, 200);
            toastr.success("Successfully updated tracking information");
            //$scope.tracking_information = {};
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, response.data.message);
        });
    };

}]);
