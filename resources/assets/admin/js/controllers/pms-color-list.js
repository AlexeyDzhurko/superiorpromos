app.controller('PmsColorListController', ['$scope', '$http', 'modalHelper', 'validationErrorsHandler',
    function ($scope, $http, modalHelper, validationErrorsHandler) {

    var getPmsColors = function () {
        $http({
            method: 'GET',
            url: '/admin/api/pms-color'
        }).then(function (res) {
            $scope.pmsColors = res.data;
            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    getPmsColors();

    $scope.savePmsColor = function (data, pmsColorId, pmsColor) {
        if (pmsColorId) {
            $http({
                method: 'PATCH',
                url: '/admin/api/pms-color/' + pmsColorId,
                data: data
            }).then(function () {
                //
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        } else {
            $http({
                method: 'POST',
                url: '/admin/api/pms-color',
                data: data
            }).then(function (res) {
                pmsColor.id =  res.data.id;
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        }
    };

    $scope.addPmsColor = function () {
        $scope.pmsColors.push({
            id: null,
            name: null,
            color_hex: null
        })
    };

    $scope.deletePmsColor = function(pmsColorId) {
        modalHelper.delete('/admin/api/pms-color/' + pmsColorId, 'PMS Color')
            .then(function(modal) {
                modal.element.modal();
                modal.close.then(function(res) {
                    $scope.pmsColors = $scope.pmsColors.filter(function (pmsColor) {
                        return pmsColor.id != pmsColorId;
                    });
                });
            });
    };

}]);