app.controller('ReminderListController', ['$scope', '$http', 'validationErrorsHandler',
    function ($scope, $http, validationErrorsHandler) {

    $scope.listData = [];

    $scope.search = function () {
        $http({
            method: 'GET',
            url: '/admin/api/reminder',
            params: $scope.searchData
        }).then(function (res) {
            $scope.listData = res.data;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        })
    };

    $scope.hideLoader = true;

}]);