/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
/* eslint-disable angular/controller-as */
app.controller('AdministratorController', ['$scope', '$http', 'ModalService', 'modalHelper', 'validationErrorsHandler',
    function ($scope, $http, ModalService, modalHelper, validationErrorsHandler) {
        $scope.getListData = function () {
            $http({
                method: 'GET',
                url: '/admin/api/administrators'
            }).then(function (res) {
                $scope.listData = res.data.data;
                $scope.hideLoader = true;
            }, function errorCallback(response) {
                $scope.hideLoader = true;
                validationErrorsHandler.errorsHandling(response, 'Error occurred while get Administrators');
            });
        };

        $scope.getListData();

        $scope.deleteItem = function(itemId) {
            modalHelper.delete('/admin/api/administrators/' + itemId, 'User element')
                .then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function() {
                        $scope.listData = $scope.listData.filter(function (item) {
                            return item.id !== itemId;
                        });
                    });
                }, function errorCallback(res) {
                    validationErrorsHandler.errorsHandling(res, 'Error occurred while delete Administrator');
                });
        };

        $scope.showEditAddForm = function (item) {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/main/partials/administrators.html",
                controller: function ($scope, close, $http, $element) {
                    $scope.errors = {};

                    $scope.item = {
                        active: 0,
                        can_delete: 0
                    };

                    if (item) {
                        $scope.item = item;
                    }

                    $scope.saveItem = function () {
                        if (item) {
                            $http({
                                method: 'PUT',
                                url: '/admin/api/administrators/' + item.id,
                                data: $scope.item
                            }).then(function (res) {
                                $element.modal('hide');
                                close({
                                    item: res.data,
                                    isNew: false
                                }, 200);
                            }, function errorCallback(response) {
                                validationErrorsHandler.errorsHandling(response, 'Error occurred while update Administrator');
                                $scope.errors = response.data;
                            });
                        } else {
                            $http({
                                method: 'POST',
                                url: '/admin/api/administrators',
                                data: $scope.item
                            }).then(function (res) {
                                $element.modal('hide');
                                close({
                                    item: res.data,
                                    isNew: true
                                }, 200);
                            }, function errorCallback(response) {
                                validationErrorsHandler.errorsHandling(response, 'Error occurred while save Administrator');
                                $scope.errors = response.data;
                            });
                        }
                    }
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(data) {
                    if (data.isNew) {
                        $scope.listData.push(data.item);
                    }
                });
            });
        };
    }]);
