app.controller('OrderController', ['$scope', 'ModalService', '$http', function ($scope, ModalService, $http) {

    $scope.hideLoader = true;

    $scope.removeOrder = function (id) {
        ModalService.showModal({
            templateUrl: "/admin-assets/views/modals/delete.html",
            controller: function ($scope, close) {
                $scope.title = 'Order';
                $scope.delete = function () {
                    $http({
                        method: 'DELETE',
                        url: '/admin/api/order/' + id
                    }).then(function (res) {
                        window.location = '/admin/order';
                    }, function errorCallback(response) {
                        validationErrorsHandler.errorsHandling(response, 'Error on server');
                    })
                }
            }
        }).then(function(modal) {
            modal.element.modal();
        });
    };
}]);
