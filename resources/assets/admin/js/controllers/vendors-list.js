/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('VendorsListController', ['$scope', '$http', 'ModalService', 'modalHelper', 'validationErrorsHandler',
    function ($scope, $http, ModalService, modalHelper, validationErrorsHandler) {
        $scope.currentSort = 'created_at';
        $scope.searchData = {};
        $scope.pagination = {
            currentPage: 1,
            totalItems: 0,
            maxSize: 10,
            lastPage: 1
        };

        $scope.statuses = [
            {id: '1', text: 'Active only'},
            {id: '0', text: 'Hidden only'}
        ];

        $scope.getListData = function (page) {
            let params = $scope.searchData;

            if (page) {
                params.page = page;
            }

            $http({
                method: 'GET',
                url: '/admin/api/vendor',
                params: params
            }).then(function (res) {
                $scope.listData = res.data.data;
                $scope.pagination.totalItems = res.data.total;
                $scope.pagination.lastPage = res.data.last_page;
                $scope.pagination.currentPage = res.data.current_page;
                $scope.pagination.per_page = res.data.per_page;
                $scope.hideLoader = true;
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error occurred while get vendors list');
            });
        };

        $scope.getListData();


        $scope.deleteItem = function(itemId) {
            modalHelper.delete('/admin/api/vendor/' + itemId, 'Vendor')
                .then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(res) {
                        $scope.listData = $scope.listData.filter(function (item) {
                            return item.id != itemId;
                        });
                    });
                }, function (res) {
                    validationErrorsHandler.errorsHandling(res, 'Error on server');
                });
        };

        $scope.showEditAddForm = function (item) {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/main/partials/vendor.html",
                controller: function ($scope, close, $http, $element) {
                    $scope.errors = null;

                    if(item) {
                        $scope.item = item;
                    } else {
                        $scope.item = {};
                    }

                    $scope.states = [];
                    $http({
                        method: 'GET',
                        url: '/admin/api/state'
                    }).then(function (res) {
                        $scope.states = res.data;
                    }, function errorCallback(response) {
                        validationErrorsHandler.errorsHandling(response, 'Error on server');
                    });

                    $scope.paymentInformationTemplates = [];
                    $http({
                        method: 'GET',
                        url: '/admin/api/payment-information-template'
                    }).then(function (res) {
                        $scope.paymentInformationTemplates = res.data;
                    }, function errorCallback(response) {
                        validationErrorsHandler.errorsHandling(response, 'Error on server');
                    });

                    $scope.saveItem = function () {
                        if (item) {
                            $http({
                                method: 'PUT',
                                url: '/admin/api/vendor/' + item.id,
                                data: $scope.item
                            }).then(function (res) {
                                $element.modal('hide');
                                close({
                                    item: res.data,
                                    isNew: false
                                }, 200);
                            }, function errorCallback(response) {
                                $scope.errors = response.data;
                                validationErrorsHandler.errorsHandling(response, 'Error on server');
                            });
                        } else {
                            $http({
                                method: 'POST',
                                url: '/admin/api/vendor',
                                data: $scope.item
                            }).then(function (res) {
                                $element.modal('hide');
                                close({
                                    item: res.data,
                                    isNew: true
                                }, 200);
                            }, function errorCallback(response) {
                                $scope.errors = response.data;
                                validationErrorsHandler.errorsHandling(response, 'Error on server');
                            });
                        }
                    }
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(data) {
                    if (data.isNew) {
                        $scope.listData.push(data.item);
                    }
                });
            });
        };

        $scope.search = function () {
            $scope.getListData();
        };

        $scope.resetSearch = function () {
            $scope.searchData = {};
            $scope.getListData(1);
        };

        $scope.clearSearch = function () {
            $scope.searchData = {};
        };

        $scope.sort = function (field, order) {
            $scope.searchData.order = field;
            $scope.searchData.orderDirection = order;
            $scope.getListData(1);
        };
    }]);
