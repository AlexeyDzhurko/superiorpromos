/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('FAQListController', ['$scope', '$http', 'ModalService', 'modalHelper', 'validationErrorsHandler',
    function ($scope, $http, ModalService, modalHelper, validationErrorsHandler) {

        $scope.getListData = function (page) {

            $scope.hideLoader = false;

            if (page) {
                params.page = page;
            } else {
                params = {};
            }

            $http({
                method: 'GET',
                url: '/admin/api/faqs',
                params: params
            }).then(function (res) {
                $scope.listData = res.data.data;
                $scope.pagination.totalItems = res.data.total;
                $scope.pagination.lastPage = res.data.last_page;
                $scope.pagination.currentPage = res.data.current_page;
                $scope.pagination.per_page = res.data.per_page;
                $scope.hideLoader = true;
            }, function errorCallback(response) {
                $scope.hideLoader = true;
                validationErrorsHandler.errorsHandling(response, 'Error occurred while get FAQ\'s list');
            });
        };

        $scope.getListData();

        $scope.pagination = {
            currentPage: 1,
            totalItems: 0,
            maxSize: 10,
            lastPage: 1
        };

        $scope.searchData = {};

        $scope.deleteItem = function(itemId) {
            modalHelper.delete('/admin/api/faqs/' + itemId, 'FAQ element')
                .then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(res) {
                        $scope.listData = $scope.listData.filter(function (item) {
                            return item.id != itemId;
                        });
                    });
                }, function (res) {
                    validationErrorsHandler.errorsHandling(res, 'Error on server');
                });
        };

        $scope.showEditAddForm = function (item) {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/main/partials/faq.html",
                controller: function ($scope, close, $http, $element) {
                    $scope.errors = {};

                    $scope.item = {active: 0};

                    if (item) {
                        $scope.item = item;
                    }

                    $scope.tinymceOptions = {
                        plugins: 'link image code',
                        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                    };

                    $scope.saveItem = function () {
                        if (item) {
                            $http({
                                method: 'PUT',
                                url: '/admin/api/faqs/' + item.id,
                                data: $scope.item
                            }).then(function (res) {
                                $element.modal('hide');
                                close({
                                    item: res.data,
                                    isNew: false
                                }, 200);
                            }, function errorCallback(response) {
                                validationErrorsHandler.errorsHandling(response, 'Error on server');
                                $scope.errors = response.data;
                            });
                        } else {
                            $http({
                                method: 'POST',
                                url: '/admin/api/faqs',
                                data: $scope.item
                            }).then(function (res) {
                                $element.modal('hide');
                                close({
                                    item: res.data,
                                    isNew: true
                                }, 200);
                            }, function errorCallback(response) {
                                validationErrorsHandler.errorsHandling(response, 'Error on server');
                                $scope.errors = response.data;
                            });
                        }
                    }
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(data) {
                    if (data.isNew) {
                        $scope.listData.push(data.item);
                    }
                });
            });
        };
    }]);
