app.controller('FroogleController', ['$scope', '$http', 'validationErrorsHandler', function ($scope, $http, validationErrorsHandler) {
    $scope.categories = [];
    $scope.generated = false;
    $scope.selected = {

    };
    $http({
        method: 'GET',
        url: '/admin/api/category'
    }).then(function (res) {
        $scope.categories = res.data;
        $scope.hideLoader = true;
    }, function errorCallback(response) {
        validationErrorsHandler.errorsHandling(response, 'Error on server');
    });

    $scope.export = function () {
        $scope.hideLoader = false;

        $http({
            method: 'POST',
            url: '/admin/froogle/generate',
            data: {
                category_id: $scope.selected.category_id
            }
        }).then(function (res) {
            $scope.generated = true;
            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
            $scope.hideLoader = true;
        });
    };
}]);
