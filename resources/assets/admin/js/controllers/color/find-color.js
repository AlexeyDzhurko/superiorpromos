app.controller('FindColorController', ['$scope', 'close', '$http', '$element', '$sce', 'validationErrorsHandler',
        function ($scope, close, $http, $element, $sce, validationErrorsHandler) {
            $scope.colors = null;
            $scope.findColor = null;

            $scope.getColors = function (query) {
                return $http({
                    method: 'GET',
                    url: '/admin/api/search-colors',
                    params: {
                        query: query
                    }
                }).then(function (res) {
                    $scope.colors = res.data;
                }, function errorCallback(response) {
                    validationErrorsHandler.errorsHandling(response, 'Error on server');
                });
            }

            $scope.onSelected = function (item, model) {
                $scope.findColor = item;
            }

            $scope.selectColor = function (){
                $element.modal('hide');
                close({
                    item: $scope.findColor
                }, 200);
            }
        }
    ]
);