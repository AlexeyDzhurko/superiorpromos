/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('ColorsListController', ['$scope', '$http', 'ModalService', 'modalHelper', 'validationErrorsHandler',
    function ($scope, $http, ModalService, modalHelper, validationErrorsHandler) {

        $scope.getListData = function (page) {

            $scope.hideLoader = false;

            if (page) {
                params.page = page;
            } else {
                params = {};
            }

            $http({
                method: 'GET',
                url: '/admin/api/colors',
                params: params
            }).then(function (res) {
                $scope.listData = res.data.data;
                $scope.pagination.totalItems = res.data.total;
                $scope.pagination.lastPage = res.data.last_page;
                $scope.pagination.currentPage = res.data.current_page;
                $scope.pagination.per_page = res.data.per_page;
                $scope.hideLoader = true;
            }, function errorCallback(response) {
                $scope.hideLoader = true;
                validationErrorsHandler.errorsHandling(response, 'Error occurred while get colors list');
            });
        };

        $scope.getListData();

        $scope.pagination = {
            currentPage: 1,
            totalItems: 0,
            maxSize: 10,
            lastPage: 1
        };

        $scope.searchData = {};

        $scope.deleteItem = function(itemId) {
            modalHelper.delete('/admin/api/colors/' + itemId, 'Color')
                .then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(res) {
                        toastr.success("Successfully deleted color");
                        $scope.listData = $scope.listData.filter(function (item) {
                            return item.id != itemId;
                        });
                    });
                }, function (res) {
                    validationErrorsHandler.errorsHandling(res, 'Error on server');
                });
        };

        $scope.showEditAddForm = function (item) {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/main/partials/colors.html",
                controller: function ($scope, close, $http, Upload, $element) {
                    $scope.errors = null;
                    $scope.image = false;

                    $scope.options = {
                        // color
                        format: 'hexString',
                        alpha: false,
                        round: true,
                        case: 'lower'
                    };

                    $scope.pms_colors = [];

                    $http({
                        method: 'GET',
                        url: '/admin/api/pms-color/'
                    }).then(function(res) {
                        $scope.pms_colors = res.data;
                    }, function errorCallback(response) {
                        validationErrorsHandler.errorsHandling(response, 'Error on server');
                    });

                    $scope.color_groups = [];

                    $http({
                        method: 'GET',
                        url: '/admin/api/all-color-groups/'
                    }).then(function(res) {
                        $scope.color_groups = res.data;
                    }, function errorCallback(response) {
                        validationErrorsHandler.errorsHandling(response, 'Error on server');
                    });

                    if (item) {
                        $scope.item = item;
                    } else {
                        $scope.item = {
                            id: -1,
                            name: "",
                            color_hex: "",
                            picture_src: null,
                            picture_width: null,
                            picture_height: null,
                            created_at: null,
                            updated_at: null,
                            pms_color_id: null,
                            groups: [],
                            pms_color: null,
                        }
                    }

                    $scope.saveItem = function () {
                        $scope.item.groups = $scope.item.groups.map(function(obj) { return obj.id; });

                        if(!$scope.pms_color_id) {
                            delete $scope.item.pms_color_id;
                        }

                        if($scope.image) {
                            $scope.item.image = $scope.image;
                        } else {
                            delete $scope.item.image;
                        }

                        if (item) {
                            $scope.item._method = 'put';
                            Upload.upload({
                                url: '/admin/api/colors/' + item.id,
                                data: $scope.item
                            }).then(function (res) {
                                $element.modal('hide');
                                close({
                                    item: res.data,
                                    isNew: false
                                }, 200);
                                toastr.success("Successfully updated color");
                            }, function (res) {
                                $scope.errors = res.data;
                                validationErrorsHandler.errorsHandling(res, 'Error on server');
                            });
                        } else {
                            Upload.upload({
                                method: 'POST',
                                url: '/admin/api/colors/',
                                data: $scope.item
                            }).then(function (res) {
                                $element.modal('hide');
                                close({
                                    item: res.data,
                                    isNew: true
                                }, 200);
                                toastr.success("Successfully added color");
                            }, function (res) {
                                $scope.errors = res.data;
                                validationErrorsHandler.errorsHandling(res, 'Error on server');
                            });
                        }
                    }
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(data) {
                    if (data.isNew) {
                        $scope.listData.push(data.item);
                    }
                });
            });
        };
    }]);