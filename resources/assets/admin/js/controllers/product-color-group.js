app.controller('ProductColorGroupController', ['$scope', '$http', '$filter', 'Upload', 'validationErrorsHandler',
    function ($scope, $http, $filter, Upload, validationErrorsHandler) {

    $scope.colorGroups = [];
    $scope.colors = [];
    $scope.colorGroup = {};
    $scope.step_1 = true;
    $scope.step_2 = false;
    $scope.productColor = {};
    $scope.productColor.product_color_prices = [];

    $http({
        method: 'GET',
        url: '/admin/api/color-group/'
    }).then(function (res) {
        $scope.colorGroups = res.data;
    }, function errorCallback(response) {
        validationErrorsHandler.errorsHandling(response, 'Error on server');
    });

    $scope.uploadImage = function () {
        $scope.file.upload = Upload.upload({
            url: '/admin/api/product/' + product.id + '/product-color-group/1/product-color/1',
            data: {
                file: $scope.file,
                _method: 'patch'
            }
        }).then(function (res) {
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    $scope.addPrice = function () {
        console.log($scope.productColor)
        $scope.productColor.product_color_prices.push({
            quantity: 1,
            setup_price: null,
            item_price: null,
            id: null
        });
    };


    $scope.createColorGroup = function () {
        $scope.colorGroup = 1;
        if ($scope.colorGroup != null) {
            $http({
                method: 'GET',
                url: '/admin/api/color/',
                params: {
                    color_group_id: $scope.colorGroup
                }
            }).then(function (res) {
                $scope.colors = res.data;
                $scope.step_1 = false;
                $scope.step_2 = true;
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        } else {
            $scope.step_1 = false;
            $scope.step_2 = true;
        }
    };

}]);
