/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('BlockListController', ['$scope', '$http', 'ModalService', 'validationErrorsHandler',
    function ($scope, $http, ModalService, validationErrorsHandler) {

    $scope.getListData = function () {

        $http({
            method: 'GET',
            url: '/admin/api/block'
        }).then(function (res) {
            $scope.listData = res.data;
            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error occurred while get block list');
        });
    };

    $scope.getListData();

    $scope.showEditAddForm = function (item) {

        ModalService.showModal({
            templateUrl: "/admin-assets/views/main/partials/block.html",
            controller: function ($scope, $rootScope, close, $http, $element) {
                $scope.errors = null;
                $scope.item = {active: 0};

                if (item) {
                    $scope.item = item;
                }

                $scope.tinymceOptions = $rootScope.tinymceOptions;

                $scope.saveItem = function () {
                    $http({
                        method: 'PATCH',
                        url: '/admin/api/block/' + item.id,
                        data: $scope.item
                    }).then(function (res) {
                        $element.modal('hide');
                        close({
                            item: res.data
                        }, 200);
                    }, function errorCallback(response) {
                        $scope.errors = response.data;
                        validationErrorsHandler.errorsHandling(response, 'Error occurred while update block');
                    });
                }
            }
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(data) {

            });
        });
    };
}]);
