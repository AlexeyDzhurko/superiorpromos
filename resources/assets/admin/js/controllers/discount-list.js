app.controller('DiscountListController', ['$scope', '$http', 'ModalService', 'validationErrorsHandler',
    function ($scope, $http, ModalService, validationErrorsHandler) {

    $scope.discounts = [];

    $scope.deleteDiscount = function(discountId) {
        ModalService.showModal({
            templateUrl: "/admin-assets/views/modals/delete.html",
            controller: function ($scope, close) {
                $scope.title = 'discount';
                $scope.delete = function () {

                    $http({
                        method: 'DELETE',
                        url: '/admin/api/discount/' + discountId
                    }).then(function (res) {

                    }, function errorCallback(response) {
                        validationErrorsHandler.errorsHandling(response, 'Error on server');
                    })

                }
            }
        }).then(function(modal) {
            modal.element.modal();
        });
    };


    $scope.dragControlListeners = {
        // accept: function (sourceItemHandleScope, destSortableScope) {return true},
        itemMoved: function (event) {
            console.log(event);
        },
        orderChanged: function(event) {

            var current = $scope.discounts[event.dest.index];
            var prev = $scope.discounts[event.dest.index - 1];
            var next = $scope.discounts[event.dest.index + 1];

            if (typeof prev != 'undefined') {
                var position_discount_id = prev.id;
                var type = 'moveAfter';
            } else {
                var position_discount_id = next.id;
                var type = 'moveBefore';
            }

            $http({
                method: 'POST',
                url: '/admin/api/discount/' + current.id + '/sort',
                data: {
                    position_discount_id:  position_discount_id,
                    type: type
                }
            }).then(function (res) {
                getDiscounts();
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        }
        // containment: '#board',
        // clone: true,
        // allowDuplicates: false
    };


    var getDiscounts = function () {
        $http({
            method: 'GET',
            url: '/admin/api/discount'
        }).then(function (res) {
            $scope.discounts = res.data;
            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    getDiscounts();

}]);