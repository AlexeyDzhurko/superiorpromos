app.controller('SampleOrderAddressController', ['$scope', 'close', '$http', 'address', 'prefix', 'orderItem', '$element', 'validationErrorsHandler',
    function ($scope, close, $http, address, prefix, orderItem, $element, validationErrorsHandler) {

    $scope.address = address;

    var fields = [
        'title',
        'first_name',
        'last_name',
        'middle_name',
        'suffix',
        'company_name',
        'address_line_1',
        'address_line_2',
        'city',
        'state',
        'zip',
        'country',
        'province',
        'day_telephone'
    ];

    $scope.user = {};
    angular.forEach(fields, function(field) {
        $scope.user[field] =  orderItem[prefix + '_' +  field];
    });

    $scope.updateAddress = function() {
        var data = {};
        angular.forEach($scope.user, function(value, key) {
            data[prefix + '_' + key] = value;
        });

        $http({
            method: 'PATCH',
            url: '/admin/api/order_sample_item/' + orderItem.id,
            data: data
        }).then(function(res){
            $element.modal('hide');
            close(data, 200);
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error occurred while address updating');
        });
    };

}]);