app.controller('ProductSubOptionController', ['$scope', '$http', '$filter', 'productOption', 'validationErrorsHandler',
    function ($scope, $http, $filter, productOption, validationErrorsHandler) {

    $scope.productOption = productOption;

    var findProductSubOption = function (id) {
        return $scope.productOption.product_sub_options.find(function(productSubOption) {
            return productSubOption.id == id;
        });
    };

    $scope.deleteSubOptionPrice = function(productSubOptionId, id) {
        var selected =  findProductSubOption(productSubOptionId);
        $http({
            method: 'DELETE',
            url: '/admin/api/product/' + product.id + '/product-option/' + productOption.id + '/product-sub-option/' + productSubOptionId + '/product-sub-option-price/' + id
        }).then(function (res) {
            selected.product_sub_option_prices = selected.product_sub_option_prices.filter(function (productSubOptionPrice) {
                return productSubOptionPrice.id != id;
            });
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    $scope.addProductSubOptionPrice = function(productSubOptionId) {
        var selected =  findProductSubOption(productSubOptionId);
        selected.product_sub_option_prices.push({
            quantity: 1,
            setup_price: null,
            item_price: null,
            id: null
        });
    };

    $scope.cancelSubOptionPrices = function(productSubOptionId) {
        var selected =  findProductsubOption(productSubOptionId);
        selected.product_sub_option_prices = selected.product_sub_option_prices.filter(function (productSubOptionPrice) {
            return productSubOptionPrice.id != null;
        });
    };

    // save edits
    $scope.saveSubOptionPrices = function(productSubOptionId) {
        var selected =  findProductSubOption(productSubOptionId);

        var data = {
            data: {
                name: selected.name,
                product_sub_option_prices: selected.product_sub_option_prices
            }
        };

        if (productSubOptionId == 'new') {
            data.method = 'POST';
            data.url = '/admin/api/product/' + product.id + '/product-option/' + productOption.id + '/product-sub-option';
        } else {
            data.method = 'PATCH';
            data.url = '/admin/api/product/' + product.id + '/product-option/' + productOption.id + '/product-sub-option/' + productSubOptionId;
        }

        $http(data).then(function (res) {
            selected = res.data;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    $scope.deleteSubOption = function(productSubOptionId) {
        $http({
            method: 'DELETE',
            url: '/admin/api/product/' + product.id + '/product-option/' + productOption.id + '/product-sub-option/' + productSubOptionId
        }).then(function (res) {
            $scope.productOption.product_sub_options = $scope.productOption.product_sub_options.filter(function (productSubOption) {
                return productSubOption.id != productSubOptionId;
            });
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    $scope.addProductSubOption = function () {
        $scope.productOption.product_sub_options.push({
            id: 'new',
            name: null,
            product_sub_option_prices: []
        });
    };

}]);