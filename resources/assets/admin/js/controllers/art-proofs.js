app.controller('ArtProofsController', ['$scope', 'close', '$http', 'orderItem', 'Upload', '$element', '$sce', 'validationErrorsHandler',
    function ($scope, close, $http, orderItem, Upload, $element, $sce, validationErrorsHandler) {

    $scope.trustAsHtml = $sce.trustAsHtml;

    $scope.orderItem = orderItem;
    $scope.orderId = `${orderItem.order_id}S${orderItem.id}`;
    $scope.approve_required = 0 ;

    var getArtProofs = function() {
        $http({
            method: 'GET',
            url: '/admin/api/order_item/'+orderItem.id+'/art_proof'
        }).then(function (res) {
            $scope.artProofs = res.data;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error occurred while get art proofs');
        });
    };

    getArtProofs();

    $scope.tinymceOptions = {
        plugins: 'link image code',
        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
    };

    $scope.addArtProof = function () {

        $scope.file.upload = Upload.upload({
            url: '/admin/api/order_item/' + orderItem.id + '/art_proof',
            data: {
                _method: 'POST',
                file: $scope.file,
                note: $scope.note,
                approve_required: $scope.approve_required
            }
        }).then(function (res) {
            getArtProofs();
            $scope.note = '';
            $scope.approve_required = 0;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });

    };


}]);
