app.controller('StateListController', ['$scope', '$http', 'validationErrorsHandler',
    function ($scope, $http, validationErrorsHandler) {

    var getStates = function () {
        $http({
            method: 'GET',
            url: '/admin/api/state'
        }).then(function (res) {
            $scope.states = res.data;
            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    getStates();

    $scope.saveState = function (data, stateId) {

        $http({
            method: 'PATCH',
            url: '/admin/api/state/' + stateId,
            data: data
        }).then(function (res) {
            getStates();
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    }

}]);