app.controller('ProductCategoryController', ['$scope', '$http', '$interpolate', 'formHelper', 'ModalService', 'modalHelper', '$window', 'validationErrorsHandler', '$filter', '$sce',
    function ($scope, $http, $interpolate, formHelper, ModalService, modalHelper, $window, validationErrorsHandler, $filter, $sce) {

    $scope.activateProducts = function() {

        $scope.hideLoader = false;

        $http({
            method: 'POST',
            url: '/admin/api/product-category/activate',
            data: {'products_id' : $scope.selectedProducts}
        }).then(function (res) {
            $scope.selectedProducts.forEach(function (productId) {
                var product = $scope.getProductById(productId);

                product.active = 1;
            });

            $scope.selectedProducts = [];
            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
            $scope.hideLoader = true;
        });
    };

    $scope.deactivateProducts = function() {

        $scope.hideLoader = false;

        $http({
            method: 'POST',
            url: '/admin/api/product-category/deactivate',
            data: {'products_id' : $scope.selectedProducts}
        }).then(function (res) {
            $scope.selectedProducts.forEach(function (productId) {
                var product = $scope.getProductById(productId);

                product.active = 0;
            });

            $scope.selectedProducts = [];
            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
            $scope.hideLoader = true;
        });
    };

    $scope.showActivationDeactivationModal = function(operationType) {
        ModalService.showModal({
            templateUrl: "activation-inactivation-modal.html",
            inputs: {
                parentScope: $scope
            },
            controller: function ($scope, close, parentScope) {

                $scope.parentScope = parentScope;
                $scope.productsId = parentScope.selectedProducts;
                $scope.activationStatus = operationType;

                $scope.close = function(result) {
                    close(result, 500); // close, but give 500ms for bootstrap to animate
                };
            },
            preClose: function (modal) {
                modal.element.modal('hide');
            }
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                if(result) {
                    if(operationType == 1) {
                        $scope.activateProducts();
                    } else {
                        $scope.deactivateProducts();
                    }
                };
            });
        });
    };

    $scope.trustAsHtml = $sce.trustAsHtml;

    $scope.selectedCategory = null;

    $scope.selectCategoryForTransfer = function (categoryId) {
        if($scope.selectedCategory == categoryId) return $scope.selectedCategory = null;
        return $scope.selectedCategory = categoryId;
    };

    $scope.clearCategorySearch = function() {
        $scope.pattern = '';
    };

    $scope.transferByButton = function() {
        if($scope.selectedProducts.length < 1) {
            alert("You didn't select products");
            return false;
        }

        if($scope.selectedCategory == null) {
            alert("Please choose category for transfer");
            return false;
        }

        if($scope.checkExistsCategoryFromProducts($scope.selectedCategory)) return false;

        $scope.showTransferConfirmModal($scope.selectedCategory, $scope.selectedProducts);
    };

    $scope.getCategoryById = function (categoryId) {
        return $scope.categoriesList.find(function(category) {
            return category.id == categoryId;
        });
    };

    $scope.getProductById = function (productId) {
        return $scope.listData.find(function(product) {
            return product.id == productId;
        });
    };

    $scope.searchByCategoryId = function (categoryId) {
        $scope.searchData = {'category_id' : categoryId};
        $scope.search();
    };

    $scope.showDeleteCategoriesModal = function(categoryId, productsId) {
        ModalService.showModal({
            templateUrl: "drop-delete-modal.html",
            inputs: {
                parentScope: $scope
            },
            controller: function ($scope, close, parentScope) {

                $scope.parentScope = parentScope;
                $scope.productsId = productsId;
                $scope.categoryId = categoryId;

                $scope.close = function(result) {
                    close(result, 500); // close, but give 500ms for bootstrap to animate
                };
            },
            preClose: function (modal) {
                modal.element.modal('hide');
            }
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                if(result) $scope.deleteCategoryRequest(categoryId, productsId);
            });
        });
    };

    $scope.deleteCategoryRequest = function(categoryId, productsId) {
        $scope.hideLoader = false;

        var data = [];

        productsId.forEach(function (productId) {
            data.push({
                'product_id' : productId,
                'category_id' : categoryId
            });
        });

        $http({
            method: 'POST',
            url: '/admin/api/product-category/delete',
            data: {'data' : data}
        }).then(function (res) {
            $scope.selectedProducts.forEach(function (productId) {
                var product = $scope.getProductById(productId);

                if($scope.searchData.category_id == categoryId) {
                    var index = $scope.listData.indexOf(product);
                    $scope.listData.splice(index, 1)
                } else {
                    var index = product.categories.indexOf(categoryId);
                    product.categories.splice(index, 1);
                }
            });

            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
            $scope.hideLoader = true;
        });
    }

    $scope.deleteCategory = function(productId, categoryId) {
        $scope.showDeleteCategoriesModal(categoryId, [productId]);
    };

    $scope.deleteCategoryFromProducts = function() {
        if($scope.selectedProducts.length < 1) {
            alert("You didn't select products");
            return false;
        };

        if(!$scope.searchData.category_id) {
            alert("Please select category for display products and after select them for mass delete operation");
            return false;
        }

        $scope.showDeleteCategoriesModal($scope.searchData.category_id, $scope.selectedProducts);
    };

    $scope.treeFilter = $filter('uiTreeFilter');
    $scope.supportedFields = ['name'];

    function formingDataForRequest(categoryId, products) {
        var data = [];

        if(products.length > 0) {
            products.forEach(function(productId) {
                data.push({
                    'product_id' : productId,
                    'category_id' : categoryId
                });
            });
        } else {
            data.push({
                'product_id' : productId,
                'category_id' : categoryId
            });
        }

        return data;
    }

    $scope.showTransferConfirmModal = function(categoryId, productsId) {
        ModalService.showModal({
            templateUrl: "drop-modal.html",
            inputs: {
                parentScope: $scope
            },
            controller: function ($scope, close, parentScope) {

                $scope.parentScope = parentScope;

                $scope.categoryId = categoryId;
                $scope.productsId = productsId;

                $scope.close = function(result) {
                    close(result, 500); // close, but give 500ms for bootstrap to animate
                };
            },
            preClose: function (modal) {
                modal.element.modal('hide');
            }
        }).then(function(modal) {

            modal.element.modal();

            modal.close.then(function(result) {

                if(result) {

                    $scope.hideLoader = false;

                    data = formingDataForRequest(categoryId, productsId);

                    $http({
                        method: 'POST',
                        url: '/admin/api/product-category',
                        data: {'data' : data}

                    }).then(function (res) {

                        productsId.forEach(function (element) {
                            var product = $scope.getProductById(element);
                            product.categories.push(categoryId);
                        });

                        $scope.selectedProducts = [];

                        $scope.hideLoader = true;

                    }, function errorCallback(response) {

                        validationErrorsHandler.errorsHandling(response, 'Error on server');
                        $scope.hideLoader = true;

                    });
                }
            });
        });
    };

    $scope.treeOptions = {
        beforeDrop : function (e) {

            if(typeof e.dest.nodesScope.node == 'undefined') {
               return false;
            }

            sourceValue = e.source.cloneModel.name ? e.source.cloneModel : undefined;
            destValue = e.dest.nodesScope.node;

            if($scope.selectedProducts.length > 0) {
                if($scope.checkExistsCategoryFromProducts(destValue.id)) return false;
            } else {
                if(sourceValue.categories.indexOf(destValue.id) !== -1) {
                    return false;
                }
            }

            if($scope.selectedProducts.length > 0) {
                $scope.showTransferConfirmModal(destValue.id, $scope.selectedProducts);
            } else {
                $scope.showTransferConfirmModal(destValue.id, [sourceValue.id]);
            }

            return false;
        }
    };

    $scope.checkExistsCategoryFromProducts = function(categoryId) {
        var existsCategories = false;
        var productsWhichHaveExistsCategories = [];
        var category = $scope.getCategoryById(categoryId);

        $scope.selectedProducts.forEach(function(item) {
            if($scope.getProductById(item).categories.indexOf(category.id) !== -1) {
                existsCategories = true;
                productsWhichHaveExistsCategories.push($scope.getProductById(item));
            }
        });

        if(existsCategories) {
            var errMsg = 'Products: ';
            productsWhichHaveExistsCategories.forEach(function(item) {
                errMsg = errMsg + '"' + item.name + '", ';
            });
            errMsg = errMsg + ' already have category "' + category.text + '"';

            alert(errMsg);
            return true;
        }

        return false;
    };

    $scope.categories = {};

    $scope.selectedProducts = [];

    $scope.toggleSelection = function toggleSelection(productId) {
        var idx = $scope.selectedProducts.indexOf(productId);

        // Is currently selected
        if (idx > -1) {
            $scope.selectedProducts.splice(idx, 1);
        }

        // Is newly selected
        else {
            $scope.selectedProducts.push(productId);
        }
    };


    if (typeof categories != 'undefined') {
        $scope.categories = categories;
    }

    $scope.toggle = function (scope) {
        scope.toggle();
    };

    $scope.collapseAll = function () {
        $scope.$broadcast('angular-ui-tree:collapse-all');
    };

    $scope.collapseAll();

    $scope.expandAll = function () {
        $scope.$broadcast('angular-ui-tree:expand-all');
    };

    $scope.pagination = {
        currentPage: 1,
        totalItems: 0,
        maxSize: 20,
        lastPage: 1
    };

    $scope.statuses = [
        {id: '1', text: 'Active only'},
        {id: '0', text: 'Hidden only'}
    ];

    $scope.sageStatuses = [
        {id: '1', text: 'Yes'},
        {id: '0', text: 'No'}
    ];

    $scope.colorImagesStatuses = [
        {id: 'complete', text: 'Complete'},
        {id: 'empty', text: 'Empty'},
        {id: 'none', text: 'None'}
    ];

    $scope.searchData = {};

    // get vendors for select
    $scope.vendors = [];
    $http({
        method: 'GET',
        url: '/admin/api/vendors-list'
    }).then(function (res) {
        $scope.vendors = res.data;
    }, function errorCallback(response) {
        validationErrorsHandler.errorsHandling(response, 'Error on server');
    });

    // get categories for select
    $scope.categoriesList = [];
    $scope.getCategoriesList = function () {
        $http({
            method: 'GET',
            url: '/admin/api/category/list'
        }).then(function (res) {
            $scope.categoriesList = res.data;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };
    $scope.getCategoriesList();

    // get categories for select
    $scope.categoriesListWithSeparator = [];
    $scope.getCategoriesListWithSeparator = function () {
        $http({
            method: 'GET',
            url: '/admin/api/category'
        }).then(function (res) {
            $scope.categoriesListWithSeparator = res.data;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };
    $scope.getCategoriesListWithSeparator();


    $scope.createPages = function(min, max, step) {
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
            input.push(i);
        }
        return input;
    };

    $scope.range = [];

    $scope.getColorImageStatus = function (colors) {
        if (colors.length == 0) {
            return 'empty';
        }
        var status = 'complete';
        colors.forEach(function (color) {
            if(color.image_src == null){
                status = 'none';
            }
        });
        return status;
    };

    $scope.getListData = function (page) {

        $scope.selectedProducts = [];

        $scope.hideLoader = false;

        var params = typeof $scope.searchData != 'undefined' ? $scope.searchData : {};
        if (page) {
            params.page = page;
        }

        if($scope.listData == undefined) {
            $scope.searchData.order = 'created_at';
            $scope.searchData.orderDirection = 'desc';
        }

        $http({
            method: 'GET',
            url: '/admin/api/product/_search',
            params: params,
        }).then(function (res) {
            $scope.listData = res.data.data;

            res.data.data.forEach(function (item, i) {
                res.data.data[i].colorImageStatus = $scope.getColorImageStatus(item.colors);
            });

            $scope.pagination.totalItems = res.data.meta.pagination.total;
            $scope.pagination.lastPage = res.data.meta.pagination.total_pages;
            $scope.pagination.currentPage = res.data.meta.pagination.current_page;
            $scope.pagination.per_page = res.data.meta.pagination.per_page;
            $scope.range = $scope.createPages(1, $scope.pagination.lastPage);

            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
            $scope.hideLoader = true;
        });
    };

    $scope.getAllProducts = function(page, params) {

        $http({
            method: 'GET',
            url: '/admin/api/product/_search',
            params: params,
        }).then(function (res) {
            res.data.data.forEach(function (item, i) {
                res.data.data[i].colorImageStatus = $scope.getColorImageStatus(item.colors);
            });

            $scope.listData = $scope.listData.concat(res.data.data);

            if(page + 1 <= $scope.pagination.lastPage) {
                $scope.getAllProducts(page + 1, params);
            } else {
                $scope.pagination = [];
                $scope.hideLoader = true;
            }
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
            $scope.hideLoader = true;
        });
    };

    $scope.displayAllProducts = function() {
        $scope.hideLoader = false;

        if($scope.listData == undefined) {
            $scope.searchData.order = 'created_at';
            $scope.searchData.orderDirection = 'desc';
        }

        var params = typeof $scope.searchData != 'undefined' ? $scope.searchData : {};

        $scope.listData = [];

        $scope.getAllProducts(1, params);
    };

    $scope.getListData(1);

    $scope.search = function () {
        $scope.getListData(1);
    };

    $scope.resetSearch = function () {
        $scope.searchData = {};
        $scope.search();
    };

    $scope.clearSearch = function () {
        $scope.searchData = {};
    };

    $scope.hideLoader = true;
}]).filter('trust', function ($sce) {
    return function (val) {
        return $sce.trustAsHtml(val);
    };
}).config(function (uiTreeFilterSettingsProvider) {
    uiTreeFilterSettingsProvider.descendantCollection = ['children'];
});