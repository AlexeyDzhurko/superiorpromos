app.controller('OrdersSampleListController', ['$window', '$scope', '$http', 'ModalService', 'validationErrorsHandler',
    function ($window, $scope, $http, ModalService, validationErrorsHandler) {

        $scope.searchData = {};
        $scope.searchData.stage_id = 0;

        $scope.vendors = [];
        $http({
            method: 'GET',
            url: '/admin/api/vendors-list'
        }).then(function (res) {
            $scope.vendors = res.data;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });

        $scope.getVendorTitle = function (vendorId) {
            var vendor = $scope.vendors.find(function (vendor) {
                return vendor.id == vendorId;
            });
            return vendor.text;
        };

        $scope.paid_statuses = [
            {id: '1', text: 'Not Paid'},
            {id: '0', text: 'Paid'}
        ];

        $scope.sample_statuses = [
            {id: '1', text: 'Yes'},
            {id: '0', text: 'No'}
        ];

        $scope.check_notes_statuses = [
            {id: '1', text: 'Have'},
            {id: '0', text: 'Not have'}
        ];

        $scope.states = [];
        $http({
            method: 'GET',
            url: '/admin/api/state'
        }).then(function (res) {
            $scope.states = res.data;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });

        $scope.getStateTitle = function (stateId) {
            var state = $scope.states.find(function (state) {
                return state.id == stateId;
            });
            return state.text;
        };

        $scope.showOrderItem = function (orderItemId) {
            $window.location.href = '/admin/order_item/' + orderItemId + '/edit';
        };

        $scope.showOrderStages = function (orderItem) {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/sample-orders/partials/stage.html",
                controller: "SampleOrdersStagesController",
                inputs: {
                    orderItem: orderItem
                }
            }).then(function (modal) {
                // The modal object has the element built, if this is a bootstrap modal
                // you can call 'modal' to show it, if it's a custom modal just show or hide
                // it as you need to.
                modal.element.modal();
                modal.close.then(function (result) {
                    $scope.message = result ? "You said Yes" : "You said No";
                });
            });
        };

        $scope.setImportantStatus = function (orderItem) {
            $http({
                method: 'PATCH',
                url: '/admin/api/order_item/' + orderItem.id,
                data: {'is_important': !orderItem.is_important}
            }).then(function (res) {
                //toastr.success("Successfully updated important status for order item");
                orderItem.is_important = !orderItem.is_important;
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error occurred while important status for order item updating');
            });
        };

        $scope.showArtProofsForm = function (orderItem) {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/sample-orders/partials/art-proofs.html",
                controller: "ArtProofsController",
                inputs: {
                    orderItem: orderItem
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showNotesForm = function (orderItem) {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/sample-orders/partials/notes.html",
                controller: "UserSampleOrderNotesController",
                inputs: {
                    orderItem: orderItem
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.createPages = function (min, max, step) {
            step = step || 1;
            var input = [];
            for (var i = min; i <= max; i += step) {
                input.push(i);
            }
            return input;
        };

        $scope.range = [];

        $scope.showExtraBillingForm = function (orderItem) {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/sample-orders/partials/extra-billing.html",
                controller: "ExtraBillingController",
                inputs: {
                    orderItem: orderItem
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.usedStages = {};
        $scope.orders = {};

        $scope.showOrders = function (stageId, page) {
            stageId = $scope.searchData.stage_id == undefined ? stageId : $scope.searchData.stage_id;
            $scope.hideLoader = false;

            var requestUrl = '/admin/api/order_sample/_search';
            var params = typeof $scope.searchData != 'undefined' ? $scope.searchData : {};
            params.page = page ? page : 1;

            $scope.pagination = {
                currentPage: 1,
                totalItems: 0,
                maxSize: 3,
                lastPage: 1
            };

            if ($scope.listData == undefined) {
                $scope.searchData.order = 'created_at';
                $scope.searchData.orderDirection = 'desc';
            }

            $http({
                method: 'GET',
                url: requestUrl,
                params: params
            }).then(function (res) {
                $scope.orders = res.data.data;
                $scope.pagination.totalItems = res.data.pagination.total;
                $scope.pagination.lastPage = res.data.pagination.last_page;
                $scope.pagination.currentPage = res.data.pagination.current_page;
                $scope.pagination.perPage = res.data.pagination.per_page;
                $scope.range = $scope.createPages(1, $scope.pagination.lastPage);
                $scope.filteredStages = res.data.filteredStages;
                if (typeof $scope.usedStages[0] == 'undefined') {
                    angular.forEach(res.data.stages, function (stage) {
                        $scope.usedStages[stage.stage_id] = stage;
                    });
                    $scope.usedStages[0] = {"name": "All stages", "stage_id": 0, "orders_count": res.data.data.total};
                }
                $scope.hideLoader = true;
            }, function errorCallback(response) {
                $scope.hideLoader = true;
                validationErrorsHandler.errorsHandling(response, 'Error occurred on server side');
            });
        };

        $scope.showOrders(0, 1);

        $scope.changePage = function (stageId, page) {
            $scope.pagination.currentPage = page;
            $scope.showOrders(stageId, page);
        };

        $scope.search = function () {
            $scope.showOrders();
        };

        $scope.resetSearch = function () {
            $scope.searchData = {};
            $scope.showOrders(0);
        };

        if (typeof ($scope.searchData.user_id = new URL(window.location.href).searchParams.get("user_id")) == 'string') {
            $scope.showOrders();
        }

        $scope.searchByStageId = function (stageId) {
            $scope.searchData.stage_id = stageId;
            $scope.search();
        };

        $scope.showOrderItemsByProductId = function (productId) {
            if ($scope.searchData.product_id == undefined) {
                return true;
            } else {
                return productId == $scope.searchData.product_id;
            }
        };

        $scope.showShippingAddressForm = function(orderItem) {
            $scope.showAddressForm(orderItem, "Shipping", "shipping")
        };

        $scope.showBillingAddressForm = function(orderItem) {
            $scope.showAddressForm(orderItem, "Billing", "billing")
        };

        $scope.showAddressForm = function (orderItem, address, prefix) {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/sample-orders/partials/address-form.html",
                controller: "SampleOrderAddressController",
                inputs: {
                    address: address,
                    prefix: prefix,
                    orderItem: orderItem
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    angular.forEach(result, function (value, key) {
                        orderItem[key] = value;
                        window.location.reload;
                    })
                });
            });
        }

        $scope.updateVendor = function(data) {
            $http({
                method: 'PATCH',
                url: '/admin/api/order_sample_item/' + orderItem.id,
                data: {
                    vendor_id: data
                }
            }).then(function(res){
                window.location.reload;
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.updatePurchaseOrderNumber = function(data) {
            $http({
                method: 'PATCH',
                url: '/admin/api/order_sample_item/' + orderItem.id,
                data: {
                    po_number: data
                }
            }).then(function(res){
                //
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.showTrackingInformationForm = function () {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/sample-orders/partials/tracking-information-form.html",
                controller: "SampleOrderTrackingInformationController",
                inputs: {
                    orderItem: orderItem
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    $scope.trackingUserName = authUserName;
                    angular.forEach(result, function(value, key) {
                        $scope.orderItem[key] = value;
                    });
                });
            });
        };
    }]);
