app.controller('VendorController', ['$scope', '$http', 'ModalService', '$filter', 'modalHelper', '$parse', 'formHelper', 'Upload', 'validationErrorsHandler',
    function ($scope, $http, ModalService, $filter, modalHelper, $parse, formHelper, Upload, validationErrorsHandler) {

    $scope.vendor = {};
    $scope.vendor.state_ids = [];
    $http({
        method: 'GET',
        url: '/admin/api/vendor/' + vendor.id
    }).then(function (res) {
        $scope.vendor = res.data;
        $scope.hideLoader = true;
    }, function errorCallback(response) {
        validationErrorsHandler.errorsHandling(response, 'Error on server');
    });


    $scope.states = [];
    $http({
        method: 'GET',
        url: '/admin/api/state'
    }).then(function (res) {
        $scope.states = res.data;
    }, function errorCallback(response) {
        validationErrorsHandler.errorsHandling(response, 'Error on server');
    });

    $scope.countries = [];
    $http({
        method: 'GET',
        url: '/admin/api/country'
    }).then(function (res) {
        $scope.countries = res.data;
    }, function errorCallback(response) {
        validationErrorsHandler.errorsHandling(response, 'Error on server');
    });

    $scope.paymentInformationTemplates = [];
    $http({
        method: 'GET',
        url: '/admin/api/payment-information-template'
    }).then(function (res) {
        $scope.paymentInformationTemplates = res.data;
    }, function errorCallback(response) {
        validationErrorsHandler.errorsHandling(response, 'Error on server');
    });

    $scope.vendors = [];
    $http({
        method: 'GET',
        url: '/admin/api/vendors-list'
    }).then(function (res) {
        $scope.vendors = res.data;
    }, function errorCallback(response) {
        validationErrorsHandler.errorsHandling(response, 'Error on server');
    });


    $scope.saveVendor = function () {
        $http({
            method: 'PUT',
            url: '/admin/api/vendor/' + vendor.id,
            data: $scope.vendor
        }).then(function (res) {
            formHelper.reset($scope.vendorForm);
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
            formHelper.setErrors($scope.vendorForm, res.data);
        });
    };

}]);
