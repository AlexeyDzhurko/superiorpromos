app.controller('AddressesListController', ['$scope', '$http', 'ModalService', function ($scope, $http, ModalService) {

    $scope.user = user;

    $scope.showEditAddressForm = function (address) {
        ModalService.showModal({
            templateUrl: "/admin-assets/views/customers/partials/edit-address-form.html",
            controller: "AddressEditController",
            inputs: {
                address: address,
                user: $scope.user
            }
        }).then(function(modal) {
            modal.element.modal();
        });
    };

    $scope.showAddAddressForm = function (address) {
        ModalService.showModal({
            templateUrl: "/admin-assets/views/customers/partials/add-address-form.html",
            controller: "AddressEditController",
            inputs: {
                address: address,
                user: $scope.user
            }
        }).then(function(modal) {
            modal.element.modal();
        });
    };
}]);