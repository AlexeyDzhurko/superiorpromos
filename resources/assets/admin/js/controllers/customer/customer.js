app.controller('CustomerController', ['$scope', '$http', '$sce', 'Upload', 'validationErrorsHandler',
    function ($scope, $http, $sce, Upload, validationErrorsHandler) {

    $scope.trustAsHtml = $sce.trustAsHtml;

    $scope.updateCustomer = function () {
        $scope.user.certificate = $scope.certificate;
        $scope.user._method = 'put';

        Upload.upload({
            url: '/admin/api/customer/' + $scope.user.id,
            data: $scope.user
        }).then(function (res) {
            $scope.certificate = null;
            $scope.user.certificate = res.data;
            toastr.success('Successfully updated customer');
        }, function (res) {
            validationErrorsHandler.errorsHandling(res, 'Error occurred while customer updating');
        });
    };
}]);