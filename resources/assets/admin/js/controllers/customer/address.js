app.controller('AddressEditController', ['$scope', 'close', '$http', 'address', 'user', '$sce', '$element', 'validationErrorsHandler',
    function ($scope, close, $http, address, user, $sce, $element, validationErrorsHandler) {

    $scope.trustAsHtml = $sce.trustAsHtml;

    $scope.address = address;
    $scope.user = user;
    
    $scope.updateAddress = function () {
        $http.put('/admin/api/address/' + address.id, $scope.address).
        then(function(res) {
            $element.modal('hide');
            toastr.success('Successfully updated address');
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error occurred while address updating');
        });
    };

    $scope.addAddress = function () {
        address.user_id = $scope.user.id;
        $http.post('/admin/api/address/', $scope.address).
        then(function(res) {
            $element.modal('hide');
            toastr.success('Successfully added address');
            $scope.user.addresses.push(address);
        }, function errorCallback(response) {
            //formHelper.setErrors($scope.discountForm, response.data);
            validationErrorsHandler.errorsHandling(response, 'Error occurred while address adding');
        });
    };
}]);