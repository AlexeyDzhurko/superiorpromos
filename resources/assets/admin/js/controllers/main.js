app.controller('MainController', ['$scope', 'ModalService', 'Upload', '$http', 'dictionary', 'validationErrorsHandler', '$sce',
    function ($scope, ModalService, Upload, $http, dictionary, validationErrorsHandler, $sce) {

        $scope.trustAsHtml = $sce.trustAsHtml;
        $scope.orderItem = orderItem;
        $scope.trackingUserName = trackingUserName;

        $scope.vendors = [];

        $http({
            method: 'GET',
            url: '/admin/api/vendors-list/'
        }).then(function (res) {
            $scope.vendors = res.data;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });

        var getBackNotes = function () {
            $http({
                method: 'GET',
                url: '/admin/api/order_item/' + orderItem.id + '/back_note'
            }).then(function (res) {
                $scope.backNotes = res.data;
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        getBackNotes();

        var getArtProofs = function () {
            $http({
                method: 'GET',
                url: '/admin/api/order_item/' + orderItem.id + '/art_proof'
            }).then(function (res) {
                $scope.artProofs = res.data;
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        getArtProofs();

        $scope.updateCheckNotes = function () {
            $http({
                method: 'PATCH',
                url: '/admin/api/order_item/' + orderItem.id,
                data: {
                    check_notes: $scope.orderItem.check_notes
                }
            }).then(function (res) {
                //
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.getPrettyShippingCompany = function () {
            return dictionary.shippingCompanies()[orderItem.tracking_shipping_company] || 'empty';
        };

        $scope.showNotesForm = function () {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/orders/partials/notes.html",
                controller: "NotesController",
                inputs: {
                    orderItem: orderItem
                }
            }).then(function (modal) {
                modal.element.modal();
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.showUserNotesForm = function () {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/orders/partials/user-notes.html",
                controller: "UserNotesController",
                inputs: {
                    orderItem: orderItem
                }
            }).then(function (modal) {
                modal.element.modal();
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.showArtProofsForm = function () {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/orders/partials/art-proofs.html",
                controller: "ArtProofsController",
                inputs: {
                    orderItem: orderItem
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };

        $scope.showExtraBillingForm = function () {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/orders/partials/extra-billing.html",
                controller: "ExtraBillingController",
                inputs: {
                    orderItem: orderItem
                }
            }).then(function (modal) {
                modal.element.modal();
            });
        };


        $scope.showInvoiceForm = function () {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/orders/partials/invoice.html",
                controller: "InvoiceController",
                inputs: {
                    orderItem: orderItem
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (fileName) {
                    $scope.orderItem.invoice_file = fileName;
                });
            });
        };

        $scope.updatePurchaseOrderNumber = function (data) {
            $http({
                method: 'PATCH',
                url: '/admin/api/order_item/' + orderItem.id,
                data: {
                    po_number: data
                }
            }).then(function (res) {
                //
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.updateVendor = function (data) {
            $http({
                method: 'PATCH',
                url: '/admin/api/order_item/' + orderItem.id,
                data: {
                    vendor_id: data
                }
            }).then(function (res) {
                //
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.checkOrderInProcess = function (status) {
            // return status !== 'Complete'
            //     && status !== 'Canceled';
            return true;
        }

        $scope.canRemoveOrderItem = function (status) {
            return status === 'New Order Received - Processing'
                || status === 'New Sample Request'
                || status === ''
                || !status;
        }

        $scope.showOrderStages = function () {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/orders/partials/stage.html",
                controller: "StagesController",
                inputs: {
                    orderItem: orderItem
                }
            }).then(function (modal) {
                // The modal object has the element built, if this is a bootstrap modal
                // you can call 'modal' to show it, if it's a custom modal just show or hide
                // it as you need to.
                modal.element.modal();
                modal.close.then(function (result) {
                    $scope.message = result ? "You said Yes" : "You said No";
                });
            });
        };

        $scope.showTrackingInformationForm = function () {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/orders/partials/tracking-information-form.html",
                controller: "TrackingInformationController",
                inputs: {
                    orderItem: orderItem
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    $scope.trackingUserName = authUserName;
                    angular.forEach(result, function (value, key) {
                        $scope.orderItem[key] = value;
                    });
                });
            });
        };

        $scope.showShippingAddressForm = function () {
            showAddressForm("Shipping", "shipping")
        };

        $scope.showBillingAddressForm = function () {
            showAddressForm("Billing", "billing")
        };

        var showAddressForm = function (address, prefix) {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/orders/partials/address-form.html",
                controller: "AddressController",
                inputs: {
                    address: address,
                    prefix: prefix,
                    orderItem: orderItem
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    angular.forEach(result, function (value, key) {
                        $scope.orderItem[key] = value;
                    });
                });
            });
        };

        $scope.removeOrderItem = function () {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/modals/delete.html",
                controller: function ($scope, close) {
                    $scope.title = 'Order Item';
                    $scope.delete = function () {
                        $http({
                            method: 'DELETE',
                            url: '/admin/api/order-item/' + orderItem.id
                        }).then(function (res) {
                            window.location = '/admin/order';
                        }, function errorCallback(response) {
                            validationErrorsHandler.errorsHandling(response, 'Error on server');
                        })
                    }
                }
            }).then(function(modal) {
                modal.element.modal();
            });
        };

        $scope.hideLoader = true;

    }]);
