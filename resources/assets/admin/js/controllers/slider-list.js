app.controller('SliderListController', ['$scope', '$http', 'ModalService', 'Upload', 'validationErrorsHandler', 'modalHelper',
    function ($scope, $http, ModalService, Upload, validationErrorsHandler, modalHelper) {

    var getSliders = function () {
        $http({
            method: 'GET',
            url: '/admin/api/slider'
        }).then(function (res) {
            $scope.sliders = res.data;
            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    getSliders();


    $scope.saveSlider = function(data, sliderId, slider) {
        var selected = $scope.sliders.find(function(slider) {
            return slider.id == sliderId;
        });

        if (sliderId) {
            Upload.upload({
                url: '/admin/api/slider/' + sliderId,
                data: {
                    image: selected.image_file,
                    url: selected.url,
                    _method: 'PATCH'
                }
            }).then(function (res) {
                getSliders();
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });

        } else {
            Upload.upload({
                url: '/admin/api/slider',
                data: {
                    image: selected.image_file,
                    url: selected.url
                }
            }).then(function (res) {
                getSliders();
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        }
    };

    $scope.addSlider = function () {
        $scope.sliders.push({
            id: null,
            url: null,
            image_src: null
        });
    };

    $scope.deleteSlider = function (sliderId) {
        if(sliderId) {
            modalHelper.delete('/admin/api/slider/' + sliderId, 'Slider')
                .then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(res) {
                        $scope.sliders = $scope.sliders.filter(function (item) {
                            return item.id !== sliderId;
                        });
                    });
                });
        }
    };

    $scope.dragControlListeners = {
        itemMoved: function (event) {
            console.log(event);
        },
        orderChanged: function(event) {

            var current = $scope.sliders[event.dest.index];
            var prev = $scope.sliders[event.dest.index - 1];
            var next = $scope.sliders[event.dest.index + 1];

            if (typeof prev != 'undefined') {
                var position_slider_id = prev.id;
                var type = 'moveAfter';
            } else {
                var position_slider_id = next.id;
                var type = 'moveBefore';
            }

            $http({
                method: 'POST',
                url: '/admin/api/slider/' + current.id + '/sort',
                data: {
                    position_slider_id:  position_slider_id,
                    type: type
                }
            }).then(function (res) {
                getSliders();
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        }
    };


}]);
