app.controller('NotesController', ['$scope', 'close', '$http', 'orderItem', 'Upload', '$element', '$sce', 'validationErrorsHandler',
    function ($scope, close, $http, orderItem, Upload, $element, $sce, validationErrorsHandler) {

    $scope.trustAsHtml = $sce.trustAsHtml;

    $scope.orderItem = orderItem;
    $scope.orderId = `${orderItem.order_id}S${orderItem.id}`;

    var getBackNotes = function() {
        $http({
            method: 'GET',
            url: '/admin/api/order_item/'+orderItem.id+'/back_note'
        }).then(function (res) {
            $scope.backNotes = res.data;
        }, function (response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    var getVirtualCabinetFiles = function () {
        $http({
            method: 'GET',
            url: '/admin/api/order_item/'+orderItem.id+'/virtual_cabinet_file'
        }).then(function (res) {
            $scope.virtualCabinetFiles = res.data;
        }, function (response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    getBackNotes();
    getVirtualCabinetFiles();

    $scope.tinymceOptions = {
        plugins: 'link image code',
        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
    };

    $scope.addNote = function () {
        $http({
            method: 'POST',
            url: '/admin/api/order_item/'+orderItem.id+'/back_note',
            data: {
                note: $scope.note
            }
        }).then(function(res){
            getBackNotes();
            $scope.note = '';
        }, function (response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    $scope.updateCheckNotes = function() {
        $http({
            method: 'PATCH',
            url: '/admin/api/order_item/' + orderItem.id,
            data: {
                check_notes: $scope.orderItem.check_notes
            }
        }).then(function(res){
            //
        }, function (response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    $scope.addVirtualCabinetFile = function () {

        $scope.file.upload = Upload.upload({
            url: '/admin/api/order_item/'+orderItem.id+'/virtual_cabinet_file',
            data: {
                file: $scope.file
            }
        }).then(function (res) {
            getVirtualCabinetFiles();
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

}]);
