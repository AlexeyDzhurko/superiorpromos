/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('ProductIconListController', ['$scope', '$http', 'ModalService', 'modalHelper', 'validationErrorsHandler',
    function ($scope, $http, ModalService, modalHelper, validationErrorsHandler) {

    var getPositions = function () {
        return [
            {id: 1, label: 'Top Right'},
            {id: 2, label: 'Bottom Right'},
            {id: 3, label: 'Top Left'},
            {id: 4, label: 'Bottom Left'}
        ];
    };

    $scope.getListData = function () {

        $http({
            method: 'GET',
            url: '/admin/api/product-icon'
        }).then(function (res) {
            $scope.listData = res.data;

            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    $scope.getListData();

    $scope.deleteItem = function(itemId) {
        modalHelper.delete('/admin/api/product-icon/' + itemId, 'Product Item')
            .then(function(modal) {
                modal.element.modal();
                modal.close.then(function(res) {
                    $scope.listData = $scope.listData.filter(function (item) {
                        return item.id != itemId;
                    });
                    $scope.getListData();
                });
            });
    };

    $scope.getPrettyPosition = function(id) {
        var position = getPositions().find(function (position) {
            return position.id == id;
        });
        return position.label;
    };

    
    $scope.showEditAddForm = function (item) {
        ModalService.showModal({
            templateUrl: "/admin-assets/views/main/partials/product-icon.html",
            appendElement: angular.element('.page-content'),
            controller: function ($scope, close, $http, $element, Upload, validationErrorsHandler) {
                $scope.positions = getPositions();
                $scope.errors = null;

                if (item) {
                    $scope.item = item;
                }

                $scope.saveItem = function () {
                    if (item) {
                        var data = $scope.item;
                        data._method = 'PATCH';
                        Upload.upload({
                            url: '/admin/api/product-icon/' + item.id,
                            data: data
                        }).then(function (res) {
                            $element.modal('hide');
                            close({
                                item: res.data,
                                isNew: false
                            }, 200);
                        }, function errorCallback(response) {
                            $scope.errors = response.data;
                            validationErrorsHandler.errorsHandling(response, 'Error on server');
                        });
                    } else {
                        Upload.upload({
                            url: '/admin/api/product-icon',
                            data: $scope.item
                        }).then(function (res) {
                            $element.modal('hide');
                            close({
                                item: res.data,
                                isNew: true
                            }, 200);
                        }, function errorCallback(response) {
                            $scope.errors = response.data;
                            validationErrorsHandler.errorsHandling(response, 'Error on server');
                        });
                    }
                }


            }
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(data) {
                if (data.isNew) {
                    $scope.listData.push(data.item);
                }
            });
        });
    };


}]);