app.controller('SettingListController', ['$scope', '$http', 'ModalService', 'modalHelper', 'validationErrorsHandler',
    function ($scope, $http, ModalService, modalHelper, validationErrorsHandler) {

    var getListData = function () {
        $http({
            method: 'GET',
            url: '/admin/api/setting'
        }).then(function (res) {
            $scope.listData = res.data;
            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    getListData();

    $scope.saveItem = function(data, itemId)
    {
        $http({
            method: 'PATCH',
            url: '/admin/api/setting/' + itemId,
            data: data
        }).then(function () {

        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };


}]);