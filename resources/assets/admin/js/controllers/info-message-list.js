app.controller('InfoMessagesListController', ['$scope', '$http', 'validationErrorsHandler', function ($scope, $http, validationErrorsHandler) {

    var getListData = function () {
        $http({
            method: 'GET',
            url: '/admin/api/info-message'
        }).then(function (res) {
            $scope.listData = res.data;
            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    getListData();

    $scope.saveItem = function(data, itemId)
    {
        $http({
            method: 'PATCH',
            url: '/admin/api/info-message/' + itemId,
            data: data
        }.then(function () {
           //
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        }));
    };


}]);