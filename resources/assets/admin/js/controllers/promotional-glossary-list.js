/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('PromotionalGlossaryListController', ['$scope', '$http', 'ModalService', 'modalHelper', 'validationErrorsHandler',
    function ($scope, $http, ModalService, modalHelper, validationErrorsHandler) {

    var getListData = function () {
        $http({
            method: 'GET',
            url: '/admin/api/promotional-glossary'
        }).then(function (res) {
            $scope.listData = res.data;
            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    getListData();

    $scope.deleteItem = function(itemId) {
        modalHelper.delete('/admin/api/promotional-glossary/' + itemId, 'Promotional Glossary')
            .then(function(modal) {
                modal.element.modal();
                modal.close.then(function(res) {
                    $scope.listData = $scope.listData.filter(function (item) {
                        return item.id != itemId;
                    });
                });
            });
    };


    $scope.dragControlListeners = {
        itemMoved: function (event) {
            console.log(event);
        },
        orderChanged: function(event) {
            var current = $scope.listData[event.dest.index];
            var prev = $scope.listData[event.dest.index - 1];
            var next = $scope.listData[event.dest.index + 1];

            if (typeof prev != 'undefined') {
                var position_item_id = prev.id;
                var type = 'moveAfter';
            } else {
                var position_item_id = next.id;
                var type = 'moveBefore';
            }

            $http({
                method: 'POST',
                url: '/admin/api/promotional-glossary/' + current.id + '/sort',
                data: {
                    position_promotional_glossary_id:  position_item_id,
                    type: type
                }
            }).then(function (res) {
                getListData();
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        }
    };

    $scope.showEditAddForm = function (item) {
        ModalService.showModal({
            templateUrl: "/admin-assets/views/main/partials/promotional-glossary.html",
            controller: function ($scope, close, $http, $element, validationErrorsHandler) {
                $scope.errors = null;
                $scope.item = {active: 0};

                if (item) {
                    $scope.item = item;
                }

                $scope.tinymceOptions = {
                    plugins: 'link image code',
                    toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                };

                $scope.saveItem = function () {
                    if (item) {
                        $http({
                            method: 'PUT',
                            url: '/admin/api/promotional-glossary/' + item.id,
                            data: $scope.item
                        }).then(function (res) {
                            $element.modal('hide');
                            close({
                                item: res.data,
                                isNew: false
                            }, 200);
                        }, function errorCallback(response) {
                            $scope.errors = response.data;
                            validationErrorsHandler.errorsHandling(response, 'Error on server');
                        });
                    } else {
                        $http({
                            method: 'POST',
                            url: '/admin/api/promotional-glossary',
                            data: $scope.item
                        }).then(function (res) {
                            $element.modal('hide');
                            close({
                                item: res.data,
                                isNew: true
                            }, 200);
                        }, function errorCallback(response) {
                            $scope.errors = response.data;
                            validationErrorsHandler.errorsHandling(response, 'Error on server');
                        });
                    }
                }
            }
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(data) {
                if (data.isNew) {
                    $scope.listData.push(data.item);
                }
            });
        });
    };


}]);