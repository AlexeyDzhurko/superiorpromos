/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('TestimonialsListController', ['$scope', '$http', 'ModalService', 'modalHelper', 'validationErrorsHandler',
    function ($scope, $http, ModalService, modalHelper, validationErrorsHandler) {

        $scope.getListData = function () {
            $http({
                method: 'GET',
                url: '/admin/api/testimonials'
            }).then(function (res) {
                $scope.listData = res.data.data;
                $scope.hideLoader = true;
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error occurred while get testimonials list');
            });
        };

        $scope.getListData();

        $scope.pagination = {
            currentPage: 1,
            totalItems: 0,
            maxSize: 10,
            lastPage: 1
        };

        $scope.searchData = {};

        $scope.deleteItem = function(itemId) {
            modalHelper.delete('/admin/api/testimonials/' + itemId, 'Testimonial')
                .then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(res) {
                        $scope.listData = $scope.listData.filter(function (item) {
                            return item.id != itemId;
                        });
                    });
                }, function (res) {
                    validationErrorsHandler.errorsHandling(res, 'Error on server');
                });
        };

        $scope.showEditAddForm = function (item) {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/main/partials/testimonial.html",
                inputs: {
                    items: $scope.listData
                },
                controller: function ($scope, close, $http, $element, Upload, items) {

                    $scope.item = {active: 0};
                    $scope.image = false;
                    $scope.errors = {};

                    if(items) {
                        $scope.items = items;
                    }

                    if (item) {
                        $scope.item = item;
                    }

                    $scope.tinymceOptions = {
                        plugins: 'link image code',
                        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                    };


                    $scope.saveItem = function () {
                        if($scope.image) {
                            $scope.item.image = $scope.image;
                        } else {
                            delete $scope.item.image;
                        }
                        if (item) {
                            $scope.item._method = 'put';
                            Upload.upload({
                                url: '/admin/api/testimonials/' + item.id,
                                data: $scope.item
                            }).then(function (res) {
                                $element.modal('hide');
                                close({
                                    item: res.data,
                                    isNew: false
                                }, 200);
                            }, function (res) {
                                validationErrorsHandler.errorsHandling(res, 'Error on server');
                                $scope.errors = res.data;
                            });
                        } else {
                            Upload.upload({
                                method: 'POST',
                                url: '/admin/api/testimonials/',
                                data: $scope.item
                            }).then(function (res) {
                                $element.modal('hide');
                                close({
                                    item: res.data,
                                    isNew: true
                                }, 200);
                            }, function (res) {
                                validationErrorsHandler.errorsHandling(res, 'Error on server');
                                $scope.errors = res.data;
                                console.log("ERRORS", $scope.errors);
                            });
                        }
                    }
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(data) {
                    if (data.isNew) {
                        $scope.listData.push(data.item);
                    }
                });
            });
        };
    }]);