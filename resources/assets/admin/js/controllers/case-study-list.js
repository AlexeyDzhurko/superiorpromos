/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('CaseStudyListController', ['$scope', '$http', 'ModalService', 'modalHelper', 'validationErrorsHandler',
    function ($scope, $http, ModalService, modalHelper, validationErrorsHandler) {

    $scope.pagination = {
        currentPage: 1,
        totalItems: 0,
        maxSize: 20,
        lastPage: 1
    };

    $scope.statuses = [
        {value: '1', name: 'Active only'},
        {value: '0', name: 'Hidden only'}
    ];

    $scope.getListData = function (page) {

        var params = typeof $scope.searchData != 'undefined' ? $scope.searchData : {};
        if (page) {
            params.page = page;
        }

        $http({
            method: 'GET',
            url: '/admin/api/case-study',
            params: params
        }).then(function (res) {
            $scope.listData = res.data.data;

            $scope.pagination.totalItems = res.data.meta.pagination.total;
            $scope.pagination.lastPage = res.data.meta.pagination.total_pages;
            $scope.pagination.currentPage = res.data.meta.pagination.current_page;
            $scope.pagination.per_page = res.data.meta.pagination.per_page;

            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error occurred while get case study');
        });
    };

    $scope.getListData(1);

    $scope.search = function () {
        $scope.getListData(1);
    };

    $scope.deleteItem = function(itemId) {
        modalHelper.delete('/admin/api/case-study/' + itemId, 'Case Study')
            .then(function(modal) {
                modal.element.modal();
                modal.close.then(function(res) {
                    $scope.listData = $scope.listData.filter(function (item) {
                        return item.id != itemId;
                    });
                });
            });
    };

    $scope.dragControlListeners = {
        itemMoved: function (event) {
            console.log(event);
        },
        orderChanged: function(event) {
            $scope.hideLoader = false;

            var current = $scope.listData[event.dest.index];
            var prev = $scope.listData[event.dest.index - 1];
            var next = $scope.listData[event.dest.index + 1];

            if (typeof prev != 'undefined') {
                var position_item_id = prev.id;
                var type = 'moveAfter';
            } else {
                var position_item_id = next.id;
                var type = 'moveBefore';
            }

            $http({
                method: 'POST',
                url: '/admin/api/case-study/' + current.id + '/sort',
                data: {
                    position_case_study_id:  position_item_id,
                    type: type
                }
            }).then(function (res) {
                $scope.hideLoader = true;
            }, function (res) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
                $scope.hideLoader = true;
                $scope.getListData($scope.pagination.currentPage);
            });
        }
    };

    $scope.showEditAddForm = function (item) {
        ModalService.showModal({
            templateUrl: "/admin-assets/views/main/partials/case-study.html",
            controller: function ($scope, close, $http, $element, validationErrorsHandler) {
                $scope.item = {active: 0};
                $scope.errors = {};

                if (item) {
                    $http({
                        method: 'GET',
                        url: '/admin/api/case-study/' + item.id
                    }).then(function (res) {
                        $scope.item = res.data;
                    }, function errorCallback(response) {
                        validationErrorsHandler.errorsHandling(response, 'Error occurred while get case study object');
                    });

                    $scope.item = item;
                }

                $scope.tinymceOptions = {
                    plugins: 'link image code',
                    toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                };

                $scope.saveItem = function () {
                    if (item) {
                        $http({
                            method: 'PUT',
                            url: '/admin/api/case-study/' + item.id,
                            data: $scope.item
                        }).then(function (res) {
                            $element.modal('hide');
                            close({
                                item: res.data,
                                isNew: false
                            }, 200);
                        }, function errorCallback(response) {
                            $scope.errors = response.data;
                            validationErrorsHandler.errorsHandling(response, 'Error occurred while update case study object');
                        });
                    } else {
                        $http({
                            method: 'POST',
                            url: '/admin/api/case-study',
                            data: $scope.item
                        }).then(function (res) {
                            $element.modal('hide');
                            close({
                                item: res.data,
                                isNew: true
                            }, 200);
                        }, function errorCallback(response) {
                            $scope.errors = response.data;
                            validationErrorsHandler.errorsHandling(response, 'Error occurred while create case study object');
                        });
                    }
                }
            }
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(data) {
                if (data.isNew) {
                    $scope.listData.push(data.item);
                }
            });
        });
    };

}]);