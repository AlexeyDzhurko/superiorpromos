/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('SurveyListController', ['$scope', '$http', 'ModalService', 'modalHelper', 'validationErrorsHandler',
    function ($scope, $http, ModalService, modalHelper, validationErrorsHandler) {

    $scope.getListData = function () {

        $http({
            method: 'GET',
            url: '/admin/api/survey'
        }).then(function (res) {
            $scope.listData = res.data;

            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    $scope.getListData();

    $scope.deleteItem = function(itemId) {
        modalHelper.delete('/admin/api/survey/' + itemId, 'Survey')
            .then(function(modal) {
                modal.element.modal();
                modal.close.then(function(res) {
                    $scope.listData = $scope.listData.filter(function (item) {
                        return item.id != itemId;
                    });
                    $scope.getListData();
                });
            });
    };

    
    $scope.showEditAddForm = function (item) {
        ModalService.showModal({
            templateUrl: "/admin-assets/views/main/partials/survey.html",
            appendElement: angular.element('.page-content'),
            controller: function ($scope, close, $http, $element, validationErrorsHandler) {
                $scope.errors = null;

                $scope.types = [
                    {id: 'single', name: 'Single answer'},
                    {id: 'multiple', name: 'Multiple answers'},
                    {id: 'custom', name: 'Custom answer'}
                ];

                if (item) {
                    $scope.item = item;
                } else {
                    $scope.item = {
                        survey_questions: []

                    };
                }

                $scope.addAnswer = function (question) {
                    question.survey_answers.push({
                        id: null,
                        name: null
                    })
                };

                $scope.addQuestion = function () {
                    $scope.item.survey_questions.push({
                        id: null,
                        name: null,
                        survey_answers: [],
                    })
                };

                $scope.removeAnswer = function (question, answer) {
                    if (answer.id) {
                        $http({
                            method: 'DELETE',
                            url: '/admin/api/survey/' + item.id + '/survey-question/' + question.id + '/survey-answer/' + answer.id
                        }).then(function () {
                            //
                        }, function errorCallback(response) {
                            validationErrorsHandler.errorsHandling(response, 'Error on server');
                        });
                    }

                    question.survey_answers = question.survey_answers.filter(function(item) {
                        return item != answer
                    });
                };

                $scope.removeQuestion = function (question) {
                    if (question.id) {
                        $http({
                            method: 'DELETE',
                            url: '/admin/api/survey/' + item.id + '/survey-question/' + question.id
                        }).then(function () {
                            //
                        }, function errorCallback(response) {
                            validationErrorsHandler.errorsHandling(response, 'Error on server');
                        });
                    }

                    $scope.item.survey_questions = $scope.item.survey_questions.filter(function(item) {
                        return item != question
                    });
                };

                $scope.saveItem = function () {
                    $scope.errors = null;
                    
                    if (item) {
                        $http({
                            method: 'PUT',
                            url: '/admin/api/survey/' + item.id,
                            data: $scope.item
                        }).then(function (res) {
                            $element.modal('hide');
                            close({
                                item: res.data,
                                isNew: false
                            }, 200);
                        }, function errorCallback(response) {
                            $scope.errors = response.data;
                            validationErrorsHandler.errorsHandling(response, 'Error on server');
                        });
                    } else {
                        $http({
                            method: 'POST',
                            url: '/admin/api/survey/',
                            data: $scope.item
                        }).then(function (res) {
                            $element.modal('hide');
                            close({
                                item: res.data,
                                isNew: true
                            }, 200);
                        }, function errorCallback(response) {
                            $scope.errors = response.data;
                            validationErrorsHandler.errorsHandling(response, 'Error on server');
                        });
                    }
                }
            }
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(data) {
                if (data.isNew) {
                    $scope.listData.push(data.item);
                }
            });
        });
    };


}]);