/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('PromosController', ['$scope', '$http', 'ModalService', 'modalHelper', 'validationErrorsHandler', 'Upload',
  function($scope, $http, ModalService, modalHelper, validationErrorsHandler, Upload){
    // TODO
    $scope.getListData = function () {
      $http({
          method: 'GET',
          url: '/admin/api/promos'
      }).then(function (res) {
          $scope.listData = res.data;
          $scope.hideLoader = true;
      }, function errorCallback(response) {
          validationErrorsHandler.errorsHandling(response, 'Error occurred while get promos list');
      });
    };
    $scope.getListData();

    $scope.showEditAddForm = function (item) {
        ModalService.showModal({
            templateUrl: "/admin-assets/views/main/partials/promo-category.html",
            controller: function ($scope, close, $http, $element) {

                $scope.item = {active: 0};

                if (item) {
                    $scope.item = item;
                }

                $scope.tinymceOptions = {
                    plugins: 'link image code',
                    toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                };

                $scope.saveItem = function () {
                    let requestItem = {};
                    requestItem.url = $scope.item.url;
                    requestItem.image_src = $scope.item.path;

                    $http({
                        method: 'PATCH',
                        url: '/admin/api/promos/' + item.id,
                        data: requestItem
                    }).then(function (res) {
                        $element.modal('hide');
                        close({
                            item: res.data
                        }, 200);
                    }, function errorCallback(response) {
                        validationErrorsHandler.errorsHandling(response, 'Error occurred while update block');
                    });
                }

                $scope.isAllImagesError;
                $scope.uploadedImage;
                $scope.addFile = function(files) {
                    debugger;
                    $scope.isAllImagesError = false;
                    $scope.uploadedImage = {};

                    if(files[0]){
                        $scope.uploadedImage.image = files[0];
                    }
                    
                    let isAllImages = Array.prototype.slice.call(files).every(e => (e.type === 'image/png') || e.type === 'image/jpeg');
                    if (!isAllImages) {
                        $scope.isAllImagesError = true;
                        return;
                    }

                    Upload.upload({
                        method: 'POST',
                        url: '/admin/api/promos/file/upload',
                        data: $scope.uploadedImage
                    }).then(function (res) {
                        toastr.success('Successfully created category');
                        console.log(res);
                        $scope.item.image_src = res.data.image_src;
                        $scope.item.path = res.data.path;
                    }, function (res) {
                        validationErrorsHandler.errorsHandling(res, 'Error on server');
                    });
                }
            }
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(data) {

            });
        });
    };

    // $scope.deletePreview = function(promoImagePath) {
    //     modalHelper.deleteImage('/admin/api/promos/file/remove', 'promo preview', promoImagePath)
    //         .then(function(modal) {
    //             modal.element.modal();
    //             modal.close.then(function(res) {
    //                 $scope.listData = $scope.listData.filter(function (item) {
    //                     return item.id != itemId;
    //                 });
    //             });
    //         });
    // };

  }
]);