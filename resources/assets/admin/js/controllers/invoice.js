app.controller('InvoiceController', ['$scope', 'close', '$http', 'orderItem', 'Upload', '$element', 'validationErrorsHandler',
    function ($scope, close, $http, orderItem, Upload, $element, validationErrorsHandler) {

    $scope.orderItem = orderItem;
    $scope.fileName = null;
    $scope.file = null;

    $scope.invoice_notify_customer = 0;

    $scope.updateInvoiceFile = function () {

        if (!$scope.file) {
            $element.modal('hide');
            close(null, 200);
        }

        $scope.file.upload = Upload.upload({
            url: '/admin/api/order_item/' + orderItem.id,
            data: {
                _method: 'PATCH',
                invoice_file: $scope.file,
                invoice_notify_customer: $scope.invoice_notify_customer
            }
        }).then(function (res) {
            $scope.fileName = $scope.file.name;
            $element.modal('hide');
            close($scope.fileName, 200);
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    $scope.deleteInvoiceFile = function () {
        $http({
            method: 'DELETE',
            url: '/admin/api/order_item/' + orderItem.id + '/invoice_file'
        }).then(function (res) {
            $scope.orderItem.invoice_file = null;
            $scope.fileName = null;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };


    $scope.dismissModal = function(result) {
        close(result, 200); // close, but give 200ms for bootstrap to animate
    };

}]);