/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
/* eslint-disable angular/controller-as */
app.controller('CategoryController', ['$scope', '$http', '$interpolate', 'formHelper', 'modalHelper', '$window', 'validationErrorsHandler', 'Upload',
    function ($scope, $http, $interpolate, formHelper, modalHelper, $window, validationErrorsHandler, Upload) {

    $scope.categories = {};
    $scope.category = {};
    $scope.image = null;
    $scope.displayCategory = false;
    $scope.popularCategories = [];
    $scope.errors = {};

    $http({
        method: 'GET',
        url: '/api/categories/popular'
    }).then(function(res){
        $scope.popularCategories = res.data;
    });
    
    $scope.hideCategory = function () {
        $scope.displayCategory = false;
    };

    $scope.editCategory = function (category) {
        $scope.uploadedImageFile = null;
        $scope.category = category;
        $scope.displayCategory = true;
    };

    $scope.categoriesList = [];

    $scope.getCategoriesList = function () {
        $http({
            method: 'GET',
            url: '/admin/api/category'
        }).then(function (res) {
            $scope.categoriesList = res.data;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    $scope.getCategoriesList();

    $scope.getCategoryById = function (categoryId) {
        return $scope.categoriesList.find(function(category) {
            return category.id == categoryId;
        });
    };

    if (typeof categories != 'undefined') {
        $scope.categories = categories;
    }
    
    $scope.saveHierarchy = function () {
        $scope.hideLoader = false;
        $http({
            method: 'POST',
            url: '/admin/api/category/save-hierarchy',
            data: $scope.categories
        }).then(function (res) {
            toastr.success('Successfully updated categories hierarchy');
            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    $scope.uploadedImageFile;
    $scope.upload = function(file) {
        $scope.uploadedImageFile = file;
    }
        
    $scope.save = function () {
        $scope.category.parent = $scope.category.parent_id;

        if(($scope.category.is_quick_link || $scope.category.is_popular) && !$scope.category.image){
            validationErrorsHandler.errorsHandling('', 'Image required');
            return;
        }

        if($scope.popularCategories.length >= 10 && $scope.category.is_popular) {
            validationErrorsHandler.errorsHandling('', 'Reached limit of setted Popular Categories: 10. Turn off one of the current Popular Categories to set this one');
            $scope.category.is_popular = 0;
        }

        if(typeof $scope.category.image == 'undefined') {
            delete $scope.category.image;
        }

        if ($scope.category.id != undefined) {
            $scope.category._method = 'put';

            Upload.upload({
                url: '/admin/api/category/' + $scope.category.id,
                data: $scope.category
            }).then(function (res) {
                toastr.success('Successfully updated category');
                $scope.category = res.data;
                $scope.displayCategory = false;
            }, function (res) {
                validationErrorsHandler.errorsHandling(res, 'Error on server');
                $scope.errors = res.data;
            });
        } else {
            Upload.upload({
                method: 'POST',
                url: '/admin/api/category',
                data: $scope.category
            }).then(function (res) {
                toastr.success('Successfully created category');
                $scope.categories.push(res.data);
                $scope.displayCategory = false;
            }, function (res) {
                validationErrorsHandler.errorsHandling(res, 'Error on server');
                $scope.errors = res.data;
            });
        }
    };

    $scope.createNewCategory = function () {
        $scope.category = {};
        $scope.displayCategory = true;
    };
    
    $scope.deleteCategory = function (category) {
        modalHelper.delete('/admin/api/category/' + category.id, 'Category')
            .then(function(modal) {
                modal.element.modal();
                modal.close.then(function(res) {
                    $scope.getCategoriesList();
                    var firstIndex = $scope.categories.indexOf(category);

                    if(firstIndex != (-1)) {
                        $scope.categories.splice(firstIndex, 1);
                    } else {
                        findItemIndexInArray($scope.categories, category);
                    }

                    if($scope.category.id == category.id) {
                        $scope.category = {};
                        $scope.displayCategory = false;
                    }

                    toastr.success('Successfully deleted category');
                }, function errorCallback(response) {
                    validationErrorsHandler.errorsHandling(response, 'Error on server');
                });
            });
    };

    function findItemIndexInArray(array, category) {
        var index;

        for (var i=0; i<array.length;i++) {
            if(array[i].children.length > 0) {
                index = array[i].children.indexOf(category);
                if(index != (-1)) array[i].children.splice(index, 1);
                findItemIndexInArray(array[i].children, category);
            }
        }
    }
    
    /*$scope.remove = function (scope) {
        scope.remove();
    };*/

    $scope.toggle = function (scope) {
        scope.toggle();
    };

    $scope.collapseAll = function () {
        $scope.$broadcast('angular-ui-tree:collapse-all');
    };

    $scope.collapseAll();

    $scope.expandAll = function () {
        $scope.$broadcast('angular-ui-tree:expand-all');
    };

    $scope.hideLoader = true;
}]);
