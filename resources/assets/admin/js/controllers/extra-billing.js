app.controller('ExtraBillingController', ['$scope', 'close', '$http', 'orderItem', 'Upload', '$element', 'validationErrorsHandler',
    function ($scope, close, $http, orderItem, Upload, $element, validationErrorsHandler) {

        $scope.orderItem = orderItem;
        $scope.orderId = `${orderItem.order_id}S${orderItem.id}`;
        $scope.paymentProfiles = [];

        $scope.payments = [];

        $scope.otherBilling = {
            extra_amount: 0,
            credit_amount: 0,
            notify_customer: 0
        };

        $scope.credit = {
            notify_customer: 0
        };


        $scope.otherTotal = function () {
            return parseFloat($scope.otherBilling.extra_amount) - parseFloat($scope.otherBilling.credit_amount);
        };

        $scope.addApplyCredit = function () {
            console.log($scope.credit);
            $http({
                method: 'POST',
                url: '/admin/api/order_item/' + orderItem.id + '/extra_payment/credit',
                data: $scope.credit
            }).then(function (res) {
                $scope.getCustomerInvoices();
                $scope.otherBilling = {
                    extra_amount: 0,
                    credit_amount: 0,
                    notify_customer: 0
                };
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.addExtraOtherBilling = function () {
            $http({
                method: 'POST',
                url: '/admin/api/order_item/' + orderItem.id + '/extra_payment/other',
                data: $scope.otherBilling
            }).then(function (res) {
                $scope.otherBilling = {
                    extra_amount: 0,
                    credit_amount: 0,
                    notify_customer: 0
                };
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.updateNotPaid = function () {
            $http({
                method: 'PATCH',
                url: '/admin/api/order_item/' + orderItem.id,
                data: {
                    not_paid: $scope.orderItem.not_paid
                }
            }).then(function (res) {

            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.getCustomerPayment = function () {
            $http({
                method: 'GET',
                url: `/admin/api/order_item/${orderItem.id}/customer_payment`
            }).then(function (res) {
                $scope.paymentProfiles = res.data;
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        };

        $scope.getCustomerInvoices = function () {
            $http({
                method: 'GET',
                url: '/admin/api/order_item/' + orderItem.id + '/payment'
            }).then(function (res) {
                $scope.payments = res.data;
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        }
        $scope.getCustomerPayment();
        $scope.getCustomerInvoices();
    }]);
