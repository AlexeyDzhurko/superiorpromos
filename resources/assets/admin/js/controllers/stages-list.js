/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('StagesListController', ['$scope', '$http', 'ModalService', 'validationErrorsHandler',
    function ($scope, $http, ModalService, validationErrorsHandler) {
    $scope.errors = null;
    $scope.stageNameError = {};
    $scope.stages = [];

    $scope.newStageFormShow = false;
    $scope.newStageButtonShow = true;

    $scope.showNewStageForm = function () {
        $scope.newStageFormShow = true;
        $scope.newStageButtonShow = false;
    };

    $scope.hideNewStageForm = function () {
        $scope.newStageFormShow = false;
        $scope.newStageButtonShow = true;
    };

    $scope.addNewStage = function () {
        $http({
            method: 'POST',
            url: '/admin/api/stage/',
            data: {
                name: $scope.name
            }
        }).then(function (res) {
            getStages();
            $scope.name = '';
            $scope.hideNewStageForm();
        }, function errorCallback(response) {
            $scope.errors = response.data;
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    $scope.updateStage = function(data, stageId) {
        $scope.stageNameError[stageId] = false;

        if(data === "") {
            $scope.stageNameError[stageId] = true;
            return;
        }

        $http({
            method: 'PATCH',
            url: '/admin/api/stage/' + stageId,
            data: {
                name: data
            }
        }).then(function (res) {
            console.log(res);
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    $scope.deleteStage = function(stageId) {
        ModalService.showModal({
            templateUrl: "/admin-assets/views/modals/delete.html",
            controller: function ($scope, close) {
                $scope.title = 'stage';
                $scope.delete = function () {
                    $http({
                        method: 'DELETE',
                        url: '/admin/api/stage/' + stageId
                    }).then(function (res) {
                        if(res.status === 204) {
                            location.reload();
                        }
                    }, function errorCallback(response) {
                        validationErrorsHandler.errorsHandling(response, 'Error on server');
                    })

                }
            }
        }).then(function(modal) {
            modal.element.modal();
        });
    };

    $scope.dragControlListeners = {
        // accept: function (sourceItemHandleScope, destSortableScope) {return true},
        itemMoved: function (event) {
            console.log(event);
        },
        orderChanged: function(event) {

            var current = $scope.stages[event.dest.index];
            var prev = $scope.stages[event.dest.index - 1];
            var next = $scope.stages[event.dest.index + 1];

            if (typeof prev != 'undefined') {
                var position_stage_id = prev.id;
                var type = 'moveAfter';
            } else {
                var position_stage_id = next.id;
                var type = 'moveBefore';
            }

            $http({
                method: 'POST',
                url: '/admin/api/stage/' + current.id + '/sort',
                data: {
                    position_stage_id:  position_stage_id,
                    type: type
                }
            }).then(function (res) {
                getStages();
            }, function errorCallback(response) {
                validationErrorsHandler.errorsHandling(response, 'Error on server');
            });
        }
        // containment: '#board',
        // clone: true,
        // allowDuplicates: false
    };

    var getStages = function () {
        $http({
            method: 'GET',
            url: '/admin/api/stage '
        }).then(function (res) {
            $scope.stages = res.data;
            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    getStages();

}]);