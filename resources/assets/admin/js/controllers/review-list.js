/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('ReviewListController', ['$scope', '$http', 'ModalService', 'modalHelper', 'validationErrorsHandler',
    function ($scope, $http, ModalService, modalHelper, validationErrorsHandler) {

    $scope.pagination = {
        currentPage: 1,
        totalItems: 0,
        maxSize: 10,
        lastPage: 1
    };

    $scope.getListData = function (page) {
        $http({
            method: 'GET',
            url: '/admin/api/review',
            params: {
                page: page
            }
        }).then(function (res) {
            $scope.listData = res.data.data;

            $scope.pagination.totalItems = res.data.meta.pagination.total;
            $scope.pagination.lastPage = res.data.meta.pagination.total_pages;
            $scope.pagination.currentPage = res.data.meta.pagination.current_page;
            $scope.pagination.per_page = res.data.meta.pagination.per_page;

            $scope.hideLoader = true;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    $scope.getListData(1);

    $scope.deleteItem = function(itemId) {
        modalHelper.delete('/admin/api/review/' + itemId, 'Review')
            .then(function(modal) {
                modal.element.modal();
                modal.close.then(function(res) {
                    $scope.listData = $scope.listData.filter(function (item) {
                        return item.id != itemId;
                    });
                    $scope.getListData(1);
                });
            });
    };

    
    $scope.showEditAddForm = function (item) {
        ModalService.showModal({
            templateUrl: "/admin-assets/views/main/partials/review.html",
            controller: function ($scope, close, $http, $element, validationErrorsHandler) {
                $scope.errors = null;
                $scope.item = {active: 0};
                $scope.products = null;

                $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 1
                };

                if (item) {
                    $scope.item = item;
                }

                $scope.searchMedia = function($select) {
                    return $http.get('/admin/api/product', {
                        params: {
                            query: $select.search
                        }
                    }).then(function (res) {
                        $scope.products = res.data
                    }, function errorCallback(response) {
                        validationErrorsHandler.errorsHandling(response, 'Error on server');
                    });
                };

                $scope.tinymceOptions = {
                    plugins: 'link image code',
                    toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                };

                $scope.changeStatus = function (newStatus) {
                    var data = $scope.item;
                    data.approve = newStatus;
                    $http({
                        method: 'PUT',
                        url: '/admin/api/review/' + item.id,
                        data: data
                    }).then(function (res) {
                        $element.modal('hide');
                        close({
                            item: res.data
                        }, 200);
                    }, function errorCallback(response) {
                        $scope.errors = response.data;
                        validationErrorsHandler.errorsHandling(response, 'Error on server');
                    });
                };

                $scope.updateItem = function () {
                    $http({
                        method: 'PUT',
                        url: '/admin/api/review/' + item.id,
                        data: $scope.item
                    }).then(function (res) {
                        $element.modal('hide');
                        close({
                            item: res.data
                        }, 200);
                    }, function errorCallback(response) {
                        $scope.errors = response.data;
                        validationErrorsHandler.errorsHandling(response, 'Error on server');
                    });
                }

                $scope.createItem = function () {
                    $http({
                        method: 'POST',
                        url: '/admin/api/review',
                        data: $scope.item
                    }).then(function (res) {
                        $element.modal('hide');
                        close({
                            item: res.data,
                            isNew: true
                        }, 200);
                    }, function errorCallback(response) {
                        $scope.errors = response.data;
                        validationErrorsHandler.errorsHandling(response, 'Error on server');
                    });
                }
            }
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(data) {
                if (data.isNew) {
                    $scope.listData.push(data.item);
                }
            });
        });
    };


}]);
