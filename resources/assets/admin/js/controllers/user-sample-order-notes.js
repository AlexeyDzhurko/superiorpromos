app.controller('UserSampleOrderNotesController', ['$scope', 'close', '$http', 'orderItem', 'Upload', '$element', '$sce', 'validationErrorsHandler',
    function ($scope, close, $http, orderItem, Upload, $element, $sce, validationErrorsHandler) {

    $scope.trustAsHtml = $sce.trustAsHtml;

    $scope.orderItem = orderItem;
    $scope.approve_required = 0 ;

    var getUserNotes = function() {
        $http({
            method: 'GET',
            url: '/admin/api/order_sample_item/'+orderItem.id+'/note'
        }).then(function (res) {
            $scope.userNotes = res.data;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    getUserNotes();

    $scope.tinymceOptions = {
        plugins: 'link image code',
        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
    };

    $scope.addUserNote = function () {
        $http({
            method: 'POST',
            url: '/admin/api/order_sample_item/'+orderItem.id+'/note',
            data: {
                subject: $scope.subject,
                note: $scope.note,
                approve_required: $scope.approve_required
            }
        }).then(function(res){
            getUserNotes();
            $scope.note = '';
            $scope.subject = '';
            $scope.approve_required = 0;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };


}]);