app.controller('SampleOrdersStagesController', ['$scope', 'close', '$http', 'orderItem', 'validationErrorsHandler',
    function ($scope, close, $http, orderItem, validationErrorsHandler) {

    $scope.stages = [];
    $scope.stage = 0;
    $scope.orderItemId = orderItem.id;
    $scope.notify_customer = 0;

    $http({
        method: 'GET',
        url: '/admin/api/order_sample_item/' + $scope.orderItemId
    }).then(function(res){
        $scope.orderItem = res.data;
        $scope.autoRemind = $scope.orderItem.auto_remind == 1;
    }, function errorCallback(response) {
        validationErrorsHandler.errorsHandling(response, 'Error on server');
    });

    $http({
        method: 'GET',
        url: '/admin/api/stage'
    }).then(function(res){
        $scope.stages = res.data;
        $scope.stage = res.data[0];
    }, function errorCallback(response) {
        validationErrorsHandler.errorsHandling(response, 'Error on server');
    });

    var updateHistory = function() {
        $http({
            method: 'GET',
            url: '/admin/api/order_sample_item/' + $scope.orderItemId + '/stage'
        }).then(function(res){
            $scope.history = res.data;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    updateHistory();

    $scope.dismissModal = function(result) {
        close(result, 200); // close, but give 200ms for bootstrap to animate
    };


    $scope.setNewStage = function() {
        $http({
            method: 'POST',
            url: '/admin/api/order_sample_item/' + $scope.orderItemId + '/stage',
            data: {
                stage_id: $scope.stage.id,
                notify_customer: $scope.notify_customer
            }
        }).then(function (res) {
            updateHistory();
            $scope.stage = 0;
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };

    $scope.updateAutoRemind = function() {
        $http({
            method: 'PATCH',
            url: '/admin/api/order_sample_item/' + $scope.orderItemId,
            data: {
                auto_remind: $scope.autoRemind
            }
        }).then(function () {
            //
        }, function errorCallback(response) {
            validationErrorsHandler.errorsHandling(response, 'Error on server');
        });
    };


}]);
