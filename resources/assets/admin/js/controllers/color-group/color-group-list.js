/* eslint-disable angular/controller-as */
/* eslint-disable angular/function-type */
/* eslint-disable angular/file-name */
/* eslint-disable angular/module-getter */
app.controller('ColorGroupsListController', ['$scope', '$http', 'ModalService', 'modalHelper', 'validationErrorsHandler',
    function ($scope, $http, ModalService, modalHelper, validationErrorsHandler) {

        $scope.getListData = function (page) {
            $scope.hideLoader = false;

            if (page) {
                params.page = page;
            } else {
                params = {};
            }

            $http({
                method: 'GET',
                url: '/admin/api/color-groups',
                params: params
            }).then(function (res) {
                $scope.listData = res.data.data;
                $scope.pagination.totalItems = res.data.total;
                $scope.pagination.lastPage = res.data.last_page;
                $scope.pagination.currentPage = res.data.current_page;
                $scope.pagination.per_page = res.data.per_page;
                $scope.hideLoader = true;
            }, function errorCallback(response) {
                $scope.hideLoader = true;
                validationErrorsHandler.errorsHandling(response, 'Error occurred while get color groups list');
            });
        };

        $scope.getListData();

        $scope.pagination = {
            currentPage: 1,
            totalItems: 0,
            maxSize: 10,
            lastPage: 1
        };

        $scope.searchData = {};

        $scope.deleteItem = function(itemId) {
            modalHelper.delete('/admin/api/color-groups/' + itemId, 'Color group')
                .then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(res) {
                        toastr.success("Successfully deleted color group");
                        $scope.listData = $scope.listData.filter(function (item) {
                            return item.id != itemId;
                        });
                    });
                }, function (res) {
                    validationErrorsHandler.errorsHandling(res, 'Error on server');
                });
        };

        $scope.showEditAddForm = function (item) {
            ModalService.showModal({
                templateUrl: "/admin-assets/views/main/partials/color-groups.html",
                controller: function ($scope, close, $http, $element) {
                    $scope.errors = null;
                    $scope.colors = [];

                    $http({
                        method: 'GET',
                        url: '/admin/api/all-colors/'
                    }).then(function(res) {
                        $scope.colors = res.data;
                    }, function errorCallback(response) {
                        validationErrorsHandler.errorsHandling(response, 'Error on server');
                    });

                    if (item) {
                        $scope.item = item;
                        $scope.item.colors.selected = item.colors;
                    } else {
                        $scope.item = {
                            name: "",
                            colors: []
                        };
                    }

                    $scope.saveItem = function () {
                        $scope.item.colors = $scope.item.colors.selected.map(function(obj) { return obj.id; });

                        if (item) {
                            $http({
                                method: 'PUT',
                                url: '/admin/api/color-groups/' + item.id,
                                data: $scope.item
                            }).then(function (res) {
                                $scope.item.colors = res.data.colors;
                                $element.modal('hide');
                                close({
                                    item: res.data,
                                    isNew: false
                                }, 200);
                                toastr.success("Successfully updated color group");
                            }, function errorCallback(response) {
                                $scope.errors = response.data;
                                validationErrorsHandler.errorsHandling(response, 'Error on server');
                            });
                        } else {
                            $http({
                                method: 'POST',
                                url: '/admin/api/color-groups',
                                data: $scope.item
                            }).then(function (res) {
                                $element.modal('hide');
                                close({
                                    item: res.data,
                                    isNew: true
                                }, 200);
                                toastr.success("Successfully added color group");
                            }, function errorCallback(response) {
                                $scope.errors = response.data;
                                validationErrorsHandler.errorsHandling(response, 'Error on server');
                            });
                        }
                    }
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(data) {
                    if (data.isNew) {
                        $scope.listData.push(data.item);
                    }
                });
            });
        };
    }]);
