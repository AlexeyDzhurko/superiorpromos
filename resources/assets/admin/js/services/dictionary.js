app.service('dictionary', function() {

    this.shippingCompanies = function () {
        var dictionary = {};
        dictionary['ups'] = 'UPS';
        dictionary['fedex'] ='FedEx';
        dictionary['dhl'] = 'DHL';
        dictionary['usps'] = 'USPS';
        dictionary['truck'] = 'Truck';
        return dictionary;
    };
});
