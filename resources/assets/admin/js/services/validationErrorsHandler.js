app.service('validationErrorsHandler', function() {

    this.errorsHandling = function (response, message) {
        if(response.status == 422) {
            var errmsg = '<ul>';
            angular.forEach(response.data, function(value, key) {
                errmsg += '<li>'+value+'</li>';
            });
            errmsg += '</ul>';
            toastr.error(errmsg);
        } else {
            toastr.error(message);
        }
    };
});
