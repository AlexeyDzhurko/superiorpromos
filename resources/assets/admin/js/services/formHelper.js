app.service('formHelper', function() {

    this.reset = function (form) {
        form.$setPristine();
        form.$setUntouched();
        form.$rollbackViewValue();

        for (var field in form) {
            if (form.hasOwnProperty(field) && field.indexOf('$') != 0) {
                form[field].$setValidity('serverError', true);
                form[field].$error.serverMessage = null;
            }
        }
    };

    this.setErrors = function (form, errorsData) {
        for (var fieldName in errorsData) {
            if (errorsData.hasOwnProperty(fieldName)) {
                form[fieldName].$setValidity('serverError', false);
                form[fieldName].$error.serverMessage = errorsData[fieldName].join(', ');
            }
        }
    }
});
