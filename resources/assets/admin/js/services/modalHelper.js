/* eslint-disable angular/no-service-method */
/* eslint-disable angular/function-type */
/* eslint-disable angular/module-getter */
app.service('modalHelper', function(ModalService, $http) {

    this.delete = function (url, title) {
        return ModalService.showModal({
            templateUrl: "/admin-assets/views/modals/delete.html",
            controller: function ($scope, close, $element) {
                $scope.title = title;
                $scope.delete = function () {
                    $http({
                        method: 'DELETE',
                        url: url
                    }).then(function (res) {
                        $element.modal('hide');
                        close(true, 200);
                    });
                }
            }
        });
    };

    this.deleteImage = function (url, title, path) {
        return ModalService.showModal({
            templateUrl: "/admin-assets/views/modals/delete.html",
            controller: function ($scope, close, $element) {
                $scope.title = title;

                let body = JSON.stringify({ path: path });
                debugger;
                $scope.delete = function () {
                    $http({
                        method: 'POST',
                        url: url,
                        data: body
                    }).then(function (res) {
                        $element.modal('hide');
                        close(true, 200);
                    });
                }
            }
        })
    };
});
