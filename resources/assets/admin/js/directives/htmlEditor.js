app.directive('htmlEditor', function ($parse) {
    return {
        scope:{
            content:"=",
        },
        transclude: true,
        template : '<label for="content" class="control-label">Content</label> '
            + ' <textarea ui-tinymce="tinymceOptions" ng-model="content" class="form-control" rows="14"></textarea>',
        link: function(scope, element, attrs) {


            console.log(123)
            scope.tinymceOptions = {
                plugins: 'image link lists code',
                paste_data_images: true,
                toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | image | link | code',
                file_browser_callback: function(field_name, url, type, win) {
                    console.log('file_browser_callback called!');
                }
            };
        }
    };
});
