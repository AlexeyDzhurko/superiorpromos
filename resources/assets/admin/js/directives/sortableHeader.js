app.directive('sortableHeader', function ($parse) {
    return {
        scope:{
            change:"&",
            field:"@",
            currentSort:"="
        },
        transclude: true,
        template : '<span ng-transclude></span>&nbsp;' +
        '<span class="sort-icon" ng-class="{\'sort-active\': currentSort == field}"><i ng-show="order == \'asc\'" class="fa fa-sort-asc" aria-hidden="true"></i>' +
        '<i ng-show="order == \'desc\'" class="fa fa-sort-desc" aria-hidden="true"></i></span>',
        link: function(scope, element, attrs) {
            scope.order = 'asc';
            element.bind('click', function() {
                scope.currentSort = scope.field;
                scope.order = scope.order == 'asc' ? 'desc' : 'asc' ;

                scope.callback = scope.change();
                scope.callback(scope.field, scope.order);

                scope.$apply();
            });
        }
    };
});