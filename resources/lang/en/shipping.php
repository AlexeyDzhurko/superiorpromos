<?php

return [
    'method' => [
        'ground' => 'Ground',
        '3day' => '3Day',
        '2day' => '2Day',
        'overnight' => 'Overnight',
        'overnight_am' => 'Overnight AM',
        'overnight_saturday_delivery' => 'Overnight Saturday Delivery'
    ],
];
