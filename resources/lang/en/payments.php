<?php

return [
    'status' => [
        'paid' => 'Paid',
        'processed' => 'Processed',
        'not_paid' => 'Not Paid (Negative totals)'
    ],

    'type' => [
        'initial' => 'Initial Payment',
        'overrun' => 'Additional Billing (Shipping/Overrun)',
        'other' => 'Additional Billing (Shipping/Other)',
        'credit' => 'Credit'
    ]
];
