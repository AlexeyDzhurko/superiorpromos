<?php print '<?xml version="1.0" encoding="UTF-8" ?>'; ?>
<AccessRequest xml:lang="en-US">
    <AccessLicenseNumber>{{env('UPS_ACCESS_KEY')}}</AccessLicenseNumber>
    <UserId>{{env('UPS_USER_ID')}}</UserId>
    <Password>{{env('UPS_PASSWORD')}}</Password>
</AccessRequest>

<?php print '<?xml version="1.0" encoding="UTF-8" ?>'; ?>
<TimeInTransitRequest xml:lang="en-US">
    <Request>
        <TransactionReference>
            <CustomerContext>Testing Candidate List</CustomerContext>
            <XpciVersion>1.0002</XpciVersion>
        </TransactionReference>
        <RequestAction>TimeInTransit</RequestAction>
    </Request>
    <TransitFrom>
        <AddressArtifactFormat>
            <CountryCode>{{$senderData['country']}}</CountryCode>
            <PostcodePrimaryLow>{{$senderData['zip']}}</PostcodePrimaryLow>
        </AddressArtifactFormat>
    </TransitFrom>
    <TransitTo>
        <AddressArtifactFormat>
            @if ($raw)
                {{$shippData}}
            @else
                @if(!empty($additional))
                    <PoliticalDivision2>{{$additional['politicalDivision2']}}</PoliticalDivision2>
                    <PoliticalDivision1>{{$additional['politicalDivision1']}}</PoliticalDivision1>
                    <Country>{{$additional['country']}}</Country>
                @endif
                <CountryCode>{{$shippData['country']}}</CountryCode>
                <PostcodePrimaryLow>{{$shippData['zip']}}</PostcodePrimaryLow>
            @endif
        </AddressArtifactFormat>
    </TransitTo>
    <PickupDate>{{date('Ymd', time() + 60 * 60 * 24)}}</PickupDate>
</TimeInTransitRequest>
