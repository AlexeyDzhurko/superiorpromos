<?php print '<?xml version="1.0" encoding="UTF-8" ?>'; ?>
<AccessRequest xml:lang="en-US">
    <AccessLicenseNumber>{{env('UPS_ACCESS_KEY')}}</AccessLicenseNumber>
    <UserId>{{env('UPS_USER_ID')}}</UserId>
    <Password>{{env('UPS_PASSWORD')}}</Password>
</AccessRequest>

<?php print '<?xml version="1.0" encoding="UTF-8" ?>'; ?>
<RatingServiceSelectionRequest xml:lang="en-US">
    <Request>
        <RequestAction>Rate</RequestAction>
        <RequestOption>shop</RequestOption>
    </Request>
    <PickupType>
        <Code>01</Code>
    </PickupType>
    <Shipment>
        <Shipper>
            <Address>
                <PostalCode>{{$senderData['zip']}}</PostalCode>
                <CountryCode>{{$senderData['country']}}</CountryCode>
            </Address>
        </Shipper>
        <ShipTo>
            <Address>
                <PostalCode>{{$shippData['zip']}}</PostalCode>
                <CountryCode>{{$shippData['country']}}</CountryCode>
            </Address>
        </ShipTo>
        @for ($i = 1; $i <= $boxes; $i++)
        <Package>
            <PackagingType>
                <Code>02</Code>
            </PackagingType>
            <PackageWeight>
                <UnitOfMeasurement>
                    <Code>LBS</Code>
                </UnitOfMeasurement>
                <Weight>{{$boxWeight}}</Weight>
            </PackageWeight>
        </Package>
        @endfor
        <ShipmentServiceOptions/>
    </Shipment>
</RatingServiceSelectionRequest>