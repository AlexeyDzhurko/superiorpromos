<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
    <channel>
        <title>Promotional Products and Promotional Items by SuperiorPromos.com</title>
        <link>https://www.superiorpromos.com/</link>
        <description>Promotional Products and promotional items made easy by www.superiorpromos.com. Advertise your logo with the biggest selection of tradeshow giveaways, promotional pens and more. Our safe shopping guarantee includes the lowest prices on custom t-shirts, promotional tote bags, and custom ceramic mugs. Our talented art department can make any logo look its best for your upcoming promotional products event.</description>

        @foreach($products as $product)
            <item>
                <g:id>{{ !empty($product['id']) ? $product['id'] : '' }}</g:id>
                <title>{{ !empty($product['title']) ? htmlspecialchars(strip_tags($product['title'])) : '' }}</title>
                <description>{!! preg_replace("/\r|\n/", '', htmlspecialchars(strip_tags($product['description']))) !!}</description>
                <g:condition>new</g:condition>
                <g:price>{{ !empty($product['product_price']) ? $product['product_price'] : 0 }}</g:price>
                <g:availability>in stock</g:availability>
                <link>{{ !empty($product['full_slug']) ? url($product['full_slug']) : '' }}</link>
                <g:image_link>{{ !empty($product['thumbnail']) ? url($product['thumbnail']) : '' }}</g:image_link>
                <g:mpn/>
                <g:brand>{{ !empty($product['vendor']) ? htmlspecialchars($product['vendor']) : '' }}</g:brand>
                <g:google_product_category/>
                <gender/>
                <age_group/>
                <size/>
                <color>{{ !empty($product['colors']) ? htmlspecialchars($product['colors']) : '' }}</color>
                <material/>
                <pattern/>
                <item_group_id/>
                <tax/>
                <g:shipping_weight>{{ !empty($product['box_weight']) ? $product['box_weight'] : '' }}</g:shipping_weight>
                <sale_price/>
                <sale_price_effective_date/>
                <additional_image_link/>
                <g:product_type>{{ !empty($product['type']) ? htmlspecialchars($product['type']) : '' }}</g:product_type>
                <g:is_bundle>yes</g:is_bundle>
            </item>
        @endforeach
    </channel>
</rss>
