<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=0.666667, maximum-scale=0.666667, user-scalable=0"/>
    <meta name="viewport" content="width=device-width"/>
    <meta name="HandheldFriendly" content="true"/>
    <title></title>
    <style type="text/css">
        body {
            min-width: 100% !important;
        }
    </style>
</head>
<body>
<table width="750" cellspacing="0" cellpadding="0" border="0" bgcolor="#f3f3f3" align="center" style="text-align: center;">
    <tr>
        <td width="100" style="padding-bottom: 10px;padding-top: 20px;font-family: Arial"></td>
        <td width="550" style="padding-bottom: 10px;padding-top: 20px;text-align: center;font-family: Arial">
            <table cellspacing="0" cellpadding="0" border="0">
                <tbody>
                <tr>
                    <td align="left" style="font-family: Arial"><img width="303" height="50"
                                                                     src="http://www.superiorpromos.com/images/mail_logo4.png"/>
                    </td>
                    <td style="font-family: Arial"></td>
                </tr>
                </tbody>
            </table>
        </td>
        <td width="100" style="padding-bottom: 10px;padding-top: 20px;font-family: Arial"></td>
    </tr>
</table>
<table width="750" cellspacing="0" cellpadding="0" border="0" bgcolor="#f3f3f3" align="center" style="text-align: left;">
    <tr>
        <td width="100" style="font-family: Arial"></td>
        <td width="550" style="font-family: Arial"> @yield('content')
        </td>
        <td width="100" style="font-family: Arial"></td>
    </tr>
</table>
<table width="750" cellspacing="0" cellpadding="0" border="0" bgcolor="#f3f3f3" align="center" style="text-align: center;">
    <tr>
        <td width="100" style="font-family: Arial"></td>
        <td width="550"
            style="font-family: Arial;font-size: 11px;color: rgb(80, 80, 80);padding-top: 25px;padding-bottom: 20px"><a
                    style="color: rgb(59, 123, 176); text-decoration: none; padding-right: 15px;"
                    href="http://www.superiorpromos.com/customer_service/contact_us">Contact Us</a> <a
                    style="color: rgb(59, 123, 176); text-decoration: none; padding-right: 15px;"
                    href="http://www.superiorpromos.com/account">My Account</a> <a
                    style="color: rgb(59, 123, 176); text-decoration: none; padding-right: 15px;"
                    href="http://www.superiorpromos.com/shopping_help/order_process">Order Process</a> <a
                    style="color: rgb(59, 123, 176); text-decoration: none; padding-right: 15px;"
                    href="http://www.superiorpromos.com/customer_service/faq">FAQ</a> <a
                    style="color: rgb(59, 123, 176); text-decoration: none;"
                    href="http://www.superiorpromos.com/company_info/testimonials">Testimonials</a></td>
        <td width="100" style="font-family: Arial"></td>
    </tr>
</table>
<table width="750" cellspacing="0" cellpadding="0" border="0" bgcolor="#f3f3f3" align="center" style="text-align: center;">
    <tr>
        <td width="100" style="font-family: Arial"></td>
        <td width="550" style="font-family: Arial;font-size: 11px;color: rgb(80, 80, 80);padding-top: 5px">Thank You for
            choosing <a style="font-family: Arial; font-size: 11px; color: rgb(59, 123, 176); text-decoration: none;"
                        href="http://www.superiorpromos.com/">SuperiorPromos.com</a>, where customer satisfaction is our
            #1 priority.
        </td>
        <td width="100" style="font-family: Arial"></td>
    </tr>
</table>
<table width="750" cellspacing="0" cellpadding="0" border="0" bgcolor="#f3f3f3" align="center" style="text-align: center;">
    <tr>
        <td width="100" style="font-family: Arial"></td>
        <td width="550" style="font-family: Arial;font-size: 11px;color: rgb(80, 80, 80);padding-top: 5px">Questions
            about your order or have any discrepancies? Call us at 1-888-577-6667 9am-6pm EST
        </td>
        <td width="100" style="font-family: Arial"></td>
    </tr>
</table>
<table width="750" cellspacing="0" cellpadding="0" border="0" bgcolor="#f3f3f3" align="center" style="text-align: center;">
    <tr>
        <td width="100" style="font-family: Arial"></td>
        <td width="550" style="font-family: Arial;font-size: 11px;color: rgb(80, 80, 80);padding-top: 5px">or email us
            at <a style="font-family: Arial; font-size: 11px; color: rgb(59, 123, 176); text-decoration: none;"
                  href="mailto:info@superiorpromos.com">info@superiorpromos.com</a> You can also fax us at 718-336-4808
        </td>
        <td width="100" style="font-family: Arial"></td>
    </tr>
</table>
<table width="750" cellspacing="0" cellpadding="0" border="0" bgcolor="#f3f3f3" align="center" style="text-align: center;">
    <tr>
        <td width="100" style="font-family: Arial"></td>
        <td width="550" style="padding-top: 5px;font-family: Arial"></td>
        <td width="100" style="font-family: Arial"></td>
    </tr>
</table>
<table width="750" cellspacing="0" cellpadding="0" border="0" bgcolor="#f3f3f3" align="center" style="text-align: center;">
    <tr>
        <td width="100" style="font-family: Arial"></td>
        <td width="550" style="font-family: Arial;font-size: 11px;color: rgb(80, 80, 80);padding-top: 5px">Superior
            Promos The Original Low Cost Leader! <a
                    style="font-family: Arial; font-size: 11px; color: rgb(59, 123, 176); text-decoration: none;"
                    href="http://www.superiorpromos.com/">www.superiorpromos.com</a></td>
        <td width="100" style="font-family: Arial"></td>
    </tr>
</table>
<table width="750" cellspacing="0" cellpadding="0" border="0" bgcolor="#f3f3f3" align="center" style="text-align: center;">
    <tr>
        <td width="100" style="font-family: Arial"></td>
        <td width="550"
            style="font-family: Arial;font-size: 11px;color: rgb(80, 80, 80);padding-top: 5px;padding-bottom: 20px"><a
                    style="font-family: Arial; font-size: 11px; color: rgb(59, 123, 176); text-decoration: none;"
                    href="http://www.superiorpromos.com/">SuperiorPromos.com</a> is a registered trademark of Superior
            Promos Inc.
            <a style="font-family: Arial; font-size: 11px; color: rgb(59, 123, 176); text-decoration: none;"
               href="http://www.superiorpromos.com/privacy_policy">privacy policy</a> &amp;
            <a style="font-family: Arial; font-size: 11px; color: rgb(59, 123, 176); text-decoration: none;"
               href="http://www.superiorpromos.com/terms_conditions">terms and conditions</a>		<font color="#f3f3f3" style="font-size="1px">message_id</font></td>
        <td width="100" style="font-family: Arial"></td>
    </tr>
</table>
</body>
</html>