@extends('emails.template')

@section('content')
    <br>
    <p><h3>New Reorder Request</h3></p>
    <hr>
    <p><b>Customer</b> :: {{auth()->user()->name}}</p>
    <p><b>Customer ID</b> :: {{auth()->user()->id}}</p>
    <hr>
    <p><h3>Items to reorder :</h3></p>
    @foreach($orderItems as $orderData)
        <hr>
        <p><b>Sub Order</b> :: #@if(!is_null($orderData['order_item_id'])){{$orderData['order_id']}}S{{$orderData['order_item_id']}}@else No data @endif</p>
        <p><b>Product ID</b> :: {{$orderData['product_id'] or 'No data'}}</p>
        <p><b>Item Name</b> :: {{$orderData['product_name'] or 'No data'}}</p>

        <p><b>Is the quantity going to change?</b> :: @if($orderData['is_quantity_change']) Yes @else No @endif</p>
        @if($orderData['is_quantity_change'])
        <p><b>New quantity</b> :: {{$orderData['quantity'] or 'No data'}}</p>
        @endif

        <p><b>Are the contents of this reorder going to be exactly the same?</b> :: @if($orderData['is_same_contents']) Yes @else No @endif</p>

        <p><b>Are you going to be providing new artwork for this reorder?</b> :: @if($orderData['is_new_artwork_provide']) Yes @else No @endif</p>

        <p><b>When do you need this order delivered by?</b> :: @if($orderData['is_delivered_by_asap']) ASAP @else {{$orderData['received_date']}} @endif</p>

        <p><b>Is item color going to change?</b> :: @if($orderData['is_item_color_change']) Yes @else No @endif</p>
        @if($orderData['is_item_color_change'])
            <p><b>New item colors</b> ::
                <ul>
                @forelse($orderData['product_colors'] as $color)
                    <li>
                        Color name: {{$color['color_name']}}
                        <span title="{{$color['color_name']}}"
                          style="display:inline-block;width:20px;height:20px;border-radius:20px;background-color: {{$color['color_hex']}}; border: 1px solid black;"></span>
                        (Color group name: {{$color['color_group_name']}})
                    </li>
                @empty
                    No data
                @endforelse
                </ul>
            </p>
        @endif

        <p><b>Is the imprint color going to change?</b> :: @if($orderData['is_imprint_color_change']) Yes @else No @endif</p>
        @if($orderData['is_imprint_color_change'])
            <p><b>New imprint item colors</b>  ::</p>

            @forelse($orderData['product_imprints'] as $imprint)
                <p>Imprint name: {{$imprint['imprint_name'] or 'No data'}}</p>
                <ul>
                    @forelse($imprint['color_ids'] as $color)
                        <li>
                            Color name: {{$color['color_name']}}
                            <span title="{{$color['color_name']}}"
                                  style="display:inline-block;width:20px;height:20px;border-radius:20px;background-color: {{$color['color_hex']}}; border: 1px solid black;"></span>
                        </li>
                    @empty
                        No data
                    @endforelse
                </ul>
            @empty
                No data
            @endforelse
        @endif

        <p><b>Is the shipping address for this order the same as before?</b> :: @if($orderData['is_shipping_same']) Yes @else No @endif</p>
        <p>
        @if(!$orderData['is_shipping_same'])
            <b>First Name: </b> {{$orderData['shipping_address']['first_name']}}<br>
            <b>Last name: </b> {{$orderData['shipping_address']['last_name']}}<br>
            <b>Title: </b> {{$orderData['shipping_address']['title']}}<br>
            <b>Company name: </b> {{$orderData['shipping_address']['company_name']}}<br>
            <b>Adr. Line 1: </b> {{$orderData['shipping_address']['address_line_1']}}<br>
            <b>Adr. Line 2: </b> {{$orderData['shipping_address']['address_line_2']}}<br>
            <b>City: </b> {{$orderData['shipping_address']['city']}}<br>
            <b>State: </b> {{$orderData['shipping_address']['state']}}<br>
            <b>Zip: </b> {{$orderData['shipping_address']['zip']}}<br>
            <b>Day telephone: </b> {{$orderData['shipping_address']['day_telephone']}}<br>
            <b>Ext: </b> {{$orderData['shipping_address']['ext']}}<br>
            <b>Fax: </b> {{$orderData['shipping_address']['fax']}}<br>
        @endif
        </p>

        <p><b>Is the billing address for this order the same as before?</b> :: @if($orderData['is_billing_same']) Yes @else No @endif</p>
        <p>
        @if(!$orderData['is_billing_same'])
            <b>First Name: </b> {{$orderData['billing_address']['first_name']}}<br>
            <b>Last name: </b> {{$orderData['billing_address']['last_name']}}<br>
            <b>Title: </b> {{$orderData['billing_address']['title']}}<br>
            <b>Company name: </b> {{$orderData['billing_address']['company_name']}}<br>
            <b>Adr. Line 1: </b> {{$orderData['billing_address']['address_line_1']}}<br>
            <b>Adr. Line 2: </b> {{$orderData['billing_address']['address_line_2']}}<br>
            <b>City: </b> {{$orderData['billing_address']['city']}}<br>
            <b>State: </b> {{$orderData['billing_address']['state']}}<br>
            <b>Zip: </b> {{$orderData['billing_address']['zip']}}<br>
            <b>Day telephone: </b> {{$orderData['billing_address']['day_telephone']}}<br>
            <b>Ext: </b> {{$orderData['billing_address']['ext']}}<br>
            <b>Fax: </b> {{$orderData['billing_address']['fax']}}<br>
        @endif
        </p>

        <p><b>Should the same payment on file be used for this reorder?</b> :: @if($orderData['is_payment_same']) Yes @else No @endif</p>
        <p>
        @if(!$orderData['is_payment_same'])
            <b>First Name: </b> {{$orderData['payment_address']['first_name']}}<br>
            <b>Last name: </b> {{$orderData['payment_address']['last_name']}}<br>
            <b>Company name: </b> {{$orderData['payment_address']['company']}}<br>
            <b>Adr. Line 1: </b> {{$orderData['payment_address']['address1']}}<br>
            <b>Adr. Line 2: </b> {{$orderData['payment_address']['address2']}}<br>
            <b>City: </b> {{$orderData['payment_address']['city']}}<br>
            <b>State: </b> {{$orderData['payment_address']['state']}}<br>
            <b>Zip: </b> {{$orderData['payment_address']['zip']}}<br>
            <b>Phone: </b> {{$orderData['payment_address']['phone']}}<br>
            <b>Ext: </b> {{$orderData['payment_address']['phone_extension']}}<br>
            <b>Card number: </b> {{$orderData['payment_address']['card_number']}}<br>
        @endif
        </p>

        <p><b>Comment</b> :: {{$orderData['comment']}}</p>

        <p><b>Old Order Cost.</b> :: {{$orderData['old_order_cost'] or 'No data'}}</p>
    @endforeach
@stop