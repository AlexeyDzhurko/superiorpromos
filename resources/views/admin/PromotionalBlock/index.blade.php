@extends('admin.layout')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        Promotional Blocks
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                <?php $formLink = route('promotional_block.search', [], false);?>
                <?php $formLinkCategory = route('promotional_block.search_category', [], false);?>

                    <!-- form start -->
                    <form role="form"
                          action="{{ route('promotional_block.store') }}"
                          method="POST" enctype="multipart/form-data">
                        <div class="panel-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @foreach($promotionalBlocks as $promotionalBlock)
                                @if($promotionalBlock['id'])
                                    <h1><?= \App\Models\PromotionalBlock::getBlocksLabel()[$promotionalBlock['id']]?></h1>
                                @endif
                                @if(isset($promotionalBlock['title']))
                                    <label for="name">Title</label>
                                    <input type="text" class="form-control" name="{{$promotionalBlock['id']}}[title]" value="{{$promotionalBlock['title']}}">
                                @endif
                                @if(isset($promotionalBlock['slug']))
                                    <label for="name">Slug</label>
                                    <input type="text" class="form-control" name="{{$promotionalBlock['id']}}[slug]" value="{{$promotionalBlock['slug']}}">
                                @endif
                                <label for="name">Promotional Products</label>
                                <br/>
                                    <select class="select-multiple-{{$promotionalBlock['id']}}" name="{{$promotionalBlock['id']}}[products][]"
                                            multiple="multiple" style="width: 30%"></select>
                                <br/>

                                <label for="name">Read more category</label>
                                <br/>
                                <select class="select-category-{{$promotionalBlock['id']}}" name="{{$promotionalBlock['id']}}[category]"
                                        style="width: 30%"></select>
                            @endforeach
                            
                        </div><!-- panel-body -->

                    <!-- CSRF TOKEN -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <?php //todo: move in separate file ?>
    <script>
        $('document').ready(function () {

            var PRE_SELECT_TOP_BLOCK = JSON.parse(JSON.stringify(<?= json_encode($topBlockCollections); ?>));
            var PRE_SELECT_MIDDLE_BLOCK = JSON.parse(JSON.stringify(<?= json_encode($topMiddleCollections); ?>));
            var PRE_SELECT_BOTTOM_BLOCK = JSON.parse(JSON.stringify(<?= json_encode($topBottomCollections); ?>));
            var READ_MORE_CATEGORY_BLOCK = JSON.parse(JSON.stringify(<?= json_encode($promotionalBlocks); ?>));
            console.log(READ_MORE_CATEGORY_BLOCK);

            $('.select-multiple-1').select2({
                minimumInputLength: 2,
                maximumSelectionLength: 10,
                initSelection: function (element, callback) {
                    for (var x = 0; x<PRE_SELECT_TOP_BLOCK.length; x++) {
                        $('.select-multiple-1').append($("<option/>", {
                            value: PRE_SELECT_TOP_BLOCK[x].id,
                            text: PRE_SELECT_TOP_BLOCK[x].text,
                            selected: true
                        }));
                    }
                    callback(PRE_SELECT_TOP_BLOCK);
                },
                ajax: {
                    url: "{{ $formLink }}",
                    dataType: 'json',
                    data: function (params) {
                        return {
                            query: params.term
                        };
                    },
                    results: function (data) {
                        return { results: data };
                    },
                    processResults: function (data) {
                        var answer = [];
                        for (var i = 0; i < data.length; i++ ) {
                            answer.push({
                                id: data[i].id,
                                text: data[i].name
                            });
                        }
                        return {
                            results: answer
                        };
                    },
                    cache: true
                },
            });

            $('.select-multiple-2').select2({
                minimumInputLength: 2,
                maximumSelectionLength: 10,
                initSelection: function (element, callback) {
                    for (var x = 0; x<PRE_SELECT_MIDDLE_BLOCK.length; x++) {
                        $('.select-multiple-2').append($("<option/>", {
                            value: PRE_SELECT_MIDDLE_BLOCK[x].id,
                            text: PRE_SELECT_MIDDLE_BLOCK[x].text,
                            selected: true
                        }));
                    }
                    callback(PRE_SELECT_MIDDLE_BLOCK);
                },
                ajax: {
                    url: "{{ $formLink }}",
                    dataType: 'json',
                    data: function (params) {
                        return {
                            query: params.term
                        };
                    },
                    results: function (data) {
                        return { results: data };
                    },
                    processResults: function (data) {
                        var answer = [];
                        for (var i = 0; i < data.length; i++ ) {
                            answer.push({
                                id: data[i].id,
                                text: data[i].name
                            });
                        }
                        return {
                            results: answer
                        };
                    },
                    cache: true
                },
            });

            $('.select-multiple-3').select2({
                minimumInputLength: 2,
                maximumSelectionLength: 10,
                initSelection: function (element, callback) {
                    for (var x = 0; x<PRE_SELECT_BOTTOM_BLOCK.length; x++) {
                        $('.select-multiple-3').append($("<option/>", {
                            value: PRE_SELECT_BOTTOM_BLOCK[x].id,
                            text: PRE_SELECT_BOTTOM_BLOCK[x].text,
                            selected: true
                        }));
                    }
                    callback(PRE_SELECT_BOTTOM_BLOCK);
                },
                ajax: {
                    url: "{{ $formLink }}",
                    dataType: 'json',
                    data: function (params) {
                        return {
                            query: params.term
                        };
                    },
                    results: function (data) {
                        return { results: data };
                    },
                    processResults: function (data) {
                        var answer = [];
                        for (var i = 0; i < data.length; i++ ) {
                            answer.push({
                                id: data[i].id,
                                text: data[i].name
                            });
                        }
                        return {
                            results: answer
                        };
                    },
                    cache: true
                },
            });

            var blocs = [1,2,3];
            blocs.forEach(function(item, i) {
                $('.select-category-' + item).select2({
                    minimumInputLength: 2,
                    maximumSelectionLength: 10,
                    initSelection: function (element, callback) {
                        $('.select-category-' + item).append($("<option/>", {
                            value: READ_MORE_CATEGORY_BLOCK[i]['category'].id,
                            text: READ_MORE_CATEGORY_BLOCK[i]['category'].name,
                            selected: true
                        }));
                        callback(READ_MORE_CATEGORY_BLOCK[i]['category']);
                    },
                    ajax: {
                        url: "{{ $formLinkCategory }}",
                        dataType: 'json',
                        data: function (params) {
                            return {
                                query: params.term
                            };
                        },
                        results: function (data) {
                            return { results: data };
                        },
                        processResults: function (data) {
                            var answer = [];
                            for (var i = 0; i < data.length; i++ ) {
                                answer.push({
                                    id: data[i].id,
                                    text: data[i].name
                                });
                            }

                            return {
                                results: answer
                            };
                        },
                        cache: true
                    }
                });
                $('.select-category-' + item).change();
            });

            $('.select-multiple-1').change();
            $('.select-multiple-2').change();
            $('.select-multiple-3').change();
        });

    </script>
@stop
