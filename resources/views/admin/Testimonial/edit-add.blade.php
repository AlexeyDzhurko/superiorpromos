@extends('vendor.voyager.master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $adminModel->getIcon() }}"></i> @if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'New' }}@endif {{ $adminModel->getDisplayNameSingular() }}
    </h1>
@stop

@section('content')
    <?php
        $formLink = null;
    ?>
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">

                    <div class="panel-heading">
                        <h3 class="panel-title">@if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'Add New' }}@endif {{ $adminModel->getDisplayNameSingular() }}</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form"
                          action="@if(isset($dataTypeContent->id)){{ route('admin_entity.update', ['admin_entity' => $adminModel->getBaseRouteName(), 'id' => $dataTypeContent->id]) }}@else{{ route('admin_entity.store', ['admin_entity' => $adminModel->getBaseRouteName()]) }}@endif"
                          method="POST" enctype="multipart/form-data">
                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @foreach($adminModel->formFields() as $formField)
                                <div class="form-group">
                                    <label for="name">{{ $formField->displayName }}</label>

                                    @if($formField->type == "text")
                                        <input type="text" class="form-control" name="{{ $formField->field }}"
                                               placeholder="{{ $formField->displayName }}"
                                               value="@if(isset($dataTypeContent->{$formField->field})){{ old($formField->field, $dataTypeContent->{$formField->field}) }}@else{{old($formField->field)}}@endif">
                                    @elseif($formField->type == "integer")
                                        <input type="number" step="1" min="1" class="form-control" name="{{
                                        $formField->field }}" placeholder="{{ $formField->displayName }}"
                                               value="@if(isset($dataTypeContent->{$formField->field})){{ old($formField->field, $dataTypeContent->{$formField->field}) }}@else{{old($formField->field)}}@endif">
                                    @elseif($formField->type == "password")
                                        @if(isset($dataTypeContent->{$formField->field}))
                                            <br>
                                            <small>Leave empty to keep the same</small>
                                        @endif
                                        <input type="password" class="form-control" name="{{ $formField->field }}" value="">
                                    @elseif($formField->type == "text_area")
                                        <textarea class="form-control"
                                                  name="{{ $formField->field }}">@if(isset($dataTypeContent->{$formField->field})){{ old($formField->field, $dataTypeContent->{$formField->field}) }}@else{{old($formField->field)}}@endif</textarea>
                                    @elseif($formField->type == "rich_text_box")
                                        <textarea class="form-control richTextBox"
                                                  name="{{ $formField->field }}">@if(isset($dataTypeContent->{$formField->field})){{ old($formField->field, $dataTypeContent->{$formField->field}) }}@else{{old($formField->field)}}@endif</textarea>
                                    @elseif($formField->type == "image" || $formField->type == "file")
                                        @if($formField->type == "image" && isset($dataTypeContent->{$formField->field}))
                                            <img src="{{ Voyager::image( $dataTypeContent->{$formField->field} ) }}"
                                                 style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
                                        @elseif($formField->type == "file" && isset($dataTypeContent->{$formField->field}))
                                            <div class="fileType">{{ $dataTypeContent->{$formField->field} }} }}</div>
                                        @endif
                                        <input type="file" name="{{ $formField->field }}">
                                    @elseif($formField->type == "select_dropdown")
                                        <?php $selected_value = (isset($dataTypeContent->{$formField->field}) && !empty(old($formField->field,
                                                        $dataTypeContent->{$formField->field}))) ? old($formField->field,
                                                $dataTypeContent->{$formField->field}) : old($formField->field); ?>
                                        <select class="form-control" name="{{ $formField->field }}">
                                            <?php $default = ($formField->getDefault() && !isset($dataTypeContent->{$formField->field})) ? $formField->getDefault() : NULL; ?>
                                            @if($formField->getOptions())
                                                @foreach($formField->getOptions() as $key => $option)
                                                    <option value="{{ $key }}" @if($default == $key && $selected_value === NULL){{ 'selected="selected"' }}@endif @if($selected_value == $key){{ 'selected="selected"' }}@endif>{{ $option }}</option>
                                                @endforeach
                                            @endif
                                        </select>

                                    @elseif($formField->type == "checkbox_list")
                                        @if($formField->getOptions())
                                            @foreach($formField->getOptions() as $key => $option)
                                                <p>
                                                    <input type="checkbox" name="{{ $option['id'] }}"> {{
                                                    $option['name'] }}
                                                </p>
                                            @endforeach
                                        @endif
                                    @elseif($formField->type == "radio_btn")
                                        <?php $options = json_decode($row->details); ?>
                                        <?php $selected_value = (isset($dataTypeContent->{$formField->field}) && !empty(old($formField->field,
                                                        $dataTypeContent->{$formField->field}))) ? old($formField->field,
                                                $dataTypeContent->{$formField->field}) : old($formField->field); ?>
                                        <?php $default = (isset($options->default) && !isset($dataTypeContent->{$formField->field})) ? $options->default : NULL; ?>
                                        <ul class="radio">
                                            @if(isset($options->options))
                                                @foreach($options->options as $key => $option)
                                                    <li>
                                                        <input type="radio" id="option-{{ $key }}"
                                                               name="{{ $formField->field }}"
                                                               value="{{ $key }}" @if($default == $key && $selected_value === NULL){{ 'checked' }}@endif @if($selected_value == $key){{ 'checked' }}@endif>
                                                        <label for="option-{{ $key }}">{{ $option }}</label>
                                                        <div class="check"></div>
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    @elseif($formField->type == "collection")
                                        <?php
                                            $formLink = $formField->link;
                                        ?>
                                        <select class="select-multiple" name="{{ $formField->field }}[]"
                                                style="width: 30%"></select>
                                    @elseif($formField->type == "checkbox")

                                        <?php $options = $formField->options; ?>
                                        <?php $checked = (isset($dataTypeContent->{$formField->field}) && old($formField->field,
                                                        $dataTypeContent->{$formField->field}) == 1) ? true : old($formField->field); ?>
                                        @if(isset($options->on) && isset($options->off))
                                            <input type="checkbox" name="{{ $formField->field }}"
                                                   data-on="1" @if($checked) checked
                                                   @endif data-off="0" value="1">
                                        @else
                                            <input type="checkbox" name="{{ $formField->field }}"
                                                   data-on="1" @if($checked) checked
                                                   @endif data-off="0" value="1">
                                        @endif

                                    @endif

                                </div>
                            @endforeach

                        </div><!-- panel-body -->


                        <!-- PUT Method if we are editing -->
                        @if(isset($dataTypeContent->id))
                            <input type="hidden" name="_method" value="PUT">
                    @endif

                    <!-- CSRF TOKEN -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('admin.upload') }}" target="form_target" method="post"
                          enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                               onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $adminModel->getBaseRouteName() }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script src="{{ config('voyager.assets_path') }}/lib/js/tinymce/tinymce.min.js"></script>
    <script src="{{ config('voyager.assets_path') }}/js/voyager_tinymce.js"></script>
@stop