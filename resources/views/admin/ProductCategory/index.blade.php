@extends('admin.base.product-categories-dnd')

@section('title', 'Product Categories')

@section('angular_controller', 'ProductCategoryController')

@section('panel_content')
    <div class="page-content container-fluid">
        <div ng-hide="hideLoader" class="voyager-loader">
            <img src="<% config('voyager.assets_path') %>/images/logo-icon.png" alt="Voyager Loader">
        </div>
        @if(isset($nestedCategories))
            <script>
                var categories = {!! json_encode($nestedCategories) !!}
            </script>
        @endif
        <div class="panel panel-bordered">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Nested node template -->
                        <script type="text/ng-template" id="nodes_renderer1.html">
                            <div ui-tree-handle class="tree-node tree-node-content">
                                <div class="card" style="box-shadow: none;">
                                    <div data-nodrag class="card-body" ng-style="selectedProducts.indexOf(node.id) > -1 ? {'background-color':'#FFDC0F'} : ''"
                                         ng-checked="selectedProducts.indexOf(node.id) > -1"
                                         ng-click="toggleSelection(node.id)">
                                        <div class="product-item">
                                            <span class="product-active-status-badge product-active-badge" ng-if="node.active == 1">ACTIVE</span>
                                            <span class="product-active-status-badge product-inactive-badge" ng-if="node.active == 0">INACTIVE</span>
                                            <img src="{{node.image_src}}"
                                                 alt="{{node.name}}" class="img-thumbnail" style="max-height: 100px">
                                        </div>
                                        <h5 class="card-title" ng-bind-html="trustAsHtml(node.name)"></h5>
                                        <h6 class="card-title" style="color: dodgerblue">Item #{{node.id}}</h6>
                                        <h6 class="card-subtitle mb-1 text-muted">Categories:</h6>
                                        <span data-nodrag ng-repeat="category in node.categories track by $index"><small
                                                    style="font-size: 12px;">{{getCategoryById(category).text}}
                                                <span class="glyphicon glyphicon-remove text-danger"
                                                      ng-click="deleteCategory(node.id, category)"></span></small></span>
                                    </div>
                                </div>
                            </div>
                            <div ui-tree-nodes="" ng-model="node.nodes">
                                <div ng-repeat="node in node.nodes">
                                    <hr ng-if="!($index%4)">
                                    <div class="col-sm-4" ui-tree-node ng-include="'nodes_renderer1.html'"></div>
                                </div>
                            </div>
                        </script>

                        <script type="text/ng-template" id="nodes_renderer.html">
                            <div ui-tree-handle>
                                <a class="btn btn-success btn-xs" ng-if="node.children && node.children.length > 0"
                                   data-nodrag ng-click="toggle(this)">
                                    <span class="glyphicon"
                                          ng-class="{
                                          'glyphicon-chevron-right': collapsed,
                                          'glyphicon-chevron-down': !collapsed
                                        }">
                                    </span>
                                </a>
                                <span ng-class="{'hovered' : node.id == searchData.category_id}"
                                      ng-click="searchByCategoryId(node.id)" ng-bind-html="node.name | highlight:pattern | trust"></span>

                                <span
                                        {{--ng-click="selectCategoryForTransfer(node.id)"--}}
                                        {{--ng-class="{'hovered-category' : node.id == selectedCategory}"--}}

                                        ng-click="toggleSelectionCategory(node.id)"
                                        ng-class="{'hovered-category' : selectedCategories.indexOf(node.id) > -1}"
                                        class="fa fa-check">
                                </span>
                            </div>
                            <ol ui-tree-nodes="" ng-model="node.children" ng-class="{hidden: collapsed}">
                                <li ng-repeat="node in node.children" ui-tree-node ng-include="'nodes_renderer.html'"
                                    ng-hide="!treeFilter(node, pattern, supportedFields)"
                                    ng-class="{'filtered-out':!treeFilter(node, pattern, supportedFields)}"></li>
                            </ol>
                        </script>

                        <script type="text/ng-template" id="drop-modal.html">
                            <div class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" ng-click="close(false)"
                                                    data-dismiss="modal" aria-hidden="true">&times;
                                            </button>
                                            <h4 class="modal-title">Please confirm your choose</h4>
                                        </div>
                                        <div class="modal-body">
                                            Do you want add products:
                                            <ul>
                                                <li ng-repeat="productId in productsId">
                                                    <b>"{{parentScope.getProductById(productId).name}}"</b>
                                                </li>
                                            </ul>
                                            to categories:
                                            <ul>
                                                <li ng-repeat="categoryId in categoriesId">
                                                    <b>"{{parentScope.getCategoryById(categoryId).text }}"</b>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" ng-click="close(false)" class="btn btn-default"
                                                    data-dismiss="modal">No
                                            </button>
                                            <button type="button" ng-click="close(true)" class="btn btn-primary"
                                                    data-dismiss="modal">Yes
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </script>

                        <script type="text/ng-template" id="drop-delete-modal.html">
                            <div class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" ng-click="close(false)"
                                                    data-dismiss="modal" aria-hidden="true">&times;
                                            </button>
                                            <h4 class="modal-title">Please confirm your choose</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p ng-repeat="productId in productsId">Do you want delete category <b>"{{parentScope.getCategoryById(categoryId).text}}"</b> from product

                                                <b>"{{parentScope.getProductById(productId).name}}"</b>?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" ng-click="close(false)" class="btn btn-default"
                                                    data-dismiss="modal">No
                                            </button>
                                            <button type="button" ng-click="close(true)" class="btn btn-primary"
                                                    data-dismiss="modal">Yes
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </script>

                        <script type="text/ng-template" id="activation-inactivation-modal.html">
                            <div class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" ng-click="close(false)"
                                                    data-dismiss="modal" aria-hidden="true">&times;
                                            </button>
                                            <h4 class="modal-title">Please confirm your choose</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p ng-repeat="productId in productsId">Do you want {{activationStatus == 1 ? 'activate' : 'deactivate'}} product
                                                <b>"{{parentScope.getProductById(productId).name}}"</b>?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" ng-click="close(false)" class="btn btn-default"
                                                    data-dismiss="modal">No
                                            </button>
                                            <button type="button" ng-click="close(true)" class="btn btn-primary"
                                                    data-dismiss="modal">Yes
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </script>

                        <%-- content --%>
                        <div class="row">
                            <div class="col-sm-4">
                                <h3>Categories</h3>
                                <br>
                                <div class="col-sm-12 text-align-center">
                                    <p>Search: <input ng-model="pattern"></p>
                                    <button ng-click="expandAll()" class="btn btn-default">Expand all</button>
                                    <button ng-click="collapseAll()" class="btn btn-default">Collapse all</button>
                                    <button ng-click="clearCategorySearch()" type="reset"
                                            class="btn btn-default btn-sm">Clear
                                    </button>
                                    <br><br>
                                    <p ng-if="searchData.category_id">Products displayed from category <b>"{{getCategoryById(searchData.category_id).text}}"</b></p>
                                    <p ng-if="selectedCategory != null">Category <b>"{{getCategoryById(selectedCategory).text}}"</b> selected for move products</p>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <h3>Products</h3>
                                <br>
                                <form class="form-horizontal search-form" ng-submit="search()">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row form-group" style="margin-top: -10px">
                                                <div class="col-md-4 form-group form-group-sm">
                                                    <label for="id" class="col-md-4 control-label">ID</label>
                                                    <div class="col-md-8">
                                                        <input ng-model="searchData.id" type="text" class="form-control"
                                                               id="id" placeholder="ID">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 form-group form-group-sm">
                                                    <label for="name" class="col-md-4 control-label">Name</label>
                                                    <div class="col-md-8">
                                                        <input ng-model="searchData.name" type="text"
                                                               class="form-control" id="name" placeholder="Name">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 form-group form-group-sm">
                                                    <label class="col-md-4 control-label" for="sku">V.Sku</label>
                                                    <div class="col-md-8">
                                                        <input ng-model="searchData.sku" type="text"
                                                               class="form-control" id="sku" placeholder="SKU">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-group" style="margin-top: -10px">
                                                <div class="col-md-4 form-group form-group-sm">
                                                    <label class="col-md-4 control-label" for="vendor">Category</label>
                                                    <div class="col-md-8">
                                                        <selectize placeholder="Category"
                                                                   config='{valueField: "id", labelField: "text", maxItems: 1}'
                                                                   options='categoriesListWithSeparator'
                                                                   ng-model="searchData.category_id"></selectize>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 form-group form-group-sm">
                                                    <label class="col-md-4 control-label" for="vendor">Vendor</label>
                                                    <div class="col-md-8">
                                                        <selectize placeholder="Vendor"
                                                                   config='{valueField: "id", labelField: "text", maxItems: 1}'
                                                                   options='vendors'
                                                                   ng-model="searchData.vendor_id"></selectize>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 form-group form-group-sm">
                                                    <label class="col-md-4 control-label" for="active">SageID</label>
                                                    <div class="col-md-8">
                                                        <selectize placeholder="Contains"
                                                                   config='{valueField: "id", labelField: "text", maxItems: 1}'
                                                                   options='sageStatuses'
                                                                   ng-model="searchData.sage"></selectize>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-group" style="margin-top: -10px">
                                                <div class="col-md-4 form-group form-group-sm">
                                                    <label for="date_from" class="col-md-4 control-label">Date</label>
                                                    <div class="col-md-8">
                                                        <span class="dropdown">
                                                            <a class="dropdown-toggle" id="date_from" role="button"
                                                               data-toggle="dropdown"
                                                               data-target="#" href="#">
                                                                <input name="date_from" id="date_from"
                                                                       placeholder="From" type="text"
                                                                       class="form-control"
                                                                       data-ng-model="searchData.date_from">
                                                            </a>
                                                            <ul class="dropdown-menu" role="menu"
                                                                aria-labelledby="dLabel">
                                                                <datetimepicker data-ng-model="searchData.date_from"
                                                                                data-datetimepicker-config="{ dropdownSelector: '#date_from', modelType: 'YYYY-MM-DD', minView: 'day' }"/>
                                                            </ul>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 form-group form-group-sm">
                                                    <label class="col-md-4 control-label" for="date_to">To</label>
                                                    <div class="col-md-8">
                                                        <span class="dropdown">
                                                            <a class="dropdown-toggle" id="date_to" role="button"
                                                               data-toggle="dropdown" data-target="#"
                                                               href="#">
                                                                <input name="date_to" id="date_to" placeholder="To"
                                                                       type="text" class="form-control"
                                                                       data-ng-model="searchData.date_to">
                                                            </a>
                                                            <ul class="dropdown-menu" role="menu"
                                                                aria-labelledby="dLabel">
                                                                <datetimepicker data-ng-model="searchData.date_to"
                                                                                data-datetimepicker-config="{ dropdownSelector: '#date_to', modelType: 'YYYY-MM-DD', minView: 'day' }"/>
                                                            </ul>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 form-group form-group-sm">
                                                    <label for="active" class="col-md-4 control-label">Activity</label>
                                                    <div class="col-md-8">
                                                        <selectize placeholder="Activity"
                                                                   config='{valueField: "id", labelField: "text", maxItems: 1}'
                                                                   options='statuses'
                                                                   ng-model="searchData.active"></selectize>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-group" style="margin-top: -10px">
                                                <div class="col-md-12 text-right">
                                                    <div class="row">
                                                        <div class="text-left col-sm-6">
                                                            <button ng-click="deleteCategoryFromProducts()" type="reset"
                                                                    class="btn btn-default btn-sm btn-danger" ng-if="searchData.category_id && selectedProducts.length > 0">Remove from "{{getCategoryById(searchData.category_id).text}}"</button>
                                                            <button ng-click="transferByButton()" type="reset"
                                                                    class="btn btn-default btn-sm btn-success" ng-if="selectedCategory != null">Add to "{{getCategoryById(selectedCategory).text}}"
                                                            </button>
                                                            <button ng-click="transferProductsByButton()" type="reset"
                                                                    class="btn btn-default btn-sm btn-success" ng-if="selectedProducts.length > 0 && selectedCategories.length > 0">Add to other categories
                                                            </button>
                                                            <button ng-click="showActivationDeactivationModal(1)" type="reset"
                                                                    class="btn btn-default btn-sm btn-warning" ng-if="selectedProducts.length > 0">Activate
                                                            </button>
                                                            <button ng-click="showActivationDeactivationModal(0)" type="reset"
                                                                    class="btn btn-default btn-sm btn-warning" ng-if="selectedProducts.length > 0">Deactivate
                                                            </button>
                                                        </div>
                                                        <div class="text-right col-sm-6">
                                                            <button type="submit" class="btn btn-default btn-sm">Search</button>
                                                            <button ng-click="displayAllProducts()" type="reset"
                                                                    class="btn btn-default btn-sm">View all
                                                            </button>
                                                            <button ng-click="resetSearch()" type="reset"
                                                                    class="btn btn-default btn-sm">Reset
                                                            </button>
                                                            <button ng-click="clearSearch()" type="reset"
                                                                    class="btn btn-default btn-sm">Clear
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row" style="height: 600px;">
                            <div style="overflow-y: auto; height: 100%;" class="col-sm-4 categories-tree">
                                <div style="overflow-y: auto; max-height: 100%;">
                                    <div ui-tree id="tree2-root" data-clone-enabled="true" data-nodrag="true">
                                        <ol ui-tree-nodes="" ng-model="categories">
                                            <li ng-repeat="node in categories" ui-tree-node
                                                ng-include="'nodes_renderer.html'"
                                                ng-hide="!treeFilter(node, pattern, supportedFields)"></li>
                                        </ol>
                                    </div>
                                </div>
                            </div>

                            <div style="overflow-y: auto; height: 100%;" class="col-sm-8">
                                <div class="row">
                                    <div ng-if="pagination.lastPage > 1">
                                        <span ng-click="getListData(pagination.currentPage - 1)"
                                              ng-if="pagination.currentPage > 1" style="color: #337ab7"><<</span>
                                        <select ng-change="getListData(pagination.currentPage)"
                                                ng-model="pagination.currentPage"
                                                ng-options="n as 'Page '+n for n in range">
                                        </select>
                                        of {{pagination.lastPage}} <span ng-click="getListData(pagination.currentPage + 1)"
                                                                         ng-if="pagination.currentPage < pagination.lastPage"
                                                                         style="color: #337ab7">>></span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div data-ui-tree="treeOptions" id="tree1-root" data-clone-enabled="true"
                                         data-nodrop-enabled="true" data-dropzone-enabled="false">
                                        <div ui-tree-nodes="" ng-model="listData">
                                            <div ng-repeat="node in listData">
                                                <div class="clearfix" ng-if="$index % 4 == 0"></div>
                                                <div class="col-sm-3" ui-tree-node
                                                     ng-include="'nodes_renderer1.html'"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div ng-if="pagination.lastPage > 1">
                                    <span ng-click="getListData(pagination.currentPage - 1)"
                                          ng-if="pagination.currentPage > 1" style="color: #337ab7"><<</span>
                                        <select ng-change="getListData(pagination.currentPage)"
                                                ng-model="pagination.currentPage"
                                                ng-options="n as 'Page '+n for n in range">
                                        </select>
                                        of {{pagination.lastPage}} <span ng-click="getListData(pagination.currentPage + 1)"
                                                                         ng-if="pagination.currentPage < pagination.lastPage"
                                                                         style="color: #337ab7">>></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
