@extends('admin.layout')

@section('css')
    <meta name="csrf-token" content="<% csrf_token() %>">
    <link rel="stylesheet" href="/admin-assets/css/assets.min.css">
@stop

@section('page_header')
    <h1 class="page-title">
        @yield('title')
    </h1>
@stop

@section('content')
    <?php
    $formLink = null;
    ?>
    <div class="page-content container-fluid" ng-app="app" ng-controller="@yield('angular_controller')" id="pageElement">
        <div ng-hide="hideLoader" class="voyager-loader">
            <img src="<% config('voyager.assets_path') %>/images/logo-icon.png" alt="Voyager Loader">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-body">
                                @yield('panel_content')
                            </div>
                        </div>
                    </div>
                </div>
             </div>
        </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
    <script src="<% config('voyager.assets_path') %>/lib/js/tinymce/tinymce.min.js"></script>
    <script src="<% config('voyager.assets_path') %>/js/voyager_tinymce.js"></script>
@stop
