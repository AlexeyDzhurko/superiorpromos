@extends('admin.layout')

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <h3 class="panel-title">Edit Current Promotions</h3>
                    </div>
                    <form role="form" action="{{route('promotion.store')}}" method="POST">
                        <div class="panel-body">
                            <div class="form-group">
                                <textarea rows="25" class="form-control promotion-info" name="data"><?=(isset($promotion['data'])) ? $promotion['data'] : ''?></textarea>
                                @if($errors->has('data'))
                                    <div style="color: #FF0000" class="error">{{ $errors->first('data') }}</div>
                                @endif
                            </div>
                        </div>
                        <!-- CSRF TOKEN -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{$promotion['id']}}">
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Edit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @stop
@section('javascript')
    <script src="{{ config('voyager.assets_path') }}/lib/js/tinymce/tinymce.min.js"></script>
    <script>
        $(document).ready(function(){
            tinymce.init({
                menubar: false,
                selector:'textarea.promotion-info',
                plugins: 'link, code',
                extended_valid_elements : 'input[onclick|value|style|type]',
                toolbar: 'styleselect bold italic underline | alignleft aligncenter alignright | bullist numlist outdent indent | link | code',
                convert_urls: false,
                image_caption: true,
                image_title: true
            });
        });
    </script>
@stop
