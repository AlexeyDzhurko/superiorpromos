@extends('admin.layout')

@section('css')
    <meta name="csrf-token" content="<% csrf_token() %>">
    <link rel="stylesheet" href="/admin-assets/css/assets.min.css">
@stop

@section('page_header')
    <h1 class="page-title">
        Order #<% $order->id %>
    </h1>
@stop

@section('content')
    <?php
    $formLink = null;
    ?>
    <div class="page-content container-fluid" ng-app="app" ng-controller="OrderController">
        <div ng-hide="hideLoader" class="voyager-loader">
            <img src="<% config('voyager.assets_path') %>/images/logo-icon.png" alt="Voyager Loader">
        </div>
        <script>
            order = {!! $order->toJson() !!};
        </script>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">

                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="panel-footer">

                        <h5>Order Data</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered table-hover table-info">
                                        <tbody>
                                        <tr>
                                            <th class="info" scope="row">Order Date</th>
                                            <td><% $order->created_at %></td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">Customer</th>
                                            <td><% $order->user->name %> </td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">Customer ID</th>
                                            <td><% $order->user->id %></td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">CIM Profile ID</th>
                                            <td><% $order->user->authorize_net_id %></td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">Customer Email</th>
                                            <td><a href="mailto:<% $order->user->email %>"><% $order->user->email %></a></td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">Questions</th>
                                            <td>
                                                When is the date of your event? <% $order->event_date %> <br>
                                                What is the quantity that you eventually looking to purchase? <% $order->quantity %> <br>
                                                Would you like samples to be blank or with a random imprint? <% $order->sample %> <br>
                                                What attracted you to these items? <% $order->attraction %> <br>
                                                How many times a year to you purchase promotional items? <% $order->frequency %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">Upcoming event</th>
                                            <td>{!! $order->sample_comment !!}</td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered table-hover table-info">
                                        <tbody>
                                            <tr>
                                                <th class="info" scope="row">Product Total</th>
                                                <td><% $order->total_price %></td>
                                            </tr>
                                            <tr>
                                                <th class="info" scope="row">Discount Used</th>
                                                <td>@if(is_null($order->discount_id)) None @else Yes @endif</td>
                                            </tr>
                                            <%--<tr>
                                                <th class="info" scope="row">Payment Type</th>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th class="info" scope="row">CIM Payment Profile ID</th>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th class="info" scope="row">Auth. Transaction #</th>
                                                <td></td>
                                            </tr>--%>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <h5>Shipping Address</h5>
                                <div>
                                    @php
                                        $shippingAddress = $order->user->addresses->where('type', 1)->first();
                                        $billingAddress = $order->user->addresses->where('type', 2)->first();
                                    @endphp
                                     <div><%$shippingAddress->first_name%> <%$shippingAddress->middle_name%> <%$shippingAddress->last_name%></div>
                                    <div><%$shippingAddress->company_name%></div>
                                    <div><%$shippingAddress->address_line_1%> <%$shippingAddress->address_line_2%></div>
                                    <div>
                                        <%$shippingAddress->city%>
                                        @if($shippingAddress->state), @endif
                                        <%$shippingAddress->state%> <%$shippingAddress->zip%>
                                    </div>
                                    <div><%$shippingAddress->day_telephone%></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h5>Billing Address</h5>
                                <div>
                                    <div><%$billingAddress->first_name%> <%$billingAddress->middle_name%> <%$billingAddress->last_name%></div>
                                    <div><%$billingAddress->company_name%></div>
                                    <div><%$billingAddress->address_line_1%> <%$billingAddress->address_line_2%></div>
                                    <div>
                                        <%$billingAddress->city%>
                                        @if($billingAddress->state), @endif
                                        <%$billingAddress->state%> <%$billingAddress->zip%>
                                    </div>
                                    <div><%$billingAddress->day_telephone%></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h5>Order Contents</h5>
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered table-hover table-info">
                                        <thead>
                                            <tr>
                                                <th class="info">Reference #</th>
                                                <th class="info">Product</th>
                                                <th class="info">In-Hand Date</th>
                                                <th class="info">Price</th>
                                                <th class="info">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($order->orderSampleItems as $orderSampleItem)
                                                <tr>
                                                    <td><%$orderSampleItem->order_sample_id%>S<%$orderSampleItem->id%></td>
                                                    <td>
                                                        <%$orderSampleItem->product->name ?? null%>
                                                        <img class="img-thumbnail admin-thumbnail" alt="<%$orderSampleItem->product->name%>" src="<%$orderSampleItem->product->image_src or '/img/image-not-available.jpg'%>"/>
                                                        (Item #<%$orderSampleItem->product->id ?? null%>)
                                                    </td>
                                                    <td>@if(is_null($orderSampleItem->received_date) or $orderSampleItem->received_date == '0000-00-00') ASAP @else $orderSampleItem->received_date @endif</td>
                                                    <td>$<%$orderSampleItem->price + $orderSampleItem->options_price %></td>
                                                    <td><a href="<% route('order_sample_item.edit', ['orderSampleItem' => $orderSampleItem]) %>"
                                                           class="btn-sm btn-primary pull-right edit"><i class="voyager-edit"></i> Edit</a></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            var PRE_SELECT = <?php echo json_encode($collections); ?>

            $('.select-multiple').select2({
                minimumInputLength: 2,
                initSelection: function (element, callback) {
                    for (var x = 0; x<PRE_SELECT.length; x++) {
                        $('.select-multiple').append($("<option/>", {
                            value: PRE_SELECT[x].id,
                            text: PRE_SELECT[x].text,
                            selected: true
                        }));
                    }
                    callback(PRE_SELECT);
                },
                ajax: {
                    url: "<% $formLink %>",
                    dataType: 'json',
                    data: function (params) {
                        return {
                            query: params.term
                        };
                    },
                    results: function (data) {
                        return { results: data };
                    },
                    processResults: function (data) {
                        var answer = [];
                        for (var i = 0; i < data.length; i++ ) {
                            answer.push({
                                id: data[i].id,
                                text: data[i].name
                            });
                        }
                        return {
                            results: answer
                        };
                    },
                    cache: true
                },
            });
            $('.select-multiple').change();
        });
    </script>
    <script src="<% config('voyager.assets_path') %>/lib/js/tinymce/tinymce.min.js"></script>
    <script src="<% config('voyager.assets_path') %>/js/voyager_tinymce.js"></script>
@stop
