@extends('admin.layout')

@section('css')
    <meta name="csrf-token" content="<% csrf_token() %>">
    <link rel="stylesheet" href="/admin-assets/css/assets.min.css">
@stop

@section('content')
    <div class="page-content container-fluid" ng-app="app" ng-controller="OrdersSampleListController">
        <div ng-hide="hideLoader" class="voyager-loader">
            <img src="<% config('voyager.assets_path') %>/images/logo-icon.png" alt="Voyager Loader">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form class="form-horizontal search-form" ng-submit="search()">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row form-group">
                                        <div class="col-md-6 form-group form-group-sm">
                                            <label for="id" class="col-md-4 control-label"><b>Order Sample ID</b></label>
                                            <div class="col-md-8">
                                                <input ng-model="searchData.id" type="text" class="form-control" id="id"
                                                       placeholder="Order Sample ID">
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-group form-group-sm">
                                            <label for="sub_order_id" class="col-md-4 control-label"><b>Sub-Order ID</b></label>
                                            <div class="col-md-8">
                                                <input ng-model="searchData.sub_order_id" type="text" class="form-control" id="sub_order_id"
                                                       placeholder="Sub-Order ID">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group" style="margin-top: -5px;">
                                        <div class="col-md-6 form-group form-group-sm">
                                            <label for="billing_first_name" class="col-md-4 control-label"><b>User Name</b></label>
                                            <div class="col-md-8">
                                                <input ng-model="searchData.billing_first_name" type="text" class="form-control"
                                                       id="billing_first_name" placeholder="Customer Name">
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-group form-group-sm">
                                            <label for="user_id" class="col-md-4 control-label"><b>User ID</b></label>
                                            <div class="col-md-8">
                                                <input ng-model="searchData.user_id" type="text" class="form-control"
                                                       id="user_id" placeholder="User ID">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group" style="margin-top: -5px;">
                                        <div class="col-md-6 form-group form-group-sm">
                                            <label for="paid" class="col-md-4 control-label"><b>Check Notes</b></label>
                                            <div class="col-md-8">
                                                <selectize placeholder="Check Notes" config='{valueField: "id", labelField: "text", maxItems: 1}' options='check_notes_statuses' ng-model="searchData.check_notes"></selectize>
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-group form-group-sm">
                                            <label for="vendor_id" class="col-md-4 control-label"><b>Vendor ID</b></label>
                                            <div class="col-md-8">
                                                <selectize id="vendor_id" placeholder="Vendor" config='{valueField: "id", labelField: "text", maxItems: 1}' options='vendors' ng-model="searchData.vendor_id"></selectize>
                                            </div>
                                            showOrderItemsByProductId(orderItem.product.id)        </div>
                                    </div>
                                    <div class="row form-group" style="margin-top: -15px;">
                                        <div class="col-md-6 form-group form-group-sm">
                                            <label for="product_id" class="col-md-4 control-label"><b>Product ID</b></label>
                                            <div class="col-md-8">
                                                <input ng-model="searchData.product_id" type="text" class="form-control"
                                                       id="product_id" placeholder="Product ID">
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-group form-group-sm">
                                            <label for="date_from" class="col-md-4 control-label"><b>Creation Date</b></label>
                                            <div class="col-md-4">
                                                <span class="dropdown">
                                                    <a class="dropdown-toggle" id="date_from" role="button" data-toggle="dropdown" data-target="#" href="#">
                                                        <input name="date_from" id="date_from" placeholder="From" type="text" class="form-control" ng-model="searchData.date_from">
                                                    </a>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                        <datetimepicker data-ng-model="searchData.date_from" data-datetimepicker-config="{ dropdownSelector: '#date_from', modelType: 'YYYY-MM-DD', minView: 'day' }"/>
                                                    </ul>
                                                </span>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="dropdown">
                                                    <a class="dropdown-toggle" id="date_to" role="button" data-toggle="dropdown" data-target="#" href="#">
                                                        <input name="date_to" id="date_to" placeholder="To" type="text" class="form-control" data-ng-model="searchData.date_to">
                                                    </a>
                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                        <datetimepicker data-ng-model="searchData.date_to" data-datetimepicker-config="{ dropdownSelector: '#date_to', modelType: 'YYYY-MM-DD', minView: 'day' }"/>
                                                    </ul>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group" style="margin-top: -10px;">
                                        <div class="col-md-6 form-group form-group-sm">
                                            <label for="state" class="col-md-4 control-label"><b>State</b></label>
                                            <div class="col-md-8">
                                                <selectize id="state" placeholder="State" config='{valueField: "abbr", labelField: "name", maxItems: 1}' options='states' ng-model="searchData.state"></selectize>
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-group form-group-sm">
                                            <label for="po_number" class="col-md-4 control-label"><b>Stage</b></label>
                                            <div class="col-md-8">
                                                <select ng-model="searchData.stage_id" class="form-control" ng-change="search()">
                                                    <option ng-repeat="stage in usedStages" ng-value="{{stage.stage_id}}">{{stage.name}} {{stage.orders_count != undefined ? '('+stage.orders_count+')' : ''}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4" ng-if="pagination.lastPage > 1">
                                    <span ng-click="changePage(searchData.stage_id, pagination.currentPage - 1)" ng-if="pagination.currentPage > 1" style="color: #337ab7"><<</span>
                                    <select ng-change="changePage(searchData.stage_id, pagination.currentPage)" ng-model="pagination.currentPage"
                                            ng-options="n as 'Page '+n for n in range">
                                    </select>
                                    of {{pagination.lastPage}} <span ng-click="changePage(searchData.stage_id, pagination.currentPage + 1)" ng-if="pagination.currentPage < pagination.lastPage"  style="color: #337ab7">>></span>
                                </div>
                                <div class="col-md-4" ng-if="pagination.lastPage > 1">
                                    <span>Go to page: </span>
                                    <input class="" type="text" size="5" ng-model="searchData.page" value="{{pagination.currentPage}}" placeholder="{{pagination.currentPage}}">
                                    <input class="btn btn-default" type="button" ng-click="changePage(searchData.stage_id, searchData.page)" value="Submit">
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-default btn-sm">Search</button>
                                    <button ng-click="resetSearch()" type="reset"
                                            class="btn btn-default btn-sm">Reset
                                    </button>
                                    <button ng-click="searchData = {}" class="btn btn-default btn-sm" type="reset">Clear</button>
                                </div>
                            </div>
                        </form>

                        <div class="container-fluid">
                            <ul>
                                <li ng-click="searchByStageId(filteredStage.stage_id)" ng-repeat="filteredStage in filteredStages"><b>{{filteredStage.name}} ({{filteredStage.orders_count}})</b></li>
                            </ul>
                            <div class="row order-list-container">
                                <table class="table order-table">
                                    <thead>
                                    <tr>
                                        <th>Order Id</th>
                                        <th>Date</th>
                                        <th>Customer</th>
                                    </tr>
                                    </thead>
                                </table>
                                <div class="col-md-12 order-container" ng-repeat="order in orders">
                                    <table class="table order-table">
                                        <tbody>
                                        <tr>
                                            <td><a href="/admin/order_sample/{{order.order_id}}/edit" title="Edit order" >{{order.order_id}}</a></td>
                                            <td>{{order.created_at|date:'short'}}</td>
                                            <td><a href="/admin/customer/{{order.user_id}}/edit" >{{order.first_name}} {{order.last_name}} ({{order.user_id}})</a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table ng-hide="hideOrder[order.order_id]" class="table table-condensed table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Ref #</th>
                                                <th>Item #</th>
                                                <th>Item name</th>
                                                <th>Details</th>
                                                <th>In Hand Date</th>
                                                <th>P.Stage</th>
                                                <th>Actions</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-if="showOrderItemsByProductId(orderItem.product.id)" ng-repeat="orderItem in order.order_items" ng-style="orderItem.is_important ? {'background-color':'#ff8282'} : ''">
                                                <td><a href="/admin/order_sample_item/{{orderItem.id}}/edit">{{order.order_id}}S{{orderItem.id}}</a></td>
                                                <td>
                                                    <a href="/admin/product/{{orderItem.product.id}}/edit">{{orderItem.product.id}}</a>
                                                </td>
                                                <td>{{orderItem.product.title}}</td>
                                                <td>
                                                    <div ng-show="orderItem">
                                                        <div ng-show="orderItem.colors.length">
                                                            <p style="margin: 0"><b>Colors:</b></p>
                                                            <ul ng-repeat="color in orderItem.colors" class="list-unstyled">
                                                                <li>{{color.color_group_name}}: {{color.color_name.name}}</li>
                                                            </ul>
                                                        </div>
                                                        <div ng-show="orderItem.cart_item.product_options.length">
                                                            <p style="margin: 0"><b>Options:</b></p>
                                                            <ul ng-repeat="option in orderItem.cart_item.product_options" class="list-unstyled">
                                                                <li>{{option.name}}:
                                                                    <span ng-repeat="sub_option in option.product_sub_options">{{sub_option.name}}<span ng-hide="$last">, </span></span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-info">{{orderItem.received_date}}</td>
                                                <td>{{orderItem.stage.name}}</td>
                                                <td>
                                                    <i ng-click="showOrderStages(orderItem)" class="fa fa-database" aria-hidden="true" title="Manage Production Stages"></i>
                                                    <i ng-click="showNotesForm(orderItem)" class="fa fa-folder-open" aria-hidden="true" title="Manage Order Notes & File Cabinet"></i>
                                                </td>
                                                <td>
                                                    <span ng-show="orderItem.not_paid" class="bg-primary status-block">$</span>
                                                    <span ng-show="orderItem.check_notes" class="bg-danger status-block">N</span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row" ng-if="pagination.lastPage > 1">
                            <div class="col-md-9">
                                <span ng-click="changePage(searchData.stage_id, pagination.currentPage - 1)" ng-if="pagination.currentPage > 1" style="color: #337ab7"><<</span>
                                <select ng-change="changePage(searchData.stage_id, pagination.currentPage)" ng-model="pagination.currentPage"
                                        ng-options="n as 'Page '+n for n in range">
                                </select>
                                of {{pagination.lastPage}} <span ng-click="changePage(searchData.stage_id, pagination.currentPage + 1)" ng-if="pagination.currentPage < pagination.lastPage"  style="color: #337ab7">>></span>
                            </div>
                            <div class="col-md-3">
                                <span>Go to page: </span>
                                <input class="" type="text" size="5" ng-model="searchData.page" value="{{pagination.currentPage}}" placeholder="{{pagination.currentPage}}">
                                <input class="btn btn-default" type="button" ng-click="changePage(searchData.stage_id, searchData.page)" value="Submit">
                            </div>
                        </div>

                        <div class="center-text" ng-show="orders.length == 0"><h4>Search result is empty, try again with new parameters :(</h4></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
