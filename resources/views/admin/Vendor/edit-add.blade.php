@extends('admin.base.edit-add')

@section('angular_controller', 'VendorController')

@section('title')
    Vendor #<% $vendor->id %> (<% $vendor->name %>)
@stop

@section('panel_content')
    <script>
        vendor = {!! $vendor->toJson() !!};
    </script>
    <form name="vendorForm" ng-submit="saveVendor()" novalidate class="form-horizontal">
        @include('admin.blocks.text-field', ['model' => 'vendor', 'field' => 'name'])
        @include('admin.blocks.ui-select-field', ['model' => 'vendor', 'field' => 'move_after', 'label' => 'Position After', 'options' => 'vendors'])
        @include('admin.blocks.text-field', ['model' => 'vendor', 'field' => 'phone'])
        @include('admin.blocks.text-field', ['model' => 'vendor', 'field' => 'fax'])
        @include('admin.blocks.text-field', ['model' => 'vendor', 'field' => 'email'])
        @include('admin.blocks.ui-select-field', ['model' => 'vendor', 'field' => 'country_id', 'label' => 'County', 'options' => 'countries'])
        @include('admin.blocks.ui-select-field', ['model' => 'vendor', 'field' => 'state_id', 'label' => 'State', 'options' => 'states'])
        @include('admin.blocks.text-field', ['model' => 'vendor', 'field' => 'city'])
        @include('admin.blocks.text-field', ['model' => 'vendor', 'field' => 'zip_code', 'label' => 'ZIP'])
        @include('admin.blocks.text-field', ['model' => 'vendor', 'field' => 'address_1', 'label' => 'Address 1'])
        @include('admin.blocks.text-field', ['model' => 'vendor', 'field' => 'address_2', 'label' => 'Address 2'])
        @include('admin.blocks.text-field', ['model' => 'vendor', 'field' => 'site ', 'label' => 'SAGE Web Address'])
        @include('admin.blocks.text-field', ['model' => 'vendor', 'field' => 'supplier_id ', 'label' => 'SAGE ID'])
        @include('admin.blocks.ui-select-field', ['model' => 'vendor', 'field' => 'payment_information_template_id', 'label' => 'SAGE Pricing Template', 'options' => 'paymentInformationTemplates'])
        @include('admin.blocks.ui-multiselect-field', ['model' => 'vendor', 'field' => 'state_ids', 'label' => 'Tax States', 'options' => 'states'])

        <button type="submit"  class="btn btn-primary">Save</button>
    </form>
@stop