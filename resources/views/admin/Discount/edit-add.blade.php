@extends('admin.layout')

@section('css')
    <meta name="csrf-token" content="<% csrf_token() %>">
    <link rel="stylesheet" href="/admin-assets/css/assets.min.css">
@stop

@section('page_header')
    <h1 class="page-title">
        @if(isset($discount)) Edit Discount <% $discount->name %> @else New Discount @endif
    </h1>
@stop

@section('content')
    <?php
    $formLink = null;
    ?>
    <div class="page-content container-fluid" ng-app="app" ng-controller="DiscountController">
        <div ng-hide="hideLoader" class="voyager-loader">
            <img src="<% config('voyager.assets_path') %>/images/logo-icon.png" alt="Voyager Loader">
        </div>
        @if(isset($discount))
            <script>
                var discount = {!! $discount->toJson() !!}
            </script>
        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form name="discountForm" ng-submit="saveDiscount()" novalidate>
                            <div class="col-md-12 form-block-container">
                                <div class="row">
                                    <div class="col-md-6 form-horizontal">
                                        <div class="form-group"
                                             ng-class="{'has-error': discountForm.name.$error.serverError}">
                                            <label class="control-label col-sm-4" for="name">Name</label>
                                            <div class="col-sm-8">
                                                <input ng-model="discount.name" id="name"
                                                       name="name" class="form-control"
                                                       placeholder="Name" type="text">
                                            </div>
                                            <span class="help-block"
                                                  ng-show="discountForm.name.$error.serverMessage">{{discountForm.name.$error.serverMessage}}</span>
                                        </div>
                                        <div class="form-group"
                                             ng-class="{'has-error': discountForm.code.$error.serverError}">
                                            <label class="control-label col-sm-4" for="code">Code</label>
                                            <div class="col-sm-8">
                                                <input ng-model="discount.code" id="name"
                                                       name="name" class="form-control"
                                                       placeholder="Code" type="text">
                                            </div>
                                            <span class="help-block"
                                                  ng-show="discountForm.code.$error.serverMessage">{{discountForm.code.$error.serverMessage}}</span>
                                        </div>
                                        <div class="form-group"
                                             ng-class="{'has-error': discountForm.active.$error.serverError}">
                                            <label class="control-label col-sm-4" for="active">Active</label>
                                            <div class="col-sm-8">
                                                <input ng-true-value="1" ng-false-value="0"
                                                       type="checkbox" ng-model="discount.active" id="active"
                                                       name="active">
                                            </div>
                                            <span class="help-block"
                                                  ng-show="discountForm.active.$error.serverMessage">{{discountForm.active.$error.serverMessage}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 form-block-container">
                                <h4>Condition</h4>
                                <div class="row">
                                    <div class="col-md-12 form-horizontal">
                                        <div class="form-group">
                                            <div ng-if="selectedConditions.length == 0" class="col-sm-2">
                                        <span uib-dropdown on-toggle="toggled(open)">
                                            <a class="pull-right btn btn-primary btn-xs" data-nodrag
                                               uib-dropdown-toggle>
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </a>
                                            <ul class="dropdown-menu" uib-dropdown-menu
                                                aria-labelledby="simple-dropdown">
                                                <li ng-repeat="choice in discountConditions">
                                                  <a ng-click="addFirstCondition(choice)">{{choice.title}}</a>
                                                </li>
                                          </ul>
                                        </span>
                                            </div>
                                            <div ng-if="selectedConditions.length > 0" class="col-sm-8">
                                                <div ui-tree class="tree-root">
                                                    <ol ui-tree-nodes ng-model="selectedConditions">
                                                        <li ng-repeat="node in selectedConditions" ui-tree-node
                                                            ng-include="'condition_node_renderer.html'"></li>
                                                    </ol>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 form-block-container">
                                <h4>Discounts</h4>
                                <div class="row">
                                    <div class="col-md-12 form-horizontal">
                                        <div class="row">
                                            <div class="col-sm-2">
                                        <span uib-dropdown on-toggle="toggled(open)" ng-if="selectedCalc.length === 0">
                                            <a class="pull-right btn btn-primary btn-xs" data-nodrag
                                               uib-dropdown-toggle>
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </a>
                                            <ul class="dropdown-menu" uib-dropdown-menu
                                                aria-labelledby="simple-dropdown">
                                                <li ng-repeat="choice in discountCalc">
                                                  <a ng-click="addCalc(choice, null)">{{choice.title}}</a>
                                                </li>
                                          </ul>
                                        </span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div ng-if="selectedCalc.length > 0" class="col-sm-8">
                                                <div ui-tree class="tree-root">
                                                    <ol ui-tree-nodes ng-model="selectedCalc">
                                                        <li ng-repeat="node in selectedCalc" ui-tree-node
                                                            ng-include="'calc_node_renderer.html'"></li>
                                                    </ol>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <button type="submit" ng-click="saveDiscount()" class="btn btn-primary">Save</button>
                    </div>

                    <!-- Nested node template -->
                    <script type="text/ng-template" id="condition_node_renderer.html">
                        <div class="tree-node tree-node-content">
                            <div class="row">
                                <a class="pull-left btn btn-success btn-xs" data-nodrag ng-click="toggle(this)">
                                <span class="glyphicon"
                                      ng-class="{
                                          'glyphicon-chevron-right': collapsed,
                                          'glyphicon-chevron-down': !collapsed
                                        }">
                                </span>
                                </a>
                                <span class="col-md-9 tree-node-title">{{getConditionNodeTitle(node)}}</span>
                                <a class="pull-right btn btn-danger btn-xs" data-nodrag ng-click="remove(this)"><span
                                            class="glyphicon glyphicon-remove"></span></a>
                                <span class="pull-right" ng-show="node.discount_condition.allow_nested" uib-dropdown
                                      on-toggle="toggled(open)">
                                <a class="btn btn-primary btn-xs" data-nodrag uib-dropdown-toggle>
                                    <span class="glyphicon glyphicon-plus"></span>
                                </a>
                                <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="simple-dropdown">
                                    <li ng-repeat="choice in discountConditions">
                                      <a ng-click="addCondition(choice, this)">{{choice.title}}</a>
                                    </li>
                              </ul>
                            </span>
                            </div>

                            <div ng-repeat="param in node.discount_condition.parameters" ng-class="{hidden: collapsed}">
                                <div class="form-group">
                                    <label class="control-label col-sm-6" for="param_{{$index}}">{{param.label}}
                                        :</label>
                                    <div class="col-sm-6">
                                        <span ng-if="param.type == 'text'">
                                            <input id="param_{{$index}}" ng-model="node.params[param.name]" type="text"
                                                   class="form-control"/>
                                        </span>
                                        <span ng-if="param.type == 'model'">
                                            <ui-select ng-model="node.params[param.name]">
                                              <ui-select-match placeholder="Search for Products">{{$select.selected.id}}
                                                  - {{$select.selected.name}}</ui-select-match>
                                              <ui-select-choices refresh="searchMedia($select)" refresh-delay="50"
                                                                 repeat="item.id as item in searchRes">
                                                <span ng-bind="item.id"></span> - <span ng-bind="item.name"></span>
                                              </ui-select-choices>
                                            </ui-select>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <ol ui-tree-nodes="" ng-model="node.nodes" ng-class="{hidden: collapsed}">
                            <li ng-repeat="node in node.nodes" ui-tree-node ng-include="'condition_node_renderer.html'">
                            </li>
                        </ol>
                    </script>

                    <!-- Nested node template -->
                    <script type="text/ng-template" id="calc_node_renderer.html">
                        <div class="tree-node tree-node-content">
                            <div class="row">
                                <a class="pull-left btn btn-success btn-xs" data-nodrag ng-click="toggle(this)">
                                <span class="glyphicon"
                                      ng-class="{
                                          'glyphicon-chevron-right': collapsed,
                                          'glyphicon-chevron-down': !collapsed
                                        }">
                                </span>
                                </a>
                                <span class="col-md-9 tree-node-title">{{getCalcNodeTitle(node)}}</span>
                                <a class="pull-right btn btn-danger btn-xs" data-nodrag ng-click="remove(this)"><span
                                            class="glyphicon glyphicon-remove"></span></a>
                                </span>
                            </div>

                            <div ng-repeat="param in node.discount_calc.parameters" ng-class="{hidden: collapsed}">
                                <div class="form-group">
                                    <label class="control-label col-sm-6" for="param_{{$index}}">{{param.label}}
                                        :</label>
                                    <div class="col-sm-6">
                                        <span ng-if="param.type == 'text'">
                                            <input id="param_{{$index}}" ng-model="node.params[param.name]" type="text"
                                                   class="form-control"/>
                                        </span>
                                        <span ng-if="param.type == 'model'">
                                            <ui-select ng-model="node.params[param.name]">
                                              <ui-select-match placeholder="Search for Products">{{$select.selected.id}}
                                                  - {{$select.selected.name}}</ui-select-match>
                                              <ui-select-choices refresh="searchMedia($select)" refresh-delay="50"
                                                                 repeat="item.id as item in searchRes">
                                                <span ng-bind="item.id"></span> - <span ng-bind="item.name"></span>
                                              </ui-select-choices>
                                            </ui-select>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <ol ui-tree-nodes="" ng-model="node.nodes" ng-class="{hidden: collapsed}">
                            <li ng-repeat="node in node.nodes" ui-tree-node ng-include="'calc_node_renderer.html'">
                            </li>
                        </ol>
                    </script>

                    <div class="row">
                        <div class="col-md-12">
                            <span uib-dropdown on-toggle="toggled(open)">
                                <ul class="dropdown-menu" uib-dropdown-menu aria-labelledby="simple-dropdown">
                                    <li ng-repeat="choice in discountConditions">
                                      <a ng-click="addCondition(choice, this)">{{choice.title}}</a>
                                    </li>
                              </ul>
                            </span>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        @stop

        @section('javascript')
            <script>
                $('document').ready(function () {
                    $('.toggleswitch').bootstrapToggle();
                });
            </script>
            <script src="<% config('voyager.assets_path') %>/lib/js/tinymce/tinymce.min.js"></script>
            <script src="<% config('voyager.assets_path') %>/js/voyager_tinymce.js"></script>
@stop
