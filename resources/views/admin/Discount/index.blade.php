@extends('admin.layout')

@section('css')
    <meta name="csrf-token" content="<% csrf_token() %>">
    <link rel="stylesheet" href="/admin-assets/css/assets.min.css">
@stop

@section('content')
    <div class="page-content container-fluid" ng-app="app" ng-controller="DiscountListController">
        <div ng-hide="hideLoader" class="voyager-loader">
            <img src="<% config('voyager.assets_path') %>/images/logo-icon.png" alt="Voyager Loader">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">

                        <ul data-as-sortable="dragControlListeners" data-ng-model="discounts">
                            <li data-ng-repeat="item in discounts" data-as-sortable-item>
                                <span data-as-sortable-item-handle>{{item.name}}</span>
                                <a href="/admin/discount/{{item.id}}/edit" class="btn btn-default btn-xs" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                <span ng-click="deleteDiscount(item.id)" class="btn btn-xs btn-danger">
                                    <i class="voyager-trash"></i>
                                </span>
                            </li>
                        </ul>
                        <a href="/admin/discount/create" class="btn btn-success"s><i class="voyager-plus"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
