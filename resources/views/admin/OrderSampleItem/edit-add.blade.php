@extends('admin.layout')

@section('css')
    <meta name="csrf-token" content="<% csrf_token() %>">
    <link rel="stylesheet" href="/admin-assets/css/assets.min.css">
@stop

@section('content')
    <?php
    $formLink = null;
    ?>
    <div class="page-content container-fluid" ng-app="app" ng-controller="OrdersSampleListController">
        <div ng-hide="hideLoader" class="voyager-loader">
            <img src="<% config('voyager.assets_path') %>/images/logo-icon.png" alt="Voyager Loader">
        </div>
        <script>
            console.log({!! $orderItem->toJson() !!})
            orderItem = {!! $orderItem->toJson() !!};
            trackingUserName = '<% $orderItem->orderSample->user->name ?? null %>';
            authUserName = '<% Auth::user()->name %>';
        </script>
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">

                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <%$orderItem->order_sample_id%>S<%$orderItem->id%>
                        </h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                        <div class="panel-footer">
                            <div class="table-responsive">
                                <table class="table table-condensed table-bordered table-hover table-info">
                                    <tbody>
                                        <tr>
                                            <th class="info" scope="row">Reference #</th>
                                            <td><%$orderItem->order_sample_id%>S<%$orderItem->id%></td>
                                            <th class="info">In-Hand Date</th>
                                            <td>ASAP</td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">Order Date</th>
                                            <td>ASAP</td>
                                            <th class="info">Order Status</th>
                                            <td>
                                                <%$orderItem->stage->name ?? 'Without Stage'%>
                                                <a class="btn btn-default btn-xs" ng-click="showOrderStages(<% $orderItem %>)" role="button">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">Customer ID</th>
                                            <td>
                                                <a href="/admin/customer/<% $orderItem->orderSample->user->id ?? 'null' %>/edit"><% $orderItem->orderSample->user->id ?? 'null' %></a>
                                            </td>
                                            <th class="info">Vendor</th>
                                            <td>
                                                <span editable-select="orderItem.vendor_id" e-form="vendorForm"  e-ng-options="s.id as s.text for s in vendors" onbeforesave="updateVendor($data)">
                                                   <% $orderItem->vendor->name or 'empty' %>
                                                </span>
                                                <a ng-click="vendorForm.$show()" ng-hide="vendorForm.$visible" class="btn btn-default btn-xs" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">Contact Name</th>
                                            <td><%$orderItem->orderSample->user->name%></td>
                                            <th class="info">Tracking #</th>
                                            <td>
                                                <% $orderItem->orderSampleTrackingInformation()->orderBy('id', 'DESC')->first()->tracking_number or 'empty' %>
                                                <a class="btn btn-default btn-xs" role="button" ng-click="showTrackingInformationForm()">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">Contact Email</th>
                                            <td><a href="mailto:<%$orderItem->orderSample->user->email%>"><%$orderItem->orderSample->user->email%></a></td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">Customer Ph</th>
                                            <td><%$orderItem->orderSample->user->contact_telephone%></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>Shipping Address
                                        <a class="btn btn-default btn-xs" ng-click="showShippingAddressForm(<% $orderItem %>)" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    </h5>
                                    <div class="table-responsive">
                                        <div><%$orderItem->shipping_first_name%> <%$orderItem->shipping_middle_name%> <%$orderItem->shipping_last_name%></div>
                                        <div><%$orderItem->shipping_company_name%></div>
                                        <div><%$orderItem->shipping_address_line_1%> <%$orderItem->shipping_address_line_2%></div>
                                        <div>
                                            <%$orderItem->shipping_city%>
                                            @if($orderItem->shipping_state), @endif
                                            <%$orderItem->shipping_state%> <%$orderItem->shipping_zip%>
                                        </div>
                                        <div><%$orderItem->shipping_day_telephone%></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h5>Billing Address
                                        <a class="btn btn-default btn-xs" role="button" ng-click="showBillingAddressForm(<% $orderItem %>)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    </h5>
                                    <div class="table-responsive">
                                        <div><%$orderItem->billing_first_name%> <%$orderItem->billing_middle_name%> <%$orderItem->billing_last_name%></div>
                                        <div><%$orderItem->billing_company_name%></div>
                                        <div><%$orderItem->billing_address_line_1%> <%$orderItem->billing_address_line_2%></div>
                                        <div>
                                            <%$orderItem->billing_city%>
                                            @if($orderItem->billing_state), @endif
                                            <%$orderItem->billing_state%> <%$orderItem->billing_zip%>
                                        </div>
                                        <div><%$orderItem->billing_day_telephone%></div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>Order Details</h5>
                                    <div class="table-responsive">
                                        <table class="table table-condensed table-bordered table-hover table-info">
                                            <tbody>
                                            <tr>
                                                <th class="col-md-2 info" scope="row">Delivery Date</th>
                                                <td class="col-md-10">{{orderItem.received_date || 'ASAP'}}</td>
                                            </tr>
                                            @if(!empty($orderItem->cartItem))
                                            <tr>
                                                <th class="col-md-2 info" scope="row">Colors</th>
                                                <td class="col-md-10">
                                                    @forelse($orderItem->cartItem->cartItemColors as $color)
                                                        <%$color->color_group_name  or 'Blank Goods'%>: <%$color->color_name%><br>
                                                    @empty
                                                        none
                                                    @endforelse
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="col-md-2 info" scope="row">Options</th>
                                                <td class="col-md-10">
                                                    @forelse($orderItem->cartItem->cartItemProductOptions as $option)
                                                        <li><%$option->name%></li>
                                                    @empty
                                                        none
                                                    @endforelse
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="col-md-2 info" scope="row">Imprints</th>
                                                <td class="col-md-10">
                                                    @forelse($orderItem->cartItem->cartItemImprints as $imprint)
                                                        <%$imprint->imprint_name or 'Blank Goods'%>:
                                                            @forelse($imprint->cartItemImprintColors as $imprintColor)
                                                                <span><%$imprintColor->name%></span>
                                                            @empty

                                                            @endforelse
                                                        <br>
                                                    @empty
                                                        none
                                                    @endforelse
                                                </td>
                                            </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>

                                    <h5>Pricing $
                                    </h5>
                                    <div class="table-responsive">
                                        <table class="table table-condensed table-bordered table-hover table-info">
                                            <tbody>
                                            <tr>
                                                <th class="col-md-2 info" scope="row">Quantity</th>
                                                <td class="col-md-10"><% $orderItem->quantity %></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    @if(!empty($orderItem->orderSample->shipping_method))

                                    <h5>Extra</h5>
                                    <div class="table-responsive">
                                        <table class="table table-condensed table-bordered table-hover table-info">
                                            <tbody>
                                            <tr>
                                                <th class="col-md-2 info" scope="row">Own account data</th>
                                                <td class="col-md-10">Ship via: <% $orderItem->orderSample->own_shipping_system %>, Account #: <% $orderItem->orderSample->own_account_number %>, Method: <% $orderItem->orderSample->own_shipping_type %></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    @endif


                                </div>
                                <div class="col-md-6">
                                    <h5>Product Info</h5>
                                    <div class="table-responsive">
                                        <table class="table table-condensed table-bordered table-hover table-info">
                                            <tbody>
                                                <tr>
                                                    <th class="info" scope="row">Preview</th>
                                                    <td>
                                                        <a href="<%$orderItem->product->image_src or '/img/image-not-available.jpg'%>">
                                                            <img class="img-thumbnail admin-thumbnail" src="<%$orderItem->product->image_src or '/img/image-not-available.jpg'%>">
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th class="col-md-2 info" scope="row">Id</th>
                                                    <td class="col-md-10">Item <a href="/admin/product/<%$orderItem->product->id%>/edit">#<%$orderItem->product->id%></a></td>
                                                </tr>
                                                <tr>
                                                    <th class="col-md-2 info" scope="row">Name</th>
                                                    <td class="col-md-10"><%$orderItem->product->name%></td>
                                                </tr>
                                                <tr>
                                                    <th class="info" scope="row">Sage ID</th>
                                                    <td><% $orderItem->product->sage_id %></td>
                                                </tr>
                                                <tr>
                                                    <th class="info" scope="row">Vendor</th>
                                                    <td><% $orderItem->vendor->name or null %></td>
                                                </tr>
                                                <tr>
                                                    <th class="info" scope="row">SKU Vendor</th>
                                                    <td>Table cell</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <%--<button type="submit" class="btn btn-primary">Submit</button>--%>
                        </div>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="<% route('admin.upload') %>" target="form_target" method="post"
                          enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                               onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="<% $adminModel->getBaseRouteName() %>">
                        <input type="hidden" name="_token" value="<% csrf_token() %>">
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            var PRE_SELECT = <?php echo json_encode($collections); ?>

            $('.select-multiple').select2({
                minimumInputLength: 2,
                initSelection: function (element, callback) {
                    for (var x = 0; x<PRE_SELECT.length; x++) {
                        $('.select-multiple').append($("<option/>", {
                            value: PRE_SELECT[x].id,
                            text: PRE_SELECT[x].text,
                            selected: true
                        }));
                    }
                    callback(PRE_SELECT);
                },
                ajax: {
                    url: "<% $formLink %>",
                    dataType: 'json',
                    data: function (params) {
                        return {
                            query: params.term
                        };
                    },
                    results: function (data) {
                        return { results: data };
                    },
                    processResults: function (data) {
                        var answer = [];
                        for (var i = 0; i < data.length; i++ ) {
                            answer.push({
                                id: data[i].id,
                                text: data[i].name
                            });
                        }
                        return {
                            results: answer
                        };
                    },
                    cache: true
                },
            });
            $('.select-multiple').change();
        });
    </script>
    <script src="<% config('voyager.assets_path') %>/lib/js/tinymce/tinymce.min.js"></script>
    <script src="<% config('voyager.assets_path') %>/js/voyager_tinymce.js"></script>
@stop
