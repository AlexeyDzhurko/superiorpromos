@extends('admin.base.index')

@section('title', 'Categories')

@section('angular_controller', 'CategoryController')

@section('panel_content')
    <div class="page-content container-fluid">
        <div ng-hide="hideLoader" class="voyager-loader">
            <img src="{{ config('voyager.assets_path') }}/images/logo-icon.png" alt="Voyager Loader">
        </div>
        @if(isset($nestedCategories))
            <script>
                var categories = {!! json_encode($nestedCategories) !!}
            </script>
        @endif
        <div class="row">
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <div class="col-md-6">
                        <div class="form-group" style="font-size: 14px;">
                            <div class="categories-tree">
                                <!-- Nested node template -->
                                <script type="text/ng-template" id="nodes_renderer.html">
                                    <div
                                        ui-tree-handle
                                        ng-class="{
                                            'level_1' : !collapsed && node.hierarchy_level === 1,
                                            'level_2' : !collapsed && node.hierarchy_level === 2,
                                            'level_3' : !collapsed && node.hierarchy_level === 3
                                        }"
                                    >
                                        <a class="btn btn-success btn-xs" ng-if="node.children && node.children.length > 0" data-nodrag ng-click="toggle(this)"><span
                                                    class="glyphicon"
                                                    ng-class="{
                                              'glyphicon-chevron-right': collapsed,
                                              'glyphicon-chevron-down': !collapsed
                                            }"></span></a>
                                        @{{node.name}}
                                        <a data-nodrag ng-click="editCategory(node)" class="angular-ui-tree-edit-button">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </a>
                                        <a data-nodrag ng-click="deleteCategory(node)" class="angular-ui-tree-delete-button">
                                            <span class="glyphicon glyphicon-remove text-danger"></span>
                                        </a>
                                    </div>
                                    <ol ui-tree-nodes="" ng-model="node.children"  ng-class="{hidden: collapsed}">
                                        <li ng-repeat="node in node.children" ui-tree-node ng-include="'nodes_renderer.html'">
                                        </li>
                                    </ol>
                                </script>

                                <div ui-tree>
                                    <ol ui-tree-nodes="" ng-model="categories" id="tree-root">
                                        <li ng-repeat="node in categories" ui-tree-node ng-include="'nodes_renderer.html'"></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" style="position: fixed; right: 0; margin-top: -50px">
                        <div class="row">
                            <div class="col-sm-12">
                                <button ng-click="expandAll()" class="btn btn-default">Expand all</button>
                                <button ng-click="collapseAll()" class="btn btn-default">Collapse all</button>
                                <button ng-click="saveHierarchy()" class="btn btn-default">Save hierarchy</button>
                                <button ng-click="createNewCategory()" class="btn btn-default">Create new</button>
                            </div>
                        </div>
                        <form name="addressForm" class="form-horizontal" ng-submit="save()" ng-if="displayCategory">
                            <div class="col-sm-12">
                                <div class="form-group form-group-sm col-sm-6">
                                    <label for="name" class="col-sm-2 control-label"><b>Name</b></label>
                                    <div class="col-sm-10">
                                        <input ng-model="category.name" type="text" class="form-control" id="name" placeholder="Name" ng-value="category.name">
                                    </div>
                                    <strong class="text-danger col-sm-10 pull-right" ng-show="errors.name"> The name field is required.</strong>
                                </div>
                                <div class="form-group form-group-sm col-sm-6">
                                    <label for="slug" class="col-sm-2 control-label"><b>Slug</b></label>
                                    <div class="col-sm-10">
                                        <input ng-model="category.slug" type="text" class="form-control" id="slug" placeholder="Slug" ng-value="category.slug">
                                    </div>
                                    <strong class="text-danger" ng-show="errors.slug">The slug field is required.</strong>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-group-sm col-sm-6">
                                    <label for="meta_title" class="col-sm-2 control-label"><b>Meta title</b></label>
                                    <div class="col-sm-10">
                                        <input ng-model="category.meta_title" type="text" class="form-control" id="meta_title" placeholder="Meta title" ng-value="category.meta_title">
                                    </div>
                                    <strong class="text-danger" ng-show="errors.meta_title">The meta title field is required.</strong>
                                </div>
                                <div class="form-group form-group-sm col-sm-6">
                                    <label for="meta_description" class="col-sm-2 control-label"><b>Meta description</b></label>
                                    <div class="col-sm-10">
                                        <input ng-model="category.meta_description" type="text" class="form-control" id="meta_description" placeholder="Meta description" ng-value="category.meta_description">
                                    </div>
                                    <strong class="text-danger" ng-show="errors.meta_description">The meta description field is required.</strong>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-group-sm col-sm-6">
                                    <label for="keywords" class="col-sm-2 control-label"><b>Keywords</b></label>
                                    <div class="col-sm-10">
                                        <input ng-model="category.keywords" type="text" class="form-control" id="keywords" placeholder="Keywords" ng-value="category.keywords">
                                    </div>
                                    <strong class="text-danger" ng-show="errors.keywords">The keywords field is required.</strong>
                                </div>
                                <div class="form-group form-group-sm col-sm-6">
                                    <label for="title" class="col-sm-2 control-label"><b>Title</b></label>
                                    <div class="col-sm-10">
                                        <input ng-model="category.title" type="text" class="form-control" id="title" placeholder="Title" ng-value="category.title">
                                    </div>
                                    <strong class="text-danger" ng-show="errors.title">The title field is required.</strong>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-group-sm col-sm-6">
                                    <label for="description" class="col-sm-2 control-label"><b>Description</b></label>
                                    <div class="col-sm-10">
                                        <input ng-model="category.description" type="text" class="form-control" id="description" placeholder="Description" ng-value="category.description">
                                    </div>
                                    <strong class="text-danger" ng-show="errors.description">The description field is required.</strong>
                                </div>
                                <div class="form-group form-group-sm col-sm-6">
                                    <label for="footer_text" class="col-sm-2 control-label"><b>Footer text</b></label>
                                    <div class="col-sm-10">
                                        <input ng-model="category.footer_text" type="text" class="form-control" id="footer_text" placeholder="Footer text" ng-value="category.footer_text">
                                    </div>
                                    <strong class="text-danger" ng-show="errors.footer_text">The footer text field is required.</strong>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-group-sm col-sm-6">
                                    <label for="froogle_description" class="col-sm-2 control-label"><b>Froogle description</b></label>
                                    <div class="col-sm-10">
                                        <input ng-model="category.froogle_description" type="text" class="form-control" id="froogle_description" placeholder="Froogle description" ng-value="category.froogle_description">
                                    </div>
                                    <strong class="text-danger" ng-show="errors.froogle_description">The froogle description field is required.</strong>
                                </div>
                                <div class="form-group form-group-sm col-sm-6">
                                    <label for="parent_id" class="col-sm-2 control-label"><b>Parent</b></label>
                                    <div class="col-sm-10">
                                        <selectize id="parent_id" placeholder="Parent Category" config='{valueField: "id", labelField: "text", maxItems: 1}' options='categoriesList' ng-model="category.parent_id"></selectize>
                                    </div>
                                    <strong class="text-danger" ng-show="errors.parent_id">The parent id field is required.</strong>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-group-sm col-sm-6">
                                    <label for="active" class="col-sm-2 control-label"><b>Active</b></label>
                                    <div class="col-sm-10">
                                        <input id="active" type="checkbox" ng-true-value="1" ng-false-value="0" ng-model="category.active">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-group-sm col-sm-6">
                                    <label for="is_quick_link" class="col-sm-5 control-label" ng-class="{'text-muted': category.is_popular }"><b>Is quick link</b></label>
                                    <div class="col-sm-7">
                                        <input id="is_quick_link" type="checkbox" ng-true-value="1" ng-false-value="0" ng-model="category.is_quick_link" ng-disabled="category.is_popular">
                                    </div>
                                </div>
                                <div class="form-group form-group-sm col-sm-6">
                                    <label for="is_popular" class="col-sm-5 control-label" ng-class="{'text-muted': category.is_quick_link }"><b>Is popular category</b></label>
                                    <div class="col-sm-7">
                                        <input id="is_popular" type="checkbox" ng-true-value="1" ng-false-value="0" ng-model="category.is_popular" ng-disabled="category.is_quick_link">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group col-sm-12 no-padding form-group-sm" ng-if="category.is_quick_link || category.is_popular">
                                <label class="col-sm-2 control-label">Image</label>
                                <div class="col-sm-10 drop-box" style="width: 100px; height: 100px" ng-model="category.image" name="item.icon"
                                    ngf-drag-over-class="'dragover'" ngf-multiple="false" ngf-select="upload($file)">
                                    Drop file to upload
                                </div>
                                <img ngf-thumbnail="item.icon" class="img-thumbnail admin-thumbnail" >
                                <img ng-if="uploadedImageFile" class="img-thumbnail admin-thumbnail" ngf-thumbnail="uploadedImageFile || '/thumb.jpg'">
                                <img ng-if="(category.image_src != null) && !uploadedImageFile" class="img-thumbnail admin-thumbnail" ng-src="/@{{ category.image_src }}"/>
                                <strong class="text-danger" ng-show="errors.image">The image is required.</strong>
                            </div>
                            <div class="form-group form-group-sm">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default">Save</button>
                                    <span ng-click="hideCategory()" class="btn btn-default">Close</span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
