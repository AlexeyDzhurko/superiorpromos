@extends('admin.layout')

@section('css')
    <meta name="csrf-token" content="<% csrf_token() %>">
    <link rel="stylesheet" href="/admin-assets/css/assets.min.css">
@stop

@section('page_header')
    <h1 class="page-title">
        Order #<% $order->id %>
    </h1>
@stop

@section('content')
    <?php
    $formLink = null;
    ?>
    <div class="page-content container-fluid" ng-app="app" ng-controller="OrderController">
        <div ng-hide="hideLoader" class="voyager-loader">
            <img src="<% config('voyager.assets_path') %>/images/logo-icon.png" alt="Voyager Loader">
        </div>
        <script>
            order = {!! $order->toJson() !!};
        </script>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="panel-footer">

                        <h5>Order Data</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered table-hover table-info">
                                        <tbody>
                                        <tr>
                                            <th class="info" scope="row">Order Date</th>
                                            <td><% $order->created_at %></td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">Customer</th>
                                            <td><% $order->billing_first_name %> <% $order->billing_middle_name %> <% $order->billing_last_name %></td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">Customer ID</th>
                                            <td><% $order->user->id %></td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">CIM Profile ID</th>
                                            <td><% $order->user->authorize_net_id %></td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">Customer Email</th>
                                            <td><a href="mailto:<% $order->user->email %>"><% $order->user->email %></a></td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">Tax Exemption Document</th>
                                            <td>
                                                @if(!is_null($order->user->tax_exemption_certificate))
                                                    <p><a href="<% $order->user->tax_exemption_certificate %>" target="_blank">Certificate</a></p>
                                                @endif
                                                @if($order->user->provide_tax_exemption_later)
                                                    <p>I will provide certificate later</p>
                                                @endif
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered table-hover table-info">
                                        <tbody>
                                            <tr>
                                                <th class="info" scope="row">Product Total</th>
                                                <td><?= $order->price ?></td>
                                            </tr>
                                            <tr>
                                                <th class="info" scope="row">Discount Amount</th>
                                                <td><% $order->discount_amount %></td>
                                            </tr>
                                            <tr>
                                                <th class="info" scope="row">Billed Total</th>
                                                <td><% $order->total_price %></td>
                                            </tr>
                                            <tr>
                                                <th class="info" scope="row">Discount Used</th>
                                                <td>@if(is_null($order->discount_id)) None @else Yes @endif</td>
                                            </tr>
                                            @if(is_null($order->payment))
                                                <tr>
                                                    <th class="info" scope="row">Payment Type</th>
                                                    <td>Check/Money Order</td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <th class="info" scope="row">Payment Type</th>
                                                    <td>
                                                        @if(is_null($order->payment->paymentProfile))
                                                            Card
                                                        @else
                                                            <% $order->payment->paymentProfile->card_type %> <% $order->payment->paymentProfile->card_number %>
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th class="info" scope="row">CIM Payment Profile ID</th>
                                                    <td><% $order->payment->payment_profile_id %></td>
                                                </tr>
                                                <tr>
                                                    <th class="info" scope="row">Auth. Transaction #</th>
                                                    <td><% $order->payment->transaction_id %></td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <h5>Shipping Address</h5>
                                <div>
                                    <div><%$order->shipping_first_name%> <%$order->shipping_middle_name%> <%$order->shipping_last_name%></div>
                                    <div><%$order->shipping_company_name%></div>
                                    <div><%$order->shipping_address_line_1%> <%$order->shipping_address_line_2%></div>
                                    <div>
                                        <%$order->shipping_city%>
                                        @if($order->shipping_state), @endif
                                        <%$order->shipping_state%> <%$order->shipping_zip%>
                                    </div>
                                    <div><%$order->shipping_day_telephone%></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h5>Billing Address</h5>
                                <div>
                                    <div><%$order->billing_first_name%> <%$order->billing_middle_name%> <%$order->billing_last_name%></div>
                                    <div><%$order->billing_company_name%></div>
                                    <div><%$order->billing_address_line_1%> <%$order->billing_address_line_2%></div>
                                    <div>
                                        <%$order->billing_city%>
                                        @if($order->billing_state), @endif
                                        <%$order->billing_state%> <%$order->billing_zip%>
                                    </div>
                                    <div><%$order->billing_day_telephone%></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h5>Order Contents</h5>
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered table-hover table-info">
                                        <thead>
                                            <tr>
                                                <th class="info">Reference #</th>
                                                <th class="info">Product</th>
                                                <th class="info">In-Hand Date</th>
                                                <th class="info">Shipping Cost</th>
                                                <th class="info">Price</th>
                                                <th class="info">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($order->orderItems as $orderItem)
                                                <tr>
                                                    <td><%$orderItem->order_id%>S<%$orderItem->id%></td>
                                                    <td>
                                                        <%$orderItem->product->name ?? null%>
                                                        <img class="img-thumbnail admin-thumbnail" src="<%$orderItem->product->image_src or '/img/image-not-available.jpg'%>"/>
                                                        (Item #<%$orderItem->product->id ?? null%>)
                                                    </td>
                                                    <td>@if(is_null($orderItem->received_date) or $orderItem->received_date == '0000-00-00') ASAP @else <% $orderItem->received_date %> @endif</td>
                                                    @if($orderItem->estimation_shipping_price > 0)
                                                        <td>$<% $orderItem->estimation_shipping_price %></td>
                                                    @elseif(!is_null($orderItem->own_account_number))
                                                        <td>$0.00 My Own Account</td>
                                                    @else
                                                        <td>TBD</td>
                                                    @endif
                                                    <td>$<%$orderItem->price %></td>
                                                    <td><a href="<% route('order_item.edit', ['orderItem' => $orderItem->id]) %>"
                                                           class="btn-sm btn-primary pull-right edit"><i class="voyager-edit"></i> Edit</a></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h5>Shipp To Multiple Locations Notes</h5>
                                <div><%$order->multiple_location_comment ?? 'empty'%></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                            @if($canRemove)
                                <div class="float-right">
                                    <button ng-click="removeOrder(<?= $order->id ?>)"
                                            class="btn btn-danger btn-xs">
                                        Remove Order
                                    </button>
                                </div>
                            @endif
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            var PRE_SELECT = <?php echo json_encode($collections); ?>

            $('.select-multiple').select2({
                minimumInputLength: 2,
                initSelection: function (element, callback) {
                    for (var x = 0; x<PRE_SELECT.length; x++) {
                        $('.select-multiple').append($("<option/>", {
                            value: PRE_SELECT[x].id,
                            text: PRE_SELECT[x].text,
                            selected: true
                        }));
                    }
                    callback(PRE_SELECT);
                },
                ajax: {
                    url: "<% $formLink %>",
                    dataType: 'json',
                    data: function (params) {
                        return {
                            query: params.term
                        };
                    },
                    results: function (data) {
                        return { results: data };
                    },
                    processResults: function (data) {
                        var answer = [];
                        for (var i = 0; i < data.length; i++ ) {
                            answer.push({
                                id: data[i].id,
                                text: data[i].name
                            });
                        }
                        return {
                            results: answer
                        };
                    },
                    cache: true
                },
            });
            $('.select-multiple').change();
        });
    </script>
    <script src="<% config('voyager.assets_path') %>/lib/js/tinymce/tinymce.min.js"></script>
    <script src="<% config('voyager.assets_path') %>/js/voyager_tinymce.js"></script>
@stop
