<?php print '<?xml version="1.0" encoding="UTF-8" ?>'; ?>
<XMLDataStreamRequest>
    <Ver>3.2</Ver>
    <Auth>
        <AcctID>{{ $acctId }}</AcctID>
        <LoginID>{{ $loginId }}</LoginID>
        <Password>{{ $password }}</Password>
    </Auth>
    <Search>
        <ExtraReturnFields>COLORS,DESCRIPTION,NETPRICES,THEMES</ExtraReturnFields>
        <StartNum>@if($current_page == 1){{$current_page}}@else{{(($current_page - 1) * $items_per_page) + 1}}@endif</StartNum>
        <MaxRecs>{{$items_per_page}}</MaxRecs>
        <QuickSearch>{{$keywords}}</QuickSearch>
        {{--@foreach ($parameters as $key => $value)
            @if($key == 'category') <Category>{{$value}}</Category> @endif
            @if($key == 'quick_keywords') <QuickSearch>{{$value}}</QuickSearch> @endif
            @if($key == 'ad_iname') <ItemName>{{$value}}</ItemName> @endif
            @if($key == 'ad_inumber') <ItemNum>{{$value}}</ItemNum> @endif
            @if($key == 'ad_code') <SPC>{{$value}}</SPC> @endif
            @if($key == 'ad_keywords') <Keywords>{{$value}}</Keywords> @endif
            @if($key == 'ad_colors') <Colors>{{$value}}</Colors> @endif
            @if($key == 'ad_themes') <Themes>{{$value}}</Themes> @endif
            @if($key == 'ad_price_low') <PriceLow>{{$value}}</PriceLow> @endif
            @if($key == 'ad_price_high') <PriceHigh>{{$value}}</PriceHigh> @endif
            @if($key == 'ad_qty') <Qty>{{$value}}</Qty> @endif
            @if($key == 'start_num') <StartNum>{{$value}}</StartNum> @endif
            @if($key == 'rec_per_page') <MaxRecs>{{$value}}</MaxRecs> @endif
            --}}{{--@if($key == 'extra_fields') <ExtraReturnFields>COLORS,THEMES,PRODTIME,COMPANY</ExtraReturnFields> @endif--}}{{--
        @endforeach--}}
    </Search>
</XMLDataStreamRequest>