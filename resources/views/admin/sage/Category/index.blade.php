@extends('admin.layout')

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                           @foreach($dataTypeContent as $key => $category)
                               <div class="row sage-category-list">
                                   <p>{{ $key }}</p>
                                   @foreach($category as $item)
                                        <div class="pull-left">
                                            <ul>
                                                @foreach($item as $value)
                                                    <li>{{ $value['name'] }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                   @endforeach
                               </div>
                           @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
