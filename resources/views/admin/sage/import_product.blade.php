<?php print '<?xml version="1.0" encoding="UTF-8" ?>'; ?>
<XMLDataStreamRequest>
    <Ver>3.2</Ver>
    <Auth>
        <AcctID>{{ $acctId }}</AcctID>
        <LoginID>{{ $loginId }}</LoginID>
        <Password>{{ $password }}</Password>
    </Auth>
    <SupplierProductDataDump>
        <SuppID>{{ $supplier }}</SuppID>
    </SupplierProductDataDump>
</XMLDataStreamRequest>