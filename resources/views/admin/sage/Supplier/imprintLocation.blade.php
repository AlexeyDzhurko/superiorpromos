<div class="imprint-block" data-key="{{ $position }}">
    <div class="form-group">
        <label for="importImprintName">Imprint Name</label>
        <input type="text" class="form-control import-imprint-name" data-key="{{ $position }}" value="{{ $imprintName
         }}" disabled>
    </div>
    <div class="form-group">
        <label for="importImprintMaxColors">Max colors</label>
        <input type="text" class="form-control" id="importImprintMaxColors" value="4" disabled>
    </div>
    @if ($imprintName !== 'Main Imprint Location')
        <div class="checkbox">
            <label>
                <input type="checkbox" class="imprint-status" data-id="{{$position}}"> Save this
                imprint block?
            </label>
        </div>
    @else
        <div class="checkbox">
            <label>
                <input type="checkbox" hidden class="imprint-status" data-id="{{$position}}" checked> Save this
                imprint block?
            </label>
        </div>
    @endif
    <div class="form-group">
        <label for="importImprintColorGroup">Imprint Color Groups</label>
        <select class="form-control" id="importImprintColorGroup{{$position}}">
            @foreach($colorGroups as $group)
                <option value="{{ $group->id }}">{{$group->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="importImprintPriceGrid">Imprint Price grid</label>
        <table class="table table-bordered price-grid-table">
            <tr>
                <td>Quantity</td>
                <td>
                    <input type="text" class="quantity-1" placeholder="0">
                </td>
                <td>
                    <input type="text" class="quantity-2" placeholder="0">
                </td>
                <td>
                    <input type="text" class="quantity-3" placeholder="0">
                </td>
                <td>
                    <input type="text" class="quantity-4" placeholder="0">
                </td>
                <td>
                    <input type="text" class="quantity-5" placeholder="0">
                </td>
                <td>
                    <input type="text" class="quantity-6" placeholder="0">
                </td>
            </tr>
            <tr>
                <td>Location Setup Fee</td>
                <td>
                    <input type="text" class="location-setup-fee-{{ $position }}-1" placeholder="0">
                </td>
                <td>
                    <input type="text" class="location-setup-fee-{{ $position }}-2" placeholder="0">
                </td>
                <td>
                    <input type="text" class="location-setup-fee-{{ $position }}-3" placeholder="0">
                </td>
                <td>
                    <input type="text" class="location-setup-fee-{{ $position }}-4" placeholder="0">
                </td>
                <td>
                    <input type="text" class="location-setup-fee-{{ $position }}-5" placeholder="0">
                </td>
                <td>
                    <input type="text" class="location-setup-fee-{{ $position }}-6" placeholder="0">
                </td>
            </tr>
            <tr>
                <td>Additional Location Running Fee</td>
                <td>
                    <input type="text" class="additional-location-running-fee-{{ $position }}-1" placeholder="0">
                </td>
                <td>
                    <input type="text" class="additional-location-running-fee-{{ $position }}-2" placeholder="0">
                </td>
                <td>
                    <input type="text" class="additional-location-running-fee-{{ $position }}-3" placeholder="0">
                </td>
                <td>
                    <input type="text" class="additional-location-running-fee-{{ $position }}-4" placeholder="0">
                </td>
                <td>
                    <input type="text" class="additional-location-running-fee-{{ $position }}-5" placeholder="0">
                </td>
                <td>
                    <input type="text" class="additional-location-running-fee-{{ $position }}-6" placeholder="0">
                </td>
            </tr>
            <tr>
                <td>Additional Color Setup Fee</td>
                <td>
                    <input type="text" class="additional-color-setup-fee-{{ $position }}-1" placeholder="0">
                </td>
                <td>
                    <input type="text" class="additional-color-setup-fee-{{ $position }}-2" placeholder="0">
                </td>
                <td>
                    <input type="text" class="additional-color-setup-fee-{{ $position }}-3" placeholder="0">
                </td>
                <td>
                    <input type="text" class="additional-color-setup-fee-{{ $position }}-4" placeholder="0">
                </td>
                <td>
                    <input type="text" class="additional-color-setup-fee-{{ $position }}-5" placeholder="0">
                </td>
                <td>
                    <input type="text" class="additional-color-setup-fee-{{ $position }}-6" placeholder="0">
                </td>
            </tr>
            <tr>
                <td>Additional Color Running Fee</td>
                <td>
                    <input type="text" class="additional-color-running-fee-{{ $position }}-1" placeholder="0">
                </td>
                <td>
                    <input type="text" class="additional-color-running-fee-{{ $position }}-2" placeholder="0">
                </td>
                <td>
                    <input type="text" class="additional-color-running-fee-{{ $position }}-3" placeholder="0">
                </td>
                <td>
                    <input type="text" class="additional-color-running-fee-{{ $position }}-4" placeholder="0">
                </td>
                <td>
                    <input type="text" class="additional-color-running-fee-{{ $position }}-5" placeholder="0">
                </td>
                <td>
                    <input type="text" class="additional-color-running-fee-{{ $position }}-6" placeholder="0">
                </td>
            </tr>
        </table>
    </div>
</div>