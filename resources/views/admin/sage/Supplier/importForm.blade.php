<div class="import-form-block">
    <form action="sage-import-product">
        <h3>General properties</h3>
        <div class="form-group">
            <label for="importName">Product name</label>
            <input type="text" class="form-control" id="importName" placeholder="Product name">
        </div>
        <div class="form-group">
            <label for="importCategory">Main Category</label>
            <select class="form-control" id="importCategory">
                @foreach($categories as $category)
                    <option value="{{ $category['id'] }}" @if(!preg_match("/^-/i", $category["text"])) style="color:green" @endif >
                        {{ $category['text'] }}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="importCategory">Child's Category</label>
            <div class="categories-tree">
                <div>Categories Tree</div>
                @foreach($nestedCategories as $category)
                <ul class="Container">
                    <li class="Node IsRoot IsLast ExpandClosed">
                        <div class="Expand"></div>
                        <div class="Content category-content" data-category-id="{{ $category['id'] }}">{{
                        $category['name']
                        }}</div>
                        @if ($category['children'])
                            {!! View::make('admin.sage.Supplier.categoriesList', ['categories' => $category['children']]) !!}
                        @endif
                    </li>
                </ul>
                @endforeach
            </div>
        </div>
        <div class="form-group">
            <label for="importUrlPrefix">Url prefix</label>
            <input type="text" class="form-control" id="importUrlPrefix" placeholder="url prefix">
        </div>
        <div class="form-group">
            <label class="checkbox-inline">
                <input type="checkbox" id="importUseUrlPrefix"> Use url prefix
            </label>
        </div>
        <div class="form-group">
            <label for="importUrlName">URL name</label>
            <input type="text" class="form-control" id="importUrlName" placeholder="URL name">
        </div>
        <div class="form-group">
            <label for="importKeywords">Keywords</label>
            <textarea id="importKeywords" cols="30" rows="3" class="form-control" placeholder="keywords"></textarea>
        </div>
        <div class="form-group">
            <label for="importDescription">Description</label>
            <textarea id="importDescription" cols="30" rows="3" class="form-control" placeholder="description"></textarea>
        </div>
        <h3>Shipping Options </h3>
        <div class="form-group">
            <label for="importProductTimeFrom">Product time from</label>
            <input type="text" class="form-control" id="importProductTimeFrom" placeholder="Time from">
        </div>
        <div class="form-group">
            <label for="importProductTimeTo">Product time to</label>
            <input type="text" class="form-control" id="importProductTimeTo" placeholder="Time to">
        </div>
        <div class="form-group">
            <label for="importQty">Qty per box</label>
            <input type="text" class="form-control" id="importQty" placeholder="Qty per box">
        </div>
        <div class="form-group">
            <label for="importBoxWeight">Box weight</label>
            <input type="text" class="form-control" id="importBoxWeight" placeholder="Box weight">
        </div>
        <div class="form-group">
            <label for="importShippingZip">Shipping zip</label>
            <input type="text" class="form-control" id="importShippingZip"
                   placeholder="Shipping zip">
        </div>
        <div class="form-group">
            <label for="importVendor">Vendors</label>
            <input type="text" class="form-control" id="importVendor"
                   placeholder="Vendor">
        </div>
        <div class="form-group">
            <label for="importVendorSku">Vendor Sku</label>
            <input type="text" class="form-control" id="importVendorSku" placeholder="vendor sku">
        </div>
        <div class="form-group">
            <label for="importColorMode">Color Mode</label>
            <select class="form-control" id="importColorMode">
                <option value="1">Default</option>
                <option value="2">MultiColor Mode</option>
                <option value="3">SingleColor Mode</option>
            </select>
        </div>
        <h3>Price Options </h3>
        <div class="form-group">
            <label for="importPriceCode">Price code</label>
            <input type="text" class="form-control" id="importPriceCode" disabled>
        </div>
        <div class="form-group">
            <label for="importSetupPriceCode">Setup charge price code</label>
            <input type="text" class="form-control" id="importSetupPriceCode" disabled>
        </div>
        <div class="form-group">
            <label for="importAdditionalColor">Additional Color Setup charge code</label>
            <input type="text" class="form-control" id="importAdditionalColor" disabled>
        </div>
        <div class="form-group">
            <label for="importAdditionalColorRun">Additional Color Run Charge Code</label>
            <input type="text" class="form-control" id="importAdditionalColorRun" disabled>
        </div>
        <div class="form-group">
            <label class="checkbox-inline">
                <input type="checkbox" id="importSale"> Sale
            </label>
        </div>
        {!! View::make('admin.sage.Supplier.priceTable', []) !!}
        <div class="form-group">
            <a href="#" id="importProductColors" data-toggle="modal" data-target="#product-colors-modal">
                Product colors
            </a>
            {!! View::make('admin.sage.Supplier.modal', []) !!}
        </div>
        <div id="imprint-blocks">
        <h3>Imprint Location</h3>
            <div class="form-group">
                <label for="importImprintArea">Imprint area</label>
                <input type="text" class="form-control" id="importImprintArea">
            </div>
            {!! View::make('admin.sage.Supplier.imprintLocation', [
                'imprintName' => 'Main Imprint Location',
                'colorGroups' => $imprintColorGroups,
                'position' => 1
            ]) !!}
            <button type="submit" class="btn btn-default open-block" data-id="2">Add 2 location</button>
            <div class="hide imprint-block-2">
                {!! View::make('admin.sage.Supplier.imprintLocation', [
                    'imprintName' => '2 Imprint Location',
                    'colorGroups' => $imprintColorGroups,
                    'position' => 2
                ]) !!}
                <button type="submit" class="btn btn-default open-block" data-id="3">Add 3 location</button>
                <div class="hide imprint-block-3">
                    {!! View::make('admin.sage.Supplier.imprintLocation', [
                        'imprintName' => '3 Imprint Location',
                        'colorGroups' => $imprintColorGroups,
                        'position' => 3
                    ]) !!}
                    <button type="submit" class="btn btn-default open-block" data-id="4">Add 4 location</button>
                    <div class="hide imprint-block-4">
                        {!! View::make('admin.sage.Supplier.imprintLocation', [
                            'imprintName' => '4 Imprint Location',
                            'colorGroups' => $imprintColorGroups,
                            'position' => 4
                        ]) !!}
                        <button type="submit" class="btn btn-default open-block" data-id="5">Add 5 location</button>
                        <div class="hide imprint-block-5">
                            {!! View::make('admin.sage.Supplier.imprintLocation', [
                                'imprintName' => '5 Imprint Location',
                                'colorGroups' => $imprintColorGroups,
                                'position' => 5
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>