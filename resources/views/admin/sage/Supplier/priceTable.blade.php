<div class="form-group">
    <label for="importPriceGrid">Price grid</label>
    <table class="table table-bordered price-grid-table">
        <tr>
            <td>Quantity</td>
            <td>
                <input type="text" class="quantity-1" placeholder="0">
            </td>
            <td>
                <input type="text" class="quantity-2" placeholder="0">
            </td>
            <td>
                <input type="text" class="quantity-3" placeholder="0">
            </td>
            <td>
                <input type="text" class="quantity-4" placeholder="0">
            </td>
            <td>
                <input type="text" class="quantity-5" placeholder="0">
            </td>
            <td>
                <input type="text" class="quantity-6" placeholder="0">
            </td>
        </tr>
        <tr>
            <td>Regular Price</td>
            <td>
                <input type="text" id="regular-price-1" placeholder="0">
            </td>
            <td>
                <input type="text" id="regular-price-2" placeholder="0">
            </td>
            <td>
                <input type="text" id="regular-price-3" placeholder="0">
            </td>
            <td>
                <input type="text" id="regular-price-4" placeholder="0">
            </td>
            <td>
                <input type="text" id="regular-price-5" placeholder="0">
            </td>
            <td>
                <input type="text" id="regular-price-6" placeholder="0">
            </td>
        </tr>
        <tr>
            <td>Catalog Price</td>
            <td>
                <input type="text" id="catalog-price-1" placeholder="0" disabled>
            </td>
            <td>
                <input type="text" id="catalog-price-2" placeholder="0" disabled>
            </td>
            <td>
                <input type="text" id="catalog-price-3" placeholder="0" disabled>
            </td>
            <td>
                <input type="text" id="catalog-price-4" placeholder="0" disabled>
            </td>
            <td>
                <input type="text" id="catalog-price-5" placeholder="0" disabled>
            </td>
            <td>
                <input type="text" id="catalog-price-6" placeholder="0" disabled>
            </td>
        </tr>
        <tr>
            <td>Net Price</td>
            <td>
                <input type="text" id="net-price-1" placeholder="0" disabled>
            </td>
            <td>
                <input type="text" id="net-price-2" placeholder="0" disabled>
            </td>
            <td>
                <input type="text" id="net-price-3" placeholder="0" disabled>
            </td>
            <td>
                <input type="text" id="net-price-4" placeholder="0" disabled>
            </td>
            <td>
                <input type="text" id="net-price-5" placeholder="0" disabled>
            </td>
            <td>
                <input type="text" id="net-price-6" placeholder="0" disabled>
            </td>
        </tr>
        <tr>
            <td>Sale Price</td>
            <td>
                <input type="text" data-sale-price="" class="sale-price" id="sale-price-1" placeholder="0">
            </td>
            <td>
                <input type="text" data-sale-price="" class="sale-price" id="sale-price-2" placeholder="0">
            </td>
            <td>
                <input type="text" data-sale-price="" class="sale-price" id="sale-price-3" placeholder="0">
            </td>
            <td>
                <input type="text" data-sale-price="" class="sale-price" id="sale-price-4" placeholder="0">
            </td>
            <td>
                <input type="text" data-sale-price="" class="sale-price" id="sale-price-5" placeholder="0">
            </td>
            <td>
                <input type="text" data-sale-price="" class="sale-price" id="sale-price-6" placeholder="0">
            </td>
        </tr>
        <tr>
            <td>Individual recalc</td>
            <td>
                <input type="text" id="individual-recalc-1" placeholder="0"> %
                <button type="button" data-position="1" class="btn btn-xs btn-primary ind-recalc">set</button>
            </td>
            <td>
                <input type="text" id="individual-recalc-2" placeholder="0"> %
                <button type="button" data-position="2" class="btn btn-xs btn-primary ind-recalc">set</button>
            </td>
            <td>
                <input type="text" id="individual-recalc-3" placeholder="0"> %
                <button type="button" data-position="3" class="btn btn-xs btn-primary ind-recalc">set</button>
            </td>
            <td>
                <input type="text" id="individual-recalc-4" placeholder="0"> %
                <button type="button" data-position="4" class="btn btn-xs btn-primary ind-recalc">set</button>
            </td>
            <td>
                <input type="text" id="individual-recalc-5" placeholder="0"> %
                <button type="button" data-position="5" class="btn btn-xs btn-primary ind-recalc">set</button>
            </td>
            <td>
                <input type="text" id="individual-recalc-6" placeholder="0"> %
                <button type="button" data-position="6" class="btn btn-xs btn-primary ind-recalc">set</button>
            </td>
        </tr>
    </table>
    <p>Row SalePrice Recalculation:</p>
    <input type="text" id="importPriceGrid" placeholder="0"> %
    <button type="button" class="btn btn-xs btn-primary all-recalc">set</button>
</div>