@extends('admin.layout')

@section('script')
    <script type="text/javascript" src="{{ config('voyager.assets_path') }}/js/custom/sage_supplier.js"></script>
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form class="form-inline" id="supplier-form">
                            <label for="disabledTextInput">Search by</label>
                            <select class="form-control" id="search-type">
                                <option value="name">Name</option>
                                <option value="id">Id</option>
                            </select>
                            <div class="form-group space-left">
                                <label for="exampleInputEmail2">Search field</label>
                                <select type="text" class="form-control" id="search-supplier"
                                       placeholder="supplier name" >
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Apply</button>
                            <button type="button" class="btn btn-primary add-to-form">Add to form</button>
                            <button class="btn btn-success" id="import-product" disabled>Import Products</button>
                        </form>
                        <hr>
                        <form class="form-inline" id="quick-search-form">
                            {{--<label for="disabledTextInput">Search by</label>
                            <select class="form-control">
                                <option value="ad_code">SAGE ID</option>
                            </select>--}}
                            <div class="form-group space-left">
                                <label for="exampleInputEmail2">Quick search keywords</label>
                                <input type="text" name="product_search_field" class="form-control"
                                        placeholder="Search field">
                            </div>
                            <button type="button" class="btn btn-primary" id="products-search-button">Search</button>
                            <button type="button" class="btn btn-primary add-to-form">Add to form</button>
                        </form>
                        <div class="checkbox">
                            <label>
                                <input id="select-deselect-all" type="checkbox">Select / Deselect all
                            </label>
                        </div>
                        <div class="col-md-5">
                            <div id="pagination"></div>
                            <table class="table" id="import-supplier-table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Id</th>
                                        <th>Picture</th>
                                        <th>Product Data</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <div class="col-md-7">
                            {!! View::make('admin.sage.Supplier.importForm', [
                                'categories' => $categories,
                                'nestedCategories' => $nestedCategories,
                                'imprintColorGroups' => $imprintColorGroups,
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
