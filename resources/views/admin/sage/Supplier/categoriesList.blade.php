@foreach($categories as $category)
    <ul class="Container">
        <li class="Node ExpandClosed IsLast">
            <div class="Expand"></div>
            <div class="Content category-content" data-category-id="{{ $category['id'] }}">{{ $category['name'] }}</div>
            @if ($category['children'])
                {!! View::make('admin.sage.Supplier.categoriesList', ['categories' => $category['children']]) !!}
            @endif
        </li>
    </ul>
@endforeach