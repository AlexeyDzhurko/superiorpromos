<?php print '<?xml version="1.0" encoding="UTF-8" ?>'; ?>
<XMLDataStreamRequest>
    <Ver>3.2</Ver>
    <Auth>
        <AcctID>{{ $acctId }}</AcctID>
        <LoginID>{{ $loginId }}</LoginID>
        <Password>{{ $password }}</Password>
    </Auth>
    <SupplierProductDataDump>
        <MaxRecsToReturn>{{$items_per_page}}</MaxRecsToReturn>
        <StartRec>@if($current_page == 1){{$current_page}}@else{{(($current_page - 1) * $items_per_page) + 1}}@endif</StartRec>
        <SuppID>{{ $supplier }}</SuppID>
    </SupplierProductDataDump>
</XMLDataStreamRequest>