@extends('vendor.voyager.master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class=""></i>Edit
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">

                    <div class="panel-heading">
                        <h3 class="panel-title">Edit Company Info</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form"
                          action="{{route('company_info.update')}}"
                          method="POST" enctype="multipart/form-data">
                        <div class="panel-body">

                            <div class="form-group">
                                <label for="name">Title</label>
                                <input type="text" class="form-control" name="title"
                                       placeholder="Tile"
                                       value="<?=(isset($data['title'])) ? $data['title'] : ''?>">
                                <label for="name">Description</label>
                                <textarea class="form-control richTextBox" name="description"><?=(isset($data['description'])) ? $data['description'] : ''?></textarea>
                            </div>

                        </div><!-- panel-body -->

                        <!-- CSRF TOKEN -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
    <script src="{{ config('voyager.assets_path') }}/lib/js/tinymce/tinymce.min.js"></script>
    <script src="{{ config('voyager.assets_path') }}/js/voyager_tinymce.js"></script>
@stop