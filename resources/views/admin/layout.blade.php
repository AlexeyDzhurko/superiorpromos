@extends('vendor.voyager.angular-master')

@section('script')
    <script src="<?='/admin-assets/dist/js-assets-min.js?=' . time() ?>"></script>
    <script src="https://rawgit.com/ppham27/material/dist/dist/angular-material.js"></script>
    <script src="<?='/admin-assets/dist/javascript.js?=' . time() ?>"></script>
@stop

@section('page_header')
    @if(isset($adminModel))
        <h1 class="page-title">
            <i class="{{ $adminModel->getIcon() }}"></i> {{ $adminModel->getDisplayNamePlural() }}
            <a href="{{ route('admin_entity.create', ['admin_entity' => $adminModel->getBaseRouteName()]) }}" class="btn btn-success"><i class="voyager-plus"></i> Add
                New</a>
        </h1>
    @endif
@stop

@section('page_header_actions')

@stop

@section('javascript')
    <!-- DataTables -->
    <script>
        $('td').on('click', '.delete', function (e) {
            var id = $(this).data('id');
            var form = $('#delete_form')[0];
            var action = parseActionUrl(form.action, id);

            form.action = action;

            $('#delete_modal').modal('show');

        });

        function parseActionUrl(action, id) {
            if (action.match(/\/[0-9]+$/)) {
                return action.replace(/([0-9]+$)/, id);
            }
            return action + '/' + id;
        }
    </script>
@stop
