@extends('admin.layout')

@section('css')
    <meta name="csrf-token" content="<% csrf_token() %>">
    <link rel="stylesheet" href="/admin-assets/css/assets.min.css">
@stop

@section('page_header')
    <h1 class="page-title">
        @if (isset($product))
            Product #<% $product->id %> (<% $product->name %>)
        @else
            New Product
        @endif
    </h1>
@stop

@section('content')
    <?php
    $formLink = null;
    ?>
    <div class="page-content container-fluid" ng-app="app" ng-controller="ProductController" id="pageElement">
        <div ng-hide="hideLoader" class="voyager-loader">
            <img src="<% config('voyager.assets_path') %>/images/logo-icon.png" alt="Voyager Loader">
        </div>
        <script>
            product = {!! isset($product) ? $product->toJson() : '{}'  !!};
        </script>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="row">
                        <div class="col-md-12">
                            <form name="productForm" ng-submit="saveProduct(product.id)" novalidate>
                                <div class="col-md-12 form-block-container">
                                    <h4>Main</h4>
                                    <div
                                            ng-class="{'has-error': productForm.slug.$error.serverError}">
                                        <label class="control-label" for="active">Active</label>
                                        <input ng-true-value="1" ng-false-value="0"
                                               type="checkbox" ng-model="product.active" id="active"
                                               name="active">

                                        <span class="help-block"
                                              ng-show="productForm.active.$error.serverMessage">{{productForm.active.$error.serverMessage}}</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 form-horizontal">
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.name.$error.serverError}">
                                                <label class="control-label col-sm-3" for="name">Name</label>
                                                <div class="col-sm-9">
                                                    <input ng-model="product.name" id="name" name="name"
                                                           class="form-control"
                                                           placeholder="Name">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.name.$error.serverMessage">{{productForm.name.$error.serverMessage}}</span>
                                            </div>
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.dimensions.$error.serverError}">
                                                <label class="control-label col-sm-3"
                                                       for="dimensions">Dimensions</label>
                                                <div class="col-sm-9">
                                                    <input ng-model="product.dimensions" id="dimensions"
                                                           name="dimensions" class="form-control"
                                                           placeholder="Dimensions">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.dimensions.$error.serverMessage">{{productForm.dimensions.$error.serverMessage}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-horizontal">
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.url.$error.serverError}">
                                                <label class="control-label col-sm-3" for="url">Slug</label>
                                                <div class="col-sm-9">
                                                    <input ng-model="product.url" id="url" name="url"
                                                           class="form-control"
                                                           placeholder="Slug">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.name.$error.serverMessage">{{productForm.url.$error.serverMessage}}</span>
                                            </div>
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.popularity.$error.serverError}">
                                                <label class="control-label col-sm-3"
                                                       for="popularity">Popularity</label>
                                                <div class="col-sm-9">
                                                    <input type="number" ng-model="product.popularity"
                                                           id="popularity" name="popularity"
                                                           class="form-control"
                                                           placeholder="Popularity">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.popularity.$error.serverMessage">{{productForm.popularity.$error.serverMessage}}</span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.description.$error.serverError}">
                                                <label class="control-label"
                                                       for="description">Description</label>
                                                <textarea ui-tinymce="tinymceOptions"
                                                          ng-model="product.description"
                                                          id="description" name="description"
                                                          class="form-control"
                                                          placeholder="Description" rows="6"></textarea>
                                                <span class="help-block"
                                                      ng-show="productForm.description.$error.serverMessage">{{productForm.description.$error.serverMessage}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.imprint_area.$error.serverError}">
                                                <label class="control-label" for="imprint_area">Imprint
                                                    area</label>
                                                <textarea ui-tinymce="tinymceOptions"
                                                          ng-model="product.imprint_area" id="imprint_area"
                                                          name="imprint_area" class="form-control"
                                                          placeholder="Imprint Area" rows="6"></textarea>
                                                <span class="help-block"
                                                      ng-show="productForm.imprint_area.$error.serverMessage">{{productForm.imprint_area.$error.serverMessage}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.pricing_information.$error.serverError}">
                                                <label class="control-label" for="pricing_information">Pricing
                                                    Information</label>
                                                <textarea ui-tinymce="tinymceOptions"
                                                          ng-model="product.pricing_information"
                                                          id="pricing_information" name="pricing_information"
                                                          class="form-control"
                                                          placeholder="Pricing Information" rows="6"></textarea>
                                                <span class="help-block"
                                                      ng-show="productForm.imprint_area.$error.serverMessage">{{productForm.pricing_information.$error.serverMessage}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group"
                                                         ng-class="{'has-error': productForm.additional_specifications.$error.serverError}">
                                                        <label class="control-label"
                                                               for="additional_specifications">Additional
                                                            Specifications</label>
                                                        <div ngf-drop ngf-select ng-model="additional_specifications"
                                                             class="drop-box"
                                                             name="file"
                                                             ngf-drag-over-class="'dragover'"
                                                             ngf-multiple="false">
                                                            Drop file to upload
                                                        </div>
                                                        <div ng-if="additional_specifications">
                                                            File: {{additional_specifications.name}}</div>
                                                        <div ng-if="product.additional_specifications">
                                                            <a target="_blank"
                                                               href="{{product.additional_specifications}}">{{product.additional_specifications}}</a>
                                                        </div>
                                                        <span class="help-block"
                                                              ng-show="productForm.additional_specifications.$error.serverMessage">{{productForm.additional_specifications.$error.serverMessage}}</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group"
                                                         ng-class="{'has-error': productForm.video.$error.serverError}">
                                                        <label class="control-label" for="video_url">Video</label>
                                                        <input ng-model="product.video_url" id="video_url"
                                                               name="video_url"
                                                               class="form-control"
                                                               placeholder="File/URL">
                                                        <div ngf-drop ngf-select ng-model="video"
                                                             class="drop-box"
                                                             name="file"
                                                             ngf-drag-over-class="'dragover'"
                                                             ngf-multiple="false">
                                                            Drop file to upload
                                                        </div>
                                                        <div ng-if="video">File: {{video.name}}</div>
                                                        <div ng-if="product.video">
                                                            <a target="_blank"
                                                               href="{{product.video}}">{{product.video}}</a>
                                                        </div>
                                                        <span class="help-block"
                                                              ng-show="productForm.video.$error.serverMessage">{{productForm.video.$error.serverMessage}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.product_icon_id.$error.serverError}">
                                                <label class="control-label col-sm-4" for="position">Product
                                                    Icon</label>
                                                <div class="col-sm-8">
                                                    <ui-select ng-model="product.product_icon_id">
                                                        <ui-select-match placeholder="Choose product icon"
                                                                         allow-clear="true">
                                                            <span ng-bind="$select.selected.name"></span>
                                                        </ui-select-match>
                                                        <ui-select-choices
                                                                repeat="item.id as item in productIcons | filter: $select.search| limitTo: 50">
                                                            <span ng-bind="item.name"></span>
                                                        </ui-select-choices>
                                                    </ui-select>
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.position.$error.serverMessage">{{productForm.product_icon_id.$error.serverMessage}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.size_group_id.$error.serverError}">
                                                <label class="control-label col-sm-4" for="position">Size Group</label>
                                                <div class="col-sm-8">
                                                    <ui-select ng-model="product.size_group_id">
                                                        <ui-select-match placeholder="Choose product size group"
                                                                         allow-clear="true">
                                                            <span ng-bind="$select.selected.name"></span>
                                                        </ui-select-match>
                                                        <ui-select-choices
                                                                repeat="item.id as item in sizeGroups | filter: $select.search| limitTo: 50">
                                                            <span ng-bind="item.name"></span>
                                                        </ui-select-choices>
                                                    </ui-select>
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.position.$error.serverMessage">{{productForm.size_group_id.$error.serverMessage}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 form-block-container">
                                    <h4>Vendors</h4>
                                    <div class="row">
                                        <div class="col-md-6 form-horizontal">
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.vendors.$error.serverError}">
                                                <label class="control-label col-sm-2"
                                                       for="gender">Vendors</label>
                                                <div class="col-sm-10">
                                                    <ui-select name="vendors" ng-model="product.vendors"
                                                               multiple
                                                               sortable="true">
                                                        <ui-select-match
                                                                placeholder="Select a Vendor">
                                                            {{$item.text === undefined ? getVendorById($item).text : $item.text}}
                                                        </ui-select-match>
                                                        <ui-select-choices
                                                                repeat="vendor.id as vendor in vendors | filter: $select.search | limitTo: 30">
                                                            <span ng-bind="vendor.text"></span>
                                                        </ui-select-choices>
                                                    </ui-select>
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.vendors.$error.serverMessage">{{productForm.vendors.$error.serverMessage}}</span>
                                            </div>
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.sage_id.$error.serverError}">
                                                <label class="control-label col-sm-2" for="sage_id">Sage
                                                    ID</label>
                                                <div class="col-sm-10">
                                                    <input ng-model="product.sage_id" id="sage_id"
                                                           name="sage_id"
                                                           class="form-control"
                                                           placeholder="sage_id">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.sage_id.$error.serverMessage">{{productForm.sage_id.$error.serverMessage}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-horizontal">
                                            <div ng-repeat="vendorId in product.vendors" class="form-group"
                                                 ng-class="{'has-error': productForm.vendor_sku[vendorId].$error.serverError}">
                                                <label class="control-label col-sm-6"
                                                       for="vendor_sku_{{vendorId}}">SKU
                                                    #{{vendorId}} - {{getVendorById(vendorId).text}}</label>
                                                <div class="col-sm-6">
                                                    <input ng-model="product.vendor_sku[vendorId]"
                                                           id="vendor_sku_{{vendorId}}"
                                                           name="vendor_sku_{{vendorId}}  " class="form-control"
                                                           placeholder="SKU # - {{getVendorById(vendorId).text}}">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.vendor_sku[vendorId].$error.serverMessage">{{productForm.vendor_sku[vendorId].$error.serverMessage}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 form-block-container">
                                    <h4>Categories</h4>
                                    <div class="row">
                                        <div class="col-md-6 form-horizontal">
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.categories.$error.serverError}">
                                                <label class="control-label col-sm-2"
                                                       for="categories">Categories</label>
                                                <div class="col-sm-10">
                                                    <ui-select ng-model="product.categories" multiple
                                                               sortable="true">
                                                        <ui-select-match
                                                                placeholder="Categories">
                                                            {{$item.text === undefined ? getCategoryById($item).text : $item.text}}
                                                        </ui-select-match>
                                                        <ui-select-choices style="max-height: 800px" repeat="category.id as category in categories | filter: $select.search">
                                                            <span ng-bind="category.text"
                                                                  style="{{ getCategoryStyle(category.nested) }}">
                                                            </span>
                                                        </ui-select-choices>
                                                    </ui-select>
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.categories.$error.serverMessage">{{productForm.categories.$error.serverMessage}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-horizontal">
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.position.$error.serverError}">
                                                <label class="control-label col-sm-4" for="position">Position
                                                    After</label>
                                                <div class="col-sm-8">
                                                    <ui-select ng-model="product.position">
                                                        <ui-select-match>
                                                            <span ng-bind="$select.selected.name"></span>
                                                        </ui-select-match>
                                                        <ui-select-choices
                                                                repeat="item in (mainCategoryProducts | filter: $select.search| limitTo: 50) track by item.id">
                                                            <span ng-bind="item.name"></span>
                                                        </ui-select-choices>
                                                    </ui-select>
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.position.$error.serverMessage">{{productForm.position.$error.serverMessage}}</span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-12 form-block-container">
                                    <h4>Shipping</h4>
                                    <div class="row">
                                        <div class="col-md-6 form-horizontal">
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.quantity_per_box.$error.serverError}">
                                                <label class="control-label col-sm-6" for="quantity_per_box">Quantity
                                                    Per Box</label>
                                                <div class="col-sm-6">
                                                    <input ng-model="product.quantity_per_box"
                                                           id="quantity_per_box"
                                                           name="quantity_per_box" class="form-control"
                                                           placeholder="Quantity Per Box" type="number" min="1">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.quantity_per_box.$error.serverMessage">{{productForm.quantity_per_box.$error.serverMessage}}</span>
                                            </div>
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.box_weight.$error.serverError}">
                                                <label class="control-label col-sm-6" for="box_weight">Weight of
                                                    the
                                                    box</label>
                                                <div class="col-sm-6">
                                                    <input ng-model="product.box_weight" id="box_weight"
                                                           name="box_weight" class="form-control"
                                                           placeholder="Weight of the box" type="number" step=".01"
                                                           min="0.01">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.box_weight.$error.serverMessage">{{productForm.box_weight.$error.serverMessage}}</span>
                                            </div>
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.zip_from.$error.serverError}">
                                                <label class="control-label col-sm-6" for="zip_from">Shipping
                                                    From
                                                    ZIP Code</label>
                                                <div class="col-sm-6">
                                                    <input ng-model="product.zip_from" id="zip_from"
                                                           name="zip_from"
                                                           class="form-control"
                                                           placeholder="Shipping From ZIP Code">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.zip_from.$error.serverMessage">{{productForm.zip_from.$error.serverMessage}}</span>
                                            </div>
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.custom_shipping_method.$error.serverError}">
                                                <label class="control-label col-sm-6" for="custom_shipping_method">Custom
                                                    Method</label>
                                                <div class="col-sm-6">
                                                    <select ng-model="product.custom_shipping_method"
                                                            id="custom_shipping_method"
                                                            name="custom_shipping_method"
                                                            class="form-control">
                                                        <option hidden value="" disabled selected>Shipping Custom Method</option>
                                                        <option value="ground">Ground</option>
                                                        <option value="3day">3Day</option>
                                                        <option value="2day">2Day</option>
                                                        <option value="overnight">Overnight</option>
                                                        <option value="overnight_am">Overnight AM</option>
                                                        <option value="overnight_saturday_delivery">Overnight Saturday Delivery</option>
                                                    </select>
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.custom_shipping_method.$error.serverMessage">{{productForm.custom_shipping_method.$error.serverMessage}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-horizontal">
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.shipping_additional_type.$error.serverError}">
                                                <label class="control-label col-sm-4"
                                                       for="shipping_additional_type">Shipping Additional
                                                    Type</label>
                                                <div class="col-sm-8">
                                                    <select ng-model="product.shipping_additional_type"
                                                            id="shipping_additional_type"
                                                            name="shipping_additional_type"
                                                            class="form-control">
                                                        <option hidden value="" disabled selected>Shipping
                                                            Additional Type
                                                        </option>
                                                        <option value="percentage">Percentage</option>
                                                        <option value="absolute">Absolute</option>
                                                    </select>
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.shipping_additional_type.$error.serverMessage">{{productForm.shipping_additional_type.$error.serverMessage}}</span>
                                            </div>
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.shipping_additional_value.$error.serverError}">
                                                <label class="control-label col-sm-4"
                                                       for="shipping_additional_value">Shipping Additional
                                                    Value</label>
                                                <div class="col-sm-8">
                                                    <input ng-model="product.shipping_additional_value"
                                                           id="shipping_additional_value"
                                                           name="shipping_additional_value" class="form-control"
                                                           placeholder="Shipping Additional Value">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.shipping_additional_value.$error.serverMessage">{{productForm.shipping_additional_value.$error.serverMessage}}</span>
                                            </div>
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.production_time_from.$error.serverError}">
                                                <label class="control-label col-sm-4"
                                                       for="production_time_from">Production
                                                    Time From</label>
                                                <div class="col-sm-3">
                                                    <input ng-model="product.production_time_from"
                                                           id="production_time_from" name="production_time_from"
                                                           class="form-control"
                                                           placeholder="From" type="number">
                                                </div>
                                                <label class="control-label col-sm-2"
                                                       for="production_time_to">To</label>
                                                <div class="col-sm-3">
                                                    <input ng-model="product.production_time_to"
                                                           id="production_time_to" name="production_time_to"
                                                           class="form-control"
                                                           placeholder="To" type="number">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.production_time_to.$error.serverMessage">{{productForm.production_time_to.$error.serverMessage}}</span>
                                                <span class="help-block"
                                                      ng-show="productForm.production_time_from.$error.serverMessage">{{productForm.production_time_from.$error.serverMessage}}</span>
                                            </div>

                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.zip_from.$error.serverError}">
                                                <label class="control-label col-sm-4" for="custom_shipping_cost">Custom
                                                    Cost</label>
                                                <div class="col-sm-8">
                                                    <input ng-model="product.custom_shipping_cost"
                                                           id="custom_shipping_cost"
                                                           name="custom_shipping_cost"
                                                           class="form-control"
                                                           placeholder="Custom Cost">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.custom_shipping_cost.$error.serverMessage">{{productForm.custom_shipping_cost.$error.serverMessage}}</span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 form-block-container">
                                    <h4>Prices grid</h4>
                                    <div class="row">
                                        <div class="form-group col-sm-6"
                                             ng-class="{'has-error': productForm.on_sale.$error.serverError}">
                                            <label class="control-label" for="on_sale">On Sale</label>
                                            <input ng-true-value="1" ng-false-value="0"
                                                   type="checkbox" ng-model="product.on_sale" id="on_sale"
                                                   name="on_sale">

                                            <span class="help-block"
                                                  ng-show="productForm.on_sale.$error.serverMessage">{{productForm.on_sale.$error.serverMessage}}</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <form></form>
                                            <form editable-form name="tableform_prices"
                                                  onaftersave="savePrices()"
                                                  oncancel="cancelPrices()">
                                             <span class="section-title">
                                                    RETAIL Pricing
                                             </span>
                                                <span class="pull-left" ng-show="tableform_prices.$visible">
                                                    Please use "." (dot) separator for a prices
                                                </span>
                                                <a class="btn btn-default btn-xs pull-right"
                                                   ng-show="!tableform_prices.$visible"
                                                   ng-click="tableform_prices.$show()" role="button">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>
                                                <table class="table table-condensed table-bordered table-hover table-info">
                                                    <tbody>
                                                    <tr>
                                                        <th class="info" scope="row">Count From</th>
                                                        <td class="col-md-1"
                                                            ng-repeat="price in product.product_prices">
                                                        <span editable-text="price.quantity"
                                                              e-form="tableform_prices">
                                                                {{ price.quantity || 'empty' }}
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th class="info" scope="row">Setup Price</th>
                                                        <td class="col-md-1"
                                                            ng-repeat="price in product.product_prices">
                                                        <span editable-text="price.setup_price"
                                                              e-form="tableform_prices">
                                                                {{ price.setup_price || 'empty' }}
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th class="info" scope="row">Per Item Price</th>
                                                        <td class="col-md-1"
                                                            ng-repeat="price in product.product_prices">
                                                        <span editable-text="price.item_price"
                                                              e-form="tableform_prices">
                                                                {{ price.item_price || 'empty' }}
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th class="info" scope="row">Per Item Sale Price</th>
                                                        <td class="col-md-1"
                                                            ng-repeat="price in product.product_prices">
                                                        <span editable-text="price.sale_item_price"
                                                              e-form="tableform_prices">
                                                                {{ price.sale_item_price || 'empty' }}
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th class="info" scope="row">Actions</th>
                                                        <td class="col-md-1"
                                                            ng-repeat="(index, price) in product.product_prices">
                                                            <span ng-show="tableform_prices.$visible"
                                                                  ng-click="deletePrice(price.id, index)"
                                                                  class="btn btn-xs btn-danger">
                                                                <i class="voyager-trash"></i>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <div class="btn-form" ng-show="tableform_prices.$visible">
                                                    <button type="button"
                                                            ng-disabled="tableform_prices.$waiting"
                                                            ng-click="addPrice()"
                                                            class="btn btn-success pull-right">
                                                        <i class="voyager-plus"></i>
                                                    </button>
                                                    <button type="submit"
                                                            ng-disabled="tableform_prices.$waiting"
                                                            class="btn btn-primary">Save
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 form-block-container">
                                    <h4>Item Colors Groups</h4>
                                    <div class="row">
                                        <div class="col-md-12" ng-repeat="colorGroup in product.product_color_groups">
                                            <form editable-form name="tableform_color[colorGroup.id]"
                                                  onaftersave="saveColor(colorGroup.id)"
                                                  oncancel="cancelColor(colorGroup.id, color.id)">
                                            <span class="section-title" editable-text="colorGroup.name"
                                                  e-form="tableform_color[colorGroup.id]">
                                                    {{ colorGroup.name || 'empty' }}
                                            </span>
                                                <span ng-click="deleteColorGroup(colorGroup.id)"
                                                      class="btn btn-xs btn-danger">
                                                                                <i class="voyager-trash"></i>
                                                                            </span>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <ul class="nav nav-pills">
                                                            <li ng-repeat="color in colorGroup.product_colors"
                                                                ng-click="setCurrentColor(colorGroup.id, color)"
                                                                ng-class="{'active': currentColors[colorGroup.id].id == color.id}">
                                                                <a>{{color.color.title}}</a></li>
                                                        </ul>
                                                        <div ng-show="tableform_color[colorGroup.id].$visible">
                                                            <ui-select ng-model="product.newColor[colorGroup.id]">
                                                                <ui-select-match
                                                                        placeholder="Search for Colors">{{$select.selected.id}}
                                                                    - {{$select.selected.name}}</ui-select-match>
                                                                <ui-select-choices refresh="searchColors($select)"
                                                                                   refresh-delay="50"
                                                                                   repeat="item.id as item in colors | limitTo: 50">
                                                                    <span ng-bind="item.id"></span> - <span
                                                                            ng-bind="item.name"></span>
                                                                </ui-select-choices>
                                                            </ui-select>
                                                            <button type="button"
                                                                    ng-click="addColorToColorGroup(colorGroup.id)"
                                                                    class="btn btn-success">
                                                                <i class="voyager-plus"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <span class="pull-left">Please use "." (dot) separator for a prices</span>
                                                        <span ng-click="deleteColor(colorGroup.id, currentColors[colorGroup.id].id)"
                                                              class="btn btn-xs btn-danger pull-right">
                                                            <i class="voyager-trash"></i>
                                                        </span>
                                                        <a class="btn btn-default btn-xs pull-right"
                                                           ng-show="!tableform_color[colorGroup.id].$visible"
                                                           ng-click="tableform_color[colorGroup.id].$show()"
                                                           role="button"><i class="fa fa-pencil" aria-hidden="true"></i>
                                                        </a>
                                                        <table class="table table-condensed table-bordered table-hover table-info">
                                                            <tbody>
                                                            <tr>
                                                                <th class="info col-md-1" scope="row">Count From</th>
                                                                <td ng-repeat="colorPrice in currentColors[colorGroup.id].product_color_prices"
                                                                    class="col-md-1">
                                                                            <span editable-text="colorPrice.quantity"
                                                                                  e-form="tableform_color[colorGroup.id]">
                                                                                {{ colorPrice.quantity || 'empty' }}
                                                                            </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="info col-md-1" scope="row">Setup Price</th>
                                                                <td ng-repeat="colorPrice in currentColors[colorGroup.id].product_color_prices"
                                                                    class="col-md-1">
                                                                            <span editable-text="colorPrice.setup_price"
                                                                                  e-form="tableform_color[colorGroup.id]">
                                                                                {{ colorPrice.setup_price || 'empty' }}
                                                                            </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="info col-md-1" scope="row">Per Item Price
                                                                </th>
                                                                <td ng-repeat="colorPrice in currentColors[colorGroup.id].product_color_prices"
                                                                    class="col-md-1">
                                                                            <span editable-text="colorPrice.item_price"
                                                                                  e-form="tableform_color[colorGroup.id]">
                                                                                {{ colorPrice.item_price || 'empty' }}
                                                                            </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="info col-md-1" scope="row">Actions</th>
                                                                <td ng-repeat="colorPrice in currentColors[colorGroup.id].product_color_prices"
                                                                    class="col-md-1">
                                                                            <span ng-show="tableform_color[colorGroup.id].$visible"
                                                                                  ng-click="deleteColorPrice(colorGroup.id, currentColors[colorGroup.id].id, colorPrice.id)"
                                                                                  class="btn btn-xs btn-danger">
                                                                                <i class="voyager-trash"></i>
                                                                            </span>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>


                                                        <div class="btn-form"
                                                             ng-show="tableform_color[colorGroup.id].$visible">
                                                            <button type="button"
                                                                    ng-disabled="tableform_color[colorGroup.id].$waiting"
                                                                    ng-click="addColorPrice(colorGroup.id)"
                                                                    class="btn btn-success pull-right">
                                                                <i class="voyager-plus"></i>
                                                            </button>
                                                            <button type="submit"
                                                                    ng-disabled="tableform_color[colorGroup.id].$waiting"
                                                                    class="btn btn-success">Save
                                                            </button>
                                                            <button type="button"
                                                                    ng-disabled="tableform_color[colorGroup.id].$waiting"
                                                                    ng-click="tableform_color[colorGroup.id].$cancel()"
                                                                    class="btn btn-default">Cancel
                                                            </button>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2 images-container">
                                                        <div ng-show="tableform_color[colorGroup.id].$visible" ngf-drop
                                                             ngf-select ng-model="colorImage[colorGroup.id]"
                                                             class="drop-box"
                                                             name="file"
                                                             ngf-drag-over-class="'dragover'"
                                                             ngf-multiple="false">
                                                            Drop file to upload
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <img ngf-thumbnail="colorImage[colorGroup.id]"
                                                             class="img-responsive">
                                                        <img ng-if="currentColors[colorGroup.id].image_src && (!colorImage[colorGroup.id])"
                                                             ng-src="{{currentColors[colorGroup.id].image_src}}"
                                                             class="img-responsive">
                                                    </div>

                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-md-6" ng-show="colorGroupForm">
                                        <div class="col-md-12 form-horizontal">
                                            <div class="form-group">
                                                <label class="control-label col-sm-5" for="colorGroup">Import
                                                    colors from color group</label>
                                                <div class="col-sm-7">
                                                    <ui-select id="colorGroup" ng-model="newColorGroup.item">
                                                        <ui-select-match>
                                                            <span ng-bind="$select.selected.name"></span>
                                                        </ui-select-match>
                                                        <ui-select-choices
                                                                repeat="item in (colorGroups | filter: $select.search | limitTo: 50) track by item.id">
                                                            <span ng-bind="item.name"></span>
                                                        </ui-select-choices>
                                                    </ui-select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 form-horizontal">
                                            <div class="form-group">
                                                <label class="control-label col-sm-5" for="custom_color_group">Custom
                                                    color group</label>
                                                <div class="col-sm-7">
                                                    <input ng-model="newColorGroupName"
                                                           id="custom_color_group" class="form-control"
                                                           placeholder="Color Group Name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button type="button" ng-click="addColorGroup()" class="btn btn-success">
                                                Add
                                            </button>
                                            <button type="button" ng-click="hideColorGroupForm()"
                                                    class="btn btn-default">Cancel
                                            </button>
                                        </div>
                                    </div>

                                    <button ng-hide="colorGroupForm" type="button" class="btn btn-default"
                                            ng-click="showColorGroupForm()">
                                        Add Color Group
                                    </button>
                                </div>
                                <div class="col-md-12 form-block-container">
                                    <h4>Images</h4>
                                    <div class="row images-container">
                                        <div class="col-md-2">

                                            <div ngf-keep="'distinct'" ngf-drop ngf-select ngf-allow-dir="true"
                                                 ng-model="images" class="drop-box" name="images"
                                                 ngf-drag-over-class="'dragover'" ngf-multiple="true"
                                                 accept="image/*"
                                                 ngf-pattern="'image/*'">
                                                Drop file to upload
                                            </div>

                                        </div>

                                        <div ng-repeat="image in product.product_extra_images" class="col-md-2">
                                            <img ng-src="{{image.medium}}"
                                                 ng-click="selectThumbnailProductExtraImage(image)"
                                                 ng-style="product.product_thumbnail == image.id ? {'border':'5px solid #22A7F0'} : ''"
                                                 class="img-responsive">
                                            <span
                                                    ng-click="deleteProductExtraImage(image)"
                                                    class="btn btn-xs btn-danger">
                                                            <i class="voyager-trash"></i>
                                                        </span>
                                        </div>

                                        <div ng-repeat="image in images" class="col-md-2">
                                            <img ngf-thumbnail="image" class="img-responsive">
                                            <span
                                                    ng-click="deleteImageFile(image)"
                                                    class="btn btn-xs btn-danger">
                                                            <i class="voyager-trash"></i>
                                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 form-block-container">
                                    <h4>Imprints</h4>
                                    <div class="row">
                                        <div class="col-md-12" ng-repeat="imprint in product.imprints">
                                            <form editable-form name="tableform[imprint.id]"
                                                  onaftersave="saveImprintPrices(imprint)"
                                                  oncancel="cancelImprintPrices(imprint.id)">
                                                <table class="table table-condensed table-bordered table-hover table-info">
                                                    <tbody>
                                                    <tr>
                                                        <th class="info" scope="row">Name</th>
                                                        <td>
                                                    <span editable-text="imprint.name"
                                                          e-form="tableform[imprint.id]">
                                                        {{ imprint.name || 'empty' }}
                                                    </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th class="info" scope="row">Max Colors</th>
                                                        <td>
                                                    <span editable-text="imprint.max_colors"
                                                          e-form="tableform[imprint.id]">
                                                        {{ imprint.max_colors || 'empty' }}
                                                    </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th class="info" scope="row">Color Groups</th>
                                                        <td>
                                                        <span editable-select="imprint.color_group_id"
                                                              e-ng-options="s.id as s.name for s in colorGroups">
                                                                {{ imprint.color_group.name || 'empty' }}
                                                        </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th class="info" scope="row">Custom Colors</th>
                                                        <td>
                                                            <span ng-repeat="imprintColor in imprint.colors">
                                                                <i class="fa fa-circle" style="color: {{ imprintColor.color.color_hex }}"></i>
                                                                {{ imprintColor.color.title || 'empty' }}
                                                                <i class="fa fa-close"
                                                                   ng-click="dropColor(imprintColor.id, imprint.id)"
                                                                   ng-show="tableform[imprint.id].$visible">
                                                                </i>
                                                            </span>
                                                            <span class="btn btn-success pull-right"
                                                                    ng-show="tableform[imprint.id].$visible"
                                                                    ng-click="addCustomColor(imprint.id)">
                                                                <i class="voyager-plus"></i>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <span class="pull-left" ng-show="tableform[imprint.id].$visible">
                                                    Please use "." (dot) separator for a prices
                                                </span>
                                                <a class="btn btn-default btn-xs pull-right"
                                                   ng-show="!tableform[imprint.id].$visible"
                                                   ng-click="tableform[imprint.id].$show()" role="button"><i
                                                            class="fa fa-pencil" aria-hidden="true"></i></a>
                                                <!-- table -->
                                                <table class="table table-condensed table-bordered table-hover table-info">
                                                    <tbody>
                                                    <tr>
                                                        <th class="info" scope="row">Count From</th>
                                                        <td class="col-md-1"
                                                            ng-repeat="imprintPrice in imprint.imprint_prices">
                                                    <span editable-text="imprintPrice.quantity"
                                                          e-form="tableform[imprint.id]">
                                                        {{ imprintPrice.quantity || 'empty' }}
                                                    </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th class="info" scope="row">Location Setup Fee</th>
                                                        <td class="col-md-1"
                                                            ng-repeat="imprintPrice in imprint.imprint_prices">
                                                    <span editable-text="imprintPrice.setup_price"
                                                          e-form="tableform[imprint.id]">
                                                        {{ imprintPrice.setup_price || 'empty' }}
                                                    </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th class="info" scope="row">Additional Location Running Fee</th>
                                                        <td class="col-md-1"
                                                            ng-repeat="imprintPrice in imprint.imprint_prices">
                                                    <span editable-text="imprintPrice.item_price"
                                                          e-form="tableform[imprint.id]">
                                                        {{ imprintPrice.item_price || 'empty' }}
                                                    </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th class="info" scope="row">Additional Color Setup Fee</th>
                                                        <td class="col-md-1"
                                                            ng-repeat="imprintPrice in imprint.imprint_prices">
                                                    <span editable-text="imprintPrice.color_setup_price"
                                                          e-form="tableform[imprint.id]">
                                                        {{ imprintPrice.color_setup_price || 'empty' }}
                                                    </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th class="info" scope="row">Additional Color Running Fee</th>
                                                        <td class="col-md-1"
                                                            ng-repeat="imprintPrice in imprint.imprint_prices">
                                                    <span editable-text="imprintPrice.color_item_price"
                                                          e-form="tableform[imprint.id]">
                                                        {{ imprintPrice.color_item_price || 'empty' }}
                                                    </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th class="info" scope="row">Actions</th>
                                                        <td class="col-md-1"
                                                            ng-repeat="imprintPrice in imprint.imprint_prices">
                                                    <span ng-show="tableform[imprint.id].$visible"
                                                          ng-click="deleteImprintPrice(imprint.id, imprintPrice.id)"
                                                          class="btn btn-xs btn-danger">
                                                        <i class="voyager-trash"></i>
                                                    </span>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <div class="btn-form" ng-show="tableform[imprint.id].$visible">
                                                    <button type="button" ng-disabled="tableform[imprint.id].$waiting"
                                                            ng-click="addImprintPrice(imprint.id)"
                                                            class="btn btn-success pull-right">
                                                        <i class="voyager-plus"></i>
                                                    </button>
                                                    <button type="submit" ng-disabled="tableform[imprint.id].$waiting"
                                                            class="btn btn-primary">save
                                                    </button>
                                                    <button type="button" ng-disabled="tableform[imprint.id].$waiting"
                                                            ng-click="tableform[imprint.id].$cancel()"
                                                            class="btn btn-default">cancel
                                                    </button>
                                                    <button type="button" ng-disabled="tableform[imprint.id].$waiting"
                                                            ng-click="deleteImprint(imprint.id)"
                                                            class="btn btn-danger"><i class="voyager-trash"></i>
                                                    </button>
                                                </div>

                                            </form>
                                        </div>
                                        <hr>
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-default" ng-click="addImprint()">
                                                Add Imprint
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 form-block-container">
                                    <h4>Product Options</h4>
                                    <div class="row">
                                        <div class="col-md-6" ng-repeat="productOption in product.product_options">
                                            <form editable-form name="tableform[productOption.id]"
                                                  onaftersave="saveOptionPrices(productOption)"
                                                  oncancel="cancelOptionPrices(productOption.id)">
                                        <span class="section-title" editable-text="productOption.name"
                                              e-form="tableform[productOption.id]">
                                            {{ productOption.name || 'empty' }}
                                        </span>

                                                <br>
                                                <div ng-show="tableform[productOption.id].$visible">
                                                    <selectize placeholder="Show As"
                                                               config='{valueField: "id", labelField: "text", maxItems: 1, placeholder: "Pick option input type"}'
                                                               options='addProductOptionShowAsSelect'
                                                               ng-model="productOption.show_as"></selectize>
                                                    <b>Require:</b> <input ng-true-value="1" ng-false-value="0"
                                                                           type="checkbox"
                                                                           ng-model="productOption.required"
                                                                           name="required">
                                                </div>
                                                <div ng-show="!tableform[productOption.id].$visible">
                                                    <b>Show
                                                        as:</b> {{getOptionFieldNameByShowAsAttribute(productOption.show_as)}}
                                                    <br>
                                                    <b>Required:</b> {{productOption.required ? 'Yes' : 'No'}}
                                                </div>

                                                <a class="btn btn-default btn-xs pull-right"
                                                   ng-show="!tableform[productOption.id].$visible"
                                                   ng-click="tableform[productOption.id].$show()" role="button"><i
                                                            class="fa fa-pencil" aria-hidden="true"></i></a>
                                                <table class="table table-condensed table-bordered table-hover table-info">

                                                    <tbody>
                                                    <tr>
                                                        <th class="info" scope="row">Count From</th>
                                                        <td class="col-md-1"
                                                            ng-repeat="productOptionPrice in productOption.product_option_prices">
                                                    <span editable-text="productOptionPrice.quantity"
                                                          e-form="tableform[productOption.id]">
                                                            {{ productOptionPrice.quantity || 'empty' }}
                                                        </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th class="info" scope="row">Setup Price</th>
                                                        <td class="col-md-1"
                                                            ng-repeat="productOptionPrice in productOption.product_option_prices">
                                                    <span editable-text="productOptionPrice.setup_price"
                                                          e-form="tableform[productOption.id]">
                                                            {{ productOptionPrice.setup_price || 'empty' }}
                                                        </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th class="info" scope="row">Per Item Price</th>
                                                        <td class="col-md-1"
                                                            ng-repeat="productOptionPrice in productOption.product_option_prices">
                                                    <span editable-text="productOptionPrice.item_price"
                                                          e-form="tableform[productOption.id]">
                                                            {{ productOptionPrice.item_price || 'empty' }}
                                                        </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th class="info" scope="row">Actions</th>
                                                        <td class="col-md-1"
                                                            ng-repeat="productOptionPrice in productOption.product_option_prices">
                                                        <span ng-show="tableform[productOption.id].$visible"
                                                              ng-click="deleteOptionPrice(productOption.id, productOptionPrice.id)"
                                                              class="btn btn-xs btn-danger">
                                                            <i class="voyager-trash"></i>
                                                        </span>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                                <div class="btn-form" ng-show="tableform[productOption.id].$visible">
                                                    <button type="button"
                                                            ng-disabled="tableform[productOption.id].$waiting"
                                                            ng-click="addProductOptionPrice(productOption.id)"
                                                            class="btn btn-success pull-right">
                                                        <i class="voyager-plus"></i>
                                                    </button>
                                                    <button type="submit"
                                                            ng-disabled="tableform[productOption.id].$waiting"
                                                            class="btn btn-primary">Save
                                                    </button>
                                                    <button type="button"
                                                            ng-disabled="tableform[productOption.id].$waiting"
                                                            ng-click="tableform[productOption.id].$cancel()"
                                                            class="btn btn-default">Cancel
                                                    </button>
                                                    <button type="button"
                                                            ng-disabled="tableform[productOption.id].$waiting"
                                                            ng-click="showProductSubOptionsForm(productOption.id)"
                                                            class="btn btn-default">Edit Sub Options
                                                    </button>
                                                    <button type="button"
                                                            ng-disabled="tableform[productOption.id].$waiting"
                                                            ng-click="deleteOption(productOption.id)"
                                                            class="btn btn-danger"><i
                                                                class="voyager-trash"></i></button>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-default" ng-click="addProductOption()">
                                        Add Product Option
                                    </button>
                                </div>
                                <div class="col-md-12 form-block-container">
                                    <h4>SEO</h4>
                                    <div class="row">
                                        <div class="col-md-6 form-horizontal">
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.meta_title.$error.serverError}">
                                                <label class="control-label col-sm-3" for="meta_title">Meta
                                                    Title</label>
                                                <div class="col-sm-9">
                                                    <input ng-model="product.meta_title" id="meta_title"
                                                           name="meta_title" class="form-control"
                                                           placeholder="Meta Title">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.meta_title.$error.serverMessage">{{productForm.meta_title.$error.serverMessage}}</span>
                                            </div>
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.keywords.$error.serverError}">
                                                <label class="control-label col-sm-3"
                                                       for="keywords">Keywords</label>
                                                <div class="col-sm-9">
                                                    <input ng-model="product.keywords" id="keywords"
                                                           name="keywords"
                                                           class="form-control"
                                                           placeholder="Keywords">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.keywords.$error.serverMessage">{{productForm.keywords.$error.serverMessage}}</span>
                                            </div>
                                            <div ng-class="{'has-error': productForm.meta_description.$error.serverError}">
                                                <label class="control-label" for="meta_description">Meta
                                                    Description</label>
                                                <textarea ui-tinymce="tinymceOptions"
                                                          ng-model="product.meta_description"
                                                          id="meta_description"
                                                          name="meta_description" class="form-control"
                                                          placeholder="Meta Description" rows="6"></textarea>
                                                <span class="help-block"
                                                      ng-show="productForm.meta_description.$error.serverMessage">{{productForm.meta_description.$error.serverMessage}}</span>
                                            </div>
                                        </div>

                                        <div class="col-md-6 form-horizontal">
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.google_product_category.$error.serverError}">
                                                <label class="control-label col-sm-3"
                                                       for="google_product_category">Google
                                                    Product Category</label>
                                                <div class="col-sm-9">
                                                    <input ng-model="product.google_product_category"
                                                           id="google_product_category"
                                                           name="google_product_category" class="form-control"
                                                           placeholder="Google Product Category">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.google_product_category.$error.serverMessage">{{productForm.google_product_category.$error.serverMessage}}</span>
                                            </div>
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.image_alt.$error.serverError}">
                                                <label class="control-label col-sm-3" for="image_alt">Image Alt
                                                    Text</label>
                                                <div class="col-sm-9">
                                                    <input ng-model="product.image_alt" id="image_alt"
                                                           name="image_alt" class="form-control"
                                                           placeholder="Image Alt Text">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.image_alt.$error.serverMessage">{{productForm.image_alt.$error.serverMessage}}</span>
                                            </div>
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.brand.$error.serverError}">
                                                <label class="control-label col-sm-3" for="brand">Brand</label>
                                                <div class="col-sm-9">
                                                    <input ng-model="product.brand" id="brand" name="brand"
                                                           class="form-control"
                                                           placeholder="Brand">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.brand.$error.serverMessage">{{productForm.brand.$error.serverMessage}}</span>
                                            </div>
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.gender.$error.serverError}">
                                                <label class="control-label col-sm-3"
                                                       for="gender">Gender</label>
                                                <div class="col-sm-9">
                                                    <input ng-model="product.gender" id="gender" name="gender"
                                                           class="form-control"
                                                           placeholder="Gender">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.gender.$error.serverMessage">{{productForm.gender.$error.serverMessage}}</span>
                                            </div>
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.age_group.$error.serverError}">
                                                <label class="control-label col-sm-3" for="age_group">Age
                                                    Group</label>
                                                <div class="col-sm-9">
                                                    <input ng-model="product.age_group" id="age_group"
                                                           name="age_group" class="form-control"
                                                           placeholder="Age Group">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.age_group.$error.serverMessage">{{productForm.age_group.$error.serverMessage}}</span>
                                            </div>
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.material.$error.serverError}">
                                                <label class="control-label col-sm-3"
                                                       for="material">Material</label>
                                                <div class="col-sm-9">
                                                    <input ng-model="product.material" id="material"
                                                           name="material"
                                                           class="form-control"
                                                           placeholder="Material">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.material.$error.serverMessage">{{productForm.material.$error.serverMessage}}</span>
                                            </div>
                                            <div class="form-group"
                                                 ng-class="{'has-error': productForm.pattern.$error.serverError}">
                                                <label class="control-label col-sm-3"
                                                       for="pattern">Pattern</label>
                                                <div class="col-sm-9">
                                                    <input ng-model="product.pattern" id="pattern"
                                                           name="pattern"
                                                           class="form-control"
                                                           placeholder="Pattern">
                                                </div>
                                                <span class="help-block"
                                                      ng-show="productForm.pattern.$error.serverMessage">{{productForm.pattern.$error.serverMessage}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" ng-click="saveProduct(product.id)" class="btn btn-primary">Save
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @stop

        @section('javascript')
            <script>
                $('document').ready(function () {
                    $('.toggleswitch').bootstrapToggle();

                    var PRE_SELECT = <?php echo json_encode($collections); ?>

                    $('.select-multiple').select2({
                        minimumInputLength: 2,
                        initSelection: function (element, callback) {
                            for (var x = 0; x < PRE_SELECT.length; x++) {
                                $('.select-multiple').append($("<option/>", {
                                    value: PRE_SELECT[x].id,
                                    text: PRE_SELECT[x].text,
                                    selected: true
                                }));
                            }
                            callback(PRE_SELECT);
                        },
                        ajax: {
                            url: "<% $formLink %>",
                            dataType: 'json',
                            data: function (params) {
                                return {
                                    query: params.term
                                };
                            },
                            results: function (data) {
                                return {results: data};
                            },
                            processResults: function (data) {
                                var answer = [];
                                for (var i = 0; i < data.length; i++) {
                                    answer.push({
                                        id: data[i].id,
                                        text: data[i].name
                                    });
                                }
                                return {
                                    results: answer
                                };
                            },
                            cache: true
                        },
                    });
                    $('.select-multiple').change();
                });
            </script>
            <script src="<% config('voyager.assets_path') %>/lib/js/tinymce/tinymce.min.js"></script>
            <script src="<% config('voyager.assets_path') %>/js/voyager_tinymce.js"></script>
@stop
