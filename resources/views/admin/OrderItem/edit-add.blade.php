@extends('admin.layout')

@section('css')
    <meta name="csrf-token" content="<% csrf_token() %>">
    <link rel="stylesheet" href="/admin-assets/css/assets.min.css">
@stop

@section('content')
    <?php
    $formLink = null;
    ?>
    <div class="page-content container-fluid" ng-app="app" ng-controller="MainController">
        <div ng-hide="hideLoader" class="voyager-loader">
            <img src="<% config('voyager.assets_path') %>/images/logo-icon.png" alt="Voyager Loader">
        </div>
        <script>
            orderItem = {!! $orderItem->toJson() !!};
            trackingUserName = '<% $orderItem->trackingUser->name ?? null %>';
            authUserName = '<% Auth::user()->name %>';
        </script>
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">

                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <%$orderItem->order_id%>S<%$orderItem->id%>
                        </h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="panel-footer">

                        <div class="row">
                            <div class="col-md-12">
                                <button ng-click="showNotesForm()" class="btn btn-primary btn-xs">Order Notes / File
                                    Cabinet
                                </button>
                                <button ng-click="showUserNotesForm()" class="btn btn-primary btn-xs">User Notes
                                </button>
                                <button ng-click="showArtProofsForm()" class="btn btn-primary btn-xs">Art Proofs
                                </button>
                                <button ng-click="showExtraBillingForm()" class="btn btn-primary btn-xs">Extra Billing
                                </button>
                                <div class="float-right">
                                    <button ng-if="canRemoveOrderItem('<?= $orderItem->stage ? $orderItem->stage->name : '' ?>')"
                                            ng-click="removeOrderItem()"
                                            class="btn btn-danger btn-xs">
                                        Remove Order
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-condensed table-bordered table-hover table-info">
                                <tbody>
                                <tr>
                                    <th class="info" scope="row">Reference #</th>
                                    <td><%$orderItem->order_id%>S<%$orderItem->id%></td>
                                    <th class="info">Order Total</th>
                                    <td>$<% number_format($cartItemCost->getTotal(), 2) %></td>
                                </tr>
                                <tr>
                                    <th class="info" scope="row">Order Date</th>
                                    <td><% \Carbon\Carbon::parse($orderItem->order->created_at)->format('m/d/y g:i A')
                                        ?? 'ASAP' %>
                                    </td>
                                    <th class="info">In-Hand Date</th>
                                    <td><% $orderItem->received_date == '0000-00-00 00:00:00' ?
                                        \Carbon\Carbon::parse($orderItem->received_date)->format('m/d/y g:i A') : 'ASAP'
                                        %>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="info" scope="row">Customer ID</th>
                                    <td>
                                        <a href="/admin/customer/<%$orderItem->order->user_id%>/edit">
                                            <%$orderItem->order->user_id%>
                                        </a>
                                    </td>
                                    <th class="info">Order Status</th>
                                    <td>
                                        <%$orderItem->stage->name ?? 'Without Stage'%>
                                        <a ng-if="checkOrderInProcess('<?= $orderItem->stage ? $orderItem->stage->name : '' ?>')"
                                           class="btn btn-default btn-xs"
                                           ng-click="showOrderStages()"
                                           role="button">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="info" scope="row">Contact Name</th>
                                    <td><%$orderItem->order->user->name%></td>
                                    <th class="info">Purchase Order #</th>
                                    <td class="col-md-4">
                                                <span editable-text="orderItem.po_number" e-form="textBtnForm"
                                                      onbeforesave="updatePurchaseOrderNumber($data)">
                                                    {{orderItem.po_number || 'empty'}}
                                                </span>
                                        <a ng-if="checkOrderInProcess('<?= $orderItem->stage ? $orderItem->stage->name : '' ?>')"
                                           ng-click="textBtnForm.$show()"
                                           ng-hide="textBtnForm.$visible"
                                           class="btn btn-default btn-xs" role="button">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="info" scope="row">Contact Email</th>
                                    <td><a href="mailto:<%$orderItem->order->user->email%>"><%$orderItem->order->user->email%></a>
                                    </td>
                                    <th class="info">Vendor</th>
                                    <td>
                                                <span editable-select="orderItem.vendor_id" e-form="vendorForm"
                                                      e-ng-options="s.id as s.text for s in vendors"
                                                      onbeforesave="updateVendor($data)">
                                                   <% $orderItem->vendor->name or 'empty' %>
                                                </span>
                                        <a ng-if="checkOrderInProcess('<?= $orderItem->stage ? $orderItem->stage->name : '' ?>')"
                                           ng-click="vendorForm.$show()"
                                           ng-hide="vendorForm.$visible"
                                           class="btn btn-default btn-xs"
                                           role="button">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="info" scope="row">Customer Ph</th>
                                    <td><%$orderItem->order->user->contact_telephone%></td>
                                    <th class="info">Tracking #</th>
                                    <td>
                                        <% $orderItem->trackingInformations()->orderBy('id',
                                        'DESC')->first()->tracking_number or 'empty' %>
                                        <a ng-if="checkOrderInProcess('<?= $orderItem->stage ? $orderItem->stage->name : '' ?>')"
                                           class="btn btn-default btn-xs"
                                           role="button"
                                           ng-click="showTrackingInformationForm()">
                                            <i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="info" scope="row">
                                        Invoice
                                    </th>
                                    <td colspan="3">
                                        <div>
                                                    <span ng-if="orderItem.invoice_file">
                                                        {{orderItem.invoice_file}}
                                                        <a download
                                                           href="/admin/api/order_item/<% $orderItem->id %>/invoice_file">Download</a>
                                                    </span>
                                            <span ng-if="!orderItem.invoice_file">
                                                        Not uploaded
                                                    </span>
                                            <a ng-if="checkOrderInProcess('<?= $orderItem->stage ? $orderItem->stage->name : '' ?>')"
                                               class="btn btn-default btn-xs"
                                               role="button"
                                               ng-click="showInvoiceForm()">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <h5>Shipping Address
                                    <a ng-if="checkOrderInProcess('<?= $orderItem->stage ? $orderItem->stage->name : '' ?>')"
                                       class="btn btn-default btn-xs"
                                       ng-click="showShippingAddressForm()"
                                       role="button">
                                        <i class="fa fa-pencil" aria-hidden="true"></i></a>
                                </h5>
                                <div class="table-responsive">
                                    <div>{{orderItem.shipping_first_name}} {{orderItem.shipping_middle_name}} {{orderItem.shipping_last_name}}</div>
                                    <div>{{orderItem.shipping_company_name}}</div>
                                    <div>{{orderItem.shipping_address_line_1}} {{orderItem.shipping_address_line_2}}</div>
                                    <div>
                                        {{orderItem.shipping_city}}
                                        @if($orderItem->shipping_state), @endif
                                        {{orderItem.shipping_state}} {{orderItem.shipping_zip}}
                                    </div>
                                    <div>{{orderItem.shipping_day_telephone}}</div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h5>Billing Address
                                    <a ng-if="checkOrderInProcess('<?= $orderItem->stage ? $orderItem->stage->name : '' ?>')"
                                       class="btn btn-default btn-xs"
                                       role="button"
                                       ng-click="showBillingAddressForm()">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </a>
                                </h5>
                                <div class="table-responsive">
                                    <div>{{orderItem.billing_first_name}} {{orderItem.billing_middle_name}} {{orderItem.billing_last_name}}</div>
                                    <div>{{orderItem.billing_company_name}}</div>
                                    <div>{{orderItem.billing_address_line_1}} {{orderItem.billing_address_line_2}}</div>
                                    <div>
                                        {{orderItem.billing_city}}
                                        @if($orderItem->billing_state), @endif
                                        {{orderItem.billing_state}} {{orderItem.billing_zip}}
                                    </div>
                                    <div>{{orderItem.billing_day_telephone}}</div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Order Details</h5>
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered table-hover table-info">
                                        <tbody>
                                        <tr>
                                            <th class="col-md-2 info" scope="row">Delivery Date</th>
                                            <td class="col-md-10">{{orderItem.received_date || 'ASAP'}}</td>
                                        </tr>
                                        @if(!empty($orderItem->cartItem))
                                            <tr>
                                                <th class="col-md-2 info" scope="row">Colors</th>
                                                <td class="col-md-10">
                                                    @forelse($orderItem->cartItem->cartItemColors as $color)
                                                        <%$color->color_group_name  or 'Blank Goods'%>:
                                                        <%$color->color_name%><br>
                                                    @empty
                                                        none
                                                    @endforelse
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="col-md-2 info" scope="row">Options</th>
                                                <td class="col-md-10">
                                                    @forelse($orderItem->cartItem->cartItemProductOptions as $option)
                                                        <li><%$option->name%></li>
                                                    @empty
                                                        none
                                                    @endforelse
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="col-md-2 info" scope="row">Imprints</th>
                                                <td class="col-md-10">
                                                    @forelse($orderItem->cartItem->cartItemImprints as $imprint)
                                                        <%$imprint->imprint_name or 'Blank Goods'%>:
                                                        @forelse($imprint->cartItemImprintColors as $imprintColor)
                                                            <span><%$imprintColor->name%></span>
                                                        @empty

                                                        @endforelse
                                                        <br>
                                                    @empty
                                                        none
                                                    @endforelse
                                                </td>
                                            </tr>
                                        @endif
                                        @if(!empty($artFiles))
                                            <tr>
                                                <th class="col-md-2 info" scope="row">Uploaded artwork</th>
                                                <td class="col-md-10">
                                                    <ul>
                                                        @foreach($artFiles as $key => $artFile)
                                                            <li>
                                                                <a target="_blank" href="<?= Storage::url($artFile) ?>">Atrfile <?= $key + 1 ?></a>
                                                            </li>
                                                        @endforeach
                                                    </ul>

                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h5>Product Info</h5>
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered table-hover table-info">
                                        <tbody>
                                        <tr>
                                            <th class="info" scope="row">Preview</th>
                                            <td>
                                                <a href="<%$orderItem->product->image_src or '/img/image-not-available.jpg'%>">
                                                    <img class="img-thumbnail admin-thumbnail"
                                                         src="<%$orderItem->product->image_src or '/img/image-not-available.jpg'%>">
                                                </a>
                                            </td>
                                        </tr>
                                        @if(!is_null($orderItem->cartItem->art_file_path))
                                            <tr>
                                                <th class="info" scope="row">User imprint file</th>
                                                <td>
                                                    <a href="/storage/<%$orderItem->cartItem->art_file_path%>">
                                                        <img class="img-thumbnail admin-thumbnail"
                                                             src="/storage/<%$orderItem->cartItem->art_file_path%>">
                                                    </a>
                                                </td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <th class="col-md-2 info" scope="row">Id</th>
                                            <td class="col-md-10">Item <a
                                                        href="/admin/product/<%$orderItem->product->id%>/edit">#<%$orderItem->product->id%></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="col-md-2 info" scope="row">Name</th>
                                            <td class="col-md-10"><%$orderItem->product->name%></td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">Sage ID</th>
                                            <td><% $orderItem->product->sage_id %></td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">Vendor</th>
                                            <td><% $orderItem->vendor->name or null %></td>
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">SKU Vendor</th>
                                            <td>Table cell</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Pricing $
                                </h5>
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered table-hover table-info">
                                        <tbody>
                                        <tr>
                                            <th class="info" scope="row">Quantity</th>
                                            <td><% $orderItem->quantity %></td>
                                        </tr>

                                        <tr>
                                            <th class="info" scope="row">Base Unit Price</th>
                                            <td>$<% $cartItemCost->getBaseItemPrice() - $cartItemCost->getFinalItemPrice() %>
                                            </td>
                                        </tr>

                                        @if($cartItemCost->getSetupPrice() > 0)
                                        <tr>
                                            <th class="info" scope="row">Setup Price</th>
                                            <td>$<% $cartItemCost->getSetupPrice() %>
                                            </td>
                                        </tr>
                                        @endif
                                        @if(!is_null($orderItem->cartItem->cartItemImprints))
                                            @foreach($orderItem->cartItem->cartItemImprints as $cartItemImprint)
                                                @if($cartItemImprint->item_price > 0)
                                                    <tr>
                                                        <th class="info" scope="row">
                                                            <% $cartItemImprint->imprint_name %> Additional fee (per item)
                                                        </th>
                                                        <td>$<% $cartItemImprint->item_price %></td>
                                                    </tr>
                                                @endif
                                                @if($cartItemImprint->color_item_price > 0 && count($orderItem->cartItem->cartItemImprints) > 1)
                                                    <tr>
                                                        <th class="info" scope="row">
                                                            <% $cartItemImprint->imprint_name %> Additional Color fee (per item)
                                                        </th>
                                                        @if(!is_null($cartItemImprint->cartItemImprintColors))
                                                            @if(count($cartItemImprint->cartItemImprintColors) > 1)
                                                                <td>$<% $cartItemImprint->color_item_price *
                                                                    (count($cartItemImprint->cartItemImprintColors) - 1)
                                                                    %>
                                                                </td>
                                                            @else
                                                                <td>$<% $cartItemImprint->color_item_price %></td>
                                                            @endif
                                                        @else
                                                            <td>$<% $cartItemImprint->color_item_price %></td>
                                                        @endif
                                                    </tr>
                                                @endif
                                                @if($cartItemImprint->setup_price > 0)
                                                    <tr>
                                                        <th class="info" scope="row">
                                                            <% $cartItemImprint->imprint_name %> Setup Fee
                                                        </th>
                                                        <td>$<% $cartItemImprint->setup_price %></td>
                                                    </tr>
                                                @endif
                                                @if($cartItemImprint->color_setup_price > 0 && count($orderItem->cartItem->cartItemImprints) > 1)
                                                    <tr>
                                                        <th class="info" scope="row">
                                                            <% $cartItemImprint->imprint_name %> Additional Color Setup
                                                        </th>
                                                        @if(!is_null($cartItemImprint->cartItemImprintColors))
                                                            @if(count($cartItemImprint->cartItemImprintColors) > 1)
                                                                <td>$<% $cartItemImprint->color_setup_price *
                                                                    (count($cartItemImprint->cartItemImprintColors) - 1)
                                                                    %>
                                                                </td>
                                                            @else
                                                                <td>$<% $cartItemImprint->color_setup_price %></td>
                                                            @endif
                                                        @else
                                                            <td>$<% $cartItemImprint->color_setup_price %></td>
                                                        @endif
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                        <tr>
                                            <th class="info" scope="row">Shipping Cost</th>
                                            @if($orderItem->estimation_shipping_price > 0)
                                                <td>$<% $orderItem->estimation_shipping_price %></td>
                                            @elseif(!is_null($orderItem->own_account_number))
                                                <td>$0.00 My Own Account</td>
                                            @else
                                                <td>TBD</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <th class="info" scope="row">Total Billed</th>
                                            <td>$<% number_format($cartItemCost->getTotal(), 2) %></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h5>Extra</h5>
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered table-hover table-info">
                                        <tbody>
                                        <tr>
                                            <th class="col-md-2 info" scope="row">User Comments</th>
                                            <td class="col-md-10">{!! $orderItem->imprint_comment !!}</td>
                                        </tr>
                                        <tr>
                                            <th class="col-md-2 info" scope="row">Own account data</th>
                                            <td class="col-md-10">Ship via: <% $orderItem->own_shipping_system %>,
                                                Account #: <% $orderItem->own_account_number %>, Method: <%
                                                $orderItem->own_shipping_type %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="col-md-2 info" scope="row">Shipping estimate</th>
                                            <td class="col-md-10">Zip code: <% $orderItem->estimation_zip %>, Method: <%
                                                $orderItem->estimation_shipping_method %>, Estimate cost: $<%
                                                $orderItem->estimation_shipping_price %>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <%--
                        <button type="submit" class="btn btn-primary">Submit</button>
                        --%>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <h5>Order Notes
                                    <a class="btn btn-default btn-xs" role="button" ng-click="showNotesForm()">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </a>
                                </h5>
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered table-hover table-info">
                                        <thead>
                                        <tr>
                                            <th class="info">Date</th>
                                            <th class="info">User</th>
                                            <th class="info">Note</th>
                                        </tr>
                                        </thead>
                                        <tbody ng-repeat="backNote in backNotes">
                                        <tr>
                                            <td>{{backNote.created_at|date:'short'}}</td>
                                            <td>{{backNote.user.name}}</td>
                                            <td>
                                                <div ng-bind-html="trustAsHtml(backNote.note)"></div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <h5>Art Proofs
                                    <a class="btn btn-default btn-xs" role="button" ng-click="showArtProofsForm()"><i
                                                class="fa fa-pencil" aria-hidden="true"></i></a>
                                </h5>
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered table-hover table-info">
                                        <thead>
                                        <tr>
                                            <th class="info">ID</th>
                                            <th class="info">Submitted On</th>
                                            <th class="info">User</th>
                                            <th class="info">File</th>
                                            <th class="info">Admin Note</th>
                                            <th class="info">Customer Note</th>
                                            <th class="info">Status</th>
                                            <th class="info">Date of Approval / Denial</th>
                                        </tr>
                                        </thead>
                                        <tbody ng-repeat="artProof in artProofs">
                                        <tr>
                                            <td>{{artProof.id}}</td>
                                            <td>{{artProof.created_at|date:'short'}}</td>
                                            <td>{{artProof.user.name}}</td>
                                            <td>
                                                <a target="_blank"
                                                   href="/admin/api/order_item/{{orderItem.id}}/art_proof/{{artProof.id}}/file">{{artProof.path}}</a>
                                            </td>
                                            <td>
                                                <div ng-bind-html="trustAsHtml(artProof.note)"></div>
                                            </td>
                                            <td>
                                                <div ng-bind-html="trustAsHtml(artProof.customer_note)"></div>
                                            </td>
                                            <td>{{artProof.status}}</td>
                                            <td>{{artProof.answered_at || ''}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="<% route('admin.upload') %>" target="form_target" method="post"
                          enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                               onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug"
                               value="<% $adminModel->getBaseRouteName() %>">
                        <input type="hidden" name="_token" value="<% csrf_token() %>">
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            var PRE_SELECT = <?php echo json_encode($collections); ?>

            $('.select-multiple').select2({
                minimumInputLength: 2,
                initSelection: function (element, callback) {
                    for (var x = 0; x < PRE_SELECT.length; x++) {
                        $('.select-multiple').append($("<option/>", {
                            value: PRE_SELECT[x].id,
                            text: PRE_SELECT[x].text,
                            selected: true
                        }));
                    }
                    callback(PRE_SELECT);
                },
                ajax: {
                    url: "<% $formLink %>",
                    dataType: 'json',
                    data: function (params) {
                        return {
                            query: params.term
                        };
                    },
                    results: function (data) {
                        return {results: data};
                    },
                    processResults: function (data) {
                        var answer = [];
                        for (var i = 0; i < data.length; i++) {
                            answer.push({
                                id: data[i].id,
                                text: data[i].name
                            });
                        }
                        return {
                            results: answer
                        };
                    },
                    cache: true
                },
            });
            $('.select-multiple').change();
        });
    </script>
    <script src="<% config('voyager.assets_path') %>/lib/js/tinymce/tinymce.min.js"></script>
    <script src="<% config('voyager.assets_path') %>/js/voyager_tinymce.js"></script>
@stop
