@extends('admin.base.index')

@section('title', 'Editable blocks')

@section('angular_controller', 'BlockListController')

@section('panel_content')
    <table class="table table-condensed table-bordered table-hover table-info">
        <thead>
        <tr>
            <th class="info">Name</th>
            <th class="info">Status</th>
            <th class="info">Actions</th>
        </tr>
        </thead>
        <tbody>
        <tr ng-repeat="item in listData">
            <td>{{item.display_name}}</td>
            <td class="full-width">
                <span ng-if="item.active"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span ng-if="item.active == 0"><i class="fa fa-times" aria-hidden="true"></i></span>
            </td>
            <td>
                <div class="buttons">
                    <button type="button" class="btn btn-default" ng-click="showEditAddForm(item)"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
@stop