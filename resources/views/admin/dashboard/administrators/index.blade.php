@extends('admin.base.index')

@section('title', 'Administrators')

@section('angular_controller', 'AdministratorController')

@section('panel_content')
    <table class="table table-condensed table-bordered table-hover table-info">
        <thead>
        <tr>
            <th class="info">id</th>
            <th class="info">Name</th>
            <th class="info">Login</th>
            <th class="info col-md-2">Can Delete</th>
            <th class="info col-md-2">Active</th>
            <th class="info">Actions</th>
        </tr>
        </thead>
        <tbody>
        <tr ng-repeat="item in listData">
            <td>{{item.id}}</td>
            <td>{{item.name}}</td>
            <td>{{item.email}}</td>
            <td>{{item.can_delete ? 'Yes' : 'No'}}</td>
            <td>{{item.active ? 'Yes' : 'No'}}</td>
            <td>
                <button type="button" class="btn btn-default" ng-click="showEditAddForm(item)"><i
                            class="fa fa-pencil" aria-hidden="true"></i></button>
                <button type="button" class="btn btn-danger" ng-click="deleteItem(item.id)"><i
                            class="voyager-trash"></i></button>
            </td>
        </tr>
        </tbody>
    </table>
    <button type="button" class="btn btn-default" ng-click="showEditAddForm()">Add new</button>
@stop
