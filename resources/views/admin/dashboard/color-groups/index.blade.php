@extends('admin.base.index')

@section('title', 'Color Groups')

@section('angular_controller', 'ColorGroupsListController')

@section('panel_content')
    <table class="table table-condensed table-bordered table-hover table-info">
        <thead>
            <tr>
                <th class="info">ID</th>
                <th class="info">Name</th>
                <th class="info">Colors</th>
                <th class="info">Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="item in listData">
                <td>
                    {{item.id}}
                </td>
                <td>
                    {{ item.name}}
                </td>
                <td>
                    <span ng-repeat="color in item.colors" title="{{color.name}}" class="color-block" ng-style="{'background-color': color.color_hex}" style="background-color: rgb(255, 0, 0); border: 1px solid #d9edf7"></span>
                </td>
                <td>
                    <button type="button" class="btn btn-default" ng-click="showEditAddForm(item)"><i
                                class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-danger" ng-click="deleteItem(item.id)"><i
                                class="voyager-trash"></i></button>
                </td>
            </tr>
        </tbody>
    </table>
    <button type="button" class="btn btn-default" ng-click="showEditAddForm()">Add new</button>
    <div>
        <ul uib-pagination items-per-page="pagination.per_page" ng-show="pagination.lastPage > 1" total-items="pagination.totalItems" ng-model="pagination.currentPage" max-size="pagination.maxSize" next-text="&raquo;" previous-text="&laquo;" class="pagination-sm" boundary-links="true" ng-click="getListData(pagination.currentPage)"></ul>
    </div>
@stop
