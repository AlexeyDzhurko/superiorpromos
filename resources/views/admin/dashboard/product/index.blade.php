@extends('admin.base.index')

@section('title', 'Products')

@section('angular_controller', 'ProductListController')

@section('panel_content')

    @include('admin.dashboard.product.search')
    <div ng-if="pagination.lastPage > 1">
        <span ng-click="getListData(pagination.currentPage - 1)" ng-if="pagination.currentPage > 1" style="color: #337ab7"><<</span>
        <select ng-change="getListData(pagination.currentPage)" ng-model="pagination.currentPage"
                ng-options="n as 'Page '+n for n in range">
        </select>
        of {{pagination.lastPage}} <span ng-click="getListData(pagination.currentPage + 1)" ng-if="pagination.currentPage < pagination.lastPage"  style="color: #337ab7">>></span>
    </div>
    <table class="table table-condensed table-bordered table-hover table-info">
        <thead>
        <tr>
            <th class="info" sortable-header field="id" current-sort="currentSort" change="sort">ID</th>
            <th class="info" sortable-header field="name_sort" current-sort="currentSort" change="sort">Name</th>
            <th class="info" sortable-header field="created_at" current-sort="currentSort" change="sort">Creation Date</th>
            <th class="info">Active</th>
            <th class="info">Images for Color</th>
            <th class="info">Vendor/SKU</th>
            <th class="info col-md-2">Actions</th>
        </tr>
        </thead>
        <tbody>
        <tr ng-repeat="item in listData">
            <td>
                {{item.id}}
            </td>
            <td>
                <a  href="/admin/product/{{item.id}}/edit">{{ item.name}}</a>
            </td>
            <td>{{item.created_at|date:'short'}}</td>
            <td ng-click="toggleActiveProduct(item)">
                <span ng-if="item.active"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span ng-if="item.active == 0"><i class="fa fa-times" aria-hidden="true"></i></span>
            </td>
            <td ng-class="{'bg-danger': item.colorImageStatus == 'none', 'bg-success': item.colorImageStatus == 'complete'}">
                <span>{{item.colorImageStatus}}</span>
            </td>
            <td>
                <span ng-repeat="vendor in item.vendors">{{getVendorTitle(vendor.vendor_id) || 'N/A'}}/{{vendor.sku || '-'}}
                    <span ng-hide="$last">, </span>
                </span>
            </td>
            <td>
                <form editable-form name="rowform" onaftersave="saveSlider($data, slider.id, slider)"
                      ng-show="rowform.$visible" class="form-buttons form-inline">
                    <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-primary">
                        save
                    </button>
                    <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()"
                            class="btn btn-default">
                        cancel
                    </button>
                </form>
                <div class="buttons" ng-show="!rowform.$visible">
                    <a  href="/admin/product/{{item.id}}/edit"><i
                                class="fa fa-pencil" aria-hidden="true"></i></a>
                    <a ng-click="deleteItem(item.id)"><i
                                class="voyager-trash"></i></a>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <div ng-if="pagination.lastPage > 1">
        <span ng-click="getListData(pagination.currentPage - 1)" ng-if="pagination.currentPage > 1" style="color: #337ab7"><<</span>
        <select ng-change="getListData(pagination.currentPage)" ng-model="pagination.currentPage"
                ng-options="n as 'Page '+n for n in range">
        </select>
        of {{pagination.lastPage}} <span ng-click="getListData(pagination.currentPage + 1)" ng-if="pagination.currentPage < pagination.lastPage"  style="color: #337ab7">>></span>
    </div>
    <button type="button" class="btn btn-default" onclick="location.href='/admin/product/create'">Add Product</button>

@stop
