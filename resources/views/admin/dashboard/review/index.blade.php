@extends('admin.base.index')

@section('title', 'Reviews')

@section('angular_controller', 'ReviewListController')

@section('panel_content')
    <button type="button" class="btn btn-default" ng-click="showEditAddForm(null)"><i class="fa fa-plus" aria-hidden="true"></i></button>
    <table class="table table-condensed table-bordered table-hover table-info">
        <thead>
        <tr>
            <th class="info">ID</th>
            <th class="info">Product</th>
            <th class="info">Customer</th>
            <th class="info">Rating</th>
            <th class="info">Review Date</th>
            <th class="info">Created Date</th>
            <th class="info">Approved</th>
            <th class="info">Actions</th>
        </tr>
        </thead>
        <tbody data-ng-model="listData">
        <tr ng-repeat="item in listData">
            <td>{{item.id}}</td>
            <td>{{item.product.title}} (# {{item.product.id}})</td>
            <td>{{item.reviewer_name}} (<a href="mailto:{{item.reviewer_email}}">{{item.reviewer_email}})</a></td>
            <td>{{item.rating}}</td>
            <td>{{item.review_date|date:'short'}}</td>
            <td>{{item.created_at|date:'short'}}</td>
            <td>
                <span ng-if="item.approve"><i class="fa fa-check" aria-hidden="true"></i></span>
                <span ng-if="item.approve == 0"><i class="fa fa-times" aria-hidden="true"></i></span>
                <span ng-if="item.approve == null"><i class="fa fa-question" aria-hidden="true"></i></span>
            </td>
            <td>
                <div class="buttons">
                    <button type="button" class="btn btn-default" ng-click="showEditAddForm(item)"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-danger" ng-click="deleteItem(item.id)"><i class="voyager-trash"></i></button>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <ul uib-pagination items-per-page="pagination.per_page" ng-show="pagination.lastPage > 1" total-items="pagination.totalItems" ng-model="pagination.currentPage" max-size="pagination.maxSize" next-text="&raquo;" previous-text="&laquo;" class="pagination-sm" boundary-links="true" ng-click="getListData(pagination.currentPage)">
    </ul>
@stop
