<form class="form-horizontal search-form" ng-submit="search()">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4 form-group form-group-sm">
                    <label for="id" class="col-md-4 control-label">ID</label>
                    <div class="col-md-8">
                        <input ng-model="searchData.id" type="text" class="form-control" id="id" placeholder="ID">
                    </div>
                </div>
                <div class="col-md-4 form-group form-group-sm">
                    <label for="name" class="col-md-4 control-label">Name contains</label>
                    <div class="col-md-8">
                        <input ng-model="searchData.name" type="text" class="form-control" id="name" placeholder="Name">
                    </div>
                </div>
                <div class="col-md-4 form-group form-group-sm">
                    <label for="active" class="col-md-4 control-label">Activity</label>
                    <div class="col-md-8">
                        <selectize placeholder="Activity" config='{valueField: "id", labelField: "text", maxItems: 1}' options='statuses' ng-model="searchData.active"></selectize>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 form-group form-group-sm">
                    <label for="date_from" class="col-md-4 control-label">Creation Date From</label>
                    <div class="col-md-8">
                        <span class="dropdown">
                            <a class="dropdown-toggle" id="date_from" role="button" data-toggle="dropdown" data-target="#" href="#">
                                <input name="date_from" id="date_from" placeholder="From" type="text" class="form-control" data-ng-model="searchData.date_from">
                            </a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                <datetimepicker data-ng-model="searchData.date_from" data-datetimepicker-config="{ dropdownSelector: '#date_from', modelType: 'YYYY-MM-DD', minView: 'day' }"/>
                            </ul>
                        </span>
                    </div>
                </div>
                <div class="col-md-4 form-group form-group-sm">
                    <label class="col-md-4 control-label" for="date_to">To</label>
                    <div class="col-md-8">
                        <span class="dropdown">
                            <a class="dropdown-toggle" id="date_to" role="button" data-toggle="dropdown" data-target="#" href="#">
                                <input name="date_to" id="date_to" placeholder="To" type="text" class="form-control" data-ng-model="searchData.date_to">
                            </a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                <datetimepicker data-ng-model="searchData.date_to" data-datetimepicker-config="{ dropdownSelector: '#date_to', modelType: 'YYYY-MM-DD', minView: 'day' }"/>
                            </ul>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-8">
                    <button type="submit" class="btn btn-default btn-sm">Search</button>
                    <button ng-click="resetSearch()" type="reset" class="btn btn-default btn-sm">Reset</button>
                    <button ng-click="clearSearch()" type="reset" class="btn btn-default btn-sm">Clear</button>
                </div>
            </div>
        </div>
    </div>
</form>
