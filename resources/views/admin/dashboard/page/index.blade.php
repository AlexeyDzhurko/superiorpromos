@extends('admin.base.index')

@section('title', 'Static Pages')

@section('angular_controller', 'PageListController')

@section('panel_content')
    <table class="table table-condensed table-bordered table-hover table-info">
        <thead>
        <tr>
            <th class="info">ID</th>
            <th class="info">Title</th>
            <th class="info">URL</th>
            <th class="info">Actions</th>
        </tr>
        </thead>
        <tbody data-ng-model="listData">
        <tr ng-repeat="item in listData">
            <td>{{item.id}}</td>
            <td>{{item.title}}</td>
            <td>{{item.slug}}</td>
            <td>
                <div class="buttons">
                    <button type="button" class="btn btn-default" ng-click="showEditAddForm(item)"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-danger" ng-click="deleteItem(item.id)"><i class="voyager-trash"></i></button>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <button type="button" class="btn btn-default" ng-click="showEditAddForm()">Add Page</button>
@stop