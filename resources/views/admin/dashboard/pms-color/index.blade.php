@extends('admin.layout')

@section('css')
    <meta name="csrf-token" content="<% csrf_token() %>">
    <link rel="stylesheet" href="/admin-assets/css/assets.min.css">
@stop


@section('page_header')
    <h1 class="page-title">
        PMS Colors
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid" ng-app="app" ng-controller="PmsColorListController">
        <div ng-hide="hideLoader" class="voyager-loader">
            <img src="<% config('voyager.assets_path') %>/images/logo-icon.png" alt="Voyager Loader">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">

                        <table class="table table-condensed table-bordered table-hover table-info">
                            <thead>
                                <tr>
                                    <th class="info">ID</th>
                                    <th class="info">Name</th>
                                    <th class="info">Color</th>
                                    <th class="info">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="pmsColor in pmsColors">
                                <td>{{pmsColor.id}}</td>
                                <td>
                                    <span editable-text="pmsColor.name" e-name="name" e-form="rowform">
                                      {{ pmsColor.name || 'empty' }}
                                    </span>
                                </td>
                                <td>
                                    <span class="color-block" ng-style="{'background-color': pmsColor.color_hex}" ></span>
                                    <span editable-text="pmsColor.color_hex" e-name="color_hex" e-form="rowform">
                                      {{ pmsColor.color_hex || 0 }}
                                    </span>
                                </td>
                                <td>
                                    <form editable-form name="rowform" onaftersave="savePmsColor($data, pmsColor.id, pmsColor)" ng-show="rowform.$visible" class="form-buttons form-inline" >
                                        <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-success">
                                            save
                                        </button>
                                        <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()" class="btn btn-default">
                                            cancel
                                        </button>
                                    </form>
                                    <div class="buttons" ng-show="!rowform.$visible">
                                        <button type="button" class="btn btn-default" ng-click="rowform.$show()"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                        <button type="button" class="btn btn-danger" ng-click="deletePmsColor(pmsColor.id)"><i class="voyager-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-default" ng-click="addPmsColor()">Add PMS Color</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
