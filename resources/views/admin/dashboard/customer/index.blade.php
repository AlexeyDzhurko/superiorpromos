@extends('admin.layout')

@section('css')
    <meta name="csrf-token" content="<% csrf_token() %>">
    <link rel="stylesheet" href="/admin-assets/css/assets.min.css">
@stop

@section('content')
    <div class="page-content container-fluid" ng-app="app" ng-controller="CustomerListController">
        <div ng-hide="hideLoader" class="voyager-loader">
            <img src="<% config('voyager.assets_path') %>/images/logo-icon.png" alt="Voyager Loader">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        @include('admin.dashboard.customer.search')
                        <table class="table table-condensed table-bordered table-hover table-info">
                            <thead>
                            <tr>
                                <th class="info" sortable-header field="id" current-sort="currentSort" change="sort">ID</th>
                                <th class="info" sortable-header field="email" current-sort="currentSort" change="sort">Email</th>
                                <th class="info" sortable-header field="name" current-sort="currentSort" change="sort">Contact Name</th>
                                <th class="info" sortable-header field="contact_telephone" current-sort="currentSort" change="sort">Contact Phone</th>
                                <th class="info">Active</th>
                                <th class="info">Orders</th>
                                <th class="info" sortable-header field="created_at" current-sort="currentSort" change="sort">Registered</th>
                                <th class="info">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="item in listData">
                                <td>
                                    {{item.id}}
                                </td>
                                <td>
                                    <a href="/admin/customer/{{item.id}}/edit">{{ item.email}}</a>
                                </td>
                                <td>
                                    {{ item.name}}
                                </td>
                                <td>
                                    {{ item.contact_telephone}}
                                </td>
                                <td>
                                    <span ng-if="item.active"><i class="fa fa-check" aria-hidden="true" style="color: blue;"></i></span>
                                    <span ng-if="item.active == 0"><i class="fa fa-times" aria-hidden="true"></i></span>
                                </td>
                                <td>
                                    <a href="/admin/order?user_id={{item.id}}" ng-if="item.orders_count > 0">Orders ({{item.orders_count}})</a>
                                    <span ng-if="item.orders_count == 0">No orders</span>
                                </td>
                                <td>{{item.created_at|date:'short'}}</td>
                                <td>
                                    <form editable-form name="rowform" onaftersave="saveSlider($data, slider.id, slider)"
                                          ng-show="rowform.$visible" class="form-buttons form-inline">
                                        <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-primary">
                                            save
                                        </button>
                                        <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()"
                                                class="btn btn-default">
                                            cancel
                                        </button>
                                    </form>
                                    <div class="buttons" ng-show="!rowform.$visible">
                                        <a  href="/admin/customer/{{item.id}}/edit"><i class="fa fa-pencil" aria-hidden="true" title="edit customer"></i></a>
                                        <i class="voyager-trash" ng-click="deleteItem(item.id)" title="delete customer"></i>
                                        <i class="fa fa-key" aria-hidden="true" ng-click="loginByUser(item.id)" title="login by customer"></i>
                                        <a href="mailto:{{item.email}}"><i class="fa fa-envelope" aria-hidden="true" title="send email for customer"></i></a>
                                        <a href="/admin/order?user_id={{item.id}}"><i class="fa fa-suitcase" aria-hidden="true" title="search customer orders"></i></a>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="row" ng-if="pagination.lastPage > 1">
                            <div class="col-md-9">
                                <span ng-click="getListData(pagination.currentPage - 1)" ng-if="pagination.currentPage > 1" style="color: #337ab7"><<</span>
                                <select ng-change="getListData(pagination.currentPage)" ng-model="pagination.currentPage"
                                        ng-options="n as 'Page '+n for n in range">
                                </select>
                                of {{pagination.lastPage}} <span ng-click="getListData(pagination.currentPage + 1)" ng-if="pagination.currentPage < pagination.lastPage"  style="color: #337ab7">>></span>
                            </div>
                            <div class="col-md-3">
                                <span>Go to page: </span>
                                <input class="" type="text" size="5" ng-model="listData.page" value="{{pagination.currentPage}}" placeholder="{{pagination.currentPage}}">
                                <input class="btn btn-default" type="button" ng-click="getListData(listData.page)" value="Submit">
                            </div>
                        </div>
                        <div class="center-text" ng-show="listData.length == 0"><h4>Search result is empty, try again with new parameters :(</h4></div>
                        <!-- <button type="button" class="btn btn-default" ng-click="showEditAddForm()">Add User</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
