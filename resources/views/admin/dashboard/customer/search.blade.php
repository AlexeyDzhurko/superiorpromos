<form class="form-horizontal search-form">
    <div class="row">
        <div class="col-md-12">
            <div class="row form-group">
                <div class="col-md-6 form-group form-group-sm">
                    <label for="id" class="col-md-4 control-label">ID</label>
                    <div class="col-md-8">
                        <input ng-model="searchData.id" type="text" class="form-control" id="id" placeholder="ID">
                    </div>
                </div>
                <div class="col-md-6 form-group form-group-sm">
                    <label for="contact_telephone" class="col-md-4 control-label">Contact Phone</label>
                    <div class="col-md-8">
                        <input ng-model="searchData.contact_telephone" type="text" class="form-control" id="contact_telephone" placeholder="Contact Phone">
                    </div>
                </div>
            </div>
            <div class="row form-group" style="margin-top: -10px">
                <div class="col-md-6 form-group form-group-sm">
                    <label for="email" class="col-md-4 control-label">Email contains</label>
                    <div class="col-md-8">
                        <input ng-model="searchData.email" type="text" class="form-control" id="email" placeholder="Email">
                    </div>
                </div>
                <div class="col-md-6 form-group form-group-sm">
                    <label for="billing_telephone" class="col-md-4 control-label">Billing Phone</label>
                    <div class="col-md-8">
                        <input ng-model="searchData.billing_telephone" type="text" class="form-control" id="billing_telephone" placeholder="Billing Phone">
                    </div>
                </div>
            </div>
            <div class="row form-group" style="margin-top: -10px">
                <div class="col-md-6 form-group form-group-sm">
                    <label for="company" class="col-md-4 control-label">Company name</label>
                    <div class="col-md-8">
                        <input ng-model="searchData.company" type="text" class="form-control" id="company" placeholder="Company Name">
                    </div>
                </div>
                <div class="col-md-6 form-group form-group-sm">
                    <label for="zip" class="col-md-4 control-label">Shipping Zip</label>
                    <div class="col-md-8">
                        <input ng-model="searchData.zip" type="text" class="form-control" id="zip" placeholder="Zip">
                    </div>
                </div>
            </div>
            <div class="row form-group" style="margin-top: -10px">
                <div class="col-md-6 form-group form-group-sm">
                    <label for="name" class="col-md-4 control-label">User Name</label>
                    <div class="col-md-8">
                        <input ng-model="searchData.name" type="text" class="form-control" id="name" placeholder="Name">
                    </div>
                </div>
                <div class="col-md-6 form-group form-group-sm">
                    <label for="date_from" class="col-md-4 control-label">Creation Date</label>
                    <div class="col-md-4">
                        <span class="dropdown">
                            <a class="dropdown-toggle" id="date_from" role="button" data-toggle="dropdown" data-target="#" href="#">
                                <input name="date_from" id="date_from" placeholder="From" type="text" class="form-control" data-ng-model="searchData.date_from">
                            </a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                <datetimepicker data-ng-model="searchData.date_from" data-datetimepicker-config="{ dropdownSelector: '#date_from', modelType: 'YYYY-MM-DD', minView: 'day' }"/>
                            </ul>
                        </span>
                    </div>
                    <div class="col-md-4">
                        <span class="dropdown">
                            <a class="dropdown-toggle" id="date_to" role="button" data-toggle="dropdown" data-target="#" href="#">
                                <input name="date_to" id="date_to" placeholder="To" type="text" class="form-control" data-ng-model="searchData.date_to">
                            </a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                <datetimepicker data-ng-model="searchData.date_to" data-datetimepicker-config="{ dropdownSelector: '#date_to', modelType: 'YYYY-MM-DD', minView: 'day' }"/>
                            </ul>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row form-group" style="margin-top: -10px">
                <div class="col-md-6 form-group form-group-sm">
                    <label for="frequency" class="col-md-4 control-label">Frequency:</label>
                    <div class="col-md-8" id="frequency">
                        <select ng-model="searchData.date_from" class="form-control">
                            <option ng-value="'<%\Carbon\Carbon::today()->toDateString()%>'">Today</option>
                            <option ng-value="'<%\Carbon\Carbon::today()->subDays(3)->toDateString()%>'">Last 3 days</option>
                            <option ng-value="'<%\Carbon\Carbon::today()->subDays(5)->toDateString()%>'">Last 5 days</option>
                            <option ng-value="'<%\Carbon\Carbon::today()->subDays(7)->toDateString()%>'">Last 7 days</option>
                            <option ng-value="'<%\Carbon\Carbon::today()->subDays(14)->toDateString()%>'">Last 14 days</option>
                            <option ng-value="'<%\Carbon\Carbon::today()->subDays(30)->toDateString()%>'">Last 30 days</option>
                            <option ng-value="'<%\Carbon\Carbon::today()->subDays(60)->toDateString()%>'">Last 60 days</option>
                            <option ng-value="'<%\Carbon\Carbon::today()->subDays(90)->toDateString()%>'">Last 90 days</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 form-group form-group-sm">
                    <label for="active" class="col-md-4 control-label">Activity</label>
                    <div class="col-md-8">
                        <selectize placeholder="Activity" config='{valueField: "id", labelField: "text", maxItems: 1}' options='statuses' ng-model="searchData.active"></selectize>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4" ng-if="pagination.lastPage > 1">
            <span ng-click="getListData(pagination.currentPage - 1)" ng-if="pagination.currentPage > 1" style="color: #337ab7"><<</span>
            <select ng-change="getListData(pagination.currentPage)" ng-model="pagination.currentPage"
                    ng-options="n as 'Page '+n for n in range">
            </select>
            of {{pagination.lastPage}} <span ng-click="getListData(pagination.currentPage + 1)" ng-if="pagination.currentPage < pagination.lastPage"  style="color: #337ab7">>></span>
        </div>
        <div class="col-md-4" ng-if="pagination.lastPage > 1">
            <span>Go to page: </span>
            <input class="" type="text" size="5" ng-model="listData.page" value="{{pagination.currentPage}}" placeholder="{{pagination.currentPage}}">
            <input class="btn btn-default" type="button" ng-click="getListData(listData.page)" value="Submit">
        </div>
        <div class="col-md-4">
            <button ng-click="search()" type="submit" class="btn btn-default btn-sm">Search</button>
            <button ng-click="resetSearch()" type="reset" class="btn btn-default btn-sm">Reset</button>
            <button ng-click="clearSearch()" type="reset" class="btn btn-default btn-sm">Clear</button>
        </div>
    </div>
</form>