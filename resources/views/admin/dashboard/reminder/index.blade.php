@extends('admin.base.index')

@section('title', 'Reminders')

@section('angular_controller', 'ReminderListController')

@section('panel_content')

    <form class="form-inline" ng-click="search()">

        <div class="form-group">
            <label for="date_from" class="control-label">Order Date From</label>
            <span class="dropdown">
                <a class="dropdown-toggle" id="date_from" role="button" data-toggle="dropdown" data-target="#" href="#">
                    <input name="date_from" id="date_from" placeholder="From" type="text" class="form-control" data-ng-model="searchData.date_from">
                </a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                    <datetimepicker data-ng-model="searchData.date_from" data-datetimepicker-config="{ dropdownSelector: '#date_from', modelType: 'YYYY-MM-DD', minView: 'day' }"/>
                </ul>
            </span>
        </div>

        <div class="form-group">
            <label for="date_from" class="control-label">To</label>
            <span class="dropdown">
                <a class="dropdown-toggle" id="date_to" role="button" data-toggle="dropdown" data-target="#" href="#">
                    <input name="date_to" id="date_to" placeholder="From" type="text" class="form-control" data-ng-model="searchData.date_to">
                </a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                    <datetimepicker data-ng-model="searchData.date_to" data-datetimepicker-config="{ dropdownSelector: '#date_to', modelType: 'YYYY-MM-DD', minView: 'day' }"/>
                </ul>
            </span>
        </div>

        <div class="form-group">
            <label for="user_id">Customer ID</label>
            <input ng-model="searchData.user_id" type="text" class="form-control" id="user_id" placeholder="Customer ID">
        </div>
        <div class="form-group">
            <label for="user_name">Name</label>
            <input ng-model="searchData.user_name" type="text" class="form-control" id="user_name" placeholder="Name">
        </div>



        <button type="submit" class="btn btn-default">Search</button>
    </form>

    <table class="table table-condensed table-bordered table-hover table-info">
        <thead>
        <tr>
            <th class="info">Order #</th>
            <th class="info">Date</th>
            <th class="info">Customer</th>
            <th class="info">Total</th>
            <th class="info">Status</th>
            <th class="info">Actions</th>
        </tr>
        </thead>
        <tbody data-ng-model="listData">
        <tr ng-repeat="item in listData">
            <td>{{item.id}}</td>
            <td>{{item.created_at|date:'short'}}</td>
            <td>{{item.billing_first_name}} {{item.billing_last_name}}</td>
            <td></td>
            <td>Not Sent</td>
            <td>

            </td>
        </tr>
        </tbody>
    </table>
@stop