@extends('admin.base.index')

@section('title', 'Colors')

@section('angular_controller', 'ColorsListController')

@section('panel_content')
    <table class="table table-condensed table-bordered table-hover table-info">
        <thead>
        <tr>
            <th class="info">ID</th>
            <th class="info">Name</th>
            <th class="info col-md-2">Color Hex</th>
            <th class="info col-1">Color</th>
            <th class="info">Pic. width</th>
            <th class="info">Pic. height</th>
            <th class="info">Actions</th>
        </tr>
        </thead>
        <tbody>
        <tr ng-repeat="item in listData">
            <td>
                {{item.id}}
            </td>
            <td>
                {{ item.name}}
            </td>
            <td>
                {{ item.color_hex}}
            </td>
            <td ng-style="{'background-color': item.color_hex}"></td>
            <td>
                {{ item.picture_width}}
            </td>
            <td>
                {{ item.picture_height}}
            </td>
            <td>
                <button type="button" class="btn btn-default" ng-click="showEditAddForm(item)"><i
                            class="fa fa-pencil" aria-hidden="true"></i></button>
                <button type="button" class="btn btn-danger" ng-click="deleteItem(item.id)"><i
                            class="voyager-trash"></i></button>
            </td>
        </tr>
        </tbody>
    </table>
    <button type="button" class="btn btn-default" ng-click="showEditAddForm()">Add new</button>
    <div>
        <ul uib-pagination items-per-page="pagination.per_page" ng-show="pagination.lastPage > 1" total-items="pagination.totalItems" ng-model="pagination.currentPage" max-size="pagination.maxSize" next-text="&raquo;" previous-text="&laquo;" class="pagination-sm" boundary-links="true" ng-click="getListData(pagination.currentPage)"></ul>
    </div>
@stop
