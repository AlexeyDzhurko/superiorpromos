@extends('admin.base.index')

@section('title', 'Post Categories')

@section('angular_controller', 'PostCategoryController')

@section('panel_content')
    <table class="table table-condensed table-bordered table-hover table-info">
        <thead>
        <tr>
            <th class="info">ID</th>
            <th class="info">Title</th>
            <th class="info">Date</th>
            <th class="info col-md-2">Actions</th>
        </tr>
        </thead>
        <tbody>
        <tr ng-repeat="item in listData">
            <td>
                {{item.id}}
            </td>
            <td class="col-md-2">
                {{ item.title}}
            </td>
            <td class="col-md-2">{{item.created_at|date:'short'}}</td>
            <td>
                <div class="buttons" ng-show="!rowform.$visible">
                    <button type="button" class="btn btn-default" ng-click="showEditAddForm(item, listData)"><i
                                class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-danger" ng-click="deleteItem(item.id)"><i
                                class="voyager-trash"></i></button>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <button type="button" class="btn btn-default" ng-click="showEditAddForm()">Add Post</button>
    <div>
    <ul uib-pagination items-per-page="pagination.per_page" ng-show="pagination.lastPage > 1" total-items="pagination.totalItems" ng-model="pagination.currentPage" max-size="pagination.maxSize" next-text="&raquo;" previous-text="&laquo;" class="pagination-sm" boundary-links="true" ng-click="getListData(pagination.currentPage)">
    </ul>
    </div>
@stop
