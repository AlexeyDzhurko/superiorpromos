@extends('admin.base.index')

@section('title', 'Promos')

@section('angular_controller', 'PromosController')

@section('panel_content')
    <table class="table table-condensed table-bordered table-hover table-info">
        <thead>
        <tr>
            <th class="info">Name</th>
            <th class="info">Preview</th>
            <th class="info">Actions</th>
        </tr>
        </thead>
        <tbody>
            <tr ng-repeat="item in listData.left">
                <td>{{ item.title }}</td>
                <td class="full-width">
                    <div class="preview-image-wrapper">
                        <img class="preview-image" ng-src="{{ item.image_src }}" alt="{{ item.title + ' image' }}">
                        <!-- <div class="preview-delete-button" ng-if="item.image_src" ng-click="deletePreview(item.path)">
                            <span class="glyphicon glyphicon-remove"></span>
                        </div> -->
                    </div>
                </td>
                <td>
                    <div class="buttons">
                        <button type="button" class="btn btn-default" ng-click="showEditAddForm(item)"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    </div>
                </td>
            </tr>
            <tr ng-repeat="item in listData.center">
                <td>{{ item.title }}</td>
                <td class="full-width">
                    <div class="preview-image-wrapper">
                        <img class="preview-image" ng-src="{{ item.image_src }}" alt="{{ item.title + ' image' }}">
                        <!-- <div class="preview-delete-button" ng-if="item.image_src" ng-click="deletePreview(item.path)">
                            <span class="glyphicon glyphicon-remove"></span>
                        </div> -->
                    </div>
                </td>
                <td>
                    <div class="buttons">
                        <button type="button" class="btn btn-default" ng-click="showEditAddForm(item)"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    </div>
                </td>
            </tr>
            <tr ng-repeat="item in listData.right">
                <td>{{ item.title }}</td>
                <td class="full-width">
                    <div class="preview-image-wrapper">
                        <img class="preview-image" ng-src="{{ item.image_src }}" alt="{{ item.title + ' image' }}">
                        <!-- <div class="preview-delete-button" ng-if="item.image_src" ng-click="deletePreview(item.path)">
                            <span class="glyphicon glyphicon-remove"></span>
                        </div> -->
                    </div>
                </td>
                <td>
                    <div class="buttons">
                        <button type="button" class="btn btn-default" ng-click="showEditAddForm(item)"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
@stop