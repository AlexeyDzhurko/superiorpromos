@extends('admin.base.index')

@section('title', 'Product Icons')

@section('angular_controller', 'ProductIconListController')

@section('panel_content')
    <table class="table table-condensed table-bordered table-hover table-info">
        <thead>
        <tr>
            <th class="info">ID</th>
            <th class="info">Name</th>
            <th class="info">Image</th>
            <th class="info">Position</th>
            <th class="info">Actions</th>
        </tr>
        </thead>
        <tbody data-ng-model="listData">
        <tr ng-repeat="item in listData">
            <td>{{item.id}}</td>
            <td>{{item.name}}</td>
            <td>
                <img class="img-thumbnail admin-thumbnail" ng-src="{{item.image_src}}"/>
            </td>
            <td>{{getPrettyPosition(item.position)}}</td>
            <td>
                <div class="buttons">
                    <button type="button" class="btn btn-default" ng-click="showEditAddForm(item)"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-danger" ng-click="deleteItem(item.id)"><i class="voyager-trash"></i></button>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <button type="button" class="btn btn-default" ng-click="showEditAddForm()">Add Product Icon</button>
@stop