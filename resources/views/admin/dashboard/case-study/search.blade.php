<form class="form-inline search-form" ng-click="search()">

    <div class="row">
        <div class="col-md-12">
            <div class="form-group form-group-sm">
                <label for="id">ID</label>
                <input ng-model="searchData.id" type="text" class="form-control" id="id" placeholder="ID">
            </div>
            <div class="form-group form-group-sm">
                <label for="title">Title contains</label>
                <input ng-model="searchData.title" type="text" class="form-control" id="title" placeholder="Title">
            </div>

            <div class="form-group form-group-sm">
                <label for="active">Activity</label>
                <select ng-model="searchData.active" class="form-control"
                        ng-options="option.name for option in statuses track by option.value">
                </select>
            </div>
        </div>
        <div class="col-md-12">

            <div class="form-group form-group-sm">
                <label for="date_from" class="control-label">Creation Date From</label>
                <span class="dropdown">
                        <a class="dropdown-toggle" id="date_from" role="button" data-toggle="dropdown" data-target="#" href="#">
                            <input name="date_from" id="date_from" placeholder="From" type="text" class="form-control" data-ng-model="searchData.date_from">
                        </a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                            <datetimepicker data-ng-model="searchData.date_from" data-datetimepicker-config="{ dropdownSelector: '#date_from', modelType: 'YYYY-MM-DD', minView: 'day' }"/>
                        </ul>
                    </span>
            </div>
            <div class="form-group form-group-sm">
                <label for="date_from" class="control-label">To</label>
                <span class="dropdown">
                        <a class="dropdown-toggle" id="date_to" role="button" data-toggle="dropdown" data-target="#" href="#">
                            <input name="date_to" id="date_to" placeholder="From" type="text" class="form-control" data-ng-model="searchData.date_to">
                        </a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                            <datetimepicker data-ng-model="searchData.date_to" data-datetimepicker-config="{ dropdownSelector: '#date_to', modelType: 'YYYY-MM-DD', minView: 'day' }"/>
                        </ul>
                    </span>
            </div>
            <button type="submit" class="btn btn-default btn-sm">Search</button>
            <button type="reset" class="btn btn-default btn-sm">Reset</button>
        </div>
    </div>

</form>