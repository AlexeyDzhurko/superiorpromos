@extends('admin.layout')

@section('css')
    <meta name="csrf-token" content="<% csrf_token() %>">
    <link rel="stylesheet" href="/admin-assets/css/assets.min.css">
@stop

@section('content')
    <div class="page-content container-fluid" ng-app="app" ng-controller="SliderListController">
        <div ng-hide="hideLoader" class="voyager-loader">
            <img src="<% config('voyager.assets_path') %>/images/logo-icon.png" alt="Voyager Loader">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">


                        <table class="table table-condensed table-bordered table-hover table-info">
                            <thead>
                            <tr>
                                <th class="info">ID</th>
                                <th class="info">Image</th>
                                <th class="info">URL</th>
                                <th class="info">Actions</th>
                            </tr>
                            </thead>
                            <tbody data-as-sortable="dragControlListeners" data-ng-model="sliders">
                            <tr ng-repeat="slider in sliders" data-as-sortable-item>
                                <td data-as-sortable-item-handle>
                                    <i class="fa fa-sort drag-icon" aria-hidden="true"></i>{{slider.id}}
                                </td>
                                <td>
                                    <div ng-show="rowform.$visible" ngf-drop ngf-select ng-model="slider.image_file" class="drop-box" name="slider.image_file"
                                         ngf-drag-over-class="'dragover'" ngf-multiple="false">
                                        Drop file to upload</div>
                                    <img ngf-thumbnail="slider.image_file" class="img-thumbnail admin-thumbnail" >
                                    <img ng-if="!slider.image_file" class="img-thumbnail admin-thumbnail" ng-src="{{slider.image_src}}"/>
                                </td>
                                <td>
                                    <span editable-text="slider.url" e-name="url" e-form="rowform">
                                      {{ slider.url || 'empty' }}
                                    </span>
                                </td>
                                <td>
                                    <form editable-form name="rowform" onaftersave="saveSlider($data, slider.id, slider)" ng-show="rowform.$visible" class="form-buttons form-inline" >
                                        <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-primary">
                                            save
                                        </button>
                                        <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()" class="btn btn-default">
                                            cancel
                                        </button>
                                    </form>
                                    <div class="buttons" ng-show="!rowform.$visible">
                                        <button type="button" class="btn btn-default" ng-click="rowform.$show()"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                        <button type="button" class="btn btn-danger" ng-click="deleteSlider(slider.id)"><i class="voyager-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-default" ng-click="addSlider()">Add Slide</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
