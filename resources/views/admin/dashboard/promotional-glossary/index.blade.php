@extends('admin.layout')

@section('css')
    <meta name="csrf-token" content="<% csrf_token() %>">
    <link rel="stylesheet" href="/admin-assets/css/assets.min.css">
@stop

@section('content')
    <div class="page-content container-fluid" ng-app="app" ng-controller="PromotionalGlossaryListController">
        <div ng-hide="hideLoader" class="voyager-loader">
            <img src="<% config('voyager.assets_path') %>/images/logo-icon.png" alt="Voyager Loader">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">

                        <table class="table table-condensed table-bordered table-hover table-info">
                            <thead>
                            <tr>
                                <th class="info">ID</th>
                                <th class="info">Subject</th>
                                <th class="info">Actions</th>
                            </tr>
                            </thead>
                            <tbody data-as-sortable="dragControlListeners" data-ng-model="listData">
                            <tr ng-repeat="item in listData" data-as-sortable-item>
                                <td data-as-sortable-item-handle>
                                    <i class="fa fa-sort drag-icon" aria-hidden="true"></i>{{item.id}}
                                </td>
                                <td>
                                    {{ item.title}}
                                </td>
                                <td>
                                    <form editable-form name="rowform" onaftersave="saveSlider($data, slider.id, slider)" ng-show="rowform.$visible" class="form-buttons form-inline" >
                                        <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-primary">
                                            save
                                        </button>
                                        <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()" class="btn btn-default">
                                            cancel
                                        </button>
                                    </form>
                                    <div class="buttons" ng-show="!rowform.$visible">
                                        <button type="button" class="btn btn-default" ng-click="showEditAddForm(item)"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                        <button type="button" class="btn btn-danger" ng-click="deleteItem(item.id)"><i class="voyager-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-default" ng-click="showEditAddForm()">Add Promotional Glossary</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
