@extends('admin.base.index')

@section('title', 'Testimonials')

@section('angular_controller', 'TestimonialsListController')

@section('panel_content')
    <table class="table table-condensed table-bordered table-hover table-info">
        <thead>
        <tr>
            <th class="info" sortable-header field="id" current-sort="currentSort" change="sort">ID</th>
            <th class="info" sortable-header field="name_sort" current-sort="currentSort" change="sort">Item Name</th>
            <th class="info" sortable-header field="created_at" current-sort="currentSort" change="sort">Creation Date</th>
            <th class="info col-md-2">Image</th>
            <th class="info col-md-2">Actions</th>
        </tr>
        </thead>
        <tbody>
        <tr ng-repeat="item in listData">
            <td>
                {{item.id}}
            </td>
            <td>
                {{ item.item_name}}
            </td>
            <td>{{item.created_at|date:'short'}}</td>
            <td><img ng-if="item.image" class="img-thumbnail admin-thumbnail" ng-src="/uploads/{{item.image}}"/></td>
            <td>
                <form editable-form name="rowform" onaftersave="saveSlider($data, slider.id, slider)"
                      ng-show="rowform.$visible" class="form-buttons form-inline">
                    <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-primary">
                        save
                    </button>
                    <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()"
                            class="btn btn-default">
                        cancel
                    </button>
                </form>
                <div class="buttons" ng-show="!rowform.$visible">
                    <button type="button" class="btn btn-default" ng-click="showEditAddForm(item, listData)"><i
                                class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-danger" ng-click="deleteItem(item.id)"><i
                                class="voyager-trash"></i></button>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <button type="button" class="btn btn-default" ng-click="showEditAddForm()">Add Template</button>
    <div>
    <ul uib-pagination items-per-page="pagination.per_page" ng-show="pagination.lastPage > 1" total-items="pagination.totalItems" ng-model="pagination.currentPage" max-size="pagination.maxSize" next-text="&raquo;" previous-text="&laquo;" class="pagination-sm" boundary-links="true" ng-click="getListData(pagination.currentPage)">
    </ul>
    </div>
@stop
