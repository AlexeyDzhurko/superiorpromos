@extends('admin.layout')

@section('css')
    <meta name="csrf-token" content="<% csrf_token() %>">
    <link rel="stylesheet" href="/admin-assets/css/assets.min.css">
@stop


@section('page_header')
    <h1 class="page-title">
        Tax States
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid" ng-app="app" ng-controller="StateListController">
        <div ng-hide="hideLoader" class="voyager-loader">
            <img src="<% config('voyager.assets_path') %>/images/logo-icon.png" alt="Voyager Loader">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">

                        <table class="table table-condensed table-bordered table-hover table-info">
                            <thead>
                                <tr>
                                    <th class="info">ID</th>
                                    <th class="info">Name</th>
                                    <th class="info">Tax</th>
                                    <th class="info">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="state in states">
                                <td>{{state.abbr}}</td>
                                <td>{{state.name}}</td>
                                <td>
                                    <span editable-text="state.tax" e-name="tax" e-form="rowform">
                                      {{ state.tax || 0 }}
                                    </span>
                                </td>
                                <td>
                                    <form editable-form name="rowform" onaftersave="saveState($data, state.id)" ng-show="rowform.$visible" class="form-buttons form-inline" >
                                        <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-primary">
                                            save
                                        </button>
                                        <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()" class="btn btn-default">
                                            cancel
                                        </button>
                                    </form>
                                    <div class="buttons" ng-show="!rowform.$visible">
                                        <button type="button" class="btn btn-primary" ng-click="rowform.$show()">edit</button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>



                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
