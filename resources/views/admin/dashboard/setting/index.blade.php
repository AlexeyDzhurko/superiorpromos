@extends('admin.base.index')

@section('title', 'Site settings')

@section('angular_controller', 'SettingListController')

@section('panel_content')
    <table class="table table-condensed table-bordered table-hover table-info">
        <thead>
        <tr>
            <th class="info">Parameter</th>
            <th class="info">Value</th>
            <th class="info">Actions</th>
        </tr>
        </thead>
        <tbody ng-repeat="(name, items) in listData">
            <tr>
                <td class="lead" colspan="3">{{name}}</td>
            </tr>
            <tr ng-repeat="item in items">
                <td>{{item.display_name}}</td>
                <td class="full-width">
                    <span editable-text="item.value" e-name="value" e-form="rowform">
                        {{ item.value || 'empty' }}
                    </span>
                </td>
                <td>
                    <form editable-form name="rowform" onaftersave="saveItem($data, item.id)" ng-show="rowform.$visible" class="form-buttons form-inline" >
                        <button type="submit" ng-disabled="rowform.$waiting" class="btn btn-success">
                            Save
                        </button>
                        <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()" class="btn btn-default">
                            Cancel
                        </button>
                    </form>
                    <div class="buttons" ng-show="!rowform.$visible">
                        <button type="button" class="btn btn-default" ng-click="rowform.$show()"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
@stop