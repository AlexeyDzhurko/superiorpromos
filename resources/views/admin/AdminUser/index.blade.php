@extends('admin.layout')

@section('content')
<div class="page-content container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <table id="dataTable" class="table table-hover">
                        <thead>
                        <tr>
                            @foreach($adminModel->showFields() as $formField)
                            <th>{{ $formField->displayName }}</th>
                            @endforeach
                            <th class="actions">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($dataTypeContent as $data)
                        <tr>
                            @foreach($adminModel->showFields() as $formField)
                            <td>
                                @if($formField->type == 'bool')
                                    @if($data->{$formField->field})
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                    @else
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    @endif
                                @else
                                    {{ $data->{$formField->field} }}
                                @endif
                            </td>
                            @endforeach
                            <td class="no-sort no-click">
                                <div class="btn-sm btn-danger pull-right delete" data-id="{{ $data->id }}"
                                     id="delete-{{ $data->id }}">
                                    <i class="voyager-trash"></i> Delete
                                </div>
                                <a href="{{ route('admin_entity.edit', ['admin_entity' => $adminModel->getBaseRouteName(), 'id' => $data->id]) }}"
                                   class="btn-sm btn-primary pull-right edit"><i class="voyager-edit"></i> Edit</a>
                                <a href="{{ route('admin_entity.show', ['admin_entity' => $adminModel->getBaseRouteName(), 'id' => $data->id])  }}"
                                   class="btn-sm btn-warning pull-right"><i class="voyager-eye"></i> View</a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="center-text">
                {!! $dataTypeContent->render() !!}
            </div>
        </div>
    </div>
</div>
</div>
{!! View::make('admin.blocks.modal', ['adminModel' => $adminModel]) !!}
@stop
