<ul class="nav navbar-nav">
    <li class="dropdown">

        <a data-toggle="collapse" href="#dynamic-section-dropdown-element">
            <span class="icon glyphicon glyphicon-th-list"></span>
            <span class="title">Dynamic Sections</span>
            <span class="site-menu-arrow"></span>
        </a>

        <div id="dynamic-section-dropdown-element" class="panel-collapse collapse">
            <div class="panel-body">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'product']) %>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Product</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'testimonials'])%>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Testimonials</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'slider']) %>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Main Page Carousel</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('category.index') %>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Categories</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'faqs']) %>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">FAQ</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'case-study'])%>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Case Studies</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'state'])%>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Tax States</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'size-group']) %>">
                            <span class="icon voyager-list"></span>
                            <span class="title">Size Group</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'pms-color'])%>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">PMS Colors</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'vendors']) %>">
                            <span class="icon glyphicon glyphicon-user"></span>
                            <span class="title">Vendors</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'product-icon'])%>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Product Icons</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link"
                           href="<% route('dashboard.index', ['name' => 'promotional-glossary'])%>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Promotional Glossary</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <a data-toggle="collapse" href="#misc-section-dropdown-element">
            <span class="icon glyphicon glyphicon-th-list"></span>
            <span class="title">Misc Section</span>
            <span class="site-menu-arrow"></span>
        </a>

        <div id="misc-section-dropdown-element" class="panel-collapse collapse">
            <div class="panel-body">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'survey']) %>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Surveys</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link"
                           href="<% route('dashboard.index', ['name' => 'payment-information-template']) %>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Payment Information Templates</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('froogle.index') %>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Froogle</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'review'])%>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Reviews</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <a data-toggle="collapse" href="#cms-section-dropdown-element">
            <span class="icon glyphicon glyphicon-th-list"></span>
            <span class="title">CMS Section</span>
            <span class="site-menu-arrow"></span>
        </a>

        <div id="cms-section-dropdown-element" class="panel-collapse collapse">
            <div class="panel-body">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'info-message']) %>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Info Messages</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'block']) %>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Editable Blocks</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>


        <a data-toggle="collapse" href="#products-dropdown-element">
            <span class="icon glyphicon glyphicon-qrcode"></span>
            <span class="title">Product Block</span>
            <span class="site-menu-arrow"></span>
        </a>
        <div id="products-dropdown-element" class="panel-collapse collapse">
            <div class="panel-body">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'product']) %>">
                            <span class="icon glyphicon glyphicon-gift"></span>
                            <span class="title">Product</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% url('/admin/product-category') %>">
                            <span class="icon glyphicon glyphicon-random"></span>
                            <span class="title">Product&Categories</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'color-groups']) %>">
                            <span class="icon glyphicon glyphicon-align-center"></span>
                            <span class="title">Color Group</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'colors']) %>">
                            <span class="icon glyphicon glyphicon-tint"></span>
                            <span class="title">Color</span>
                        </a>
                    </li>

                    <li>
                        <a class="animsition-link" href="<% route('category.index') %>">
                            <span class="icon glyphicon glyphicon-copyright-mark"></span>
                            <span class="title">Category</span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
        <a data-toggle="collapse" href="#sage-dropdown-element">
            <span class="icon glyphicon glyphicon-equalizer"></span>
            <span class="title">Sage Block</span>
            <span class="site-menu-arrow"></span>
        </a>
        <div id="sage-dropdown-element" class="panel-collapse collapse">
            <div class="panel-body">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="animsition-link" href="<% route('sage_category.index')%>">
                            <span class="icon glyphicon glyphicon-menu-hamburger"></span>
                            <span class="title">Category</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('sage_supplier.index')%>">
                            <span class="icon glyphicon glyphicon-ruble"></span>
                            <span class="title">Import product</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <a data-toggle="collapse" href="#section-dropdown-element">
            <span class="icon glyphicon glyphicon-th-list"></span>
            <span class="title">Sections</span>
            <span class="site-menu-arrow"></span>
        </a>
        <div id="section-dropdown-element" class="panel-collapse collapse">
            <div class="panel-body">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'faqs']) %>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">FAQ</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'setting'])%>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Settings</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="panel-body">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'testimonials']) %>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Testimonials</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="panel-body">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="animsition-link" href="<% route('promotional_block.index')%>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Promotional products</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="panel-body">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="animsition-link" href="<% route('company_info.index')%>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Company Info</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <a data-toggle="collapse" href="#user-dropdown-element">
            <span class="icon glyphicon glyphicon-user"></span>
            <span class="title">Users</span>
            <span class="site-menu-arrow"></span>
        </a>
        <div id="user-dropdown-element" class="panel-collapse collapse">
            <%--<div class="panel-body">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="animsition-link" href="<% route('admin_user.index')%>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Administrators/Managers</span>
                        </a>
                    </li>
                </ul>
            </div>--%>
            <div class="panel-body">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'customer']) %>">
                            <span class="icon glyphicon glyphicon-minus"></span>
                            <span class="title">Customers</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <a data-toggle="collapse" href="#orders-dropdown-element">
            <span class="icon glyphicon voyager-shop"></span>
            <span class="title">Orders</span>
            <span class="site-menu-arrow"></span>
        </a>
        <div id="orders-dropdown-element" class="panel-collapse collapse">
            <div class="panel-body">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="animsition-link" href="<% route('order.index')%>">
                            <span class="title">Orders</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('order_sample.index')%>">
                            <span class="title">Orders a sample</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('discount.index')%>">
                            <span class="title">Discounts</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <a data-toggle="collapse" href="#blog-dropdown-element">
            <span class="icon glyphicon voyager-font"></span>
            <span class="title">Blog</span>
            <span class="site-menu-arrow"></span>
        </a>
        <div id="blog-dropdown-element" class="panel-collapse collapse">
            <div class="panel-body">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'post']) %>">
                            <span class="icon glyphicon glyphicon-pencil"></span>
                            <span class="title">Posts</span>
                        </a>
                    </li>
                    <li>
                        <a class="animsition-link" href="<% route('dashboard.index', ['name' => 'post-category']) %>">
                            <span class="icon glyphicon glyphicon-tag"></span>
                            <span class="title">Post Categories</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </li>
</ul>