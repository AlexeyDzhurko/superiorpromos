<div class="form-group"
     ng-class="{'has-error': <% $model %>Form.<% $field %>.$error.serverError}">
    <label class="control-label col-sm-3" for="<% $field %>"><% isset($label) ? $label : ucfirst($field) %></label>
    <div class="col-sm-9">
        <input ng-model="<% $model %>.<% $field %>" id="<% $field %>"
               name="<% $field %>" class="form-control"
               placeholder="<% isset($label) ? $label : ucfirst($field) %>">
    </div>
    <span class="help-block"
          ng-show="<% $model %>Form.<% $field %>.$error.serverMessage">{{<% $model %>Form.<% $field %>.$error.serverMessage}}</span>
</div>