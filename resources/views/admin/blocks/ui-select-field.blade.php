<div class="form-group"
     ng-class="{'has-error': <% $model %>Form.<% $field %>.$error.serverError}">
    <label class="control-label col-sm-3"
           for="gender"><% isset($label) ? $label : ucfirst($field) %></label>
    <div class="col-sm-9">
        <ui-select name="<% $field %>" ng-model="<% $model %>.<% $field %>">
            <ui-select-match
                    placeholder="<% isset($label) ? $label : ucfirst($field) %>">{{$select.selected.name}}</ui-select-match>
            <ui-select-choices
                    repeat="option.id as option in <% $options %> | filter: $select.search| limitTo: 50">
                <span ng-bind="option.name"></span>
            </ui-select-choices>
        </ui-select>
    </div>
    <span class="help-block"
          ng-show="<% $model %>Form.<% $field %>.$error.serverMessage">{{<% $model %>Form.<% $field %>.$error.serverMessage}}</span>
</div>