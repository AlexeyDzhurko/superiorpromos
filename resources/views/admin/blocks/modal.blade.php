<div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="voyager-trash"></i> Are you sure you want to delete
                    this {{ $adminModel->getDisplayNameSingular() }}?</h4>
            </div>
            <div class="modal-footer">
                <form action="{{ route('admin_entity.index', ['admin_entity' => $adminModel->getBaseRouteName()]) }}" id="delete_form" method="POST">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="submit" class="btn btn-danger pull-right delete-confirm"
                           value="Yes, Delete This {{ $adminModel->getDisplayNameSingular() }}">
                </form>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->