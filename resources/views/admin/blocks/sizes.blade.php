<h1 class="page-title">
    <i class="{{ $adminModel->getIcon() }}"></i> {{ $adminModel->getDisplayNamePlural() }} <a
            href="{{ route('admin_entity.create', ['admin_entity' => $adminModel->getBaseRouteName()]) }}" class="btn btn-success"><i class="voyager-plus"></i> Add
        New</a>
</h1>
<div class="page-content container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <table id="dataTable" class="table table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th class="actions">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($sizes as $data)
                            <tr>
                                <td>
                                    {{ $data->id }}
                                </td>
                                <td>
                                    {{ $data->name }}
                                </td>
                                <td class="no-sort no-click">
                                    <div class="btn-sm btn-danger pull-right delete" data-id="{{ $data->id }}"
                                         id="delete-{{ $data->id }}">
                                        <i class="voyager-trash"></i> Delete
                                    </div>
                                    <a href="{{ route('admin_entity.edit', ['admin_entity' => $adminModel->getBaseRouteName(), 'id' => $data->id]) }}"
                                       class="btn-sm btn-primary pull-right edit"><i class="voyager-edit"></i> Edit</a>
                                    <a href="{{ route('admin_entity.show', ['admin_entity' => $adminModel->getBaseRouteName(), 'id' => $data->id])  }}"
                                       class="btn-sm btn-warning pull-right"><i class="voyager-eye"></i> View</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>