@extends('admin.layout')

@section('css')
    <meta name="csrf-token" content="<% csrf_token() %>">
    <link rel="stylesheet" href="/admin-assets/css/assets.min.css">
@stop

@section('page_header')
    <h1 class="page-title">
        Edit Customer
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid" ng-app="app" ng-controller="AddressesListController">
        <script>
            user = {!! $customer->toJson() !!};
        </script>
        <div class="page-content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- form start -->
                    <form role="form" ng-controller="CustomerController" ng-submit="updateCustomer()" method="POST"
                          enctype="multipart/form-data">
                        <div class="panel panel-bordered">
                            <div class="panel-heading">
                                <h3 class="panel-title">Edit Customer (ID: {{user.id}}) (send mail <a href="mailto:<% $customer->email %>"><i class="fa fa-envelope" aria-hidden="true"></i></a>)</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="name"><b>Contact Name</b></label>
                                    <input type="text" class="form-control" name="name" ng-model="user.name" placeholder="Customer Name"
                                           value="{{user.name}}">
                                </div>
                                <div class="form-group">
                                    <label for="name"><b>Email</b></label>
                                    <input type="text" class="form-control" name="email" ng-model="user.email" placeholder="Email"
                                           value="{{user.email}}">
                                </div>
                                <div class="form-group">
                                    <label for="name"><b>Password</b></label>
                                    <br>
                                    <small>Leave empty to keep the same</small>
                                    <input type="password" class="form-control" name="password" ng-model="user.password" placeholder="**********">
                                </div>
                                <div class="form-group">
                                    <label for="name"><b>Contact telephone</b></label>
                                    <input type="text" class="form-control" name="contact_telephone"
                                           ng-model="user.contact_telephone" placeholder="Contact telephone" value="{{user.contact_telephone}}">
                                </div>
                                <div class="form-group">
                                    <label for="name"><b>Contact Phone Number Extention</b></label>
                                    <input type="text" class="form-control" name="ext"
                                           ng-model="user.ext" placeholder="Contact Phone Number Extention" value="{{user.ext}}">
                                </div>
                                <div class="form-group">
                                    <label for="fax"><b>Fax</b></label>
                                    <input type="text" class="form-control" name="fax"
                                           ng-model="user.fax" placeholder="Fax" value="{{user.fax}}">
                                </div>
                                <div class="form-group">
                                    <label for="name"><b>CIM Profile ID</b></label>
                                    <input type="text" class="form-control" name="authorize_net_id"
                                           ng-model="user.authorize_net_id" placeholder="Authorize Net ID" value="{{user.authorize_net_id}}">
                                </div>
                                <div class="form-group">
                                    <label for="name"><b>Tax Exempt</b></label>
                                    <input type="checkbox" name="tax_exempt" ng-model="user.tax_exempt" ng-true-value="1" ng-false-value="0" ng-checked="{{user.tax_exempt}}">
                                </div>
                                <div class="form-group">
                                    <label for="name"><b>Active</b></label>
                                    <input type="checkbox" name="active" ng-model="user.active" ng-true-value="1" ng-false-value="0" ng-checked="{{user.active}}">
                                </div>
                                <div class="form-group">
                                    <label for="name"><b>Subscribe to newsletter</b></label>
                                    <input type="checkbox" name="subscribe" ng-model="user.subscribe" ng-true-value="1" ng-false-value="0" ng-checked="{{user.subscribe}}">
                                </div>
                                <div class="form-group">
                                    <label for="name"><b>Tax exemption certificate</b></label>
                                    <div ng-if="user.certificate">
                                        <a target="_blank" href="/storage/{{user.certificate}}">
                                            <img class="img-thumbnail admin-thumbnail" src="/storage/{{user.certificate}}">
                                        </a>
                                    </div>
                                    <div ngf-drop ngf-select ng-model="certificate"
                                         class="drop-box"
                                         name="file"
                                         style="width: 100px; height: 100px"
                                         ngf-drag-over-class="'dragover'"
                                         ngf-multiple="false">
                                        Drop file to upload
                                    </div>
                                    <div ng-if="certificate">File: {{certificate.name}}</div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="page-content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">
                        <div class="panel-heading">
                            <h3 class="panel-title">Addresses</h3>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-6" ng-if="user.addresses.length > 0">
                                <div ng-repeat="address in user.addresses">
                                    <div class="shipping_address" ng-if="address.type == 1">
                                        <h4> Shipping Address <i ng-click="showEditAddressForm(address)" class="fa fa-pencil" aria-hidden="true"></i></h4>
                                        <h4 ng-if="address.is_default">DEFAULT <i class="fa fa-check" aria-hidden="true"></i></h4>
                                        <div>{{address.first_name}} {{address.middle_name}} {{address.last_name}}</div>
                                        <div>{{address.company_name}}</div>
                                        <div>{{address.ext}}</div>
                                        <div>{{address.address_line_1}} {{address.address_line_2}}</div>
                                        <div>{{address.city}} {{address.state}} {{address.zip}}</div>
                                        <div>{{address.day_telephone}}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" ng-if="user.addresses.length > 0">
                                <div ng-repeat="address in user.addresses">
                                    <div class="billing_address"  ng-if="address.type == 2">
                                        <h4> Billing Address <i ng-click="showEditAddressForm(address)" class="fa fa-pencil" aria-hidden="true"></i></h4>
                                        <h4 ng-if="address.is_default">DEFAULT <i class="fa fa-check" aria-hidden="true"></i></h4>
                                        <div>{{address.first_name}} {{address.middle_name}} {{address.last_name}}</div>
                                        <div>{{address.company_name}}</div>
                                        <div>{{address.ext}}</div>
                                        <div>{{address.address_line_1}} {{address.address_line_2}}</div>
                                        <div>{{address.city}} {{address.state}} {{address.zip}}</div>
                                        <div>{{address.day_telephone}}</div>
                                    </div>
                                </div>
                            </div>
                            <p ng-if="user.addresses.length == 0" class="center-text">No data</p>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-primary" ng-click="showAddAddressForm({})">Add address</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">
                        <div class="panel-heading">
                            <h3 class="panel-title">Payment Profiles</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-condensed table-bordered table-hover table-info" style="width:100%" ng-if="user.payment_profiles.length > 0">
                                <tr>
                                    <th class="info"><b>Cim Profile Id:</b></th>
                                    <th class="info"><b>Cim Payment Profile Id:</b></th>
                                    <th class="info"><b>Default Payment profile:</b></th>
                                    <th class="info"><b>ACCT:</b></th>
                                    <th class="info"><b>Card Type:</b></th>
                                    <th class="info"><b>Expiration Date:</b></th>
                                    <th class="info"><b>Name:</b></th>
                                    <th class="info"><b>Company:</b></th>
                                    <th class="info"><b>Address:</b></th>
                                    <th class="info"><b>Phone:</b></th>
                                    <%--<th class="info"></th>--%>
                                </tr>
                                <tr ng-repeat="payment_profile in user.payment_profiles">
                                    <td>{{user.authorize_net_id}}</td>
                                    <td>{{payment_profile.id}}</td>
                                    <td>{{payment_profile.is_default ? 'yes' : 'no'}}</td>
                                    <td>{{payment_profile.card_number}}</td>
                                    <td>{{payment_profile.card_type}}</td>
                                    <td>{{payment_profile.expiration_month}}/{{payment_profile.expiration_year}}</td>
                                    <td>{{payment_profile.card_holder}}</td>
                                    <td>{{payment_profile.company}}</td>
                                    <td>{{payment_profile.address}}</td>
                                    <td>{{payment_profile.phone}}</td>
                                    <%--<td><i ng-click="showEditForm(address, user.id)" class="fa fa-pencil" aria-hidden="true"></i></td>--%>
                                </tr>
                            </table>
                            <p ng-if="user.payment_profiles.length == 0" class="center-text">No data</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script src="<% config('voyager.assets_path') %>/lib/js/tinymce/tinymce.min.js"></script>
    <script src="<% config('voyager.assets_path') %>/js/voyager_tinymce.js"></script>
@stop
