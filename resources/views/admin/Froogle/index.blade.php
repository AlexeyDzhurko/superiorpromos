@extends('admin.base.index')

@section('title', 'Froogle')

@section('angular_controller', 'FroogleController')

@section('panel_content')
    <div class="col-md-6 form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2"
                   for="categories">Category</label>
            <div class="col-sm-10">
                <form>
                <ui-select ng-model="selected.category_id">
                    <ui-select-match
                            placeholder="Choose the any category or to export all categories, leave this field blank." allow-clear="true">{{$select.selected.text}}</ui-select-match>
                    <ui-select-choices
                            repeat="category.id as category in categories | filter: $select.search| limitTo: 50">
                        <span ng-bind="category.text"></span>
                    </ui-select-choices>
                </ui-select>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6 form-horizontal">
        <div class="form-group">
            <button type="button" ng-click="export()" class="btn btn-primary">Export</button>
        </div>
    </div>
    <div class="col-md-12">
        <h6>New file generating some time. Please wait and update page after 5-7 min. Check update - see file timestamp</h6>
    </div>
    <div class="col-md-6 form-horizontal">
        @if (!empty($existFiles['txt']))
        <div>
            <a target="_blank" href="/admin/froogle/csv">Get exported data in text (<?= $existFiles['txt']['createdAt'] ?>)</a>
        </div>
        @endif
        @if (!empty($existFiles['xml']))
        <div>
            <a target="_blank" href="/admin/froogle/xml">Get exported data in XML (<?= $existFiles['xml']['createdAt'] ?>)</a>
        </div>
        @endif
    </div>

@stop
