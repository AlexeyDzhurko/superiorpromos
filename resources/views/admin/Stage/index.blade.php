@extends('admin.layout')

@section('css')
    <meta name="csrf-token" content="<% csrf_token() %>">
    <link rel="stylesheet" href="/admin-assets/css/assets.min.css">
@stop

@section('script')
    @parent
    <script src="/admin-assets/dist/js-assets.js"></script>
    <script src="https://rawgit.com/ppham27/material/dist/dist/angular-material.js"></script>
    <script src="<?='/admin-assets/dist/javascript.js?=' . time() ?>"></script>
@stop

@section('content')
    <div class="page-content container-fluid" ng-app="app" ng-controller="StagesListController">
        <div ng-hide="hideLoader" class="voyager-loader">
            <img src="<% config('voyager.assets_path') %>/images/logo-icon.png" alt="Voyager Loader">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">

                        <ul data-as-sortable="dragControlListeners" data-ng-model="stages">
                            <li data-ng-repeat="item in stages" data-as-sortable-item>
                                <span data-as-sortable-item-handle editable-text="item.name" e-form="textBtnForm" onbeforesave="updateStage($data, item.id)">
                                    {{item.name}}
                                </span>
                                <a ng-click="textBtnForm.$show()" ng-hide="textBtnForm.$visible" class="btn btn-default btn-xs" role="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                <span ng-click="deleteStage(item.id)" class="btn btn-xs btn-danger">
                                    <i class="voyager-trash"></i>
                                </span>
                                <div><strong class="text-danger" ng-show="stageNameError[item.id]">Field should not be empty</strong></div>
                            </li>
                        </ul>
                        <div class="row" ng-show="newStageFormShow">
                            <div class="col-md-4">
                                <form role="form" ng-submit="addNewStage()">
                                    <input ng-model="name" type="text" class="form-control ng-pristine ng-untouched ng-valid ng-not-empty" id="subject" placeholder="Subject" aria-invalid="false">
                                    <div><strong class="text-danger" ng-show="errors.name">The name field is required.</strong></div>
                                    <button type="submit" class="btn btn-success">Add</button>
                                    <button ng-click="hideNewStageForm()" class="btn btn-default">Cancel</button>
                                </form>
                            </div>
                        </div>
                        <a ng-click="showNewStageForm()" ng-show="newStageButtonShow" class="btn btn-success" ><i class="voyager-plus"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
