<!DOCTYPE html>
<html lang="en" ng-app="superiorApp" ng-cloak>
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="/img/favicon.ico">
    <title ng-bind="$root.title"></title>
    <base href="/">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Promotional products, promotional items made easy by www.superiorpromos.com. ' +
                    'Advertise your logo with the biggest selection of promotional pens, koozies & more." ng-bind="$root.description">
    <meta name="copyright" content="Copyright © 2003-2015 www.SuperiorPromos.com Promotional Products Promotional Items Inc.">
    <meta name="msvalidate.01" content="F406A51E29AD190D20DA3E32888153CD">
    <meta name="google-site-verification" content="<?=env('GOOGLE_SITE_VERIFICATION', '')?>"/>
    <link rel="preconnect" href="https://seals.resellerratings.com/landing.php?seller=52232" as="image">-
    <link rel="preconnect" href="https://use.fontawesome.com/19fb627ebc.js" as="script">
    <link rel="preconnect" href="https://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js" as="script">
    <link rel="preconnect" href="http://www.providesupport.com?messenger=0qfpmimfzdjxx0diyzqahv7zk4" as="image">
    <style>
      .first-content {
        color: white;
        height: 0;
        font-size: 0;
      }
    </style>
</head>

<body>
    <div class="first-content">.</div>
    <div ui-view="header" autoscroll="true"></div>
    <div class="main-view" main-view-toggle>
        <div class="main-view-block" id="main-view-block">
            <div ui-view="sidebar" autoscroll="true"></div>
            <div ui-view="content" autoscroll="true"></div>
        </div>
    </div>
    <div ui-view="welcome" autoscroll="true"></div>
    <div ui-view="footer" autoscroll="true"></div>
    {{--<script src="https://use.fontawesome.com/19fb627ebc.js"></script>--}}
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js"></script>
    <script>
      WebFont.load({
        google: {
          families: ['Roboto: 400,500,700,900', 'Material+Icons:400,700']
        }
      });
    </script>

    <script type="text/javascript" src="<?='/dist/vendors.min.js?=' . time() ?>"></script>
    <script type="text/javascript" src="<?='/dist/superior.min.js?=' . time() ?>"></script>

    <!-- BEGIN ProvideSupport.com Graphics Chat Button Code -->
    <div id="ciDzNJ" style="z-index:100;position:fixed"></div>
    <div id="scDzNJ" class="chat-wrapper" style="display:inline"></div>
    <div id="sdDzNJ" style="display:none"></div>
    <script type="text/javascript">var seDzNJ = document.createElement("script");
        seDzNJ.type = "text/javascript";
        var seDzNJs = (location.protocol.indexOf("https") == 0 ? "https" : "http") + "://image.providesupport.com/js/0qfpmimfzdjxx0diyzqahv7zk4/safe-standard.js?ps_h=DzNJ&ps_t=" + new Date().getTime();
        setTimeout("seDzNJ.src=seDzNJs;document.getElementById('sdDzNJ').appendChild(seDzNJ)", 1)</script>
    <script>(function (w, d, t, r, u) {
            var f, n, i;
            w[u] = w[u] || [], f = function () {
                var o = {ti: "23005982"};
                o.q = w[u], w[u] = new UET(o), w[u].push("pageLoad")
            }, n = d.createElement(t), n.src = r, n.async = 1, n.onload = n.onreadystatechange = function () {
                var s = this.readyState;
                s && s !== "loaded" && s !== "complete" || (f(), n.onload = n.onreadystatechange = null)
            }, i = d.getElementsByTagName(t)[0], i.parentNode.insertBefore(n, i)
        })(window, document, "script", "//bat.bing.com/bat.js", "uetq");
    </script>
    <noscript>
        <div style="display:inline"><a  rel="preconnect" href="http://www.providesupport.com?messenger=0qfpmimfzdjxx0diyzqahv7zk4">Online
                Chat</a></div>
    </noscript>
    <!-- END ProvideSupport.com Graphics Chat Button Code -->


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-2202411-2"></script>

    <!-- Bing -->
    <script>
        (function (w, d, t, r, u) {
            var f, n, i;
            w[u] = w[u] || [], f = function () {
                var o = {ti: "23005982"};
                o.q = w[u], w[u] = new UET(o), w[u].push("pageLoad")
            }, n = d.createElement(t), n.src = r, n.async = 1, n.onload = n.onreadystatechange = function () {
                var s = this.readyState;
                s && s !== "loaded" && s !== "complete" || (f(), n.onload = n.onreadystatechange = null)
            }, i = d.getElementsByTagName(t)[0], i.parentNode.insertBefore(n, i)
        })(window, document, "script", "//bat.bing.com/bat.js", "uetq");
    </script>
</body>
</html>
