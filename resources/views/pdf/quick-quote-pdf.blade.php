<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Document</title>
    <link rel="stylesheet" href="{{ resource_path('/assets/pdf/main.css') }}">
</head>
<body>
<div class="main-content">
    <header>
        <div class="content">
            <div class="img">
                {{--                <img src="{{ public_path('img/new_logo.png') }}" alt="logo">--}}
                <img class="site-logo" src="{{ public_path('/img/new_logo.svg') }}" alt="logo">
            </div>
            <div class="contact">
                <div class="tel">1-888-577-6667</div>
{{--                <div class="web">www.superiorpromos.com | info@www.superiorpromos.com</div>--}}
            </div>
        </div>
    </header>
    <section>
        <div class="content">
            <div class="quote-prod-details-block">
                <div class="quote-prod-info">
                    <h1 class="prod-name">@if(isset($quickQuote['product']['title'])) {{ $quickQuote['product']['title'] }}@else @endif</h1>
                    <p class="prod-number">@if(isset($quickQuote['product']['id'])) #Item {{ $quickQuote['product']['id'] }}@else @endif</p>
                    <p class="prod-details">
                        @if(isset($quickQuote['product']['description'])) {!! $quickQuote['product']['description'] !!} @else @endif
                    </p>
                </div>
            </div>
            @if(isset($quickQuote['product']['img']))
                <div class="prod-img"><img src="{{ public_path($quickQuote['product']['img']) }}" alt="product"></div>
            @endif
            <div class="clerfix"></div>
            <div class="quote-prod-options-block">
                @if(isset($quickQuote['options']) and !empty($quickQuote['options']))
                    <div class="prod-options">
                        <div class="header-top">
                            <h4 class="header">Product Options</h4>
                        </div>
                        <table class="product-options">
                            <tbody>
                            @foreach ( $quickQuote['options'] as $item)
                                <tr>
                                    <td class="header" style="text-align: left; padding-left: 40px;">  {{$item['title']}}</td>
                                    <td style="text-align: right">{{implode(', ', array_column($item['sub_option'], 'title'))}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
                @if(isset($quickQuote['colors']) and !empty($quickQuote['colors']))
                    <div class="prod-options">
                        <div class="header-top">
                            <h4 class="header">Product Colors</h4>
                        </div>
                        <table class="product-options">
                            <tbody>
                            @foreach ($quickQuote['colors'] as $item)
                                <tr>
                                    <td class="header" style="text-align: left; padding-left: 40px;">{{$item['title']}}</td>
                                    <td style="text-align: right">{{$item['value']}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
                @if(isset($quickQuote['imprints']) and !empty($quickQuote['imprints']))
                    <div class="prod-options">
                        <div class="header-top">
                            <h4 class="header">Product Imprints</h4>
                        </div>
                        <table class="product-options">
                            <tbody>
                            @foreach ( $quickQuote['imprints'] as $item)
                                <tr>
                                    <td class="header" style="text-align: left; padding-left: 40px;">{{$item['title']}}</td>
                                    <td style="text-align: right">{{$item['count_colors']}} colors</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
                @if(isset($quickQuote['shipping']) and !empty($quickQuote['shipping']))
                    <div class="prod-options">
                        <div class="header-top">
                            <h4 class="header">Shipping Cost Estimate & Transit Time</h4>
                        </div>
                        <table class="product-options">
                            <tbody>
                            <tr>
                                <td class="header" style="text-align: left; padding-left: 40px;">Service Type</td>
                                <td style="text-align: right">{{$quickQuote['shipping']['service_type']}}</td>
                            </tr>
                            <tr>
                                <td class="header" style="text-align: left; padding-left: 40px;">Cost</td>
                                <td style="text-align: right">{{$quickQuote['shipping']['cost']}}</td>
                            </tr>
                            <tr>
                                <td class="header" style="text-align: left; padding-left: 40px;">Time In Transit</td>
                                <td style="text-align: right">{{$quickQuote['shipping']['transit_time']}} </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                @endif

                <div class="prod-options">
                    <div class="header-top">
                        <h4 class="header">Cost Estimate</h4>
                    </div>
                    <table class="product-options">
                        <tbody>
                        @if(isset($quickQuote['product_prices']) and !empty($quickQuote['product_prices']))
                            @foreach($quickQuote['product_prices'] as $item)
                                <tr>
                                    <td class="header" style="text-align: left; padding-left: 40px;">{{$item['title']}}</td>
                                    <td style="text-align: right">{{$item['value']}}</td>
                                </tr>
                            @endforeach
                        @endif
                        @if(isset($quickQuote['prices_colors']) and !empty($quickQuote['prices_colors']))
                            @foreach($quickQuote['prices_colors'] as $item)
                                <tr>
                                    <td class="header" style="text-align: left; padding-left: 40px;">{{$item['title']}}</td>
                                    <td style="text-align: right">{{$item['value']}}</td>
                                </tr>
                            @endforeach
                        @endif
                        @if(isset($quickQuote['prices_options']) and !empty($quickQuote['prices_options']))
                            @foreach($quickQuote['prices_options'] as $item)
                                <tr>
                                    <td class="header" style="text-align: left; padding-left: 40px;">{{$item['title']}}</td>
                                    <td style="text-align: right">{{$item['value']}}</td>
                                </tr>
                            @endforeach
                        @endif
                        @if(isset($quickQuote['prices_imprints']) and !empty($quickQuote['prices_imprints']))
                            @foreach($quickQuote['prices_imprints'] as $item)
                                <tr>
                                    <td class="header" style="text-align: left; padding-left: 40px;">{{$item['title']}}</td>
                                    <td style="text-align: right">{{$item['value']}}</td>
                                </tr>
                            @endforeach
                        @endif
                        <tr>
                            <td class="header" style="text-align: left; padding-left: 40px;">Final Unit price</td>
                            <td style="text-align: right">@if(isset($quickQuote['final_unit_price'])){{$quickQuote['final_unit_price']}}@endif</td>
                        </tr>
                        @if(isset($quickQuote['prices_setup']) and !empty($quickQuote['prices_setup']))
                            @foreach($quickQuote['prices_setup'] as $item)
                                <tr>
                                    <td class="header" style="text-align: left; padding-left: 40px;">{{$item['title']}}</td>
                                    <td style="text-align: right">@if(isset($item['value'])){{$item['value']}}@endif</td>
                                </tr>
                            @endforeach
                        @endif
                        @if(isset($quickQuote['shipping']) and !empty($quickQuote['shipping']))
                            <tr>
                                <td class="header" style="text-align: left; padding-left: 40px;">Shipping</td>
                                <td style="text-align: right">@if(isset($quickQuote['shipping']['cost'])){{$quickQuote['shipping']['cost']}}@endif</td>
                            </tr>
                        @endif
                        <tr>
                            <td class="header" style="text-align: left; padding-left: 40px;">Total</td>
                            <td style="text-align: right">@if(isset($quickQuote['subtotal'])){{$quickQuote['subtotal']}}@endif</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
<footer>
    <div class="content">
        <div class="inc">Superior Promos Inc.</div>
        <div class="web">www.superiorpromos.com | info@www.superiorpromos.com</div>
    </div>
</footer>
</body>
</html>
