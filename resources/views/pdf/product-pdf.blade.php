<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Document</title>
    <link rel="stylesheet" href="{{ resource_path('/assets/pdf/main.css') }}">
</head>
<body>
<div class="main-content">
    <header>
        <div class="content">
            <div class="img">
{{--                <img src="{{ public_path('img/new_logo.png') }}" alt="logo">--}}
                <img class="logo" src="{{ public_path('/img/new_logo.svg') }}" alt="logo">
            </div>
            <div class="contact">
                <div class="tel">1-888-577-6667</div>
{{--                <div class="web">www.superiorpromos.com | info@www.superiorpromos.com</div>--}}
            </div>
        </div>
    </header>
    <section>
        <div class="content">
            <div class="prod-details-block">
                <h1 class="prod-name">{{ strip_tags($product['title']) }}</h1>
                <p class="prod-number">#Item {{ $product['id'] }}</p>
                <ul class="prod-details">
                    {!! $product['description'] !!}
                </ul>
            </div>
            <div class="prod-img">
                <img src="{{ file_exists(public_path($product['thumbnail'])) ? public_path($product['thumbnail']) :  public_path('img/image-not-available.jpg')}}" alt="product">
            </div>
            <div class="clerfix"></div>
            <table class="price-block">
                <tbody>
                <tr class="quantity">
                    <th class="header">Quantity</th>
                    @foreach ( $product['price_grid'] as $item )
                        <th class="cost top">{{ $item['quantity'] }}</th>
                    @endforeach
                </tr>
                <tr>
                    <td class="header" style="text-align: left; padding-left: 40px;">Price</td>
                    @foreach ( $product['price_grid'] as $item )
                        <td class="cost">${{ $item['regular_price'] }}</td>
                    @endforeach
                </tr>
                <tr>
                    <td class="header" style="text-align: left; padding-left: 40px;">Sale price</td>
                    @foreach ( $product['price_grid'] as $item )
                        <td class="cost green">${{ $item['sale_price'] }}</td>
                    @endforeach
                </tr>
                </tbody>
            </table>

            <div class="prod-options">
                <div class="header-top">
                    <h4 class="header">Product details</h4>
                </div>
                <ul class="details-list">
                    <li>
                        <div class="name">
                            <h4 class="header">Minimum Order Amount</h4>
                        </div>
                        <div class="description">
                            {{ $product['min_quantity'] }}
                        </div>
                        <div class="clearfix"></div>
                    </li>
                    <li>
                        <div class="name">
                            <h4 class="header">Product time</h4>
                        </div>
                        <div class="description">
                            {{ $product['production_time_from'] . ' - ' .$product['production_time_to'] }} working days
                        </div>
                        <div class="clearfix"></div>
                    </li>
                    <li>
                        <div class="name">
                            <h4 class="header">Product dimensions</h4>
                        </div>
                        <div class="description">
                            {{ $product['dimensions'] }}
                        </div>
                        <div class="clearfix"></div>
                    </li>
                </ul>
            </div>
            <div class="prod-options">
                <div class="header-top">
                    <h4 class="header">Product colors</h4>
                </div>

                <ul class="details-list">
                    @foreach ( $product['product_color_groups'] as $item)
                    <li>
                        <div class="name">
                            <h4 class="header">{{ $item['name'] }}</h4>
                        </div>
                        <div class="description">
                            @foreach ( $item['product_colors'] as $value )
                                {{ $value['color']['title'] . ','}}
                            @endforeach
                        </div>
                        <div class="clearfix"></div>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="prod-options">
                <div class="header-top">
                    <h4 class="header">Pricing information</h4>
                </div>
                <ul class="details-list pricing-info">
                    {!! $product['pricing_information'] !!}
                </ul>
            </div>
        </div>
    </section>
</div>
<footer>
    <div class="content">
        <div class="inc">Superior Promos Inc.</div>
        <div class="web">{!! config('app.url') !!} | {!! config('mail.superior_info_email_address') !!}</div>
    </div>
</footer>
</body>
</html>
