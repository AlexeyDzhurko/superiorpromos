@extends('vendor.voyager.master')

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $adminModel->getIcon() }}"></i> {{ $adminModel->getDisplayNamePlural() }} <a
                href="{{ route('admin_entity.create', ['admin_entity' => $adminModel->getBaseRouteName()]) }}" class="btn btn-success"><i class="voyager-plus"></i> Add
            New</a>
    </h1>
@stop

@section('page_header_actions')

@stop

@section('content')

    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <table id="dataTable" class="table table-hover">
                            <thead>
                            <tr>
                                @foreach($adminModel->showFields() as $formField)
                                    <th>{{ $formField->displayName }}</th>
                                @endforeach
                                <th class="actions">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dataTypeContent as $data)
                                <tr>
                                    @foreach($adminModel->showFields() as $formField)
                                        <td>

                                            @if($formField->type == 'image')
                                                <img src="@if( strpos($data->{$formField->field}, 'http://') === false && strpos($data->{$formField->field}, 'https://') === false){{ Voyager::image( $data->{$formField->field} ) }}@else{{ $data->{$formField->field} }}@endif"
                                                     style="width:100px">
                                            @elseif($formField->type == 'link')
                                                <a href="{{ $formField->link }}">products</a>
                                            @elseif($formField->type == 'bool')
                                                @if($data->{$formField->field})
                                                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                                @else
                                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                @endif
                                            @else
                                                {{ $data->{$formField->field} }}
                                            @endif
                                        </td>
                                    @endforeach
                                    <td class="no-sort no-click">
                                        <div class="btn-sm btn-danger pull-right delete" data-id="{{ $data->id }}"
                                             id="delete-{{ $data->id }}">
                                            <i class="voyager-trash"></i> Delete
                                        </div>
                                        <a href="{{ route('admin_entity.edit', ['admin_entity' => $adminModel->getBaseRouteName(), 'id' => $data->id]) }}"
                                           class="btn-sm btn-primary pull-right edit"><i class="voyager-edit"></i> Edit</a>
                                        <a href="{{ route('admin_entity.show', ['admin_entity' => $adminModel->getBaseRouteName(), 'id' => $data->id])  }}"
                                           class="btn-sm btn-warning pull-right"><i class="voyager-eye"></i> View</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    {!! View::make('admin.blocks.modal', ['adminModel' => $adminModel]) !!}
    @stop

    @section('javascript')
            <!-- DataTables -->
    <script>

        $(document).ready(function () {
            $('#dataTable').DataTable();

        });

        $('td').on('click', '.delete', function (e) {
            var id = $(this).data('id');
            var form = $('#delete_form')[0];
            var action = parseActionUrl(form.action, id);

            form.action = action;

            $('#delete_modal').modal('show');

        });

        function parseActionUrl(action, id) {
            if (action.match(/\/[0-9]+$/)) {
                return action.replace(/([0-9]+$)/, id);
            }
            return action + '/' + id;
        }
    </script>
@stop
