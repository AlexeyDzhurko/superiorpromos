@extends('vendor.voyager.master')

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $adminModel->getIcon() }}"></i> Viewing {{ $adminModel->getDisplayNameSingular() }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered" style="padding-bottom:5px;">


                    <!-- /.box-header -->
                    <!-- form start -->


                    @foreach($adminModel->showFields() as $formField)

                        <div class="panel-heading" style="border-bottom:0;">
                            <h3 class="panel-title">{{ $formField->displayName }}</h3>
                        </div>

                        <div class="panel-body" style="padding-top:0;">
                            @if($formField->type == "image")
                                <img style="max-width:640px"
                                     src="<?= Voyager::image($dataTypeContent->{$formField->field}) ?>">
                            @else
                                <p><?= $dataTypeContent->{$formField->field} ?></p>
                            @endif
                        </div><!-- panel-body -->
                        @if(!$loop->last)
                            <hr style="margin:0;">
                        @endif
                    @endforeach


                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')

@stop