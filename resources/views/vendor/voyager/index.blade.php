@extends('vendor.voyager.angular-master')

@section('css')
    <link rel="stylesheet" type="text/css" href="<% config('voyager.assets_path') %>/css/ga-embed.css">
@stop

@section('content')
    <style>
        .dashboard-block-helper {
            margin: 0 30px !important;
        }

        .dashboard-block-helper a {
            color: #337ab7 !important;
        }
    </style>
    <div class="row">
        <div class="panel panel-bordered dashboard-block-helper">
            <div class="panel-body">
                <div class="col-sm-4">
                    <ul style="list-style-type: none">
                        <li style="margin-left: -20px;"><b>Dynamic Sections</b></li>
                        <a href="/admin/product"><li>Products</li></a>
                        <a href="/admin/category"><li>Categories</li></a>
                        @if(auth()->user()->hasRole('admin'))
                            <a href="/admin/testimonials"><li>Testimonials</li></a>
                            <a href="/admin/slider"><li>Main Page Carousel</li></a>
                            <a href="/admin/faqs"><li>FAQ</li></a>
                            <a href="/admin/case-study"><li>Case Studies</li></a>
                            <a href="/admin/state"><li>Tax States</li></a>
                            <a href="/admin/promotional-glossary"><li>Promotional Glossary</li></a>
                        @endif
                        <a href="/admin/size-group"><li>Size Groups</li></a>
                        @if(auth()->user()->hasRole('admin'))
                            <a href="/admin/pms-color"><li>PMS Colors</li></a>
                        @endif
                        <a href="/admin/vendors"><li>Vendors</li></a>
                        @if(auth()->user()->hasRole('admin'))
                            <a href="/admin/product-icon"><li>Product Icons</li></a>
                        @endif
                        <a href="/admin/color-groups"><li>Color Groups</li></a>
                        <a href="/admin/colors"><li>Colors</li></a>
                    </ul>
                    @if(auth()->user()->hasRole('admin'))
                    <ul style="list-style-type: none">
                        <li style="margin-left: -20px;"><b>User</b></li>
                        <a href="/admin/customer"><li>Customers</li></a>
                    </ul>
                    @endif
                    @if(auth()->user()->hasRole('admin'))
                    <ul style="list-style-type: none">
                        <li style="margin-left: -20px;"><b>Orders</b></li>
                        <a href="/admin/order"><li>Orders</li></a>
                        <a href="/admin/order_sample"><li>Orders a Sample</li></a>
                        <a href="/admin/discount"><li>Discounts</li></a>
                        <a href="/admin/stage"><li>Production Stages</li></a>
                    </ul>
                    @endif
                </div>
                @if(auth()->user()->hasRole('admin'))
                    <div class="col-sm-4">
                        <ul style="list-style-type: none">
                            <li style="margin-left: -20px;"><b>Misc Section</b></li>
                            <a href="/admin/survey"><li>Surveys</li></a>
                            <a href="/admin/payment-information-template"><li>Payment Information Templates</li></a>
                            <a href="/admin/froogle"><li>Froogle</li></a>
                            <a href="/admin/review"><li>Reviews</li></a>
                        </ul>
                        <ul style="list-style-type: none">
                            <li style="margin-left: -20px;"><b>CMS Section</b></li>
                            <a href="/admin/info-message"><li>Info Messages</li></a>
                            <a href="/admin/block"><li>Editable Blocks</li></a>
                        </ul>
                        <ul style="list-style-type: none">
                            <li style="margin-left: -20px;"><b>Product Block</b></li>
                            <a href="/admin/product-category"><li>Product&Categories</li></a>
                        </ul>
                    </div>
                @endif
                <div class="col-sm-4">
                    <ul style="list-style-type: none">
                        <li style="margin-left: -20px;"><b>Sage Block</b></li>
                        <a href="/admin/sage/category"><li>Category</li></a>
                        <a href="/admin/sage/supplier"><li>Import Product</li></a>
                    </ul>
                    @if(auth()->user()->hasRole('admin'))
                    <ul style="list-style-type: none">
                        <li style="margin-left: -20px;"><b>Sections</b></li>
                        <a href="/admin/faqs"><li>FAQ</li></a>
                        <a href="/admin/setting"><li>Settings</li></a>
                        <a href="/admin/testimonials"><li>Testimonials</li></a>
                        <a href="/admin/promotional-block"><li>Promotional Products</li></a>
                        <a href="/admin/company-info"><li>Company Info</li></a>
                        <a href="/admin/promos"><li>Promos</li></a>
                        <a href="/admin/administrators"><li>Administrators</li></a>
                        <a href="/admin/promotions"><li>Current promotions</li></a>
                    </ul>
                    @endif
                    @if(auth()->user()->hasRole('admin'))
                    <ul style="list-style-type: none">
                        <li style="margin-left: -20px;"><b>Blog</b></li>
                        <a href="<% route('admin.dashboard.index', ['name' => 'post']) %>"><li>Posts</li></a>
                        <a href="<% route('admin.dashboard.index', ['name' => 'post-category']) %>"><li>Post Categories</li></a>
                    </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="page-content">
        <div class="widgets">
            <div class="panel widget center bgimage"
                 style="background-image:url(<% config('voyager.assets_path') %>/images/widget-backgrounds/02.png);">
                <div class="dimmer"></div>
                <div class="panel-content">
                    <i class="voyager-group"></i>
                    <?php $user_count = App\User::count(); ?>
                    <h4><% $user_count %> Users</h4>
                    <p>You have <% $user_count %> users registered. Click on 'View All Users' to view all your
                        current users.</p>

                    <a href="<% route('admin.dashboard.index', ['name' => 'customer']) %>" class="btn btn-primary">View All Users</a>
                </div>

            </div>

            <div class="panel widget center bgimage"
                 style="background-image:url(<% config('voyager.assets_path') %>/images/widget-backgrounds/03.png);">
                <div class="dimmer"></div>
                <div class="panel-content">
                    <i class="voyager-news"></i>
                    <?php $post_count = App\Models\Order::count(); ?>
                    <h4><% $post_count %> Orders</h4>
                    <p>You have <% $post_count %> orders. Click on 'View All Orders' below to view all
                        orders.</p>

                    <a href="<% route('order.index') %>" class="btn btn-primary">View All Orders</a>
                </div>

            </div>

            <div class="panel widget center bgimage"
                 style="background-image:url(<% config('voyager.assets_path') %>/images/widget-backgrounds/04.png);">
                <div class="dimmer"></div>
                <div class="panel-content">
                    <i class="voyager-file-text"></i>
                    <?php $page_count = App\Models\Product::count(); ?>
                    <h4><% $page_count %> Products</h4>
                    <p>You have <% $page_count %> products. Click on 'View All Products' below to view all
                        products.</p>

                    <a href="<% route('dashboard.index', ['name' => 'product']) %>" class="btn btn-primary">View All Products</a>
                </div>

            </div>

        </div>

    </div>

@stop
