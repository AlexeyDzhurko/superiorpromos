<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="../../../../admin-assets/css/password.css">

    <!-- Scripts -->
</head>
<body>
<div id="app">

            <div class="forgot-block">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="separator left"></div>
                        <h3 class="forgot-header">Reset password</h3>
                        <div class="separator"></div>
                    </div>
                    <p class="forgot-info">Status</p>
                    <div class="panel-body">
                        <div class="reset-status">
                            <span>{{$status}}</span>
                        </div>
                    </div>
                    <p class="forgot-info">You can close this window</p>
                </div>
            </div>
</div>
</body>
</html>
