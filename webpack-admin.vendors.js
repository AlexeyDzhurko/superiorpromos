// window.$ = require("jquery");

// CSS vendors
require("./node_modules/sweetalert/dist/sweetalert.css");
require("./node_modules/font-awesome/css/font-awesome.min.css");
// require("./node_modules/angular-bootstrap-datetimepicker/src/css/datetimepicker.css");
require("./node_modules/angular-xeditable/dist/css/xeditable.min.css");
require("./node_modules/ng-sortable/dist/ng-sortable.min.css");
require("./node_modules/ng-sortable/dist/ng-sortable.style.min.css");
require("./node_modules/ui-select/dist/select.min.css");
require("./node_modules/angular-ui-tree/dist/angular-ui-tree.min.css");
// require("./node_modules/selectize/dist/css/selectize.default.css");
require("./node_modules/angular-color-picker/angular-color-picker.css");
// require("./node_modules/angular-color-picker/dist/themes/angularjs-color-picker-bootstrap.min.css");
require("./resources/assets/admin/css/custom.css");

// JS vendors
import angular from 'angular';
// require("./node_modules/angular/angular.js");
// require("./node_modules/sweetalert/dist/sweetalert.min.js");
// require("./node_modules/ngsweetalert/ngSweetAlert.js");
require("./node_modules/angular-bootstrap/ui-bootstrap-tpls.min.js");
require("./node_modules/angular-ui-router/release/angular-ui-router.min.js"); // **
require("./node_modules/angular-animate/angular-animate.min.js");
require("./node_modules/angular-carousel/dist/angular-carousel.min.js"); // **
require("./node_modules/angular-carousel/src/directives/rn-carousel-auto-slide.js"); // **
require("./node_modules/angularjs-slider/dist/rzslider.min.js"); // **
// require("./node_modules/slick-carousel/slick/slick.min.js"); // **
require("./node_modules/angular-slick/dist/slick.min.js");
require("./node_modules/ui-select/dist/select.min.js"); 
require("./node_modules/ngsticky/dist/sticky.min.js");
require("./node_modules/angular-scroll/angular-scroll.min.js"); // **
// require("./node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"); // **
// require("./node_modules/ng-scrollbars/src/scrollbars.js"); // **
require("./node_modules/angular-aria/angular-aria.min.js");
// TODOs: npm i headroom.js
// require("./node_modules/headroom.js/dist/headroom.min.js");
// require("./node_modules/headroom.js/dist/angular.headroom.min.js");
require("./node_modules/angular-modal-service/dst/angular-modal-service.min.js");
require("./node_modules/angular-xeditable/dist/js/xeditable.min.js");
require("./node_modules/moment/moment.js");
// TODOs: npm i angular-bootstrap-datetimepicker
// require("./node_modules/angular-bootstrap-datetimepicker/esm2015/angular-bootstrap-datetimepicker.js");
// require("./node_modules/angular-bootstrap-datetimepicker/src/js/datetimepicker.templates.js");
require("./node_modules/ng-file-upload/dist/ng-file-upload-all.min.js");
// TODOs: npm i tinymce-dist
// require("./node_modules/tinymce-dist/tinymce.js");
require("./node_modules/angular-ui-tinymce/dist/tinymce.min.js");
require("./node_modules/ng-sortable/dist/ng-sortable.min.js");
require("./node_modules/angular-ui-tree/dist/angular-ui-tree.min.js");
require("./node_modules/angular-ui-tree-filter/dist/angular-ui-tree-filter.min.js");
// require("./node_modules/angular-ui-tree-filter/dist/angular-ui-tree-filter.min.js");
// require("./node_modules/selectize/dist/js/standalone/selectize.min.js"); 
require("./node_modules/angular-selectize2/dist/selectize.js");
// TODOs: there is no such npm package - tinycolor. It's only - tinycolor2
// require("./node_modules/tinycolor/dist/tinycolor-min.js");
require("./node_modules/angular-color-picker/angular-color-picker.js");
