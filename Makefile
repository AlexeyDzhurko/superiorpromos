include Makefile-variables

all: | ${PROJECT_ENV}

############# Environments
dev: | docker-build docker-up composer-install artisan-keygen swagger-dump npm-i
prod: | prod-docker-build docker-up prod-composer-install artisan-keygen cache-clear npm-i webpack-prod


############## docker compose
docker-build:
	docker-compose ${COMPOSE_FILE_OPTION} build

prod-docker-build:
	docker-compose ${COMPOSE_FILE_OPTION} build

docker-up:
	docker-compose ${COMPOSE_FILE_OPTION} up -d --force-recreate

directory-permissions:
	sudo chmod o+w -R storage

prod-directory-permissions:
	chmod o+w -R storage

artisan-keygen:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm php artisan key:generate

############## composer
composer-install:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm composer install --no-interaction

prod-composer-install:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm composer install --no-dev  --optimize-autoloader --no-interaction


############## db
db: | db-migrate db-seeds elastic-reindex

db-migrate:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm php artisan migrate

db-migrate-fresh:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm php artisan migrate:fresh

db-seeds:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm php artisan db:seed

############## elastic
elastic-reindex:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm php artisan app:elastic:reindex

############## tests
test-phpunit:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm sh -c "vendor/bin/phpunit --stop-on-fail"

test-code-coverage:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm sh -c "vendor/bin/phpunit --stop-on-fail --coverage-html storage/tests-coverage"

############## api-docs
swagger-dump:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm php artisan l5:generate

############## cache
cache-clear:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm php artisan cache:clear

############# webpack
build-front-dev: | bower-install npm-i bower-i

build-front-prod: | bower-install npm-i bower-i webpack-admin-prod gulp-admin-prod

bower-install:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm npm install -g bower

npm-i:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm npm install

bower-i:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm bower install --allow-root

webpack-prod:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm npm run webpack-prod

webpack-dev:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm npm run webpack-dev

webpack-admin-prod:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm npm run webpack-admin-prod

webpack-admin-dev:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm npm run webpack-admin-dev

gulp-admin-prod:
	docker-compose ${COMPOSE_FILE_OPTION} exec ${COMPOSE_EXEC_OPTIONS} php-fpm npm run gulp-admin-dev
