{
  "top_block": {
    "title": "Top block",
    "slug": "/top-block",
    "products": [
      {
        "id": 3,
        "thumbnail": "http://www.promoplace.com/ws/ws.dll/QPic?SN=68819&P=383177705&RS=300",
        "title": "BPA Free Tumbler w/ Grip - 14 Oz.",
        "min_quantity": 96,
        "production_time": "within 1 working day*",
        "lowest_price": "7.49",
        "slug": "pens-pencils/pens-plastic/bic-clic-stic-pen",
        "color_groups": [
          {
            "id": 3,
            "name": "BPA Free Tumbler w/ Grip - 14 Oz.",
            "colors": [
              {
                "id": 7,
                "title": "Black",
                "color_hex": "#b15b5b",
                "image": null
              },
              {
                "id": 6,
                "title": "Black/Blue",
                "color_hex": "#b15b5b",
                "image": null
              },
              {
                "id": 5,
                "title": "Black/Green",
                "color_hex": "#b15b5b",
                "image": null
              }
            ]
          },
          {
            "id": 1,
            "name": "14 Oz. BPA Free Stainless Steel Tumbler",
            "colors": [
              {
                "id": 1,
                "title": "Silver/Black",
                "color_hex": "#eeeeee",
                "image": null
              },
              {
                "id": 2,
                "title": "Silver/Blue",
                "color_hex": "#4AC9FF",
                "image": null
              },
              {
                "id": 3,
                "title": "Silver/Red",
                "color_hex": "#BC4348",
                "image": null
              }
            ]
          },
          {
            "id": 13,
            "name": "14 Oz. BPA Free Stainless Steel Tumbler",
            "colors": [
              {
                "id": 131,
                "title": "Silver/Black",
                "color_hex": "#eeeeee",
                "image": null
              },
              {
                "id": 132,
                "title": "Silver/Blue",
                "color_hex": "#4AC9FF",
                "image": null
              },
              {
                "id": 133,
                "title": "Silver/Red",
                "color_hex": "#BC4348",
                "image": null
              }
            ]
          },
          {
            "id": 31,
            "name": "14 Oz. BPA Free Stainless Steel Tumbler",
            "colors": [
              {
                "id": 311,
                "title": "Silver/Black",
                "color_hex": "#eeeeee",
                "image": null
              },
              {
                "id": 312,
                "title": "Silver/Blue",
                "color_hex": "#4AC9FF",
                "image": null
              },
              {
                "id": 313,
                "title": "Silver/Red",
                "color_hex": "#BC4348",
                "image": null
              }
            ]
          }
        ],
        "price_grid": [
          {
            "quantity": 96,
            "regular_price": "7.79",
            "sale_price": "5.14"
          },
          {
            "quantity": 288,
            "regular_price": "7.69",
            "sale_price": "5.07"
          },
          {
            "quantity": 768,
            "regular_price": "7.59",
            "sale_price": "5.01"
          },
          {
            "quantity": 1536,
            "regular_price": "7.49",
            "sale_price": "4.94"
          }
        ],
        "product_options": [],
        "imprints": [],
        "product_extra_images": [],
        "size_group": {
          "id": 2,
          "name": "None",
          "sizes": []
        }
      }
    ]
  },
  "middle_block": {
    "title": "Middle block",
    "slug": "/middle-block",
    "products": [
      {
        "id": 1,
        "thumbnail": "http://www.promoplace.com/ws/ws.dll/QPic?SN=68819&P=383177304&RS=300",
        "title": "14 Oz. BPA Free Stainless Steel Tumbler",
        "min_quantity": 96,
        "production_time": "within 1 working day*",
        "lowest_price": "3.39",
        "slug": "pens-pencils/pens-plastic/bic-clic-stic-pen",
        "color_groups": [
          {
            "id": 1,
            "name": "14 Oz. BPA Free Stainless Steel Tumbler",
            "colors": [
              {
                "id": 1,
                "title": "Silver/Black",
                "color_hex": "#eeeeee",
                "image": null
              },
              {
                "id": 2,
                "title": "Silver/Blue",
                "color_hex": "#4AC9FF",
                "image": null
              },
              {
                "id": 3,
                "title": "Silver/Red",
                "color_hex": "#BC4348",
                "image": null
              }
            ]
          }
        ],
        "price_grid": [
          {
            "quantity": 96,
            "regular_price": "3.69",
            "sale_price": "2.43"
          },
          {
            "quantity": 288,
            "regular_price": "3.59",
            "sale_price": "2.37"
          },
          {
            "quantity": 768,
            "regular_price": "3.49",
            "sale_price": "2.30"
          },
          {
            "quantity": 1536,
            "regular_price": "3.39",
            "sale_price": "2.23"
          }
        ],
        "product_options": [
          {
            "id": 1,
            "name": "Options group #1",
            "product_option_prices": [
              {
                "id": 1,
                "quantity": 96,
                "setup_price": "12.00",
                "item_price": "2.20"
              },
              {
                "id": 3,
                "quantity": 288,
                "setup_price": "11.00",
                "item_price": "2.05"
              },
              {
                "id": 5,
                "quantity": 768,
                "setup_price": "10.00",
                "item_price": "1.80"
              },
              {
                "id": 8,
                "quantity": 1536,
                "setup_price": "8.00",
                "item_price": "1.50"
              }
            ],
            "product_sub_options": [
              {
                "id": 1,
                "name": "Sub-option 1-1",
                "product_sub_option_prices": [
                  {
                    "id": 1,
                    "quantity": 1,
                    "setup_price": "0.00",
                    "item_price": "0.00"
                  }
                ]
              },
              {
                "id": 2,
                "name": "Sub-option 1-2",
                "product_sub_option_prices": [
                  {
                    "id": 2,
                    "quantity": 96,
                    "setup_price": "4.50",
                    "item_price": "0.45"
                  },
                  {
                    "id": 16,
                    "quantity": 288,
                    "setup_price": "4.40",
                    "item_price": "0.40"
                  },
                  {
                    "id": 17,
                    "quantity": 768,
                    "setup_price": "4.20",
                    "item_price": "0.38"
                  },
                  {
                    "id": 18,
                    "quantity": 1536,
                    "setup_price": "4.00",
                    "item_price": "0.36"
                  }
                ]
              },
              {
                "id": 3,
                "name": "Sub-option 1-3",
                "product_sub_option_prices": [
                  {
                    "id": 3,
                    "quantity": 1,
                    "setup_price": "4.50",
                    "item_price": "0.45"
                  }
                ]
              },
              {
                "id": 4,
                "name": "Sub-option 1-4",
                "product_sub_option_prices": [
                  {
                    "id": 4,
                    "quantity": 1,
                    "setup_price": "4.60",
                    "item_price": "0.60"
                  }
                ]
              },
              {
                "id": 5,
                "name": "Sub-option 1-5",
                "product_sub_option_prices": [
                  {
                    "id": 5,
                    "quantity": 1,
                    "setup_price": "5.00",
                    "item_price": "0.50"
                  }
                ]
              }
            ]
          },
          {
            "id": 2,
            "name": "Options group #2",
            "product_option_prices": [
              {
                "id": 2,
                "quantity": 1,
                "setup_price": "14.00",
                "item_price": "2.40"
              }
            ],
            "product_sub_options": [
              {
                "id": 6,
                "name": "sub 1",
                "product_sub_option_prices": [
                  {
                    "id": 6,
                    "quantity": 1,
                    "setup_price": "10.00",
                    "item_price": "0.10"
                  },
                  {
                    "id": 7,
                    "quantity": 1000,
                    "setup_price": "1.00",
                    "item_price": "0.20"
                  }
                ]
              },
              {
                "id": 7,
                "name": "sub 2",
                "product_sub_option_prices": [
                  {
                    "id": 8,
                    "quantity": 1,
                    "setup_price": "5.00",
                    "item_price": "0.10"
                  },
                  {
                    "id": 9,
                    "quantity": 10,
                    "setup_price": "4.00",
                    "item_price": "0.02"
                  },
                  {
                    "id": 10,
                    "quantity": 100,
                    "setup_price": "3.00",
                    "item_price": "0.10"
                  },
                  {
                    "id": 11,
                    "quantity": 1000,
                    "setup_price": "1.00",
                    "item_price": "0.00"
                  }
                ]
              },
              {
                "id": 8,
                "name": "sub 3",
                "product_sub_option_prices": [
                  {
                    "id": 12,
                    "quantity": 1,
                    "setup_price": "8.00",
                    "item_price": "0.10"
                  },
                  {
                    "id": 13,
                    "quantity": 10,
                    "setup_price": "6.00",
                    "item_price": "0.02"
                  },
                  {
                    "id": 14,
                    "quantity": 100,
                    "setup_price": "2.00",
                    "item_price": "0.10"
                  },
                  {
                    "id": 15,
                    "quantity": 1000,
                    "setup_price": "1.00",
                    "item_price": "0.00"
                  }
                ]
              },
              {
                "id": 9,
                "name": "sub 4 - free (10+)",
                "product_sub_option_prices": [
                  {
                    "id": 19,
                    "quantity": 1,
                    "setup_price": "1.00",
                    "item_price": "0.10"
                  },
                  {
                    "id": 20,
                    "quantity": 10,
                    "setup_price": "0.00",
                    "item_price": "0.00"
                  }
                ]
              }
            ]
          }
        ],
        "imprints": [
          {
            "id": 1,
            "position": 1,
            "name": "imprint 1",
            "max_colors": 2,
            "color_group_id": 1,
            "imprint_prices": [
              {
                "id": 1,
                "quantity": 96,
                "setup_price": "40.00",
                "item_price": "5.00",
                "color_setup_price": "10.00",
                "color_item_price": "2.00"
              },
              {
                "id": 3,
                "quantity": 288,
                "setup_price": "35.00",
                "item_price": "4.50",
                "color_setup_price": "9.50",
                "color_item_price": "1.85"
              },
              {
                "id": 4,
                "quantity": 768,
                "setup_price": "30.00",
                "item_price": "4.00",
                "color_setup_price": "9.00",
                "color_item_price": "1.78"
              },
              {
                "id": 5,
                "quantity": 1536,
                "setup_price": "25.00",
                "item_price": "0.00",
                "color_setup_price": "8.50",
                "color_item_price": "0.00"
              }
            ],
            "color_group": {
              "id": 1,
              "name": "14 Oz. BPA Free Stainless Steel Tumbler",
              "colors": [
                {
                  "id": 1,
                  "title": "Silver/Black",
                  "color_hex": "#eeeeee",
                  "image": null
                },
                {
                  "id": 2,
                  "title": "Silver/Blue",
                  "color_hex": "#4AC9FF",
                  "image": null
                },
                {
                  "id": 3,
                  "title": "Silver/Red",
                  "color_hex": "#BC4348",
                  "image": null
                }
              ]
            }
          },
          {
            "id": 3,
            "position": 3,
            "name": "imprint 22",
            "max_colors": 3,
            "color_group_id": 17,
            "imprint_prices": [
              {
                "id": 2,
                "quantity": 1,
                "setup_price": "45.00",
                "item_price": "6.00",
                "color_setup_price": "12.00",
                "color_item_price": "4.00"
              }
            ],
            "color_group": {
              "id": 17,
              "name": "17 oz. Perka Tumbler",
              "colors": [
                {
                  "id": 7,
                  "title": "Black",
                  "color_hex": "#b15b5b",
                  "image": null
                },
                {
                  "id": 10,
                  "title": "Blue/Black",
                  "color_hex": "#b15b5b",
                  "image": null
                },
                {
                  "id": 38,
                  "title": "Brown/Black",
                  "color_hex": "#b15b5b",
                  "image": null
                },
                {
                  "id": 40,
                  "title": "Light Blue/Black",
                  "color_hex": "#b15b5b",
                  "image": null
                },
                {
                  "id": 39,
                  "title": "Lime Green/Black",
                  "color_hex": "#b15b5b",
                  "image": null
                },
                {
                  "id": 41,
                  "title": "Pink/Black",
                  "color_hex": "#b15b5b",
                  "image": null
                }
              ]
            }
          }
        ],
        "product_extra_images": [
          {
            "id": 4,
            "original": "http://localhost:8000/uploads/products/extra/4/original.jpg",
            "small": "http://localhost:8000/uploads/products/extra/4/110_110.jpg",
            "medium": "http://localhost:8000/uploads/products/extra/4/270_270.jpg"
          }
        ],
        "size_group": {
          "id": 1,
          "name": "Apparel",
          "sizes": [
            {
              "id": 1,
              "name": "S"
            },
            {
              "id": 2,
              "name": "M"
            },
            {
              "id": 3,
              "name": "L"
            },
            {
              "id": 4,
              "name": "XL"
            }
          ]
        }
      }
    ]
  },
  "bottom_block": {
    "title": "Bottom block",
    "slug": "/bottom-block",
    "products": [
      {
        "id": 1,
        "thumbnail": "http://www.promoplace.com/ws/ws.dll/QPic?SN=68819&P=383177304&RS=300",
        "title": "14 Oz. BPA Free Stainless Steel Tumbler",
        "min_quantity": 96,
        "production_time": "within 1 working day*",
        "lowest_price": "3.39",
        "slug": "pens-pencils/pens-plastic/bic-clic-stic-pen",
        "color_groups": [
          {
            "id": 1,
            "name": "14 Oz. BPA Free Stainless Steel Tumbler",
            "colors": [
              {
                "id": 1,
                "title": "Silver/Black",
                "color_hex": "#eeeeee",
                "image": null
              },
              {
                "id": 2,
                "title": "Silver/Blue",
                "color_hex": "#4AC9FF",
                "image": null
              },
              {
                "id": 3,
                "title": "Silver/Red",
                "color_hex": "#BC4348",
                "image": null
              }
            ]
          }
        ],
        "price_grid": [
          {
            "quantity": 96,
            "regular_price": "3.69",
            "sale_price": "2.43"
          },
          {
            "quantity": 288,
            "regular_price": "3.59",
            "sale_price": "2.37"
          },
          {
            "quantity": 768,
            "regular_price": "3.49",
            "sale_price": "2.30"
          },
          {
            "quantity": 1536,
            "regular_price": "3.39",
            "sale_price": "2.23"
          }
        ],
        "product_options": [
          {
            "id": 1,
            "name": "Options group #1",
            "product_option_prices": [
              {
                "id": 1,
                "quantity": 96,
                "setup_price": "12.00",
                "item_price": "2.20"
              },
              {
                "id": 3,
                "quantity": 288,
                "setup_price": "11.00",
                "item_price": "2.05"
              },
              {
                "id": 5,
                "quantity": 768,
                "setup_price": "10.00",
                "item_price": "1.80"
              },
              {
                "id": 8,
                "quantity": 1536,
                "setup_price": "8.00",
                "item_price": "1.50"
              }
            ],
            "product_sub_options": [
              {
                "id": 1,
                "name": "Sub-option 1-1",
                "product_sub_option_prices": [
                  {
                    "id": 1,
                    "quantity": 1,
                    "setup_price": "0.00",
                    "item_price": "0.00"
                  }
                ]
              },
              {
                "id": 2,
                "name": "Sub-option 1-2",
                "product_sub_option_prices": [
                  {
                    "id": 2,
                    "quantity": 96,
                    "setup_price": "4.50",
                    "item_price": "0.45"
                  },
                  {
                    "id": 16,
                    "quantity": 288,
                    "setup_price": "4.40",
                    "item_price": "0.40"
                  },
                  {
                    "id": 17,
                    "quantity": 768,
                    "setup_price": "4.20",
                    "item_price": "0.38"
                  },
                  {
                    "id": 18,
                    "quantity": 1536,
                    "setup_price": "4.00",
                    "item_price": "0.36"
                  }
                ]
              },
              {
                "id": 3,
                "name": "Sub-option 1-3",
                "product_sub_option_prices": [
                  {
                    "id": 3,
                    "quantity": 1,
                    "setup_price": "4.50",
                    "item_price": "0.45"
                  }
                ]
              },
              {
                "id": 4,
                "name": "Sub-option 1-4",
                "product_sub_option_prices": [
                  {
                    "id": 4,
                    "quantity": 1,
                    "setup_price": "4.60",
                    "item_price": "0.60"
                  }
                ]
              },
              {
                "id": 5,
                "name": "Sub-option 1-5",
                "product_sub_option_prices": [
                  {
                    "id": 5,
                    "quantity": 1,
                    "setup_price": "5.00",
                    "item_price": "0.50"
                  }
                ]
              }
            ]
          },
          {
            "id": 2,
            "name": "Options group #2",
            "product_option_prices": [
              {
                "id": 2,
                "quantity": 1,
                "setup_price": "14.00",
                "item_price": "2.40"
              }
            ],
            "product_sub_options": [
              {
                "id": 6,
                "name": "sub 1",
                "product_sub_option_prices": [
                  {
                    "id": 6,
                    "quantity": 1,
                    "setup_price": "10.00",
                    "item_price": "0.10"
                  },
                  {
                    "id": 7,
                    "quantity": 1000,
                    "setup_price": "1.00",
                    "item_price": "0.20"
                  }
                ]
              },
              {
                "id": 7,
                "name": "sub 2",
                "product_sub_option_prices": [
                  {
                    "id": 8,
                    "quantity": 1,
                    "setup_price": "5.00",
                    "item_price": "0.10"
                  },
                  {
                    "id": 9,
                    "quantity": 10,
                    "setup_price": "4.00",
                    "item_price": "0.02"
                  },
                  {
                    "id": 10,
                    "quantity": 100,
                    "setup_price": "3.00",
                    "item_price": "0.10"
                  },
                  {
                    "id": 11,
                    "quantity": 1000,
                    "setup_price": "1.00",
                    "item_price": "0.00"
                  }
                ]
              },
              {
                "id": 8,
                "name": "sub 3",
                "product_sub_option_prices": [
                  {
                    "id": 12,
                    "quantity": 1,
                    "setup_price": "8.00",
                    "item_price": "0.10"
                  },
                  {
                    "id": 13,
                    "quantity": 10,
                    "setup_price": "6.00",
                    "item_price": "0.02"
                  },
                  {
                    "id": 14,
                    "quantity": 100,
                    "setup_price": "2.00",
                    "item_price": "0.10"
                  },
                  {
                    "id": 15,
                    "quantity": 1000,
                    "setup_price": "1.00",
                    "item_price": "0.00"
                  }
                ]
              },
              {
                "id": 9,
                "name": "sub 4 - free (10+)",
                "product_sub_option_prices": [
                  {
                    "id": 19,
                    "quantity": 1,
                    "setup_price": "1.00",
                    "item_price": "0.10"
                  },
                  {
                    "id": 20,
                    "quantity": 10,
                    "setup_price": "0.00",
                    "item_price": "0.00"
                  }
                ]
              }
            ]
          }
        ],
        "imprints": [
          {
            "id": 1,
            "position": 1,
            "name": "imprint 1",
            "max_colors": 2,
            "color_group_id": 1,
            "imprint_prices": [
              {
                "id": 1,
                "quantity": 96,
                "setup_price": "40.00",
                "item_price": "5.00",
                "color_setup_price": "10.00",
                "color_item_price": "2.00"
              },
              {
                "id": 3,
                "quantity": 288,
                "setup_price": "35.00",
                "item_price": "4.50",
                "color_setup_price": "9.50",
                "color_item_price": "1.85"
              },
              {
                "id": 4,
                "quantity": 768,
                "setup_price": "30.00",
                "item_price": "4.00",
                "color_setup_price": "9.00",
                "color_item_price": "1.78"
              },
              {
                "id": 5,
                "quantity": 1536,
                "setup_price": "25.00",
                "item_price": "0.00",
                "color_setup_price": "8.50",
                "color_item_price": "0.00"
              }
            ],
            "color_group": {
              "id": 1,
              "name": "14 Oz. BPA Free Stainless Steel Tumbler",
              "colors": [
                {
                  "id": 1,
                  "title": "Silver/Black",
                  "color_hex": "#eeeeee",
                  "image": null
                },
                {
                  "id": 2,
                  "title": "Silver/Blue",
                  "color_hex": "#4AC9FF",
                  "image": null
                },
                {
                  "id": 3,
                  "title": "Silver/Red",
                  "color_hex": "#BC4348",
                  "image": null
                }
              ]
            }
          },
          {
            "id": 3,
            "position": 3,
            "name": "imprint 22",
            "max_colors": 3,
            "color_group_id": 17,
            "imprint_prices": [
              {
                "id": 2,
                "quantity": 1,
                "setup_price": "45.00",
                "item_price": "6.00",
                "color_setup_price": "12.00",
                "color_item_price": "4.00"
              }
            ],
            "color_group": {
              "id": 17,
              "name": "17 oz. Perka Tumbler",
              "colors": [
                {
                  "id": 7,
                  "title": "Black",
                  "color_hex": "#b15b5b",
                  "image": null
                },
                {
                  "id": 10,
                  "title": "Blue/Black",
                  "color_hex": "#b15b5b",
                  "image": null
                },
                {
                  "id": 38,
                  "title": "Brown/Black",
                  "color_hex": "#b15b5b",
                  "image": null
                },
                {
                  "id": 40,
                  "title": "Light Blue/Black",
                  "color_hex": "#b15b5b",
                  "image": null
                },
                {
                  "id": 39,
                  "title": "Lime Green/Black",
                  "color_hex": "#b15b5b",
                  "image": null
                },
                {
                  "id": 41,
                  "title": "Pink/Black",
                  "color_hex": "#b15b5b",
                  "image": null
                }
              ]
            }
          }
        ],
        "product_extra_images": [
          {
            "id": 4,
            "original": "http://localhost:8000/uploads/products/extra/4/original.jpg",
            "small": "http://localhost:8000/uploads/products/extra/4/110_110.jpg",
            "medium": "http://localhost:8000/uploads/products/extra/4/270_270.jpg"
          }
        ],
        "size_group": {
          "id": 1,
          "name": "Apparel",
          "sizes": [
            {
              "id": 1,
              "name": "S"
            },
            {
              "id": 2,
              "name": "M"
            },
            {
              "id": 3,
              "name": "L"
            },
            {
              "id": 4,
              "name": "XL"
            }
          ]
        }
      }
    ]
  }
}
