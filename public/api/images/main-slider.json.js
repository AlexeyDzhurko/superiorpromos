[
    {
        "id": 1,
        "image": "/img/promo-b-g.png",
        "url": "products/clearance-products",
        "newPrice": 9.29,
        "oldPrice": 12.29,
        "name": "Steel Thermal Beverage Container"
    },
    {
        "id": 2,
        "image": "/img/promo-b-g-1.jpg",
        "url": "products/clearance-products",
        "newPrice": 14.99,
        "oldPrice": 16.00,
        "name": "Steel Thermal Beverage Container"
    },
    {
        "id": 3,
        "image": "/img/promo-b-g-2.jpg",
        "url": "products/clearance-products",
        "newPrice": 19.99,
        "oldPrice": 25.00,
        "name": "Steel Thermal Beverage Container"
    }
]

