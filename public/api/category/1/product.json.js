[
    {
        "thumbnail": "img/products/image1.png",
        "title": "Promotional Shiny Bistro White Mug - 15 oz",
        "id": 7732,
        "min_quantity": 150,
        "production_time": "within 1 working day*",
        "lowest_price": 0.27,
        "slug": "pens-pencils/pens-plastic/bic-clic-stic-pen",
        "color_groups": [
            {
                "name": "Trim Colors",
                "colors": [
                    {
                        "title": "Black",
                        "id": 100,
                        "color_hex": "#000000",
                        "image": null
                    },
                    {
                        "title": "Assorted Assorted",
                        "id": 101,
                        "color_hex": null,
                        "image": "img/colors/assorted.jpg"
                    },
                    {
                        "title": "Grey",
                        "id": 102,
                        "color_hex": "#808080",
                        "image": null
                    },
                    {
                        "title": "White",
                        "id": 103,
                        "color_hex": "#fff",
                        "image": null
                    },
                    {
                        "title": "Pink",
                        "id": 104,
                        "color_hex": "#ff0201",
                        "image": null
                    },
                    {
                        "title": "Orange",
                        "id": 105,
                        "color_hex": "#ffb402",
                        "image": null
                    },
                    {
                        "title": "Green",
                        "id": 106,
                        "color_hex": "#50e3c2",
                        "image": null
                    },
                    {
                        "title": "Crimson",
                        "id": 107,
                        "color_hex": "#ce031b",
                        "image": null
                    },
                    {
                        "title": "Orange",
                        "id": 108,
                        "color_hex": "#ffb402",
                        "image": null
                    },
                    {
                        "title": "Green",
                        "id": 109,
                        "color_hex": "#50e3c2",
                        "image": null
                    },
                    {
                        "title": "Crimson",
                        "id": 110,
                        "color_hex": "#ce031b",
                        "image": null
                    },
                    {
                        "title": "Green",
                        "id": 106,
                        "color_hex": "#50e3c2",
                        "image": null
                    },
                    {
                        "title": "Crimson",
                        "id": 107,
                        "color_hex": "#ce031b",
                        "image": null
                    },
                    {
                        "title": "Orange",
                        "id": 108,
                        "color_hex": "#ffb402",
                        "image": null
                    },
                    {
                        "title": "Green",
                        "id": 109,
                        "color_hex": "#50e3c2",
                        "image": null
                    },
                    {
                        "title": "Crimson",
                        "id": 110,
                        "color_hex": "#ce031b",
                        "image": null
                    },
                    {
                        "title": "Green",
                        "id": 106,
                        "color_hex": "#50e3c2",
                        "image": null
                    },
                    {
                        "title": "Crimson",
                        "id": 107,
                        "color_hex": "#ce031b",
                        "image": null
                    },
                    {
                        "title": "Orange",
                        "id": 108,
                        "color_hex": "#ffb402",
                        "image": null
                    },
                    {
                        "title": "Green",
                        "id": 109,
                        "color_hex": "#50e3c2",
                        "image": null
                    },
                    {
                        "title": "Crimson",
                        "id": 110,
                        "color_hex": "#ce031b",
                        "image": null
                    }
                ]
            },
            {
                "name": "Barrel Colors",
                "colors": [
                    {
                        "title": "Assorted",
                        "id": 111,
                        "color_hex": null,
                        "image": "img/colors/assorted.jpg"
                    },
                    {
                        "title": "Assorted",
                        "id": 112,
                        "color_hex": null,
                        "image": "img/colors/assorted.jpg"
                    },
                    {
                        "title": "Assorted",
                        "id": 113,
                        "color_hex": null,
                        "image": "img/colors/assorted.jpg"
                    }
                ]
            }
        ],
        "imprint_colors": [
            {
                "name": "Trim Colors",
                "colors": [
                    {
                        "title": "Black",
                        "id": 100,
                        "color_hex": "#000000",
                        "image": null
                    },
                    {
                        "title": "Assorted",
                        "id": 100,
                        "color_hex": null,
                        "image": "img/colors/assorted.jpg"
                    }
                ]
            }
        ],
        "price_grid": [
            {
                "quantity": 250,
                "regular_price": 0.34,
                "sale_price": 0.28
            },
            {
                "quantity": 500,
                "regular_price": 0.25,
                "sale_price": 0.22
            },
            {
                "quantity": 1000,
                "regular_price": 0.17,
                "sale_price": 0.15
            },
            {
                "quantity": 3000,
                "regular_price": 0.16,
                "sale_price": 0.13
            },
            {
                "quantity": 5000,
                "regular_price": 0.14,
                "sale_price": 0.12
            },
            {
                "quantity": 7000,
                "regular_price": 0.12,
                "sale_price": 0.10
            }
        ],
        "sizes":
        [
            "XS",
            "L",
            "XL",
            "XXL",
            "XXXL"
        ]
    },
    {
        "thumbnail": "img/products/image3.png",
        "title": "Promotional Shiny Bistro Color Mug - 15 oz",
        "id": 7733,
        "min_quantity": 150,
        "production_time": "within 1 working day*",
        "lowest_price": 75.82,
        "slug": "pens-pencils/pens-plastic/bic-clic-stic-pen",
        "color_groups": [
            {
                "name": "Trim Colors",
                "colors": [
                    {
                        "title": "Assorted",
                        "id": 101,
                        "color_hex": null,
                        "image": "img/colors/assorted.jpg"
                    },
                    {
                        "title": "Grey",
                        "id": 102,
                        "color_hex": "#808080",
                        "image": null
                    },
                    {
                        "title": "White",
                        "id": 103,
                        "color_hex": "#fff",
                        "image": null
                    },
                    {
                        "title": "Pink",
                        "id": 104,
                        "color_hex": "#ff0201",
                        "image": null
                    },
                    {
                        "title": "Orange",
                        "id": 105,
                        "color_hex": "#ffb402",
                        "image": null
                    },
                    {
                        "title": "Green",
                        "id": 106,
                        "color_hex": "#50e3c2",
                        "image": null
                    },
                    {
                        "title": "Black",
                        "id": 100,
                        "color_hex": "#000000",
                        "image": null
                    },
                    {
                        "title": "Assorted",
                        "id": 101,
                        "color_hex": null,
                        "image": "img/colors/assorted.jpg"
                    },
                    {
                        "title": "Grey",
                        "id": 102,
                        "color_hex": "#808080",
                        "image": null
                    },
                    {
                        "title": "White",
                        "id": 103,
                        "color_hex": "#fff",
                        "image": null
                    },
                    {
                        "title": "Pink",
                        "id": 104,
                        "color_hex": "#ff0201",
                        "image": null
                    },
                    {
                        "title": "Orange",
                        "id": 105,
                        "color_hex": "#ffb402",
                        "image": null
                    },
                    {
                        "title": "Green",
                        "id": 106,
                        "color_hex": "#50e3c2",
                        "image": null
                    },
                    {
                        "title": "Crimson",
                        "id": 107,
                        "color_hex": "#ce031b",
                        "image": null
                    },
                    {
                        "title": "Orange",
                        "id": 108,
                        "color_hex": "#ffb402",
                        "image": null
                    }
                ]
            }
        ],
        "imprint_colors": [
            {
                "name": "Trim Colors",
                "colors": [
                    {
                        "title": "Pink",
                        "id": 100,
                        "color_hex": "#ff0201",
                        "image": null
                    },
                    {
                        "title": "Assorted",
                        "id": 100,
                        "color_hex": null,
                        "image": "img/colors/assorted.jpg"
                    }
                ]
            }
        ],
        "price_grid": [
            {
                "quantity": 250,
                "regular_price": 0.34,
                "sale_price": 0.28
            },
            {
                "quantity": 500,
                "regular_price": 0.25,
                "sale_price": 0.22
            },
            {
                "quantity": 1000,
                "regular_price": 0.17,
                "sale_price": 0.15
            },
            {
                "quantity": 3000,
                "regular_price": 0.16,
                "sale_price": 0.13
            },
            {
                "quantity": 5000,
                "regular_price": 0.14,
                "sale_price": 0.12
            },
            {
                "quantity": 7000,
                "regular_price": 0.12,
                "sale_price": 0.10
            }
        ]
    },
    {
        "thumbnail": "img/products/image2.png",
        "title": "Sticky Notes 3 x 3 pad 25 sheet count",
        "id": 7734,
        "min_quantity": 150,
        "production_time": "within 1 working day*",
        "lowest_price": 0.27,
        "slug": "pens-pencils/pens-plastic/bic-clic-stic-pen",
        "color_groups": [
            {
                "name": "Trim Colors",
                "colors": [
                    {
                        "title": "Pink",
                        "id": 100,
                        "color_hex": "#ff0201",
                        "image": null
                    },
                    {
                        "title": "Assorted",
                        "id": 100,
                        "color_hex": null,
                        "image": "img/colors/assorted.jpg"
                    }
                ]
            }
        ],
        "imprint_colors": [
            {
                "name": "Trim Colors",
                "colors": [
                    {
                        "title": "Black",
                        "id": 100,
                        "color_hex": "#000000",
                        "image": null
                    },
                    {
                        "title": "Assorted",
                        "id": 100,
                        "color_hex": null,
                        "image": "img/colors/assorted.jpg"
                    }
                ]
            }
        ],
        "price_grid": [
            {
                "quantity": 250,
                "regular_price": 0.34,
                "sale_price": 0.28
            },
            {
                "quantity": 500,
                "regular_price": 0.25,
                "sale_price": 0.22
            },
            {
                "quantity": 1000,
                "regular_price": 0.17,
                "sale_price": 0.15
            },
            {
                "quantity": 3000,
                "regular_price": 0.16,
                "sale_price": 0.13
            },
            {
                "quantity": 5000,
                "regular_price": 0.14,
                "sale_price": 0.12
            },
            {
                "quantity": 7000,
                "regular_price": 0.12,
                "sale_price": 0.10
            }
        ],
        "sizes":
        [
            "XS",
            "S",
            "M",
            "L",
            "XL",
            "XXL",
            "XXXL"
        ]
    }
]