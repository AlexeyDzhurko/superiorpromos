[
    {
        "title": "Apparel & custom caps",
        "id": 1,
        "active": false,
        "slug": "aparel-and-custom-caps",
        "sub_categories": [
            {
                "title": "Custom T-Shirts2",
                "id": 10,
                "slug": "auto-accessories/license-plate-frames",
                "sub_categories": [
                    {
                        "title": "Coolers & Lunch Bags3",
                        "slug": "",
                        "sub_categories": [
                            {
                                "title": "Coolers & Lunch Bags4",
                                "slug": "auto-accessories/license-plate-frames"
                            },
                            {
                                "title": "Clear Bags (NFL Ready)",
                                "slug": "bags-totes-more2/clear-bags"
                            },
                            {
                                "title": "Grocery Tote Bags",
                                "slug": "bags-totes-more/grocery-tote-bags"
                            },
                            {
                                "title": "Tote Bags",
                                "slug": ""
                            },
                            {
                                "title": "Drawstring & Backpacks",
                                "slug": ""
                            },
                            {
                                "title": "Plastic, Paper, Shopping Bags",
                                "slug": ""
                            },
                            {
                                "title": "Duffle & Garment Bags",
                                "slug": ""
                            },
                            {
                                "title": "Sport & Gym Bags",
                                "slug": ""
                            },
                            {
                                "title": "Messenger & Computer Bags",
                                "slug": ""
                            },
                            {
                                "title": "Golf Bags",
                                "slug": ""
                            },
                            {
                                "title": "Laundry Bags",
                                "slug": ""
                            },
                            {
                                "title": "Shoe & Utility Bags",
                                "slug": ""
                            },
                            {
                                "title": "Fanny Pack",
                                "slug": ""
                            }
                        ]
                    },
                    {
                        "title": "Clear Bags (NFL Ready)",
                        "slug": "bags-totes-more2/clear-bags"
                    },
                    {
                        "title": "Grocery Tote Bags",
                        "slug": "grocery-tote-bags"
                    },
                    {
                        "title": "Tote Bags",
                        "slug": ""
                    },
                    {
                        "title": "Drawstring & Backpacks",
                        "slug": ""
                    },
                    {
                        "title": "Plastic, Paper, Shopping Bags",
                        "slug": ""
                    },
                    {
                        "title": "Duffle & Garment Bags",
                        "slug": ""
                    },
                    {
                        "title": "Sport & Gym Bags",
                        "slug": ""
                    },
                    {
                        "title": "Messenger & Computer Bags",
                        "slug": ""
                    },
                    {
                        "title": "Golf Bags",
                        "slug": ""
                    },
                    {
                        "title": "Laundry Bags",
                        "slug": ""
                    },
                    {
                        "title": "Shoe & Utility Bags",
                        "slug": ""
                    },
                    {
                        "title": "Fanny Pack",
                        "slug": ""
                    }
                ]
            },
            {
                "title": "Caps, Visors & Bandannas",
                "slug": "auto-accessories/license-plate-frames"
            },
            {
                "title": "Polo Shirts",
                "slug": ""
            },
            {
                "title": "Performance Apparel",
                "slug": ""
            },
            {
                "title": "Fleece, Jackets & Outerwear",
                "slug": ""
            },
            {
                "title": "Womens Shop",
                "slug": ""
            },
            {
                "title": "American Apparel",
                "slug": ""
            },
            {
                "title": "Dress Shirts",
                "slug": ""
            },
            {
                "title": "Belt Buckles",
                "slug": ""
            },
            {
                "title": "Kitchen Wear",
                "slug": ""
            },
            {
                "title": "Sandals",
                "slug": ""
            },
            {
                "title": "Youth T-Shirts",
                "slug": ""
            },
            {
                "title": "Safety Gear",
                "slug": ""
            },
            {
                "title": "Socks",
                "slug": ""
            }
        ]
    },
    {
        "title": "Auto accessories",
        "active": false,
        "slug": "auto-accessories",
        "sub_categories": [
            {
                "title": "License Plate Frames",
                "slug": "license-plate-frames"
            },
            {
                "title": "Auto Tools & Emergency Kits",
                "slug": ""
            },
            {
                "title": "Hitch Cover",
                "slug": ""
            },
            {
                "title": "Air Fresheners",
                "slug": ""
            },
            {
                "title": "Ice Scrapers",
                "slug": ""
            },
            {
                "title": "Promotional Pens",
                "slug": ""
            },
            {
                "title": "CD Holders",
                "slug": ""
            },
            {
                "title": "Car Organizers",
                "slug": ""
            },
            {
                "title": "Window Shades",
                "slug": ""
            },
            {
                "title": "Visor Accessories",
                "slug": ""
            },
            {
                "title": "Car Flags",
                "slug": ""
            },
            {
                "title": "Bumper Stickers & Decals",
                "slug": ""
            },
            {
                "title": "Ad Plates",
                "slug": "/t-shirts"
            }
        ]
    },
    {
        "title": "Awards & recognition",
        "active": false,
        "sub_categories": [
            {
                "title": "Plaques & Awards",
                "slug": ""
            },
            {
                "title": "Frames",
                "slug": ""
            },
            {
                "title": "Chest Boxes",
                "slug": ""
            },
            {
                "title": "Paperweights",
                "slug": ""
            },
            {
                "title": "Ornaments",
                "slug": ""
            },
            {
                "title": "Drinkware",
                "slug": ""
            },
            {
                "title": "Vases & Bowls",
                "slug": ""
            },
            {
                "title": "Clocks",
                "slug": ""
            }
        ]
    },
    {
        "title": "Bags, Totes & more",
        "active": false,
        "slug": "bags-totes-more",
        "sub_categories": [
            {
                "title": "Coolers & Lunch Bags",
                "slug": ""
            },
            {
                "title": "Clear Bags (NFL Ready)",
                "slug": "bags-totes-more2/clear-bags"
            },
            {
                "title": "Grocery Tote Bags",
                "slug": "bags-totes-more/grocery-tote-bags"
            },
            {
                "title": "Tote Bags",
                "slug": ""
            },
            {
                "title": "Drawstring & Backpacks",
                "slug": ""
            },
            {
                "title": "Plastic, Paper, Shopping Bags",
                "slug": ""
            },
            {
                "title": "Duffle & Garment Bags",
                "slug": ""
            },
            {
                "title": "Sport & Gym Bags",
                "slug": ""
            },
            {
                "title": "Messenger & Computer Bags",
                "slug": ""
            },
            {
                "title": "Golf Bags",
                "slug": ""
            },
            {
                "title": "Laundry Bags",
                "slug": ""
            },
            {
                "title": "Shoe & Utility Bags",
                "slug": ""
            },
            {
                "title": "Fanny Pack",
                "slug": ""
            }
        ]
    },
    {
        "title": "Calendars & more",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Card Cases, Wallets",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Clocks & Watches",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Computer & Office",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Direct Mail Post",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "CardsElectronic and Tech",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Golf Products",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Greeting Cards",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Health & Personal",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Keychains & Tags",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Kitchen, Home & Pet",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Magnets",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Mint & Edibles",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Mugs & Drinkware",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Novelty & Toys",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Outdoor Items",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Pens & more",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Portfolios & more",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Sticky Notes & Cubes",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Stress Relievers",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Tools & Flashlights",
        "active": false,
        "sub_categories": [

        ]
    },
    {
        "title": "Trade Show Items",
        "active": false,
        "sub_categories": [

        ]
    }
]