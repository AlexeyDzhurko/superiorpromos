$(function () {
    var type = 'name';
    var selectItems = [];
    var productArray = [];
    var colorList = '';
    var validate = true;
    var searchType = null;
    var total = 0;
    var current_page = 1;
    var items_per_page = 20;
    var pages = 1;

    init();

    /**
     * Init function
     */
    function init() {
        searchSupplier();
        changeType();
        selectAll();
        selectItem();
        importSupplierProducts();
        moveProductToForm();
        recalculatePriceItem();
        recalculateAllPriceItems();
        sendRequestAddColor();
        openImprintBlock();
        categoriesTree();
        activeCategories();
    }

    /**
     * Change search by type (search by supplier_id or supplier_name)
     */
    function changeType() {
        $('#search-type').change(function () {
            type = $( '#search-type' ).val();
            searchSupplier();
        });
    }

    /**
     * Search supplier in array
     */
    function searchSupplier() {
        $('#search-supplier option').remove();

        $('#search-supplier').select2({
            width: 300,
            ajax: {
                url: '/admin/sage/supplier-list',
                dataType: 'json',
                data: function (params) {
                    return {
                        query: params.term,
                        type: type
                    };
                },
                results: function (data) {
                    return { results: data };
                },
                processResults: function (data) {
                    var answer = [];
                    for (var i = 0; i < data.length; i++ ) {
                        answer.push({
                            id: data[i].id,
                            text: type === 'name' ? data[i].name: data[i].supplier_id
                        });
                    }
                    return {
                        results: answer
                    };
                },
                cache: true
            },
        });
    }

    /**
     * Get supplier products by parameter (supplier_id or supplier_name)
     */
    function search() {
        var selectedVal = $('#search-supplier option:selected').val();
        $('#voyager-loader').show();
        clearData();
        productArray = [];

        $.ajax({
            url: '/admin/sage/supplier-product-list/',
            type: 'POST',
            data: {
                supplier_id: selectedVal,
                items_per_page: items_per_page,
                current_page: current_page
            },
            success: function (res) {
                /**Store to local storage **/
                $('#voyager-loader').hide();
                productArray = res.items;
                searchType = 'supplier';
                formedPagination(res);
                formedAnswer(res.items);
            },
            error: function (error) {
                console.error(error);
                $('#voyager-loader').hide();
            }
        });
    }

    $('#supplier-form').submit(function (e) {
        e.preventDefault();
        current_page = 1;
        search();
    });

    /**
     * Formed supplier's product table
     * @param data
     */
    function formedAnswer(data) {
        import_supplier_table = $('#import-supplier-table tbody');
        import_supplier_table.html('');
        $('#pagination').html('');

        for (var i = 0; i < data.length; i++) {
            import_supplier_table.append('<tr data-product-key="'+i+'" class="import-item">'+
                '<td class="checkbox-import"> <input type="checkbox" data-origin-id="'+data[i].ProductID+'" data-origin-name="'+data[i].PrName+'" data-origin-image="'+data[i].PicLink+'">' +
                '<td>' +
                    data[i].SPC +
                '</td>' +
                '<td><img src="'+data[i].PicLink+'" width="100" height="auto" alt="'+data[i].PrName+'"></td>' +
                '<td width="500">' +
                    '<p>' +
                        '<b>Name:</b> '+data[i].PrName+'' +
                    '</p>' +
                    /*'<p>' +
                        '<b>Description:</b> '+data[i].Description+'' +
                    '</p>' +
                    '<p>' +
                        '<b>Colors:</b>' +data[i].Colors+'' +
                    '</p>' +
                    '<p>' +
                        '<b>Themes:</b>' +data[i].Themes+'' +
                    '</p>' +
                    '<p>' +
                        '<b>Price:</b>' +data[i].MinPrice+'-'+data[i].MaxPrice+'' +
                    '</p>' +
                    '<p>' +
                        '<b>Code:</b>' +data[i].SPC+'' +
                    '</p>' +*/
                '</td>' +
                '</tr>>')
        }

        pagination_str =
            '<ul class="pagination">' +
                '<li class="page-item ' + (current_page == 1 ? 'disabled' : '') + '">' +
                    '<a class="page-link" href="#" data-target="pagination" data-content="1" >First</a>' +
                '</li>' +
                '<li class="page-item ' + (current_page == 1 ? 'disabled' : '') + '">' +
                    '<a class="page-link" href="#" data-target="pagination" data-content="' + (current_page - 1) + '" >&laquo;</a>' +
                '</li>';

        for (var i = 0; i < pages; i++) {
            pagination_str +=
                '<li class="page-item' + (i == current_page - 1 ? ' active' : '') + '">' +
                    '<a class="page-link" data-target="pagination" data-content="' + (i + 1) + '" href="#">' + (i + 1) + '</a>' +
                '</li>';
        }

        pagination_str +=
                '<li class="page-item ' + (current_page == pages ? 'disabled' : '') + '">' +
                    '<a class="page-link" href="#" data-target="pagination" data-content="' + (parseInt(current_page) + 1) + '" >&raquo;</a>' +
                '</li>' +
                '<li class="page-item ' + (current_page == pages ? 'disabled' : '') + '">' +
                    '<a class="page-link" href="#" data-target="pagination" data-content="' + pages + '" >Last</a>' +
                '</li>' +
            '</ul>';

        $('#pagination').append(pagination_str);
    }

    /**
     * Select / Deselect all
     */
    function selectAll() {
        $('.page-content').on('change', '#select-deselect-all', function () {
            var selectData = $('#select-deselect-all').prop('checked');
            selectItems = [];
            var input = null;

            $('#import-supplier-table tbody tr').each(function() {
                if (selectData) {
                    var key = $(this).data('product-key');
                    $(this).find('.checkbox-import').find('input').prop('checked','checked');
                    $(this).addClass('import-active');
                    input = $(this).find('.checkbox-import').find('input');
                    selectItems.push(productArray[key]);
                } else {
                    $(this).find('.checkbox-import').find('input').prop('checked', '');
                    $(this).removeClass('import-active');
                }
            });

            if (selectItems.length > 0 ) {
                $('.add-to-form').prop("disabled", false);
            } else {
                $('.add-to-form').prop("disabled", true);
                clearData();
            }

            if(!selectData) {
                selectItems = [];
            }
        });
    }

    /**
     * Change select / deselect some one item
     */
    function selectItem() {
        $('#import-supplier-table').on('click', '.import-item', function () {
            var element = $(this);
            if (element.hasClass('import-active')) {
                element.removeClass('import-active');
                element.find('.checkbox-import input[type="checkbox"]').prop('checked', '');
            } else {
                element.addClass('import-active');
                element.find('.checkbox-import input[type="checkbox"]').prop('checked','checked');
            }
            checkSelectItems();
        });

        checkSelectItems();
    }

    /**
     * Check select items
     */
    function checkSelectItems() {
        var allSelect = true;
        selectItems = [];
        var input = null;

        $('#import-supplier-table tbody tr').each(function() {
            if (!$(this).find('.checkbox-import').find('input').prop('checked')) {
                allSelect = false;
            } else {
                var key = $(this).data('product-key');
                input = $(this).find('.checkbox-import').find('input');
                selectItems.push(productArray[key]);
            }
        });

        if (selectItems.length > 0 ) {
            $('.add-to-form').prop("disabled", false);
        } else {
            $('.add-to-form').prop("disabled", true);
            clearData();
        }

        if (allSelect && selectItems.length > 0) {
            $('#select-deselect-all').prop('checked', 'checked');
        } else {
            $('#select-deselect-all').prop('checked', '');
        }
    }

    /**
     * Add data single product into import form
     */
    function moveProductToForm() {
        $('.page-content').on('click', '.add-to-form', function () {
            var single = true;
            var product = selectItems[0];
            var query_result = null;

            if (selectItems.length > 1) {
                single = false;
            }

            if(searchType == 'product' && single) {
                query_result = getProductDetail(product.ProductID);
                selectItems[0] = query_result;
                product = selectItems[0];
            }

            $('.modal-body').html('');

            var element = $(this);
            var key = element.data('product-key');

            $('#import-product').prop("disabled", false);
            if (single) {
                /**
                 * Import only one
                 */
                $('#importName').val('Generate automatically').prop("disabled", false);
                $('#importUrlName').val('Generate automatically').prop("disabled", false);
                $('#importKeywords').val('Generate automatically').prop("disabled", false);
                $('#importDescription').val('Generate automatically').prop("disabled", false);
                $('#importProductTimeFrom').val('Generate automatically').prop("disabled", false);
                $('#importProductTimeTo').val('Generate automatically').prop("disabled", false);
                $('#importQty').val('Generate automatically').prop("disabled", false);
                $('#importBoxWeight').val('Generate automatically').prop("disabled", false);
                $('#importShippingZip').val('Generate automatically').prop("disabled", false);
                $('#importVendor').val('Generate automatically').prop("disabled", false);
                $('#importVendorSku').val('Generate automatically').prop("disabled", false);
                $('#importImprintArea').val('Generate automatically').prop("disabled", false);

                $('.price-grid-table').show();
                $('#imprint-blocks').show();

                $('#importName').val(product.PrName);
                $('#importUrlName').val(product.Url);
                $('#importKeywords').val(product.Keywords);
                $('#importDescription').val(product.Description);
                $('#importProductTimeFrom').val(product.TimeFrom);
                $('#importProductTimeTo').val(product.TimeTo);
                $('#importQty').val(product.UnitsPerCarton);
                $('#importBoxWeight').val(product.WeightPerCarton);
                $('#importShippingZip').val(product.ShipPointZip);
                $('#importVendor').val(product.LineName);
                $('#importVendorSku').val(product.ItemNum);
                $('#importPriceCode').val(product.PrCode);
                $('#importSetupPriceCode').val(product.SetupChgCode);
                $('#importAdditionalColor').val(product.AddClrChgCode);
                $('#importAdditionalColorRun').val(product.AddClrRunChgCode);
                $('#importImprintArea').val(product.ImprintArea);

                for (var n = 1; n < 7; n++) {
                    $('.quantity-'+n).val(formedPrice(product['Qty'+n]));
                    $('#regular-price-'+n).val(formedPrice(product['Prc'+n]));
                    $('#catalog-price-'+n).val(formedPrice(product['CatPrc'+n])).prop("disabled", true);
                    $('#net-price-'+n).val(formedPrice(product['Net'+n])).prop("disabled", true);
                    $('#sale-price-'+n).val(formedPrice(product['Net'+n]));
                    $('#sale-price-'+n).data('sale-price', formedPrice(product['Net'+n]));
                }

                imprintLocation(product);

                var colors = product.Colors.split(', ');
                findColors(colors, product.PrName);
            } else {
                /**
                 * Bulk import
                 */
                $('#importName').val('Generate automatically').prop("disabled", true);
                $('#importUrlName').val('Generate automatically').prop("disabled", true);
                $('#importKeywords').val('Generate automatically').prop("disabled", true);
                $('#importDescription').val('Generate automatically').prop("disabled", true);
                $('#importProductTimeFrom').val('Generate automatically').prop("disabled", true);
                $('#importProductTimeTo').val('Generate automatically').prop("disabled", true);
                $('#importQty').val('Generate automatically').prop("disabled", true);
                $('#importBoxWeight').val('Generate automatically').prop("disabled", true);
                $('#importShippingZip').val('Generate automatically').prop("disabled", true);
                $('#importVendor').val('Generate automatically').prop("disabled", true);
                $('#importVendorSku').val('Generate automatically').prop("disabled", true);
                $('#importImprintArea').val('Generate automatically').prop("disabled", true);

                $('.price-grid-table').hide();
                $('#imprint-blocks').hide();
                var colors = [];
                for (var x = 0; x < selectItems.length; x++) {
                    var colorPart = selectItems[x].Colors.split(', ');
                    for(var y = 0; y < colorPart.length; y++) {
                        colors.push(colorPart[y]);
                    }
                }
                findColors(colors);
            }
        });
    }

    /**
     * @param colors
     */
    function findColors(colors) {
        $.ajax({
            url: '/admin/color/find',
            type: 'POST',
            data: {'data': colors},
            success: function (data) {
                var notExist = [];
                var uniqueColor = colors.filter((v, i, a) => a.indexOf(v) === i);
                for (var i = 0; i< uniqueColor.length; i++) {
                    if (uniqueColor[i] in data) {
                        $('.modal-body').append('<div class="form-group"><input type="text" class="form-control color-name" name="name" value="'+data[uniqueColor[i]].name+colorList+'" disabled></div>')
                    } else {
                        notExist.push({'id': i, 'name': uniqueColor[i]});
                    }
                }
                /**
                 * If not empty
                 */
                if (notExist.length > 0) {
                    $('.modal-body').append(createColor(notExist))
                }
            }
        });
    }

    /**
     * Add color form
     * @param notExist
     * @returns {string}
     */
    function createColor(notExist) {
        var result = '';
        result += '<form class="form-inline" id="add-color">';
        for (var i = 0; i < notExist.length; i++) {
            result += '<div class="row-color">';
            result += '<div class="form-group">';
            result += '<input type="text" class="form-control color-name" name="name" value="'+notExist[i].name+'" disabled>';
            result += '</div>';
            result += '<div class="form-group hex-color">';
            result += '<input type="color" class="form-control color-hex" name="color_hex" value="#b15b5b">';
            result += '</div>';
            result += '</div>';
            result += '<br>';
        }
        result += '<button type="submit" class="btn btn-default add-color">add</button>';
        result += '</form>';

        return result;
    }

    /**
     * Add color to system
     */
    function sendRequestAddColor() {
        $('.modal-body').on('click', '.add-color', function (e) {
            e.preventDefault();
            var data = [];
            var elem = $(this);
            $('.row-color').each(function () {
                data.push({
                    'name': $(this).children('.form-group').find('.color-name').val(),
                    'color_hex': $(this).children('.form-group').find('.color-hex').val(),
                });
            });

            $.ajax({
                url: '/admin/color/multiple',
                type: 'POST',
                data: {colors: data},
                success: function () {
                    elem.hide();
                    $('.row-color').each(function () {
                        $(this).children('.form-group').find('.color-hex').hide();
                        $(this).children('.hex-color').append('<span class="glyphicon glyphicon-ok"></span>');
                    });
                }
            });
        });
    }

    /**
     * @param val
     * @returns float
     */
    function formedPrice(val) {
        return val.length > 0 ? parseFloat(val) : 0;
    }

    /**
     * Recalculate one price item
     */
    function recalculatePriceItem() {
        $('.page-content').on('click', '.ind-recalc', function () {
            var position = $(this).data('position');
            var salePrice = $('#sale-price-' + position).data('sale-price') ? $('#sale-price-' + position).data('sale-price') :  $('#sale-price-' + position).val();
            var percent = $('#individual-recalc-' + position).val();

            if (salePrice) {
                $('#sale-price-' + position).val(
                    (
                        parseFloat(salePrice) * (1 + parseFloat(percent)/100)
                    ).
                    toFixed(2)
                );
            }
        });
    }

    /**
     * Recalculate all price items
     */
    function recalculateAllPriceItems() {
        $('.page-content').on('click', '.all-recalc', function () {
            var percent = $('#importPriceGrid').val();
            $( ".sale-price" ).each(function() {
                var salePrice = $( this ).data('sale-price')?$( this ).data('sale-price'):$( this ).val();
                if (salePrice) {
                    $( this ).val(
                        (
                            parseFloat(salePrice) * (1 + parseFloat(percent)/100)
                        ).
                        toFixed(2)
                    );
                }
            });

        });
    }

    /**
     * Add products for import
     */
    function importSupplierProducts() {
        $('.page-content').on('click', '#import-product', function (e) {
            e.preventDefault();
            var selectedVal = $('#search-supplier option:selected').val();
            var priceGridPercent = formedPriceGrid();
            var childCategories = getChildCategories();

            var imprintPriceGrid = formedImprintPriceGrid();

            if(searchType == 'product') {
                if(selectItems.length > 3) {
                    alert("You selected " + selectItems.length + " items. This long request, please wait ending it.");
                }
                for (var i = 0, len = selectItems.length; i < len; i++) {
                    if (typeof selectItems[i].supplier_id == 'undefined') {
                        selectItems[i] = getProductDetail(selectItems[i].ProductID);
                    }
                }
            }

            var data = {
                products: selectItems,
                supplierId: selectedVal,
                priceGridPercent: priceGridPercent,
                childCategories: childCategories,
                imprintPriceGrid: imprintPriceGrid,
            };

            validationAndInsert(data);
        });
    }

    /**
     * Show imprint block
     */
    function openImprintBlock() {
        $('.page-content').on('click', '.open-block', function (e) {
            e.preventDefault();

            var id = $(this).data('id');
            $('.imprint-block-'+id).removeClass('hide');
        });
    }

    /**
     * Get list child categories
     * @return {Array}
     */
    function getChildCategories() {
        var result = [];
        $('.category-content.active').each(function() {
            result.push($(this).data('category-id'));
        });

        return result;
    }

    /**
     * write imprint data
     * @param sageProduct
     */
    function imprintLocation(sageProduct) {

        for (var n = 1; n < 6; n++) {
            for (var i = 1; i < 7; i++) {
                $('.location-setup-fee-'+n+'-'+i).val(formedPrice(sageProduct.SetupChg));
                if (n !== 1 ) {
                    $('.additional-location-running-fee-'+n+'-'+i).val(formedPrice(sageProduct['AddClrRunChg'+i]));
                }
                $('.additional-color-setup-fee-'+n+'-'+i).val(formedPrice(sageProduct.SetupChg));
                $('.additional-color-running-fee-'+n+'-'+i).val(formedPrice(sageProduct['AddClrRunChg'+i]));
            }
        }
    }

    /**
     * formed imprint data for request
     */
    function formedImprintPriceGrid() {
        var result = [];

        $('.imprint-block').each(function() {
            var checkUse = $(this).find('.imprint-status').prop('checked');
            var type = $(this).find('.import-imprint-name').data('key');
            var key = $(this).find('.import-imprint-name').data('key');
            if(checkUse) {
                var quantity= [];
                var locationSetupFee= [];
                var additionalLocationRunningFee= [];
                var additionalColorSetupFee= [];
                var additionalColorRunningFee = [];

                for (var i = 1; i < 7; i++) {
                    quantity.push(
                        $('.quantity-'+i).val() ? parseInt($('.quantity-'+i).val()):0
                    )
                    locationSetupFee.push(
                        $('.location-setup-fee-'+key+'-'+i).val() ? parseFloat($('.location-setup-fee-'+key+'-'+i).val()):0
                    );
                    additionalLocationRunningFee.push(
                        $('.additional-location-running-fee-'+key+'-'+i).val() ? parseFloat($('.additional-location-running-fee-'+key+'-'+i).val()) : 0
                    );
                    additionalColorSetupFee.push(
                        $('.additional-color-setup-fee-'+key+'-'+i).val() ? parseFloat($('.additional-color-setup-fee-'+key+'-'+i).val()) : 0
                    );
                    additionalColorRunningFee.push(
                        $('.additional-color-running-fee-'+key+'-'+i).val() ? parseFloat($('.additional-color-running-fee-'+key+'-'+i).val()) : 0
                    );
                }
                result.push({
                    quantity: quantity,
                    locationSetupFee: locationSetupFee,
                    additionalLocationRunningFee: additionalLocationRunningFee,
                    additionalColorSetupFee: additionalColorSetupFee,
                    additionalColorRunningFee: additionalColorRunningFee,
                    type: type
                });
            }
        });

        return result;
    }

    /**
     * Formed price info
     * @returns {Array}
     */
    function formedPriceGrid() {
        var result = [];
        for (var n = 1; n < 7; n++) {
            var percent = checkPercent($('#individual-recalc-'+n).val(), $('#importPriceGrid').val());
            result.push({
                quantity: parseInt($('.quantity-'+n).val()),
                regular_price: parseFloat($('#regular-price-'+n).val()),
                catalog_price: parseFloat($('#catalog-price-'+n).val()),
                net_price: parseFloat($('#net-price-'+n).val()),
                sale_price: parseFloat($('#sale-price-'+n).val()),
                percent: percent,
                total: $('#importPriceGrid').val()
            });
        }

        return result;
    }

    /**
     * Check percent amount
     * @returns {int}
     */
    function checkPercent(individual, group) {
        if (!individual && !group) {
            return 0;
        } else {
            return individual ? parseInt(individual) : parseInt(group);
        }
    }

    /**
     *
     * @param data
     */
    function validationAndInsert(data) {
        var colors = [];
        $('#voyager-loader').show();
        for (var key = 0; key < data.products.length; key++) {
            var preArr = data.products[key].Colors.split(', ');
            for (var i = 0; i< preArr.length; i++) {
                colors.push(preArr[i])
            }
        }

        $.ajax({
            url: '/admin/sage/check-colors',
            type: 'POST',
            data: {
                colors: colors.filter((v, i, a) => a.indexOf(v) === i)
            },
            success: function (resp) {
                if (parseInt(resp) !== colors.filter((v, i, a) => a.indexOf(v) === i).length) {
                    alert('not select all colors!');
                    $('#voyager-loader').hide();
                } else {
                    $.ajax({
                        url: '/admin/sage/supplier-product-import',
                        type: 'POST',
                        data: {
                            products: data.products,
                            supplierId: data.supplierId,
                            priceGridPercent: data.priceGridPercent,
                            colorMode: $('#importColorMode option:selected').val(),
                            mainCategory: $('#importCategory option:selected').val(),
                            childCategories: data.childCategories,
                            sale: $('#importSale').prop('checked'),
                            urlPrefix: $('#importUrlPrefix').val(),
                            imprintColorGroup: $('#importImprintColorGroup option:selected').val(),
                            imprintPriceGrid: data.imprintPriceGrid,
                        },
                        success: function () {
                            $('#voyager-loader').hide();
                            toastr.success("Successfully imported product");
                            //location.reload();
                        },
                        error: function (error) {
                            console.error(error);
                            toastr.error("Product import error");
                            console.log(error);
                            $('#voyager-loader').hide();
                        }
                    });
                }
            },
            error: function (error) {
                alert('Check colors request doesn\'t work!!!');
                console.log(error);
                $('#voyager-loader').hide();
            }
        });
    }

    /**
     * Active / Deactivate category
     */
    function activeCategories() {
        $('.page-content').on('click', '.category-content', function () {
            $(this).toggleClass('active');
        });
    }


    /**
     * Categories tree
     * @param event
     */
    function categoriesTree() {
        $('.add-to-form').prop("disabled", true);

        $('.page-content').on('click', '.categories-tree', function (event) {
            event = event || window.event
            var clickedElem = event.target || event.srcElement

            if (!hasClass(clickedElem, 'Expand')) {
                return;
            }

            var node = clickedElem.parentNode
            if (hasClass(node, 'ExpandLeaf')) {
                return;
            }

            var newClass = hasClass(node, 'ExpandOpen') ? 'ExpandClosed' : 'ExpandOpen'
            var re =  /(^|\s)(ExpandOpen|ExpandClosed)(\s|$)/
            node.className = node.className.replace(re, '$1'+newClass+'$3')
        });
    }

    /**
     *
     * @param elem
     * @param className
     * @returns {boolean}
     */
    function hasClass(elem, className) {
        return new RegExp("(^|\\s)"+className+"(\\s|$)").test(elem.className)
    }

    /**
     * Clear form data
     */
    function clearData() {
        selectItems = [];
        colorList = '';
        validate = true;

        for (var n = 1; n < 7; n++) {
            $('.quantity-'+n).val('');
            $('#regular-price-'+n).val('');
            $('#catalog-price-'+n).val('');
            $('#net-price-'+n).val('');
            $('#sale-price-'+n).val('');
            $('#sale-price-'+n).data('sale-price', '');
        }

        $('#importName').val('');
        $('#importUrlName').val('');
        $('#importKeywords').val('');
        $('#importDescription').val('');
        $('#importProductTimeFrom').val('');
        $('#importProductTimeTo').val('');
        $('#importQty').val('');
        $('#importBoxWeight').val('');
        $('#importShippingZip').val('');
        $('#importVendor').val('');
        $('#importVendorSku').val('');
        $('#importPriceCode').val('');
        $('#importSetupPriceCode').val('');
        $('#importAdditionalColor').val('');
        $('#importAdditionalColorRun').val('');
        $('#importImprintArea').val('');
        $('#importPriceGrid').val('');

        for (var n = 1; n < 6; n++) {
            for (var i = 1; i < 7; i++) {
                $('.location-setup-fee-'+n+'-'+i).val('');
                if (n !== 1 ) {
                    $('.additional-location-running-fee-'+n+'-'+i).val('');
                }
                $('.additional-color-setup-fee-'+n+'-'+i).val('');
                $('.additional-color-running-fee-'+n+'-'+i).val('');
            }
        }

        $('.add-to-form').prop("disabled", true);
        $('#import-product').prop("disabled", true);
        $('.price-grid-table').show();
        $('#imprint-blocks').show();
    }

    function quickProductsSearch() {
        $('#voyager-loader').show();
        clearData();

        $.ajax({
            type: "POST",
            url: '/admin/sage/supplier-products-search',
            data: {
                keywords: $('input[name=product_search_field]').val(),
                items_per_page: items_per_page,
                current_page: current_page
            },
            success: function (res) {
                /**Store to local storage **/
                $('#voyager-loader').hide();
                productArray = res.items;
                searchType = 'product';
                formedPagination(res);
                formedAnswer(res.items);
            },
            error: function (error) {
                console.error(error);
                $('#voyager-loader').hide();
            }
        });
    }

    $('#products-search-button').click(function() {
        current_page = 1;
        quickProductsSearch();
    });

    function getProductDetail(product_id) {
        $('#voyager-loader').show();
        var productDetail = null;

        $.ajax({
            type: "GET",
            url: '/admin/sage/supplier-product-detail/' + product_id,
            async: false, //TODO fix it in future
            success: function (res) {
                productDetail = res;
                $('#voyager-loader').hide();
            },
            error: function (error) {
                console.log(error);
                $('#voyager-loader').hide();
            }
        });

        return productDetail;
    }

    function formedPagination(data) {
        total = data.total;
        items_per_page = data.items_per_page;
        current_page = data.current_page;
        pages = data.pages;
    }

    function getProductsByPageId(obj) {
        current_page = obj.data("content");
        if(searchType == 'product') {
            quickProductsSearch();
        } else {
            search();
        }
    }

    $('.page-content').on('click', '.page-link', function() {
        getProductsByPageId($(this));
    });

    $('#quick-search-form').submit(function (e) {
        e.preventDefault();
        current_page = 1;
        quickProductsSearch();
    })
});