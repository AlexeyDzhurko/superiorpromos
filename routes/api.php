<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'SecurityController@login');
Route::post('registration', 'SecurityController@register');

Route::post('password/reset', 'SecurityController@reset');
Route::get('check-email/{email}', 'SecurityController@checkEmail');

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

/**
 * OAuth
 */
Route::get('oauth/{provider}/login', 'OAuthController@loginByToken');

/**
 * Product
 */
Route::group(['prefix' => 'product'], function () {
    Route::get('/', 'ProductController@index');
    Route::get('promotional-products', 'ProductController@promotionalProducts');
    Route::get('viewed-products', 'ProductController@view');
    Route::get('get-shipping-cost', 'ProductController@shippingCost');
    Route::post('quick-quote', 'ProductController@quickQuote');
    Route::post('quick-quote/email', 'ProductController@sendQuickQuoteEmail');
    Route::get('{product}/pdf', 'ProductController@pdf');


    /**Should be in the end always**/
    Route::get('{currentProduct}', 'ProductController@show')->where('currentProduct', '.+');
});

/**
 * Search
 */
Route::post('search', 'ProductController@search');
Route::post('quick-search', 'ProductController@quickSearch');

/**
 * Subscribes
 */
Route::group(['prefix' => 'subscribe'], function () {
    Route::post('coupon', 'SubscribeController@subscribeCoupon');
    Route::post('newsletter', 'SubscribeController@subscribeNewsletter');
});


/**
 * FAQ
 */
Route::resource('faq', 'FaqController', ['only' => ['index']]);

/**
 * Testimonials
 */
Route::get('testimonial', 'TestimonialController@index');

/**
 * Case Studies
 */
Route::get('case-study', 'CaseStudyController@index');

/**
 * Editable Blocks
 */
Route::get('block', 'BlockController@index');

/**
 * Get Editable block by key
 */
Route::get('block/{key}', 'BlockController@getByKey');

/**
 * Main Page Carousel (Slider)
 */
Route::get('slider', 'SliderController@index');

/**
 * Category
 */
Route::get('categories/popular', 'CategoryController@getPopularCategories');
Route::resource('categories', 'CategoryController', ['only' => ['index']]);
Route::get('categories/{category}/sub-categories', 'CategoryController@getSubCategories');
Route::get('categories/{category}', 'CategoryController@categoryProduct')->where('category', '.+');

/**
 * Colors
 */
Route::resource('colors', 'ColorController', ['only' => ['index']]);

/**
 * Quick links
 */
Route::resource('quick-links', 'QuickLinkController', ['only' => ['index']]);

/**
 * Blog
 */
Route::get('blog/posts', 'PostController@index');
Route::get('blog/posts/search', 'PostController@search');
Route::get('blog/categories', 'PostCategoriesController@index');

Route::get('promos', 'PromosController@index');

/**
 * Methods which needs Auth
 */
Route::group(['middleware' => ['jwt.auth']], function () {

    /**
     * Art Proofs
     */
    Route::group(['prefix' => 'art-proofs'], function () {
        Route::get('/', 'ArtProofController@index');
        Route::patch('{artProof}/approve', 'ArtProofController@approve');
        Route::patch('{artProof}/denied', 'ArtProofController@denied');
        Route::post('{artProof}/store-note', 'ArtProofController@storeNote');
        Route::get('{artProof}/file', 'ArtProofController@file');
        Route::get('unread', 'ArtProofController@getUnread');
        Route::patch('{artProof}/read', 'ArtProofController@setCustomerReadFlagAsRead');
    });

    /**
     * Wish list
     */
    Route::resource('wish-list', 'WishController', ['only' => ['index', 'store', 'destroy']]);
    Route::post('wish-list/clear', 'WishController@clearList');

    /**
     * Address
     */
    Route::match(['put', 'patch'], 'address/{address}/default', 'AddressController@makeDefault')->name('address.make_default');
    Route::resource('address', 'AddressController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);
    /**
     * User
     */
    Route::get('account', 'UserController@myAccountPage');
    Route::get('account/account-information', 'UserController@myAccountInformation');
    Route::post('account/account-information', 'UserController@updateMyAccountInformation');
    Route::group(['prefix' => 'user'], function () {
        Route::resource('payment_profile', 'PaymentProfileController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);
        Route::post('payment_profile/{payment_profile}/expiration_date', 'PaymentProfileController@updateExpirationDate');
        Route::match(['put', 'patch'], 'payment_profile/{payment_profile}/default', 'PaymentProfileController@makeDefault');
        Route::model('payment_profile', App\PaymentProfile::class);
    });

    /**
     * Order
     */
    Route::post('order/checkout', 'OrderController@checkout');
    Route::get('order', 'OrderController@index');

    /**
     * User Note
     */
    Route::resource('order/user-note', 'UserNoteController', [
        'only' => ['update'],
        'parameters' => ['user-note' => 'userNote']
    ]);
    Route::get('order/user-note/unread', 'UserNoteController@getUnread');
    Route::patch('order/user-note/{userNote}/read', 'UserNoteController@setCustomerReadFlagAsRead');

    /**
     * Order Item
     */
    Route::get('order/{order}/order-item/{orderItem}', 'OrderItemController@show');

    /**
     * Tax Exemption
     */
    Route::post('tax-exemption-certificate/upload', 'TaxExemptionCertificateController@uploadCertificate');

    /**
     * Saved Cart
     */
    Route::put('user/saved-cart/{savedCartItem}', 'SavedCartController@update');
    Route::post('user/saved-cart', 'SavedCartController@store');
    Route::get('user/saved-cart', 'SavedCartController@index');
    Route::delete('user/saved-cart/{savedCartItem}', 'SavedCartController@destroy');
    Route::post('user/saved-cart/move-to-cart', 'SavedCartController@moveToCart');

    /**
     * Order A Sample
     */
//    Route::resource('order_sample', 'OrderSampleController', ['only' => ['store', 'index']]);
    Route::get('order_sample', 'OrderSampleController@index');
    Route::post('cart-item/order-sample', 'OrderSampleController@store');
});

Route::group(['middleware' => ['jwt.optional']], function () {
    Route::resource('cart', 'CartController', ['only' => 'store']);
    Route::delete('cart', 'CartController@destroy');
    Route::put('cart/{id}', 'CartController@update');
    Route::resource('cart-item', 'CartItemController', ['only' => ['store', 'index']]);
    Route::resource('cart-discount', 'CartDiscountController', ['only' => ['index', 'store']]);
    Route::delete('cart-discount', 'CartDiscountController@destroy');
    Route::post('cart-item/import-from-order', 'CartItemController@importFromOrder');
    Route::post('cart-item/reorder', 'OrderController@reorder');
    Route::match(['PATCH', 'PUT'], 'cart-item/{cartItem}', 'CartItemController@update')->name('cart-item.update');
    Route::delete('cart-item/{cartItem}', 'CartItemController@destroy')->name('cart-item.destroy');
});

/**
 * Promotional Glossary
 */
Route::get('promotional-glossary', 'PromotionalGlossaryController@index');

/**
 * Current promotions
 */
Route::get('promotions', 'PromotionsController@index');

/**
 * Contact Us
 */
Route::post('contact-us', 'ContactUsController@store');
