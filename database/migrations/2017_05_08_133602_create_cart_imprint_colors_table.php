<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartImprintColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_item_imprint_colors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cart_item_imprint_id')->unsigned();
            $table->foreign('cart_item_imprint_id')
                ->references('id')
                ->on('cart_item_imprints')
                ->onDelete('cascade');
            $table->integer('color_id')->unsigned()->nullable();
            $table->foreign('color_id')
                ->references('id')
                ->on('colors')
                ->onDelete('set null');
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_item_imprint_colors');
    }
}
