<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartItemProductSubOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_item_product_sub_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cart_item_product_option_id')->unsigned();
            $table->foreign('cart_item_product_option_id', 'cip_option_id_cip_sub_option_id_foreign')
                ->references('id')
                ->on('cart_item_product_options')
                ->onDelete('cascade');
            $table->integer('product_sub_option_id')->unsigned()->nullable();
            $table->foreign('product_sub_option_id')
                ->references('id')
                ->on('product_sub_options')
                ->onDelete('set null');
            $table->string('name');
            $table->decimal('setup_price')->default(0);
            $table->decimal('item_price')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_item_product_sub_options');
    }
}
