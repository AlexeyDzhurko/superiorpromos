<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressFieldsToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('shipping_first_name');
            $table->string('shipping_middle_name')->nullable();
            $table->string('shipping_last_name');
            $table->string('shipping_title')->nullable();
            $table->string('shipping_suffix')->nullable();
            $table->string('shipping_company_name')->nullable();
            $table->string('shipping_address_line_1');
            $table->string('shipping_address_line_2')->nullable();
            $table->string('shipping_city');
            $table->string('shipping_state');
            $table->string('shipping_zip');
            $table->string('shipping_country');
            $table->string('shipping_province')->nullable();
            $table->string('shipping_day_telephone');
            $table->string('shipping_ext')->nullable();
            $table->string('shipping_fax')->nullable();

            $table->string('billing_first_name');
            $table->string('billing_middle_name')->nullable();
            $table->string('billing_last_name');
            $table->string('billing_title')->nullable();
            $table->string('billing_suffix')->nullable();
            $table->string('billing_company_name')->nullable();
            $table->string('billing_address_line_1');
            $table->string('billing_address_line_2')->nullable();
            $table->string('billing_city');
            $table->string('billing_state');
            $table->string('billing_zip');
            $table->string('billing_country');
            $table->string('billing_province')->nullable();
            $table->string('billing_day_telephone');
            $table->string('billing_ext')->nullable();
            $table->string('billing_fax')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn([
                'shipping_first_name',
                'shipping_last_name',
                'shipping_middle_name',
                'shipping_title',
                'shipping_suffix',
                'shipping_company_name',
                'shipping_address_line_1',
                'shipping_address_line_2',
                'shipping_city',
                'shipping_state',
                'shipping_zip',
                'shipping_country',
                'shipping_province',
                'shipping_day_telephone',
                'shipping_ext',
                'shipping_fax',
                'billing_first_name',
                'billing_last_name',
                'billing_middle_name',
                'billing_title',
                'billing_suffix',
                'billing_company_name',
                'billing_address_line_1',
                'billing_address_line_2',
                'billing_city',
                'billing_state',
                'billing_zip',
                'billing_country',
                'billing_province',
                'billing_day_telephone',
                'billing_ext',
                'billing_fax',
            ]);
        });
    }
}
