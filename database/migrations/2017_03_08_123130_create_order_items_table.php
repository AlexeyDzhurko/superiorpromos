<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned()->nullable();
            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('set null');
            $table->integer('quantity');
            $table->double('price');
            $table->double('options_price')->default(0);
            $table->boolean('check_notes')->default(false);
            $table->boolean('auto_remind')->default(false);
            $table->boolean('not_paid')->default(false);
            $table->string('art_file_name')->nullable();
            $table->string('art_file_path')->nullable();
            $table->string('po_number')->nullable();
            $table->string('invoice_file')->nullable();
            $table->boolean('tax_exempt')->default(false);

            $table->string('estimation_zip')->nullable();
            $table->string('estimation_shipping_method')->nullable();
            $table->string('estimation_shipping_price')->nullable();

            $table->string('own_shipping_type');
            $table->string('own_account_number')->nullable();
            $table->string('own_shipping_system')->nullable();
            $table->string('shipping_method');

            $table->integer('vendor_id')->unsigned()->nullable();
            $table->foreign('vendor_id')
                ->references('id')->on('vendors')
                ->onDelete('set null');

            $table->integer('cart_id')->unsigned()->nullable();
            $table->foreign('cart_id')
                ->references('id')->on('carts')
                ->onDelete('set null');
            $table->date('received_date')->nullable();
            $table->text('imprint_comment')->nullable();

            $table->string('shipping_first_name');
            $table->string('shipping_middle_name')->nullable();
            $table->string('shipping_last_name');
            $table->string('shipping_title')->nullable();
            $table->string('shipping_suffix')->nullable();
            $table->string('shipping_company_name')->nullable();
            $table->string('shipping_address_line_1');
            $table->string('shipping_address_line_2')->nullable();
            $table->string('shipping_city');
            $table->string('shipping_state');
            $table->string('shipping_zip');
            $table->string('shipping_country');
            $table->string('shipping_province')->nullable();
            $table->string('shipping_day_telephone');
            $table->string('shipping_ext')->nullable();
            $table->string('shipping_fax')->nullable();

            $table->string('billing_first_name');
            $table->string('billing_middle_name')->nullable();
            $table->string('billing_last_name');
            $table->string('billing_title')->nullable();
            $table->string('billing_suffix')->nullable();
            $table->string('billing_company_name')->nullable();
            $table->string('billing_address_line_1');
            $table->string('billing_address_line_2')->nullable();
            $table->string('billing_city');
            $table->string('billing_state');
            $table->string('billing_zip');
            $table->string('billing_country');
            $table->string('billing_province')->nullable();
            $table->string('billing_day_telephone');
            $table->string('billing_ext')->nullable();
            $table->string('billing_fax')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
