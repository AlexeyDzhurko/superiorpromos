<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->string('session')->nullable();
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('cascade');
            $table->integer('quantity');
            $table->double('regular_price')->nullable();
            $table->double('price');
            $table->boolean('is_sale')->default(false);
            $table->boolean('later_size_breakdown')->default(false);
            $table->text('imprint_comment')->nullable();
            $table->boolean('tax_exemption')->default(false);

            $table->string('estimation_zip', 10)->nullable();
            $table->string('estimation_shipping_method')->nullable();
            $table->string('estimation_shipping_code')->nullable();
            $table->double('estimation_shipping_price')->nullable();

            $table->string('own_account_number')->nullable();
            $table->string('own_shipping_system')->nullable();
            $table->string('shipping_method');
            $table->date('received_date')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
