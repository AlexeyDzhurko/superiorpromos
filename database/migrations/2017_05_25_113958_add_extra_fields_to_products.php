<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraFieldsToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('keywords')->nullable();
            $table->string('meta_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->integer('popularity')->default(0);
            $table->integer('quantity_per_box')->nullable();
            $table->string('brand')->nullable();
            $table->string('gender')->nullable();
            $table->string('age_group')->nullable();
            $table->string('material')->nullable();
            $table->string('pattern')->nullable();
            $table->string('google_product_category')->nullable();
            $table->enum('shipping_additional_type', ['percentage', 'absolute'])->nullable();
            $table->double('shipping_additional_value')->nullalbe();
            $table->text('pricing_information')->nullable();
            $table->string('video')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn([
                'keywords',
                'meta_title',
                'meta_description',
                'popularity',
                'quantity_per_box',
                'brand',
                'gender',
                'age_group',
                'material',
                'pattern',
                'google_product_category',
                'shipping_additional_type',
                'shipping_additional_value',
                'pricing_information',
                'video',
            ]);
        });
    }
}
