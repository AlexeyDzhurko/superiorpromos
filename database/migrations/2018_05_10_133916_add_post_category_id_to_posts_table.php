<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostCategoryIdToPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::table('posts', function (Blueprint $table) {
            if (! Schema::hasColumn('posts', 'post_category_id')) {
                $table->integer('post_category_id')->unsigned();
            }
            $table->foreign('post_category_id')
                ->references('id')
                ->on('post_categories')
                ->onDelete('cascade');
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::table('posts', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            $table->dropForeign(['post_category_id']);
            $table->dropColumn('post_category_id');
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
