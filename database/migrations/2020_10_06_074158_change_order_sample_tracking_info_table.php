<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOrderSampleTrackingInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_sample_tracking_info', function (Blueprint $table) {
            $table->boolean('tracking_notify_customer')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_sample_tracking_info', function (Blueprint $table) {
            $table->dropColumn('tracking_notify_customer');
        });
    }
}
