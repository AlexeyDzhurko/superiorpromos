<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image_alt', 255)->default('Promotional Products - Promotional Items -');
            $table->string('additional_specifications', 255)->nullable();
            $table->string('dimensions', 255)->nullable();
            $table->string('items_per_box', 255);
            $table->text('pricing_information')->nullable();
            $table->text('imprint_area')->nullable();
            $table->text('description');
            $table->integer('production_time_from')->default(0);
            $table->float('box_weight')->default(0);
            $table->integer('old_product_id')->unique()->nullable();
            $table->integer('production_time_to')->default(0);
            $table->tinyInteger('on_sale')->default(0);
            $table->decimal('custom_shipping_cost', 10, 2)->nullable();
            $table->tinyInteger('custom_shipping_method')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
