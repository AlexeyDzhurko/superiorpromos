<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixCartItemRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cart_item_product_options', function (Blueprint $table) {
            $table->dropForeign('cart_item_product_options_cart_item_id_foreign');
            $table->foreign('cart_item_id')
                ->references('id')
                ->on('cart_items')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cart_item_product_options', function (Blueprint $table) {
            $table->dropForeign('cart_item_product_options_cart_item_id_foreign');
            $table->foreign('cart_item_id')
                ->references('id')
                ->on('order_items')
                ->onDelete('cascade');
        });
    }
}
