<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixSizesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('sizes_sizes_groups');
        Schema::table('sizes', function (Blueprint $table) {
            $table->integer('size_group_id')->unsigned();
            $table->foreign('size_group_id')
                ->references('id')
                ->on('size_groups')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('sizes_sizes_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id')->unsigned()->nullable();
            $table->foreign('group_id')
                ->references('id')
                ->on('size_groups')
                ->onDelete('cascade');
            $table->integer('size_id')->unsigned()->nullable();
            $table->foreign('size_id')
                ->references('id')
                ->on('sizes')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }
}
