<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackingInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracking_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tracking_shipping_date')->nullable();
            $table->string('tracking_number')->nullable();
            $table->string('tracking_shipping_company')->nullable();
            $table->text('tracking_note')->nullable();
            $table->text('tracking_url')->nullable();
            $table->integer('tracking_user_id')->unsigned()->nullable();
            $table->foreign('tracking_user_id')
                ->references('id')->on('users')
                ->onDelete('set null');
            $table->integer('order_item_id')->unsigned()->nullable();
            $table->foreign('order_item_id')
                ->references('id')->on('order_items')
                ->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracking_informations');
    }
}
