<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPromotionalBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promotional_blocks', function ($table) {
            $table->unsignedInteger('read_more_category_id')->nullable();
            $table->foreign('read_more_category_id')
                ->references('id')->on('categories')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('promotional_blocks', function (Blueprint $table) {
            $table->dropForeign('promotional_blocks_read_more_category_id_foreign');
            $table->dropColumn('read_more_category_id');
        });

        Schema::enableForeignKeyConstraints();
    }
}
