<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeForeignKeysInCartItemColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cart_item_colors', function (Blueprint $table) {
            $table->dropForeign(['color_id']);
            $table->dropForeign(['color_group_id']);

            $table->unsignedInteger('product_color_group_id')->nullable();
            $table->foreign('product_color_group_id')->references('id')->on('product_color_groups');
            $table->unsignedInteger('product_color_id')->nullable();
            $table->foreign('product_color_id')->references('id')->on('product_colors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cart_item_colors', function (Blueprint $table) {
            $table->dropForeign(['product_color_group_id']);
            $table->dropForeign(['product_color_id']);

            $table->unsignedInteger('color_group_id')->nullable();
            $table->foreign('color_group_id')->references('id')->on('product_colors');
            
            $table->unsignedInteger('color_id')->nullable();
            $table->foreign('color_id')->references('id')->on('product_colors');
        });
        Schema::drop('product_color_group_id');
        Schema::drop('product_color_id');
    }
}
