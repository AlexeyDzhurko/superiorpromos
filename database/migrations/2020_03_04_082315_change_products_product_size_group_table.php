<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeProductsProductSizeGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign(['size_group_id']);
            $table->dropColumn('size_group_id');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->unsignedInteger('size_group_id')->nullable();
        });

        Schema::table('products', function (Blueprint $table) {
            $table->foreign('size_group_id')
                ->references('id')
                ->on('size_groups')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign(['size_group_id']);
            $table->dropColumn('size_group_id');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->unsignedInteger('size_group_id')->nullable();
        });

        Schema::table('products', function (Blueprint $table) {
            $table->foreign('size_group_id')
                ->references('id')
                ->on('size_groups')
                ->onDelete('set null');
        });
    }
}
