<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImprintColorGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imprint_color_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
        });

        Schema::create('imprint_color_color_group', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('imprint_group_id')->unsigned()->nullable();
            $table->foreign('imprint_group_id')
                ->references('id')
                ->on('imprint_color_groups')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->integer('color_id')->unsigned()->nullable();
            $table->foreign('color_id')
                ->references('id')
                ->on('colors')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('imprint_color_color_group');
        Schema::drop('imprint_color_groups');
    }
}
