<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTrackingInfoToOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->date('tracking_shipping_date')->nullable();
            $table->string('tracking_number')->nullable();
            $table->string('tracking_shipping_company')->nullable();
            $table->text('tracking_note')->nullable();
            $table->integer('tracking_user_id')->unsigned()->nullable();
            $table->foreign('tracking_user_id')
                ->references('id')->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*Schema::table('order_items', function (Blueprint $table) {
            $table->dropColumn([
                'tracking_shipping_date',
                'tracking_number',
                'tracking_shipping_company',
                'tracking_note'
            ]);
        });*/
    }
}
