<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderSampleItemColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_sample_item_colors', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('order_sample_item_id')
                ->unsigned()->nullable();
            $table->foreign('order_sample_item_id')
                ->references('id')
                ->on('order_sample_items')
                ->onDelete('set null');

            $table->unsignedInteger('color_group_id')
                ->nullable();
            $table->foreign('color_group_id')
                ->references('id')
                ->on('product_color_groups');
            
            $table->unsignedInteger('color_id')
                ->nullable();
            $table->foreign('color_id')
                ->references('id')
                ->on('product_colors');

//            $table->string('color_group_name');
//            $table->string('color_name');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_sample_item_colors');
    }
}
