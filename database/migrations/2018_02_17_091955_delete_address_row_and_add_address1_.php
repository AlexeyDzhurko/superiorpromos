<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteAddressRowAndAddAddress1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_profiles', function (Blueprint $table) {
            $table->dropColumn('address');
        });

        Schema::table('payment_profiles', function (Blueprint $table) {
            $table->string('address1');
            $table->string('address2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
