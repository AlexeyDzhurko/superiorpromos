<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderSampleItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_sample_items', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('product_id')->unsigned()->nullable();
            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('set null');

            $table->integer('order_sample_id')->unsigned()->nullable();
            $table->foreign('order_sample_id')
                ->references('id')->on('order_sample')
                ->onDelete('set null');

            $table->integer('stage_id')->unsigned()->nullable();
            $table->foreign('stage_id')
                ->references('id')->on('stages')
                ->onDelete('set null');

            $table->boolean('auto_remind')->default(false);
            $table->boolean('check_notes')->default(false);
            $table->integer('quantity');
            $table->double('price');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_sample_items');
    }
}
