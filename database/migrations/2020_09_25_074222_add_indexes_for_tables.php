<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesForTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table){
            $table->index('name');
            $table->index('slug');
        });

        Schema::table('products', function (Blueprint $table){
            $table->index('name');
            $table->index('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table){
            $table->dropIndex('name');
            $table->dropIndex('slug');
        });

        Schema::table('products', function (Blueprint $table){
            $table->dropIndex('name');
            $table->dropIndex('url');
        });
    }
}
