<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductColorPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_color_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_color_id')->unsigned();
            $table->foreign('product_color_id')
                ->references('id')
                ->on('product_colors')
                ->onDelete('cascade');
            $table->integer('quantity');
            $table->decimal('setup_price')->default(0);
            $table->decimal('item_price')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_color_prices');
    }
}
