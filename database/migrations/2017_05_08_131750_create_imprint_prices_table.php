<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImprintPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imprint_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('imprint_id')->unsigned();
            $table->foreign('imprint_id')
                ->references('id')
                ->on('imprints')
                ->onDelete('cascade');
            $table->integer('vendor_id')->unsigned()->nullable();
            $table->foreign('vendor_id')
                ->references('id')
                ->on('vendors')
                ->onDelete('cascade');
            $table->integer('quantity');
            $table->decimal('setup_price')->default(0);
            $table->decimal('item_price')->default(0);
            $table->decimal('color_setup_price')->default(0);
            $table->decimal('color_item_price')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imprint_prices');
    }
}
