<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixCustomShippingMethod extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('custom_shipping_method');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->string('custom_shipping_method')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('custom_shipping_method');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->tinyInteger('custom_shipping_method')->nullable();
        });
    }
}
