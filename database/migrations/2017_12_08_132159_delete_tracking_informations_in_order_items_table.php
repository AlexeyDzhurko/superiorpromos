<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteTrackingInformationsInOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->dropColumn('tracking_shipping_date');
            $table->dropColumn('tracking_number');
            $table->dropColumn('tracking_shipping_company');
            $table->dropColumn('tracking_note');
            $table->dropColumn('tracking_url');
            $table->dropForeign(['tracking_user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
