<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductsChangeFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('sage_id', 50)->unique()->nullable();
            $table->string('url', 255)->unique()->nullable();
            $table->string('url_prefix', 255)->nullable();
            $table->dropColumn('items_per_box');
            $table->dropColumn('pricing_information');
            $table->dropColumn('image_alt');
            $table->integer('vendor_id')->unsigned();
            $table->foreign('vendor_id')
                ->references('id')
                ->on('vendors')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign('products_vendor_id_foreign');
            $table->dropColumn('sage_id');
            $table->dropColumn('url');
            $table->dropColumn('url_prefix');
            $table->dropColumn('vendor_id');
            $table->string('items_per_box', 255);
            $table->string('image_alt', 255)->default('Promotional Products - Promotional Items -');
            $table->text('pricing_information')->nullable();
        });
        Schema::enableForeignKeyConstraints();
    }
}
