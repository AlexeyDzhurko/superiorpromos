<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderSampleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_sample', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('set null');

            $table->decimal('total_price')->default(0);
            $table->string('own_shipping_type');
            $table->string('own_account_number')->nullable();
            $table->string('own_shipping_system')->nullable();
            $table->string('shipping_method');
            $table->string('quantity')->nullable();
            $table->string('attraction')->nullable();
            $table->string('event_date')->nullable();
            $table->string('frequency')->nullable();
            $table->string('sample')->nullable();
            $table->string('sample_comment')->nullable();
            $table->string('upcoming_event')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_sample');
    }
}
