<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_colors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cart_id')->unsigned();
            $table->foreign('cart_id')
                ->references('id')->on('carts')
                ->onDelete('cascade');
            $table->integer('color_group_id')->unsigned()->nullable();
            $table->foreign('color_group_id')
                ->references('id')->on('color_groups')
                ->onDelete('set null');
            $table->integer('color_id')->unsigned()->nullable();
            $table->foreign('color_id')
                ->references('id')->on('colors')
                ->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_colors');
    }
}
