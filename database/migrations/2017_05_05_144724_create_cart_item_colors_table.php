<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartItemColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_item_colors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cart_item_id')->unsigned();
            $table->integer('color_group_id')->unsigned()->nullable();
            $table->string('color_group_name');
            $table->integer('color_id')->unsigned()->nullable();
            $table->string('color_name');
            $table->timestamps();
            $table->unique(['cart_item_id','color_group_id','color_id']);
            $table->foreign('cart_item_id')
                ->references('id')
                ->on('cart_items')
                ->onDelete('cascade');
            $table->foreign('color_group_id')
                ->references('id')
                ->on('color_groups')
                ->onDelete('set null');
            $table->foreign('color_id')
                ->references('id')
                ->on('colors')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_item_colors');
    }
}
