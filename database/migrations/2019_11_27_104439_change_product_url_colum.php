<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeProductUrlColum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('url');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->string('url', 255)->nullable()->after('sage_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('url');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->string('url', 255)->unique()->nullable()->after('sage_id');
        });
    }
}
