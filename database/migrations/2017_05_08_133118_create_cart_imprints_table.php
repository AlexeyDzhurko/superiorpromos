<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartImprintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_item_imprints', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cart_item_id')->unsigned();
            $table->foreign('cart_item_id')
                ->references('id')
                ->on('cart_items')
                ->onDelete('cascade');
            $table->integer('imprint_id')->unsigned();
            $table->foreign('imprint_id')
                ->references('id')
                ->on('imprints')
                ->onDelete('cascade');
            $table->string('imprint_name');
            $table->decimal('setup_price')->default(0);
            $table->decimal('item_price')->default(0);
            $table->decimal('color_setup_price')->default(0);
            $table->decimal('color_item_price')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_item_imprints');
    }
}
