<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartItemSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_item_sizes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cart_item_size_group_id')->unsigned();
            $table->foreign('cart_item_size_group_id')
                ->references('id')
                ->on('cart_item_size_groups')
                ->onDelete('cascade');
            $table->integer('size_id')->unsigned()->nullable();
            $table->foreign('size_id')
                ->references('id')
                ->on('sizes')
                ->onDelete('set null');
            $table->string('qty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_item_sizes');
    }
}
