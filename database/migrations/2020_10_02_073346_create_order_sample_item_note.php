<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderSampleItemNote extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_sample_item_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('note');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('order_sample_item_id');
            $table->timestamps();
        });

        Schema::table('order_sample_item_notes', function (Blueprint $table) {
            $table->foreign('order_sample_item_id')->references('id')->on('order_sample_items')->onDelete('CASCADE');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_sample_item_notes');
    }
}
