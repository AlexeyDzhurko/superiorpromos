<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderSampleItemStageRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_sample_item_stage', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('order_sample_item_id')->unsigned();
            $table->foreign('order_sample_item_id')
                ->references('id')->on('order_sample_items')
                ->onDelete('cascade');

            $table->integer('stage_id')->unsigned();
            $table->foreign('stage_id')
                ->references('id')->on('stages')
                ->onDelete('cascade');

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_sample_item_stage');
    }
}
