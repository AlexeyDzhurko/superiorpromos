<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPmsColorsToColors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('colors', function (Blueprint $table) {
            $table->integer('pms_color_id')->unsigned()->nullable();
            $table->foreign('pms_color_id')
                ->references('id')
                ->on('pms_colors')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('colors', function (Blueprint $table) {
            $table->dropForeign('colors_pms_color_id_foreign');
            $table->dropColumn('pms_color_id');
        });
    }
}
