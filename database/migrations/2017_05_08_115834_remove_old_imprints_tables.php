<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveOldImprintsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('imprint_color_color_group');
        Schema::dropIfExists('products_imprint_locations');
        Schema::dropIfExists('imprint_locations');
        Schema::dropIfExists('imprint_color_groups');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('imprint_color_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
        });

        Schema::create('imprint_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('imprint_color_group_id')->unsigned()->nullable();
            $table->foreign('imprint_color_group_id')
                ->references('id')
                ->on('imprint_color_groups')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->integer('max_colors')->default(4);
            $table->integer('quantity');
            $table->decimal('location_setup_fee', 5, 2);
            $table->decimal('additional_location_running_fee', 5, 2);
            $table->decimal('additional_color_setup_fee', 5, 2);
            $table->decimal('additional_color_running_fee', 5, 2);
            $table->enum('type', [1,2,3,4,5])->default(1);

            $table->timestamps();
        });

        Schema::create('products_imprint_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned()->nullable();
            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->integer('imprint_location_id')->unsigned()->nullable();
            $table->foreign('imprint_location_id')
                ->references('id')
                ->on('imprint_locations')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('imprint_color_color_group', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('imprint_group_id')->unsigned()->nullable();
            $table->foreign('imprint_group_id')
                ->references('id')
                ->on('imprint_color_groups')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->integer('color_id')->unsigned()->nullable();
            $table->foreign('color_id')
                ->references('id')
                ->on('colors')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }
}
