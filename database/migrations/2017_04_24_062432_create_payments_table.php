<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_item_id')->unsigned()->nullable();
            $table->foreign('order_item_id')->references('id')->on('order_items')->onDelete('set null');
            $table->bigInteger('payment_profile_id')->nullable();
            $table->foreign('payment_profile_id')->references('id')->on('payment_profiles')->onDelete('set null');
            $table->string('status');
            $table->string('transaction_id')->unique()->nullable();
            $table->dateTime('shipping_date')->nullable();
            $table->string('type');
            $table->decimal('tax')->nullable();
            $table->string('shipping_company')->nullable();
            $table->integer('shipping_quantity')->nullable();
            $table->integer('over_quantity')->nullable();
            $table->decimal('over_fees')->nullable();
            $table->decimal('price');
            $table->decimal('credit_amount')->nullable();
            $table->boolean('notify_customer')->default(false);
            $table->text('notify_message')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
