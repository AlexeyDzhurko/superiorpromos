<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveOldCartsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->dropForeign('order_items_cart_id_foreign');
            $table->dropColumn('cart_id');
        });
        Schema::dropIfExists('cart_colors');
        Schema::dropIfExists('carts');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->string('session')->nullable();
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('cascade');
            $table->integer('quantity');
            $table->double('regular_price')->nullable();
            $table->double('price');
            $table->boolean('is_sale')->default(false);
            $table->boolean('later_size_breakdown')->default(false);
            $table->text('imprint_comment')->nullable();
            $table->boolean('tax_exemption')->default(false);

            $table->string('estimation_zip', 10)->nullable();
            $table->string('estimation_shipping_method')->nullable();
            $table->string('estimation_shipping_code')->nullable();
            $table->double('estimation_shipping_price')->nullable();

            $table->string('own_account_number')->nullable();
            $table->string('own_shipping_system')->nullable();
            $table->string('shipping_method');
            $table->date('received_date')->nullable();


            $table->timestamps();
        });

        Schema::create('cart_colors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cart_id')->unsigned();
            $table->foreign('cart_id')
                ->references('id')->on('carts')
                ->onDelete('cascade');
            $table->integer('color_group_id')->unsigned()->nullable();
            $table->foreign('color_group_id')
                ->references('id')->on('color_groups')
                ->onDelete('set null');
            $table->integer('color_id')->unsigned()->nullable();
            $table->foreign('color_id')
                ->references('id')->on('colors')
                ->onDelete('set null');
            $table->timestamps();
        });

        Schema::table('order_items', function (Blueprint $table) {
            $table->integer('cart_id')->unsigned()->nullable();
            $table->foreign('cart_id')
                ->references('id')->on('carts')
                ->onDelete('set null');
        });
    }
}
