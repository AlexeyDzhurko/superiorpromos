<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quantity');
            $table->integer('percent');
            $table->decimal('regular_price', 5, 2);
            $table->decimal('catalog_price', 5, 2);
            $table->decimal('net_price', 5, 2);
            $table->decimal('sale_price', 5, 2);
            $table->timestamps();
        });

        Schema::create('products_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('price_id')->unsigned();
            $table->foreign('price_id')
                ->references('id')
                ->on('prices')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });

        Schema::table('color_groups', function (Blueprint $table) {
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('products_prices');
        Schema::dropIfExists('prices');

        /*Schema::table('color_groups', function (Blueprint $table) {
            $table->dropForeign('color_groups_product_id_foreign');
            $table->dropColumn('product_id');
        });*/
        Schema::enableForeignKeyConstraints();
    }
}
