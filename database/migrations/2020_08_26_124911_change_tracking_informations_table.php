<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTrackingInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tracking_informations', function (Blueprint $table) {
            $table->boolean('tracking_notify_customer')->default(false);
            $table->boolean('tracking_request_rating')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tracking_informations', function (Blueprint $table) {
            $table->dropColumn('tracking_notify_customer');
            $table->dropColumn('tracking_request_rating');
        });
    }
}
