<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeProductsProductIconTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign(['product_icon_id']);
            $table->dropColumn('product_icon_id');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->unsignedInteger('product_icon_id')->nullable();
        });

        Schema::table('products', function (Blueprint $table) {
            $table->foreign('product_icon_id')
                ->references('id')
                ->on('product_icons')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign(['product_icon_id']);
            $table->dropColumn('product_icon_id');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->unsignedInteger('product_icon_id')->nullable();
        });

        Schema::table('products', function (Blueprint $table) {
            $table->foreign('product_icon_id')
                ->references('id')
                ->on('product_icons')
                ->onDelete('cascade');
        });
    }
}
