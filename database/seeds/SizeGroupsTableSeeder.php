<?php

use Illuminate\Database\Seeder;

class SizeGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('size_groups')->delete();

        \DB::table('size_groups')->insert([
            0 =>
                [
                    'id' => 1,
                    'name' => 'Apparel',
                    'created_at' => '2016-10-21 22:31:20',
                    'updated_at' => '2016-10-21 22:31:20',
                ]
        ]);
    }
}
