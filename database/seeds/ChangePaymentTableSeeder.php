<?php

use Illuminate\Database\Seeder;

class ChangePaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payments')->where('status', '=', 'Paid')->update(['status' => 'paid']);
        DB::table('payments')->where('status', '=', 'Processed')->update(['status' => 'processed']);
        DB::table('payments')->where('status', 'LIKE', 'Not Paid%')->update(['status' => 'not_paid']);

        DB::table('payments')->where('type', '=', 'Credit')->update(['type' => 'credit']);
        DB::table('payments')->where('type', '=', 'Initial Payment')->update(['type' => 'initial']);
        DB::table('payments')->where('type', '=', 'Billing (Shipping/Other)')->update(['type' => 'other']);
        DB::table('payments')->where('type', '=', 'Billing (Shipping/Overrun)')->update(['type' => 'overrun']);
    }
}
