<?php

use Illuminate\Database\Seeder;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('states')->delete();

        \DB::table('states')->insert([
            ['name' => 'Armed Forces Americas (except Canada)', 'abbr' => 'AA'],
            ['name' => 'Alberta', 'abbr' => 'AB'],
            ['name' => 'Armed Forces Europe, Canada, Middle East, Africa', 'abbr' => 'AE'],
            ['name' => 'Alaska', 'abbr' => 'AK'],
            ['name' => 'Alabama', 'abbr' => 'AL'],
            ['name' => 'Armed Forces Pacific', 'abbr' => 'AP'],
            ['name' => 'Arkansas', 'abbr' => 'AR'],
            ['name' => 'American Samoa', 'abbr' => 'AS'],
            ['name' => 'Arizona', 'abbr' => 'AZ'],
            ['name' => 'British Columbia', 'abbr' => 'BC'],
            ['name' => 'California', 'abbr' => 'CA'],
            ['name' => 'Colorado', 'abbr' => 'CO'],
            ['name' => 'Connecticut', 'abbr' => 'CT'],
            ['name' => 'District Of Columbia', 'abbr' => 'DC'],
            ['name' => 'Delaware', 'abbr' => 'DE'],
            ['name' => 'Florida', 'abbr' => 'FL'],
            ['name' => 'Fdr. States Of Micronesia', 'abbr' => 'FM'],
            ['name' => 'Georgia', 'abbr' => 'GA'],
            ['name' => 'Guam', 'abbr' => 'GU'],
            ['name' => 'Hawaii', 'abbr' => 'HI'],
            ['name' => 'Iowa', 'abbr' => 'IA'],
            ['name' => 'Idaho', 'abbr' => 'ID'],
            ['name' => 'Illinois', 'abbr' => 'IL'],
            ['name' => 'Indiana', 'abbr' => 'IN'],
            ['name' => 'Kansas', 'abbr' => 'KS'],
            ['name' => 'Kentucky', 'abbr' => 'KY'],
            ['name' => 'Louisiana', 'abbr' => 'LA'],
            ['name' => 'Massachusetts', 'abbr' => 'MA'],
            ['name' => 'Manitoba', 'abbr' => 'MB'],
            ['name' => 'Maryland', 'abbr' => 'MD'],
            ['name' => 'Maine', 'abbr' => 'ME'],
            ['name' => 'Marshall Islands', 'abbr' => 'MH'],
            ['name' => 'Michigan', 'abbr' => 'MI'],
            ['name' => 'Minnesota', 'abbr' => 'MN'],
            ['name' => 'Missouri', 'abbr' => 'MO'],
            ['name' => 'Northern Mariana Islands', 'abbr' => 'MP'],
            ['name' => 'Mississippi', 'abbr' => 'MS'],
            ['name' => 'Montana', 'abbr' => 'MT'],
            ['name' => 'New Brunswick', 'abbr' => 'NB'],
            ['name' => 'North Carolina', 'abbr' => 'NC'],
            ['name' => 'North Dakota', 'abbr' => 'ND'],
            ['name' => 'Nebraska', 'abbr' => 'NE'],
            ['name' => 'New Hampshire', 'abbr' => 'NH'],
            ['name' => 'New Jersey', 'abbr' => 'NJ'],
            ['name' => 'Newfoundland & Labrador', 'abbr' => 'NL'],
            ['name' => 'New Mexico', 'abbr' => 'NM'],
            ['name' => 'Nova Scotia', 'abbr' => 'NS'],
            ['name' => 'Northwest Territories', 'abbr' => 'NT'],
            ['name' => 'Nunavut', 'abbr' => 'NU'],
            ['name' => 'Nevada', 'abbr' => 'NV'],
            ['name' => 'New York', 'abbr' => 'NY'],
            ['name' => 'Ohio', 'abbr' => 'OH'],
            ['name' => 'Oklahoma', 'abbr' => 'OK'],
            ['name' => 'Ontario', 'abbr' => 'ON'],
            ['name' => 'Oregon', 'abbr' => 'OR'],
            ['name' => 'Pennsylvania', 'abbr' => 'PA'],
            ['name' => 'Prince Edward Island', 'abbr' => 'PE'],
            ['name' => 'Puerto Rico', 'abbr' => 'PR'],
            ['name' => 'Palau', 'abbr' => 'PW'],
            ['name' => 'Quebec', 'abbr' => 'QC'],
            ['name' => 'Rhode Island', 'abbr' => 'RI'],
            ['name' => 'South Carolina', 'abbr' => 'SC'],
            ['name' => 'South Dakota', 'abbr' => 'SD'],
            ['name' => 'Saskatchewan', 'abbr' => 'SK'],
            ['name' => 'Tennessee', 'abbr' => 'TN'],
            ['name' => 'Texas', 'abbr' => 'TX'],
            ['name' => 'Utah', 'abbr' => 'UT'],
            ['name' => 'Virginia', 'abbr' => 'VA'],
            ['name' => 'Virgin Islands', 'abbr' => 'VI'],
            ['name' => 'Vermont', 'abbr' => 'VT'],
            ['name' => 'Washington', 'abbr' => 'WA'],
            ['name' => 'Wisconsin', 'abbr' => 'WI'],
            ['name' => 'West Virginia', 'abbr' => 'WV'],
            ['name' => 'Wyoming', 'abbr' => 'WY'],
            ['name' => 'Yukon Territory', 'abbr' => 'YT']
        ]);
    }
}
