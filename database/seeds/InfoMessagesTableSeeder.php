<?php

use Illuminate\Database\Seeder;

class InfoMessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('info_messages')->delete();

        \DB::table('info_messages')->insert([
            [
                'key' => 'cart0',
                'display_name' => 'Empty product amount',
                'value' => 'Please enter product amount.',
                'group' => 'Shopping Cart',
            ],
            [
                'key' => 'cart1',
                'display_name' => 'Invalid product amount',
                'value' => 'Please enter correct product amount, minimum allowed is %s',
                'group' => 'Shopping Cart',
            ],
            [
                'key' => 'cart2',
                'display_name' => 'Invalid colors',
                'value' => 'Please choose correct numbers of colors',
                'group' => 'Shopping Cart',
            ],
            [
                'key' => 'cart3',
                'display_name' => 'Invalid In-Hand Date',
                'value' => 'Please select an In-Hand Date for your order or choose ASAP for Regular Production Time',
                'group' => 'Shopping Cart',
            ],
            [
                'key' => 'login0',
                'display_name' => 'Invalid credentials',
                'value' => 'Incorrect login or password. Please, try again.',
                'group' => 'Customer login',
            ],
            [
                'key' => 'E00027',
                'display_name' => 'E00027',
                'value' => '<p>An approval was not returned for the transaction.</p>
<p>Please contact your bank issuer for details or our customer service team at 888-577-6667</p>
<p>Possible reasons your transaction was declined</p>
<ul>
<li>Proper funds are not available for this transaction</li>
<li>Spending limit restriction</li>
<li>Account closed or not active</li>
<li>Account on hold with the card issuing bank</li>
</ul>',
                'group' => 'CIM',
            ],
            [
                'key' => 'E00013',
                'display_name' => 'E00013',
                'value' => 'One of the field values is not valid.',
                'group' => 'CIM',
            ],
            [
                'key' => 'E00039',
                'display_name' => 'E00039',
                'value' => 'A duplicate customer profile, or customer payment. Please contact Customer Service at 888-577-6667 to complete the transaction.',
                'group' => 'CIM',
            ],
            [
                'key' => 'E00042',
                'display_name' => 'E00042',
                'value' => 'The maximum number of payment profiles for the customer profile has been reached.',
                'group' => 'CIM',
            ],

        ]);
    }
}
