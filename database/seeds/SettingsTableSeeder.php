<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('settings')->delete();
        
        \DB::table('settings')->insert([
            [
                'key' => 'customer_from_name',
                'display_name' => 'Customer\'s messages coming from name',
                'value' => 'Superior Promos',
                'type' => 'text',
                'group' => 'Mail settings',
            ],
            [
                'key' => 'customer_from_email',
                'display_name' => 'Customer\'s messages coming from email',
                'value' => 'info@superiorpromos.com',
                'type' => 'text',
                'group' => 'Mail settings',
            ],
            [
                'key' => 'admin_email',
                'display_name' => 'Site administrator E-Mail',
                'value' => 'boris@superiorpromos.com, eddie@superiorpromos.com',
                'type' => 'text',
                'group' => 'Mail settings',
            ],
            [
                'key' => 'contact_us_email',
                'display_name' => 'Contact Us E-Mail',
                'value' => 'info@superiorpromos.com, boris@superiorpromos.com',
                'type' => 'text',
                'group' => 'Mail settings',
            ],
            [
                'key' => 'order_sample_email',
                'display_name' => 'Order a Sample E-Mail',
                'value' => 'boris@superiorpromos.com',
                'type' => 'text',
                'group' => 'Mail settings',
            ],
            [
                'key' => 'quick_quote_email',
                'display_name' => 'Quick Quote E-Mail',
                'value' => 'boris@superiorpromos.com, eddie@superiorpromos.com',
                'type' => 'text',
                'group' => 'Mail settings',
            ],
            [
                'key' => 'new_reviews_email',
                'display_name' => 'New Reviews E-Mail',
                'value' => 'boris@superiorpromos.com, eddie@superiorpromos.com',
                'type' => 'text',
                'group' => 'Mail settings',
            ],
            [
                'key' => 'new_order_email',
                'display_name' => 'New Order E-Mail',
                'value' => 'boris@superiorpromos.com,eddie@superiorpromos.com',
                'type' => 'text',
                'group' => 'Mail settings',
            ],
            [
                'key' => 'order_note_email',
                'display_name' => 'Order Notes E-Mail',
                'value' => 'boris@superiorpromos.com, art@superiorpromos.com',
                'type' => 'text',
                'group' => 'Mail settings',
            ],
            [
                'key' => 'art_proof_email',
                'display_name' => 'Art Proofs E-Mail',
                'value' => 'art@superiorpromos.com',
                'type' => 'text',
                'group' => 'Mail settings',
            ],
            [
                'key' => 'customer_alert_email',
                'display_name' => 'User Info Change E-Mail',
                'value' => 'info@superiorpromos.com, billing@superiorpromos.com',
                'type' => 'text',
                'group' => 'Mail settings',
            ],
            [
                'key' => 'reminder_interval',
                'display_name' => 'Order reminder interval (days)',
                'value' => '45',
                'type' => 'text',
                'group' => 'Mail settings',
            ],
            [
                'key' => 'sender_company',
                'display_name' => 'Sender company',
                'value' => 'Superior Promos',
                'type' => 'text',
                'group' => 'Shipping settings',
            ],
            [
                'key' => 'sender_address_1',
                'display_name' => 'Address 1',
                'value' => 'PO BOX 297189',
                'type' => 'text',
                'group' => 'Shipping settings',
            ],
            [
                'key' => 'sender_address_2',
                'display_name' => 'Address 2',
                'value' => '',
                'type' => 'text',
                'group' => 'Shipping settings',
            ],
            [
                'key' => 'sender_city',
                'display_name' => 'City',
                'value' => 'Brooklyn',
                'type' => 'text',
                'group' => 'Shipping settings',
            ],
            [
                'key' => 'sender_state',
                'display_name' => 'State',
                'value' => 'NY',
                'type' => 'text',
                'group' => 'Shipping settings',
            ],
            [
                'key' => 'sender_zip',
                'display_name' => 'ZIP',
                'value' => '11229',
                'type' => 'text',
                'group' => 'Shipping settings',
            ],
            [
                'key' => 'sender_country',
                'display_name' => 'Country',
                'value' => 'US',
                'type' => 'text',
                'group' => 'Shipping settings',
            ],
            [
                'key' => 'sender_phone',
                'display_name' => 'Phone',
                'value' => '1-888-577-6667',
                'type' => 'text',
                'group' => 'Shipping settings',
            ],
            [
                'key' => 'sender_email',
                'display_name' => 'Email',
                'value' => 'info@superiorpromos.com',
                'type' => 'text',
                'group' => 'Shipping settings',
            ],
            [
                'key' => 'shipping_additional',
                'display_name' => 'Shipping cost addition ($)',
                'value' => '2.50',
                'type' => 'text',
                'group' => 'Shipping settings',
            ],
            [
                'key' => 'fedex_account',
                'display_name' => 'FedEx account number',
                'value' => '510087100',
                'type' => 'text',
                'group' => 'Shipping settings',
            ],
            [
                'key' => 'fedex_trxid',
                'display_name' => 'FedEx transaction #',
                'value' => '1249409',
                'type' => 'text',
                'group' => 'Shipping settings',
            ],
            [
                'key' => 'UPS license',
                'display_name' => 'UPS license number',
                'value' => '9C34F751FE4E893C',
                'type' => 'text',
                'group' => 'Shipping settings',
            ],
            [
                'key' => 'ups_user_id',
                'display_name' => 'UPS user ID',
                'value' => 'superiorpromos',
                'type' => 'text',
                'group' => 'Shipping settings',
            ],
            [
                'key' => 'ups_password',
                'display_name' => 'UPS password',
                'value' => 'promos0308',
                'type' => 'text',
                'group' => 'Shipping settings',
            ],
        ]);
        
        
    }
}
