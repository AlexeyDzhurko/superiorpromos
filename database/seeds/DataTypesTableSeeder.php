<?php

use Illuminate\Database\Seeder;

class DataTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('data_types')->delete();

        \DB::table('data_types')->insert([0 => [
            'id' => 1,
            'name' => 'menus',
            'slug' => 'menus',
            'display_name_singular' => 'Menu',
            'display_name_plural' => 'Menus',
            'icon' => 'voyager-list',
            'model_name' => 'TCG\\Voyager\\Models\\Menu',
            'description' => '',
            'created_at' => NULL,
            'updated_at' => '2016-06-29 00:09:35',
        ]]);
    }
}
