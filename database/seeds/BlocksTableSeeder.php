<?php

use Illuminate\Database\Seeder;

class BlocksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('blocks')->delete();

        \DB::table('blocks')->insert([
            [
                'key' => 'contact_us_left_block',
                'display_name' => 'Contact Us Left block',
            ],
            [
                'key' => 'contact_us_middle_block',
                'display_name' => 'Contact Us Middle block',
            ],
            [
                'key' => 'contact_us_right_block',
                'display_name' => 'Contact Us Right block',
            ],
            [
                'key' => 'footer_seo_text',
                'display_name' => 'Footer SEO text',
            ],
            [
                'key' => 'header_text',
                'display_name' => 'Idea center (header)',
            ],
            [
                'key' => 'footer_text',
                'display_name' => 'Idea center (footer)',
            ],
            [
                'key' => 'home_slogan',
                'display_name' => 'First page header slogan',
            ],
            [
                'key' => 'home_h1',
                'display_name' => 'First page H1',
            ],
            [
                'key' => 'coupon_popup',
                'display_name' => 'Coupon Popup',
            ],
        ]);
    }
}
