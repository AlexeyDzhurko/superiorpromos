<?php

use Illuminate\Database\Seeder;

class StageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stages')->delete();

        DB::table('stages')->insert([
            [
                'name' => 'New Order Received - Processing',
                'description' => 'No Status (new orders)',
                'position' => 1,
            ],
            [
                'name' => 'Art Department',
                'description' => 'Art Department',
                'position' => 2,
            ],
            [
                'name' => 'Art Department - Rush',
                'description' => 'Art Department - Creating Proof Ext. 8002',
                'position' => 3,
            ],
            [
                'name' => 'Shipped - Collecting Freight Charges',
                'description' => 'Shipped',
                'position' => 4,
            ],
            [
                'name' => 'Complete',
                'description' => 'Your order has been complete.',
                'position' => 5,
            ],
        ]);
    }
}
