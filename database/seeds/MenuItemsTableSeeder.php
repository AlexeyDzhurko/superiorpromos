<?php

use Illuminate\Database\Seeder;

class MenuItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $prefix = config('voyager.routes.prefix', 'admin');

        \DB::table('menu_items')->delete();
        
        \DB::table('menu_items')->insert([
            0 => 
            [
                'id' => 1,
                'menu_id' => 1,
                'title' => 'Dashboard',
                'url' => "/{$prefix}",
                'target' => '_self',
                'icon_class' => 'voyager-boat',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 1,
                'created_at' => '2016-05-31 22:17:38',
                'updated_at' => '2016-06-01 20:24:01',
            ],
            1 => [
                'id' => 2,
                'menu_id' => 1,
                'title' => 'CMS',
                'url' => "#",
                'target' => '_self',
                'icon_class' => '',
                'color' => NULL,
                'parent_id' => NULL,
                'order' => 2,
                'created_at' => '2016-05-31 22:17:38',
                'updated_at' => '2016-06-01 20:24:01',
            ],
            2 => [
                'id' => 3,
                'menu_id' => 1,
                'title' => 'Email Templates',
                'url' => "/admin/email-templates",
                'target' => '_self',
                'icon_class' => '',
                'color' => NULL,
                'parent_id' => 2,
                'order' => 1,
                'created_at' => '2016-05-31 22:17:38',
                'updated_at' => '2016-06-01 20:24:01',
            ],
        ]);
        
        
    }
}
