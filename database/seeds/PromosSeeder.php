<?php

use Illuminate\Database\Seeder;

class PromosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('promos')->delete();

        \DB::table('promos')->insert([
            0 =>
                [
                    'title' => 'Left block',
                    'url' => 'test',
                    'image_src' => 'test',
                    'position' => 'left',
                ],
            1 =>
                [
                    'title' => 'Left block',
                    'url' => 'test',
                    'image_src' => 'test',
                    'position' => 'left',
                ],
            2 =>
                [
                    'title' => 'Left block',
                    'url' => 'test',
                    'image_src' => 'test',
                    'position' => 'left',
                ],
            3 =>
                [
                    'title' => 'Left block',
                    'url' => 'test',
                    'image_src' => 'test',
                    'position' => 'left',
                ],
            4 =>
                [
                    'title' => 'Center block',
                    'url' => 'test',
                    'image_src' => 'test',
                    'position' => 'center',
                ],
            5 =>
                [
                    'title' => 'Right block',
                    'url' => 'test',
                    'image_src' => 'test',
                    'position' => 'right',
                ],
            6 =>
                [
                    'title' => 'Right block',
                    'url' => 'test',
                    'image_src' => 'test',
                    'position' => 'right',
                ],
            7 =>
                [
                    'title' => 'Right block',
                    'url' => 'test',
                    'image_src' => 'test',
                    'position' => 'right',
                ],
            8 =>
                [
                    'title' => 'Right block',
                    'url' => 'test',
                    'image_src' => 'test',
                    'position' => 'right',
                ],
        ]);
    }
}
