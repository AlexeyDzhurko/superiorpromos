<?php

use Illuminate\Database\Seeder;

class PromotionalBlockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('promotional_blocks')->delete();

        \DB::table('promotional_blocks')->insert([
            0 =>
                [
                    'id' => 1,
                    'title' => 'Top block',
                    'slug' => '/top-block',
                ],
            1 =>
                [
                    'id' => 2,
                    'title' => 'Middle block',
                    'slug' => '/middle-block',
                ],
            2 =>
                [
                    'id' => 3,
                    'title' => 'Bottom block',
                    'slug' => '/bottom-block',
                ],
        ]);
    }
}
