<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('countries')->delete();

        \DB::table('countries')->insert([
            ["name" =>"United States"],
            ["name" =>"Canada"],
            ["name" =>"Afghanistan"],
            ["name" =>"Albania"],
            ["name" =>"Algeria"],
            ["name" =>"American Samoa"],
            ["name" =>"Andorra"],
            ["name" =>"Angola"],
            ["name" =>"Anguilla"],
            ["name" =>"Antarctica"],
            ["name" =>"Antigua and Barbuda"],
            ["name" =>"Argentina"],
            ["name" =>"Armenia"],
            ["name" =>"Aruba"],
            ["name" =>"Australia"],
            ["name" =>"Austria"],
            ["name" =>"Azerbaijan"],
            ["name" =>"Bahamas"],
            ["name" =>"Bahamas"],
            ["name" =>"Bahrain"],
            ["name" =>"Bangladesh"],
            ["name" =>"Barbados"],
            ["name" =>"Belarus"],
            ["name" =>"Belgium"],
            ["name" =>"Belize"],
            ["name" =>"Benin"],
            ["name" =>"Bermuda"],
            ["name" =>"Bhutan"],
            ["name" =>"Bolivia"],
            ["name" =>"Bosnia and Herzegovina"],
            ["name" =>"Botswana"],
            ["name" =>"Bouvet Island"],
            ["name" =>"Brazil"],
            ["name" =>"British Indian Ocean Territory"],
            ["name" =>"Brunei Darussalam"],
            ["name" =>"Bulgaria"],
            ["name" =>"Burkina Faso"],
            ["name" =>"Burundi"],
            ["name" =>"Cambodia"],
            ["name" =>"Cameroon"],
            ["name" =>"Canada"],
            ["name" =>"Cape Verde"],
            ["name" =>"Cayman Islands"],
            ["name" =>"Central African Republic"],
            ["name" =>"Chad"],
            ["name" =>"Chile"],
            ["name" =>"China"],
            ["name" =>"Christmas Island"],
            ["name" =>"Cocos (Keeling) Islands"],
            ["name" =>"Colombia"],
            ["name" =>"Comoros"],
            ["name" =>"Congo"],
            ["name" =>"Congo, Democratic Republic"],
            ["name" =>"Cook Islands"],
            ["name" =>"Costa Rica"],
            ["name" =>"Croatia"],
            ["name" =>"Cuba"],
            ["name" =>"Cyprus"],
            ["name" =>"Czech Republic"],
            ["name" =>"Czechoslovakia (former)"],
            ["name" =>"Denmark"],
            ["name" =>"Djibouti"],
            ["name" =>"Dominica"],
            ["name" =>"Dominican Republic"],
            ["name" =>"East Timor"],
            ["name" =>"Ecuador"],
            ["name" =>"Egypt"],
            ["name" =>"El Salvador"],
            ["name" =>"Equatorial Guinea"],
            ["name" =>"Eritrea"],
            ["name" =>"Estonia"],
            ["name" =>"Ethiopia"],
            ["name" =>"F.Y.R.O.M. (Macedonia)"],
            ["name" =>"Falkland Islands (Malvinas)"],
            ["name" =>"Faroe Islands"],
            ["name" =>"Fiji"],
            ["name" =>"Finland"],
            ["name" =>"France"],
            ["name" =>"France, Metropolitan"],
            ["name" =>"French Guiana"],
            ["name" =>"French Polynesia"],
            ["name" =>"French Southern Territories"],
            ["name" =>"Gabon"],
            ["name" =>"Gambia"],
            ["name" =>"Georgia"],
            ["name" =>"Germany"],
            ["name" =>"Ghana"],
            ["name" =>"Gibraltar"],
            ["name" =>"Greece"],
            ["name" =>"Greenland"],
            ["name" =>"Grenada"],
            ["name" =>"Guadeloupe"],
            ["name" =>"Guam"],
            ["name" =>"Guatemala"],
            ["name" =>"Guinea"],
            ["name" =>"Guinea"],
            ["name" =>"Guyana"],
            ["name" =>"Haiti"],
            ["name" =>"Heard and McDonald Islands"],
            ["name" =>"Honduras"],
            ["name" =>"Hong Kong"],
            ["name" =>"Hungary"],
            ["name" =>"Iceland"],
            ["name" =>"India"],
            ["name" =>"Indonesia"],
            ["name" =>"Iran"],
            ["name" =>"Iraq"],
            ["name" =>"Ireland"],
            ["name" =>"Israel"],
            ["name" =>"Italy"],
            ["name" =>"Jamaica"],
            ["name" =>"Japan"],
            ["name" =>"Jordan"],
            ["name" =>"Kazakhstan"],
            ["name" =>"Kenya"],
            ["name" =>"Kiribati"],
            ["name" =>"Korea (North)"],
            ["name" =>"Korea (South)"],
            ["name" =>"Kuwait"],
            ["name" =>"Kyrgyzstan"],
            ["name" =>"Laos"],
            ["name" =>"Latvia"],
            ["name" =>"Lebanon"],
            ["name" =>"Lesotho"],
            ["name" =>"Liberia"],
            ["name" =>"Libya"],
            ["name" =>"Liechtenstein"],
            ["name" =>"Lithuania"],
            ["name" =>"Luxembourg"],
            ["name" =>"Macau"],
            ["name" =>"Madagascar"],
            ["name" =>"Malawi"],
            ["name" =>"Malaysia"],
            ["name" =>"Maldives"],
            ["name" =>"Mali"],
            ["name" =>"Malta"],
            ["name" =>"Marshall Islands"],
            ["name" =>"Martinique"],
            ["name" =>"Mauritania"],
            ["name" =>"Mauritius"],
            ["name" =>"Mayotte"],
            ["name" =>"Mexico"],
            ["name" =>"Micronesia"],
            ["name" =>"Moldova"],
            ["name" =>"Monaco"],
            ["name" =>"Mongolia"],
            ["name" =>"Montserrat"],
            ["name" =>"Morocco"],
            ["name" =>"Mozambique"],
            ["name" =>"Myanmar"],
            ["name" =>"Namibia"],
            ["name" =>"Nauru"],
            ["name" =>"Nepal"],
            ["name" =>"Netherlands"],
            ["name" =>"Netherlands Antilles"],
            ["name" =>"Neutral Zone"],
            ["name" =>"New Caledonia"],
            ["name" =>"New Zealand"],
            ["name" =>"Nicaragua"],
            ["name" =>"Niger"],
            ["name" =>"Nigeria"],
            ["name" =>"Niue"],
            ["name" =>"Norfolk Island"],
            ["name" =>"Northern Mariana Islands"],
            ["name" =>"Norway"],
            ["name" =>"Oman"],
            ["name" =>"Pakistan"],
            ["name" =>"Palau"],
            ["name" =>"Panama"],
            ["name" =>"Papua New Guinea"],
            ["name" =>"Paraguay"],
            ["name" =>"Peru"],
            ["name" =>"Philippines"],
            ["name" =>"Pitcairn"],
            ["name" =>"Poland"],
            ["name" =>"Portugal"],
            ["name" =>"Puerto Rico"],
            ["name" =>"Qatar"],
            ["name" =>"Reunion"],
            ["name" =>"Romania"],
            ["name" =>"Russian Federation"],
            ["name" =>"Rwanda"],
            ["name" =>"S. Georgia and S. Sandwich Isls."],
            ["name" =>"Saint Kitts and Nevis"],
            ["name" =>"Saint Lucia"],
            ["name" =>"Saint Vincent &amp; the Grenadines"],
            ["name" =>"Samoa"],
            ["name" =>"San Marino"],
            ["name" =>"Sao Tome and Principe"],
            ["name" =>"Saudi Arabia"],
            ["name" =>"Senegal"],
            ["name" =>"Seychelles"],
            ["name" =>"Sierra Leone"],
            ["name" =>"Singapore"],
            ["name" =>"Slovak Republic"],
            ["name" =>"Slovenia"],
            ["name" =>"Solomon Islands"],
            ["name" =>"Somalia"],
            ["name" =>"South Africa"],
            ["name" =>"Spain"],
            ["name" =>"Sri Lanka"],
            ["name" =>"St. Helena"],
            ["name" =>"St. Pierre and Miquelon"],
            ["name" =>"Sudan"],
            ["name" =>"Suriname"],
            ["name" =>"Svalbard &amp; Jan Mayen Islands"],
            ["name" =>"Swaziland"],
            ["name" =>"Sweden"],
            ["name" =>"Switzerland"],
            ["name" =>"Syria"],
            ["name" =>"Taiwan"],
            ["name" =>"Tajikistan"],
            ["name" =>"Tanzania"],
            ["name" =>"Thailand"],
            ["name" =>"Togo"],
            ["name" =>"Tokelau"],
            ["name" =>"Tonga"],
            ["name" =>"Trinidad and Tobago"],
            ["name" =>"Tunisia"],
            ["name" =>"Turkey"],
            ["name" =>"Turkmenistan"],
            ["name" =>"Turks and Caicos Islands"],
            ["name" =>"Tuvalu"],
            ["name" =>"Uganda"],
            ["name" =>"Ukraine"],
            ["name" =>"United Arab Emirates"],
            ["name" =>"United Kingdom (Great Britain)"],
            ["name" =>"United States"],
            ["name" =>"Uruguay"],
            ["name" =>"US Minor Outlying Islands"],
            ["name" =>"USSR (former)"],
            ["name" =>"Uzbekistan"],
            ["name" =>"Vanuatu"],
            ["name" =>"Vatican City State (Holy See)"],
            ["name" =>"Venezuela"],
            ["name" =>"Viet Nam"],
            ["name" =>"Virgin Islands (British)"],
            ["name" =>"Virgin Islands (U.S.)"],
            ["name" =>"Wallis and Futuna Islands"],
            ["name" =>"Western Sahara"],
            ["name" =>"Yemen"],
            ["name" =>"Yugoslavia"],
            ["name" =>"Zaire"],
            ["name" =>"Zambia"],
            ["name" =>"Zimbabwe"],
        ]);
    }
}
