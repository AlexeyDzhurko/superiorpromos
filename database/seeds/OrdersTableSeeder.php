<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Order::class, 20)->create()->each(function($order) { /** @var \App\Models\Order $order */
            $order->orderItems()->saveMany(factory(App\Models\OrderItem::class, 5)->make());
        });
    }
}
