<?php

use Illuminate\Database\Seeder;

class VendorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('vendors')->delete();

        \DB::table('vendors')->insert([
            0 =>
                [
                    'name'             => 'Empty Vendor',
                    'phone'            => null,
                    'fax'              => null,
                    'email'            => null,
                    'state'            => null,
                    'city'             => null,
                    'country'          => 'US',
                    'zip_code'         => '77777',
                    'address_1'        => 'no data',
                    'address_2'        => null,
                    'supplier_id'      => null,
                    'site'             => null,
                    'payment_template' => 3,
                    'id'               => 1,
                ],
        ]);
    }
}
