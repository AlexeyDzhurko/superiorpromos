<?php

use Illuminate\Database\Seeder;

class ImprintColorGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('imprint_color_color_group')->delete();
        \DB::table('imprint_color_groups')->delete();

        \DB::table('imprint_color_groups')->insert([
            0 =>
                [
                    'id' => 1,
                    'name' => 'Standard Colors',
                ],
            1 =>
                [
                    'id' => 2,
                    'name' => 'Air-tex',
                ],
        ]);

        \DB::table('imprint_color_color_group')->insert([
            0 =>
                [
                    'id' => 1,
                    'imprint_group_id' => 1,
                    'color_id' => 1,
                ],
            1 =>
                [
                    'id' => 2,
                    'imprint_group_id' => 1,
                    'color_id' => 2,
                ],
            2 =>
                [
                    'id' => 3,
                    'imprint_group_id' => 2,
                    'color_id' => 1,
                ],
            3 =>
                [
                    'id' => 4,
                    'imprint_group_id' => 2,
                    'color_id' => 2,
                ],
        ]);
    }
}
