<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();
        \DB::table('user_roles')->delete();
        
        \DB::table('users')->insert([
            [
                'id' => 1,
                'name' => 'Admin',
                'email' => '1@admin.com',
                'password' => bcrypt('1111'),
                'remember_token' => '',
                'created_at' => '2016-01-28 11:20:57',
                'updated_at' => '2016-10-25 14:32:23',
                'avatar' => 'users/default.png',
                'authorize_net_id' => '1807611011',
                'contact_telephone' => '(050) 555-11-22',
                'ext' => '123',
                'fax' => '(050) 555-22-22'
            ],[
                'id' => 2,
                'name' => 'Boris',
                'email' => 'art@superiorpromos.com',
                'password' => bcrypt('boris1234'),
                'remember_token' => '',
                'created_at' => '2016-01-28 11:20:57',
                'updated_at' => '2016-10-25 14:32:23',
                'avatar' => 'users/default.png',
                'authorize_net_id' => '1807611011',
                'contact_telephone' => '(050) 555-11-22',
                'ext' => '123',
                'fax' => '(050) 555-22-22'
            ],[
                'id' => 3,
                'name' => 'Eddie',
                'email' => 'info+1@superiorpromos.com',
                'password' => bcrypt('eddie1234'),
                'remember_token' => '',
                'created_at' => '2016-01-28 11:20:57',
                'updated_at' => '2016-10-25 14:32:23',
                'avatar' => 'users/default.png',
                'authorize_net_id' => '1807611011',
                'contact_telephone' => '(050) 555-11-22',
                'ext' => '123',
                'fax' => '(050) 555-22-22'
            ],[
                'id' => 4,
                'name' => 'David',
                'email' => 'david+1@vectorart.co',
                'password' => bcrypt('davidvector'),
                'remember_token' => '',
                'created_at' => '2016-01-28 11:20:57',
                'updated_at' => '2016-10-25 14:32:23',
                'avatar' => 'users/default.png',
                'authorize_net_id' => '1807611011',
                'contact_telephone' => '(050) 555-11-22',
                'ext' => '123',
                'fax' => '(050) 555-22-22'
            ],[
                'id' => 5,
                'name' => 'Alex',
                'email' => 'alex+1@superiorpromos.com',
                'password' => bcrypt('davidvector'),
                'remember_token' => '',
                'created_at' => '2016-01-28 11:20:57',
                'updated_at' => '2016-10-25 14:32:23',
                'avatar' => 'users/default.png',
                'authorize_net_id' => '1807611011',
                'contact_telephone' => '(050) 555-11-22',
                'ext' => '123',
                'fax' => '(050) 555-22-22'
            ],
        ]);

        \DB::table('user_roles')->insert([
            [
                'role_id' => 1,
                'user_id' => 1
            ],
            [
                'role_id' => 2,
                'user_id' => 1
            ],
            [
                'role_id' => 1,
                'user_id' => 2
            ],
            [
                'role_id' => 2,
                'user_id' => 2
            ],
            [
                'role_id' => 1,
                'user_id' => 3
            ],
            [
                'role_id' => 2,
                'user_id' => 3
            ],
            [
                'role_id' => 1,
                'user_id' => 4
            ],
            [
                'role_id' => 2,
                'user_id' => 4
            ],
            [
                'role_id' => 1,
                'user_id' => 5
            ],
            [
                'role_id' => 2,
                'user_id' => 5
            ],
        ]);
    }
}
