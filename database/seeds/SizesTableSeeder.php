<?php

use Illuminate\Database\Seeder;

class SizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('sizes')->delete();

        \DB::table('sizes')->insert([
            0 =>
                [
                    'name' => 'XS',
                    'size_group_id' => 1,
                ],
            1 =>
                [
                    'name' => 'S',
                    'size_group_id' => 1,
                ],
            2 =>
                [
                    'name' => 'M',
                    'size_group_id' => 1,
                ],
            3 =>
                [
                    'name' => 'L',
                    'size_group_id' => 1,
                ],
            4 =>
                [
                    'name' => 'XL',
                    'size_group_id' => 1,
                ],
            5 =>
                [
                    'name' => 'XXL',
                    'size_group_id' => 1,
                ],
            6 =>
                [
                    'name' => 'XXXL',
                    'size_group_id' => 1,
                ],
            7 =>
                [
                    'name' => 'XXXXL',
                    'size_group_id' => 1,
                ],
        ]);
    }
}