<?php

use Illuminate\Database\Seeder;

class ColorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('colors')->delete();

        \DB::table('colors')->insert([
            0 =>
                [
                    'id' => 1,
                    'name' => 'Silver/Black',
                    'color_hex' => '#000',
                ],
            1 =>
                [
                    'id' => 2,
                    'name' => 'Silver/Red',
                    'color_hex' => 'red',
                ],
            2 =>
                [
                    'id' => 3,
                    'name' => 'Silver/Blue',
                    'color_hex' => 'blue',
                ],
        ]);
    }
}
