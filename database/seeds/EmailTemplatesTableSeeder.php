<?php

use Illuminate\Database\Seeder;

class EmailTemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('email_templates')->delete();

        \DB::table('email_templates')->insert([0 => [
            'id' => 1,
            'title' => 'Account Registration',
            'subject' => 'Welcome to SuperiorPromos.com',
            'body' => $this->getAccountRegistrationBody(),
            'type' => 'html',
            'description' => 'CUSTOMER_NAME Customer Name
PASSWORD Customer Password
CUSTOMER_ID Customer Id
CUSTOMER_LOGIN Customer E-Mail',
        ]]);
    }

    protected function getAccountRegistrationBody()
    {
        return '<table border="0" width="550" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="font-family: Arial; font-size: 14px; color: #404040; border: 1px solid #e7e7e7; padding: 30px 20px 30px 20px;" align="left" bgcolor="#ffffff"><strong style="font-family: Arial;">Dear [[CUSTOMER_NAME]],</strong>
<p style="font-family: Arial;">Thank You for becoming a member of the world\'s most sophisticated customizable promotional products website.</p>
<p style="font-family: Arial;">At SuperiorPromos.com we want to provide you with the highest level of superior service, the most competitive pricing, the best products and an array of advanced features to make your shopping experience as easy as possible.</p>
<table border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="padding-right: 30px; font-family: Arial;"><strong style="font-family: Arial;">Customer No:</strong></td>
<td style="font-family: Arial;">[[CUSTOMER_ID]]</td>
</tr>
<tr>
<td style="padding-right: 30px; font-family: Arial;"><strong style="font-family: Arial;">Login:</strong></td>
<td style="font-family: Arial;">[[CUSTOMER_LOGIN]]</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table border="0" width="550" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="height: 2px; font-size: 2px; font-family: Arial;" width="2">&nbsp;</td>
<td style="background: #e7e7e7; height: 2px; font-size: 2px; font-family: Arial;" width="546">&nbsp;</td>
<td style="height: 2px; font-size: 2px; font-family: Arial;" width="2">&nbsp;</td>
</tr>
</tbody>
</table>';
    }
}
