<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(VoyagerDatabaseSeeder::class);
        //$this->call(EmailTemplatesTableSeeder::class);
        //$this->call(PromotionalBlockSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(StateTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UserRolesTableSeeder::class);
        $this->call(VendorsTableSeeder::class);
        $this->call(PromotionalBlockSeeder::class);
        //$this->call(ColorTableSeeder::class);
        //$this->call(ImprintColorGroupTableSeeder::class);
        //$this->call(StageTableSeeder::class);
        $this->call(SizeGroupsTableSeeder::class);
        $this->call(SizesTableSeeder::class);
    }
}
