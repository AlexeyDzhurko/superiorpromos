<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Order::class, function (Faker\Generator $faker) {
    return [
        'user_id' => 1,
        'shipping_first_name' => $faker->firstName,
        'shipping_middle_name' => $faker->lastName,
        'shipping_last_name' => $faker->lastName,
        'shipping_title' => $faker->title,
        'shipping_suffix' => '',
        'shipping_company_name' => $faker->company,
        'shipping_address_line_1' => $faker->address,
        'shipping_address_line_2' => $faker->address,
        'shipping_city' => $faker->city,
        'shipping_state' => 'NY',
        'shipping_zip' => $faker->postcode,
        'shipping_country' => 'USA',
        'shipping_province' => '',
        'shipping_day_telephone' => $faker->phoneNumber,
        'shipping_ext' => '',
        'shipping_fax' => '',
        'billing_first_name' => $faker->firstName,
        'billing_middle_name' => $faker->lastName,
        'billing_last_name' => $faker->lastName,
        'billing_title' => $faker->title,
        'billing_suffix' => '',
        'billing_company_name' => $faker->company,
        'billing_address_line_1' => $faker->address,
        'billing_address_line_2' => $faker->address,
        'billing_city' => $faker->city,
        'billing_state' => 'NY',
        'billing_zip' => $faker->postcode,
        'billing_country' => 'USA',
        'billing_province' => '',
        'billing_day_telephone' => $faker->phoneNumber,
        'billing_ext' => '',
        'billing_fax' => '',
        'multiple_location_comment' => $faker->text,
    ];
});

$factory->define(App\Models\OrderItem::class, function (Faker\Generator $faker) {

    $unitPrice = $faker->randomFloat(2, 0.1, 100);
    $quantity = $faker->numberBetween(100, 2000);
   return [
        'product_id' => 4,
        'quantity' => $quantity,
        'item_price' => $unitPrice,
        'price' => $unitPrice*$quantity,
        'options_price' => 0,
        'check_notes' => $faker->boolean(20),
        'auto_remind' => false,
        'not_paid' => $faker->boolean(10),
        'art_file_name' => null,
        'art_file_path' => null,
        'po_number' => null,
        'invoice_file' => null,
        'tax_exempt' => false,
        'estimation_zip' => null,
        'estimation_shipping_method' => null,
        'estimation_shipping_price' => null,
        'own_shipping_type' => null,
        'own_account_number' => null,
        'own_shipping_system' => null,
        'shipping_method' => 'outside_us',
        'vendor_id' => null,
        'cart_id' => null,
        'received_date' => $faker->dateTime,
        'imprint_comment' => $faker->text(500),
        'shipping_first_name' => $faker->firstName,
        'shipping_middle_name' => $faker->lastName,
        'shipping_last_name' => $faker->lastName,
        'shipping_title' => $faker->title,
        'shipping_suffix' => '',
        'shipping_company_name' => $faker->company,
        'shipping_address_line_1' => $faker->address,
        'shipping_address_line_2' => $faker->address,
        'shipping_city' => $faker->city,
        'shipping_state' => 'NY',
        'shipping_zip' => $faker->postcode,
        'shipping_country' => 'USA',
        'shipping_province' => '',
        'shipping_day_telephone' => $faker->phoneNumber,
        'shipping_ext' => '',
        'shipping_fax' => '',
        'billing_first_name' => $faker->firstName,
        'billing_middle_name' => $faker->lastName,
        'billing_last_name' => $faker->lastName,
        'billing_title' => $faker->title,
        'billing_suffix' => '',
        'billing_company_name' => $faker->company,
        'billing_address_line_1' => $faker->address,
        'billing_address_line_2' => $faker->address,
        'billing_city' => $faker->city,
        'billing_state' => 'NY',
        'billing_zip' => $faker->postcode,
        'billing_country' => 'USA',
        'billing_province' => '',
        'billing_day_telephone' => $faker->phoneNumber,
        'billing_ext' => '',
        'billing_fax' => '',
        'tracking_shipping_date' => $faker->dateTime->format('d-m-Y'),
        'tracking_number' => $faker->numberBetween(10000, 100000),
        'tracking_shipping_company' => 'UPS',
        'tracking_note' => $faker->text(400),
        'tracking_user_id' => 1,
   ];
});
