**Categories Tree**

_GET /api/categories_

`[
{"id": 42, "title":"Auto accessories", "slug":"auto-accessories", "sub_categories": 
    [
        {"id": 47, "title":"CD Holders", "slug":"auto-accessories/cd-holders", "sub_categories": []}
    ]
}
]`