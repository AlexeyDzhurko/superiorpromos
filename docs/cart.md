**Categories Tree**

_GET /api/cart_

```json 
{
    "items": [
        {
            "cart_id": 1,
            "thumbnail":"http://www.superiorpromos.com/img/ucart/images/pimage/1005/_thumbnails/150by150/CS.jpg", 
            "title": "Bic Clic Stic Promotional Pen",
            "id": 7732,
            "lowest_price": 96.18,
            "quantity": 3000,
            "item_price": 0.10,
            "shipping": "~",
            "total: 300.00,
            "price_grid": [
                            {
                                "quantity":250,
                                "regular_price":0.38,
                                "sale_price":0.28,
                                "active": false,
                            },  
                            {
                                "quantity":300,
                                "regular_price":0.38,
                                "sale_price":0.28,
                                "active": false,
                            },      
                            {
                                "quantity":3000,
                                "regular_price":0.38,
                                "sale_price":0.28,
                                "active": true,
                            },                                                           
            ],
        }
    ],
    "total": 600.00
}
```