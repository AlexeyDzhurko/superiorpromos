**Main Page Slider**

_GET /api/slider_

`[
{"image":"http://www.superiorpromos.com/img/ucart/images/carousel/rotate-clearance.jpg", "url":"http://www.superiorpromos.com/pens-pencils/pens-plastic"},
{"image":"http://www.superiorpromos.com/img/ucart/images/carousel/rotate-charge.jpg", "url":"http://www.superiorpromos.com/products/electronic-items"},
{"image":"http://www.superiorpromos.com/img/ucart/images/carousel/rotating-pens-.25less.jpg", "url":"http://www.superiorpromos.com/products/clearance-products"}
]`