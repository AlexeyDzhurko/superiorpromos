**Promotional Products for Main Page**

_GET /api/product/promotional-products

```
{
    "top_block": 
        "title":"Promotional Products Promotional Items Best Selling Promotional Products!",
        "slug": "products/clearance-products",
        "products":
            [
                {
                        "thumbnail":"http://www.superiorpromos.com/img/ucart/images/pimage/1005/_thumbnails/150by150/CS.jpg", 
                        "title": "Bic Clic Stic Promotional Pen",
                        "id": 7732,
                        "lowest_price": 96.18,
                        "min_quantity": 150,
                        "production_time": "within 1 working day*",
                        "slug":"pens-pencils/pens-plastic/bic-clic-stic-pen",
                        "color_groups":
                            [
                                {
                                    "name":"Trim Colors",
                                    "colors": 
                                        [
                                            {
                                                "title":"Black",
                                                "id":100,
                                                "rgb":"000000",
                                                "image":null,
                                            },
                                            {
                                                 "title":"Assorted",
                                                 "id":100,
                                                 "rgb":null,
                                                 "image":"img/ucart/images/colors/assorted.jpg",
                                             },                           
                                        ]
                                    
                                }
                            ]
                            "imprint_colors":
                            [
                                {
                                    "color_group":"Trim Colors",
                                    "colors": 
                                        [
                                            {
                                                "title":"Black",
                                                "id":100,
                                                "rgb":"000000",
                                                "image":null,
                                            },
                                            {
                                                 "title":"Assorted",
                                                 "id":100,
                                                 "rgb":null,
                                                 "image":"img/ucart/images/colors/assorted.jpg",
                                             },                           
                                        ]
                                    
                                }
                            ],
                            "price_grid": 
                            [
                                {
                                    "quantity":250,
                                    "regular_price":0.38,
                                    "sale_price":0.28
                                }    
                            ],
                    }
            ],    
    "middle_block": [],    
    "bottom_block": [],    
}
```