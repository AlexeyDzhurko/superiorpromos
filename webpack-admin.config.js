var path = require('path'),
    UglifyJSPlugin = require('uglifyjs-webpack-plugin'),
    fs = require('fs'),
    webpack = require('webpack'),
    CleanObsoleteChunks = require('webpack-clean-obsolete-chunks'),
    autoprefixer = require('autoprefixer'),
    CompressionPlugin = require("compression-webpack-plugin"),
    cleanOptions = {
        verbose: true
    };
module.exports = function(env) {
    return {
        devtool: env.NODE_ENV == 'development' ? '#eval' : '#source-map',
        entry: {
            vendorsAdmin: './webpack-admin.vendors.js',
            superiorAdmin: [
                // app
                './resources/assets/admin/js/app.js',
                // controllers
                './resources/assets/admin/js/controllers/color/color-list.js',
                './resources/assets/admin/js/controllers/color-group/color-group-list.js',
                './resources/assets/admin/js/controllers/customer/address.js',
                './resources/assets/admin/js/controllers/customer/addresses-list.js',
                './resources/assets/admin/js/controllers/customer/customer.js',
                './resources/assets/admin/js/controllers/faq/faq-list.js',
                './resources/assets/admin/js/controllers/post/post-list.js',
                './resources/assets/admin/js/controllers/post-category/post-category-list.js',
                './resources/assets/admin/js/controllers/testimonials/testimonials-list.js',
                './resources/assets/admin/js/controllers/address.js',
                './resources/assets/admin/js/controllers/art-proofs.js',
                './resources/assets/admin/js/controllers/block-list.js',
                './resources/assets/admin/js/controllers/case-study-list.js',
                './resources/assets/admin/js/controllers/category.js',
                './resources/assets/admin/js/controllers/customer-list.js',
                './resources/assets/admin/js/controllers/discount-list.js',
                './resources/assets/admin/js/controllers/discount.js',
                './resources/assets/admin/js/controllers/extra-billing.js',
                './resources/assets/admin/js/controllers/froogle.js',
                './resources/assets/admin/js/controllers/info-message-list.js',
                './resources/assets/admin/js/controllers/invoice.js',
                './resources/assets/admin/js/controllers/main.js',
                './resources/assets/admin/js/controllers/notes.js',
                './resources/assets/admin/js/controllers/order.js',
                './resources/assets/admin/js/controllers/orders-list.js',
                './resources/assets/admin/js/controllers/page-list.js',
                './resources/assets/admin/js/controllers/payment-information-template-list.js',
                './resources/assets/admin/js/controllers/pms-color-list.js',
                './resources/assets/admin/js/controllers/product-category.js',
                './resources/assets/admin/js/controllers/product-color-group.js',
                './resources/assets/admin/js/controllers/product-icon-list.js',
                './resources/assets/admin/js/controllers/product-list.js',
                './resources/assets/admin/js/controllers/product-suboption.js',
                './resources/assets/admin/js/controllers/product.js',
                './resources/assets/admin/js/controllers/promos.js',
                './resources/assets/admin/js/controllers/promotional-glossary-list.js',
                './resources/assets/admin/js/controllers/reminder-list.js',
                './resources/assets/admin/js/controllers/review-list.js',
                './resources/assets/admin/js/controllers/setting-list.js',
                './resources/assets/admin/js/controllers/size-group-list.js',
                './resources/assets/admin/js/controllers/slider-list.js',
                './resources/assets/admin/js/controllers/stages-list.js',
                './resources/assets/admin/js/controllers/stages.js',
                './resources/assets/admin/js/controllers/state-list.js',
                './resources/assets/admin/js/controllers/survey-list.js',
                './resources/assets/admin/js/controllers/tracking-information.js',
                './resources/assets/admin/js/controllers/user-notes.js',
                './resources/assets/admin/js/controllers/vendor.js',
                './resources/assets/admin/js/controllers/vendors-list.js',
                // directives
                './resources/assets/admin/js/directives/sortableHeader.js',
                './resources/assets/admin/js/directives/htmlEditor.js',
                // services
                './resources/assets/admin/js/services/dictionary.js',
                './resources/assets/admin/js/services/formHelper.js',
                './resources/assets/admin/js/services/modalHelper.js',
                './resources/assets/admin/js/services/validationErrorsHandler.js'
            ]
        },
        resolve: {
            modules:[
                path.resolve('./resources/assets/admin'),
                path.resolve('./node_modules')
            ]
        },
        plugins: env.NODE_ENV === 'production' ? [
            new CompressionPlugin({
                filename: '[path].gz[query]',
                algorithm: "gzip",
                test: /\.js$|\.html$/,
                threshold: 10240,
                minRatio: 0.8
            }),
            new UglifyJSPlugin(
                {
                    parallel: 4,
                    include: /\.min\.js$/,
                    minimize: true,
                    uglifyOptions: {
                        output: {
                            comments: false,
                            beautify: false
                        }
                    }
                }
            ),
            new webpack.LoaderOptionsPlugin({
                minimize: true,
                debug: false
            }),
            new CleanObsoleteChunks(cleanOptions),
            new webpack.optimize.OccurrenceOrderPlugin()
        ] : [
            new webpack.LoaderOptionsPlugin({
                minimize: true,
                debug: false
            }),
            new CleanObsoleteChunks(cleanOptions)
        ],
        output: {
            filename: '[name].min.js',
            path: path.join(__dirname, 'public/dist/admin'),
            publicPath: './dist/'
        },
        module: {
            rules: [
                {
                    test: /\.html$/,
                    use:[
                        {
                            loader:'html-loader'
                            //options:{
                            //    ignoreCustomFragments: [/\{\{.*?}}/],
                            //    attrs: ['ng-src']
                            //}
                        }
                    ]
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    use: [
                        'url-loader?limit=1000&name=img/[name].[ext]',
                        'img-loader'
                    ]
                },
                {
                    test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    loader: 'url-loader?name=fonts/[name].[ext]'
                },
                {
                    test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    loader: 'file-loader?name=fonts/[name].[ext]'
                },
                {
                    test: /\.(css|scss)$/,
                    use:[
                        {
                            loader: 'style-loader'
                        },
                        {
                            loader: 'css-loader'
                        },
                        {
                            loader:'sass-loader'
                        },
                        {
                            loader:'postcss-loader',
                            options: {
                                plugins: function () {
                                    return [
                                        autoprefixer
                                    ];
                                }
                            }
                        }
                    ]
                }
            ]
        }
    }
};
